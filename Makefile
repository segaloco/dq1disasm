#
# Dragon Quest/Warrior for the Famicom/NES
#
AS		=	ca65
ASFLAGS_DQ	=	-DDQ -DCNROM
ASFLAGS_DW	=	-DDW -DMMC1
ASFLAGS		=	-U -I./inc $(ASFLAGS_DQ)
LD		=	ld65
LDFLAGS_DQ	=	-C link.dq.ld
LDFLAGS_DW	=	-C link.dw.ld
AWK		=	gawk
LANG_JP		=	ja_JP.utf-8
CHECK		=	util/check.sh

CKSUM_DQ	=	2725274875
CKSUM_CHRZERO_DQ =	3882872165
CKSUM_DW	=	2810802716
CKSUM_CHRZERO_DW =	1389796132

DQ		= 	dq.nes
DW		=	dw.nes

TGT		=	$(DQ)

PRG_DQ		=	PRG.dq.bin
CHR0_DQ		=	CHR0.dq.bin
CHR1_DQ		=	CHR1.dq.bin
CHR2_DQ		=	CHR2.dq.bin
CHR3_DQ		=	CHR3.dq.bin

PRG0_DW		=	PRG0.dw.bin
PRG1_DW		=	PRG1.dw.bin
PRG2_DW		=	PRG2.dw.bin
PRG3_DW		=	PRG3.dw.bin
CHR0_DW		=	CHR0.dw.bin
CHR1_DW		=	CHR1.dw.bin

ROMS_DQ		=	$(PRG_DQ) \
			$(CHR0_DQ) \
			$(CHR1_DQ) \
			$(CHR2_DQ) \
			$(CHR3_DQ)

ROMS_DW		=	$(PRG0_DW) \
			$(PRG1_DW) \
			$(PRG2_DW) \
			$(PRG3_DW) \
			$(CHR0_DW) \
			$(CHR1_DW)

PRG_RODATA_WINDOWS =	data/tbl_windows.o \
			data/windows/main.o \
			data/windows/stats.o \
			data/windows/dialog.o \
			data/windows/field.o \
			data/windows/battle.o \
			data/windows/talk_dir.o \
			data/windows/spell.o \
			data/windows/tool.o \
			data/windows/shop.o \
			data/windows/decision.o \
			data/windows/buysell.o

PRG_RODATA_MAPS	=	data/map/map_overworld.o \
			data/map/tbl_maps.o \
			data/map/map_02.o \
			data/map/map_03.o \
			data/map/map_04.o \
			data/map/map_05.o \
			data/map/map_06.o \
			data/map/map_07.o \
			data/map/map_08.o \
			data/map/map_0A.o \
			data/map/map_09.o \
			data/map/map_0B.o \
			data/map/map_0C.o \
			data/map/map_0D.o \
			data/map/map_0E.o \
			data/map/map_0F.o \
			data/map/map_10.o \
			data/map/map_11.o \
			data/map/map_12.o \
			data/map/map_13.o \
			data/map/map_14.o \
			data/map/map_15.o \
			data/map/map_16.o \
			data/map/map_17.o \
			data/map/map_18.o \
			data/map/map_1A.o \
			data/map/map_1B.o \
			data/map/map_19.o \
			data/map/map_1C.o \
			data/map/map_1D.o

PRG_RODATA	=	data/tbl_shops.o \
			data/tbl_exp.o \
			data/sprites/tbl_enemy_spr_idx.o \
			data/tbl_encounters.o \
			data/tbl_metatiles.o \
			data/tbl_blkids.o \
			$(PRG_RODATA_WINDOWS) \
			data/palettes.o \
			data/ppu/bgmap_battle_field.o \
			data/tbl_mpcosts.o \
			data/strings/str_common.o \
			$(PRG_RODATA_MAPS) \
			data/tbl_sprites.o \
			data/tbl_npcs.o

PRG_LIB		=	src/ppu/ppu_nmi.o \
			src/ppu/ppu_nmi_write.o \
			src/ppu/ppu_pal_fadeout.o \
			src/ppu/ppu_pal_fadein.o \
			src/ppu/ppu_pal_load.o \
			src/library/nt_calc.o \
			src/library/ppu_write_attr.o \
			src/library/area_check.o \
			src/library/map_calc.o \
			src/library/math_mulw.o \
			src/library/math_div.o \
			src/library/math_rand.o \
			src/library/text_loader.o \
			src/ppu/oam.o \
			src/ppu/ppu_clearram.o \
			src/ppu/ppu_position.o \
			src/map.o \
			src/general_bss.o \
			src/library/joypad.o \
			src/ppu/ppu_bss.o \
			src/sprite.o

PRG_WINDOW	=	src/window/putc.o \
			src/window/function.o \
			src/window/unloader.o \
			src/window/attr.o \
			src/window/string.o \
			src/window/ntpos.o \
			src/window/cnrom_w.o \
			src/window/chooser.o \
			src/window/bss_w.o

PRG_START	=	src/start.o \
			src/game.o

PRG_DIALOGS	=	src/dialogs/command.o \
			src/dialogs/talk.o \
			src/dialogs/shop.o \
			src/dialogs/keys.o \
			src/dialogs/fairy.o \
			src/dialogs/inn.o \
			src/dialogs/routines.o \
			src/dialogs/spell.o \
			src/dialogs/strings.o \
			src/dialogs/inventory.o \
			src/dialogs/search.o \
			src/dialogs/combat.o

PRG_ENG		=	src/battle.o \
			src/stats.o \
			src/password.o \
			src/game_bss.o \
			src/cnrom/cnrom_high.o \
			src/credits.o

PRG_APU		=	src/apu.o \
			data/apu_tables.o \
			data/apu/sfx/sfx_fire.o \
			data/apu/sfx/sfx_ffdamage.o \
			data/apu/sfx/sfx_wing.o \
			data/apu/sfx/sfx_stairs.o \
			data/apu/sfx/sfx_run.o \
			data/apu/sfx/sfx_swamp.o \
			data/apu/sfx/sfx_menu.o \
			data/apu/sfx/sfx_confirm.o \
			data/apu/sfx/sfx_strike.o \
			data/apu/sfx/sfx_power.o \
			data/apu/sfx/sfx_hit.o \
			data/apu/sfx/sfx_damage.o \
			data/apu/sfx/sfx_windup.o \
			data/apu/sfx/sfx_miss1.o \
			data/apu/sfx/sfx_miss2.o \
			data/apu/sfx/sfx_bump.o \
			data/apu/sfx/sfx_text.o \
			data/apu/sfx/sfx_spell.o \
			data/apu/sfx/sfx_glow.o \
			data/apu/sfx/sfx_chest.o \
			data/apu/sfx/sfx_door.o \
			data/apu/songs/song_null.o \
			data/apu/songs/song_castle.o \
			data/apu/songs/song_levelup.o \
			data/apu/songs/song_lora.o \
			data/apu/songs/song_inn.o \
			data/apu/songs/song_win.o \
			data/apu/songs/song_town.o \
			data/apu/songs/song_overworld.o \
			data/apu/songs/song_dungeon.o \
			data/apu/songs/song_battle.o \
			data/apu/songs/song_boss.o \
			data/apu/songs/song_lyre.o \
			data/apu/songs/song_flute.o \
			data/apu/songs/song_bridge.o \
			data/apu/songs/song_lose.o \
			data/apu/songs/song_curse.o

PRG_DQ_OBJS	=	$(PRG_RODATA) \
			$(PRG_LIB) \
			$(PRG_WINDOW) \
			$(PRG_START) \
			$(PRG_DIALOGS) \
			$(PRG_ENG) \
			$(PRG_APU)

PRG0_DW_OBJS	=	data/prg0.dw.o

PRG1_DW_OBJS	=	data/prg1.dw.o

PRG2_DW_OBJS	=	data/prg2.dw.o

PRG3_DW_OBJS	=	src/start.o \
			data/prg3.dw.o
			

CHR0_DQ_OBJS	=	data/chr00.o

CHR1_DQ_OBJS	=	data/strings/strings.o \
			data/tbl_levelstats.o

CHR2_DQ_OBJS	=	data/chr02.o \
			data/tbl_credits.o \
			data/apu/songs/song_credits.o

DATA_START_PW	=	src/chr3/start_pw_load_map.o \
			src/chr3/start_pw.o

DATA_START_NEW	=	src/chr3/start_new_load_map.o \
			src/chr3/start_new.o

DATA_ENEMY	=	data/sprites/tbl_enemy_spr.o \
			data/ppu/tbl_enemy_pal.o \
			data/ppu/dialog_combat_ani.o

DATA_MAPITEMS	=	data/tbl_treasures.o \
			data/map_transitions_low.o \
			data/map_transitions_high.o \

DATA_TITLE	=	src/chr3/title_load_map.o \
			src/chr3/title.o

CHR3_DQ_OBJS	=	$(DATA_START_PW) \
			$(DATA_START_NEW) \
			$(DATA_ENEMY) \
			$(DATA_MAPITEMS) \
			data/tbl_enemy_props.o \
			src/chr3/dragonlord.o \
			data/strings/offsets.o \
			$(DATA_TITLE)

CHR0_DW_OBJS	=	data/chr00.dw.o

CHR1_DW_OBJS	=	data/chr01.dw.o

OBJS_DQ		=	$(PRG_DQ_OBJS) \
			$(CHR0_DQ_OBJS) \
			$(CHR1_DQ_OBJS) \
			$(CHR2_DQ_OBJS) \
			$(CHR3_DQ_OBJS)

OBJS_DW		=	$(PRG0_DW_OBJS) \
			$(PRG1_DW_OBJS) \
			$(PRG2_DW_OBJS) \
			$(PRG3_DW_OBJS) \
			$(CHR0_DW_OBJS) \
			$(CHR1_DW_OBJS)

HEADER_DQ	=	data/header.dq.bin

HEADER_DW	=	data/header.dw.bin

STRINGS_DQ	=	data/tbl_enemy_props.j.s \
			data/strings/str_common.j.s \
			data/windows/battle.j.s \
			data/windows/buysell.j.s \
			data/windows/decision.j.s \
			data/windows/dialog.j.s \
			data/windows/field.j.s \
			data/windows/main.j.s \
			data/windows/shop.j.s \
			data/windows/spell.j2.s \
			data/windows/stats.j2.s \
			data/windows/talk_dir.j2.s \
			data/windows/tool.j2.s \
			src/dialogs/search.j.s \
			data/strings/strings.j2.s \
			src/chr3/start_pw_load_map.j2.s \
			src/chr3/start_pw.j2.s \
			src/chr3/start_new_load_map.j2.s \
			src/chr3/start_new.j2.s

STRINGS_DW	=	

.SUFFIXES : .j .j2

all: $(TGT)

$(DQ): $(HEADER_DQ) $(OBJS_DQ)
	$(LD) $(OBJS_DQ) $(LDFLAGS_DQ)
	cat $(HEADER_DQ) $(ROMS_DQ) > $@
	$(CHECK) $@ $(CKSUM_DQ)

$(DW): $(HEADER_DW) $(OBJS_DW)
	$(LD) $(OBJS_DW) $(LDFLAGS_DW)
	cat $(HEADER_DW) $(ROMS_DW) > $@
	$(CHECK) $@ $(CKSUM_DW)

.j2.o:
	LANG=$(LANG_JP) $(AWK) -f util/string.awk $< >$<.s
	$(AS) $(ASFLAGS) -o $@ $<.s

.j.o:
	LANG=$(LANG_JP) $(AWK) -f util/string.awk $< | sed -f util/string2.sed >$<.s
	$(AS) $(ASFLAGS) -o $@ $<.s

data/header.dq.bin:
	data/header.dq.sh >$@

data/header.dw.bin:
	data/header.dw.sh >$@

clean:
	rm -rf $(DQ) $(DW) $(ROMS_DQ) $(ROMS_DW) $(OBJS_DQ) $(OBJS_DW) $(HEADER_DQ) $(HEADER_DW) $(STRINGS_DQ) $(STRINGS_DW)
