.ifndef MAP_CALC_I
MAP_CALC_I = 1

PPU_POS_BIT7		=	$80
PPU_POS_BIT6		=	$40
PPU_POS_BIT5		=	$20
PPU_POS_MASK		=	$1F

	.import		map_tile_xpos: zeropage		; $0F
	.import		map_tile_ypos: zeropage		; $10
	.import		map_data: zeropage		; $11
	.import		map_width: zeropage		; $13
	.import		map_height: zeropage		; $14
	.import		map_bound_id: zeropage		; $15
	.import		map_type: zeropage		; $16
	.import		map_cover_data: zeropage	; $17
	.import		map_cover_stat: zeropage	; $19

	.import		map_get_blkid
	.import		map_check_other
	.import		map_block_mod

.endif	; MAP_CALC_I
