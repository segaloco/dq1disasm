.ifndef	PPU_NMI_WRITE_I
PPU_NMI_WRITE_I = 1

	.import		ppu_nmi_write_data: zeropage
	.import		ppu_nmi_apply_mirror: zeropage
	.import		ppu_nmi_write_dest: zeropage

	.import		ppu_nmi_write

.endif	; PPU_NMI_WRITE_I
