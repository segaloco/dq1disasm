.ifndef	PPU_NMI_I
PPU_NMI_I = 1

.include	"ppu_commands.i"

	.import		zeroword: zeropage
	.import		ppu_nmi_run: zeropage
	.import		ppu_nmi_entry_count: zeropage
	.import		ppu_nmi_entry_index: zeropage
	.import		ppu_nmi_scc_h: zeropage
	.import		ppu_nmi_bg_high: zeropage
	.import		ppu_nmi_scc_v: zeropage

	.import		ppu_nmi_buffer

	.import		ppu_nmi_enter
	.import		ppu_nmi_refresh
	.import		ppu_nmi_wait

.endif	; PPU_NMI_I
