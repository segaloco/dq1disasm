.ifndef JOYPAD_I
JOYPAD_I = 1

	.enum	joypad_button
		face_a	= 1
		face_b	= 2
		select	= 4
		start	= 8
		up	= 16
		down	= 32
		left	= 64
		right	= 128
	.endenum

	.import	joypad_state: zeropage

.endif	; JOYPAD_I
