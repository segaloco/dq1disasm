.ifndef NPC_I
NPC_I = 1

	.import		map_npc_xpos: zeropage
	.import		map_npc_ypos: zeropage
	.import		sprite_npc_stop: zeropage

	.import		sprite_npc_lora_xpos: zeropage
	.import		sprite_npc_lora_ypos: zeropage
	.import		sprite_npc_lora_offset: zeropage

	NPC_ENTRY_SIZE	= 3

	NPCS_STOP	= $FF
	NPCS_RUN	= $00

	.enum	npc_type
		man		; $00
		fighter		; $20
		guard_1		; $40
		shopkeep	; $60
		king		; $80
		wizard		; $A0
		woman		; $C0
		guard_2		; $E0
	.endenum

.endif	; NPC_I