.ifndef APU_COMMANDS_I
APU_COMMANDS_I = 1

	.enum	apu_notes
		C2	= $00
		Cx2	= $01
		D2	= $02
		Dx2	= $03
		E2	= $04
		F2	= $05
		Fx2	= $06
		G2	= $07
		Ab2	= $08
		A2	= $09
		Ax2	= $0A
		B2	= $0B
		C3	= $0C
		Cx3	= $0D
		D3	= $0E
		Dx3	= $0F
		E3	= $10
		F3	= $11
		Fx3	= $12
		G3	= $13
		Ab3	= $14
		A3	= $15
		Ax3	= $16
		B3	= $17
		C4	= $18
		Cx4	= $19
		D4	= $1A
		Dx4	= $1B
		E4	= $1C
		F4	= $1D
		Fx4	= $1E
		G4	= $1F
		Ab4	= $20
		A4	= $21
		Ax4	= $22
		B4	= $23
		C5	= $24
		Cx5	= $25
		D5	= $26
		Dx5	= $27
		E5	= $28
		F5	= $29
		Fx5	= $2A
		G5	= $2B
		Ab5	= $2C
		A5	= $2D
		Ax5	= $2E
		B5	= $2F
		C6	= $30
		Cx6	= $31
		D6	= $32
		Dx6	= $33
		E6	= $34
		F6	= $35
		Fx6	= $36
		G6	= $37
		Ab6	= $38
		A6	= $39
		Ax6	= $3A
		B6	= $3B
		C7	= $3C
		Cx7	= $3D
		D7	= $3E
		Dx7	= $3F
		E7	= $40
		F7	= $41
		Fx7	= $42
		G7	= $43
		Ab7	= $44
		A7	= $45
		Ax7	= $46
		B7	= $47
		C8	= $48
	.endenum

apu_cmd_note_load		= $80
apu_cmd_noise_play		= $E0
apu_cmd_trough_end		= $F6
apu_cmd_trough			= $F7
apu_cmd_noise_vol_change	= $F8
apu_cmd_note_get		= $F9
apu_cmd_pulse_sweep_cfg		= $FA
apu_cmd_wave_vol_change		= $FB
apu_cmd_next_byte		= $FC
apu_cmd_song_ret		= $FD
apu_cmd_song_jsr		= $FE
apu_cmd_tempo_change		= $FF

APU_SONG_INDEX_SIZE		= 6
APU_SFX_INDEX_SIZE		= 2

APU_DATA_SFX_DONE		= 0

apu_empty			= 0

APU_SONG_NULL			=	.lobyte((apu_song_null-apu_data_songs) / APU_SONG_INDEX_SIZE)
APU_SONG_CASTLE_THRONEROOM	=	.lobyte((apu_song_caste_throneroom-apu_data_songs) / APU_SONG_INDEX_SIZE)
APU_SONG_CASTLE_COURTYARD	=	.lobyte((apu_song_castle_courtyard-apu_data_songs) / APU_SONG_INDEX_SIZE)
APU_SONG_TOWN			=	.lobyte((apu_song_town-apu_data_songs) / APU_SONG_INDEX_SIZE)
APU_SONG_OVERWORLD			=	.lobyte((apu_song_overworld-apu_data_songs) / APU_SONG_INDEX_SIZE)
APU_SONG_DUNGEON0		=	.lobyte((apu_song_dungeon0-apu_data_songs) / APU_SONG_INDEX_SIZE)
APU_SONG_DUNGEON1		=	.lobyte((apu_song_dungeon1-apu_data_songs) / APU_SONG_INDEX_SIZE)
APU_SONG_DUNGEON2		=	.lobyte((apu_song_dungeon2-apu_data_songs) / APU_SONG_INDEX_SIZE)
APU_SONG_DUNGEON3		=	.lobyte((apu_song_dungeon3-apu_data_songs) / APU_SONG_INDEX_SIZE)
APU_SONG_DUNGEON4		=	.lobyte((apu_song_dungeon4-apu_data_songs) / APU_SONG_INDEX_SIZE)
APU_SONG_DUNGEON5		=	.lobyte((apu_song_dungeon5-apu_data_songs) / APU_SONG_INDEX_SIZE)
APU_SONG_DUNGEON6		=	.lobyte((apu_song_dungeon6-apu_data_songs) / APU_SONG_INDEX_SIZE)
APU_SONG_DUNGEON7		=	.lobyte((apu_song_dungeon7-apu_data_songs) / APU_SONG_INDEX_SIZE)

; apusong_m - start the APU song at the given index
.macro	apusong_m	song
	lda	#.lobyte((song-apu_data_songs) / APU_SONG_INDEX_SIZE)
.endmacro

; apusfx_m - play the APU sfx at the given index
.macro	apusfx_m	sfx
	lda	#.lobyte((sfx-apu_data_sfx_table) / APU_SFX_INDEX_SIZE)+$80
.endmacro

.endif	; APU_COMMANDS_I
