.ifndef PPU_BSS_I
PPU_BSS_I = 1

	.import		scroll_pos_x: zeropage
	.import		scroll_pos_y: zeropage
	.import		ppu_nt_xpos: zeropage
	.import		ppu_nt_ypos: zeropage
	.import		ppu_blkdel_flags: zeropage
	.import		ppu_blk_counter: zeropage
	.import		ppu_npc_loop_iter: zeropage
	.import		ppu_frame_count: zeropage

.endif  ; PPU_BSS_I
