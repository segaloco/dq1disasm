.ifndef PPU_I
PPU_I = 1

; References
; 1 - Ricoh RP2C02 Picture Processing Unit - December 3rd, 1982
; 2 - Ricoh RP2A03 Custom CPU - December 3rd, 1982

; Object Attribute RAM (OAM) [1](11.1.2)(p. 5)
        OBJ_POS_V       = 0
        OBJ_CHR_NO      = 1
        OBJ_ATTR        = 2
        OBJ_POS_H       = 3
        OBJ_SIZE        = 4

; Control Register 0 [1](11.2.2)(p. 6)
PPU_CTLR0	= $2000
	.enum   ppu_ctlr0
		bg1	= 0
		bg2	= 1
		bg3	= 2
		bg4	= 3

		obj_seg0	= 0
		obj_seg1	= 8
		bg_seg0		= 0
		bg_seg1		= 16
		int		= 128
	.endenum

; Control Register 1 [1](11.2.3)(p. 6)
PPU_CTLR1	= $2001
	.enum   ppu_ctlr1
		color		= 0
		monotone	= 1
		bglblk_on	= 0
		bglblk_off	= 2
		objlblk_on	= 0
		objlblk_off	= 4
		bgblk_on	= 0
		bgblk_off	= 8
		objblk_on	= 0
		objblk_off	= 16
		red		= 32
		green		= 64
		blue		= 128
	.endenum

; Status Register [1](11.2.4)(p. 6)
PPU_SR		= $2002

; Scroll Register H,V [1](11.2.7)(p. 6)
PPU_SCC_H_V	= $2005
PPU_SCANLINE_240	= $F0

; VRAM Address Register H,L [1](11.2.8)(p. 7)
PPU_VRAM_AR	= $2006

; VRAM Read/Write [1](11.2.8)(p. 7)
PPU_VRAM_IO	= $2007

; Video RAM (VRAM) [1](11.2.9)(p. 7)
PPU_VRAM_BG1	= $2000
PPU_VRAM_BG2	= $2400
PPU_VRAM_COLOR	= $3F00

; Background Format [1](11.2.9.2)(p. 8)
PPU_VRAM_BG_VIS         = $3C0

; Object DMA [2](11.5)(p. 9)
OBJ_DMA		= $4014
	.enum	obj_attribute
		color0		= 0
		color1		= 1
		color2		= 2
		color3		= 3
		color_bits	= 3

		priority_high	= $00
		priority_low	= $20
		h_flip		= $40
		v_flip		= $80
		flip_bits	= (h_flip|v_flip)
	.endenum

; Color Generator [1](11.10)(p. 9)
PPU_COLOR_PAGE_BG	= PPU_VRAM_COLOR+0
PPU_COLOR_PAGE_OBJ	= PPU_VRAM_COLOR+$10
PPU_COLOR_GREY		= $00
PPU_COLOR_AZURE		= $01
PPU_COLOR_BLUE		= $02
PPU_COLOR_VIOLET	= $03
PPU_COLOR_MAGENTA	= $04
PPU_COLOR_ROSE		= $05
PPU_COLOR_RED		= $06
PPU_COLOR_ORANGE	= $07
PPU_COLOR_YELLOW	= $08
PPU_COLOR_YGREEN	= $09
PPU_COLOR_GREEN		= $0A
PPU_COLOR_SPRING	= $0B
PPU_COLOR_CYAN		= $0C
PPU_COLOR_ERR		= $0D
PPU_COLOR_BLACK		= $0E
PPU_COLOR_PITCH		= $0F
PPU_COLOR_LEV0		= $00
PPU_COLOR_LEV1		= $10
PPU_COLOR_LEV2		= $20
PPU_COLOR_LEV3		= $30

PPU_TILE_SIZE		= $10
PPU_TILE_PER_LINE	= $20

.endif	; PPU_I
