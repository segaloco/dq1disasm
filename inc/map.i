.ifndef MAP_I
MAP_I = 1

	.enum	blkprop
		covered = 8
	.endenum

	.enum	maps
		map_00			= $0
		overworld		= $1
		map_02			= $2
		map_03			= $3
		castle_courtyard	= $4
		castle_throneroom	= $5
		map_06			= $6
		map_07			= $7
		map_08			= $8
		map_09			= $9
		map_0A			= $A
		map_0B			= $B
		map_0C			= $C
		map_0D			= $D
		map_0E			= $E
		map_0F			= $F
		map_10			= $10
		map_11			= $11
		map_12			= $12
		map_13			= $13
		map_14			= $14
		dungeon_greendragon	= $15
		map_16			= $16
		map_17			= $17
		map_18			= $18
		map_19			= $19
		map_1A			= $1A
		map_1B			= $1B
		map_1C			= $1C
		map_1D			= $1D
	.endenum

	.enum	tile_type_overworld
		grass
		sand
		hill
		mountain
		water
		rocks
		trees
		swamp
		town
		cave
		castle
		bridge
		stairs
	.endenum

	.enum	tile_type_town
		grass
		sand
		water
		treasure
		rocks
		stairs_up
		brick
		stairs_down
		trees
		swamp
		barrier
		door
		shop
		inn
		bridge
		misc
	.endenum

	.enum	tile_type_sub
		grass
		sand
		water
		treasure
		rocks
		stairs_up
		brick
		stairs_down
		grass_2
		sand_2
		water_2
		treasure_2
		rocks_2
		stairs_up_2
		brick_2
		stairs_down_2
	.endenum

	.enum	tile_type_dungeon
		rocks
		stairs_up
		brick
		stairs_down
		treasure
		door
		lora
		blank
		rocks_2
		stairs_up_2
		brick_2
		stairs_down_2
		treasure_2
		door_2
		lora_2
		blank_2
	.endenum

	.import map_chest_x_pos: zeropage
	.import map_chest_y_pos: zeropage
	.import map_chest_tbl: zeropage
	.import map_chest_tbl_end: zeropage
	.import map_door_x_pos: zeropage
	.import map_door_y_pos: zeropage
	.import map_eng_number: zeropage
	.import	map_door_pos_buffer: zeropage
	.import	map_door_pos_buffer_end: zeropage
	.import map_player_x: zeropage
	.import	map_player_y: zeropage
	.import map_pw_table: zeropage
	.import map_pw_table_end: zeropage

.endif	; MAP_I
