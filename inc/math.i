.ifndef MATH_I
MATH_I = 1

	.import		math_mulw_a: zeropage
	.import		math_mulw_b: zeropage
	.import		math_div_a: zeropage
	.import		math_div_b: zeropage
	.import		math_result: zeropage
	.import		old_rand_word: zeropage
	.import		math_rand_word: zeropage

	.import		math_mulw
	.import		math_divb
	.import		math_divw

	MATH_CHANCE_EQ_1_2	= 1
	MATH_CHANCE_EQ_1_4	= 3
	MATH_CHANCE_EQ_1_8	= 7
	MATH_CHANCE_EQ_1_16	= $F
	MATH_CHANCE_EQ_1_32	= $1F

.endif	; MATH_I