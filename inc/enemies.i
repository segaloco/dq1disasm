.ifndef ENEMIES_I
ENEMIES_I = 1

	.enum	enemy
		slime
		slime_beth
		drakee
		ghost
		magician
		magidrakee
		scorpion
		druin
		poltergeist
		droll
		drakeema
		skeleton
		warlock
		scorpion_iron
		lycan
		wraith
		slime_metal
		hell_ghost
		wolflord
		druinlord

		drollmagi
		chimera
		scorpion_death
		knight_wraith
		golem
		goldman
		knight
		chimera_magi
		knight_dark
		killer_lycan

		dragon_green
		chimera_star
		wizard
		knight_axe
		dragon_blue

		stoneman
		knight_death
		dragon_red
		dragonlord_weak
		dragonlord
	.endenum

.endif	; ENEMIES_I
