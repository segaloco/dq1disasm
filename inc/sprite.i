.ifndef SPRITE_I
SPRITE_I = 1

	.import sprite_npc_tbl: zeropage
	.import sprite_npc_tbl_end: zeropage
	.import sprite_npc_x: zeropage
	.import sprite_npc_y: zeropage
	.import sprite_npc_z: zeropage
	.import sprite_char_x: zeropage
	.import sprite_char_y: zeropage
	.import sprite_x: zeropage
	.import sprite_y: zeropage
	.import sprite_check_flag: zeropage

SPRITE_ENTRY_SIZE	=	3

SPRITE_0D_X	=	sprite_npc_x+($0D*SPRITE_ENTRY_SIZE)
SPRITE_0D_Y	=	sprite_npc_y+($0D*SPRITE_ENTRY_SIZE)
SPRITE_0D_Z	=	sprite_npc_z+($0D*SPRITE_ENTRY_SIZE)

SPRITE_END	= $00

.endif  ; SPRITE_I
