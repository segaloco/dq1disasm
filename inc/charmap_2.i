.ifndef	CHARMAP_2_I
CHARMAP_2_I	= 1

.include	"./tunables.i"

	.charmap	'!', $00

	.charmap	'A', ENG_FONT2_TILE_BASE+$00
	.charmap	'C', ENG_FONT2_TILE_BASE+$01
	.charmap	'D', ENG_FONT2_TILE_BASE+$02
	.charmap	'E', ENG_FONT2_TILE_BASE+$03
	.charmap	'F', ENG_FONT2_TILE_BASE+$04
	.charmap	'G', ENG_FONT2_TILE_BASE+$05
	.charmap	'I', ENG_FONT2_TILE_BASE+$06
	.charmap	'L', ENG_FONT2_TILE_BASE+$07
	.charmap	'M', ENG_FONT2_TILE_BASE+$08
	.charmap	'N', ENG_FONT2_TILE_BASE+$09
	.charmap	'O', ENG_FONT2_TILE_BASE+$0A
	.charmap	'P', ENG_FONT2_TILE_BASE+$0B
	.charmap	'R', ENG_FONT2_TILE_BASE+$0C
	.charmap	'S', ENG_FONT2_TILE_BASE+$0D
	.charmap	'T', ENG_FONT2_TILE_BASE+$0E
	.charmap	'U', ENG_FONT2_TILE_BASE+$0F
	.charmap	'W', ENG_FONT2_TILE_BASE+$10
	.charmap	'X', ENG_FONT2_TILE_BASE+$11
	.charmap	'Y', ENG_FONT2_TILE_BASE+$12
	.charmap	'c', ENG_FONT2_TILE_BASE+$13
	.charmap	'1', ENG_FONT2_TILE_BASE+$14
	.charmap	'9', ENG_FONT2_TILE_BASE+$15
	.charmap	'8', ENG_FONT2_TILE_BASE+$16
	.charmap	'6', ENG_FONT2_TILE_BASE+$17
	.charmap	'-', ENG_FONT2_TILE_BASE+$18
	.charmap	'>', ENG_FONT2_TILE_BASE+$19
	.charmap	'B', ENG_FONT2_TILE_BASE+$1A
	.charmap	'H', ENG_FONT2_TILE_BASE+$1B
	.charmap	'J', ENG_FONT2_TILE_BASE+$1C
	.charmap	'K', ENG_FONT2_TILE_BASE+$1D
	.charmap	'Q', ENG_FONT2_TILE_BASE+$1E
	.charmap	'V', ENG_FONT2_TILE_BASE+$1F
	.charmap	'Z', ENG_FONT2_TILE_BASE+$20
	.charmap	'2', ENG_FONT2_TILE_BASE+$21
	.charmap	'3', ENG_FONT2_TILE_BASE+$22
	.charmap	'4', ENG_FONT2_TILE_BASE+$23
	.charmap	'5', ENG_FONT2_TILE_BASE+$24
	.charmap	'7', ENG_FONT2_TILE_BASE+$25
	.charmap	'0', ENG_FONT2_TILE_BASE+$26

CHAR2_CHUN_C	= ENG_FONT2_TILE_BASE+$27
CHAR2_CHUN_H	= ENG_FONT2_TILE_BASE+$28
CHAR2_CHUN_U	= ENG_FONT2_TILE_BASE+$29
CHAR2_CHUN_N	= ENG_FONT2_TILE_BASE+$2A

CHAR2_THE_END	= ENG_FONT2_TILE_BASE+$2B

	.charmap	' ', $5F

.endif