.ifndef WINDOW_I
WINDOW_I = 1

	.import	window_block_col: zeropage
	.import window_row: zeropage
	.import window_block_row: zeropage
	.import window_block_count: zeropage
	.import window_block_addr: zeropage
	.import window_map_buffer: zeropage
	.import window_start_pos: zeropage
	.import window_block_width: zeropage
	.import window_text: zeropage
	.import window_desc: zeropage
	.import window_itemname: zeropage
	.import window_dialog_str_tbl: zeropage
	.import window_str_id: zeropage
	.import window_str_id_ptr: zeropage

tbl_windows_MIRROR	=	0
tbl_windows_TEXT	=	1
tbl_windows_XPOS	=	3
tbl_windows_YPOS	=	4
tbl_windows_WIDTH	=	5
tbl_windows_HEIGHT	=	6
tbl_windows_HEIGHT_SIZE = 2

FUNC_WINDOW_DEF_SIZE	= 7
FUNC_WINDOW_NORMAL	= $00
FUNC_WINDOW_MIRROR	= $FF

WINDOW_H_OFFSET	= $100

WINDOW_SCREEN_WIDTH = 32

.macro	window_mid_m	window_id_l
	lda	#<(window_id_l)
	jsr	window_string_low_a
.endmacro

.macro	window_high_m	window_id_h
	jsr	window_string_high_s
	.byte	<(window_id_h-WINDOW_H_OFFSET)
.endmacro

.macro	window_low_m	window_id_l
	jsr	window_string_low_s
	.byte	<(window_id_l)
.endmacro

.macro	window_function_m	window_id
	jsr	window_function
	.byte	<(window_id)
.endmacro

.macro	window_function_clear_m	window_id
	lda	#<(window_id)
	jsr	window_remove
.endmacro

.endif	; WINDOW_I
