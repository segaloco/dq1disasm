.ifndef PPU_PAL_I
PPU_PAL_I = 1

	.import		ppu_pal_load_src: zeropage
	
	.import		ppu_pal_fade_has_bg: zeropage
	.import		ppu_pal_fade_obj: zeropage
	.import		ppu_pal_fade_bg: zeropage
	
	.import		ppu_pal_load_cutoff: zeropage

	.import		ppu_pal_load_obj
	.import		ppu_pal_load_bg

PPU_PAL_BG_FALSE	=	0
PPU_PAL_BG_TRUE		=	$FF

.endif  ; PPU_PAL_I
