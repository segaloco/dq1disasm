.ifndef RPG_I
RPG_I = 1

	.import		rpg_name: zeropage
	.import		rpg_name_end: zeropage
	.import		rpg_started: zeropage
	.import		rpg_exp: zeropage
	.import		rpg_gold: zeropage
	.import		rpg_equip: zeropage
	.import		rpg_keys: zeropage
	.import		rpg_herbs: zeropage
	.import		rpg_inv: zeropage
	.import		rpg_hp: zeropage
	.import		rpg_mp: zeropage
	.import		rpg_level: zeropage
	.import		rpg_str: zeropage
	.import		rpg_agi: zeropage
	.import		rpg_maxhp: zeropage
	.import		rpg_maxmp: zeropage
	.import		rpg_att: zeropage
	.import		rpg_def: zeropage
	.import		rpg_flags: zeropage
	.import		rpg_glowdia: zeropage
	.import		rpg_ppulayout: zeropage

	.enum	item
		key
		torch
		fairy_water
		chimera_wing
		dragon_scale
		fairy_flute
		ring_fighter
		token_loto
		token_lora
		curse_belt
		harp
		curse_necklace
		stones
		staff
		drop
	.endenum

.endif  ; RPG_I
