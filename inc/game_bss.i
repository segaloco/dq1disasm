.ifndef	GAME_BSS_I
GAME_BSS_I = 1

	.import game_glow_timer: zeropage
	.import game_protection_timer: zeropage
	.import game_player_flags: zeropage
	.import	battle_enemy: zeropage
	.import game_story_flags: zeropage
	.import window_char_y: zeropage
	.import window_char_x: zeropage
	.import window_choice: zeropage
	.import window_column: zeropage
	.import window_text_x: zeropage
	.import window_text_y: zeropage
	.import msg_speed: zeropage
	.import window_string_len: zeropage
	.import game_win_ppuaddr: zeropage
	.import tile_current: zeropage
	.import	battle_enemy_hp: zeropage
	.import magic_armor_buff: zeropage
	.import dialog_item_idx: zeropage

.endif	; GAME_BSS_I
