.ifndef FLAGS_I
FLAGS_I = 1
	.enum	state_flags
		heal = 1
		sizz = 2
		snooze = 4
		glow = 8
		fizzle = 16
		evac = 32
		zoom = 64
		protection = 128
		midheal = 256
		sizzle = 512
		dragonlord_open = 1024
		bridge_open = 2048
		equipped_scale = 4096
		equipped_ring = 8192
		equipped_cursed_belt = 16384
		equipped_cursed_necklace = 32768
	.endenum

	.enum	player_flags
		lora_carry = 1
		lora_saved = 2
		own_cursed_necklace = 4
		game_started = 8
		fizzle_player = 16
		fizzle_enemy = 32
		snooze_enemy = 64
		snooze_player = 128
	.endenum

	.enum	story_flags
		defeated_golem = 2
		defeated_dragonlord = 4
		game_started = 8
		defeated_greendragon = 64
	.endenum

	.enum	map_types
		overworld	= $00
		town		= $10
		dungeon		= $20
	.endenum
.endif ; FLAGS_I