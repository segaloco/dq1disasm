.include	"charmap.i"

.include	"./tbl_windows.i"

.segment	"RODATA"

	.export func_window_dialog
func_window_dialog:
dialog_null:	a_dq_charmap_str("╟                  ╢\z")
dialog_footer:	a_dq_charmap_str("╚╧╧╧╧╧╧╧╧╧╧╧╧╧╧╧╧╧╧╝\z")
dialog_header:	a_dq_charmap_str("╔══════════════════╗\z")
dialog_body:	a_dq_charmap_str("╟                  ╢\z")
		a_dq_charmap_str("╟                  ╢\z")
		a_dq_charmap_str("╟                  ╢\z")
		a_dq_charmap_str("╟                  ╢\z")
