.include	"charmap.i"

.include	"./tbl_windows.i"

.segment	"RODATA"

	.export func_window_shop
func_window_shop:
shop_null:	a_dq_charmap_str("╟              ╢\z")
shop_footer:	a_dq_charmap_str("╚╧╧╧╧╧╧╧╧╧╧╧╧╧╧╝\z")
shop_header:	a_dq_charmap_str("╔══╤はなす════════╗\z")

shop_body:
	a_dq_charmap_str("╟ ")
	.byte	CHAR_STR_WEAPON_1
	.byte	CHAR_CTRL_RLE, ((SHOP_WIDTH-9)-1)-2, " "
	.byte	CHAR_CTRL_PRINT5
	.addr	cost_bamboo_pole
	a_dq_charmap_str("╢\z")

	a_dq_charmap_str("╟ ")
	.byte	CHAR_STR_WEAPON_2
	.byte	CHAR_CTRL_RLE, ((SHOP_WIDTH-9)-1)-2, " "
	.byte	CHAR_CTRL_PRINT5
	.addr	cost_club
	a_dq_charmap_str("╢\z")

	a_dq_charmap_str("╟ ")
	.byte	CHAR_STR_WEAPON_3
	.byte	"  "
	.byte	CHAR_CTRL_PRINT5
	.addr	cost_sword_copper
	a_dq_charmap_str("╢\z")

	a_dq_charmap_str("╟ ")
	.byte	CHAR_STR_WEAPON_4
	.byte	"   "
	.byte	CHAR_CTRL_PRINT5
	.addr	cost_axe_iron
	a_dq_charmap_str("╢\z")

	a_dq_charmap_str("╟ ")
	.byte	CHAR_STR_WEAPON_5
	.byte	" "
	.byte	CHAR_CTRL_PRINT5
	.addr	cost_sword_steel
	a_dq_charmap_str("╢\z")

	a_dq_charmap_str("╟ ")
	.byte	CHAR_STR_WEAPON_6
	.byte	" "
	.byte	CHAR_CTRL_PRINT5
	.addr	cost_sword_flame
	a_dq_charmap_str("╢\z")

	a_dq_charmap_str("\z")

	a_dq_charmap_str("╟ ")
	.byte	CHAR_STR_ARMOR_1
	.byte	"   "
	.byte	CHAR_CTRL_PRINT5
	.addr	cost_clothes_cloth
	a_dq_charmap_str("╢\z")

	a_dq_charmap_str("╟ ")
	.byte	CHAR_STR_ARMOR_2
	.byte	"   "
	.byte	CHAR_CTRL_PRINT5
	.addr	cost_clothes_leather
	a_dq_charmap_str("╢\z")

	a_dq_charmap_str("╟ ")
	.byte	CHAR_STR_ARMOR_3
	.byte	" "
	.byte	CHAR_CTRL_PRINT5
	.addr	cost_chain_mail
	a_dq_charmap_str("╢\z")

	a_dq_charmap_str("╟ ")
	.byte	CHAR_STR_ARMOR_4
	.byte	"  "
	.byte	CHAR_CTRL_PRINT5
	.addr	cost_armor_iron
	a_dq_charmap_str("╢\z")

	a_dq_charmap_str("╟ ")
	.byte	CHAR_STR_ARMOR_5
	.byte	" "
	.byte	CHAR_CTRL_PRINT5
	.addr	cost_armor_steel
	a_dq_charmap_str("╢\z")

	a_dq_charmap_str("╟ ")
	.byte	CHAR_STR_ARMOR_6
	.byte	" "
	.byte	CHAR_CTRL_PRINT5
	.addr	cost_armor_magic
	a_dq_charmap_str("╢\z")

	a_dq_charmap_str("\z")

	a_dq_charmap_str("╟ ")
	.byte	CHAR_STR_SHIELD_1
	.byte	"   "
	.byte	CHAR_CTRL_PRINT5
	.addr	cost_shield_leather
	a_dq_charmap_str("╢\z")

	a_dq_charmap_str("╟ ")
	.byte	CHAR_STR_SHIELD_2
	.byte	"   "
	.byte	CHAR_CTRL_PRINT5
	.addr	cost_shield_iron
	a_dq_charmap_str("╢\z")

	a_dq_charmap_str("╟ ")
	.byte	CHAR_STR_SHIELD_3
	.byte	" "
	.byte	CHAR_CTRL_PRINT5
	.addr	cost_shield_mirror
	a_dq_charmap_str("╢\z")

	a_dq_charmap_str("╟ ")
	.byte	CHAR_STR_TOOL_01
	.byte	CHAR_CTRL_RLE, ((SHOP_WIDTH-9)-1)-2, " "
	.byte	CHAR_CTRL_PRINT5
	.addr	cost_herb
	a_dq_charmap_str("╢\z")

	a_dq_charmap_str("\z")

	a_dq_charmap_str("╟ ")
	.byte	CHAR_STR_TOOL_03
	.byte	CHAR_CTRL_RLE, ((SHOP_WIDTH-9)-1)-2, " "
	.byte	CHAR_CTRL_PRINT5
	.addr	cost_torch
	a_dq_charmap_str("╢\z")

	a_dq_charmap_str("\z")

	a_dq_charmap_str("╟ ")
	.byte	CHAR_STR_TOOL_05
	.byte	" "
	.byte	CHAR_CTRL_PRINT5
	.addr	cost_chimera_wing
	a_dq_charmap_str("╢\z")

	a_dq_charmap_str("╟ ")
	.byte	CHAR_STR_TOOL_06
	.byte	" "
	.byte	CHAR_CTRL_PRINT5
	.addr	cost_dragon_scale
	a_dq_charmap_str("╢\z")

	a_dq_charmap_str("\z")
