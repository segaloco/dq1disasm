.include	"charmap.i"

.include	"./tbl_windows.i"

.segment	"RODATA"

	.export func_window_command_field
func_window_command_field:
cmd_field_null:		a_dq_charmap_str("╟            ╢\z")
cmd_field_footer:	a_dq_charmap_str("╚╧╧╧╧╧╧╧╧╧╧╧╧╝\z")
cmd_field_header:	a_dq_charmap_str("╔═══╤コマンド════╗\z")
cmd_field_body:		a_dq_charmap_str("╟ はなす   じゅもん ╢\z")
			a_dq_charmap_str("╟ つよさ   どうぐ  ╢\z")
			a_dq_charmap_str("╟ かいだん  しらべる ╢\z")
			a_dq_charmap_str("╟ とびら   とる   ╢\z")
