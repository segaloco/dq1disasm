.include	"charmap.i"

.include	"./tbl_windows.i"

.segment	"RODATA"

	.export func_window_command_battle
func_window_command_battle:
cmd_battle_null:	a_dq_charmap_str("╟            ╢\z")
cmd_battle_footer:	a_dq_charmap_str("╚╧╧╧╧╧╧╧╧╧╧╧╧╝\z")
cmd_battle_header:	a_dq_charmap_str("╔═══╤コマンド════╗\z")
cmd_battle_body:	a_dq_charmap_str("╟ たたかう  じゅもん ╢\z")
			a_dq_charmap_str("╟ にげる   どうぐ  ╢\z")
			a_dq_charmap_str("\r")
