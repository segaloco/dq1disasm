.include	"charmap.i"

.include	"./tbl_windows.i"

.segment	"RODATA"

	.export func_window_buysell
func_window_buysell:
buysell_null:		a_dq_charmap_str("╟          ╢\z")
buysell_footer:		a_dq_charmap_str("╚╧╧╧╧╧╧╧╧╧╧╝\z")
buysell_header:		a_dq_charmap_str("╔══╤はなす════╗\z")
buysell_body:		a_dq_charmap_str("╟ ものをかいにきた ╢\z")
			a_dq_charmap_str("╟ ものをうりにきた ╢\z")
