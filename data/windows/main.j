.include	"charmap.i"
.include	"rpg.i"

.include	"./tbl_windows.i"

.segment	"RODATA"

	.export func_window_main
func_window_main:
main_null:	a_dq_charmap_str("╟      ╢\z")
main_footer:	a_dq_charmap_str("╚╧╧╧╧╧╧╝\z")

main_header:
	a_dq_charmap_str("╔╤")
	.byte	CHAR_CTRL_PRINTNAME
	a_dq_charmap_str("═╗\z")

main_body:
	a_dq_charmap_str("╟レベル")
	.byte	CHAR_CTRL_PRINT3
	.addr	rpg_level
	a_dq_charmap_str("╢\z")

	a_dq_charmap_str("╟HP ")
	.byte	CHAR_CTRL_PRINT3
	.addr	rpg_hp
	a_dq_charmap_str("╢\z")

	a_dq_charmap_str("╟MP ")
	.byte	CHAR_CTRL_PRINT3
	.addr	rpg_mp
	a_dq_charmap_str("╢\z")

	a_dq_charmap_str("╟G")
	.byte	CHAR_CTRL_PRINT5
	.addr	rpg_gold
	a_dq_charmap_str("╢\z")

	a_dq_charmap_str("╟E")
	.byte	CHAR_CTRL_PRINT5
	.addr	rpg_exp
	a_dq_charmap_str("╢\z")
