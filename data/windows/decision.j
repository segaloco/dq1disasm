.include	"charmap.i"

.include	"./tbl_windows.i"

.segment	"RODATA"

	.export func_window_decision
func_window_decision:
decision_null:		a_dq_charmap_str("╟        ╢\z")
decision_footer:	a_dq_charmap_str("╚╧╧╧╧╧╧╧╧╝\z")
decision_header:	a_dq_charmap_str("╔══╤はなす══╗\z")
decision_body:		a_dq_charmap_str("╟  はい    ╢\z")
			a_dq_charmap_str("╟  いいえ   ╢\z")
