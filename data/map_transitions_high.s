.include	"bssmap.i"
.include	"map.i"

.segment	"CHR3CODE"

	.export dialog_chr_high_addr
dialog_chr_high_addr:
.org	PPU_NMI_BUFFER
	.byte   maps::map_09, $00, $0E
        .byte   maps::map_0D, $04, $09
        .byte   maps::map_07, $13, $17
        .byte   maps::map_08, $00, $0F
        .byte   maps::castle_courtyard, $0B, $1D
        .byte   maps::dungeon_greendragon, $00, $00
        .byte   maps::map_02, $0A, $13
        .byte   maps::dungeon_greendragon, $00, $1D
        .byte   maps::map_16, $00, $07
        .byte   maps::map_0B, $1D, $0E
        .byte   maps::map_03, $00, $0A
        .byte   maps::map_0A, $0F, $00
        .byte   maps::map_0E, $00, $04
        .byte   maps::map_1C, $00, $00
        .byte   maps::map_0F, $09, $00
        .byte   maps::map_0F, $08, $0D
        .byte   maps::map_0F, $11, $0F
        .byte   maps::map_0C, $00, $04
        .byte   maps::castle_courtyard, $07, $07
        .byte   maps::map_18, $06, $0B
        .byte   maps::map_10, $08, $00
        .byte   maps::map_10, $04, $04
        .byte   maps::map_10, $09, $08
        .byte   maps::map_10, $08, $09
        .byte   maps::map_10, $00, $01
        .byte   maps::map_10, $00, $00
        .byte   maps::map_10, $05, $00
        .byte   maps::map_11, $07, $00
        .byte   maps::map_11, $02, $02
        .byte   maps::map_11, $05, $04
        .byte   maps::map_11, $00, $09
        .byte   maps::map_12, $00, $09
        .byte   maps::map_12, $07, $07
        .byte   maps::map_13, $09, $00
        .byte   maps::map_13, $04, $00
        .byte   maps::map_14, $00, $00
        .byte   maps::map_14, $00, $06
        .byte   maps::map_14, $00, $00
        .byte   maps::map_06, $0A, $1D
        .byte   maps::map_17, $00, $00
        .byte   maps::map_17, $06, $05
        .byte   maps::map_17, $0C, $0C
        .byte   maps::map_19, $0B, $02
        .byte   maps::map_1A, $0E, $01
        .byte   maps::map_1A, $12, $01
        .byte   maps::map_1A, $06, $0B
        .byte   maps::map_1A, $02, $11
        .byte   maps::map_1A, $12, $0D
        .byte   maps::map_1B, $00, $04
        .byte   maps::map_1B, $05, $04
        .byte   maps::map_1D, $08, $09
