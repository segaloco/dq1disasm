.include	"bssmap.i"
.include	"map.i"

.segment	"CHR3CODE"

	.export map_transitions_low
map_transitions_low:
.org	PPU_NMI_BUFFER
	.byte   maps::overworld, $02, $02
        .byte   maps::overworld, $51, $01
        .byte   maps::overworld, $68, $0A
        .byte   maps::overworld, $30, $29
        .byte   maps::overworld, $2B, $2B
        .byte   maps::overworld, $68, $2C
        .byte   maps::overworld, $30, $30
        .byte   maps::overworld, $68, $31
        .byte   maps::overworld, $1D, $39
        .byte   maps::overworld, $66, $48
        .byte   maps::overworld, $19, $59
        .byte   maps::overworld, $49, $66
        .byte   maps::overworld, $6C, $6D
        .byte   maps::overworld, $1C, $0C
        .byte   maps::map_02, $0A, $01
        .byte   maps::map_02, $04, $0E
        .byte   maps::map_02, $0F, $0E
        .byte   maps::castle_courtyard, $1D, $1D
        .byte   maps::castle_throneroom, $08, $08
        .byte   maps::map_09, $13, $00
        .byte   maps::map_0F, $0F, $01
        .byte   maps::map_0F, $0D, $07
        .byte   maps::map_0F, $13, $07
        .byte   maps::map_0F, $0E, $09
        .byte   maps::map_0F, $02, $0E
        .byte   maps::map_0F, $02, $04
        .byte   maps::map_0F, $08, $13
        .byte   maps::map_10, $03, $00
        .byte   maps::map_10, $09, $01
        .byte   maps::map_10, $00, $08
        .byte   maps::map_10, $01, $09
        .byte   maps::map_11, $01, $06
        .byte   maps::map_11, $07, $07
        .byte   maps::map_12, $02, $02
        .byte   maps::map_12, $08, $01
        .byte   maps::map_13, $05, $05
        .byte   maps::map_13, $00, $00
        .byte   maps::map_14, $09, $00
        .byte   maps::map_14, $09, $06
        .byte   maps::map_16, $00, $00
        .byte   maps::map_16, $06, $05
        .byte   maps::map_16, $0C, $0C
        .byte   maps::map_18, $01, $12
        .byte   maps::map_19, $01, $01
        .byte   maps::map_19, $0C, $01
        .byte   maps::map_19, $05, $06
        .byte   maps::map_19, $01, $0A
        .byte   maps::map_19, $0C, $0A
        .byte   maps::map_1A, $09, $05
        .byte   maps::map_1A, $0A, $09
        .byte   maps::map_1C, $09, $09
