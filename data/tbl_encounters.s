.include	"enemies.i"
.include	"tunables.i"

.segment	"RODATA"

	.export tbl_protection
tbl_protection:
	.byte	$05, $07, $09, $0B
	.byte	$0B, $0E, $12, $14
	.byte	$12, $18, $16, $1C
	.byte	$1C, $24, $28, $2C
	.byte	$0A, $28, $32, $2F
	.byte	$34, $38, $3C, $44
	.byte	$78, $30, $4C, $4E
	.byte	$4F, $56, $58, $56
	.byte	$50, $5E, $62, $64
	.byte	$69, $78, $5A, $8C

	.export tbl_encounters
tbl_encounters:
	.byte	(((enemygroup_03-tbl_enemygroups)/ENEMYGROUP_SIZE)<<4)|(((enemygroup_03-tbl_enemygroups)/ENEMYGROUP_SIZE))
	.byte	(((enemygroup_02-tbl_enemygroups)/ENEMYGROUP_SIZE)<<4)|(((enemygroup_02-tbl_enemygroups)/ENEMYGROUP_SIZE))
	.byte	(((enemygroup_03-tbl_enemygroups)/ENEMYGROUP_SIZE)<<4)|(((enemygroup_05-tbl_enemygroups)/ENEMYGROUP_SIZE))
	.byte	(((enemygroup_04-tbl_enemygroups)/ENEMYGROUP_SIZE)<<4)|(((enemygroup_05-tbl_enemygroups)/ENEMYGROUP_SIZE))
	.byte	(((enemygroup_03-tbl_enemygroups)/ENEMYGROUP_SIZE)<<4)|(((enemygroup_02-tbl_enemygroups)/ENEMYGROUP_SIZE))
	.byte	(((enemygroup_01-tbl_enemygroups)/ENEMYGROUP_SIZE)<<4)|(((enemygroup_02-tbl_enemygroups)/ENEMYGROUP_SIZE))
	.byte	(((enemygroup_03-tbl_enemygroups)/ENEMYGROUP_SIZE)<<4)|(((enemygroup_03-tbl_enemygroups)/ENEMYGROUP_SIZE))
	.byte	(((enemygroup_04-tbl_enemygroups)/ENEMYGROUP_SIZE)<<4)|(((enemygroup_05-tbl_enemygroups)/ENEMYGROUP_SIZE))
	.byte	(((enemygroup_04-tbl_enemygroups)/ENEMYGROUP_SIZE)<<4)|(((enemygroup_01-tbl_enemygroups)/ENEMYGROUP_SIZE))
	.byte	(((enemygroup_00-tbl_enemygroups)/ENEMYGROUP_SIZE)<<4)|(((enemygroup_00-tbl_enemygroups)/ENEMYGROUP_SIZE))
	.byte	(((enemygroup_02-tbl_enemygroups)/ENEMYGROUP_SIZE)<<4)|(((enemygroup_03-tbl_enemygroups)/ENEMYGROUP_SIZE))
	.byte	(((enemygroup_04-tbl_enemygroups)/ENEMYGROUP_SIZE)<<4)|(((enemygroup_05-tbl_enemygroups)/ENEMYGROUP_SIZE))
	.byte	(((enemygroup_05-tbl_enemygroups)/ENEMYGROUP_SIZE)<<4)|(((enemygroup_01-tbl_enemygroups)/ENEMYGROUP_SIZE))
	.byte	(((enemygroup_01-tbl_enemygroups)/ENEMYGROUP_SIZE)<<4)|(((enemygroup_12-tbl_enemygroups)/ENEMYGROUP_SIZE))
	.byte	(((enemygroup_06-tbl_enemygroups)/ENEMYGROUP_SIZE)<<4)|(((enemygroup_06-tbl_enemygroups)/ENEMYGROUP_SIZE))
	.byte	(((enemygroup_06-tbl_enemygroups)/ENEMYGROUP_SIZE)<<4)|(((enemygroup_06-tbl_enemygroups)/ENEMYGROUP_SIZE))
	.byte	(((enemygroup_05-tbl_enemygroups)/ENEMYGROUP_SIZE)<<4)|(((enemygroup_05-tbl_enemygroups)/ENEMYGROUP_SIZE))
	.byte	(((enemygroup_04-tbl_enemygroups)/ENEMYGROUP_SIZE)<<4)|(((enemygroup_12-tbl_enemygroups)/ENEMYGROUP_SIZE))
	.byte	(((enemygroup_09-tbl_enemygroups)/ENEMYGROUP_SIZE)<<4)|(((enemygroup_07-tbl_enemygroups)/ENEMYGROUP_SIZE))
	.byte	(((enemygroup_07-tbl_enemygroups)/ENEMYGROUP_SIZE)<<4)|(((enemygroup_07-tbl_enemygroups)/ENEMYGROUP_SIZE))
	.byte	(((enemygroup_10-tbl_enemygroups)/ENEMYGROUP_SIZE)<<4)|(((enemygroup_09-tbl_enemygroups)/ENEMYGROUP_SIZE))
	.byte	(((enemygroup_08-tbl_enemygroups)/ENEMYGROUP_SIZE)<<4)|(((enemygroup_12-tbl_enemygroups)/ENEMYGROUP_SIZE))
	.byte	(((enemygroup_12-tbl_enemygroups)/ENEMYGROUP_SIZE)<<4)|(((enemygroup_12-tbl_enemygroups)/ENEMYGROUP_SIZE))
	.byte	(((enemygroup_08-tbl_enemygroups)/ENEMYGROUP_SIZE)<<4)|(((enemygroup_07-tbl_enemygroups)/ENEMYGROUP_SIZE))
	.byte	(((enemygroup_10-tbl_enemygroups)/ENEMYGROUP_SIZE)<<4)|(((enemygroup_10-tbl_enemygroups)/ENEMYGROUP_SIZE))
	.byte	(((enemygroup_11-tbl_enemygroups)/ENEMYGROUP_SIZE)<<4)|(((enemygroup_12-tbl_enemygroups)/ENEMYGROUP_SIZE))
	.byte	(((enemygroup_13-tbl_enemygroups)/ENEMYGROUP_SIZE)<<4)|(((enemygroup_13-tbl_enemygroups)/ENEMYGROUP_SIZE))
	.byte	(((enemygroup_09-tbl_enemygroups)/ENEMYGROUP_SIZE)<<4)|(((enemygroup_08-tbl_enemygroups)/ENEMYGROUP_SIZE))
	.byte	(((enemygroup_11-tbl_enemygroups)/ENEMYGROUP_SIZE)<<4)|(((enemygroup_11-tbl_enemygroups)/ENEMYGROUP_SIZE))
	.byte	(((enemygroup_12-tbl_enemygroups)/ENEMYGROUP_SIZE)<<4)|(((enemygroup_13-tbl_enemygroups)/ENEMYGROUP_SIZE))
	.byte	(((enemygroup_13-tbl_enemygroups)/ENEMYGROUP_SIZE)<<4)|(((enemygroup_12-tbl_enemygroups)/ENEMYGROUP_SIZE))
	.byte	(((enemygroup_09-tbl_enemygroups)/ENEMYGROUP_SIZE)<<4)|(((enemygroup_09-tbl_enemygroups)/ENEMYGROUP_SIZE))

	.export tbl_caves
tbl_caves:
	.byte	(enemygroup_16-tbl_enemygroups)/ENEMYGROUP_SIZE
	.byte	(enemygroup_17-tbl_enemygroups)/ENEMYGROUP_SIZE
	.byte	(enemygroup_17-tbl_enemygroups)/ENEMYGROUP_SIZE
	.byte	(enemygroup_17-tbl_enemygroups)/ENEMYGROUP_SIZE
	.byte	(enemygroup_18-tbl_enemygroups)/ENEMYGROUP_SIZE
	.byte	(enemygroup_18-tbl_enemygroups)/ENEMYGROUP_SIZE
	.byte	(enemygroup_19-tbl_enemygroups)/ENEMYGROUP_SIZE
	.byte	(enemygroup_19-tbl_enemygroups)/ENEMYGROUP_SIZE
	.byte	(enemygroup_14-tbl_enemygroups)/ENEMYGROUP_SIZE
	.byte	(enemygroup_14-tbl_enemygroups)/ENEMYGROUP_SIZE
	.byte	(enemygroup_07-tbl_enemygroups)/ENEMYGROUP_SIZE
	.byte	(enemygroup_15-tbl_enemygroups)/ENEMYGROUP_SIZE
	.byte	(enemygroup_15-tbl_enemygroups)/ENEMYGROUP_SIZE

	.export tbl_enemygroups
tbl_enemygroups:
enemygroup_00:	.byte	enemy::slime,         enemy::slime_beth,     enemy::slime,         enemy::slime_beth,   enemy::slime
enemygroup_01:	.byte	enemy::slime_beth,    enemy::slime,          enemy::slime_beth,    enemy::drakee,       enemy::slime_beth
enemygroup_02:	.byte	enemy::slime,         enemy::ghost,          enemy::drakee,        enemy::ghost,        enemy::slime_beth
enemygroup_03:	.byte	enemy::slime_beth,    enemy::slime_beth,     enemy::drakee,        enemy::ghost,        enemy::magician
enemygroup_04:	.byte	enemy::ghost,         enemy::magician,       enemy::magidrakee,    enemy::magidrakee,   enemy::scorpion
enemygroup_05:	.byte	enemy::ghost,         enemy::magician,       enemy::magidrakee,    enemy::scorpion,     enemy::skeleton
enemygroup_06:	.byte	enemy::magidrakee,    enemy::scorpion,       enemy::skeleton,      enemy::warlock,      enemy::lycan
enemygroup_07:	.byte	enemy::skeleton,      enemy::warlock,        enemy::scorpion_iron, enemy::lycan,        enemy::lycan
enemygroup_08:	.byte	enemy::scorpion_iron, enemy::wraith,         enemy::wolflord,      enemy::wolflord,     enemy::goldman
enemygroup_09:	.byte	enemy::wraith,        enemy::chimera,        enemy::wolflord,      enemy::chimera,      enemy::goldman
enemygroup_10:	.byte	enemy::chimera,       enemy::scorpion_death, enemy::knight_wraith, enemy::knight,       enemy::knight_dark
enemygroup_11:	.byte	enemy::knight_wraith, enemy::knight,         enemy::chimera_magi,  enemy::knight_dark,  enemy::slime_metal
enemygroup_12:	.byte	enemy::knight,        enemy::chimera_magi,   enemy::knight_dark,   enemy::killer_lycan, enemy::chimera_star
enemygroup_13:	.byte	enemy::killer_lycan,  enemy::dragon_green,   enemy::chimera_star,  enemy::chimera_star, enemy::wizard

enemygroup_14:	.byte	enemy::poltergeist,   enemy::droll,          enemy::drakeema,      enemy::skeleton,     enemy::warlock
enemygroup_15:	.byte	enemy::hell_ghost,    enemy::wolflord,       enemy::druinlord,     enemy::drollmagi,    enemy::knight_wraith
enemygroup_16:	.byte	enemy::killer_lycan,  enemy::dragon_green,   enemy::chimera_star,  enemy::wizard,       enemy::knight_axe
enemygroup_17:	.byte	enemy::wizard,        enemy::knight_axe,     enemy::dragon_blue,   enemy::dragon_blue,  enemy::stoneman
enemygroup_18:	.byte	enemy::wizard,        enemy::stoneman,       enemy::knight_death,  enemy::knight_death, enemy::dragon_red
enemygroup_19:	.byte	enemy::ghost,         enemy::magician,       enemy::scorpion,      enemy::druin,        enemy::druin

	.end
