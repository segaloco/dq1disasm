.include	"bssmap.i"
.include	"tunables.i"
.include	"charmap.i"

.segment	"CHR3CODE"

	.export tbl_enemy_props_addr, tbl_enemy_props
tbl_enemy_props_addr:
.org	STACK_BASE
tbl_enemy_props:
	;	att, def, hp,  spell, agi, mdef, exp, gold
	.byte	5,   3,   3,   $00,   15,  1,    1,   2
	:
	a_dq_charmap_str("スライム")
	:
	.res	ENEMY_NAME_LEN-((:-)-(:--)), CHAR_CTRL_STREND
; ----------------------------
	;	att, def, hp,  spell, agi, mdef, exp, gold
	.byte	7,   3,   4,   $00,   15,  1,    1,   3
	:
	a_dq_charmap_str("スライムベス")
	:
	.res	ENEMY_NAME_LEN-((:-)-(:--)), CHAR_CTRL_STREND
; ----------------------------
	;	att, def, hp,  spell, agi, mdef, exp, gold
	.byte	9,   6,   6,   $00,   15,  1,    2,   3
	:
	a_dq_charmap_str("ドラキー")
	:
	.res	ENEMY_NAME_LEN-((:-)-(:--)), CHAR_CTRL_STREND
; ----------------------------
	;	att, def, hp,  spell, agi, mdef, exp, gold
	.byte	11,  8,   7,   $00,   15,  4,    3,   5
	:
	a_dq_charmap_str("ゴースト")
	:
	.res	ENEMY_NAME_LEN-((:-)-(:--)), CHAR_CTRL_STREND
; ----------------------------
	;	att, def, hp,  spell, agi, mdef, exp, gold
	.byte	11,  12,  13,  $02,   0,   1,    4,   12
	:
	a_dq_charmap_str("まほうつかい")
	:
	.res	ENEMY_NAME_LEN-((:-)-(:--)), CHAR_CTRL_STREND
; ----------------------------
	;	att, def, hp,  spell, agi, mdef, exp, gold
	.byte	14,  14,  15,  $02,   0,   1,    5,   12
	:
	a_dq_charmap_str("メイジドラキー")	
	:
	.res	ENEMY_NAME_LEN-((:-)-(:--)), CHAR_CTRL_STREND
; ----------------------------
	;	att, def, hp,  spell, agi, mdef, exp, gold
	.byte	18,  16,  20,  $00,   15,  1,    6,   16
	:
	a_dq_charmap_str("おおさそり")
	:
	.res	ENEMY_NAME_LEN-((:-)-(:--)), CHAR_CTRL_STREND
; ----------------------------
	;	att, def, hp,  spell, agi, mdef, exp, gold
	.byte	20,  18,  22,  $00,   15,  2,    7,   16
	:
	a_dq_charmap_str("メーダ")
	:
	.res	ENEMY_NAME_LEN-((:-)-(:--)), CHAR_CTRL_STREND
; ----------------------------
	;	att, def, hp,  spell, agi, mdef, exp, gold
	.byte	18,  20,  23,  $03,   0,   6,    8,   18
	:
	a_dq_charmap_str("メトロゴースト")
	:
	.res	ENEMY_NAME_LEN-((:-)-(:--)), CHAR_CTRL_STREND
; ----------------------------
	;	att, def, hp,  spell, agi, mdef, exp, gold
	.byte	24,  24,  25,  $00,   14,  2,    10,  25
	:
	a_dq_charmap_str("ドロル")
	:
	.res	ENEMY_NAME_LEN-((:-)-(:--)), CHAR_CTRL_STREND
; ----------------------------
	;	att, def, hp,  spell, agi, mdef, exp, gold
	.byte	22,  26,  20,  $92,   32,  6,    11,  20
	:
	a_dq_charmap_str("ドラキーマ")
	:
	.res	ENEMY_NAME_LEN-((:-)-(:--)), CHAR_CTRL_STREND
; ----------------------------
	;	att, def, hp,  spell, agi, mdef, exp, gold
	.byte	28,  22,  30,  $00,   15,  4,    11,  30
	:
	a_dq_charmap_str("がいこつ")
	:
	.res	ENEMY_NAME_LEN-((:-)-(:--)), CHAR_CTRL_STREND
; ----------------------------
	;	att, def, hp,  spell, agi, mdef, exp, gold
	.byte	28,  22,  30,  $12,   49,  2,    13,  35
	:
	a_dq_charmap_str("まどうし")
	:
	.res	ENEMY_NAME_LEN-((:-)-(:--)), CHAR_CTRL_STREND
; ----------------------------
	;	att, def, hp,  spell, agi, mdef, exp, gold
	.byte	36,  42,  22,  $00,   15,  2,    14,  40
	:
	a_dq_charmap_str("てつのさそり")
	:
	.res	ENEMY_NAME_LEN-((:-)-(:--)), CHAR_CTRL_STREND
; ----------------------------
	;	att, def, hp,  spell, agi, mdef, exp, gold
	.byte	40,  30,  34,  $00,   31,  2,    16,  50
	:
	a_dq_charmap_str("リカント")
	:
	.res	ENEMY_NAME_LEN-((:-)-(:--)), CHAR_CTRL_STREND
; ----------------------------
	;	att, def, hp,  spell, agi, mdef, exp, gold
	.byte	44,  34,  36,  $90,   112, 4,    17,  60
	:
	a_dq_charmap_str("しりょう")
	:
	.res	ENEMY_NAME_LEN-((:-)-(:--)), CHAR_CTRL_STREND
; ----------------------------
	;	att, def, hp,  spell, agi, mdef, exp, gold
	.byte	10,  255, 4,   $03,   255, 241,  115, 6
	:
	a_dq_charmap_str("メタルスライム")
	:
	.res	ENEMY_NAME_LEN-((:-)-(:--)), CHAR_CTRL_STREND
; ----------------------------
	;	att, def, hp,  spell, agi, mdef, exp, gold
	.byte	40,  38,  36,  $13,   49,  4,    18,  70
	:
	a_dq_charmap_str("ヘルゴースト")
	:
	.res	ENEMY_NAME_LEN-((:-)-(:--)), CHAR_CTRL_STREND
; ----------------------------
	;	att, def, hp,  spell, agi, mdef, exp, gold
	.byte	50,  36,  38,  $60,   71,  2,    20,  80
	:
	a_dq_charmap_str("リカントマムル")
	:
	.res	ENEMY_NAME_LEN-((:-)-(:--)), CHAR_CTRL_STREND
; ----------------------------
	;	att, def, hp,  spell, agi, mdef, exp, gold
	.byte	47,  40,  35,  $B1,   240, 4,    20,  85
	:
	a_dq_charmap_str("メーダロード")
	:
	.res	ENEMY_NAME_LEN-((:-)-(:--)), CHAR_CTRL_STREND
; ----------------------------
	;	att, def, hp,  spell, agi, mdef, exp, gold
	.byte	52,  50,  38,  $60,   34,  1,    22,  90
	:
	a_dq_charmap_str("ドロルメイジ")
	:
	.res	ENEMY_NAME_LEN-((:-)-(:--)), CHAR_CTRL_STREND
; ----------------------------
	;	att, def, hp,  spell, agi, mdef, exp, gold
	.byte	56,  48,  42,  $00,   79,  2,    24,  100
	:
	a_dq_charmap_str("キメラ")
	:
	.res	ENEMY_NAME_LEN-((:-)-(:--)), CHAR_CTRL_STREND
; ----------------------------
	;	att, def, hp,  spell, agi, mdef, exp, gold
	.byte	60,  90,  35,  $00,   127, 2,    26,  110
	:
	a_dq_charmap_str("しのさそり")
	:
	.res	ENEMY_NAME_LEN-((:-)-(:--)), CHAR_CTRL_STREND
; ----------------------------
	;	att, def, hp,  spell, agi, mdef, exp, gold
	.byte	68,  56,  46,  $B0,   80,  52,   28,  120
	:
	a_dq_charmap_str("しりょうのきし")
	:
	.res	ENEMY_NAME_LEN-((:-)-(:--)), CHAR_CTRL_STREND
; ----------------------------
	;	att, def, hp,  spell, agi, mdef, exp, gold
	.byte	120, 60,  70,  $00,   255, 240,  5,   10
	:
	a_dq_charmap_str("ゴーレム")
	:
	.res	ENEMY_NAME_LEN-((:-)-(:--)), CHAR_CTRL_STREND
; ----------------------------
	;	att, def, hp,  spell, agi, mdef, exp, gold
	.byte	48,  40,  50,  $00,   223, 1,    6,   200
	:
	a_dq_charmap_str("ゴールドマン")
	:
	.res	ENEMY_NAME_LEN-((:-)-(:--)), CHAR_CTRL_STREND
; ----------------------------
	;	att, def, hp,  spell, agi, mdef, exp, gold
	.byte	76,  78,  55,  $60,   103, 1,    33,  130
	:
	a_dq_charmap_str("よろいのきし")
	:
	.res	ENEMY_NAME_LEN-((:-)-(:--)), CHAR_CTRL_STREND
; ----------------------------
	;	att, def, hp,  spell, agi, mdef, exp, gold
	.byte	78,  68,  58,  $20,   32,  2,    34,  140
	:
	a_dq_charmap_str("メイジキメラ")
	:
	.res	ENEMY_NAME_LEN-((:-)-(:--)), CHAR_CTRL_STREND
; ----------------------------
	;	att, def, hp,  spell, agi, mdef, exp, gold
	.byte	79,  64,  50,  $00,   255, 255,  37,  150
	:
	a_dq_charmap_str("かげのきし")
	:
	.res	ENEMY_NAME_LEN-((:-)-(:--)), CHAR_CTRL_STREND
; ----------------------------
	;	att, def, hp,  spell, agi, mdef, exp, gold
	.byte	86,  70,  60,  $00,   127, 7,    40,  155
	:
	a_dq_charmap_str("キラーリカント")
	:
	.res	ENEMY_NAME_LEN-((:-)-(:--)), CHAR_CTRL_STREND
; ----------------------------
	;	att, def, hp,  spell, agi, mdef, exp, gold
	.byte	88,  74,  65,  $09,   127, 34,   45,  160
	:
	a_dq_charmap_str("ドラゴン")
	:
	.res	ENEMY_NAME_LEN-((:-)-(:--)), CHAR_CTRL_STREND
; ----------------------------
	;	att, def, hp,  spell, agi, mdef, exp, gold
	.byte	86,  80,  65,  $F9,   128, 18,   43,  160
	:
	a_dq_charmap_str("スターキメラ")
	:
	.res	ENEMY_NAME_LEN-((:-)-(:--)), CHAR_CTRL_STREND
; ----------------------------
	;	att, def, hp,  spell, agi, mdef, exp, gold
	.byte	80,  70,  65,  $06,   247, 242,  50,  165
	:
	a_dq_charmap_str("だいまどう")
	:
	.res	ENEMY_NAME_LEN-((:-)-(:--)), CHAR_CTRL_STREND
; ----------------------------
	;	att, def, hp,  spell, agi, mdef, exp, gold
	.byte	94,  82,  70,  $10,   243, 17,   54,  165
	:
	a_dq_charmap_str("あくまのきし")
	:
	.res	ENEMY_NAME_LEN-((:-)-(:--)), CHAR_CTRL_STREND
; ----------------------------
	;	att, def, hp,  spell, agi, mdef, exp, gold
	.byte	98,  84,  70,  $09,   255, 114,  60,  150
	:
	a_dq_charmap_str("キースドラゴン")
	:
	.res	ENEMY_NAME_LEN-((:-)-(:--)), CHAR_CTRL_STREND
; ----------------------------
	;	att, def, hp,  spell, agi, mdef, exp, gold
	.byte	100, 40,  160, $00,   47,  113,  65,  140
	:
	a_dq_charmap_str("ストーンマン")
	:
	.res	ENEMY_NAME_LEN-((:-)-(:--)), CHAR_CTRL_STREND
; ----------------------------
	;	att, def, hp,  spell, agi, mdef, exp, gold
	.byte	105, 86,  90,  $F5,   247, 18,   70,  140
	:
	a_dq_charmap_str("しにがみのきし")
	:
	.res	ENEMY_NAME_LEN-((:-)-(:--)), CHAR_CTRL_STREND
; ----------------------------
	;	att, def, hp,  spell, agi, mdef, exp, gold
	.byte	120, 90,  100, $19,   247, 242,  100, 140
	:
	a_dq_charmap_str("ダースドラゴ")
	:
	.res	ENEMY_NAME_LEN-((:-)-(:--)), CHAR_CTRL_STREND
; ----------------------------
	;	att, def, hp,  spell, agi, mdef, exp, gold
	.byte	90,  75,  100, $57,   255, 240,  0,   0
	:
	a_dq_charmap_str("りゅうおう")
	:
	.res	ENEMY_NAME_LEN-((:-)-(:--)), CHAR_CTRL_STREND
; ----------------------------
	;	att, def, hp,  spell, agi, mdef, exp, gold
	.byte	140, 200, 130, $0E,   255, 240,  0,   0
	:
	a_dq_charmap_str("りゅうおう")
	:
	.res	ENEMY_NAME_LEN-((:-)-(:--)), CHAR_CTRL_STREND
