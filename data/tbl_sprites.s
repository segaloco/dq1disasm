.include	"system/ppu.i"

.segment	"RODATA"

	.export tbl_sprites
tbl_sprites:
	.byte	$95, obj_attribute::color0
	.byte	$95, obj_attribute::h_flip|obj_attribute::color0
	.byte	$96, obj_attribute::color0
	.byte	$97, obj_attribute::color0
	.byte	$95, obj_attribute::color0
	.byte	$95, obj_attribute::h_flip|obj_attribute::color0
	.byte	$97, obj_attribute::h_flip|obj_attribute::color0
	.byte	$96, obj_attribute::h_flip|obj_attribute::color0
	.byte	$04, obj_attribute::color3
	.byte	$00, obj_attribute::h_flip|obj_attribute::color3
	.byte	$05, obj_attribute::color3
	.byte	$03, obj_attribute::color3
	.byte	$06, obj_attribute::color3
	.byte	$00, obj_attribute::h_flip|obj_attribute::color3
	.byte	$07, obj_attribute::color3
	.byte	$01, obj_attribute::h_flip|obj_attribute::color3
	.byte	$92, obj_attribute::color2
	.byte	$93, obj_attribute::color2
	.byte	$01, obj_attribute::color2
	.byte	$94, obj_attribute::color2
	.byte	$92, obj_attribute::color2
	.byte	$93, obj_attribute::color2
	.byte	$03, obj_attribute::h_flip|obj_attribute::color2
	.byte	$94, obj_attribute::color2
	.byte	$0c, obj_attribute::color1
	.byte	$0c, obj_attribute::h_flip|obj_attribute::color1
	.byte	$0d, obj_attribute::color1
	.byte	$0f, obj_attribute::color1
	.byte	$0c, obj_attribute::color1
	.byte	$0c, obj_attribute::h_flip|obj_attribute::color1
	.byte	$0f, obj_attribute::h_flip|obj_attribute::color1
	.byte	$0d, obj_attribute::h_flip|obj_attribute::color1
	.byte	$10, obj_attribute::color3
	.byte	$10, obj_attribute::h_flip|obj_attribute::color3
	.byte	$11, obj_attribute::color3
	.byte	$11, obj_attribute::h_flip|obj_attribute::color3
	.byte	$10, obj_attribute::color3
	.byte	$10, obj_attribute::h_flip|obj_attribute::color3
	.byte	$11, obj_attribute::color3
	.byte	$11, obj_attribute::h_flip|obj_attribute::color3
	.byte	$12, obj_attribute::color0
	.byte	$14, obj_attribute::color0
	.byte	$13, obj_attribute::color0
	.byte	$15, obj_attribute::color0
	.byte	$12, obj_attribute::color0
	.byte	$16, obj_attribute::color0
	.byte	$13, obj_attribute::color0
	.byte	$17, obj_attribute::color0
	.byte	$18, obj_attribute::color3
	.byte	$1a, obj_attribute::color3
	.byte	$19, obj_attribute::color3
	.byte	$1b, obj_attribute::color3
	.byte	$1a, obj_attribute::h_flip|obj_attribute::color3
	.byte	$18, obj_attribute::h_flip|obj_attribute::color3
	.byte	$1b, obj_attribute::h_flip|obj_attribute::color3
	.byte	$19, obj_attribute::h_flip|obj_attribute::color3
	.byte	$92, obj_attribute::color2
	.byte	$92, obj_attribute::h_flip|obj_attribute::color2
	.byte	$03, obj_attribute::h_flip|obj_attribute::color2
	.byte	$01, obj_attribute::h_flip|obj_attribute::color2
	.byte	$1c, obj_attribute::color2
	.byte	$1e, obj_attribute::color1
	.byte	$1d, obj_attribute::color2
	.byte	$02, obj_attribute::color2
	.byte	$00, obj_attribute::color0
	.byte	$00, obj_attribute::h_flip|obj_attribute::color0
	.byte	$01, obj_attribute::color0
	.byte	$03, obj_attribute::color0
	.byte	$00, obj_attribute::color0
	.byte	$00, obj_attribute::h_flip|obj_attribute::color0
	.byte	$03, obj_attribute::h_flip|obj_attribute::color0
	.byte	$01, obj_attribute::h_flip|obj_attribute::color0
	.byte	$04, obj_attribute::color0
	.byte	$00, obj_attribute::h_flip|obj_attribute::color0
	.byte	$05, obj_attribute::color0
	.byte	$03, obj_attribute::color0
	.byte	$06, obj_attribute::color0
	.byte	$00, obj_attribute::h_flip|obj_attribute::color0
	.byte	$07, obj_attribute::color0
	.byte	$01, obj_attribute::h_flip|obj_attribute::color0
	.byte	$00, obj_attribute::color0
	.byte	$08, obj_attribute::color0
	.byte	$01, obj_attribute::color0
	.byte	$09, obj_attribute::color0
	.byte	$00, obj_attribute::color0
	.byte	$0a, obj_attribute::color0
	.byte	$03, obj_attribute::h_flip|obj_attribute::color0
	.byte	$0b, obj_attribute::color0
	.byte	$04, obj_attribute::color0
	.byte	$08, obj_attribute::color0
	.byte	$05, obj_attribute::color0
	.byte	$09, obj_attribute::color0
	.byte	$06, obj_attribute::color0
	.byte	$0a, obj_attribute::color0
	.byte	$07, obj_attribute::color0
	.byte	$0b, obj_attribute::color0
	.byte	$4f, obj_attribute::color3
	.byte	$51, obj_attribute::color0
	.byte	$50, obj_attribute::color3
	.byte	$52, obj_attribute::color0
	.byte	$4f, obj_attribute::color3
	.byte	$51, obj_attribute::color0
	.byte	$50, obj_attribute::color3
	.byte	$1f, obj_attribute::color0
	.byte	$9a, obj_attribute::h_flip|obj_attribute::color3
	.byte	$9a, obj_attribute::color3
	.byte	$9b, obj_attribute::h_flip|obj_attribute::color3
	.byte	$9b, obj_attribute::color3
	.byte	$9a, obj_attribute::h_flip|obj_attribute::color3
	.byte	$9a, obj_attribute::color3
	.byte	$9b, obj_attribute::h_flip|obj_attribute::color3
	.byte	$9b, obj_attribute::color3
	.byte	$98, obj_attribute::color0
	.byte	$98, obj_attribute::h_flip|obj_attribute::color0
	.byte	$99, obj_attribute::color0
	.byte	$99, obj_attribute::h_flip|obj_attribute::color0
	.byte	$98, obj_attribute::color0
	.byte	$98, obj_attribute::h_flip|obj_attribute::color0
	.byte	$99, obj_attribute::color0
	.byte	$99, obj_attribute::h_flip|obj_attribute::color0
	.byte	$92, obj_attribute::color2
	.byte	$92, obj_attribute::h_flip|obj_attribute::color2
	.byte	$01, obj_attribute::color2
	.byte	$03, obj_attribute::color2
	.byte	$1e, obj_attribute::h_flip|obj_attribute::color1
	.byte	$1c, obj_attribute::h_flip|obj_attribute::color2
	.byte	$02, obj_attribute::h_flip|obj_attribute::color2
	.byte	$1d, obj_attribute::h_flip|obj_attribute::color2
