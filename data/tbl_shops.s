.include	"tunables.i"

.segment	"RODATA"

COST_SIZE	= 2

	.export tbl_costs, tbl_costs_equip
	.export	cost_bamboo_pole, cost_club
	.export	cost_sword_copper, cost_axe_iron
	.export	cost_sword_steel, cost_sword_flame
tbl_costs:
tbl_costs_equip:
tbl_costs_weapons:
cost_bamboo_pole:	.word	10
cost_club:		.word	60
cost_sword_copper:	.word	180
cost_axe_iron:		.word	560
cost_sword_steel:	.word	1500
cost_sword_flame:	.word	9800

cost_sword_loto:	.word	2

	.export	cost_clothes_cloth, cost_clothes_leather
	.export	cost_chain_mail, cost_armor_iron
	.export	cost_armor_steel, cost_armor_magic
tbl_costs_armor:
cost_clothes_cloth:	.word	20
cost_clothes_leather:	.word	70
cost_chain_mail:	.word	300
cost_armor_iron:	.word	1000
cost_armor_steel:	.word	3000
cost_armor_magic:	.word	7700

cost_armor_loto:	.word	2

	.export	cost_shield_leather, cost_shield_iron
	.export	cost_shield_mirror
tbl_costs_shields:
cost_shield_leather:	.word	90
cost_shield_iron:	.word	800
cost_shield_mirror:	.word	14800

	.export tbl_costs_tools
	.export	cost_herb, cost_torch
	.export	cost_chimera_wing, cost_dragon_scale
tbl_costs_tools:
cost_herb:		.word	24

cost_key:		.word	53

cost_torch:		.word	8

cost_fairy_water:	.word	38

cost_chimera_wing:	.word	70
cost_dragon_scale:	.word	20

cost_fairy_flute:	.word	0
cost_ring_fighter:	.word	30
cost_token_loto:	.word	0
cost_token_lora:	.word	0
cost_curse_belt:	.word	360
cost_harp:		.word	0
cost_curse_necklace:	.word	2400
cost_stones:		.word	0
cost_staff:		.word	0
cost_drop:		.word	0

	.export	tbl_costs_keys
tbl_costs_keys:
	.byte	98, 53, 85

	.export	tbl_costs_inns
tbl_costs_inns:
	.byte	20, 6, 25, 100, 55

	.export tbl_shopitems
tbl_shopitems:
	.byte	(cost_sword_copper-tbl_costs_weapons)/COST_SIZE
	.byte	(cost_axe_iron-tbl_costs_weapons)/COST_SIZE
	.byte	(cost_armor_iron-tbl_costs_weapons)/COST_SIZE
	.byte	(cost_armor_steel-tbl_costs_weapons)/COST_SIZE
	.byte	(cost_shield_leather-tbl_costs_weapons)/COST_SIZE
	.byte	SHOP_LIST_END

	.byte	(cost_bamboo_pole-tbl_costs_weapons)/COST_SIZE
	.byte	(cost_club-tbl_costs_weapons)/COST_SIZE
	.byte	(cost_sword_copper-tbl_costs_weapons)/COST_SIZE
	.byte	(cost_clothes_cloth-tbl_costs_weapons)/COST_SIZE
	.byte	(cost_clothes_leather-tbl_costs_weapons)/COST_SIZE
	.byte	(cost_shield_leather-tbl_costs_weapons)/COST_SIZE
	.byte	SHOP_LIST_END

	.byte	(cost_club-tbl_costs_weapons)/COST_SIZE
	.byte	(cost_sword_copper-tbl_costs_weapons)/COST_SIZE
	.byte	(cost_axe_iron-tbl_costs_weapons)/COST_SIZE
	.byte	(cost_clothes_leather-tbl_costs_weapons)/COST_SIZE
	.byte	(cost_chain_mail-tbl_costs_weapons)/COST_SIZE
	.byte	(cost_armor_iron-tbl_costs_weapons)/COST_SIZE
	.byte	(cost_shield_iron-tbl_costs_weapons)/COST_SIZE
	.byte	SHOP_LIST_END

	.byte	(cost_bamboo_pole-tbl_costs_weapons)/COST_SIZE
	.byte	(cost_club-tbl_costs_weapons)/COST_SIZE
	.byte	(cost_sword_copper-tbl_costs_weapons)/COST_SIZE
	.byte	(cost_clothes_leather-tbl_costs_weapons)/COST_SIZE
	.byte	(cost_chain_mail-tbl_costs_weapons)/COST_SIZE
	.byte	(cost_shield_iron-tbl_costs_weapons)/COST_SIZE
	.byte	SHOP_LIST_END

	.byte	(cost_axe_iron-tbl_costs_weapons)/COST_SIZE
	.byte	(cost_sword_steel-tbl_costs_weapons)/COST_SIZE
	.byte	(cost_armor_steel-tbl_costs_weapons)/COST_SIZE
	.byte	(cost_armor_magic-tbl_costs_weapons)/COST_SIZE
	.byte	SHOP_LIST_END

	.byte	(cost_sword_flame-tbl_costs_weapons)/COST_SIZE
	.byte	(cost_shield_mirror-tbl_costs_weapons)/COST_SIZE
	.byte	SHOP_LIST_END

	.byte	(cost_sword_copper-tbl_costs_weapons)/COST_SIZE
	.byte	(cost_axe_iron-tbl_costs_weapons)/COST_SIZE
	.byte	(cost_sword_steel-tbl_costs_weapons)/COST_SIZE
	.byte	(cost_armor_iron-tbl_costs_weapons)/COST_SIZE
	.byte	(cost_armor_steel-tbl_costs_weapons)/COST_SIZE
	.byte	(cost_armor_magic-tbl_costs_weapons)/COST_SIZE
	.byte	SHOP_LIST_END

	.byte	(cost_herb-tbl_costs_weapons)/COST_SIZE
	.byte	(cost_torch-tbl_costs_weapons)/COST_SIZE
	.byte	(cost_dragon_scale-tbl_costs_weapons)/COST_SIZE
	.byte	(cost_chimera_wing-tbl_costs_weapons)/COST_SIZE
	.byte	SHOP_LIST_END

	.byte	(cost_herb-tbl_costs_weapons)/COST_SIZE
	.byte	(cost_torch-tbl_costs_weapons)/COST_SIZE
	.byte	(cost_dragon_scale-tbl_costs_weapons)/COST_SIZE
	.byte	SHOP_LIST_END

	.byte	(cost_herb-tbl_costs_weapons)/COST_SIZE
	.byte	(cost_torch-tbl_costs_weapons)/COST_SIZE
	.byte	(cost_dragon_scale-tbl_costs_weapons)/COST_SIZE
	.byte	SHOP_LIST_END

	.byte	(cost_herb-tbl_costs_weapons)/COST_SIZE
	.byte	(cost_torch-tbl_costs_weapons)/COST_SIZE
	.byte	SHOP_LIST_END

	.byte	(cost_dragon_scale-tbl_costs_weapons)/COST_SIZE
	.byte	(cost_chimera_wing-tbl_costs_weapons)/COST_SIZE
	.byte	SHOP_LIST_END

	.export tbl_buf_atk_weapon, tbl_buf_def_armor, tbl_buf_def_shield
tbl_buf_atk_weapon:
	.byte	$00, $02, $04, $0A
	.byte	$0F, $14, $1C, $28

tbl_buf_def_armor:
	.byte	$00, $02, $04, $0A
	.byte	$10, $18, $18, $1C

tbl_buf_def_shield:
	.byte	$00, $04, $0A, $14

	.end
