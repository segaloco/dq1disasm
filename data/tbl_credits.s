.include "system/ppu.i"

.include "bssmap.i"
.include "credits.i"
.include "charmap_2.i"

.segment	"CHR2DATA"
	.export	str_credits_addr, str_credits, str_credits_end
str_credits_addr:
.org	CNROM_BUFFER3
str_credits:
	.addr	PPU_VRAM_BG1+$EC
	.byte	"GREAT !!"
	.byte	CREDITS_ENDLINE

	.addr	PPU_VRAM_BG1+$147
	.byte	"YOU REGAINED PEACE"
	.byte	CREDITS_ENDLINE

	.addr	PPU_VRAM_BG1+$189
	.byte	"TO THE WORLD !"
	.byte	CREDITS_ENDLINE

	.addr	PPU_VRAM_BG1+$1E4
	.byte	"BUT YOU DECIDED TO START"
	.byte	CREDITS_ENDLINE

	.addr	PPU_VRAM_BG1+$227
	.byte	"ON A NEW JOURNEY "
	.byte	$03
	.byte	CREDITS_ENDLINE

	.addr	PPU_VRAM_BG1+$287
	.byte	"MAY GOD BE ALWAYS"
	.byte	CREDITS_ENDLINE

	.addr	PPU_VRAM_BG1+$2CB
	.byte	"WITH YOU !"
	.byte	CREDITS_ENDLINE

	.addr	PPU_VRAM_BG1+PPU_VRAM_BG_VIS+$0B
	.byte	CREDITS_RLE, 2, $55
	.byte	CREDITS_ENDLINE

	.addr	PPU_VRAM_BG1+PPU_VRAM_BG_VIS+$10
	.byte	CREDITS_RLE, 32, $AA
	.byte	CREDITS_ENDPAGE
; ----------------------------
	.addr	PPU_VRAM_BG1+$189
	.byte	"DRAGON QUEST"
	.byte	CREDITS_ENDLINE

	.addr	PPU_VRAM_BG1+$1EC
	.byte	"STAFF"
	.byte	CREDITS_ENDLINE

	.addr	PPU_VRAM_BG1+PPU_VRAM_BG_VIS+$00
	.byte	CREDITS_RLE, 32, $FF
	.byte	CREDITS_ENDPAGE
; ----------------------------
	.addr	PPU_VRAM_BG1+$186
	.byte	"SCENARIO WRITTEN BY"
	.byte	CREDITS_ENDLINE

	.addr	PPU_VRAM_BG1+$1EB
	.byte	"YUJI HORII"
	.byte	CREDITS_ENDLINE

	.addr	PPU_VRAM_BG1+PPU_VRAM_BG_VIS+$00
	.byte	CREDITS_RLE, 32, $05
	.byte	CREDITS_ENDPAGE
; ----------------------------
	.addr	PPU_VRAM_BG1+$185
	.byte	"CHARACTER DESIGNED BY"
	.byte	CREDITS_ENDLINE

	.addr	PPU_VRAM_BG1+$1E9
	.byte	"AKIRA TORIYAMA"
	.byte	CREDITS_ENDLINE

	.addr	PPU_VRAM_BG1+PPU_VRAM_BG_VIS+$00
	.byte	CREDITS_RLE, 32, $0A
	.byte	CREDITS_ENDPAGE
; ----------------------------
	.addr	PPU_VRAM_BG1+$187
	.byte	"MUSIC COMPOSED BY"
	.byte	CREDITS_ENDLINE

	.addr	PPU_VRAM_BG1+$1E8
	.byte	"KOICHI SUGIYAMA"
	.byte	CREDITS_ENDLINE

	.addr	PPU_VRAM_BG1+PPU_VRAM_BG_VIS+$00
	.byte	CREDITS_RLE, 32, $0F
	.byte	CREDITS_ENDPAGE
; ----------------------------
	.addr	PPU_VRAM_BG1+$12A
	.byte	"PROGRAMED BY"
	.byte	CREDITS_ENDLINE

	.addr	PPU_VRAM_BG1+$1A8
	.byte	"KOICHI NAKAMURA"
	.byte	CREDITS_ENDLINE

	.addr	PPU_VRAM_BG1+$20A
	.byte	"KOJI YOSHIDA"
	.byte	CREDITS_ENDLINE

	.addr	PPU_VRAM_BG1+$267
	.byte	"TAKENORI YAMAMORI"
	.byte	CREDITS_ENDLINE

	.addr	PPU_VRAM_BG1+PPU_VRAM_BG_VIS+$10
	.byte	CREDITS_RLE, 8, $05
	.byte	CREDITS_RLE, 16, $00
	.byte	CREDITS_ENDPAGE
; ----------------------------
	.addr	PPU_VRAM_BG1+$189
	.byte	"CG DESIGNED BY"
	.byte	CREDITS_ENDLINE

	.addr	PPU_VRAM_BG1+$1E9
	.byte	"TAKASHI YASUNO"
	.byte	CREDITS_ENDLINE

	.addr	PPU_VRAM_BG1+PPU_VRAM_BG_VIS+$18
	.byte	CREDITS_RLE, 8, $0A
	.byte	CREDITS_ENDPAGE
; ----------------------------
	.addr	PPU_VRAM_BG1+$186
	.byte	"SCENARIO ASSISTED BY"
	.byte	CREDITS_ENDLINE

	.addr	PPU_VRAM_BG1+$1E8
	.byte	"HIROSHI MIYAOKA"
	.byte	CREDITS_ENDLINE

	.addr	PPU_VRAM_BG1+PPU_VRAM_BG_VIS+$00
	.byte	CREDITS_RLE, 32, $0F
	.byte	CREDITS_ENDPAGE
; ----------------------------
	.addr	PPU_VRAM_BG1+$14A
	.byte	"ASSISTED BY"
	.byte	CREDITS_ENDLINE

	.addr	PPU_VRAM_BG1+$1CA
	.byte	"RIKA SUZUKI"
	.byte	CREDITS_ENDLINE

	.addr	PPU_VRAM_BG1+$228
	.byte	"TADASHI FUKUZAWA"
	.byte	CREDITS_ENDLINE

	.addr	PPU_VRAM_BG1+PPU_VRAM_BG_VIS+$10
	.byte	CREDITS_RLE, 8, $50
	.byte	CREDITS_RLE, 16, $00
	.byte	CREDITS_ENDPAGE
; ----------------------------
	.addr	PPU_VRAM_BG1+$127
	.byte	"TITLE DESIGNED BY"
	.byte	CREDITS_ENDLINE

	.addr	PPU_VRAM_BG1+$189
	.byte	"KAZUO ENOMOTO"
	.byte	CREDITS_ENDLINE

	.addr	PPU_VRAM_BG1+$205
	.byte	"MANUAL ILLUSTRATED BY"
	.byte	CREDITS_ENDLINE

	.addr	PPU_VRAM_BG1+$26A
	.byte	"TAKAYUKI DOI"
	.byte	CREDITS_ENDLINE

	.addr	PPU_VRAM_BG1+PPU_VRAM_BG_VIS+$10
	.byte	CREDITS_RLE, 8, $0A
	.byte	CREDITS_RLE, 8, $00
	.byte	CREDITS_RLE, 8, $0F
	.byte	CREDITS_ENDPAGE
; ----------------------------
	.addr	PPU_VRAM_BG1+$187
	.byte	"SPECIAL THANKS TO"
	.byte	CREDITS_ENDLINE

	.addr	PPU_VRAM_BG1+$1E7
	.byte	"KAZUHIKO TORISHIMA"
	.byte	CREDITS_ENDLINE

	.addr	PPU_VRAM_BG1+PPU_VRAM_BG_VIS+$00
	.byte	CREDITS_RLE, 32, $05
	.byte	CREDITS_ENDPAGE
; ----------------------------
	.addr	PPU_VRAM_BG1+$18A
	.byte	"DIRECTED BY"
	.byte	CREDITS_ENDLINE

	.addr	PPU_VRAM_BG1+$1E8
	.byte	"KOICHI NAKAMURA"
	.byte	CREDITS_ENDLINE

	.addr	PPU_VRAM_BG1+PPU_VRAM_BG_VIS+$00
	.byte	CREDITS_RLE, 32, $0A
	.byte	CREDITS_ENDPAGE
; ----------------------------
	.addr	PPU_VRAM_BG1+$18A
	.byte	"PRODUCED BY"
	.byte	CREDITS_ENDLINE

	.addr	PPU_VRAM_BG1+$1E9
	.byte	"YUKINOBU CHIDA"
	.byte	CREDITS_ENDLINE

	.addr	PPU_VRAM_BG1+PPU_VRAM_BG_VIS+$00
	.byte	CREDITS_RLE, 32, $0F
	.byte	CREDITS_ENDPAGE
; ----------------------------
	.addr	PPU_VRAM_BG1+$0CB
	.byte	"COPYRIGHT"
	.byte	CREDITS_ENDLINE

	.addr	PPU_VRAM_BG1+$145
	.byte	"ARMOR PROJECT"
	.byte	CREDITS_ENDLINE

	.addr	PPU_VRAM_BG1+$156
	.byte	"1986"
	.byte	CREDITS_ENDLINE

	.addr	PPU_VRAM_BG1+$1A5
	.byte	"BIRD STUDIO"
	.byte	CREDITS_ENDLINE

	.addr	PPU_VRAM_BG1+$1B6
	.byte	"1986"
	.byte	CREDITS_ENDLINE

	.addr	PPU_VRAM_BG1+$205
	.byte	"KOICHI SUGIYAMA"
	.byte	CREDITS_ENDLINE

	.addr	PPU_VRAM_BG1+$216
	.byte	"1986"
	.byte	CREDITS_ENDLINE

	.addr	PPU_VRAM_BG1+$265
	.byte	CHAR2_CHUN_C, CHAR2_CHUN_H, CHAR2_CHUN_U, CHAR2_CHUN_N, " SOFT"
	.byte	CREDITS_ENDLINE

	.addr	PPU_VRAM_BG1+$276
	.byte	"1986"
	.byte	CREDITS_ENDLINE

	.addr	PPU_VRAM_BG1+$2EB
	.byte	"ENIX"
	.byte	CREDITS_ENDLINE

	.addr	PPU_VRAM_BG1+$2F1
	.byte	"1986"
	.byte	CREDITS_ENDLINE

	.addr	PPU_VRAM_BG1+PPU_VRAM_BG_VIS+$08
	.byte	CREDITS_RLE, 8, $FF
	.byte	CREDITS_RLE, 5, $00
	.byte	CREDITS_RLE, 3, $AA 
	.byte	CREDITS_RLE, 5, $00
	.byte	CREDITS_RLE, 4, $AA
	.byte	CREDITS_ENDLINE

	.addr	PPU_VRAM_BG1+PPU_VRAM_BG_VIS+$21
	.byte	$10
	.byte	CREDITS_RLE, 3, $00
	.byte	CREDITS_RLE, 3, $AA
	.byte	CREDITS_RLE, 4, $00
	.byte	CREDITS_RLE, 4, $AA
	.byte	CREDITS_ENDPAGE
; ----------------------------
	.addr	PPU_VRAM_BG1+$18F
	.byte	$04, $05, $06
	.byte	CREDITS_ENDLINE

	.addr	PPU_VRAM_BG1+$1AE
	.byte	$07, $08, $09, $0A
	.byte	CREDITS_ENDLINE

	.addr	PPU_VRAM_BG1+$1CE
	.byte	$0B, $0C, $60, $61
	.byte	CREDITS_ENDLINE

	.addr	PPU_VRAM_BG1+$1EE
	.byte	$62, $63, $64, $65
	.byte	CREDITS_ENDLINE

	.addr	PPU_VRAM_BG1+$20E
	.byte	$66, $67, $68, $69
	.byte	CREDITS_ENDLINE

	.addr	PPU_VRAM_BG1+PPU_VRAM_BG_VIS+$18
	.byte	CREDITS_RLE, 16, $FF
	.byte	CREDITS_ENDPAGE
; ----------------------------
	.addr	PPU_VRAM_BG1+$1AA
	.byte	$38, $3A, $40, $42, $44, $46, $48, $4A, $4C, $4E, $50, $52
	.byte	CREDITS_ENDLINE

	.addr	PPU_VRAM_BG1+$1CA
	.byte	$39, $3B, $41, $43, $45, $47, $49, $4B, $4D, $4F, $51, $53
	.byte	CREDITS_ENDLINE

	.addr	PPU_VRAM_BG1+PPU_VRAM_BG_VIS+$10
	.byte	CREDITS_RLE, 32, $00
	.byte	CREDITS_ENDPAGE
str_credits_end:
	.end
