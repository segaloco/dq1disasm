.include	"tunables.i"
.include	"npc.i"

.segment	"RODATA"

	.export tbl_npcs_mobile, tbl_npcs_static
tbl_npcs_mobile:
	.addr	npcs_castle_mobile
	.addr	npcs_throneroom_mobile
	.addr	npcs_dragoncastle_mobile
	.addr	npcs_maira_mobile
	.addr	npcs_rada_mobile
	.addr	npcs_galai_mobile
	.addr	npcs_mercado_mobile
	.addr	npcs_rim_mobile
	.addr	npcs_basement_mobile
	.addr	npcs_cave_staff_mobile
	.addr	npcs_cave_drop_mobile
	.addr	npcs_victory_mobile

tbl_npcs_static:
	.addr	npcs_castle_static
	.addr	npcs_throneroom_static
	.addr	npcs_dragoncastle_static
	.addr	npcs_maira_static
	.addr	npcs_rada_static
	.addr	npcs_galai_static
	.addr	npcs_mercado_static
	.addr	npcs_rim_static
	.addr	npcs_basement_static
	.addr	npcs_cave_staff_static
	.addr	npcs_dave_drop_static
	.addr	npcs_victory_static
; ----------------------------
npcs_castle_mobile:
	.byte	(npc_type::woman<<5)   |8,  13, $62
	.byte	(npc_type::guard_1<<5) |19, 2,  $17
	.byte	(npc_type::man<<5)     |11, 11, $1C
	.byte	(npc_type::wizard<<5)  |17, 11, $1D
	.byte	(npc_type::shopkeep<<5)|4,  21, $1F
	.byte	(npc_type::fighter<<5) |25, 11, $16
	.byte	(npc_type::guard_1<<5) |18, 18, $72
	.byte	(npc_type::guard_1<<5) |2,  12, $1B
	.byte	(npc_type::shopkeep<<5)|6,  25, $20
	.byte	(npc_type::fighter<<5) |24, 21, $22
	.byte	NPC_TABLE_END

npcs_castle_static:
	.byte	(npc_type::shopkeep<<5)|24, 1,  $0E
	.byte	(npc_type::woman<<5)   |27, 5,  $1A
	.byte	(npc_type::guard_1<<5) |8,  6,  $19
	.byte	(npc_type::man<<5)     |2,  8,  $18
	.byte	(npc_type::guard_1<<5) |8,  8,  $71
	.byte	(npc_type::guard_1<<5) |26, 15, $1E
	.byte	(npc_type::guard_1<<5) |15, 20, $63
	.byte	(npc_type::wizard<<5)  |20, 26, $6A
	.byte	(npc_type::guard_1<<5) |9,  27, $21
	.byte	(npc_type::guard_1<<5) |12, 27, $21
	.byte	NPC_TABLE_END
; ----------------------------
npcs_throneroom_mobile:
	.byte	(npc_type::guard_1<<5) |7,  5,  $65
	.byte	NPC_TABLE_END

npcs_throneroom_static:
	.byte	(npc_type::king<<5)    |3,  3,  $6E
	.byte	(npc_type::guard_1<<5) |3,  6,  $23
	.byte	(npc_type::guard_1<<5) |5,  6,  $24
	.byte	(npc_type::woman<<5)   |6,  3,  $6F
	.byte	NPC_TABLE_END
; ----------------------------
npcs_basement_mobile:
	.byte	NPC_TABLE_END

npcs_basement_static:
	.byte	(npc_type::wizard<<5)  |4,  6,  $66
	.byte	NPC_TABLE_END
; ----------------------------
npcs_victory_mobile:
	.byte	(npc_type::guard_1<<5) |19, 2,  $17
	.byte	(npc_type::man<<5)     |14, 23, $1C
	.byte	(npc_type::fighter<<5) |25, 11, $16
	.byte	(npc_type::guard_1<<5) |18, 18, $72
	.byte	(npc_type::guard_1<<5) |2,  12, $1B
	.byte	(npc_type::shopkeep<<5)|6,  25, $20
	.byte	(npc_type::fighter<<5) |24, 21, $22
	.byte	NPC_TABLE_END

npcs_victory_static:
	.byte	(npc_type::king<<5)    |11, 7,  $FE
	.byte	(npc_type::guard_2<<5) |9,  9,  $FD
	.byte	(npc_type::guard_2<<5) |9,  11, $FD
	.byte	(npc_type::guard_2<<5) |9,  13, $FD
	.byte	(npc_type::wizard<<5)  |12, 9,  $FD
	.byte	(npc_type::wizard<<5)  |12, 11, $FD
	.byte	(npc_type::wizard<<5)  |12, 13, $FD
	.byte	(npc_type::guard_1<<5) |9,  27, $FD
	.byte	(npc_type::guard_1<<5) |12, 27, $FD
	.byte	NPC_TABLE_END
; ----------------------------
npcs_dragoncastle_mobile:
	.byte	NPC_TABLE_END

npcs_dragoncastle_static:
	.byte	(npc_type::wizard<<5)  |16, 24, $70
	.byte	NPC_TABLE_END
; ----------------------------
npcs_cave_staff_mobile:
	.byte	NPC_TABLE_END

npcs_cave_staff_static:
	.byte	(npc_type::wizard<<5)  |4,  4,  $6C
	.byte	NPC_TABLE_END
; ----------------------------
npcs_cave_drop_mobile:
	.byte	NPC_TABLE_END

npcs_dave_drop_static:
	.byte	(npc_type::wizard<<5)  |4,  5,  $6D
	.byte	NPC_TABLE_END
; ----------------------------
npcs_mercado_mobile:
	.byte	(npc_type::man<<5)     |20, 15, $4B
	.byte	(npc_type::guard_1<<5) |5,  6,  $60
	.byte	(npc_type::shopkeep<<5)|25, 17, $4C
	.byte	(npc_type::woman<<5)   |4,  14, $49
	.byte	(npc_type::shopkeep<<5)|22, 5,  $03
	.byte	(npc_type::woman<<5)   |9,  16, $4A
	.byte	(npc_type::wizard<<5)  |14, 28, $6B
	.byte	(npc_type::guard_1<<5) |15, 6,  $48
	.byte	(npc_type::shopkeep<<5)|3,  26, $4E
	.byte	(npc_type::guard_1<<5) |22, 9,  $4D
	.byte	NPC_TABLE_END

npcs_mercado_static:
	.byte	(npc_type::shopkeep<<5)|8,  3,  $14
	.byte	(npc_type::wizard<<5)  |27, 6,  $0C
	.byte	(npc_type::man<<5)     |2,  7,  $0A
	.byte	(npc_type::shopkeep<<5)|2,  12, $45
	.byte	(npc_type::shopkeep<<5)|7,  12, $0B
	.byte	(npc_type::guard_1<<5) |24, 12, $05
	.byte	(npc_type::woman<<5)   |22, 13, $10
	.byte	(npc_type::wizard<<5)  |15, 16, $46
	.byte	(npc_type::wizard<<5)  |22, 22, $47
	.byte	(npc_type::shopkeep<<5)|27, 26, $04
	.byte	NPC_TABLE_END
; ----------------------------
npcs_rim_mobile:
	.byte	(npc_type::woman<<5)   |6,  21, $59
	.byte	(npc_type::man<<5)     |11, 8,  $30
	.byte	(npc_type::man<<5)     |6,  23, $5A
	.byte	(npc_type::woman<<5)   |22, 14, $56
	.byte	(npc_type::fighter<<5) |5,  25, $5B
	.byte	(npc_type::fighter<<5) |23, 11, $52
	.byte	(npc_type::man<<5)     |14, 11, $55
	.byte	(npc_type::fighter<<5) |16, 26, $69
	.byte	(npc_type::guard_1<<5) |8,  16, $54
	.byte	(npc_type::fighter<<5) |24, 19, $57
	.byte	NPC_TABLE_END

npcs_rim_static:
	.byte	(npc_type::man<<5)     |27, 0,  $51
	.byte	(npc_type::shopkeep<<5)|2,  4,  $4F
	.byte	(npc_type::wizard<<5)  |4,  7,  $0D
	.byte	(npc_type::shopkeep<<5)|23, 7,  $06
	.byte	(npc_type::woman<<5)   |15, 8,  $50
	.byte	(npc_type::wizard<<5)  |6,  13, $53
	.byte	(npc_type::shopkeep<<5)|16, 18, $15
	.byte	(npc_type::wizard<<5)  |3,  23, $61
	.byte	(npc_type::wizard<<5)  |20, 23, $58
	.byte	(npc_type::woman<<5)   |0,  26, $5C
	.byte	NPC_TABLE_END
; ----------------------------
npcs_rada_mobile:
	.byte	(npc_type::wizard<<5)  |9,  4,  $2B
	.byte	(npc_type::fighter<<5) |12, 19, $5D
	.byte	(npc_type::shopkeep<<5)|15, 9,  $2E
	.byte	(npc_type::man<<5)     |25, 22, $31
	.byte	(npc_type::man<<5)     |10, 14, $2C
	.byte	(npc_type::woman<<5)   |24, 4,  $0F
	.byte	(npc_type::guard_1<<5) |26, 15, $2F
	.byte	(npc_type::woman<<5)   |15, 24, $2D
	.byte	(npc_type::fighter<<5) |19, 18, $30
	.byte	(npc_type::fighter<<5) |3,  26, $27
	.byte	NPC_TABLE_END

npcs_rada_static:
	.byte	(npc_type::shopkeep<<5)|5,  4,  $01
	.byte	(npc_type::fighter<<5) |28, 1,  $25
	.byte	(npc_type::woman<<5)   |4,  7,  $29
	.byte	(npc_type::man<<5)     |20, 10, $26
	.byte	(npc_type::wizard<<5)  |24, 10, $67
	.byte	(npc_type::man<<5)     |1,  13, $2A
	.byte	(npc_type::shopkeep<<5)|10, 21, $12
	.byte	(npc_type::man<<5)     |20, 23, $28
	.byte	(npc_type::shopkeep<<5)|25, 25, $08
	.byte	(npc_type::guard_1<<5) |10, 26, $64
	.byte	NPC_TABLE_END
; ----------------------------
npcs_maira_mobile:
	.byte	(npc_type::man<<5)     |14, 13, $36
	.byte	(npc_type::man<<5)     |5,  12, $30
	.byte	(npc_type::guard_1<<5) |12, 10, $37
	.byte	(npc_type::wizard<<5)  |2,  12, $5E
	.byte	(npc_type::wizard<<5)  |20, 19, $38
	.byte	(npc_type::fighter<<5) |6,  7,  $35
	.byte	(npc_type::woman<<5)   |11, 14, $2E
	.byte	(npc_type::shopkeep<<5)|7,  19, $5F
	.byte	(npc_type::wizard<<5)  |20, 8,  $39
	.byte	NPC_TABLE_END

npcs_maira_static:
	.byte	(npc_type::wizard<<5)  |1,  1,  $68
	.byte	(npc_type::woman<<5)   |12, 1,  $32
	.byte	(npc_type::shopkeep<<5)|19, 4,  $11
	.byte	(npc_type::shopkeep<<5)|22, 12, $00
	.byte	(npc_type::fighter<<5) |20, 13, $33
	.byte	(npc_type::shopkeep<<5)|14, 21, $07
	.byte	(npc_type::guard_1<<5) |1,  23, $34
	.byte	NPC_TABLE_END
; ----------------------------
npcs_galai_mobile:
	.byte	(npc_type::woman<<5)   |12, 4,  $3E
	.byte	(npc_type::woman<<5)   |12, 12, $43
	.byte	(npc_type::wizard<<5)  |12, 8,  $3F
	.byte	(npc_type::wizard<<5)  |2,  10, $42
	.byte	(npc_type::man<<5)     |11, 7,  $3D
	.byte	(npc_type::man<<5)     |18, 12, $44
	.byte	(npc_type::fighter<<5) |7,  17, $41
	.byte	NPC_TABLE_END

npcs_galai_static:
	.byte	(npc_type::wizard<<5)  |14, 1,  $3A
	.byte	(npc_type::guard_1<<5) |3,  5,  $3B
	.byte	(npc_type::guard_1<<5) |5,  5,  $3B
	.byte	(npc_type::shopkeep<<5)|9,  6,  $3C
	.byte	(npc_type::shopkeep<<5)|5,  11, $09
	.byte	(npc_type::shopkeep<<5)|17, 15, $13
	.byte	(npc_type::wizard<<5)  |2,  17, $40
	.byte	(npc_type::shopkeep<<5)|10, 18, $02
	.byte	NPC_TABLE_END
