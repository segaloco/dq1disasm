.linecont +
.include	"system/apu.i"

.include	"apu_commands.i"

	.export apu_data_notes
	.export apu_data_songs
	.export apu_song_null, apu_song_title, apu_song_caste_throneroom
	.export apu_song_castle_courtyard, apu_song_town, apu_song_overworld
	.export apu_song_dungeon0, apu_song_dungeon1, apu_song_dungeon2
	.export apu_song_dungeon3, apu_song_dungeon4, apu_song_dungeon5
	.export apu_song_dungeon6, apu_song_dungeon7, apu_song_battle
	.export apu_song_boss, apu_song_credits, apu_song_lyre
	.export apu_song_flute, apu_song_bridge, apu_song_lose
	.export apu_song_inn, apu_song_lora, apu_song_curse
	.export apu_song_battle_cont, apu_song_win, apu_song_levelup
	.export apu_data_sfx_table
	.export sfx_ffdamage
	.export sfx_wing
	.export sfx_stairs
	.export sfx_run
	.export sfx_swamp
	.export sfx_menu
	.export sfx_confirm
	.export sfx_strike
	.export sfx_power
	.export sfx_hit
	.export sfx_damage
	.export sfx_damage
	.export sfx_windup
	.export sfx_miss1
	.export sfx_miss2
	.export sfx_bump
	.export sfx_text
	.export sfx_spell
	.export sfx_glow
	.export sfx_chest
	.export sfx_door
	.export sfx_fire

APU_DATA_NOTE_LENGTH	= 1

apu_data_notes:
	.word	(APU_DATA_NOTE_LENGTH<<11)|$6AD	;C2@65.4Hz,	[0x80]
	.word	(APU_DATA_NOTE_LENGTH<<11)|$64D	;C#2@69.3Hz,	[0x81]
	.word	(APU_DATA_NOTE_LENGTH<<11)|$5F3	;D2@73.4Hz,	[0x82]
	.word	(APU_DATA_NOTE_LENGTH<<11)|$59D	;D#2@77.8Hz,	[0x83]
	.word	(APU_DATA_NOTE_LENGTH<<11)|$54C	;E2@82.4Hz,	[0x84]
	.word	(APU_DATA_NOTE_LENGTH<<11)|$500	;F2@87.3Hz,	[0x85]
	.word	(APU_DATA_NOTE_LENGTH<<11)|$4B8	;F#2@92.5Hz,	[0x86]
	.word	(APU_DATA_NOTE_LENGTH<<11)|$474	;G2@98.0Hz,	[0x87]
	.word	(APU_DATA_NOTE_LENGTH<<11)|$434	;Ab2@103.9Hz,	[0x88]
	.word	(APU_DATA_NOTE_LENGTH<<11)|$3F8	;A2@110.0Hz,	[0x89]
	.word	(APU_DATA_NOTE_LENGTH<<11)|$3BF	;A#2@116.5Hz,	[0x8A]
	.word	(APU_DATA_NOTE_LENGTH<<11)|$389	;B2@123.5Hz,	[0x8B]
	.word	(APU_DATA_NOTE_LENGTH<<11)|$356	;C3@130.8Hz,	[0x8C]
	.word	(APU_DATA_NOTE_LENGTH<<11)|$326	;C#3@138.6Hz,	[0x8D]
	.word	(APU_DATA_NOTE_LENGTH<<11)|$2F9	;D3@146.8Hz,	[0x8E]
	.word	(APU_DATA_NOTE_LENGTH<<11)|$2CE	;D#3@155.6Hz,	[0x8F]
	.word	(APU_DATA_NOTE_LENGTH<<11)|$2A6	;E3@164.8Hz,	[0x90]
	.word	(APU_DATA_NOTE_LENGTH<<11)|$280	;F3@174.5Hz,	[0x91]
	.word	(APU_DATA_NOTE_LENGTH<<11)|$25C	;F#3@184.9Hz,	[0x92]
	.word	(APU_DATA_NOTE_LENGTH<<11)|$23A	;G3@196.0Hz,	[0x93]
	.word	(APU_DATA_NOTE_LENGTH<<11)|$21A	;Ab3@207.6Hz,	[0x94]
	.word	(APU_DATA_NOTE_LENGTH<<11)|$1FB	;A3@220.2Hz,	[0x95]
	.word	(APU_DATA_NOTE_LENGTH<<11)|$1DF	;A#3@233.1Hz,	[0x96]
	.word	(APU_DATA_NOTE_LENGTH<<11)|$1C4	;B3@247.0Hz,	[0x97]
	.word	(APU_DATA_NOTE_LENGTH<<11)|$1AB	;C4@261.4Hz,	[0x98]
	.word	(APU_DATA_NOTE_LENGTH<<11)|$193	;C#4@276.9Hz,	[0x99]
	.word	(APU_DATA_NOTE_LENGTH<<11)|$17C	;D4@293.6Hz,	[0x9A]
	.word	(APU_DATA_NOTE_LENGTH<<11)|$167	;D#4@310.8Hz,	[0x9B]
	.word	(APU_DATA_NOTE_LENGTH<<11)|$152	;E4@330.0Hz,	[0x9C]
	.word	(APU_DATA_NOTE_LENGTH<<11)|$13F	;F4@349.6Hz,	[0x9D]
	.word	(APU_DATA_NOTE_LENGTH<<11)|$12D	;F#4@370.4Hz,	[0x9E]
	.word	(APU_DATA_NOTE_LENGTH<<11)|$11C	;G4@392.5Hz,	[0x9F]
	.word	(APU_DATA_NOTE_LENGTH<<11)|$10C	;Ab4@414.4Hz,	[0xA0]
	.word	(APU_DATA_NOTE_LENGTH<<11)|$FD	;A4@440.0Hz,	[0xA1]
	.word	(APU_DATA_NOTE_LENGTH<<11)|$EF	;A#4@466.1Hz,	[0xA2]
	.word	(APU_DATA_NOTE_LENGTH<<11)|$E1	;B4@495.0Hz,	[0xA3]
	.word	(APU_DATA_NOTE_LENGTH<<11)|$D5	;C5@522.8Hz,	[0xA4]
	.word	(APU_DATA_NOTE_LENGTH<<11)|$C9	;C#5@553.8Hz,	[0xA5]
	.word	(APU_DATA_NOTE_LENGTH<<11)|$BD	;D5@588.8Hz,	[0xA6]
	.word	(APU_DATA_NOTE_LENGTH<<11)|$B3	;D#5@621.5Hz,	[0xA7]
	.word	(APU_DATA_NOTE_LENGTH<<11)|$A9	;E5@658.1Hz,	[0xA8]
	.word	(APU_DATA_NOTE_LENGTH<<11)|$9F	;F5@699.2Hz,	[0xA9]
	.word	(APU_DATA_NOTE_LENGTH<<11)|$96	;F#5@740.9Hz,	[0xAA]
	.word	(APU_DATA_NOTE_LENGTH<<11)|$8E	;G5@782.3Hz,	[0xAB]
	.word	(APU_DATA_NOTE_LENGTH<<11)|$86	;Ab5@828.7Hz,	[0xAC]
	.word	(APU_DATA_NOTE_LENGTH<<11)|$7E	;A5@880.9Hz,	[0xAD]
	.word	(APU_DATA_NOTE_LENGTH<<11)|$77	;A#5@932.3Hz,	[0xAE]
	.word	(APU_DATA_NOTE_LENGTH<<11)|$70	;B5@990.0Hz,	[0xAF]
	.word	(APU_DATA_NOTE_LENGTH<<11)|$6A	;C6@1046Hz,	[0xB0]
	.word	(APU_DATA_NOTE_LENGTH<<11)|$64	;C#6@1108Hz,	[0xB1]
	.word	(APU_DATA_NOTE_LENGTH<<11)|$5E	;D6@1178Hz,	[0xB2]
	.word	(APU_DATA_NOTE_LENGTH<<11)|$59	;D#6@1243Hz,	[0xB3]
	.word	(APU_DATA_NOTE_LENGTH<<11)|$54	;E6@1316Hz,	[0xB4]
	.word	(APU_DATA_NOTE_LENGTH<<11)|$4F	;F6@1398Hz,	[0xB5]
	.word	(APU_DATA_NOTE_LENGTH<<11)|$4B	;F#6@1472Hz,	[0xB6]
	.word	(APU_DATA_NOTE_LENGTH<<11)|$46	;G6@1576Hz,	[0xB7]
	.word	(APU_DATA_NOTE_LENGTH<<11)|$42	;Ab6@1670Hz,	[0xB8]
	.word	(APU_DATA_NOTE_LENGTH<<11)|$3F	;A6@1748Hz,	[0xB9]
	.word	(APU_DATA_NOTE_LENGTH<<11)|$3B	;A#6@1865Hz,	[0xBA]
	.word	(APU_DATA_NOTE_LENGTH<<11)|$38	;B6@1963Hz,	[0xBB]
	.word	(APU_DATA_NOTE_LENGTH<<11)|$34	;C7@2111Hz,	[0xBC]
	.word	(APU_DATA_NOTE_LENGTH<<11)|$31	;C#7@2238Hz,	[0xBD]
	.word	(APU_DATA_NOTE_LENGTH<<11)|$2F	;D7@2331Hz,	[0xBE]
	.word	(APU_DATA_NOTE_LENGTH<<11)|$2C	;D#7@2486Hz,	[0xBF]
	.word	(APU_DATA_NOTE_LENGTH<<11)|$29	;E7@2664Hz,	[0xC0]
	.word	(APU_DATA_NOTE_LENGTH<<11)|$27	;F7@2796Hz,	[0xC1]
	.word	(APU_DATA_NOTE_LENGTH<<11)|$25	;F#7@2944Hz,	[0xC2]
	.word	(APU_DATA_NOTE_LENGTH<<11)|$23	;G7@3107Hz,	[0xC3]
	.word	(APU_DATA_NOTE_LENGTH<<11)|$21	;G#7@3290Hz,	[0xC4]
	.word	(APU_DATA_NOTE_LENGTH<<11)|$1F	;A7@3496Hz,	[0xC5]
	.word	(APU_DATA_NOTE_LENGTH<<11)|$1D	;A#7@3729Hz,	[0xC6]
	.word	(APU_DATA_NOTE_LENGTH<<11)|$1B	;B7@3996Hz,	[0xC7]
	.word	(APU_DATA_NOTE_LENGTH<<11)|$1A	;C8@4144Hz,	[0xC8]

apu_data_songs:
apu_song_null:		.word	apu_pulse_null,			apu_pulse_null,		apu_triangle_null
apu_song_title:		.word	title_pulse1,			title_pulse2,		title_triangle
apu_song_caste_throneroom: .word	castle_throneroom_pulse, 	apu_empty,		castle_throneroom_triangle
apu_song_castle_courtyard: .word	castle_courtyard_pulse,		apu_empty,		castle_courtyard_triangle
apu_song_town:		.word	song_town_pulse,		apu_empty,		song_town_triangle
apu_song_overworld:	.word	song_overworld_pulse,		apu_empty,		song_overworld_triangle
apu_song_dungeon0:	.word	song_dungeon_pulse,		apu_empty,		song_dungeon_tri0
apu_song_dungeon1:	.word	song_dungeon_pulse,		apu_empty,		song_dungeon_tri1
apu_song_dungeon2:	.word	song_dungeon_pulse,		apu_empty,		song_dungeon_tri2
apu_song_dungeon3:	.word	song_dungeon_pulse,		apu_empty,		song_dungeon_tri3
apu_song_dungeon4:	.word	song_dungeon_pulse,		apu_empty,		song_dungeon_tri4
apu_song_dungeon5:	.word	song_dungeon_pulse,		apu_empty,		song_dungeon_tri5
apu_song_dungeon6:	.word	song_dungeon_pulse,		apu_empty,		song_dungeon_tri6
apu_song_dungeon7:	.word	song_dungeon_pulse,		apu_empty,		song_dungeon_tri7
apu_song_battle:	.word	song_battle_pulse,		apu_empty,		song_battle_triangle
apu_song_boss:		.word	song_boss_pulse1,		song_boss_pulse2,	song_boss_triangle
apu_song_credits:	.word	song_credits_pulse,		song_credits_pulse2,	song_credits_triangle
apu_song_lyre:		.word	song_lyre_pulse1,		song_lyre_pulse2,	apu_empty
apu_song_flute:		.word	apu_empty,			apu_empty,		song_flute_triangle
apu_song_bridge:	.word	song_bridge_pulse1,		song_bridge_pulse2,	apu_empty
apu_song_lose:		.word	song_lose_pulse1,		song_lose_pulse2,	apu_empty
apu_song_inn:		.word	song_inn_pulse1,		song_inn_pulse2,	apu_empty
apu_song_lora:		.word	song_lora_pulse1,		song_lora_pulse2,	song_lora_triangle
apu_song_curse:		.word	song_curse_pulse1,		song_curse_pulse2,	apu_empty
apu_song_battle_cont:	.word	song_battle_cont_pulse,		apu_empty,		song_battle_cont_triangle
apu_song_win:		.word	song_win_pulse1,		song_win_pulse2,	apu_empty
apu_song_levelup:	.word	song_levelup_pulse1,		song_levelup_pulse2,	apu_empty

apu_data_sfx_table:
sfx_ffdamage:	.addr	apu_sfx_ffdamage
sfx_wing:	.addr	apu_sfx_wing
sfx_stairs:	.addr	apu_sfx_stairs
sfx_run:	.addr	apu_sfx_run
sfx_swamp:	.addr	apu_sfx_swamp
sfx_menu:	.addr	apu_sfx_menu
sfx_confirm:	.addr	apu_sfx_confirm
sfx_strike:	.addr	apu_sfx_strike
sfx_power:	.addr	apu_sfx_power
sfx_hit:	.addr	apu_sfx_hit
sfx_damage:	.addr	apu_sfx_damage
sfx_damage2:	.addr	apu_sfx_damage
sfx_windup:	.addr	apu_sfx_windup
sfx_miss1:	.addr	apu_sfx_miss1
sfx_miss2:	.addr	apu_sfx_miss2
sfx_bump:	.addr	apu_sfx_bump
sfx_text:	.addr	apu_sfx_text
sfx_spell:	.addr	apu_sfx_spell
sfx_glow:	.addr	apu_sfx_glow
sfx_chest:	.addr	apu_sfx_chest
sfx_door:	.addr	apu_sfx_door
sfx_fire:	.addr	apu_sfx_fire
