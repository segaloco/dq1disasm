.segment	"PRG0CODE"

; ------------------------------------------------------------
L8000:
	brk
	brk
	ror	LBDAA, x
	tsx
	.byte	$47
	sta	LB6DA, y
	ldx	#$A7
	.byte	$62
	tax
	and	$43AA, x
	tax
L8012:	.byte	$24
L8013:	.byte	$8D
L8014:	.byte	$E6
L8015:	.byte	$8E
L8016:	.byte	$AE
L8017:	.byte	$8F
L8018:	.byte	$70
L8019:	.byte	$91
L801A:	brk
	brk
	brk
	brk
	brk
	.byte	$53
L8020:	ldx	$77
	.byte	$77
	.byte	$0F
L8024:	.byte	$B0
	.byte	$80
	.byte	$13
	.byte	$13
	asl	$78
	sta	($13, x)
	.byte	$13
	ora	($40, x)
	.byte	$82
	ora	a:$1D, x
	.byte	$02
	sty	$09
	ora	#$15
	.byte	$34
	sty	$1D
	ora	$F60F, x
	sta	$17
	.byte	$17
	.byte	$0B
	asl	$87, x
	ora	a:$1D, x
	txs
	txa
	.byte	$13
L804A:	.byte	$13
	brk
	cld
	dey
	ora	$041D, x
	.byte	$62
	.byte	$8B
	ora	a:$1D, x
	.byte	$24
L8057:	sta	$0909
	.byte	$10
L805B:	lsr	$8D, x
	ora	#$09
L805F:	.byte	$10
	.byte	$88
	sta	$0909
	bpl	L8020
	sta	$1313
	.byte	$10
	.byte	$82
	stx	$0909
	bpl	L8024
	stx	$0909
	bpl	L805B
	stx	$0909
	bpl	L8092
	.byte	$8F
	ora	#$09
	bpl	L80C9
	.byte	$8F
	ora	#$09
	bpl	L8100
	.byte	$8F
	ora	$1D
	bpl	L805F
	.byte	$8F
	ora	$100D
	sec
L808E:	bcc	L809D
	.byte	$0D
	.byte	$10
L8092:	txs
	bcc	L80A8
	.byte	$13
	bpl	L80F4
	.byte	$92
	ora	$100B
	.byte	$62
L809D:	sta	($13), y
	.byte	$13
	bpl	L80CC
	.byte	$92
	ora	#$09
	bpl	L8057
	.byte	$92
L80A8:	ora	#$09
	bpl	L808E
	.byte	$92
	ora	#$09
	bpl	L804A
	.byte	$44
L80B2:	eor	#$94
	.byte	$44
	.byte	$44
	.byte	$44
	.byte	$44
	.byte	$44
	sta	$4694, y
	.byte	$44
	sty	$AA, x
	.byte	$7A
	ldy	$66
	.byte	$64
	eor	#$44
	ror	$64
	.byte	$44
	.byte	$AF
L80C9:	.byte	$FF
	ldy	$64
L80CC:	ror	$44
	lsr	$64
	ror	$64
	.byte	$AF
	.byte	$6F
	ldy	$66
	ror	$64
	lsr	$44
	.byte	$44
	.byte	$64
	tax
	tax
	ldy	$46
	.byte	$44
	.byte	$64
	lsr	$46
	ror	$64
	tax
	tax
	ldy	$66
	.byte	$64
	.byte	$64
	lsr	$44
	.byte	$64
	.byte	$64
	lsr	$66
	.byte	$44
	.byte	$64
L80F4:	ror	$64
	.byte	$46
L80F7:	lsr	$66
	ror	$44
	ldy	$46
	ror	$64
	.byte	$64
L8100:	lsr	$44
	ldy	$46, x
	lsr	$66
	.byte	$44
	.byte	$4B
	.byte	$44
L8109:	.byte	$64
	lsr	$4A
	tax
	lsr	$44
	ldy	$44
	tax
	ldy	$64
	lsr	$4A
	lsr	a
	lsr	$46
	ror	$44
	ldy	$A4
	.byte	$64
	lsr	$4A
	tax
	lsr	$44
	ldy	$44
	tax
	ldy	$64
	lsr	$4A
	lsr	a
	lsr	$66
	ror	$44
	ldy	$A4
	.byte	$64
	lsr	$4A
	tax
	.byte	$44
	.byte	$44
	.byte	$44
	.byte	$44
	tax
	ldy	$64
	lsr	$44
	.byte	$74
	lsr	$66
	ror	$64
	.byte	$47
	.byte	$44
	.byte	$64
	lsr	$64
	.byte	$44
	ror	$46
	.byte	$64
	ror	$44
	lsr	$64
	.byte	$44
	ror	$66
	ror	$66
	ror	$66
	ror	$66
	.byte	$44
	sty	$46, x
	ror	$44
	lsr	$64
	.byte	$44
	ror	$64
	eor	#$99
	.byte	$44
	.byte	$44
	eor	#$46
	.byte	$64
	sty	$44, x
	.byte	$44
	sta	L9999, y
	sta	$4699, y
	.byte	$64
	sta	L9999, y
	sta	$1144, y
	.byte	$04
	eor	($44, x)
	.byte	$44
	.byte	$44
	lsr	$64
	sty	$41, x
	bpl	L8109
	asl	$6F, x
	.byte	$64
	.byte	$80
	ora	($69, x)
	sty	$11, x
	clc
	.byte	$04
	lsr	$64
	.byte	$64
	brk
	asl	$99
	.byte	$14
	bpl	L8198
L8198:	sta	$6444, y
	.byte	$44
	ora	#$06
	adc	#$11
	ora	($00), y
	ora	#$90
	rts

	brk
	.byte	$80
	asl	$60
	.byte	$14
	rti

	ror	$61
	sta	($66), y
	adc	($16, x)
	adc	($60, x)
	.byte	$04
	rti

	ror	$66
	asl	$61, x
	ora	($66), y
	ror	$60
	.byte	$80
	brk
	ror	$01
	ora	($08), y
	brk
	brk
	brk
	rts

	php
	rti

	ror	$01
	.byte	$44
	.byte	$44
	.byte	$14
	ror	$64
	.byte	$64
	rti

	ror	$16
	.byte	$04
	ror	$61
	.byte	$14
	bcc	L8220
	sbc	$6141, y
	ror	$04
	.byte	$44
	.byte	$F4
	.byte	$44
	ora	#$46
	sta	L8849, y
	ror	$84
	ora	($66), y
	.byte	$64
	.byte	$80
	.byte	$44
	sty	$49, x
	.byte	$80
	asl	$84, x
	.byte	$14
	.byte	$64
	.byte	$14
	dey
	ora	($99), y
	sty	$40
	adc	($84, x)
	ror	$61
	ora	($81), y
	.byte	$04
	.byte	$44
	eor	($40, x)
	ror	$04
	lsr	$64
	.byte	$44
	bpl	L8210
	ora	($14), y
	eor	#$16
L8210:	ora	($18, x)
	adc	($66, x)
	ror	$16
	.byte	$6F
	.byte	$14
	sta	$66, y
	bpl	L8285
	brk
	brk
	.byte	$04
L8220:	.byte	$64
	ora	($19), y
	adc	($66, x)
	ror	$10
	brk
	bcc	L822E
	.byte	$44
	.byte	$44
	eor	#$99
L822E:	brk
	brk
	brk
	ora	#$99
	ora	($11, x)
	ora	($11), y
	sty	$44, x
	rti

	eor	#$99
	sty	$44, x
	.byte	$14
	.byte	$44
	.byte	$44
	.byte	$44
	.byte	$44
	rti

	brk
	brk
	brk
	.byte	$04
	.byte	$44
	.byte	$44
	.byte	$44
	.byte	$04
	.byte	$44
	php
	brk
	lsr	$66
	ror	$40
	.byte	$80
	dey
	php
	.byte	$04
	ror	$66
	.byte	$64
	.byte	$04
	.byte	$64
	brk
	brk
	lsr	$66
	ror	$40
	brk
	brk
	brk
	.byte	$04
	ror	$66
	.byte	$64
	.byte	$04
	.byte	$F4
	brk
	brk
	lsr	$64
	ror	$44
	.byte	$44
	ror	$44
	.byte	$44
	ror	$46
	.byte	$64
	brk
	php
	.byte	$80
	brk
	lsr	$66
	ror	$66
	ror	$66
	ror	$66
	.byte	$66
L8285:	ror	$64
	php
	dey
	brk
	brk
	lsr	$66
	ror	$44
	.byte	$44
	.byte	$44
	.byte	$44
	.byte	$44
	ror	$66
	.byte	$64
	brk
	brk
	brk
	brk
	.byte	$44
	.byte	$44
	lsr	$46
	ror	$66
	ror	$64
	.byte	$44
	ldy	$44, x
	.byte	$44
	.byte	$64
	.byte	$44
	brk
	lsr	$66
	lsr	$45
	ror	$66
	.byte	$64
	.byte	$64
	ror	$66
	ror	$66
	ror	$64
	brk
	lsr	$66
	ror	$46
	ror	$66
	ror	$64
	ror	$66
	ror	$66
	ror	$64
	brk
	lsr	$66
	lsr	$44
	lsr	$66
	.byte	$64
	.byte	$44
	.byte	$44
	.byte	$44
	.byte	$44
	.byte	$44
	lsr	$64
	brk
	.byte	$44
	.byte	$44
	lsr	$48
	stx	$66
	pla
	sty	$66
	lsr	$64
	ror	$46
	.byte	$64
	brk
	lsr	$66
	lsr	$48
	stx	$66
	pla
	sty	$66
	lsr	$64
	ror	$46
	.byte	$64
	brk
	lsr	$66
	lsr	$48
	asl	$66
	rts

	sty	$66
	ror	$66
	ror	$66
	.byte	$64
	brk
	.byte	$43
	ror	$B6
	rti

	asl	$66
	rts

	.byte	$04
	ror	$66
	ror	$66
	ror	$64
	brk
	lsr	$36
	lsr	$40
	asl	$66
	rts

	.byte	$04
	ror	$46
	.byte	$64
	ror	$46
	.byte	$64
	brk
	.byte	$43
	.byte	$63
	lsr	$40
	ror	$66
	ror	$04
	ror	$46
	.byte	$64
	ror	$46
	.byte	$64
	brk
	.byte	$44
	.byte	$44
	lsr	$40
	.byte	$62
	.byte	$22
	rol	$04
	.byte	$44
	.byte	$44
	.byte	$44
	.byte	$44
	lsr	$44
	brk
	lsr	$66
	ror	$66
	.byte	$62
	tax
	rol	$66
	ror	$66
	.byte	$64
	ror	$66
	.byte	$64
	brk
	lsr	$66
	ror	$66
	.byte	$62
	tax
	rol	$66
	ror	$66
	.byte	$64
	tax
	tax
	ldy	$00
	.byte	$44
	lsr	$64
	.byte	$44
	.byte	$62
	.byte	$22
	rol	$44
	ror	$66
	.byte	$64
	tax
	tax
	ldy	$00
	lsr	$66
	ror	$64
	ror	$66
	ror	$46
	ror	$66
	.byte	$64
	ror	$66
	.byte	$64
	brk
	lsr	$66
	ror	$64
	lsr	$66
	.byte	$64
	.byte	$44
	.byte	$44
	lsr	$64
	ror	$66
	.byte	$64
	jsr	$6446
	ror	$66
	lsr	$66
	.byte	$64
	ror	$66
	ror	$64
	.byte	$44
	.byte	$44
	.byte	$44
	jsr	$6646
	ror	$66
	lsr	$66
	.byte	$64
	ror	$66
	ror	$64
	.byte	$22
	.byte	$22
	.byte	$22
	jsr	$2246
	ror	$46
	lsr	$66
	.byte	$64
	ror	$44
	.byte	$44
	.byte	$44
	.byte	$22
	.byte	$22
	.byte	$22
	jsr	$2242
	rol	$66
	lsr	$66
	.byte	$64
	ror	$46
	.byte	$64
	.byte	$64
	.byte	$22
	.byte	$22
	.byte	$22
	jsr	$2242
	rol	$66
	.byte	$44
	ror	$44
	ror	$66
	.byte	$6F
	.byte	$64
	.byte	$22
	.byte	$22
	.byte	$22
	jsr	$2242
	.byte	$22
	ror	$46
	ror	$64
	ror	$46
	.byte	$64
	.byte	$64
	.byte	$22
	.byte	$22
	.byte	$22
	jsr	$4444
	.byte	$44
	.byte	$44
	.byte	$44
	ror	$44
	.byte	$44
	.byte	$44
	.byte	$44
	.byte	$44
	.byte	$22
	.byte	$22
	.byte	$22
	jsr	$22
	brk
	brk
	brk
	ror	$00
	brk
	brk
	brk
	.byte	$22
	.byte	$22
	.byte	$22
	.byte	$22
	.byte	$27
	.byte	$44
	.byte	$44
	.byte	$44
	.byte	$44
	.byte	$44
	lsr	$66
	ror	$36
	.byte	$64
	lsr	$FF
	.byte	$FF
	.byte	$FF
	.byte	$64
	lsr	$F6
	.byte	$FF
	.byte	$6F
	.byte	$64
	lsr	$66
	.byte	$33
	ror	$64
	lsr	$66
	ror	$66
	.byte	$64
	lsr	$66
	ror	$66
	.byte	$64
	.byte	$44
	.byte	$44
	ldy	$44, x
	.byte	$44
	lsr	$66
	ror	$66
	.byte	$74
	.byte	$44
	.byte	$44
	.byte	$44
	.byte	$44
	.byte	$44
	.byte	$22
	.byte	$22
	.byte	$22
	.byte	$22
	.byte	$22
	.byte	$22
	.byte	$22
	.byte	$22
	.byte	$22
	.byte	$22
	.byte	$22
	.byte	$22
	.byte	$22
	.byte	$22
	.byte	$22
	.byte	$22
	bit	$44
	.byte	$42
	.byte	$22
	.byte	$22
	.byte	$22
	.byte	$44
	.byte	$44
	.byte	$44
	.byte	$44
	.byte	$42
	.byte	$44
	.byte	$42
	.byte	$22
	.byte	$22
	.byte	$44
	ror	$44
	.byte	$44
	.byte	$44
	.byte	$44
	lsr	$66
	lsr	$66
	.byte	$44
	lsr	$44
	.byte	$22
	bit	$46
	ror	$64
	lsr	$66
	.byte	$64
	ror	$66
	ror	$66
	.byte	$64
	ror	$64
	.byte	$42
	bit	$66
	ror	$66
	.byte	$44
	ror	$44
	ror	$66
	lsr	$66
	.byte	$64
	.byte	$64
	ror	$42
	bit	$66
	lsr	$66
	lsr	$66
	ror	$66
	.byte	$64
	.byte	$44
	ror	$66
	ror	$66
	.byte	$42
	bit	$66
	ror	$66
	lsr	$66
	.byte	$64
	.byte	$44
	.byte	$44
	.byte	$64
	.byte	$44
	.byte	$44
	.byte	$64
	ror	$42
	bit	$46
	ror	$64
	lsr	$66
	.byte	$64
	ror	$64
	.byte	$44
	ror	$64
	ror	$64
	.byte	$42
	.byte	$22
	.byte	$44
	ror	$44
	ror	$66
	.byte	$64
	lsr	$66
	ror	$66
	.byte	$44
	lsr	$44
	.byte	$12
	.byte	$22
	bit	$64
	lsr	$64
	.byte	$44
	.byte	$44
	.byte	$44
	ror	$46
	.byte	$64
	eor	($46, x)
	eor	($12, x)
	.byte	$22
	bit	$66
	ror	$64
	ror	$66
	.byte	$64
	lsr	$66
	.byte	$44
	ora	($11), y
	ora	($22), y
	.byte	$22
	bit	$64
	.byte	$44
	.byte	$44
	.byte	$63
	ror	$64
	.byte	$44
	.byte	$64
	eor	($12, x)
	ora	($12), y
	.byte	$22
	.byte	$22
	bit	$64
	ror	$64
	.byte	$63
	rol	$66, x
	.byte	$6B
	.byte	$64
	ora	($22), y
	.byte	$22
	asl	$2212, x
	bit	$64
	.byte	$64
	.byte	$64
	.byte	$63
	.byte	$33
	.byte	$64
	.byte	$44
	.byte	$64
	eor	($22, x)
	.byte	$22
	.byte	$22
	.byte	$02
	.byte	$22
	bit	$64
	ror	$64
	ror	$66
	.byte	$64
	lsr	$66
	.byte	$42
	.byte	$22
	.byte	$12
	and	#$02
	.byte	$22
	bit	$64
	.byte	$64
	.byte	$64
	.byte	$44
	.byte	$44
	.byte	$44
	ror	$64
	.byte	$42
	.byte	$12
	sta	($E1), y
	.byte	$22
	.byte	$22
	bit	$64
	ror	$64
	lsr	$66
	ror	$66
	.byte	$44
	.byte	$22
	.byte	$22
	brk
	.byte	$22
	.byte	$22
	.byte	$22
	bit	$64
	.byte	$64
	.byte	$64
	ror	$64
	.byte	$44
	.byte	$64
	.byte	$42
	.byte	$22
	.byte	$12
	and	#$12
	.byte	$22
	.byte	$22
	bit	$64
	ror	$64
	ror	$44
	bit	$44
	.byte	$22
	and	($12, x)
	ora	$1202, y
	.byte	$22
	bit	$64
	.byte	$64
	.byte	$64
	.byte	$64
	.byte	$42
	.byte	$22
	.byte	$22
	.byte	$22
	ora	($22), y
	ora	($01), y
	.byte	$12
	.byte	$22
	bit	$64
	ror	$64
	.byte	$64
	.byte	$22
	bit	$44
	.byte	$22
	.byte	$22
	and	($19, x)
	sta	($22), y
	.byte	$22
	.byte	$44
	.byte	$64
	lsr	$44
	.byte	$64
	.byte	$42
	.byte	$44
	.byte	$64
	.byte	$42
	.byte	$22
	ora	($00), y
	sta	($12), y
	bit	$46
	ror	$46
	lsr	$66
	.byte	$44
	lsr	$66
	.byte	$44
	.byte	$22
	.byte	$14
	lsr	$44
	.byte	$12
	bit	$66
	ror	$66
	ror	$66
	.byte	$44
	.byte	$6F
	inc	$64, x
	.byte	$44
	.byte	$44
	ror	$64
	.byte	$12
	bit	$66
	lsr	$64
	lsr	$66
	lsr	$6F
	ror	$66
	ror	$66
	ror	$64
	.byte	$02
	bit	$66
	ror	$64
	.byte	$44
	.byte	$64
	.byte	$44
	.byte	$6F
	inc	$64, x
	.byte	$44
	.byte	$44
	ror	$64
	.byte	$02
	bit	$46
	ror	$44
	bit	$64
	bit	$46
	ror	$44
	.byte	$22
	sty	$44, x
	.byte	$44
	.byte	$12
	.byte	$22
	.byte	$44
	.byte	$64
	.byte	$42
	bit	$64
	.byte	$22
	.byte	$44
	.byte	$64
	.byte	$42
	.byte	$22
	ora	($99), y
	sta	($12), y
	.byte	$22
	bit	$44
	.byte	$22
	sty	$64, x
	.byte	$92
	bit	$44
	.byte	$22
	.byte	$22
	and	($11, x)
	ora	($22), y
	.byte	$22
	.byte	$22
	.byte	$22
	and	#$94
	.byte	$54
	sta	$2222, y
	.byte	$22
	.byte	$22
	.byte	$22
	.byte	$22
	.byte	$22
	.byte	$22
	.byte	$44
	rti

	dey
	sty	$44
	.byte	$44
	dey
	dey
	dey
	dey
	.byte	$44
	.byte	$44
	lsr	$40
	php
	sty	$66
	.byte	$64
	dey
	dey
	dey
	eor	$6446
	.byte	$4F
	eor	#$08
	stx	$62
	ror	$11
	ora	($11), y
	ror	$66
	.byte	$64
	stx	$99, y
	php
	sty	$66
	.byte	$64
	sta	($88, x)
	dey
	.byte	$4F
	.byte	$44
	.byte	$44
	sta	$0490, y
	.byte	$44
	lsr	$44
	sta	($88, x)
	dey
	lsr	$48
	dey
	ora	#$00
	.byte	$04
	dey
	dey
	dey
	sta	($88, x)
	dey
	.byte	$44
	pha
	dey
	brk
	brk
	sty	$88
	dey
	dey
	sta	($88, x)
	dey
	dey
	dey
	php
	.byte	$80
	php
	sty	$88
	dey
	dey
	sta	($88, x)
	dey
	.byte	$80
	brk
	php
	dey
	.byte	$44
	.byte	$44
	.byte	$44
	dey
	dey
	ora	($18), y
	dey
	brk
	php
	brk
	dey
	dey
	dey
	sty	$88
	sta	($11, x)
	ora	($88), y
	.byte	$80
	.byte	$80
	php
	.byte	$44
	.byte	$44
	pha
	sty	$88
	ora	($11), y
	ora	($18), y
	.byte	$44
	.byte	$44
	.byte	$44
	lsr	$66
	pha
	sty	$81
	ora	($11), y
	ora	($11), y
	lsr	$64
	.byte	$64
	lsr	$66
	pha
	.byte	$1B
	ora	($11), y
	ora	($11), y
	ora	($66), y
	.byte	$6F
	.byte	$64
	lsr	$66
	pha
	.byte	$14
	sta	($11, x)
	ora	($11), y
	ora	($46), y
	.byte	$64
	.byte	$64
	.byte	$4B
	.byte	$44
	lsr	$14
	dey
	ora	($11), y
	ora	($18), y
	.byte	$44
	.byte	$44
	.byte	$44
	lsr	$46
	ror	$64
	dey
	sta	($11, x)
	ora	($88), y
	dey
	dey
	dey
	lsr	$46
	.byte	$44
	.byte	$44
	.byte	$44
	pha
	ora	($18), y
	dey
	dey
	dey
	dey
	lsr	$66
	lsr	$66
	ror	$48
	sta	($88, x)
	dey
	.byte	$44
	.byte	$44
	dey
	lsr	$46
	lsr	$06
	asl	$48
	sta	($11, x)
	ora	($11), y
	.byte	$14
	dey
	lsr	$46
	ror	$66
	ror	$48
	.byte	$44
	.byte	$44
	pha
	ora	($14), y
	dey
	lsr	$66
	lsr	$66
	ror	$44
	lsr	$63
	pha
	ora	($14), y
	dey
	.byte	$44
	.byte	$64
	lsr	$06
	asl	$66
	.byte	$6F
	.byte	$63
	pha
	sta	($88, x)
	dey
	.byte	$80
	brk
	lsr	$66
	ror	$44
	lsr	$63
	pha
	sta	($88, x)
L8709:	dey
	dey
	brk
	.byte	$44
	.byte	$44
	.byte	$44
	pha
	.byte	$44
	.byte	$44
	pha
	sta	($88, x)
	dey
	.byte	$44
	.byte	$44
	.byte	$44
	.byte	$44
	.byte	$44
	.byte	$44
	eor	($66, x)
	.byte	$14
	.byte	$44
	.byte	$44
	.byte	$44
	.byte	$44
	.byte	$44
	.byte	$44
	pha
	dey
	brk
	brk
	dey
	dey
	ora	($66), y
	ora	($88), y
	dey
	dey
	dey
	dey
	sty	$48
	.byte	$80
	brk
	brk
	php
	php
	ora	($66, x)
	bpl	L8746
	.byte	$44
	.byte	$44
	.byte	$44
	.byte	$44
	sty	$48
	.byte	$04
	.byte	$44
L8746:	.byte	$44
	brk
	brk
	brk
	ror	$00
	php
	lsr	$64
	.byte	$62
	bit	$84
	pha
	.byte	$04
	ror	$64
	brk
	brk
	brk
	ror	$10
	brk
	lsr	$6F
	.byte	$62
	bit	$84
	pha
	.byte	$04
	.byte	$6F
	.byte	$64
	brk
	brk
	brk
	ror	$10
	.byte	$80
	lsr	$64
	.byte	$62
	bit	$84
	pha
	.byte	$04
	lsr	$44
	brk
	ora	($00), y
	ror	$11
	.byte	$80
	.byte	$4B
	.byte	$44
	.byte	$44
	.byte	$44
	.byte	$04
	pha
	brk
	asl	$C0
	ora	($11, x)
	bpl	L87ED
	bpl	L8709
	brk
	php
	brk
	.byte	$80
	.byte	$04
	pha
	.byte	$80
	asl	$00
	ora	($81), y
	bpl	L87FC
	bpl	L8798
L8798:	brk
	dey
	dey
	dey
	.byte	$04
	pha
	brk
	asl	$00
	clc
	dey
	ora	($66), y
	brk
	.byte	$44
	.byte	$44
	.byte	$44
	.byte	$44
	pha
	.byte	$04
	pha
	brk
	asl	$01
	ora	($88), y
	sta	($66, x)
	brk
	lsr	$66
	lsr	$66
	rti

	.byte	$04
	rti

	brk
	asl	$01
	clc
	dey
	bpl	L8829
	brk
	lsr	$66
	lsr	$66
	rti

	.byte	$04
	pha
	.byte	$80
	asl	$00
	ora	($11), y
	bpl	L8838
	brk
	.byte	$44
	.byte	$64
	.byte	$44
	.byte	$64
	rti

	sty	$88
	brk
	asl	$00
	brk
	ora	($00), y
	ror	$00
	brk
	rts

	brk
	rts

	brk
	dey
	ror	$66
	ror	$66
	.byte	$66
L87ED:	ror	$66
	ror	$66
	ror	$66
	ror	$66
	ror	$66
	ror	$66
	ror	$66
	.byte	$66
L87FC:	ror	$66
	ror	$66
L8800:	ror	$66
	ror	$66
	ror	$66
	dey
	brk
	brk
	brk
	rts

	brk
	php
	asl	$00
	php
	brk
	brk
	.byte	$02
	.byte	$22
	dey
	pha
	.byte	$80
	brk
	brk
	rts

	brk
	dey
	asl	$08
	dey
	.byte	$80
L8820:	.byte	$22
	.byte	$22
	.byte	$22
	plp
	pha
	brk
	brk
	brk
	rts

L8829:	php
	.byte	$80
	asl	$88
	.byte	$80
	.byte	$02
	.byte	$22
	.byte	$22
	.byte	$22
	.byte	$22
	rti

	brk
	brk
	.byte	$4D
	.byte	$64
L8838:	brk
	brk
	asl	$08
	brk
	.byte	$22
	.byte	$22
	.byte	$22
	.byte	$22
	.byte	$22
	rti

	.byte	$44
	.byte	$44
	.byte	$44
	.byte	$64
	.byte	$44
	brk
L8849:	asl	$80
	.byte	$02
	.byte	$22
	jsr	$2202
	.byte	$22
	rti

	lsr	$64
	ror	$6F
	.byte	$64
	brk
	asl	$00
	.byte	$22
	brk
	brk
	brk
	.byte	$02
	.byte	$22
	rti

	lsr	$64
	.byte	$64
	.byte	$44
	.byte	$44
	php
	asl	$66
	cpx	#$00
	brk
	brk
	brk
	.byte	$02
	rti

	lsr	$6B
	ror	$66
	.byte	$64
	php
	brk
	brk
	jsr	$6404
	.byte	$44
	pha
	.byte	$02
	rti

	lsr	$44
	.byte	$64
	lsr	$64
	php
	.byte	$80
	.byte	$02
	.byte	$20
L8888:	.byte	$04
	ror	$46
	pha
	.byte	$02
	rti

	lsr	$64
	ror	$46
	.byte	$64
	dey
	dey
	.byte	$02
	.byte	$22
	.byte	$04
	ror	$F6
	rti

	.byte	$22
	rti

	lsr	$64
	ror	$46
	.byte	$64
	php
	.byte	$80
	.byte	$02
	jsr	$6604
	lsr	$40
	.byte	$82
	rti

	.byte	$44
	.byte	$44
	.byte	$44
	.byte	$44
	.byte	$44
	php
	brk
	.byte	$22
	.byte	$22
	.byte	$04
	.byte	$44
	.byte	$44
	pha
	.byte	$82
	rti

	brk
	brk
	brk
	brk
	brk
	brk
	.byte	$02
	.byte	$22
	.byte	$22
	brk
	brk
	.byte	$02
	plp
	.byte	$22
	.byte	$44
	.byte	$44
	.byte	$44
	.byte	$44
	.byte	$44
	.byte	$44
	.byte	$44
	.byte	$44
	.byte	$44
	.byte	$44
	.byte	$22
	.byte	$22
	.byte	$22
	.byte	$22
	.byte	$22
	ror	$66
	ror	$66
	ror	$66
	ror	$66
	ror	$66
	ror	$66
	ror	$66
	ror	$64
	.byte	$44
	ror	$44
	.byte	$44
	.byte	$44
	.byte	$44
	ror	$C1
	ora	($11), y
	ora	($16), y
	.byte	$44
	lsr	$64
	ror	$66
	lsr	$64
	ror	$64
	ror	$11
	.byte	$44
	.byte	$44
	.byte	$44
	asl	$46, x
	lsr	$64
	.byte	$44
	ror	$46
	.byte	$64
	ror	$64
	ror	$88
	lsr	$64
	.byte	$64
	asl	$46, x
	lsr	$66
	ror	$66
	.byte	$44
	.byte	$F4
	.byte	$64
	.byte	$44
	ror	$66
	ror	$6F
	.byte	$64
	asl	$66, x
	ror	$66
	ror	$66
	lsr	$66
	ror	$64
	ror	$66
	ror	$6F
	.byte	$64
	asl	$44, x
	lsr	$64
	.byte	$44
	ror	$44
	.byte	$64
	lsr	$64
	ror	$88
	lsr	$64
	.byte	$64
	asl	$46, x
	lsr	$64
	.byte	$6F
	ror	$1D
	ora	($46), y
	.byte	$64
	ror	$11
	.byte	$44
	.byte	$44
	.byte	$44
	asl	$4F, x
	lsr	$64
	.byte	$44
	ror	$11
	ora	($44), y
	.byte	$44
	ror	$11
	ora	($11), y
	ora	($16), y
	lsr	$46
	ror	$66
	ror	$66
	ror	$66
	ror	$66
	ror	$66
	ror	$66
	ror	$B6
	lsr	$66
	ror	$66
	ror	$66
	ror	$66
	ror	$66
	ror	$66
	ror	$64
	lsr	$46
	.byte	$64
	.byte	$44
	ror	$44
	lsr	$60
	.byte	$22
	jsr	$600
	.byte	$64
	.byte	$44
	.byte	$44
	ror	$46
	.byte	$64
	.byte	$6F
	ror	$F6
	lsr	$62
	.byte	$22
	.byte	$22
	brk
	asl	$64
	.byte	$64
	.byte	$6F
	ror	$46
	.byte	$64
	.byte	$44
	ror	$44
	lsr	$62
	plp
	.byte	$22
	jsr	$6F06
	.byte	$64
	.byte	$64
	ror	$46
	.byte	$64
	.byte	$6F
	ror	$66
	ror	$60
	.byte	$22
	jsr	$622
	.byte	$64
	.byte	$44
	.byte	$44
	.byte	$44
	lsr	$64
	.byte	$44
	.byte	$64
	.byte	$44
	lsr	$60
	brk
	brk
	asl	$6606
	ror	$66
	ror	$66
	.byte	$64
	ror	$64
	ror	$46
	rts

	brk
	brk
	.byte	$02
	asl	$66
	ror	$66
	ror	$66
	.byte	$64
	ror	$66
	ror	$46
	rts

	brk
	brk
	.byte	$22
	asl	$64
	.byte	$44
	ror	$44
	lsr	$64
	.byte	$44
	.byte	$44
	.byte	$44
	lsr	$60
	brk
	.byte	$22
	.byte	$22
	rol	$6F
	.byte	$64
	ror	$66
	lsr	$66
	ror	$66
	ror	$66
	rts

	.byte	$02
	.byte	$22
	.byte	$82
	rol	$64
	.byte	$44
	ror	$46
	lsr	$66
	ror	$66
	ror	$66
	rts

	.byte	$22
	plp
	.byte	$82
	rol	$66
	ror	$66
	.byte	$44
	lsr	$64
	.byte	$44
	ldy	$44, x
	lsr	$60
	.byte	$22
	dey
	.byte	$22
	asl	$64
	.byte	$44
	ror	$66
	ror	$64
	ora	($11), y
	ora	($46), y
	rts

	.byte	$02
	.byte	$22
	jsr	$6406
	.byte	$64
	.byte	$44
	lsr	$46
	.byte	$64
	ora	($11), y
	ora	($46), y
	rts

	brk
	.byte	$22
	brk
	asl	$64
	.byte	$F4
	ror	$66
	lsr	$64
	.byte	$44
	.byte	$44
	ora	($46), y
	ror	$46
	ror	$64
	ror	$64
	.byte	$64
	lsr	$44
	lsr	$64
	ror	$64
	ora	($B6), y
	.byte	$44
	.byte	$44
	.byte	$BB
	.byte	$44
	.byte	$44
	ror	$64
	ror	$43
	lsr	$64
	ror	$6F
	ora	($46), y
	lsr	a
	tax
	tax
	tax
	ldy	$64
	ror	$66
	inc	$46, x
	.byte	$64
	rol	$64, x
	ora	($46), y
	lsr	a
	.byte	$44
	.byte	$44
	.byte	$44
	ldy	$64
	.byte	$64
	ror	$43
	lsr	$64
	.byte	$44
	.byte	$44
	.byte	$44
	lsr	$4A
	tax
	ror	$AA
	ldy	$64
	.byte	$44
L8A88:	.byte	$44
	.byte	$44
	lsr	$66
	ror	$66
	ror	$66
	.byte	$44
	.byte	$44
	.byte	$44
	.byte	$44
	.byte	$44
	ror	$66
	ror	$66
	ror	$22
	lsr	$66
	ror	$66
	ror	$66
	ror	$22
	.byte	$47
	.byte	$22
	lsr	$11
	ora	($11), y
	ora	($11), y
	asl	$6E, x
	ror	$44
	lsr	$44
	.byte	$44
	.byte	$44
	.byte	$44
	.byte	$44
	.byte	$44
	.byte	$42
	.byte	$44
	lsr	$66
	ror	$66
	ror	$66
	.byte	$64
	.byte	$44
	.byte	$42
	.byte	$22
	lsr	$44
	.byte	$44
	lsr	$66
	ror	$62
	.byte	$22
	.byte	$42
	.byte	$44
	lsr	$46
	ror	$46
	.byte	$33
	ror	$64
	.byte	$22
	.byte	$22
	bit	$46
	.byte	$44
	ldy	$46, x
	rol	$66, x
	.byte	$64
	bit	$24
	bit	$46
	ror	$66
	ror	$66
	ror	$66
	ror	$66
	.byte	$64
	lsr	$66
	ror	$66
	ror	$66
	ror	$66
	ror	$64
	.byte	$44
	.byte	$44
	.byte	$44
	.byte	$44
	.byte	$44
	.byte	$44
	.byte	$44
	.byte	$44
	ror	$64
	bit	$66
	lsr	$48
	dey
	dey
	dey
	sty	$4B
	.byte	$44
	bit	$66
	inc	$48, x
	.byte	$80
	ror	$60
	dey
	stx	$88
	.byte	$44
	.byte	$64
	.byte	$44
	pha
	brk
	pla
	ror	$66
	ror	$66
	brk
	rts

	brk
	brk
	ror	$66
	rts

	asl	$00
	brk
	ror	$66
	ror	$66
	rts

	rts

	brk
	dec	$44, x
	pha
	dey
	.byte	$80
	rts

	.byte	$80
	brk
	jmp	($4600)

	inc	$44, x
	.byte	$44
	.byte	$44
	.byte	$64
	sta	($44, x)
	.byte	$64
	rti

	lsr	$44
	.byte	$44
	bit	$66
	.byte	$64
	eor	($46, x)
	inc	$40, x
	lsr	$46
	.byte	$64
	bit	$44
	.byte	$44
	bit	$46
	ror	$40
	lsr	$66
	.byte	$64
	.byte	$22
	.byte	$22
	.byte	$22
	.byte	$22
	.byte	$44
	.byte	$44
	rti

	.byte	$44
	.byte	$44
	.byte	$44
	brk
	brk
	brk
	brk
	brk
	brk
	brk
	brk
	brk
	brk
	brk
	brk
	brk
	brk
	brk
	brk
	.byte	$02
	.byte	$22
	.byte	$22
	.byte	$22
	.byte	$22
	.byte	$22
	.byte	$22
	.byte	$22
	brk
	brk
	brk
	brk
	brk
	brk
	.byte	$02
	.byte	$22
	.byte	$44
	pha
	.byte	$80
	brk
	brk
	brk
	.byte	$02
	.byte	$22
	jsr	$00
	brk
	brk
	asl	$6600
	pha
	brk
	brk
	php
	dey
	brk
	php
	.byte	$22
	.byte	$22
	brk
	brk
	brk
	.byte	$22
	brk
	ror	$40
	brk
	asl	$66
	ror	$66
	pla
	.byte	$80
	.byte	$02
	.byte	$22
	jsr	$2800
	lsr	$66
	rti

	.byte	$80
	asl	$08
	asl	$11
	rts

	brk
	brk
	dey
	jsr	$2800
	.byte	$44
	.byte	$F4
	rti

	.byte	$04
	lsr	$44
	lsr	$44
	rts

	.byte	$04
	.byte	$44
	.byte	$44
	jsr	$2000
	lsr	$66
	rti

	.byte	$04
	ror	$64
	ror	$64
	rts

	.byte	$04
	ror	$64
	.byte	$22
	brk
	jsr	$4444
	rti

	.byte	$04
	ror	$64
	ror	$64
	pla
	.byte	$04
	.byte	$4F
	.byte	$44
	.byte	$12
	brk
	jsr	$00
	brk
	.byte	$04
	.byte	$44
	.byte	$44
	.byte	$44
	.byte	$44
	ror	$66
	ror	$64
	.byte	$12
	brk
	jsr	$00
	.byte	$02
	plp
	brk
	brk
	php
	brk
	rts

	cpy	$66
	.byte	$64
	.byte	$12
	brk
	jsr	$2280
	.byte	$22
	.byte	$22
	brk
	asl	$66
	ror	$66
	.byte	$04
	ror	$64
	.byte	$12
	jsr	$221
	plp
	dey
	.byte	$22
	php
	ror	$66
	ror	$66
	.byte	$04
	.byte	$44
	.byte	$44
	.byte	$80
	jsr	$2221
	.byte	$80
	php
	jsr	$6606
	brk
	brk
	ror	$00
	brk
	dey
	dey
	jsr	$1121
	.byte	$80
	.byte	$82
	jsr	$6066
	php
	brk
	ror	$66
	ror	$66
	.byte	$66
L8C42:	inc	$21
	.byte	$22
	dey
	.byte	$22
	brk
	ror	$00
	dey
	brk
	ror	$66
	ror	$66
	ror	$E6
	and	($02, x)
	.byte	$22
	jsr	$6600
	php
	.byte	$80
	eor	$0860
	.byte	$80
	php
	dey
	jsr	L8020
	brk
	php
	brk
	ror	$00
	sty	$44
	.byte	$64
	.byte	$44
	.byte	$44
	.byte	$44
	.byte	$80
	jsr	$20
	brk
	dey
	.byte	$80
	ror	$08
	sty	$6F
	.byte	$64
	ror	$46
	.byte	$64
	.byte	$02
	jsr	$4420
	.byte	$44
	.byte	$44
	.byte	$44
	ror	$40
	sty	$44
	ror	$66
	ror	$64
	.byte	$02
	brk
	jsr	$6646
	ror	$66
	ror	$40
	.byte	$04
	ror	$66
	ror	$46
	.byte	$64
	.byte	$82
	brk
	jsr	$6646
	.byte	$64
	ror	$66
	rti

	sty	$46
	.byte	$44
	.byte	$4B
	.byte	$44
	.byte	$44
	.byte	$82
	brk
	jsr	$4644
	.byte	$64
	ror	$66
	rti

	.byte	$14
	ror	$64
	ror	$46
	.byte	$64
	.byte	$22
	brk
	plp
	lsr	$F6
	ror	$64
	lsr	$40
	.byte	$14
	ror	$64
	ror	$B6
	.byte	$34
	jsr	$2800
	.byte	$44
	lsr	$64
	ror	$66
	rti

	.byte	$14
	.byte	$44
	.byte	$44
	.byte	$44
	.byte	$44
	.byte	$44
	jsr	$2200
	lsr	$66
	.byte	$64
	ror	$66
	pha
	ora	($11), y
	.byte	$80
	php
	.byte	$82
	.byte	$22
	jsr	$200
	lsr	$66
	ror	$66
	ror	$48
	.byte	$80
	php
	brk
	.byte	$22
	.byte	$22
	brk
	brk
	brk
	.byte	$02
	.byte	$44
	.byte	$44
	.byte	$44
	.byte	$44
	.byte	$44
	pha
	dey
	.byte	$82
	.byte	$22
L8D01:	jsr	$00
	brk
	brk
	brk
	.byte	$02
	.byte	$22
	.byte	$22
	.byte	$22
	.byte	$22
	.byte	$22
	.byte	$22
	.byte	$22
	brk
	brk
	brk
	brk
	brk
	brk
	brk
	brk
	brk
	brk
	brk
	brk
	brk
	brk
	brk
	brk
	brk
	brk
	brk
	brk
	brk
	lsr	$66
	ror	$66
	.byte	$64
	.byte	$64
	ror	$66
	ror	$46
	ror	$66
	ror	$66
	ror	$66
	.byte	$64
	.byte	$44
	lsr	$66
	lsr	$64, x
	ror	$46
	ror	$66
	.byte	$64
	rol	$46, x
	ror	$66
	.byte	$64
	ror	$46
	ror	$66
	ror	$66
	ror	$66
	cpx	$EEEE
	inc	$4646
	ror	$66
	ror	$64
	lsr	$66
	ror	$66
	.byte	$64
	inc	$EEEE
	inc	$6666
	.byte	$44
	.byte	$44
	.byte	$44
	ror	$66
	lsr	$64
	.byte	$64
	ror	$EE
	.byte	$CB
	inc	$66EE
	ror	$46
	.byte	$64
	.byte	$64
	ror	$66
	.byte	$44
	.byte	$44
	.byte	$44
	ror	$EE
	inc	$EEEE
	ror	$46
	ror	$66
	ror	$64
	.byte	$44
	.byte	$44
	lsr	$44, x
	.byte	$44
	dec	$EE
	inc	$66EE
	lsr	$64
	.byte	$64
	.byte	$64
	ror	$46
	.byte	$44
	ror	$64
	lsr	$46
	ror	$44
	lsr	$66
	lsr	$46, x
	lsr	$46
	lsr	$66
	lsr	$63
	lsr	$46
	lsr	$66
	.byte	$44
	lsr	$66
L8DAB:	lsr	$44
	ror	$64
	lsr	$46
	.byte	$64
	.byte	$64
	.byte	$64
	ror	$46
	ror	$66
	ror	$66
	.byte	$22
	.byte	$22
	.byte	$22
	.byte	$22
	ora	($02, x)
	.byte	$22
	.byte	$02
	.byte	$22
	.byte	$22
	jsr	$00
	.byte	$22
	.byte	$02
	.byte	$22
	.byte	$02
	.byte	$03
	brk
	.byte	$02
	jsr	$2022
	.byte	$02
	brk
	brk
	.byte	$02
	brk
	.byte	$02
	.byte	$22
	jsr	$2222
	.byte	$02
	.byte	$22
	.byte	$22
	.byte	$22
	brk
	.byte	$22
	.byte	$02
	jsr	$232
	brk
	.byte	$22
	.byte	$22
	jsr	$2002
	brk
	jsr	$200
	jsr	$00
	brk
	.byte	$22
	brk
	.byte	$22
	.byte	$22
	.byte	$22
	brk
	jsr	$2222
	.byte	$02
	jsr	$2202
	brk
	.byte	$22
	.byte	$02
	jsr	$2002
	.byte	$03
	jsr	$322
	jsr	$202
	brk
	.byte	$22
	.byte	$22
	brk
	brk
	.byte	$22
	brk
	.byte	$22
	.byte	$22
	.byte	$02
	.byte	$02
	jsr	$2002
	bmi	L8E1F
	.byte	$02
	.byte	$20
L8E1F:	brk
	.byte	$02
	.byte	$02
	jsr	$2002
	.byte	$22
	.byte	$02
	.byte	$02
	jsr	$2222
	brk
	.byte	$22
	.byte	$22
	brk
	.byte	$22
	.byte	$02
	.byte	$02
	jsr	$2220
	jsr	$2002
	.byte	$02
	jsr	$202
	jsr	$20
	jsr	$2212
	.byte	$02
	.byte	$22
	.byte	$22
	.byte	$02
	jsr	$2230
	jsr	$00
	brk
	.byte	$02
	brk
	.byte	$02
	jsr	$2000
	brk
	.byte	$22
	.byte	$22
	.byte	$22
	.byte	$22
	ora	($22, x)
	jsr	$2222
	.byte	$22
	jsr	$200
	brk
	.byte	$02
	.byte	$22
	jsr	$22
	brk
	.byte	$22
	jsr	$2222
	.byte	$02
	.byte	$22
	jsr	$200
	jsr	$2000
	brk
	.byte	$02
	.byte	$22
	.byte	$02
	.byte	$22
	.byte	$22
	.byte	$22
	.byte	$22
	bmi	L8EA0
	.byte	$22
	.byte	$22
	.byte	$02
	.byte	$22
	.byte	$12
	.byte	$03
	ora	($20, x)
	.byte	$12
	clc
	asl	a
	dey
	ldy	#$03
	brk
	.byte	$22
	jsr	$22
	.byte	$22
	jsr	$200
	.byte	$22
	plp
	dey
	txs
	.byte	$82
	.byte	$02
	jsr	$2420
	brk
	.byte	$02
L8EA0:	.byte	$22
	jsr	$200
	.byte	$22
	php
	tax
	txa
	ldx	#$00
	bmi	L8EAE
	brk
	.byte	$20
L8EAE:	ora	($23, x)
	.byte	$02
	.byte	$22
	jsr	$2A12
	tax
	tax
	sta	($22, x)
	jsr	$2200
	brk
	.byte	$02
	jsr	$2210
	.byte	$22
	.byte	$22
	plp
	tay
	dey
	ldy	#$02
	jsr	$122
	.byte	$22
	.byte	$02
	jsr	$02
	.byte	$02
	.byte	$02
	.byte	$23
	.byte	$02
	.byte	$22
	brk
	.byte	$02
	brk
	brk
	.byte	$02
	.byte	$23
	.byte	$02
	.byte	$22
	.byte	$22
	brk
	brk
	.byte	$02
	.byte	$12
	.byte	$02
	.byte	$22
	.byte	$22
	.byte	$22
	.byte	$22
	.byte	$22
	.byte	$22
	.byte	$22
	.byte	$22
	jsr	$00
	.byte	$02
	.byte	$32
	jsr	$2232
	.byte	$22
	.byte	$22
	jsr	$200
	.byte	$22
	.byte	$02
	.byte	$22
	.byte	$22
	.byte	$02
	.byte	$22
	.byte	$02
	.byte	$20
	.byte	$02
L8F01:	brk
	.byte	$02
	.byte	$02
	dey
	tax
	tax
	txa
	txa
	txa
	tax
	txa
	.byte	$89
	txa
	tax
	tay
	txa
	dey
	txa
	txs
	dey
	tax
	tax
	tax
	clv
	tay
	txs
	tax
	.byte	$89
	tay
	tay
	dey
	txa
	txa
	tay
	tax
	tax
	txa
	txa
	tay
	tay
	txa
	txa
	txa
	tay
	tay
	tax
	txa
	txa
	tay
	tay
	.byte	$AB
	txa
	txa
	tay
	tay
	dey
	txa
	txa
	tay
	tax
	tax
	tax
	txa
	tay
	dey
	dey
	dey
	txa
	tax
	tax
	tax
	tax
	tax
	.byte	$12
	.byte	$22
	.byte	$22
	.byte	$22
	.byte	$23
	brk
	brk
	php
	.byte	$80
	dey
	brk
	brk
	brk
	brk
	brk
	brk
	brk
	brk
	brk
	brk
	.byte	$02
	.byte	$22
	.byte	$22
	.byte	$22
	jsr	$2202
	.byte	$22
	.byte	$22
	jsr	$2212
	.byte	$22
	.byte	$22
	.byte	$23
	.byte	$02
	.byte	$22
	.byte	$22
	.byte	$22
	jsr	$2202
	.byte	$22
	.byte	$22
	jsr	$00
	brk
	brk
	brk
	.byte	$12
	.byte	$22
	.byte	$22
	jsr	$02
	jsr	$222
	jsr	$2202
	.byte	$22
	.byte	$22
	.byte	$02
	jsr	$220
	.byte	$22
	brk
	.byte	$22
	jsr	$2202
	.byte	$22
	.byte	$22
	.byte	$02
	jsr	$220
	jsr	$2222
	jsr	$222
	jsr	$220
	jsr	$2222
	jsr	$2000
	jsr	$2222
	jsr	$20
	jsr	$2220
	jsr	$2620
	jsr	$2220
	jsr	$520
	jsr	$2A22
	tay
	brk
	brk
	.byte	$22
	.byte	$22
	.byte	$02
	jsr	$202
	.byte	$22
	.byte	$02
	.byte	$22
	txa
	ldx	#$08
	.byte	$80
	.byte	$02
	.byte	$22
	.byte	$22
	.byte	$02
	.byte	$02
	.byte	$12
	.byte	$22
	.byte	$02
	.byte	$32
	.byte	$22
	brk
	dey
	ldx	#$2A
	tax
	jsr	$2202
	.byte	$22
	jsr	$220
	jsr	$20
	brk
	.byte	$80
	ldy	#$22
	.byte	$22
	.byte	$22
	.byte	$22
	.byte	$22
	jsr	$2220
	brk
	brk
	jsr	$00
	jsr	$2222
	.byte	$22
	jsr	$2232
	jsr	$24
	jsr	$02
	brk
	brk
	brk
	.byte	$12
	.byte	$22
	.byte	$22
	.byte	$22
	jsr	$2022
	brk
	jsr	$00
	.byte	$22
	jsr	$2220
	.byte	$22
	jsr	$2022
	.byte	$22
	.byte	$22
	jsr	$2000
	jsr	$20
	jsr	$2222
	.byte	$22
	jsr	$2222
	.byte	$22
	jsr	$2020
	brk
	jsr	$3222
	.byte	$22
	.byte	$22
	.byte	$22
	.byte	$22
	jsr	$2222
	.byte	$12
	.byte	$02
	.byte	$22
	.byte	$22
	.byte	$22
	.byte	$22
	.byte	$22
	.byte	$22
	.byte	$02
	.byte	$02
	.byte	$02
	brk
	jsr	$02
	.byte	$44
	.byte	$02
	.byte	$02
	.byte	$02
	.byte	$22
	.byte	$02
	jsr	$2200
	brk
	.byte	$02
	.byte	$22
	.byte	$22
	.byte	$22
	.byte	$22
	jsr	$822
	tay
	dey
	dey
	.byte	$80
	jsr	$2212
	.byte	$22
	.byte	$22
	bit	$20
	jsr	$22
	plp
	txa
	tax
	tay
	ldy	#$00
	.byte	$02
	jsr	$222
	brk
	.byte	$22
	.byte	$22
	.byte	$02
	jsr	LAA2A
	tay
	dey
	ldx	#$22
	rti

	.byte	$02
	jsr	$2200
	jsr	$200
	brk
	plp
	tax
	tay
	tax
	ldy	#$22
	.byte	$22
	jsr	$00
L908F:	brk
	jsr	$1220
	.byte	$22
	rol	a
	tax
	tax
	tay
	ldx	#$22
	.byte	$22
	.byte	$22
	.byte	$22
	.byte	$22
	.byte	$02
	.byte	$04
	.byte	$44
	.byte	$02
	.byte	$22
	plp
	tay
	dey
	dey
	.byte	$82
	.byte	$22
	.byte	$02
	.byte	$22
	.byte	$22
	brk
	.byte	$22
	jsr	$2222
	.byte	$02
	asl	a
	txa
	tax
	txa
	ldx	#$02
	jsr	$220
	.byte	$22
	.byte	$02
	.byte	$02
	brk
	brk
	.byte	$02
	asl	a
	tay
	tax
	tax
	.byte	$80
	.byte	$02
	brk
	.byte	$02
	.byte	$02
	.byte	$02
	.byte	$22
	jsr	$200
	.byte	$22
	asl	a
	tax
	txa
	txa
	.byte	$82
	.byte	$02
	.byte	$22
	.byte	$22
	brk
	brk
	.byte	$02
	.byte	$02
	.byte	$02
	.byte	$02
	.byte	$02
	asl	a
	tay
	tax
	txa
	ldx	#$22
	.byte	$22
	.byte	$22
	.byte	$22
	.byte	$22
	.byte	$22
	jsr	$220
	brk
	asl	a
	dey
	dey
	txa
	.byte	$80
	.byte	$02
	jsr	$2220
	.byte	$22
	.byte	$22
	.byte	$02
	.byte	$22
	.byte	$22
	.byte	$02
	rol	a
	tay
	tay
	tay
	ldy	#$02
	brk
	brk
	.byte	$02
	.byte	$22
	.byte	$22
	jsr	$2022
	.byte	$12
	rol	a
	txa
	tax
	txa
	.byte	$80
	.byte	$22
	jsr	$2020
	.byte	$22
	.byte	$22
	.byte	$02
	.byte	$02
	.byte	$22
	brk
	jsr	$2020
	brk
	brk
	.byte	$02
	brk
	brk
	.byte	$02
	jsr	$2022
	.byte	$22
	jsr	$2222
	.byte	$22
	.byte	$22
	brk
	brk
	.byte	$02
	brk
	brk
	jsr	$00
	.byte	$02
	.byte	$02
	.byte	$02
	.byte	$02
	.byte	$02
	.byte	$22
	jsr	$2200
	.byte	$22
	.byte	$22
	.byte	$22
	.byte	$02
	.byte	$22
	.byte	$22
	.byte	$22
	.byte	$02
	.byte	$02
	jsr	$20
	brk
	brk
	ora	$00
	.byte	$23
	.byte	$22
	brk
	brk
	brk
	jsr	$2022
	.byte	$22
	.byte	$22
	.byte	$22
	.byte	$22
	.byte	$22
	.byte	$22
	.byte	$22
	.byte	$22
	jsr	$2022
	.byte	$22
	.byte	$22
	jsr	$2022
	.byte	$22
	.byte	$22
	.byte	$22
	.byte	$22
	.byte	$02
	.byte	$22
	bit	$20
	.byte	$22
	.byte	$22
	jsr	$2020
	.byte	$12
	.byte	$02
	.byte	$12
	.byte	$22
	jsr	$20
	brk
	.byte	$22
	.byte	$22
	.byte	$22
	.byte	$02
	.byte	$22
	jsr	$2000
	jsr	$20
	brk
	brk
	brk
	brk
	jsr	$2022
	.byte	$22
	.byte	$22
	brk
	tax
	ldy	#$02
	.byte	$22
	jsr	$20
	jsr	$223
	.byte	$22
	.byte	$22
	brk
	.byte	$02
	.byte	$22
	plp
	ldx	#$20
	brk
	.byte	$02
	bit	$22
	.byte	$02
	.byte	$02
	jsr	$2000
	jsr	$220
	tax
	ldx	#$02
	.byte	$02
	jsr	$2020
	jsr	$2220
	.byte	$22
	jsr	$2202
	tay
	tay
	ldx	#$20
	jsr	$32
	brk
	.byte	$02
	.byte	$02
	jsr	$2020
	brk
	jsr	L8A88
	.byte	$82
	.byte	$22
	.byte	$02
	jsr	$2020
	.byte	$12
	.byte	$22
	.byte	$22
	.byte	$22
	.byte	$02
	jsr	LA802
	tay
	ldy	#$00
	brk
	brk
	brk
	brk
	jsr	$2022
	jsr	$2220
	.byte	$22
	txa
	tax
	ldx	#$20
	.byte	$12
	jsr	$2022
	.byte	$22
	.byte	$02
	.byte	$02
	brk
	.byte	$02
	brk
	.byte	$02
	jsr	$2002
	.byte	$22
	.byte	$02
	.byte	$02
	.byte	$22
	.byte	$22
	.byte	$02
	.byte	$22
	jsr	$22
	brk
	.byte	$02
	brk
	brk
	.byte	$02
	.byte	$02
	.byte	$02
	jsr	$2212
	.byte	$22
	.byte	$22
	.byte	$02
	.byte	$22
	.byte	$22
	.byte	$02
	.byte	$02
	jsr	$00
	brk
	brk
	brk
	brk
	.byte	$02
	brk
	.byte	$02
	.byte	$22
	.byte	$22
	.byte	$22
	.byte	$22
	.byte	$22
	.byte	$22
	.byte	$22
	.byte	$22
	.byte	$22
	.byte	$22
	brk
	brk
	.byte	$02
	brk
	brk
	brk
	brk
	.byte	$22
	jsr	$00
	.byte	$02
	jsr	$22
	brk
	.byte	$22
	brk
	.byte	$02
	jsr	$2012
	ora	($22, x)
	.byte	$22
	brk
	.byte	$22
	brk
	.byte	$02
	jsr	$200
	jsr	$22
	brk
	brk
	.byte	$22
	jsr	$00
	brk
	.byte	$02
	brk
	brk
	brk
	brk
	brk
	brk
	brk
	.byte	$22
	.byte	$22
	.byte	$22
	.byte	$22
	.byte	$22
	.byte	$22
	.byte	$22
	.byte	$23
	.byte	$02
	brk
	brk
	brk
	.byte	$02
	.byte	$32
	jsr	$2202
	.byte	$22
	.byte	$22
	ora	($22, x)
	.byte	$22
	brk
	brk
	brk
	.byte	$02
	brk
	.byte	$02
	.byte	$22
	.byte	$02
	.byte	$22
	.byte	$22
	.byte	$22
	.byte	$22
	.byte	$02
	.byte	$22
	.byte	$02
	.byte	$02
	.byte	$02
	.byte	$02
	.byte	$02
	.byte	$02
	.byte	$22
	.byte	$02
	.byte	$23
	.byte	$22
	.byte	$02
	.byte	$02
	.byte	$02
	.byte	$22
	txa
	txa
	txa
	dey
	brk
	.byte	$82
	.byte	$22
	.byte	$02
	.byte	$22
	.byte	$22
	.byte	$02
	.byte	$22
	.byte	$22
	jsr	L8800
	dey
	dey
	dey
	txa
	.byte	$A3
	.byte	$02
	.byte	$22
	.byte	$22
	.byte	$22
	jsr	$2232
	.byte	$22
	.byte	$22
	tax
	tax
	tax
	tax
	txs
	.byte	$82
	.byte	$22
	jsr	$202
	.byte	$02
	brk
	.byte	$22
	.byte	$22
	.byte	$22
	tax
	tax
	tay
	txa
	txa
	.byte	$80
	.byte	$02
	brk
	.byte	$22
	.byte	$22
	.byte	$02
	.byte	$22
	.byte	$02
	jsr	L8820
	txa
	tax
	dey
	tax
	ldx	#$22
	.byte	$02
	.byte	$02
	brk
	jsr	$220
	.byte	$22
	.byte	$02
	tax
	tax
	txa
	txa
	txa
	.byte	$82
	.byte	$02
	.byte	$22
	.byte	$03
	.byte	$22
	.byte	$22
	.byte	$22
	.byte	$22
	.byte	$22
	jsr	L8888
	tay
	dey
	tax
	ldx	#$20
	jsr	$22
	.byte	$02
	brk
	.byte	$22
	bit	$22
	tax
	txa
	dey
	tax
	tay
	ldy	#$02
	.byte	$02
	brk
	jsr	$202
	.byte	$22
	.byte	$22
	.byte	$22
	tax
	dey
	tay
	txa
	txa
	.byte	$82
	jsr	$2220
	.byte	$22
	jsr	$2022
	.byte	$12
	brk
	brk
	brk
	brk
	brk
	brk
	brk
	brk
	brk
	brk
	brk
	brk
	brk
	brk
	brk
	brk
	brk
	brk
	brk
	brk
	brk
	brk
	brk
	brk
	brk
	brk
	brk
	brk
	brk
	brk
L9332:	.byte	$34
L9333:	.byte	$93
	brk
	brk
	brk
	rti

	.byte	$02
	brk
	.byte	$03
	brk
	brk
	brk
	brk
	rti

	.byte	$03
	rti

	.byte	$02
	rti

	cld
	.byte	$03
	cmp	$DA03, y
	.byte	$03
	.byte	$DB
	.byte	$03
	cld
	.byte	$03
	.byte	$DC
	.byte	$03
	cmp	$DE03, x
	.byte	$03
	cli
	.byte	$02
	eor	$5A02, y
	.byte	$02
	.byte	$5B
	.byte	$02
	cli
	.byte	$02
	eor	$5E02, x
	.byte	$02
	.byte	$5F
	.byte	$02
	.byte	$22
	ora	($22, x)
	eor	($2A, x)
	ora	($2B, x)
	ora	($22, x)
	ora	($22, x)
	eor	($2B, x)
	eor	($2A, x)
	eor	($70, x)
	.byte	$03
	.byte	$72
	.byte	$03
	.byte	$74
	.byte	$03
	ror	$03, x
	dey
	.byte	$03
	.byte	$89
	.byte	$03
	txa
	.byte	$03
	.byte	$8B
	.byte	$03
	beq	L9388
	sbc	($02), y
L9388:	.byte	$F2
	.byte	$02
	.byte	$F3
	.byte	$02
	beq	L9390
	.byte	$F4
	.byte	$02
L9390:	sbc	$02, x
	inc	$02, x
	.byte	$0C
	brk
	.byte	$0C
	rti

	.byte	$0F
	rti

	asl	$0C40
	brk
	.byte	$0C
	rti

	asl	$0F00
	brk
	pla
	.byte	$02
	adc	#$02
	ror	a
	.byte	$02
	.byte	$6B
	.byte	$02
	sta	L8C42
	eor	($8F, x)
	.byte	$42
	stx	$2041
	brk
	and	($00, x)
	bit	$00
	and	$00
	and	($40, x)
	jsr	$2540
	rti

	bit	$40
	rti

	brk
	eor	($00, x)
	.byte	$42
	brk
	.byte	$43
	brk
	.byte	$44
	brk
	eor	$00
	lsr	$00
	.byte	$47
	brk
	rts

	brk
	and	($00, x)
	.byte	$62
	brk
	and	$00
	.byte	$64
	brk
	jsr	$6640
	brk
	bit	$40
	.byte	$80
	brk
	eor	($00, x)
	.byte	$82
	brk
	.byte	$43
	brk
	sty	$00
	eor	$00
	stx	$00
	.byte	$47
	brk
	ldy	$00
	lda	$00
	ldx	#$00
	.byte	$A3
	brk
	ldy	$00
	lda	$00
	ldx	$00
	.byte	$A7
	brk
	clc
	.byte	$03
	clc
	.byte	$43
	.byte	$1A
	.byte	$03
	.byte	$1B
	.byte	$03
	clc
	.byte	$03
	clc
	.byte	$43
	.byte	$1B
	.byte	$43
	.byte	$1A
	.byte	$43
	cpy	$00
	cmp	$00
	dec	$00
	.byte	$C7
	brk
	cpy	$00
	iny
	brk
	cmp	#$00
	dex
	brk
	adc	#$42
	pla
	.byte	$42
	.byte	$6B
	.byte	$42
	ror	a
	.byte	$42
	sty	L8D01
	.byte	$02
	stx	L8F01
	.byte	$02
	.byte	$04
	brk
	ora	$00
	asl	$00
	.byte	$07
	brk
	.byte	$04
	brk
	ora	$00
	php
	brk
	ora	#$00
	.byte	$DF
	.byte	$03
	cpx	#$03
	sbc	($03, x)
	.byte	$E2
	.byte	$03
	.byte	$DF
	.byte	$03
	.byte	$E3
	.byte	$03
	cpx	$03
	sbc	$03
	pla
	.byte	$02
	adc	#$02
	ror	a
	.byte	$02
	.byte	$6B
	.byte	$02
	pla
	.byte	$02
	.byte	$5C
	.byte	$02
	jmp	($6D02)

	.byte	$02
	bit	$2D01
	ora	($2E, x)
	ora	($2F, x)
	ora	($2C, x)
	ora	($2D, x)
	ora	($23, x)
	ora	($1C, x)
	ora	($70, x)
	.byte	$03
	.byte	$72
	.byte	$03
	.byte	$74
	.byte	$03
	ror	$03, x
	dey
	.byte	$03
	.byte	$89
	.byte	$03
	txa
	.byte	$03
	.byte	$8B
	.byte	$03
	.byte	$F7
	.byte	$02
	sed
	.byte	$02
	sbc	$FA02, y
	.byte	$02
	.byte	$F7
	.byte	$02
	.byte	$FB
	.byte	$02
	.byte	$FC
	.byte	$02
	sbc	$1002, x
	brk
	ora	($00), y
	.byte	$14
	brk
	ora	$00, x
	bpl	L949E
L949E:	ora	($00), y
	.byte	$12
	brk
	.byte	$13
	brk
	pla
	.byte	$02
	adc	#$02
	ror	a
	.byte	$02
	.byte	$6B
	.byte	$02
	sta	L8C42
	eor	($8F, x)
	.byte	$42
	stx	$2641
	brk
	.byte	$27
	brk
	plp
	brk
	and	#$00
	bmi	L94BE
L94BE:	and	($00), y
	.byte	$32
	brk
	.byte	$33
	brk
	pha
	brk
	eor	#$00
	lsr	a
	brk
	.byte	$4B
	brk
	jmp	$4D00

	brk
	lsr	$4F00
	brk
	rol	$00
	.byte	$27
	brk
	plp
	brk
	and	#$00
	bmi	L94DE
L94DE:	and	($00), y
	.byte	$32
	brk
	.byte	$33
	brk
	pha
	brk
	eor	#$00
	lsr	a
	brk
	.byte	$4B
	brk
	jmp	$4D00

	brk
	lsr	$4F00
	brk
	tay
	brk
	lda	#$03
	tax
	brk
	.byte	$AB
	.byte	$03
	ldy	LAD00
	.byte	$03
	ldx	LAF00
	.byte	$03
	sec
	.byte	$03
	ora	$3C43, y
	.byte	$03
	and	$3803, x
	.byte	$03
	ora	$3A43, y
	.byte	$03
	.byte	$3B
	.byte	$03
	dec	$CB00
	brk
	cpy	$CD00
	brk
	dec	$CF00
	brk
	bne	L9522
L9522:	cmp	($00), y
	adc	#$42
	pla
	.byte	$42
	.byte	$6B
	.byte	$42
	ror	a
	.byte	$42
	sty	L8D01
	.byte	$02
	stx	L8F01
	.byte	$02
	ora	($00, x)
	ora	($40, x)
	asl	a
	brk
	.byte	$0B
	brk
	ora	($00, x)
	ora	($40, x)
	.byte	$0B
	rti

	asl	a
	rti

	inc	$03
	bcs	L954B
	lda	($03), y
	.byte	$E7
L954B:	.byte	$03
	inx
	.byte	$03
	bcs	L9553
	sbc	#$03
	nop
L9553:	.byte	$03
	sei
	.byte	$02
	adc	$7A02, y
	.byte	$02
	.byte	$7B
	.byte	$02
	.byte	$7C
	.byte	$02
	adc	$7E02, y
	.byte	$02
	.byte	$7F
	.byte	$02
	ora	$1D01, x
	eor	($1E, x)
	ora	($1F, x)
	ora	($1D, x)
	ora	($1D, x)
	eor	($1F, x)
	eor	($1E, x)
	eor	($70, x)
	.byte	$03
	.byte	$72
	.byte	$03
	.byte	$74
	.byte	$03
	ror	$03, x
	dey
	.byte	$03
	.byte	$89
	.byte	$03
	txa
	.byte	$03
	.byte	$8B
	.byte	$03
	inc	$FF02, x
	.byte	$02
	.byte	$C2
	.byte	$02
	.byte	$C3
	.byte	$02
	ldy	#$02
	.byte	$FF
	.byte	$02
	lda	($02, x)
	sta	$02
	ora	$0D00
	rti

	asl	$00, x
	.byte	$17
	brk
	ora	$0D00
	rti

	.byte	$17
	rti

	asl	$40, x
	pla
	.byte	$02
	adc	#$02
	ror	a
	.byte	$02
	.byte	$6B
	.byte	$02
	sta	L8C42
	eor	($8F, x)
	.byte	$42
	stx	$3441
	brk
	and	$00, x
	rol	$00, x
	.byte	$37
	brk
	and	$40, x
	.byte	$34
	rti

	.byte	$37
	rti

	rol	$40, x
	bvc	L95C6
L95C6:	eor	($00), y
	.byte	$52
	brk
	.byte	$53
	brk
	.byte	$54
	brk
	eor	$00, x
	lsr	$00, x
	.byte	$57
	brk
	.byte	$34
	brk
	adc	($00), y
	rol	$00, x
	.byte	$73
	brk
	and	$40, x
	adc	$00, x
	.byte	$37
	rti

	.byte	$77
	brk
	bvc	L95E6
L95E6:	sta	($00), y
	.byte	$52
	brk
	.byte	$93
	brk
	.byte	$54
	brk
	sta	$00, x
	lsr	$00, x
	.byte	$97
	brk
	ldy	$03, x
	lda	$00, x
	.byte	$B2
	.byte	$03
	.byte	$B3
	.byte	$03
	ldy	$03, x
	lda	$00, x
	ldx	$03, y
	.byte	$B7
	.byte	$03
	rol	$3F03, x
	.byte	$03
	cpy	#$03
	cmp	($03, x)
	rol	$3F03, x
	.byte	$03
	cmp	($43, x)
	cpy	#$43
	.byte	$D2
	brk
	.byte	$D3
	brk
	.byte	$D4
	brk
	cmp	$00, x
	dec	$00, x
	.byte	$D3
	brk
	.byte	$D7
	brk
	.byte	$92
	brk
	adc	#$42
	pla
	.byte	$42
	.byte	$6B
	.byte	$42
	ror	a
	.byte	$42
	sty	L8D01
	.byte	$02
	stx	L8F01
	.byte	$02
	ora	$40
	.byte	$04
	rti

	.byte	$07
	rti

	asl	$40
	ora	$40
	.byte	$04
	rti

	ora	#$40
	php
	rti

	.byte	$EB
	.byte	$03
	.byte	$DF
	.byte	$43
	cpx	$ED03
	.byte	$03
	.byte	$EB
	.byte	$03
	.byte	$DF
	.byte	$43
	inc	$EF03
	.byte	$03
	adc	$6802, x
	.byte	$42
	sta	($02, x)
	.byte	$83
	.byte	$02
	adc	$6802, x
	.byte	$42
	ror	$6F02
	.byte	$02
	and	$2C41
	eor	($1C, x)
	eor	($23, x)
	eor	($2D, x)
	eor	($2C, x)
	eor	($2F, x)
	eor	($2E, x)
	eor	($70, x)
	.byte	$03
	.byte	$72
	.byte	$03
	.byte	$74
	.byte	$03
	ror	$03, x
	dey
	.byte	$03
	.byte	$89
	.byte	$03
	txa
	.byte	$03
	.byte	$8B
	.byte	$03
	.byte	$87
	.byte	$02
	.byte	$F7
	.byte	$42
	adc	$02
	.byte	$67
	.byte	$02
	.byte	$87
	.byte	$02
	.byte	$F7
	.byte	$42
	adc	($02, x)
	.byte	$63
	.byte	$02
	ora	($40), y
	bpl	L96D8
	ora	$40, x
	.byte	$14
	rti

	ora	($40), y
	bpl	L96E0
	.byte	$13
	rti

	.byte	$12
	rti

	pla
	.byte	$02
	adc	#$02
	ror	a
	.byte	$02
	.byte	$6B
	.byte	$02
	sta	L8C42
	eor	($8F, x)
	.byte	$42
	stx	$2741
	rti

	rol	$40
	and	#$40
	plp
	rti

	and	($40), y
	bmi	L9700
	.byte	$33
	rti

	.byte	$32
	rti

	.byte	$27
	rti

	rol	$40
	and	#$40
	plp
	rti

	and	($40), y
	bmi	L9710
	.byte	$33
	rti

	.byte	$32
	rti

	tya
	brk
	.byte	$99
	brk
L96D8:	txs
	brk
	.byte	$9B
	brk
	.byte	$9C
	brk
	.byte	$9D
	brk
L96E0:	.byte	$9E
	brk
	.byte	$9F
	brk
	tya
	brk
	sta	L9A00, y
	brk
	.byte	$9B
	brk
	.byte	$9C
	brk
	sta	L9E00, x
	brk
	.byte	$9F
	brk
	clv
	.byte	$03
	.byte	$B9
L96F7:	brk
	tsx
	.byte	$03
	.byte	$BB
	.byte	$03
	ldy	LBD03, x
	brk
L9700:	ldx	LBF03, y
	.byte	$03
	sec
	.byte	$03
	and	$3A03, y
	.byte	$03
	.byte	$3B
	.byte	$03
	and	$3843, y
	.byte	$43
L9710:	.byte	$3C
	.byte	$03
	and	$D003, x
	brk
	cmp	($00), y
	.byte	$D2
	brk
	.byte	$D3
	brk
	.byte	$D4
	brk
	cmp	$00, x
	dec	$00, x
	.byte	$D7
	brk
	adc	#$42
	pla
	.byte	$42
	.byte	$6B
	.byte	$42
	ror	a
	.byte	$42
	sty	L8D01
	.byte	$02
	stx	L8F01
	.byte	$02
L9734:	.byte	$64
L9735:	.byte	$97
	ldx	#$97
	nop
	.byte	$97
	.byte	$B3
	tya
	adc	$98, x
	sbc	$98
	sbc	$3797, y
	tya
	.byte	$B3
	.byte	$97
	.byte	$EF
	.byte	$97
	.byte	$F4
	.byte	$97
	clv
	.byte	$97
	.byte	$83
	.byte	$97
	ldx	$97
	.byte	$EB
	.byte	$97
	.byte	$CF
	tya
	sty	$98, x
	.byte	$FB
	tya
	clc
	tya
	lsr	$98, x
	ldy	$97, x
	beq	L96F7
	sbc	$97, x
	dec	$C897
	eor	$5362
	.byte	$42
	.byte	$17
	.byte	$0B
	.byte	$4B
	.byte	$1C
	lda	($4B), y
	ora	$5564, x
	.byte	$1F
	and	$164B, y
	.byte	$52
	.byte	$52
	.byte	$72
	.byte	$42
	jmp	$661B

	eor	$3820, y
	eor	$22, x
	.byte	$FF
	sei
	eor	($0E, x)
	.byte	$DB
	eor	$1A
	pha
	lsr	$19
	.byte	$02
	pha
	clc
	pha
	php
	adc	($5A), y
	.byte	$0F
	asl	$344F, x
	.byte	$63
	ldy	$7A, x
	ror	a
	eor	#$3B
	and	($4C, x)
	.byte	$7B
	and	($FF, x)
	.byte	$47
	eor	$65
	.byte	$FF
	.byte	$83
	.byte	$43
	ror	$2643
	.byte	$23
	eor	$66
	bit	$C6
	.byte	$43
	.byte	$6F
	.byte	$FF
	.byte	$FF
	ldy	$46
	ror	$FF
	.byte	$53
	.byte	$42
	.byte	$17
	asl	$1C57
	and	$164B, y
	.byte	$52
	.byte	$52
	.byte	$72
	.byte	$42
	jmp	$661B

	eor	$3820, y
	eor	$22, x
	.byte	$FF
	.byte	$8B
	.byte	$47
	inc	$49E9, x
	sbc	$4BE9, x
	sbc	$4DE9, x
	sbc	$49AC, x
	sbc	$4BAC, x
	sbc	$4DAC, x
	sbc	$3B49, x
	sbc	$7B4C, x
	sbc	$FFFF, x
	bcs	L9845
	.byte	$70
L97EE:	.byte	$FF
	.byte	$FF
	ldy	$24
	.byte	$6C
	.byte	$FF
	.byte	$FF

	ldy	$65
	adc	$14FF
	.byte	$4F
	.byte	$4B
	eor	$46
	rts

	adc	$4C51, y
	cpy	$4E
	eor	#$76
	eor	$03
	cmp	#$50
	lsr	a
	.byte	$AE
L980C:	.byte	$5C
	.byte	$6B
	.byte	$4F
	lsr	$48
	.byte	$63
	.byte	$5A
	lsr	$4956
	eor	$68FF
	.byte	$43
	.byte	$14
	.byte	$BB
	lsr	$0C
	.byte	$02
	.byte	$27
	asl	a
	.byte	$62
	bit	$6745
	jmp	($580B)

	bit	$D605
	adc	LAF10
	bvc	L9876
	ldx	$56, y
	.byte	$47
	.byte	$7B
	.byte	$7A
	.byte	$04
	.byte	$FF
	dec	$55
	eor	$480B, y
	bmi	L9844
	.byte	$57
	.byte	$5A
	dec	$4E, x
	lsr	$25, x
L9844:	.byte	$59
L9845:	.byte	$5B
	.byte	$37
	.byte	$4B
	.byte	$52
	asl	$554B
	bmi	L98A8
	adc	#$48
	bvc	L98A6
	sec
	.byte	$53
	.byte	$57
	.byte	$FF
	.byte	$1B
	rti

	eor	($62), y
	.byte	$04
	.byte	$4F
	ldy	$07
	ora	$4777
	asl	$CF
	php
	bvc	L980C
	adc	$7053
	.byte	$32
	ora	$A3, x
	.byte	$37
	adc	($B4, x)
	.byte	$57
	cli
	cpy	#$5A
	.byte	$5C
	.byte	$FF
	.byte	$A9
L9876:	.byte	$44
	.byte	$2B
	bit	$5D53
	.byte	$6F
	eor	#$2E
	ora	$3156, y
	asl	a
	lsr	$D82C
	.byte	$44
	.byte	$0F
	.byte	$5A
	.byte	$4F
	.byte	$2F
	.byte	$CF
	cli
	and	$5233
	bmi	L98B4
	.byte	$5A
	.byte	$27
	.byte	$FF
	adc	$44
	ora	($3C, x)
	eor	($25, x)
	cpy	$47
	and	#$14
	lsr	a
	rol	$B8
	lsr	a
	.byte	$67
	ora	($4D, x)
	rol	a
L98A6:	ror	a
	.byte	$75
L98A8:	.byte	$12
	.byte	$14
	.byte	$17
	plp
	adc	$0879, y
	lsr	a
	.byte	$1A
	.byte	$64
	.byte	$FF
	.byte	$0E
L98B4:	eor	$0536
	jmp	$4C30

	lsr	a
	.byte	$37
	ldx	#$4C
	lsr	$53B4, x
	sec
	rol	$47
	and	$CB, x
	lsr	$672E
	.byte	$53
	.byte	$5F
	ldy	$48, x
	and	LA1FF, y
	eor	($68, x)
	cpy	$3241
	.byte	$73
	.byte	$04
	ora	($76), y
	jmp	($3400)

	eor	$6E33
	adc	$07, x
	eor	($57, x)
	.byte	$34
	.byte	$FF
	cpy	$3E44
	cpy	$434C
	ldy	$3F48
	ldx	#$4A
	.byte	$42
	.byte	$0B
	.byte	$47
	and	$4C12, x
	.byte	$44
	.byte	$27
	eor	($41), y
	.byte	$FF
	ldx	$3A41
	.byte	$43
	and	$3B
	eor	$65
	.byte	$3B
	adc	#$46
	.byte	$3C
	adc	$6B
	ora	#$71
	.byte	$6F
	.byte	$13
	ldx	#$31
	rti

	ror	a
	.byte	$12
	.byte	$02
	.byte	$FF
	ora	($02, x)
	brk
	ora	($00, x)
	ora	($00, x)
	ora	($01, x)
	.byte	$03
	ora	($02, x)
	.byte	$02
	ora	($02, x)
	.byte	$02
	.byte	$02
	.byte	$02
	ora	($02, x)
	.byte	$02
	.byte	$02
	.byte	$02
	.byte	$02
	.byte	$02
	.byte	$02
	.byte	$02
	.byte	$02
	.byte	$02
	.byte	$02
	.byte	$02
	.byte	$02
	.byte	$02
	.byte	$02
	.byte	$02
	.byte	$02
	.byte	$02
	.byte	$02
	.byte	$02
	.byte	$02
	.byte	$02
	.byte	$02
	.byte	$02
	.byte	$02
	.byte	$02
	.byte	$02
	.byte	$02
	.byte	$02
	.byte	$02
	.byte	$02
	.byte	$02
	asl	a
	brk
	.byte	$3C
	brk
	ldy	$00, x
	bmi	L9951
	.byte	$DC
	.byte	$05
L9951:	pha
	rol	$02
	brk
	.byte	$14
	brk
	lsr	$00
	bit	$E801
	.byte	$03
	clv
	.byte	$0B
	.byte	$14
	asl	a:$02, x
	.byte	$5A
	brk
	jsr	$D003
	and	$18, y
	and	$00, x
	php
	brk
	rol	$00
	lsr	$00
	.byte	$14
	brk
	brk
	brk
	asl	a:$00, x
	brk
	brk
	brk
	pla
	ora	($00, x)
	brk
	rts

	ora	#$00
	brk
	brk
	brk
	brk
	brk
	.byte	$62
	and	$55, x
	.byte	$14
	asl	$19
	.byte	$64
	.byte	$37
	.byte	$02
	.byte	$03
	asl	a
	.byte	$0B
	asl	a:$FD
	.byte	$01
L9999:	.byte	$02
	.byte	$07
	php
	asl	$01FD
	.byte	$02
	.byte	$03
	php
	ora	#$0A
	.byte	$0F
	sbc	$0100, x
	.byte	$02
	php
	ora	#$0F
	sbc	$0403, x
	.byte	$0B
	.byte	$0C
	sbc	$1005, x
	sbc	$0302, x
	.byte	$04
	asl	a
	.byte	$0B
	.byte	$0C
	sbc	$1311, x
	asl	$15, x
	sbc	$1311, x
	asl	$FD, x
	ora	($13), y
	asl	$FD, x
	ora	($13), y
	sbc	$1516, x
	sbc	$200, x
	.byte	$04
	asl	a
	.byte	$0F
	.byte	$14
	.byte	$1C
	plp
	brk
	.byte	$02
	.byte	$04
	asl	a
	bpl	L99F5
	clc
	.byte	$1C
	brk
	.byte	$04
	asl	a
	.byte	$14
L99E3:	brk
	ora	($02, x)
	.byte	$12
	.byte	$0F
	bpl	L99F5
	asl	$07
	php
	ora	#$0A
	.byte	$05
L99F0:	.byte	$0F
	clc
	ora	$1B1A, y
L99F5:	.byte	$1C
	ora	$1F1E, x
	jsr	$2221
	.byte	$23
	bit	$25
	.byte	$26
L9A00:	brk
	ora	($0F, x)
	.byte	$0C
	bpl	L9A09
	.byte	$04
	ora	$0B
L9A09:	asl	$0D
	ora	($13), y
	.byte	$14
	asl	a
	asl	$0310
	.byte	$04
	ora	$0C
	ora	($17), y
	.byte	$16
L9A18:	.byte	$3A
L9A19:	txs
L9A1A:	.byte	$46
L9A1B:	txs
L9A1C:	.byte	$56
L9A1D:	txs
	ror	$9A
L9A20:	.byte	$73
L9A21:	txs
	.byte	$7F
	txs
L9A24:	.byte	$8B
L9A25:	txs
	.byte	$97
	txs
	.byte	$A3
	txs
	.byte	$AF
	txs
L9A2C:	.byte	$2E
L9A2D:	txs
	asl	$3030
	asl	$2424
	asl	$2727
	asl	$2A2A
	asl	$0E0E
	asl	$0E0E
	asl	$0E0E
	asl	$0E0E
	bmi	L9A58
	ora	($10), y
	brk
	and	#$29
	.byte	$1A
	.byte	$27
	and	#$37
	ora	($0E), y
	asl	$0E0E
	bmi	L9A68
L9A58:	ora	($10), y
	brk
	asl	$29, x
	.byte	$1A
	.byte	$27
	and	#$37
L9A61:	ora	($0E), y
	asl	$0E0E
	bmi	L9A76
L9A68:	asl	a:$10
	asl	$0E, x
	asl	$0E0E
	asl	$0E0E
	bmi	L9A83
	.byte	$0E
L9A76:	asl	$0E0E
	asl	$0E0E
	asl	$0E0E
	asl	$16, x
	asl	$16, x
L9A83:	asl	$16, x
	asl	$16, x
	asl	$16, x
	asl	$16, x
	and	$30, x
	.byte	$12
	and	$27, x
	.byte	$1A
	and	$30, x
	brk
	and	$30, x
	.byte	$07
	bpl	L9AA9
	bpl	L9AAB
	bpl	L9AAD
	bpl	L9AAF
	bpl	L9AB1
	bpl	L9AB3
	asl	$0E, x
	asl	$0E0E
	.byte	$0E
L9AA9:	.byte	$0E
	.byte	$0E
L9AAB:	.byte	$0E
	.byte	$0E
L9AAD:	.byte	$0E
	.byte	$0E
L9AAF:	.byte	$1C
	.byte	$15
L9AB1:	bmi	L9AC1
L9AB3:	asl	$0E0E
	asl	$0E0E
	asl	$160E
	ora	$0E30
	.byte	$0E
	.byte	$0E
L9AC1:	asl	$0E0E
	asl	$0E0E
	ora	($15, x)
	bmi	L9AD9
	asl	$0E0E
	asl	$0E0E
	asl	$130E
	ora	$0C, x
	rol	$0C
	.byte	$0E
L9AD9:	rol	$30
	asl	$1526
	asl	$3600
	.byte	$0F
	brk
	bmi	L9AF4
	rol	$14
	and	#$0E
	asl	$1500
	asl	$0E30
	asl	$0E0E
	.byte	$0E
	.byte	$0E
L9AF4:	asl	$0E0E
	rol	$13
	asl	$0E0E, x
	bmi	L9B0C
	asl	$0E0E
	asl	$260E
	.byte	$03
	bmi	L9B1C
	.byte	$27
	.byte	$07
	.byte	$03
	ora	$0E, x
L9B0C:	asl	$0E0E
	.byte	$2B
	ora	$1B, x
	.byte	$23
	.byte	$1B
	asl	$3023
	asl	$1523
	.byte	$0E
	.byte	$34
L9B1C:	ora	$30, x
	.byte	$34
	ora	$13, x
	.byte	$34
	.byte	$07
	.byte	$13
	.byte	$07
	ora	$13, x
	rol	$0E, x
	and	$0E
	asl	$0E0E
	asl	$0E0E
	asl	$300E
	brk
	ora	$0C27
	.byte	$07
	bmi	L9B57
	ora	$0C30
	.byte	$1C
	asl	$0C
	.byte	$0F
	asl	$30
	.byte	$0F
	.byte	$17
	ora	$21, x
	asl	$060E
	bpl	L9B62
	asl	$0E0E, x
	bmi	L9B60
	asl	$0E0E
	.byte	$0E
	.byte	$0E
L9B57:	bit	$0E06
	bit	$0E30
	rol	$06
	.byte	$30
L9B60:	.byte	$2C
	.byte	$06
L9B62:	.byte	$0C
	bmi	L9B65
L9B65:	ora	$170E
	.byte	$1B
	bmi	L9B83
	ora	$1730
	.byte	$1C
	brk
	ora	$0E30
	asl	$0E0E
	asl	$0E0E
	asl	$270E
	ora	$07, x
	.byte	$1C
	.byte	$07
	asl	$301C
L9B83:	asl	$151C
	asl	$1006
	asl	$3006
	asl	$1526
	bmi	L9B97
	bpl	L9BA8
	.byte	$13
	ora	$30
	.byte	$1A
L9B97:	bit	$050C
L9B9A:	.byte	$1A
	asl	$0E0E
	asl	$0510
	bmi	L9BB3
	ora	$00
	bpl	L9BB7
	brk
L9BA8:	bpl	L9BAF
	brk
	.byte	$0C
	bpl	L9BDE
	.byte	$0C
L9BAF:	ora	$26, x
	bpl	L9BD9
L9BB3:	asl	$2630
	.byte	$0E
L9BB7:	.byte	$1C
	asl	$1E
	asl	$350E
	asl	$0E0E
	asl	$0E0E
	bmi	L9BC5
L9BC5:	ora	$0627
	.byte	$03
	bmi	L9BF0
	ora	$0630
	.byte	$1C
	rol	$16
	asl	$0E0E
	bmi	L9BE4
	asl	$0E0E
L9BD9:	asl	$370E
	.byte	$27
	.byte	$0E
L9BDE:	asl	$250E
	asl	$0E0E
L9BE4:	asl	$0E0E
	and	($1C, x)
	asl	$1C15
	asl	$0C17
	.byte	$21
L9BF0:	and	($15, x)
	asl	$3706
	bmi	L9BFD
	ora	$26, x
	.byte	$37
	rol	$0E
	.byte	$30
L9BFD:	rol	$0E
	asl	$0E0E
	asl	$0E0E
	asl	$0E0E
	asl	$0E0E
	.byte	$27
	.byte	$02
	asl	$3027
	asl	$0630
	bmi	L9C3C
	.byte	$02
	asl	$1A
	.byte	$27
	asl	$2710
	.byte	$1A
	bmi	L9C45
	.byte	$0C
	.byte	$1A
	bpl	L9C31
	ora	$34, x
	and	($15), y
	bmi	L9C50
	.byte	$34
	.byte	$27
	asl	$2630
	asl	$1037
L9C31:	.byte	$0F
	.byte	$37
	bmi	L9C44
	brk
	rol	$1C
	asl	$370E
	.byte	$10
L9C3C:	brk
	asl	a:$22
	asl	$0C15
	.byte	$30
L9C44:	.byte	$10
L9C45:	.byte	$22
	asl	$251C
	asl	$2517
	.byte	$1C
	bmi	L9C65
	.byte	$07
L9C50:	.byte	$1C
	.byte	$17
	asl	a:$10
	asl	$0E0E
	and	$0E
	asl	$0E0E
	asl	$250E
	asl	$0E, x
	.byte	$27
	asl	$0E, x
L9C65:	.byte	$10
L9C66:	.byte	$37
	.byte	$27
	and	$27
	asl	$1017
	asl	$1022
	.byte	$17
	bmi	L9C89
	.byte	$0C
	.byte	$17
	.byte	$22
	asl	$2103
	.byte	$0F
	rol	$21
	.byte	$0C
	bmi	L9C94
	.byte	$0C
	bmi	L9C97
	rol	$21
	.byte	$22
	.byte	$27
	.byte	$17
	.byte	$0C
	.byte	$30
L9C89:	.byte	$07
	ora	$30, x
	and	($27, x)
	ora	$BA, x
	ldy	LBCBC, x
	.byte	$BC
L9C94:	ldy	LBCBC, x
L9C97:	ldy	LBCBC, x
	ldy	LBDBC, x
	.byte	$BB
L9C9E:	.byte	$B2
	.byte	$AF
	lda	LB2AE
	.byte	$B2
	.byte	$B2
	.byte	$B2
	.byte	$B2
	.byte	$B2
	lda	LBEB1
	.byte	$BB
	.byte	$B2
	inc	LB2E1
	.byte	$B2
	.byte	$B2
	.byte	$AF
	bcs	L9C66
	.byte	$B2
	.byte	$B2
	.byte	$B2
	ldx	LB2BB, y
	dec	LB2E2, x
	.byte	$B2
	.byte	$B2
	.byte	$B2
	.byte	$B2
	.byte	$B2
	.byte	$B2
	lda	$B2, x
	ldx	$EAEB, y
	dex
	dec	$E3E5
	.byte	$E3
	.byte	$E3
	.byte	$DF
	cpx	$B3
	ldx	$B8, y
	ldx	$EDEC, y
	cpy	$E6D0
	sbc	#$E9
	.byte	$E7
	cpx	#$E8
	ldy	$B7, x
	lda	$DCBF, y
	cmp	($D2), y
	.byte	$D7
	.byte	$DB
	cmp	$CDCD
	cmp	$CBD3
	.byte	$CF
	.byte	$CF
	.byte	$D4
	cmp	$CDCD, x
	cmp	$CDCD
	cmp	$CDCD
	cmp	$CDCD
	.byte	$D3
	.byte	$D4
	cmp	#$C2
	cpy	$C6
	cmp	$CDCD
	cmp	$CDCD
	dec	$C6
	cmp	$C1D5
	.byte	$C3
	cmp	$C6
	cmp	$CDCD
	cmp	$CDCD
	dec	$C8
	cmp	$D9D5
	cmp	$C6C7
	cmp	$CDCD
	cmp	$C6C6
	cmp	$CDCD
	cmp	$D9, x
	cmp	$C8C6
	cmp	$CDCD
	cmp	$C6C6
	cmp	$CDCD
	cmp	$D9, x
	cmp	$CDCD
	cmp	$CDCD
	cmp	$CDCD
	cmp	$CDCD
	cmp	$D8, x
	.byte	$DA
	.byte	$DA
	.byte	$DA
	.byte	$DA
	.byte	$DA
	.byte	$DA
	.byte	$DA
	.byte	$DA
	.byte	$DA
	.byte	$DA
	.byte	$DA
	.byte	$DA
	dec	$04, x
	.byte	$02
	.byte	$02
	.byte	$03
	.byte	$02
	asl	$08
	.byte	$02
	asl	a
	.byte	$05
L9D5D:	eor	($05, x)
	.byte	$4B
	asl	$4E
	.byte	$07
	.byte	$4F
	pha
	.byte	$64
	.byte	$02
	eor	$4C09
	rti

	.byte	$02
	.byte	$63
	ora	($47, x)
	asl	a
	lsr	a
	ora	$27
	.byte	$4F
	eor	($67, x)
	brk
	cpy	#$00
	pha
	rol	$24, x
	asl	$48
L9D7E:	ora	($80, x)
	adc	$01
	.byte	$44
	ora	$64
	.byte	$02
	pha
	ora	$64
	and	$4D
	pla
	.byte	$02
	pha
	.byte	$33
	and	#$06
	.byte	$47
	brk
	pla
	asl	a
	.byte	$67
	ora	($47, x)
	.byte	$04
	pla
	.byte	$23
	.byte	$4B
	adc	#$31
	pha
	.byte	$32
	bit	$4408
L9DA3:	adc	$24
	.byte	$32
	ora	$69
	ora	($45, x)
	.byte	$04
	adc	#$24
	lsr	a
	pla
	.byte	$32
	lsr	$32
	plp
	.byte	$67
	.byte	$07
	.byte	$43
	.byte	$64
	bit	$34
	.byte	$04
	adc	#$03
	eor	($04, x)
	.byte	$64
	.byte	$42
	.byte	$64
	.byte	$23
	eor	#$68
	.byte	$32
	lsr	$32
	.byte	$27
	.byte	$6B
	asl	$42
	.byte	$62
	bit	$37
	.byte	$02
	jmp	($6308)

	.byte	$44
	.byte	$62
	.byte	$23
	pha
	.byte	$32
	ror	$32
	lsr	$2A
	adc	$4205
L9DDE:	and	$34
	.byte	$63
	.byte	$33
	.byte	$6B
	php
	adc	$42
	rol	$48
	.byte	$32
	pla
	.byte	$32
	lsr	$28
	adc	$34
	adc	$04
	eor	($23, x)
	.byte	$33
	pla
	.byte	$32
	jmp	($6505)

	.byte	$42
	.byte	$27
	eor	#$32
	pla
	.byte	$32
	.byte	$44
L9E00:	.byte	$27
	adc	$32
	.byte	$62
	.byte	$32
	adc	$02
	.byte	$41
L9E08:	.byte	$23
	and	($6B), y
	and	($65), y
	.byte	$13
	.byte	$64
	.byte	$04
	ror	$27
	pha
	.byte	$62
	.byte	$32
	adc	#$31
	.byte	$43
	.byte	$27
	.byte	$64
	and	($65), y
	and	($66), y
	.byte	$02
	rti

	and	($02, x)
	.byte	$6B
	.byte	$32
	.byte	$64
	ora	$65, x
	.byte	$04
	adc	$25
	pha
	.byte	$62
	.byte	$33
	ror	a
	and	($43), y
	rol	$69
	.byte	$80
	.byte	$62
	and	($67), y
	eor	($03, x)
	.byte	$6B
	.byte	$32
	.byte	$62
	.byte	$17
	adc	$04
	ror	$22
	eor	#$63
	.byte	$33
	adc	#$31
	.byte	$44
	rol	$6F
	.byte	$67
	.byte	$42
	.byte	$04
	adc	#$33
	adc	($13, x)
	bcc	L9E63
	.byte	$64
	ora	$66
	bit	$46
	.byte	$64
	.byte	$33
	ror	a
	bmi	L9E9F
	.byte	$27
	.byte	$6F
	.byte	$67
	.byte	$43
	.byte	$07
	ror	$33
	.byte	$61
L9E63:	asl	$65, x
	ora	$66
	bit	$44
	ror	$35
	.byte	$67
	and	($43), y
	and	#$6F
	adc	$44
	.byte	$63
	.byte	$04
	adc	$33
	.byte	$62
	.byte	$13
	.byte	$67
	.byte	$03
	.byte	$67
	.byte	$23
	ora	($42, x)
	pla
	and	$66, x
	.byte	$32
	.byte	$43
	rol	$03
	.byte	$6F
	.byte	$63
	.byte	$43
	.byte	$64
	ora	$66
	.byte	$32
	jmp	($6705)

	and	($02, x)
	eor	($66, x)
	asl	$32
	ror	$32
	.byte	$43
	bit	$05
	.byte	$6F
	eor	$61
	asl	$65
L9E9F:	.byte	$03
	jmp	($6904)

	.byte	$03
	eor	($63, x)
	.byte	$0B
	.byte	$33
	.byte	$64
	.byte	$32
	.byte	$44
	adc	($06, x)
	.byte	$6B
	lsr	$42
	adc	$06
	.byte	$63
	.byte	$04
	.byte	$6B
	.byte	$04
	adc	#$05
	eor	($0F, x)
	brk
	.byte	$32
	.byte	$64
	.byte	$32
	eor	($62, x)
	asl	$67
	.byte	$33
	.byte	$47
	eor	($68, x)
	.byte	$0C
	jmp	($6705)

	.byte	$07
	eor	($0F, x)
	brk
	.byte	$33
	.byte	$63
	.byte	$42
	.byte	$63
	asl	$63
	and	$63, x
	eor	$41
	pla
	.byte	$0C
	adc	$6505
	ora	#$B0
	ora	$65
	ora	$33
	pla
	asl	$61
	rol	$62, x
	.byte	$13
	.byte	$44
	rti

	ror	a
	ora	$066B
	.byte	$62
	.byte	$0B
	eor	($03, x)
	.byte	$67
	ora	$39
	adc	($40, x)
	.byte	$04
	adc	($34, x)
	.byte	$64
	asl	$42, x
	.byte	$6B
	asl	$0F69
	.byte	$07
	rti

	.byte	$02
	adc	#$04
	adc	($38, x)
	eor	($04, x)
	rts

	and	$1C, x
	eor	($6C, x)
	.byte	$0B
	.byte	$23
	ror	$0F
	php
	rti

	ora	($64, x)
	.byte	$23
	.byte	$62
	.byte	$02
	ror	$22
	.byte	$44
	.byte	$04
	.byte	$34
	.byte	$1F
	adc	$4206
	rol	$63
	asl	$0663
	eor	($63, x)
	and	$63
	.byte	$02
	.byte	$64
	bit	$43
	ora	$32
	ora	$1461, y
	ror	$4603
	and	$63
	ora	#$68
	asl	$41
	.byte	$62
	rol	$63
	.byte	$03
	adc	($26, x)
	.byte	$43
	.byte	$07
	clc
	.byte	$63
	.byte	$13
	.byte	$6F
	ora	($48, x)
	bit	$63
	.byte	$07
	adc	#$22
	ora	$41
	.byte	$62
	rol	$63
	ora	$26
	.byte	$43
	asl	$18
	.byte	$63
	.byte	$13
	.byte	$6F
	rts

	eor	#$23
	.byte	$63
	.byte	$07
	adc	#$25
	.byte	$04
	eor	($62, x)
	bit	$64
	asl	$66
	.byte	$42
	asl	$19
	adc	($14, x)
	eor	($6E, x)
	pha
	.byte	$32
	adc	$06
	adc	#$27
	.byte	$04
	.byte	$44
	adc	#$04
	ror	$44
	asl	$1E
	.byte	$42
	jmp	($3648)

	adc	($05, x)
	.byte	$6B
	plp
	asl	$44
	.byte	$67
	.byte	$04
	.byte	$64
	.byte	$44
	php
	.byte	$1C
	.byte	$47
	ror	$45
	.byte	$3B
	asl	$6C
	.byte	$27
	.byte	$03
	.byte	$33
	.byte	$44
	ror	$05
L9F9E:	.byte	$62
	.byte	$44
	asl	a
	.byte	$1A
	.byte	$43
	.byte	$22
	.byte	$42
	.byte	$63
	.byte	$44
	rol	$6804, x
	and	$25, x
	.byte	$02
	.byte	$34
	lsr	$66
	ora	$45
	ora	$4416
	.byte	$42
	and	$B0
	adc	($42, x)
	.byte	$3F
	bmi	L9FC2
	ror	$39
	and	$35
	pha
L9FC2:	adc	$06
	eor	$0C
	and	$45
	eor	($26, x)
	.byte	$44
	.byte	$23
	.byte	$3B
	php
	.byte	$62
	and	$3624, x
	lsr	$65
	.byte	$07
	lsr	$09
	and	#$43
	eor	($2F, x)
	.byte	$22
	rol	$0B, x
	rol	$23, x
	and	$62, x
	sec
	.byte	$44
	.byte	$67
	.byte	$07
	.byte	$44
	ora	#$27
	.byte	$63
	.byte	$42
	eor	($2F, x)
	bit	$33
	.byte	$0B
	.byte	$33
	rol	a
	and	($69), y
	.byte	$33
	.byte	$42
	adc	#$07
	.byte	$42
	ora	#$26
	ror	$42
	and	#$03
	rol	$31
	ora	$312E
	.byte	$64
	.byte	$14
	rol	$67, x
	php
	eor	($09, x)
	rol	$68
	.byte	$43
	.byte	$22
	.byte	$63
	.byte	$07
	bit	$41
	.byte	$0C
	ror	$27
	.byte	$33
	adc	($18, x)
	and	$65, x
	php
	eor	($04, x)
	.byte	$33
	.byte	$27
	ror	a
	eor	($67, x)
	ora	#$22
	.byte	$43
	asl	a
	ror	a
	and	$33
	clc
	.byte	$72
	pla
	ora	#$B0
	.byte	$02
	adc	($35, x)
	and	$6A
	rti

	ror	$0A
	.byte	$23
	.byte	$44
	.byte	$07
	adc	$4327
	.byte	$14
	.byte	$74
	pla
	ora	$43
	.byte	$64
	rol	$23, x
	.byte	$6B
	rti

	ror	$09
	.byte	$64
	.byte	$44
	.byte	$07
	adc	$2303
	.byte	$47
	.byte	$77
	ror	$03
	lsr	$67
	.byte	$34
	and	($6B, x)
	eor	($64, x)
	php
	ror	$45
	asl	$6A
	ora	#$48
	.byte	$77
	ror	$4C
	.byte	$63
	.byte	$73
	.byte	$33
	jmp	($6342)

	asl	$67
	pha
	asl	$66
	ora	$80
	.byte	$04
	.byte	$4B
	.byte	$74
	adc	$4F
	rti

	.byte	$77
	.byte	$34
	.byte	$65
LA07A:	lsr	$64
	.byte	$04
	.byte	$64
	eor	#$0A
	.byte	$63
	php
	.byte	$4F
	.byte	$43
	.byte	$64
	.byte	$4F
	lsr	$75
	rol	$49, x
	rti

	.byte	$64
	.byte	$02
	.byte	$63
	lsr	a
	.byte	$0F
	.byte	$02
	ldy	#$02
	.byte	$4F
	.byte	$4F
	.byte	$4F
	.byte	$43
	ror	$32, x
	lsr	a
	eor	($69, x)
	pha
	.byte	$0F
	.byte	$07
	.byte	$4F
	.byte	$4F
	.byte	$4F
	eor	#$70
	bcc	LA116
	and	($4B), y
	eor	($68, x)
	pha
	adc	$0F
	brk
	.byte	$47
	.byte	$32
	.byte	$4F
	lsr	$4F65
	jmp	$6843

	eor	$67
	ora	$3547
	.byte	$4F
	.byte	$47
	ror	a
	.byte	$04
	.byte	$4F
	.byte	$43
	.byte	$23
	.byte	$42
	ror	a
	.byte	$47
	ror	$0A
	lsr	$72
	rol	$4F, x
	rti

	ror	$4F08
	and	$41
	jmp	($6747)

	.byte	$07
	lsr	$30
	bvs	LA07A
	bvs	LA113
	jmp	$6D13

	.byte	$0B
	.byte	$4B
	rol	$42
	ror	a
	eor	#$67
	.byte	$07
	.byte	$44
	and	($72), y
	ora	($36), y
	eor	($12, x)
	rti

	.byte	$12
	eor	($16, x)
	jmp	($700C)

	bcc	LA167
	eor	$27
	.byte	$42
	adc	#$33
	.byte	$44
	ror	a
	asl	$43
	rol	$11, x
	.byte	$33
	ora	$43, x
	.byte	$1A
	adc	$720A
	.byte	$44
	.byte	$63
	bit	$44
	adc	$3C
	.byte	$67
	.byte	$07
	.byte	$44
	.byte	$33
LA113:	.byte	$13
	.byte	$32
	.byte	$14
LA116:	and	($43), y
	and	($19), y
	.byte	$6F
	rts

	ora	$25
	eor	($69, x)
	.byte	$43
	ora	($61, x)
	.byte	$3F
	bmi	LA18D
	ora	$44
	.byte	$33
	.byte	$13
	.byte	$34
	.byte	$12
	and	($45), y
	.byte	$32
	ora	$326E, y
	and	#$6A
	eor	($03, x)
	.byte	$3F
	.byte	$33
	.byte	$67
	.byte	$03
	lsr	$31
	ora	$34, x
	bpl	LA171
	.byte	$47
	.byte	$33
	clc
	ror	$43
	and	$28, x
	.byte	$6B
	rti

	.byte	$03
	.byte	$33
	ror	$3C
	.byte	$64
	.byte	$22
	.byte	$47
	.byte	$32
	ora	$33, x
	and	($31, x)
	pha
	.byte	$32
	.byte	$17
	.byte	$64
	lsr	$36
	.byte	$27
	ror	a
	.byte	$03
	.byte	$32
	adc	#$3D
	.byte	$63
	.byte	$22
	.byte	$47
	and	($15), y
	.byte	$32
LA167:	and	($32, x)
	eor	#$33
	.byte	$14
	.byte	$63
	.byte	$47
	.byte	$3C
	.byte	$22
	ror	a
LA171:	.byte	$03
	ror	$73
	.byte	$63
	.byte	$3C
	.byte	$63
	.byte	$22
	lsr	$31
	.byte	$14
	.byte	$32
	.byte	$22
	and	($4C), y
	.byte	$32
	bit	$63
	.byte	$43
	.byte	$33
	.byte	$64
	.byte	$37
	.byte	$6B
	rti

	.byte	$02
	adc	$76
	.byte	$64
	.byte	$33
LA18D:	.byte	$03
	bcc	LA1C1
	.byte	$62
	.byte	$23
	lsr	$32
	.byte	$12
	.byte	$32
	.byte	$23
	and	($4C), y
	and	($27), y
	.byte	$6F
	and	$4266, y
	.byte	$03
	.byte	$63
	.byte	$77
	adc	$05
	.byte	$32
	.byte	$27
	.byte	$47
	and	($11), y
	.byte	$32
	and	$31
	lsr	a
	.byte	$32
	.byte	$27
	.byte	$6F
	sec
	.byte	$12
	.byte	$63
	.byte	$43
	.byte	$02
	adc	$75
	ror	$04
	.byte	$32
	.byte	$27
	pha
	and	($70), y
	and	$20, x
	.byte	$34
LA1C1:	.byte	$4B
	and	($25), y
	php
	.byte	$67
	.byte	$37
	ora	$61, x
	.byte	$44
	.byte	$02
	ror	$73
	ror	$05
	.byte	$33
	and	$48
	and	($71), y
	.byte	$34
	.byte	$02
	.byte	$32
	eor	$2331
	ora	$176A
	adc	($43, x)
	.byte	$02
	.byte	$6F
	rts

	rol	$314B, x
	adc	($32), y
	.byte	$02
	.byte	$32
	eor	$2232
	.byte	$0F
	pla
	.byte	$17
	.byte	$63
	.byte	$42
	.byte	$03
	adc	$633A
	lsr	$7131
	.byte	$32
	.byte	$02
	.byte	$32
	eor	$2232
	.byte	$0E
LA1FF:	adc	#$15
	.byte	$64
	.byte	$42
	rti

	.byte	$03
	pla
	.byte	$22
	and	$68, x
	ora	($4D, x)
	and	($73), y
	and	($02), y
	and	($4E), y
	.byte	$32
	.byte	$27
	.byte	$62
	.byte	$04
	.byte	$6B
	.byte	$13
	ror	$41
	.byte	$03
	ror	$25
	.byte	$32
	pla
	.byte	$04
	jmp	$7531

	.byte	$32
	.byte	$4F
	rti

	and	($26), y
	.byte	$64
	.byte	$02
	.byte	$63
	.byte	$37
	jmp	($343)

	.byte	$67
	bit	$68
	asl	$4C
	.byte	$32
	adc	($32), y
	.byte	$4F
	eor	$24
	ror	a
	.byte	$32
	.byte	$04
	and	$69, x
	eor	$03
	ror	$25
	.byte	$64
	.byte	$07
	.byte	$4F
	.byte	$33
	.byte	$4F
	eor	#$6C
	.byte	$32
	php
	.byte	$33
	pla
	lsr	$04
	adc	$22
	adc	$07
	.byte	$4F
	.byte	$4F
	.byte	$4F
	rti

	ror	a
	and	($02), y
	.byte	$64
	.byte	$02
	.byte	$34
	ror	$47
	ora	$6B
	asl	$44
	ror	$4F
	.byte	$4F
	pha
	.byte	$67
	.byte	$04
	ror	$03
	.byte	$32
	ror	$48
	ora	$69
	.byte	$04
	eor	$66
	.byte	$4F
	.byte	$4F
	pha
	pla
	.byte	$04
	adc	($44, x)
	adc	($03, x)
	.byte	$32
	adc	$4A
	ora	$67
	ora	$43
	ora	($64, x)
	.byte	$4F
	.byte	$4F
	.byte	$47
	adc	#$04
	adc	($46, x)
	adc	($02, x)
	and	($66), y
	.byte	$4B
	asl	$67
	asl	a
	.byte	$62
	.byte	$4F
	.byte	$4F
	eor	#$69
	.byte	$02
	adc	($42, x)
	.byte	$02
	.byte	$42
	adc	($01, x)
	.byte	$32
	adc	$4D
	ora	$68
	ora	#$4F
	.byte	$4F
	lsr	$0167
	adc	($42, x)
	brk
	.byte	$80
	brk
	.byte	$12
	adc	($01, x)
	and	($66), y
	eor	$35
	pha
	.byte	$6B
	.byte	$03
	.byte	$4F
	.byte	$4F
	.byte	$4F
	jmp	($242)

	.byte	$42
	adc	($01, x)
	and	($66), y
	.byte	$44
	.byte	$32
	adc	$48
	ror	a
	.byte	$03
	.byte	$4F
	.byte	$4F
	.byte	$4F
	jmp	($6146)

	ora	($31, x)
	ror	$43
	.byte	$32
	adc	#$44
	ror	a
	ora	$4F
	lsr	$4837
	jmp	($6244)

	.byte	$33
	adc	$42
	.byte	$32
	.byte	$67
	.byte	$03
	eor	$67
	ora	#$4F
	eor	#$34
	asl	$48, x
	pla
	.byte	$34
	ror	$3241
	adc	$09
	.byte	$43
	ror	$0A
	.byte	$4F
	.byte	$47
	.byte	$17
	.byte	$33
	ora	($46), y
	ror	a
	.byte	$34
	jmp	($4042)

	.byte	$32
	adc	$0C
	rti

	ror	$05
	adc	($04, x)
	.byte	$4F
	eor	$3D
	ora	($46), y
	.byte	$6B
	.byte	$33
	.byte	$22
	.byte	$67
	ora	($41, x)
	.byte	$32
	.byte	$63
	php
	.byte	$63
	.byte	$02
	bcs	LA384
	.byte	$04
	.byte	$63
	.byte	$04
	eor	$1539
	and	$10, x
	pha
	ror	a
	.byte	$32
	.byte	$23
	adc	$04
	.byte	$32
	.byte	$62
	.byte	$07
	pla
	eor	($64, x)
	.byte	$04
	adc	$03
	lsr	a
	.byte	$37
	ora	$1032, x
	and	($45), y
	adc	$2231
	.byte	$64
	ora	$31
	.byte	$64
	.byte	$04
	.byte	$6B
	eor	($63, x)
	.byte	$03
	.byte	$67
	.byte	$03
	.byte	$47
	sec
	ora	($3B), y
	ora	($31), y
	bpl	LA383
	lsr	$6D
	and	($22), y
	.byte	$64
	.byte	$04
	.byte	$64
	ora	$21
	ror	a
	.byte	$33
	.byte	$62
	.byte	$03
	adc	$04
	lsr	$38
	ora	($33), y
	.byte	$17
	and	($10), y
	and	($10), y
	.byte	$32
	.byte	$47
	jmp	($2131)

	.byte	$64
	.byte	$04
	.byte	$63
	ora	$23
	pla
	.byte	$37
	.byte	$04
	.byte	$63
	ora	$47
	.byte	$37
	bpl	LA3B5
	.byte	$12
	.byte	$32
	ora	($30), y
	bpl	LA3B6
LA383:	.byte	$46
LA384:	jmp	($2132)

	.byte	$64
	.byte	$03
	.byte	$64
	.byte	$03
	.byte	$23
	pla
	.byte	$3A
	.byte	$0B
	lsr	a
	and	($14, x)
	and	$3410, y
	.byte	$13
	.byte	$32
	.byte	$47
	jmp	($2231)

	.byte	$64
	ora	($40, x)
	.byte	$64
	ora	($24, x)
	pla
	and	$4A07, x
	bit	$32
	ora	($38), y
	bpl	LA3E2
	rts

	.byte	$33
	.byte	$47
	jmp	($6731)

	eor	($64, x)
	and	$64
LA3B5:	.byte	$34
LA3B6:	asl	$36, x
	.byte	$03
	pha
	.byte	$62
	rol	$32
	.byte	$14
	.byte	$34
	bpl	LA3F7
	.byte	$62
	.byte	$32
	lsr	a
	.byte	$67
	.byte	$23
	.byte	$67
	.byte	$42
	.byte	$64
	and	$61
	.byte	$32
	.byte	$1B
	.byte	$34
	jmp	$2464

	and	$6416, x
	.byte	$32
	.byte	$4B
	.byte	$64
	bit	$67
	eor	($65, x)
	plp
	asl	$4B32, x
	adc	$25
	.byte	$36
LA3E2:	.byte	$02
	and	$3166, y
	jmp	$2365

	adc	$42
	pla
	rol	$18
	.byte	$80
	.byte	$14
	and	($4B), y
	adc	$27
	and	($62), y
	.byte	$03
LA3F7:	.byte	$43
	.byte	$37
	.byte	$64
	and	($4E), y
	adc	$23
	.byte	$62
	.byte	$44
	ror	a
	bit	$1D
	eor	$6C
	plp
	and	($64), y
	.byte	$04
	.byte	$42
	.byte	$74
	.byte	$33
	adc	($31, x)
	eor	$2566
	.byte	$62
	.byte	$43
	rti

	.byte	$6B
	and	($1B, x)
	lsr	$66
	lsr	$26
	.byte	$33
	adc	$04
	bcs	LA494
	.byte	$03
	.byte	$32
	rts

	.byte	$4F
	rti

	ror	$23
	.byte	$64
	.byte	$44
	pla
	.byte	$1A
	lsr	$63
	.byte	$43
	adc	$26
	.byte	$32
	adc	#$02
	eor	($73, x)
	ora	$31
	ora	($4F, x)
	ror	$22
	.byte	$64
	lsr	a
	adc	($1C, x)
	.byte	$44
	.byte	$67
	.byte	$47
	bit	$31
	ror	a
	.byte	$43
	.byte	$74
	ora	$33
	ora	($4F, x)
	.byte	$63
	.byte	$23
	.byte	$64
	.byte	$47
	asl	$6E49, x
	.byte	$22
	and	($67), y
	lsr	$74
	ora	$34
	brk
	eor	$4161
	.byte	$63
	.byte	$23
	.byte	$64
	.byte	$43
	.byte	$1F
	.byte	$14
	.byte	$44
	.byte	$67
	.byte	$43
	ror	$32
	.byte	$64
	.byte	$4B
	.byte	$72
	asl	$61
	and	($02), y
	.byte	$4B
	.byte	$63
	rti

	.byte	$64
	and	($64, x)
	.byte	$44
	eor	($15, x)
	and	$1A
	.byte	$64
	jmp	$3263

	.byte	$64
	jmp	$620A

	and	($02), y
	eor	#$64
	eor	($69, x)
	eor	$42
	.byte	$12
	.byte	$23
	.byte	$64
	clc
	ror	$6242
	and	($66), y
	lsr	a
LA494:	.byte	$63
	php
	.byte	$64
	.byte	$32
	ora	($4A, x)
	.byte	$63
	eor	($68, x)
	eor	$41
	and	$69
	.byte	$14
	.byte	$6F
	rts

	bcs	LA507
	.byte	$32
	ror	$46
	adc	#$06
	.byte	$67
	and	($01), y
	pha
	adc	$41
	ror	$46
	rti

	bit	$6D
	.byte	$13
	adc	$3143
	.byte	$63
	.byte	$03
	eor	#$68
	ora	$66
	and	($02), y
	pha
	adc	($70, x)
	.byte	$63
	bcs	LA52D
	.byte	$47
	rol	$6D
	.byte	$13
	ror	a
	pha
	ora	$49
	adc	#$51
	brk
	eor	($66), y
	.byte	$33
	.byte	$02
	lsr	$61
	.byte	$72
	.byte	$62
	eor	($63, x)
	pha
	bit	$33
	.byte	$6B
	.byte	$14
	ror	$4A
	ora	$4B
	pla
	bvc	LA4EB
	bvc	LA551
LA4EB:	.byte	$34
	.byte	$02
	eor	$62
	.byte	$72
	adc	($41, x)
	.byte	$62
	eor	#$25
	.byte	$34
	pla
	asl	$65, x
	and	($43), y
	.byte	$34
	ora	$4D
	ror	$50
	brk
	.byte	$80
	brk
	bvc	LA56C
	.byte	$34
	.byte	$22
LA507:	eor	$62
	adc	($62), y
	eor	$3427
	ror	$17
	.byte	$64
	.byte	$32
	eor	($33, x)
	adc	($04, x)
	eor	($03, x)
	lsr	a
	.byte	$64
	bvc	LA51E
	bvc	LA587
LA51E:	and	($24), y
	.byte	$44
	adc	($03, x)
	.byte	$62
	jmp	$3826

	.byte	$63
	.byte	$17
	and	($63, x)
	rol	$63, x
LA52D:	.byte	$03
	bcs	LA534
	.byte	$64
	lsr	$63
	.byte	$54
LA534:	pla
	.byte	$32
	and	$44
	adc	($02, x)
	.byte	$23
	.byte	$02
	pha
	rol	$31
	.byte	$64
	rol	$14, x
	and	$62
	.byte	$34
	.byte	$64
	.byte	$02
	eor	($03, x)
	adc	#$33
	.byte	$6F
	.byte	$32
	and	$47
	ora	($26, x)
LA551:	ora	$43
	rti

	bit	$31
	.byte	$67
	rol	$11, x
	.byte	$27
	and	$66, x
	.byte	$42
	.byte	$62
	.byte	$04
	.byte	$67
LA560:	and	$69, x
	and	$24, x
	eor	#$22
	.byte	$34
	.byte	$22
	.byte	$04
	.byte	$42
	.byte	$22
	.byte	$31
LA56C:	.byte	$64
	.byte	$04
	.byte	$33
	.byte	$2B
	and	$64, x
	.byte	$42
	.byte	$64
	ora	$67
	.byte	$37
	.byte	$63
	rol	$26, x
	.byte	$47
	.byte	$22
	.byte	$32
	adc	($32, x)
	and	($03, x)
	.byte	$44
	and	($31, x)
	.byte	$62
	asl	$2C
LA587:	rol	$64, x
	.byte	$42
	ror	$05
	.byte	$67
	.byte	$3F
	bmi	LA5B8
	eor	$22
	.byte	$32
	.byte	$63
	and	($22), y
	brk
	.byte	$43
	.byte	$23
	.byte	$32
	asl	$2C
	rol	$64, x
	.byte	$43
	adc	$08
	.byte	$64
	.byte	$72
	.byte	$3A
	.byte	$74
	and	$46
	.byte	$22
	.byte	$32
	.byte	$62
	cpy	#$61
	and	($01), y
	.byte	$44
	.byte	$42
	bit	$31
	php
	and	#$35
	.byte	$67
	.byte	$44
	.byte	$63
LA5B8:	ora	#$62
	.byte	$77
	rol	$76, x
	and	$46
	.byte	$22
	and	($64), y
	.byte	$33
	.byte	$03
	.byte	$43
	.byte	$23
	and	($09), y
	.byte	$27
	and	$68, x
	.byte	$43
	.byte	$64
	ora	#$62
	sei
	.byte	$34
	sei
	.byte	$23
	pha
	.byte	$22
	and	($63), y
	and	($22), y
	.byte	$02
	.byte	$44
	bit	$09
	plp
	.byte	$33
	pla
	eor	$64
	.byte	$07
	.byte	$62
	.byte	$7A
	.byte	$32
	.byte	$7A
	and	($49, x)
	.byte	$22
	.byte	$32
	rts

	.byte	$32
	.byte	$22
	.byte	$02
	lsr	$23
	.byte	$07
	.byte	$27
	.byte	$34
	pla
	lsr	$66
	ora	$63
	.byte	$7F
	sei
	.byte	$4B
	.byte	$22
	and	($60), y
	and	($22), y
	.byte	$02
	eor	$25
	ora	$23
	.byte	$42
	.byte	$37
	ror	$48
	.byte	$67
	.byte	$02
	.byte	$64
	.byte	$7A
	eor	($7A, x)
	jmp	$328

	lsr	$25
	.byte	$03
	.byte	$23
	.byte	$44
	jmp	($6C4B)

	.byte	$7A
	.byte	$43
	sei
	eor	$7225
	pha
	.byte	$47
	and	$6B42
	.byte	$4F
	adc	#$7A
	.byte	$47
	.byte	$73
	.byte	$4F
	rti

	.byte	$23
	adc	($4A), y
	pha
	bit	$25B0
	.byte	$64
	.byte	$4F
	.byte	$44
	.byte	$64
	.byte	$7C
	.byte	$4F
	lsr	$7121
	.byte	$4B
	lsr	a
	and	#$42
	rol	$4F
	.byte	$4B
	.byte	$7C
	.byte	$4F
	.byte	$4F
	.byte	$73
	.byte	$4B
	rol	$4F
	.byte	$4F
	pha
	adc	$4F4F, y
	eor	($72, x)
	.byte	$4C
LA653:	.byte	$5D
LA654:	sta	$9D6A, x
	ror	$929D, x
	sta	L9DA3, x
	ldx	$9D, y
	.byte	$CB
	sta	L9DDE, x
	.byte	$F2
	sta	L9E08, x
	jsr	$369E
	.byte	$9E
	eor	#$9E
	lsr	$719E, x
	.byte	$9E
	.byte	$87
	.byte	$9E
	.byte	$9B
	.byte	$9E
	.byte	$AF
	.byte	$9E
	cpy	$9E
	.byte	$D7
	.byte	$9E
	nop
	.byte	$9E
	inc	$0F9E, x
	.byte	$9F
	and	($9F, x)
	rol	$9F, x
	lsr	a
	.byte	$9F
	lsr	$729F, x
	.byte	$9F
	.byte	$82
	.byte	$9F
	.byte	$92
	.byte	$9F
	ldx	#$9F
	lda	$9F, x
	iny
	.byte	$9F
	cmp	$EB9F, y
	.byte	$9F
	.byte	$FC
	.byte	$9F
	ora	$21A0
	ldy	#$34
	ldy	#$47
	ldy	#$59
	ldy	#$69
	ldy	#$7B
	ldy	#$8A
	ldy	#$9A
	ldy	#$A8
	ldy	#$B5
	ldy	#$C3
	ldy	#$D1
	ldy	#$E2
	ldy	#$F9
	ldy	#$0C
	lda	($20, x)
	lda	($34, x)
	lda	($48, x)
	lda	($5D, x)
	lda	($71, x)
	lda	($88, x)
	lda	($9F, x)
	lda	($B4, x)
	lda	($CA, x)
	lda	($DF, x)
	lda	($F1, x)
	lda	($03, x)
	ldx	#$18
	ldx	#$2C
	ldx	#$3E
	ldx	#$4D
	ldx	#$5E
	ldx	#$6D
	ldx	#$7E
	ldx	#$90
	ldx	#$A2
	ldx	#$B5
	ldx	#$C5
	ldx	#$D4
	ldx	#$E3
	ldx	#$F2
	ldx	#$03
	.byte	$A3
	.byte	$17
	.byte	$A3
	bit	$41A3
	.byte	$A3
	cli
	.byte	$A3
	adc	($A3), y
	.byte	$89
	.byte	$A3
	.byte	$9E
	.byte	$A3
	lda	($A3), y
	.byte	$C7
	.byte	$A3
	cmp	$EBA3, y
	.byte	$A3
	brk
	ldy	$13
	ldy	$28
	ldy	$3C
	ldy	$4E
	ldy	$60
	ldy	$76
	ldy	$89
	ldy	$9E
	ldy	$B3
	ldy	$C9
	ldy	$DE
	ldy	$F4
	ldy	$0C
	lda	$25
	lda	$3D
	lda	$53
	lda	$69
	lda	$81
	lda	$97
	lda	$AF
	lda	$C4
	lda	$DA
	lda	$EF
	lda	$02
	ldx	$13
	ldx	$22
	ldx	$30
	ldx	$3E
	ldx	$48
	.byte	$A6
LA743:	lda	$600A
	cmp	#$27
	beq	LA76D
	lda	#$FF
	sta	$3D
	lda	L9A24
	sta	$3E
	lda	L9A25
	sta	$3F
	lda	L9A1A
	clc
	adc	$16
	sta	$40
	lda	L9A1B
	adc	#$00
	sta	$41
	jsr	LC212
	jmp	LA788

LA76D:	lda	#$FF
	sta	$3D
	lda	LBD3F
	sta	$40
	lda	LBD40
	sta	$41
	lda	LBD4D
	sta	$3E
	lda	LBD4E
	sta	$3F
	jsr	LC212

	.export	LA788
LA788:
	lda	#$00
	sta	$3C
	lda	#$04
	sta	$3D
LA790:	ldy	#$00
	lda	#$FF
LA794:	sta	($3C), y
	iny
	bne	LA794
	inc	$3D
	lda	$3D
	cmp	#$08
	bne	LA790
	rts
; -----------------------------------------------------------
	.export	LA7A2
LA7A2:
	sta	$23
	brk
	brk
	.byte	$17
	lda	$6008
	lsr	a
	ora	#$10
	sta	$64A6
	lda	$6007
	sec
	sbc	#$01
	asl	a
	asl	a
	asl	a
	asl	a
	adc	$6009
	sta	$64A7
	lda	$6009
	and	#$0F
	sta	$3C
	sec
	sbc	#$08
	asl	a
	sta	$9D
	lda	$6007
	sta	$98
	sec
	sbc	#$01
	pha
	lda	$6009
	lsr	a
	lsr	a
	lsr	a
	lsr	a
	sta	$22
	pla
	clc
	adc	$22
	sta	$22
	sec
	sbc	#$07
	asl	a
	sta	$10
	lda	$6008
	lsr	a
	sta	$9E
	lda	$22
	sta	$3E
	jsr	LC596
	lda	$0A
	sta	$99
	lda	$0B
	sta	$9A
	lda	$23
LA802:	beq	LA818
	cmp	#$02
	beq	LA818
	cmp	#$03
	beq	LA818
	cmp	#$04
	beq	LA818
	cmp	#$0B
	beq	LA818
	lda	#$00
	beq	LA81A
LA818:	lda	#$FF
LA81A:	sta	$4D
LA81C:	lda	#$00
	sta	$23
	sta	$22
	lda	$9D
	sta	$0F
	lda	$9E
	sta	$97
LA82A:	jsr	LA880
	lda	$99
	clc
	adc	#$02
	sta	$99
	bcc	LA838
	inc	$9A
LA838:	inc	$22
	inc	$22
	inc	$23
	inc	$0F
	inc	$0F
	dec	$97
	bne	LA82A
	brk
	ora	($17, x)
	lda	$99
	clc
	adc	#$C0
	sta	$99
	bcs	LA854
	dec	$9A
LA854:	lda	$9E
	asl	a
	sta	$3C
	lda	$99
	sec
	sbc	$3C
	sta	$99
	bcs	LA864
	dec	$9A
LA864:	lda	$64A7
	sec
	sbc	#$10
	sta	$64A7
	dec	$10
	dec	$10
	dec	$98
	bne	LA81C
	lda	$96
	beq	LA87F
	jsr	LFF74
	jsr	LB6DA
LA87F:	rts

LA880:	lda	$4D
	bne	LA893
	ldy	#$00
	lda	($99), y
	cmp	#$FF
	beq	LA893
	cmp	#$FE
	beq	LA893
	jmp	LA8AD

LA893:	lda	#$00
	sta	$4C
	sta	$D1
	jsr	LA921
	ldy	#$00
	lda	#$FF
	sta	($99), y
	iny
	sta	($99), y
	ldy	#$20
	sta	($99), y
	iny
	sta	($99), y
	rts

LA8AD:	lda	$4B
	asl	a
	adc	$10
	clc
	adc	#$1E
	sta	$3C
	lda	#$1E
	sta	$3E
	jsr	LC1F0
	lda	$40
	sta	$3E
	sta	$49
	lda	$4A
	asl	a
	clc
	adc	$0F
	and	#$3F
	sta	$3C
	sta	$48
	jsr	LC5AA
	ldx	$22
	ldy	#$00
	lda	($99), y
	sta	$6436, x
	iny
	lda	($99), y
	sta	$6437, x
	txa
	clc
	adc	$6008
	tax
	ldy	#$20
	lda	($99), y
	sta	$6436, x
	iny
	lda	($99), y
	sta	$6437, x
	lda	$48
	sta	$3C
	lda	$49
	sta	$3E
	ldy	#$00
	lda	($99), y
	cmp	#$C1
	bcs	LA909
	lda	#$00
	beq	LA91B
LA909:	cmp	#$CA
	bcs	LA911
	lda	#$01
	bne	LA91B
LA911:	cmp	#$DE
	bcs	LA919
	lda	#$02
	bne	LA91B
LA919:	lda	#$03
LA91B:	ldx	$23
	sta	$6496, x
	rts

LA921:	lda	$4B
	asl	a
	clc
	adc	$10
	clc
	adc	#$1E
	sta	$3C
	lda	#$1E
	sta	$3E
	jsr	LC1F0
	lda	$40
	sta	$3E
	sta	$49
	lda	$4A
	asl	a
	clc
	adc	$0F
	and	#$3F
	sta	$3C
	sta	$48
	jsr	LC5AA
	lda	$0F
	asl	a
	lda	$0F
	ror	a
	clc
	adc	$3A
	sta	$3C
	lda	$10
	asl	a
	lda	$10
	ror	a
	clc
	adc	$3B
	sta	$3E
	jsr	LAC17
	lda	$16
	cmp	#$20
	bne	LA9CD
	lda	$0F
	bpl	LA975
	eor	#$FF
	clc
	adc	#$01
	sta	$3E
	jmp	LA979

LA975:	lda	$0F
	sta	$3E
LA979:	lda	$D0
	cmp	$3E
	bcs	LA989
	lda	#$16
	sta	$3C
	lda	#$00
	sta	$4C
	beq	LA9E6
LA989:	bne	LA999
	lda	$0F
	bpl	LA995
	lda	#$05
	sta	$4C
	bne	LA999
LA995:	lda	#$0A
	sta	$4C
LA999:	lda	$10
	bpl	LA9A7
	eor	#$FF
	clc
	adc	#$01
	sta	$3E
	jmp	LA9AB

LA9A7:	lda	$10
	sta	$3E
LA9AB:	lda	$D0
	cmp	$3E
	bcs	LA9BB
	lda	#$16
	sta	$3C
	lda	#$00
	sta	$4C
	beq	LA9E6
LA9BB:	bne	LA9E6
	lda	$10
	bpl	LA9C7
	lda	#$03
	sta	$4C
	bne	LA9E6
LA9C7:	lda	#$0C
	sta	$4C
	bne	LA9E6
LA9CD:	jsr	LAAE1
	lda	$19
	eor	$3D
	and	#$08
	beq	LA9E6
	lda	$19
	bne	LA9E2
	lda	#$15
	sta	$3C
	bne	LA9E6
LA9E2:	lda	#$16
	sta	$3C
LA9E6:	lda	$3C
	asl	a
	asl	a
	adc	$3C
	adc	$F5B3
	sta	$40
	lda	$F5B4
	adc	#$00
	sta	$41
	ldx	$22
	ldy	#$00
	lda	($40), y
	sta	$6436, x
	iny
	lda	($40), y
	sta	$6437, x
	txa
	clc
	adc	$6008
	tax
	lda	$0A
	clc
	adc	#$1E
	sta	$0A
	bcc	LAA18
	inc	$0B
LAA18:	iny
	lda	($40), y
	sta	$6436, x
	iny
	lda	($40), y
	sta	$6437, x
	iny
	lda	$48
	sta	$3C
	.byte	$A5
LAA2A:	eor	#$85
	rol	$40B1, x
	sta	$08
	lda	$D1
	bne	LAA3C
	ldx	$23
	lda	$08
	sta	$6496, x
LAA3C:	rts

	jsr	LAA49
	jmp	LC529

	jsr	LAA49
	jmp	LC212

LAA49:	lda	L9A18
	sta	$3E
	lda	L9A19
	sta	$3F
	lda	L9A2C
	sta	$40
	lda	L9A2D
	sta	$41
	lda	#$FF
	sta	$3D
	rts

	lda	#$FF
	sta	$3D
	lda	L9A24
	sta	$3E
	lda	L9A25
	sta	$3F
	lda	L9A1C
	sta	$40
	lda	L9A1D
	sta	$41
	jsr	LC212
	rts

	lda	L9A24
	sta	$3E
	lda	L9A25
	sta	$3F
	lda	L9A20
	sta	$40
	lda	L9A21
	sta	$41
	lda	#$FF
	sta	$3D
	jmp	LC529

	lda	L9A18
	sta	$0C
	lda	L9A19
	sta	$0D
	lda	#$00
	sta	$3C
	.byte	$20
	.byte	$3D
LAAA9:	dec	$20
	.byte	$74
	.byte	$FF
	lda	L9A18
	sta	$0C
	lda	L9A19
	sta	$0D
	lda	#$00
	sta	$3C
	jmp	LC632

LAABE:	lda	$13
	clc
	adc	#$01
	lsr	a
	sta	$3C
	lda	#$00
	sta	$3D
	sta	$3F
	lda	$43
	sta	$3E
	jsr	LC1C9
	lda	$42
	lsr	a
	clc
	adc	$40
	sta	$3E
	lda	$41
	adc	#$00
	sta	$3F
LAAE1:	lda	$17
	ora	$18
	bne	LAAEC
LAAE7:	lda	#$00
	sta	$3D
	rts

LAAEC:	lda	$13
	cmp	$42
	bcc	LAAE7
	lda	$14
	cmp	$43
	bcc	LAAE7
	lda	$3E
	clc
	adc	$17
	sta	$3E
	lda	$3F
	adc	$18
	sta	$3F
	tya
	pha
	ldy	#$00
	lda	($3E), y
	sta	$3D
	pla
	tay
	lda	$42
	and	#$01
	bne	LAB1D
	lsr	$3D
	lsr	$3D
	lsr	$3D
	lsr	$3D
LAB1D:	lda	$3D
	and	#$08
	sta	$3D
	rts

LAB24:	tax
	pha
	ldx	#$00
	ldy	$3C
	sty	$2C
	cpy	#$77
	beq	LAB36
	iny
	sty	$3C
	jsr	LABF4
LAB36:	beq	LAB3D
	txa
	clc
	adc	#$04
	tax
LAB3D:	ldy	$2C
	beq	LAB47
	dey
	sty	$3C
	jsr	LABF4
LAB47:	beq	LAB4B
	inx
	inx
LAB4B:	lda	$2C
	sta	$3C
	ldy	$3E
	beq	LAB58
	dey
	tya
	jsr	LABE8
LAB58:	beq	LAB5B
	inx
LAB5B:	ldy	$3E
	cpy	#$77
	beq	LAB66
	iny
	tya
	jsr	LABE8
LAB66:	beq	LAB6D
	txa
	clc
	adc	#$08
	tax
LAB6D:	lda	L99F0, x
	sta	$3C
	pla
	tax
	pla
	tay
	rts

LAB77:	tya
	pha
	txa
	pha
	ldx	#$00
	lda	$45
	cmp	#$01
	bne	LABDF
	lda	$3E
	bmi	LABB1
	cmp	#$78
	bcs	LABB1
	lda	$3C
	cmp	#$FF
	beq	LABA3
	cmp	#$78
	bne	LABB1
	dec	$3C
	lda	$3E
	jsr	LABE8
	beq	LABA0
	ldx	#$02
LABA0:	jmp	LAB6D

LABA3:	inc	$3C
	lda	$3E
	jsr	LABE8
	beq	LABAE
	ldx	#$04
LABAE:	jmp	LAB6D

LABB1:	lda	$3C
	bmi	LABDF
	cmp	#$78
	bcs	LABDF
	lda	$3E
	cmp	#$FF
	beq	LABD1
	cmp	#$78
	bne	LABDF
	dec	$3E
	lda	$3E
	jsr	LABE8
	beq	LABCE
	ldx	#$01
LABCE:	jmp	LAB6D

LABD1:	inc	$3E
	lda	$3E
	jsr	LABE8
	beq	LABDC
	ldx	#$08
LABDC:	jmp	LAB6D

LABDF:	lda	$15
	sta	$3C
	pla
	tax
	pla
	tay
	rts

LABE8:	asl	a
	tay
	lda	LA653, y
	sta	$40
	lda	LA654, y
	sta	$41
LABF4:	ldy	#$00
	sty	$3D
LABF8:	lda	($40), y
	and	#$0F
	sec
	adc	$3D
	sta	$3D
	lda	$3C
	cmp	$3D
	bcc	LAC0B
	iny
	jmp	LABF8

LAC0B:	lda	($40), y
	and	#$F0
	cmp	#$40
	bne	LAC14
	rts

LAC14:	cmp	#$B0
	rts
; ------------------------------------------------------------
	.export	LAC17
LAC17:
	lda	$3C
	sta	$42
	lda	$3E
	sta	$43
	lda	$13
	cmp	$3C
	bcs	LAC28
LAC25:	jmp	LAB77

LAC28:	lda	$E0
	cmp	#$27
	bne	LAC33
	lda	#$16
	sta	$3C
	rts

LAC33:	lda	$14
	cmp	$3E
	bcc	LAC25
	tya
	pha
	lda	$45
	cmp	#$01
	bne	LAC98
	lda	$3C
	cmp	#$40
	bne	LAC5A
	lda	$3E
	cmp	#$31
	bne	LAC5A
	lda	$CF
	and	#$08
	beq	LAC5A
	lda	#$0A
	sta	$3C
	pla
	tay
	rts

LAC5A:	lda	$3E
	asl	a
	tay
	lda	LA653, y
	sta	$40
	lda	LA654, y
	sta	$41
	ldy	#$00
	sty	$3D
LAC6C:	lda	($40), y
	and	#$0F
	sec
	adc	$3D
	sta	$3D
	lda	$3C
	cmp	$3D
	bcc	LAC7F
	iny
	jmp	LAC6C

LAC7F:	lda	($40), y
	lsr	a
	lsr	a
	lsr	a
	lsr	a
	clc
	adc	$16
	cmp	#$04
	bne	LAC8F
	jmp	LAB24

LAC8F:	tay
	lda	L99E3, y
	sta	$3C
	pla
	tay
	rts

LAC98:	lda	#$00
	sta	$3D
	sta	$3F
	lda	$13
	lsr	a
	adc	#$00
	sta	$3C
	jsr	LC1C9
	lda	$42
	lsr	a
LACAB:	clc
	adc	$40
	sta	$40
	sta	$3E
	lda	$41
	adc	#$00
	sta	$41
	sta	$3F
	lda	$40
	clc
	adc	$11
	sta	$40
	lda	$41
	adc	$12
	sta	$41
	ldy	#$00
	lda	($40), y
	sta	$3C
	lda	$42
	lsr	a
	bcs	LACDA
	lsr	$3C
	lsr	$3C
	lsr	$3C
	lsr	$3C
LACDA:	lda	$45
	cmp	#$0C
	bcc	LACE4
	lda	#$07
	bne	LACE6
LACE4:	lda	#$0F
LACE6:	and	$3C
	clc
	adc	$16
	tay
	lda	L99F0, y
	sta	$3C
	cmp	#$17
	bne	LAD01
	lda	$DF
	and	#$03
	beq	LAD3C
	lda	#$04
	sta	$3C
	.byte	$D0
LAD00:	.byte	$3B
LAD01:	cmp	#$05
	bne	LAD23
	lda	$45
	cmp	#$02
	bne	LAD3C
	lda	$42
	cmp	#$0A
	bne	LAD3C
	lda	$43
	cmp	#$01
	bne	LAD3C
	lda	$CF
	and	#$04
	bne	LAD3C
	lda	#$0D
	sta	$3C
	bne	LAD3C
LAD23:	cmp	#$0C
	bne	LAD45
	ldy	#$00
LAD29:	lda	$42
	cmp	$601C, y
	bne	LAD3F
	iny
	lda	$43
	cmp	$601C, y
	bne	LAD40
LAD38:	lda	#$04
	sta	$3C
LAD3C:	pla
	tay
	rts

LAD3F:	iny
LAD40:	iny
	cpy	#$10
	bne	LAD29
LAD45:	lda	$3C
	cmp	#$11
	bne	LAD3C
	ldy	#$00
LAD4D:	lda	$42
	cmp	$600C, y
	bne	LAD5E
	iny
	lda	$43
	cmp	$600C, y
	bne	LAD5F
	beq	LAD38
LAD5E:	iny
LAD5F:	iny
	cpy	#$10
	bne	LAD4D
	beq	LAD3C

	.export	LAD66
LAD66:
	lda	$4B
	asl	a
	clc
	adc	$10
	clc
	adc	#$1E
	sta	$3C
	lda	#$1E
	sta	$3E
	jsr	LC1F0
	lda	$40
	sta	$3E
	sta	$49
	lda	$4A
	asl	a
	clc
	adc	$0F
	and	#$3F
	sta	$3C
	sta	$48
	jsr	LC5AA
	lda	$0F
	asl	a
	lda	$0F
	ror	a
	clc
	adc	$3A
	sta	$3C
	lda	$10
	asl	a
	lda	$10
	ror	a
	clc
	adc	$3B
	sta	$3E
	jsr	LAC17
	lda	$16
	cmp	#$20
	bne	LAE12
	lda	$0F
	bpl	LADBA
	eor	#$FF
	clc
	adc	#$01
	sta	$3E
	jmp	LADBE

LADBA:	lda	$0F
	sta	$3E
LADBE:	lda	$D0
	cmp	$3E
	bcs	LADCE
	lda	#$16
	sta	$3C
	lda	#$00
	sta	$4C
	beq	LAE2B
LADCE:	bne	LADDE
	lda	$0F
	bpl	LADDA
	lda	#$05
	sta	$4C
	bne	LADDE
LADDA:	lda	#$0A
	sta	$4C
LADDE:	lda	$10
	bpl	LADEC
	eor	#$FF
	clc
	adc	#$01
	sta	$3E
	jmp	LADF0

LADEC:	lda	$10
	sta	$3E
LADF0:	lda	$D0
	cmp	$3E
	bcs	LAE00
	lda	#$16
	sta	$3C
	lda	#$00
	sta	$4C
	beq	LAE2B
LAE00:	bne	LAE2B
	lda	$10
	bpl	LAE0C
	lda	#$03
	sta	$4C
	bne	LAE2B
LAE0C:	lda	#$0C
	sta	$4C
	bne	LAE2B
LAE12:	jsr	LAAE1
	lda	$19
	eor	$3D
	and	#$08
	beq	LAE2B
	lda	$19
	bne	LAE27
	lda	#$15
	sta	$3C
	bne	LAE2B
LAE27:	lda	#$16
	sta	$3C
LAE2B:	lda	$3C
	asl	a
	asl	a
	adc	$3C
	adc	$F5B3
	sta	$40
	lda	$F5B4
	adc	#$00
	sta	$41
	ldy	#$00
	lda	($40), y
	sta	$08
	jsr	LC690
	lda	$4C
	lsr	a
	bcc	LAE63
	lda	$16
	cmp	#$20
	bne	LAE5B
	ldx	$04
	dex
	lda	#$5F
	sta	$0300, x
	bne	LAE63
LAE5B:	dec	$04
	dec	$04
	dec	$04
	dec	$03
LAE63:	iny
	lda	($40), y
	sta	$08
	jsr	LC690
	lda	$4C
	and	#$02
	beq	LAE89
	lda	$16
	cmp	#$20
	bne	LAE81
	ldx	$04
	dex
	lda	#$5F
	sta	$0300, x
	bne	LAE89
LAE81:	dec	$04
	dec	$04
	dec	$04
	dec	$03
LAE89:	iny
	lda	$0A
	clc
	adc	#$1E
	sta	$0A
	bcc	LAE95
	inc	$0B
LAE95:	lda	($40), y
	sta	$08
	jsr	LC690
	lda	$4C
	and	#$04
	beq	LAEBA
	lda	$16
	cmp	#$20
	bne	LAEB2
	ldx	$04
	dex
	lda	#$5F
	sta	$0300, x
	bne	LAEBA
LAEB2:	dec	$04
	dec	$04
	dec	$04
	dec	$03
LAEBA:	iny
	lda	($40), y
	sta	$08
	jsr	LC690
	lda	$4C
	and	#$08
	beq	LAEE0
	lda	$16
	cmp	#$20
	bne	LAED8
	ldx	$04
	dex
	lda	#$5F
	sta	$0300, x
	bne	LAEE0
LAED8:	dec	$04
	dec	$04
	dec	$04
	dec	$03
LAEE0:	iny
	lda	$48
	sta	$3C
	lda	$49
	sta	$3E
	lda	($40), y
	sta	$08
	jsr	LC006
	lda	$D1
	bne	LAEFE
	lda	$0B
	clc
	adc	#$20
	sta	$0B
	jsr	LC690
LAEFE:	rts

LAEFF:	.byte	$A5
LAF00:	.byte	$3A
	sta	$3C
	lda	$3B
	sta	$3E
	jsr	LAC17
	jsr	LAAE1
	lda	$3D
	.byte	$85
LAF10:	.byte	$19
	rts

LAF12:	lda	$45
	cmp	#$0F
	bcs	LAF1F
	lda	#$01
	sta	$D0
	lsr	a
	sta	$DA
LAF1F:	lda	$45
	cmp	#$05
	beq	LAF2B
	lda	$DF
	ora	#$08
	sta	$DF
LAF2B:	lda	$45
	cmp	#$01
	bne	LAF3C
	ldx	#$00
	txa
LAF34:	sta	$600C, x
	inx
	cpx	#$20
	bne	LAF34
LAF3C:	lda	$DF
	and	#$08
	beq	LAF6A
	lda	$45
	cmp	#$05
	bne	LAF6A
	lda	#$04
	sta	$602A
	sta	$602B
	sta	$6029
	sta	$601A
	lda	#$05
	sta	$6028
	lda	#$06
	sta	$6026
	lda	#$01
	sta	$6027
	lda	#$07
	sta	$601B
LAF6A:	lda	#$08
	sta	$4A
	lda	#$07
	sta	$4B
	lda	#$00
	sta	$05
	sta	$07
	sta	$06
	lda	$45
	asl	a
	asl	a
	adc	$45
	tay
	lda	L801A, y
	sta	$11
	iny
	lda	L801A, y
	sta	$12
	iny
	lda	L801A, y
	sta	$13
	iny
	lda	L801A, y
	sta	$14
	iny
	lda	L801A, y
	sta	$15
	lda	#$FF
	sta	$8D
	lda	$E4
	and	#$04
	beq	LAFB2
	lda	$45
	cmp	#$04
	bne	LAFB2
	lda	#$0B
	bne	LAFBB
LAFB2:	lda	$45
	sec
	sbc	#$04
	cmp	#$0B
	bcs	LB01A
LAFBB:	asl	a
	tay
	lda	#$00
	sta	$8D
	lda	L9734, y
	sta	$3C
	lda	L9735, y
	sta	$3D
	lda	#$00
	tax
LAFCE:	sta	$51, x
	inx
	cpx	#$3C
	bne	LAFCE
	lda	$45
	cmp	#$06
	bne	LAFE1
	lda	$E4
	and	#$04
	bne	LB01A
LAFE1:	ldy	#$00
	ldx	#$00
LAFE5:	lda	($3C), y
	cmp	#$FF
	beq	LAFFE
	sta	$51, x
	inx
	iny
	lda	($3C), y
	sta	$51, x
	inx
	iny
	lda	#$00
	sta	$51, x
	inx
	iny
	jmp	LAFE5

LAFFE:	iny
	ldx	#$1E
LB001:	lda	($3C), y
	cmp	#$FF
	beq	LB01A
	sta	$51, x
	inx
	iny
	lda	($3C), y
	sta	$51, x
	inx
	iny
	lda	#$00
	sta	$51, x
	inx
	iny
	jmp	LB001

LB01A:	lda	$45
	cmp	#$05
	bne	LB02E
	lda	$DF
	and	#$02
	bne	LB02E
	lda	#$00
	sta	$78
	sta	$79
	sta	$7A
LB02E:	lda	$45
	cmp	#$0F
	bcc	LB03F
	lda	#$20
	sta	$16
LB038:	lda	#$00
	sta	$17
	sta	$18
	rts

LB03F:	lda	$45
	cmp	#$01
	bne	LB04B
	lda	#$00
	sta	$16
	beq	LB038
LB04B:	lda	#$10
	sta	$16
	lda	$45
	cmp	#$08
	bne	LB060
	lda	L8012
	sta	$17
	lda	L8013
	sta	$18
	rts

LB060:	cmp	#$09
	bne	LB06F
	lda	L8014
	sta	$17
	lda	L8015
	sta	$18
	rts

LB06F:	cmp	#$0A
	bne	LB07E
	lda	L8016
	sta	$17
	lda	L8017
	sta	$18
	rts

LB07E:	cmp	#$0B
	bne	LB038
	lda	L8018
	sta	$17
	lda	L8019
	sta	$18
	rts
; ------------------------------------------------------------
	.export	LB08D, LB091, LB097
LB08D:
	lda	#$00
	beq	LB099
; --------------
LB091:
	lda	#$00
	sta	$25
	beq	LB09E
; --------------
LB097:
	lda	#$01
; --------------
LB099:
	sta	$25
	jsr	LA743
; --------------
LB09E:
	jsr	LC6BB
	lda	#$00
	sta	$2001
	sta	$600A
	jsr	LFC98
	jsr	LFCAD
	lda	$25
	beq	LB0B8
	lda	#$82
	brk
	.byte	$04
	.byte	$17
LB0B8:	lda	#$18
	sta	$2001
	jsr	LAF12
	jsr	LAEFF
	lda	#$F2
	sta	$10
LB0C7:	jsr	LFF74
	lda	#$EE
	sta	$0F
LB0CE:	lda	#$00
	sta	$4C
	sta	$D1
	jsr	LAD66
	inc	$0F
	inc	$0F
	bne	LB0CE
	jsr	LFF74
LB0E0:	lda	#$00
	sta	$4C
	sta	$D1
	jsr	LAD66
	inc	$0F
	inc	$0F
	lda	$0F
	cmp	#$12
	bne	LB0E0
	inc	$10
	inc	$10
	lda	$10
	cmp	#$10
	bne	LB0C7
	jsr	LFF74
	lda	#$FF
	sta	$96
	jsr	LB6DA
	lda	#$00
	sta	$96
	jsr	LFF74
	ldx	$45
	lda	LB1AE, x
	brk
	.byte	$04
	.byte	$17
	lda	L9A24
	sta	$3E
	lda	L9A25
	sta	$3F
	lda	#$FF
	sta	$3D
	lda	L9A1A
	clc
	adc	$16
	sta	$40
	lda	L9A1B
	adc	#$00
	sta	$41
	jsr	LC529
	lda	#$02
	sta	$0A
	lda	#$24
	sta	$0B
	lda	#$5F
	sta	$08
	lda	#$0F
	sta	$4D
LB146:	jsr	LFF74
	ldy	$04
	lda	#$80
	sta	$602C
	lda	$0B
	ora	#$80
	sta	$0300, y
	lda	#$1C
	tax
	sta	$0301, y
	lda	$0A
	sta	$0302, y
	lda	$08
LB164:	sta	$0303, y
	iny
	dex
	bne	LB164
	inc	$03
	lda	$0A
	clc
	adc	#$20
	sta	$0A
	bcc	LB178
	inc	$0B
LB178:	lda	$0B
	ora	#$80
	sta	$0303, y
	lda	#$1C
	tax
	sta	$0304, y
	lda	$0A
	sta	$0305, y
	lda	$08
LB18C:	sta	$0306, y
	iny
	dex
	bne	LB18C
	inc	$03
	tya
	clc
	adc	#$06
	sta	$04
	lda	$0A
	clc
	adc	#$20
	sta	$0A
	bcc	LB1A6
	inc	$0B
LB1A6:	dec	$4D
	bne	LB146
	jsr	LFF74
	rts

LB1AE:	brk
	ora	$06
	ora	#$03
	.byte	$02
	ora	$0404
	.byte	$04
	.byte	$04
	.byte	$04
	.byte	$03
	.byte	$03
	.byte	$03
	.byte	$07
	php
	ora	#$0A
	.byte	$0B
	.byte	$0C
	asl	$06
	.byte	$07
	asl	$07
	php
	ora	#$06
	.byte	$07
LB1CC:	lda	$8E
	sta	$3C
	lda	$8F
	sta	$3E
	jsr	LAC17
	lda	$3C
	cmp	#$0E
	bcc	LB1F5
LB1DD:	lda	$3A
	sta	$8E
	lda	$3B
	sta	$8F
	pla
	pla
	pla
	pla
	lda	#$8F
	brk
	.byte	$04
	.byte	$17
	lda	#$00
	sta	$4F
	jmp	LCB30

LB1F5:	lda	$8D
	cmp	#$FF
	bne	LB1FC
	rts

LB1FC:	ldx	#$00
LB1FE:	lda	$51, x
	and	#$1F
	cmp	$8E
	bne	LB211
	lda	$52, x
	and	#$1F
	cmp	$8F
	bne	LB211
	jmp	LB1DD

LB211:	inx
	inx
	inx
	cpx	#$3C
	bne	LB1FE
	rts
; ------------------------------------------------------------
	.export	LB219, LB228
LB219:
	lda	$13
	cmp	$3A
	bcc	LB228
	lda	$14
	cmp	$3B
	bcc	LB228
	jmp	LCBF7

LB228:
	ldx	#$00
	lda	$45
LB22C:	cmp	$F461, x
	beq	LB239
	inx
	inx
	inx
	cpx	#$93
	bne	LB22C
	rts

LB239:	lda	#$02
	jmp	LD9E2

LB23E:	lda	$0484
	cmp	#$FF
	bne	LB246
	rts

LB246:	lda	$4F
	pha
	lda	#$00
	jsr	LA7A2
	pla
	sta	$4F
	rts
; ------------------------------------------------------------
	.export	LB252
LB252:
	jsr	LB23E
	lda	$4F
	and	#$0F
	beq	LB260
	pla
	pla
	jmp	LCB30

LB260:	inc	$8E
	jsr	LB1CC
	lda	$16
	cmp	#$20
	bne	LB28C
	inc	$3A
	jsr	LB2D4
	lda	$90
	clc
	adc	#$08
	sta	$90
	bcc	LB27B
	inc	$91
LB27B:	jsr	LB30E
	lda	$90
LB280:	clc
	adc	#$08
	sta	$90
	bcc	LB289
	inc	$91
LB289:	jmp	LB6DA

LB28C:	lda	#$12
	sta	$0F
	lda	#$F2
	sta	$10
	jsr	LFF74
	lda	#$00
	sta	$4C
	sta	$D1
	jsr	LAD66
	inc	$10
	inc	$10
	inc	$05
	inc	$90
	jsr	LB6DA
	lda	$10
	.byte	$C9
LB2AE:	bpl	LB280
	.byte	$E3
	jsr	LFF74
	inc	$05
	bne	LB2BE
	lda	$06
	.byte	$49
LB2BB:	ora	($85, x)
	.byte	$06
LB2BE:	inc	$4A
	lda	#$1F
	and	$4A
	sta	$4A
	inc	$3A
	inc	$90
	bne	LB2CE
	inc	$91
LB2CE:	jsr	LB6DA
	jmp	LB5FA

LB2D4:	lda	$4A
	eor	#$10
	and	#$1F
	sta	$4A
	lda	#$FA
	sta	$10
LB2E0:	.byte	$20
LB2E1:	.byte	$74
LB2E2:	.byte	$FF
	lda	#$F9
	sta	$0F
LB2E7:	lda	#$00
	sta	$4C
	sta	$D1
	jsr	LAD66
	inc	$0F
	inc	$0F
	lda	$0F
	cmp	#$09
	bne	LB2E7
	inc	$10
	inc	$10
	lda	$10
	cmp	#$08
	bne	LB2E0
	jsr	LFF74
	lda	$06
	eor	#$01
	sta	$06
	rts
; ------------------------------------------------------------
	.export	LB30E
LB30E:
	lda	$4A
	clc
	adc	#$10
	and	#$1F
	sta	$4A
	lda	#$FA
	sta	$10
	jsr	LB6DA
LB31E:	jsr	LFF74
	lda	#$FA
	sta	$0F
LB325:	lda	#$00
	sta	$4C
	sta	$D1
	jsr	LAD66
	inc	$0F
	inc	$0F
	lda	$0F
	cmp	#$08
	bne	LB325
	inc	$10
	inc	$10
	lda	$10
	cmp	#$08
	bne	LB31E
	lda	$06
	eor	#$01
	sta	$06
	jsr	LFF74
	rts
; ------------------------------------------------------------
	.export	LB34C
LB34C:
	jsr	LB23E
	lda	$4F
	and	#$0F
	beq	LB35A
	pla
	pla
	jmp	LCB30

LB35A:	dec	$8E
	jsr	LB1CC
	lda	$16
	cmp	#$20
	bne	LB386
	jsr	LB2D4
	dec	$3A
	lda	$90
	sec
	sbc	#$08
	sta	$90
	bcs	LB375
	dec	$91
LB375:	jsr	LB30E
	lda	$90
	sec
	sbc	#$08
	sta	$90
	bcs	LB383
	dec	$91
LB383:	jmp	LB6DA

LB386:	lda	#$EC
	sta	$0F
	lda	#$F2
	sta	$10
LB38E:	jsr	LFF74
	lda	#$00
	sta	$4C
	sta	$D1
	jsr	LAD66
	inc	$10
	inc	$10
	lda	$05
	sec
	sbc	#$01
	sta	$05
	bcs	LB3AD
	lda	$06
	eor	#$01
	sta	$06
LB3AD:	lda	$90
	sec
	sbc	#$01
	sta	$90
	bcs	LB3B8
	dec	$91
LB3B8:	jsr	LB6DA
	lda	$10
	cmp	#$10
	bne	LB38E
	jsr	LFF74
	dec	$05
	dec	$4A
	lda	#$1F
	and	$4A
	sta	$4A
	dec	$3A
	dec	$90
	jsr	LB6DA
	jmp	LB5FA
; ------------------------------------------------------------
	.export	LB3D8
LB3D8:
	jsr	LB23E
	lda	$4F
	and	#$0F
	beq	LB3E6
	pla
	pla
	jmp	LCB30

LB3E6:	inc	$8F
	jsr	LB1CC
	lda	$16
	cmp	#$20
	bne	LB412
	inc	$3B
	jsr	LB4C9
	lda	$92
	clc
	adc	#$08
	sta	$92
	bcc	LB401
	inc	$93
LB401:	jsr	LB30E
	lda	$92
	clc
	adc	#$08
	sta	$92
	bcc	LB40F
	inc	$93
LB40F:	jmp	LB6DA

LB412:	jsr	LFF74
	inc	$07
	inc	$92
	jsr	LB6DA
	lda	#$10
	sta	$10
	lda	#$EE
	sta	$0F
LB424:	lda	#$03
	sta	$4D
	jsr	LFF74
LB42B:	lda	#$0C
	sta	$4C
	sta	$D1
	jsr	LAD66
	inc	$0F
	inc	$0F
	dec	$4D
	bne	LB42B
	inc	$07
	inc	$92
	jsr	LB6DA
	lda	$0F
	cmp	#$12
	bne	LB424
	lda	#$10
	sta	$10
	lda	#$EC
	sta	$0F
LB451:	lda	#$05
	sta	$4D
	jsr	LFF74
LB458:	jsr	LC244
	lda	$0F
	clc
	adc	#$04
	sta	$0F
	dec	$4D
	bne	LB458
	inc	$07
	inc	$92
	jsr	LB6DA
	lda	$0F
	cmp	#$14
	bne	LB451
	lda	#$10
	sta	$10
	lda	#$EE
	sta	$0F
LB47B:	lda	#$03
	sta	$4D
	jsr	LFF74
LB482:	lda	#$03
	sta	$4C
	sta	$D1
	jsr	LAD66
	inc	$0F
	inc	$0F
	dec	$4D
	bne	LB482
	inc	$07
	inc	$92
	jsr	LB6DA
	lda	$0F
	cmp	#$12
	bne	LB47B
	jsr	LFF74
	inc	$07
	lda	$07
	cmp	#$F0
	bne	LB4AF
	lda	#$00
	sta	$07
LB4AF:	inc	$4B
	lda	$4B
	cmp	#$0F
	bne	LB4BB
	lda	#$00
	sta	$4B
LB4BB:	inc	$3B
	inc	$92
	bne	LB4C3
	inc	$93
LB4C3:	jsr	LB6DA
	jmp	LB5FA

LB4C9:	lda	$4A
	clc
	adc	#$10
	and	#$1F
	sta	$4A
	lda	#$FA
	sta	$0F
LB4D6:	jsr	LFF74
	lda	#$F9
	sta	$10
LB4DD:	lda	#$00
	sta	$4C
	sta	$D1
	jsr	LAD66
	inc	$10
	inc	$10
	lda	$10
	cmp	#$09
	bne	LB4DD
	inc	$0F
	inc	$0F
	lda	$0F
	cmp	#$08
	bne	LB4D6
	jsr	LFF74
	lda	$06
	eor	#$01
	sta	$06
	rts
; ------------------------------------------------------------
	.export	LB504
LB504:
	jsr	LB23E
	lda	$4F
	and	#$0F
	beq	LB512
	pla
	pla
	jmp	LCB30

LB512:	dec	$8F
	jsr	LB1CC
	lda	$16
	cmp	#$20
	bne	LB53E
	jsr	LB4C9
	dec	$3B
	lda	$92
	sec
	sbc	#$08
	sta	$92
	bcs	LB52D
	dec	$93
LB52D:	jsr	LB30E
	lda	$92
	sec
	sbc	#$08
	sta	$92
	bcs	LB53B
	dec	$93
LB53B:	jmp	LB6DA

LB53E:	jsr	LFF74
	dec	$07
	lda	$07
	cmp	#$FF
	bne	LB54D
	lda	#$EF
	sta	$07
LB54D:	lda	$92
	sec
	sbc	#$01
	sta	$92
	bcs	LB558
	dec	$93
LB558:	jsr	LB6DA
	lda	#$F0
	sta	$10
	lda	#$EE
	sta	$0F
LB563:	lda	#$03
	sta	$4D
	jsr	LFF74
LB56A:	lda	#$03
	sta	$4C
	sta	$D1
	jsr	LAD66
	inc	$0F
	inc	$0F
	dec	$4D
	bne	LB56A
	dec	$07
	dec	$92
	jsr	LB6DA
	lda	$0F
	cmp	#$12
	bne	LB563
	lda	#$F0
	sta	$10
	lda	#$EC
	sta	$0F
LB590:	lda	#$05
	sta	$4D
	jsr	LFF74
LB597:	jsr	LC244
	lda	$0F
	clc
	adc	#$04
	sta	$0F
	dec	$4D
	bne	LB597
	dec	$07
	dec	$92
	jsr	LB6DA
	lda	$0F
	cmp	#$14
	bne	LB590
	lda	#$F0
	sta	$10
	lda	#$EE
	sta	$0F
LB5BA:	lda	#$03
	sta	$4D
	jsr	LFF74
LB5C1:	lda	#$0C
	sta	$4C
	sta	$D1
	jsr	LAD66
	inc	$0F
	inc	$0F
	dec	$4D
	bne	LB5C1
	dec	$07
	dec	$92
	jsr	LB6DA
	lda	$0F
	cmp	#$12
	bne	LB5BA
	jsr	LFF74
	dec	$07
	dec	$4B
	lda	$4B
	cmp	#$FF
	bne	LB5F0
	lda	#$0E
	sta	$4B
LB5F0:	dec	$3B
	dec	$92
	jsr	LB6DA
	jmp	LB5FA

LB5FA:	lda	$3A
	sta	$42
	lda	$3B
	sta	$43
	jsr	LAABE
	lda	$3D
	cmp	$19
	bne	LB60C
	rts

LB60C:	sta	$19
	lda	$19
	beq	LB623
	jsr	LFF74
	lda	#$F0
	sta	$200
	sta	$0204
	sta	$0208
	sta	$020C
LB623:	lda	$4A
	clc
	adc	#$10
	and	#$1F
	sta	$4A
	lda	#$F2
	sta	$10
LB630:	jsr	LFF74
	lda	#$F0
	sta	$0F
LB637:	lda	#$00
	sta	$4C
	sta	$D1
	jsr	LAD66
	inc	$0F
	inc	$0F
	lda	$0F
	cmp	#$00
	bne	LB637
	jsr	LFF74
LB64D:	lda	#$00
	sta	$4C
	sta	$D1
	jsr	LAD66
	inc	$0F
	inc	$0F
	lda	$0F
	cmp	#$10
	bne	LB64D
	inc	$10
	inc	$10
	lda	$10
	cmp	#$10
	bne	LB630
	jsr	LFF74
	lda	#$01
	sta	$4F
	lda	#$FF
	sta	$96
	jsr	LB6DA
	lda	#$00
	sta	$96
	lda	$06
	eor	#$01
	sta	$06
	lda	#$EE
	sta	$0F
LB686:	jsr	LFF74
	lda	#$F2
	sta	$10
LB68D:	lda	#$00
	sta	$4C
	sta	$D1
	jsr	LAD66
	inc	$10
	inc	$10
	lda	$10
	cmp	#$02
	bne	LB68D
	jsr	LFF74
LB6A3:	lda	#$00
	sta	$4C
	sta	$D1
	jsr	LAD66
	inc	$10
	inc	$10
	lda	$10
	cmp	#$10
	bne	LB6A3
	lda	$0F
	clc
	adc	#$22
	sta	$0F
	cmp	#$32
	bne	LB686
	rts
; ------------------------------------------------------------
	.export	LB6C2
LB6C2:
	sta	$24
	lda	L9332
	sta	$22
	lda	L9333
	sta	$23
LB6CE:	lda	$24
	beq	LB6D9
	inc	$23
	dec	$24
	jmp	LB6CE

LB6D9:	rts
; ------------------------------------------------------------
	.export	LB6DA
LB6DA:
	lda	$E0
	cmp	#$27
	bne	LB6E1
	rts

LB6E1:	lda	$4F
	and	#$0F
	bne	LB6EE
	lda	$50
	clc
	adc	#$08
	sta	$50
LB6EE:	lda	$DF
	and	#$01
	beq	LB6FA
	lda	#$C0
	sta	$3C
	bne	LB714
LB6FA:	lda	#$80
	sta	$3C
	lda	$BE
	and	#$E0
	beq	LB708
	lda	#$90
	sta	$3C
LB708:	lda	$BE
	and	#$03
	beq	LB714
	lda	#$20
	ora	$3C
	sta	$3C
LB714:	lda	$50
	and	#$08
	ora	$3C
	tay
	ldx	#$00
	lda	#$6F
	sta	$3C
	lda	$602F
	jsr	LB6C2
LB727:	lda	#$80
	sta	$3D
LB72B:	lda	$05D0
	cmp	#$FF
	beq	LB736
	lda	#$F0
	bne	LB738
LB736:	lda	$3C
LB738:	sta	$200, x
	inx
	lda	($22), y
	sta	$200, x
	inx
	iny
	lda	($22), y
	sta	$200, x
	inx
	iny
	lda	$3D
	sta	$200, x
	inx
	lda	$3D
	clc
	adc	#$08
	sta	$3D
	cmp	#$90
	bne	LB72B
	lda	$3C
	clc
	adc	#$08
	sta	$3C
	cmp	#$7F
	bne	LB727
	lda	$8D
	and	#$F0
	beq	LB76F
	jmp	LB9FB

LB76F:	lda	$8D
	asl	a
	sta	$3C
	asl	a
	adc	$3C
	tax
	lda	#$02
	sta	$4E
LB77C:	lda	$51, x
	and	#$1F
	bne	LB78B
	lda	$52, x
	and	#$1F
	bne	LB78B
	jmp	LB8EA

LB78B:	lda	$4F
	and	#$0F
	cmp	#$01
	beq	LB796
	jmp	LB861

LB796:	lda	$96
	beq	LB7A1
LB79A:	asl	$52, x
	lsr	$52, x
	jmp	LB8EA

LB7A1:	jsr	LC55B
	lda	$52, x
	and	#$9F
	sta	$52, x
	lda	$95
	and	#$60
	ora	$52, x
	sta	$52, x
	jsr	LBA15
	jsr	LBA22
	lda	$41
	beq	LB7C2
	lda	$40
	cmp	#$FF
	bne	LB79A
LB7C2:	jsr	LAABE
	lda	$52, x
	and	#$60
	bne	LB7D0
	dec	$43
	jmp	LB7E4

LB7D0:	cmp	#$20
	bne	LB7D9
	inc	$42
	jmp	LB7E4

LB7D9:	cmp	#$40
	bne	LB7E2
	inc	$43
	jmp	LB7E4

LB7E2:	dec	$42
LB7E4:	lda	$14
	cmp	$43
	bcs	LB7ED
	jmp	LB79A

LB7ED:	lda	$13
	cmp	$42
	bcc	LB79A
	jsr	LBA22
	lda	$41
	beq	LB800
	lda	$40
	cmp	#$FF
	bne	LB79A
LB800:	lda	$42
	cmp	$3A
	bne	LB80C
	lda	$43
	cmp	$3B
	beq	LB79A
LB80C:	lda	$42
	cmp	$8E
	bne	LB81B
	lda	$43
	cmp	$8F
	bne	LB81B
	jmp	LB79A

LB81B:	ldy	#$00
LB81D:	lda	$51, y
	and	#$1F
	cmp	$42
	bne	LB832
	lda	$52, y
	and	#$1F
	cmp	$43
	bne	LB832
	jmp	LB79A

LB832:	iny
	iny
	iny
	cpy	#$3C
	bne	LB81D
	lda	$3D
	pha
	lda	$42
	sta	$3C
	lda	$43
	sta	$3E
	jsr	LAC17
	jsr	LAAE1
	pla
	cmp	$3D
	beq	LB852
	jmp	LB79A

LB852:	lda	$3C
	cmp	#$0D
	bcc	LB85B
	jmp	LB79A

LB85B:	lda	$52, x
	ora	#$80
	sta	$52, x
LB861:	lda	$52, x
	bmi	LB868
	jmp	LB8EA

LB868:	lda	$96
	beq	LB86F
	jmp	LB8EA

LB86F:	lda	$52, x
	and	#$60
	bne	LB893
	lda	$53, x
	and	#$0F
	sec
	sbc	#$01
	and	#$0F
	sta	$3C
	lda	$53, x
	and	#$F0
	ora	$3C
	sta	$53, x
	lda	$3C
	cmp	#$0F
	bne	LB8EA
	dec	$52, x
	jmp	LB8EA

LB893:	cmp	#$20
	bne	LB8B1
	lda	$53, x
	and	#$F0
	clc
	adc	#$10
	sta	$3C
	lda	$53, x
	and	#$0F
	ora	$3C
	sta	$53, x
	lda	$3C
	bne	LB8EA
	inc	$51, x
	jmp	LB8EA

LB8B1:	cmp	#$40
	bne	LB8D1
	lda	$53, x
	and	#$0F
	clc
	adc	#$01
	and	#$0F
	sta	$3C
	lda	$53, x
	and	#$F0
	ora	$3C
	sta	$53, x
	lda	$3C
	bne	LB8EA
	inc	$52, x
	jmp	LB8EA

LB8D1:	lda	$53, x
	and	#$F0
	sec
	sbc	#$10
	sta	$3C
	lda	$53, x
	and	#$0F
	ora	$3C
	sta	$53, x
	lda	$3C
	cmp	#$F0
	bne	LB8EA
	dec	$51, x
LB8EA:	inx
	inx
	inx
	dec	$4E
	beq	LB8F4
	jmp	LB77C

LB8F4:	ldx	#$00
	lda	#$10
	sta	$4E
LB8FA:	lda	$51, x
	and	#$1F
	bne	LB909
	lda	$52, x
	and	#$1F
	bne	LB909
	jmp	LB9DF

LB909:	jsr	LBA52
	lda	$3E
	clc
	adc	#$07
	sta	$3E
	lda	$3F
	adc	#$00
	beq	LB929
	cmp	#$01
	beq	LB920
	jmp	LB9DF

LB920:	lda	$3E
	cmp	#$07
	bcc	LB929
	jmp	LB9DF

LB929:	jsr	LBA84
	lda	$40
	clc
	adc	#$11
	sta	$40
	lda	$41
	adc	#$00
	beq	LB93C
	jmp	LB9DF

LB93C:	jsr	LBA15
	jsr	LBA22
	lda	$41
	beq	LB94F
	lda	$40
	cmp	#$FF
	beq	LB94F
	jmp	LB9DF

LB94F:	lda	$42
	sta	$3C
	lda	$43
	sta	$3E
	jsr	LAABE
	lda	$3D
	cmp	$19
	beq	LB963
	jmp	LB9DF

LB963:	jsr	LC0F4
	sta	$3C
	jsr	LBA52
	jsr	LBA84
	ldy	$4E
	stx	$4E
	ldx	$3C
	lda	#$00
	sta	$3C
LB978:	lda	#$00
	sta	$3D
LB97C:	lda	$3E
	clc
	adc	$3D
	sta	$42
	lda	$3F
	adc	#$00
	bne	LB9C0
	tya
	stx	$25
	tax
	ldy	$4E
	lda	$52, y
	and	#$60
	asl	a
	rol	a
	rol	a
	rol	a
	jsr	LB6C2
	ldy	$25
	lda	$42
	sta	$0203, x
	lda	$40
	clc
	adc	$3C
	sta	$200, x
	lda	($22), y
	sta	$0201, x
	iny
	lda	($22), y
	dey
	sta	$202, x
	tya
	stx	$22
	tax
	ldy	$22
	iny
	iny
	iny
	iny
LB9C0:	inx
	inx
	tya
	beq	LB9E9
	lda	$3D
	clc
	adc	#$08
	sta	$3D
	cmp	#$10
	bne	LB97C
	lda	$3C
	clc
	adc	#$08
	sta	$3C
	cmp	#$10
	bne	LB978
	ldx	$4E
	sty	$4E
LB9DF:	inx
	inx
	inx
	cpx	#$3C
	beq	LB9E9
	jmp	LB8FA

LB9E9:	ldy	$4E
	lda	#$F0
LB9ED:	cpy	#$00
	beq	LB9FB
	sta	$200, y
	iny
	iny
	iny
	iny
	jmp	LB9ED

LB9FB:	lda	$4F
	and	#$0F
	.byte	$F0
LBA00:	ora	($60, x)
	lda	$8D
	cmp	#$FF
	beq	LBA14
	inc	$8D
	lda	$8D
	cmp	#$05
	bne	LBA14
	lda	#$00
	sta	$8D
LBA14:	rts

LBA15:	lda	$51, x
	and	#$1F
	sta	$42
	lda	$52, x
	and	#$1F
	sta	$43
	rts

LBA22:	lda	#$00
	sta	$41
	lda	$42
	sec
	sbc	$3A
	clc
	adc	#$08
	sta	$3C
	cmp	#$10
	bcc	LBA35
	rts

LBA35:	lda	$43
	sec
	sbc	$3B
	clc
	adc	#$07
	sta	$3E
	cmp	#$0F
	bcc	LBA44
	rts

LBA44:	jsr	LC596
	ldy	#$00
	lda	($0A), y
	sta	$40
	lda	#$FF
	sta	$41
	rts

LBA52:	lda	$51, x
	and	#$1F
	sta	$3F
	lda	$53, x
	sta	$3E
	lsr	$3F
	ror	$3E
	lsr	$3F
	ror	$3E
	lsr	$3F
	ror	$3E
	lsr	$3F
	ror	$3E
	lda	$3E
	sec
	sbc	$90
	sta	$3E
	lda	$3F
	sbc	$91
	sta	$3F
	lda	$3E
	eor	#$80
	sta	$3E
	bmi	LBA83
	inc	$3F
LBA83:	rts

LBA84:	lda	$52, x
	and	#$1F
	sta	$41
	lda	#$00
	sta	$40
	lsr	$41
	ror	$40
	lsr	$41
	ror	$40
	lsr	$41
	ror	$40
	lsr	$41
	ror	$40
	lda	$53, x
	and	#$0F
	ora	$40
	sta	$40
	sec
	sbc	$92
	sta	$40
	lda	$41
	sbc	$93
	sta	$41
	lda	$40
	clc
	adc	#$6F
	sta	$40
	bcc	LBABC
	inc	$41
LBABC:	rts

	lda	#$FF
	sta	$3D
	jsr	LFF74
	lda	L9A1C
	sta	$40
	lda	L9A1D
	sta	$41
	lda	L9A18
	sta	$3E
	lda	L9A19
	sta	$3F
	jsr	LC212
	lda	#$00
	sta	$0A
	lda	#$20
	sta	$0B
	lda	#$1E
	sta	$3C
	lda	#$5F
	sta	$08
	jsr	LBBAE
	lda	#$00
	sta	$08
	lda	#$02
	sta	$3C
	jsr	LBBAE
	jsr	LFF74
	lda	#$FF
	ldy	#$00
LBB00:	sta	$0400, y
	sta	$0500, y
	sta	$600, y
	sta	$0700, y
	dey
	bne	LBB00
	lda	#$00
	sta	$2001
	jsr	LFCA3
	jsr	LFCA8
	ldy	#$00
LBB1C:	lda	LBBF8, y
	sta	$08
	iny
	lda	LBBF8, y
	sta	$0A
	iny
	lda	LBBF8, y
	sta	$0B
	iny
	cpy	#$3C
	bcs	LBB3D
	lda	$0A
	sec
	sbc	#$02
	sta	$0A
	bcs	LBB3D
	dec	$0B
LBB3D:	jsr	LBBDF
	cpy	#$45
	bne	LBB1C
	ldx	#$00
LBB46:	lda	LBC3D, x
	cmp	#$FF
	bne	LBB54
	lda	LBC3E, x
	cmp	#$FF
	beq	LBB5A
LBB54:	sta	$200, x
	inx
	bne	LBB46
LBB5A:	jsr	LFF74
	lda	#$18
	sta	$2001
	lda	LBD4D
	sta	$3E
	lda	LBD4E
	sta	$3F
	lda	LBD3F
	sta	$40
	lda	LBD40
	sta	$41
	lda	#$00
	sta	$05
	sta	$07
	sta	$06
	lda	#$08
	sta	$4A
	lda	#$07
	sta	$4B
	lda	#$95
	brk
	.byte	$04
	.byte	$17
	lda	#$27
	sta	$E0
	jsr	LFF74
	lda	#$FF
	sta	$3D
	jsr	LC529
	jsr	LC529
	jsr	LC529
	jsr	LC529
	jsr	LFF74
	ldx	#$28
LBBA7:	jsr	LFF74
	dex
	bne	LBBA7
	rts

LBBAE:	jsr	LFF74
	ldy	#$20
LBBB3:	jsr	LBBDF
	dey
	bne	LBBB3
	dec	$3C
	bne	LBBAE
	rts

	lda	$4B
	asl	a
	clc
	adc	$10
	clc
	adc	#$1E
	sta	$3C
	lda	#$1E
	sta	$3E
	jsr	LC1F0
	lda	$40
	.byte	$85
LBBD3:	rol	$4AA5, x
	asl	a
	clc
	adc	$0F
	and	#$3F
	sta	$3C
	rts

LBBDF:	tya
	pha
	lda	$0A
	sta	$22
	lda	$0B
	sec
	sbc	#$1C
	sta	$23
	ldy	#$00
	lda	$08
	sta	($22), y
	jsr	LC690
	pla
	tay
	rts

LBBF8:	cli
	.byte	$2F
	and	($59, x)
	.byte	$4F
	and	($5A, x)
	bvs	LBC22
	.byte	$5B
	adc	($21), y
	.byte	$5C
	stx	$5D21
	.byte	$8F
	and	($5E, x)
	bcc	LBC2E
	ror	a
	sta	($21), y
	.byte	$6B
	.byte	$92
	and	($6C, x)
	ldx	$6D21
	.byte	$AF
	and	($6E, x)
	bcs	LBC3D
	.byte	$6F
	lda	($21), y
	bvs	LBBD3
	.byte	$21
LBC22:	adc	($B3), y
	and	($72, x)
	dec	$7321
	.byte	$CF
	and	($0A, x)
	bne	LBC4F
LBC2E:	.byte	$0B
	cmp	($21), y
	sta	($D3), y
	.byte	$23
	cpy	#$DA
	.byte	$23
	nop
	.byte	$DB
	.byte	$23
	.byte	$02
	.byte	$DC
	.byte	$23
LBC3D:	.byte	$6F
LBC3E:	cpy	#$00
	.byte	$80
	.byte	$77
	cmp	($00, x)
	rts

	.byte	$77
	.byte	$C2
	brk
	pla
	.byte	$77
	.byte	$C3
	brk
	bvs	LBCC5
	.byte	$C4
LBC4F:	brk
	sei
	.byte	$77
	cmp	$00
	.byte	$80
	.byte	$7F
	dec	$00
	rts

	.byte	$7F
	.byte	$C7
	brk
	pla
	.byte	$7F
	iny
	brk
	bvs	LBCE1
	cmp	#$00
	sei
	.byte	$7F
	dex
	brk
	.byte	$80
	.byte	$87
	.byte	$CB
	brk
	adc	$87
	cpy	$6D00
	.byte	$87
	cmp	$7800
	.byte	$87
	dec	L8000
	txa
	.byte	$CF
	ora	($61, x)
	stx	$01D0
	.byte	$74
	.byte	$8F
	cmp	($00), y
	.byte	$64
	.byte	$8F
	.byte	$D2
	brk
	adc	$D38F
	brk
	.byte	$7B
	.byte	$8F
	.byte	$D4
	brk
	.byte	$83
	.byte	$92
	cmp	$00, x
	.byte	$8B
	sty	$D6, x
	ora	($8A, x)
	.byte	$97
	.byte	$D7
	brk
	.byte	$83
	.byte	$64
	cld
	ora	($4E, x)
	rts

	cmp	$5601, y
	pla
	.byte	$DA
	ora	($56, x)
	.byte	$5B
	.byte	$DB
	ora	($5E, x)
	.byte	$63
	.byte	$DC
	ora	($5E, x)
	.byte	$6B
	cmp	$5E01, x
	.byte	$5B
	dec	$6601, x
	.byte	$63
	.byte	$DF
	.byte	$01
LBCBC:	ror	$6B
	cpx	#$01
	ror	$6F
	sbc	($00, x)
	cli
LBCC5:	eor	$01E2, y
	.byte	$80
	adc	($E3, x)
	ora	($80, x)
	.byte	$6B
	cpx	$01
	.byte	$80
	eor	$01E5, y
	dey
	adc	($E6, x)
	ora	($88, x)
	adc	#$E7
	ora	($88, x)
	.byte	$5F
	inx
	ora	($90, x)
LBCE1:	.byte	$67
	sbc	#$01
	bcc	LBD4D
	nop
	ora	($98, x)
	.byte	$6F
	.byte	$EB
	ora	($8F, x)
	rol	$02EC, x
	eor	($3E), y
	sbc	$5902
	lsr	$EE
	.byte	$02
	lsr	$EF46
	.byte	$02
	lsr	$46, x
LBCFE:	beq	LBD02
	.byte	$5E
	rti

LBD02:	.byte	$F1
LBD03:	brk
	pla
	pha
	.byte	$F2
	brk
	pla
	bvc	LBCFE
	brk
	pla
	.byte	$47
	.byte	$F4
	brk
	bvs	LBD60
	sbc	$02, x
	bvs	LBD65
	inc	$00, x
	bvs	LBD61
	.byte	$F7
	brk
	sei
	.byte	$4F
	sed
	brk
	sei
	lsr	a
	sbc	$7B02, y
	.byte	$52
	.byte	$FA
	.byte	$02
	.byte	$7B
	jmp	$2FB

	.byte	$83
	.byte	$4F
	.byte	$FC
	.byte	$02
	.byte	$8B
	lsr	$FD, x
	.byte	$03
	.byte	$54
	lsr	$FE, x
	.byte	$03
	.byte	$5C
	lsr	$03FF, x
	.byte	$5C
	.byte	$FF
	.byte	$FF
LBD3F:	.byte	$41
LBD40:	lda	$0E30, x
	bmi	LBD5C
	ora	$30, x
	and	($22, x)
	.byte	$27
	.byte	$0F
	.byte	$27
	.byte	$27
LBD4D:	.byte	$4F
LBD4E:	lda	$2221, x
	.byte	$27
	.byte	$17
	.byte	$0C
	bmi	LBD5D
	ora	$30, x
	and	($27, x)
	.byte	$15
; ------------------------------------------------------------
	.export	LBD5B
LBD5B:
	.byte	$20
LBD5C:	.byte	$74
LBD5D:	.byte	$FF
	.byte	$20
	.byte	$99
LBD60:	tax
LBD61:	jsr	LC6BB
	.byte	$A9
LBD65:	php
	sta	$2000
	lda	LBDCB
	sta	$9A
	lda	LBDCA
	sta	$99
	lda	#$20
	sta	$0B
	sta	$2006
	lda	#$00
	sta	$0A
	sta	$2006
LBD81:	jsr	LBDBF
	cmp	#$FC
	beq	LBDA7
	cmp	#$F7
	bne	LBD9F
	jsr	LBDBF
	sta	$D6
	jsr	LBDBF
	sta	$08
LBD96:	jsr	LBDB3
	dec	$D6
	bne	LBD96
	beq	LBD81
LBD9F:	sta	$08
	jsr	LBDB3
	jmp	LBD81

LBDA7:	lda	$0B
	.byte	$C9
LBDAA:	bit	$D0
	.byte	$D4
	lda	#$88
	sta	$2000
	rts

LBDB3:	lda	$08
	sta	$2007
	inc	$0A
	bne	LBDBE
LBDBC:	inc	$0B
LBDBE:	rts

LBDBF:	ldy	#$00
	lda	($99), y
	inc	$99
	bne	LBDC9
	inc	$9A
LBDC9:	rts

LBDCA:	.byte	$CC
LBDCB:	lda	L80F7, x
	.byte	$5F
	.byte	$FC
	.byte	$F7
	jsr	$FC5F
	.byte	$F7
	jsr	LFCAD
	lda	#$AA
	lda	#$AA
	lda	#$AA
	lda	#$AA
	lda	#$AA
	lda	#$AA
	lda	#$AA
	lda	#$AA
	lda	#$AA
	lda	#$AA
	lda	#$AA
	lda	#$AA
	lda	#$AA
	lda	#$AA
	lda	#$AA
	lda	#$AA
	.byte	$FC
	.byte	$AB
	ldy	LACAB
	.byte	$AB
	ldy	LACAB
	.byte	$AB
	ldy	LACAB
	.byte	$AB
	ldy	LACAB
	.byte	$AB
	ldy	LACAB
	.byte	$AB
	ldy	LACAB
	.byte	$AB
	ldy	LACAB
	.byte	$AB
	ldy	LACAB
	.byte	$FC
	lda	#$AA
	lda	#$AA
	lda	#$AA
	lda	#$AA
	lda	#$AA
	lda	#$AA
	lda	#$AA
	lda	#$AA
	lda	#$AA
	lda	#$AA
	lda	#$AA
	lda	#$AA
	lda	#$AA
	lda	#$AA
	lda	#$AA
	lda	#$AA
	.byte	$FC
	.byte	$AB
	ldy	LACAB
	.byte	$AB
	ldy	LACAB
	.byte	$AB
	ldy	LACAB
	.byte	$AB
	ldy	LACAB
	.byte	$AB
	ldy	LACAB
	.byte	$AB
	ldy	LACAB
	.byte	$AB
	ldy	LACAB
	.byte	$AB
	ldy	LACAB
	.byte	$FC
	lda	#$AA
	lda	#$74
	adc	$76, x
	.byte	$77
	sei
	adc	$7B7A, y
	.byte	$7C
	adc	$7F7E, x
	.byte	$80
	sta	($82, x)
	.byte	$83
	sty	$85
	stx	$85
	stx	$87
	dey
	.byte	$89
	txa
	.byte	$8B
	sty	LAAA9
	.byte	$FC
	.byte	$AB
	ldy	L8DAB
	stx	L908F
	sta	($92), y
	.byte	$93
	sty	$95, x
	stx	$97, y
	tya
	sta	L9B9A, y
	.byte	$9C
	sta	L9F9E, x
	.byte	$9E
	.byte	$9F
	ldy	#$A1
	ldx	#$A3
	.byte	$AB
	ldy	LACAB
	.byte	$FC
	lda	#$AA
	lda	#$AA
	lda	#$AA
	ldy	$A5
	lda	#$A6
	.byte	$A7
	tax
	lda	#$AA
	lda	#$AA
	lda	#$A8
	.byte	$A9
LBEB1:	tax
	lda	#$AA
	lda	#$AA
	lda	#$AA
	lda	#$AA
	lda	#$AA
	lda	#$AA
	.byte	$FC
	.byte	$AB
	ldy	LACAB
	.byte	$AB
	ldy	LACAB
	.byte	$AB
	ldy	LACAB
	.byte	$AB
	ldy	LACAB
	.byte	$AB
	ldy	LACAB
	.byte	$AB
	ldy	LACAB
	.byte	$AB
	ldy	LACAB
	.byte	$AB
	ldy	LACAB
	.byte	$FC
	lda	#$AA
	lda	#$AA
	lda	#$AA
	lda	#$AA
	lda	#$AA
	lda	#$AA
	lda	#$AA
	lda	#$AA
	lda	#$AA
	lda	#$AA
	lda	#$AA
	lda	#$AA
	lda	#$AA
	lda	#$AA
	lda	#$AA
	lda	#$AA
	.byte	$FC
	.byte	$AB
	.byte	$AC
LBF03:	.byte	$AB
	ldy	LACAB
	.byte	$AB
	ldy	LACAB
	.byte	$AB
	ldy	LACAB
	.byte	$AB
	ldy	LACAB
	.byte	$AB
	ldy	LACAB
	.byte	$AB
	ldy	LACAB
	.byte	$AB
	ldy	LACAB
	.byte	$AB
	ldy	$F7FC
	jsr	$FCAE
	.byte	$F7
	jsr	$FC5F
	.byte	$F7
	jsr	$FC5F
	.byte	$F7
	asl	a
	.byte	$5F
	.byte	$63
	.byte	$33
	sec
	rol	$2B, x
	.byte	$5F
	rol	$37, x
	bit	$35
	.byte	$37
	.byte	$63
	.byte	$F7
	asl	a
	.byte	$5F
	.byte	$FC
	.byte	$F7
	jsr	$FC5F
	.byte	$F7
	.byte	$0B
	.byte	$5F
	.byte	$62
	ora	($09, x)
	php
	asl	$5F
	plp
	and	($2C), y
	.byte	$3B
	.byte	$F7
	.byte	$0B
	.byte	$5F
	.byte	$FC
	.byte	$F7
	jsr	$FC5F
	.byte	$F7
	.byte	$0B
	.byte	$5F
	.byte	$62
	ora	($09, x)
	php
	ora	#$5F
	plp
	and	($2C), y
	.byte	$3B
	.byte	$F7
	.byte	$0B
	.byte	$5F
	.byte	$FC
	.byte	$F7
	jsr	$FC5F
	.byte	$F7
	asl	$5F
	.byte	$2F
	bit	$2826
	and	($36), y
	plp
	.byte	$27
	.byte	$5F
	.byte	$37
	.byte	$32
	.byte	$5F
	and	($2C), y
	and	($37), y
	plp
	and	($27), y
	.byte	$32
	.byte	$F7
	asl	$5F
	.byte	$FC
	.byte	$F7
	jsr	$FC5F
	.byte	$F7
	.byte	$04
	.byte	$5F
	.byte	$37
	.byte	$30
	.byte	$5F
	.byte	$37
	and	$24, x
	.byte	$27
	plp
	bmi	LBFBF
	and	$2E, x
	.byte	$5F
	.byte	$32
	and	#$5F
	and	($2C), y
	and	($37), y
	plp
	and	($27), y
	.byte	$32
	.byte	$F7
	.byte	$04
	.byte	$5F
	.byte	$FC
	.byte	$F7
	jsr	$FC5F
	.byte	$F7
	jsr	$FC5F
	.byte	$F7
	php
	.byte	$FF
	.byte	$F7
	php
	ora	$F7
	bpl	LBFBE
LBFBE:	.byte	$FC
LBFBF:	.byte	$F7
	php
	lda	$F7
	php
	.byte	$FF
	.byte	$FC
	.byte	$F7
	.byte	$10
	.byte	$FF
	.byte	$FC
; ------------------------------------------------------------
	.res	14, $FF
; ------------------------------------------------------------
reset_banked:
	sei
	inc	LBFDF
	jmp	start_banked
; ------------------------------------------------------------
LBFDF:	.byte	$80
; ------------------------------------------------------------
LBFE0:	.byte	"DRAGON WARRIOR  "
; ------------------------------------------------------------
	.byte	$56
	.byte	$DE
	.byte	$30
	.byte	$70
	.byte	$01
	.byte	$04
	.byte	$01
	.byte	$0F
	.byte	$07
	.byte	$00
; ------------------------------------------------------------
	.addr	reset_banked
	.addr	reset_banked
	.addr	reset_banked
