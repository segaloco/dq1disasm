; sfx_fire - fire

.include	"system/apu.i"

.include	"apu_commands.i"
	
	.export	apu_sfx_fire
apu_sfx_fire:
	.byte	apu_cmd_wave_vol_change, channel_vol::duty_500|channel_vol::counter_halt|channel_vol::constant_vol|3
	.byte	apu_cmd_noise_vol_change, channel_vol::counter_halt|channel_vol::constant_vol|15

	.byte	apu_cmd_noise_play|noise_period::np_1016n_944p,  12
	.byte	apu_cmd_noise_play|noise_period::np_2034n_1890p, 48

	.byte	APU_DATA_SFX_DONE
