; sfx_swamp - swamp/poison tile

.include	"system/apu.i"

.include	"apu_commands.i"
	
	.export	apu_sfx_swamp
apu_sfx_swamp:
	.byte	apu_cmd_noise_vol_change, channel_vol::envelope|1

	.byte	apu_cmd_noise_play|noise_period::np_4068n_3778p, 6
	.byte	apu_cmd_noise_play|noise_period::np_1016n_944p,  6

	.byte	apu_cmd_noise_vol_change, channel_vol::counter_halt|channel_vol::constant_vol|0

	.byte	APU_DATA_SFX_DONE
