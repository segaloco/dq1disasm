; sfx_windup - battle windup

.include	"system/apu.i"

.include	"apu_commands.i"

	.export	apu_sfx_windup
apu_sfx_windup:
	.byte	apu_cmd_wave_vol_change, channel_vol::duty_250|channel_vol::envelope|3

	.byte	apu_cmd_note_load|apu_notes::D5,  2
	.byte	apu_cmd_note_load|apu_notes::B4,  2
	.byte	apu_cmd_note_load|apu_notes::Dx5, 2
	.byte	apu_cmd_note_load|apu_notes::D5,  2
	.byte	apu_cmd_note_load|apu_notes::B4,  2
	.byte	apu_cmd_note_load|apu_notes::Dx5, 2

	.byte	APU_DATA_SFX_DONE
