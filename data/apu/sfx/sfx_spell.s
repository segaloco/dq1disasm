; sfx_spell - spell cast

.include	 "system/apu.i"

.include	 "apu_commands.i"

	.export apu_sfx_spell
apu_sfx_spell:
	.byte	apu_cmd_wave_vol_change, $4F

	.byte	apu_cmd_note_load|apu_notes::C4,  6
	.byte	apu_cmd_note_load|apu_notes::D4,  6
	.byte	apu_cmd_note_load|apu_notes::Cx4, 6
	.byte	apu_cmd_note_load|apu_notes::E4,  6
	.byte	apu_cmd_note_load|apu_notes::Dx4, 6
	.byte	apu_cmd_note_load|apu_notes::F4,  6
	.byte	apu_cmd_note_load|apu_notes::Fx4, 6

	.byte	APU_DATA_SFX_DONE
