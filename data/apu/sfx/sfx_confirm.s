; sfx_confirm - confirmation beep

.include	"system/apu.i"

.include	"apu_commands.i"

	.export	apu_sfx_confirm
apu_sfx_confirm:
	.byte	apu_cmd_wave_vol_change, channel_vol::duty_500|channel_vol::envelope|9

	.byte	apu_cmd_note_load|apu_notes::C7,  4
	.byte	apu_cmd_note_load|apu_notes::Fx7, 4
	.byte	apu_cmd_note_load|apu_notes::C7,  4
	.byte	apu_cmd_note_load|apu_notes::Fx7, 4

	.byte	APU_DATA_SFX_DONE
