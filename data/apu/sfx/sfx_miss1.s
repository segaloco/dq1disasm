; sfx_miss1 - battle miss 1

.include	"system/apu.i"

.include	"apu_commands.i"

	.export	apu_sfx_miss1
apu_sfx_miss1:
	.byte	apu_cmd_wave_vol_change, channel_vol::duty_125|channel_vol::envelope|15

	.byte	apu_cmd_note_load|apu_notes::A5,  4
	.byte	apu_cmd_note_load|apu_notes::G5,  4
	.byte	apu_cmd_note_load|apu_notes::Dx5, 4

	.byte	APU_DATA_SFX_DONE
