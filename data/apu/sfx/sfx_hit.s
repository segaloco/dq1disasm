; sfx_hit - battle hit

.include	"system/apu.i"

.include	"apu_commands.i"

	.export	apu_sfx_hit
apu_sfx_hit:
	.byte	apu_cmd_wave_vol_change, channel_vol::duty_250|channel_vol::envelope|3

	.byte	apu_cmd_note_load|apu_notes::G6,  2
	.byte	apu_cmd_note_load|apu_notes::Ab6, 2
	.byte	apu_cmd_note_load|apu_notes::Fx6, 2
	.byte	apu_cmd_note_load|apu_notes::G6,  2
	.byte	apu_cmd_note_load|apu_notes::Ab6, 2
	.byte	apu_cmd_note_load|apu_notes::Fx6, 2

	.byte	APU_DATA_SFX_DONE
