; sfx_wing - wyvern wing/return

.include	"system/apu.i"

.include	"apu_commands.i"

	.export	apu_sfx_wing
apu_sfx_wing:
	.byte	apu_cmd_noise_vol_change, channel_vol::envelope|1
	.byte	apu_cmd_wave_vol_change,  channel_vol::duty_250|channel_vol::counter_halt|channel_vol::constant_vol|15
	.byte	apu_cmd_pulse_sweep_cfg,  pulse_sweep::enable|pulse_sweep::period_2hf|pulse_sweep::negate|3

	.byte	apu_cmd_note_load|apu_notes::C3
	.byte	apu_cmd_noise_play|noise_period::np_4068n_3778p, 6
	.byte	apu_cmd_noise_play|noise_period::np_2034n_1890p, 6
	.byte	12

	.byte	apu_cmd_pulse_sweep_cfg, pulse_sweep::enable|pulse_sweep::period_2hf|4
	.byte	6

	.byte	apu_cmd_pulse_sweep_cfg,  pulse_sweep::enable|pulse_sweep::period_2hf|pulse_sweep::negate|3
	.byte	apu_cmd_noise_vol_change, channel_vol::envelope|1

	.byte	apu_cmd_note_load|apu_notes::C3
	.byte	apu_cmd_noise_play|noise_period::np_4068n_3778p, 6
	.byte	apu_cmd_noise_play|noise_period::np_2034n_1890p, 6
	.byte	12

	.byte	apu_cmd_pulse_sweep_cfg, pulse_sweep::enable|pulse_sweep::period_2hf|4
	.byte	6

	.byte	apu_cmd_pulse_sweep_cfg,  pulse_sweep::enable|pulse_sweep::period_2hf|pulse_sweep::negate|3
	.byte	apu_cmd_noise_vol_change, channel_vol::envelope|1

	.byte	apu_cmd_note_load|apu_notes::C3
	.byte	apu_cmd_noise_play|noise_period::np_4068n_3778p, 6
	.byte	apu_cmd_noise_play|noise_period::np_2034n_1890p, 6
	.byte	12

	.byte	APU_DATA_SFX_DONE
