; sfx_chest - open chest

.include	"system/apu.i"

.include	"apu_commands.i"

	.export	apu_sfx_chest
apu_sfx_chest:
	.byte	apu_cmd_wave_vol_change, channel_vol::duty_500|channel_vol::envelope|15

	.byte	apu_cmd_note_load|apu_notes::Fx3, 3
	.byte	apu_cmd_note_load|apu_notes::C4,  3
	.byte	apu_cmd_note_load|apu_notes::G3,  3
	.byte	apu_cmd_note_load|apu_notes::Cx4, 3

	.byte	APU_DATA_SFX_DONE
