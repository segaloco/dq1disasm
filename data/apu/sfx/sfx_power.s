; sfx_power - power strike

.include	"system/apu.i"

.include	"apu_commands.i"

	.export	apu_sfx_power
apu_sfx_power:
	.byte	apu_cmd_noise_vol_change, channel_vol::envelope|15

	.byte	apu_cmd_noise_play|noise_period::np_202n_188p, 2
	.byte	apu_cmd_noise_play|noise_period::np_254n_236p, 2
	.byte	apu_cmd_noise_play|noise_period::np_380n_354p, 2
	.byte	apu_cmd_noise_play|noise_period::np_508n_472p, 2
	.byte	apu_cmd_noise_play|noise_period::np_202n_188p, 2
	.byte	apu_cmd_noise_play|noise_period::np_254n_236p, 2
	.byte	apu_cmd_noise_play|noise_period::np_380n_354p, 2
	.byte	apu_cmd_noise_play|noise_period::np_508n_472p, 2
	.byte	apu_cmd_noise_play|noise_period::np_380n_354p, 2
	.byte	apu_cmd_noise_play|noise_period::np_254n_236p, 2
	.byte	apu_cmd_noise_play|noise_period::np_202n_188p, 2
	.byte	apu_cmd_noise_play|noise_period::np_160n_148p, 2

	.byte	APU_DATA_SFX_DONE
