; sfx_menu - menu beep

.include	"system/apu.i"

.include	"apu_commands.i"

	.export	apu_sfx_menu
apu_sfx_menu:
	.byte	apu_cmd_wave_vol_change, channel_vol::duty_500|channel_vol::envelope|9

	.byte	apu_cmd_note_load|apu_notes::A7, 6

	.byte	APU_DATA_SFX_DONE
