; sfx_bump - bump

.include	"system/apu.i"

.include	"apu_commands.i"

	.export	apu_sfx_bump
apu_sfx_bump:
	.byte	apu_cmd_wave_vol_change, channel_vol::duty_500|channel_vol::envelope|15
	.byte	apu_cmd_noise_vol_change, channel_vol::envelope|0

	.byte	apu_cmd_noise_play|noise_period::np_2034n_1890p
	.byte	apu_cmd_note_load|apu_notes::Dx3, 3
	.byte	apu_cmd_note_load|apu_notes::D3,  3
	.byte	apu_cmd_note_load|apu_notes::C3,  3

	.byte	APU_DATA_SFX_DONE
