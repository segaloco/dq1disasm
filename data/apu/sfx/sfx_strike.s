; sfx_strike - battle strike

.include	 "system/apu.i"

.include	 "apu_commands.i"

	.export	apu_sfx_strike
apu_sfx_strike:
	.byte	apu_cmd_noise_vol_change, channel_vol::envelope|15

	.byte	apu_cmd_noise_play|noise_period::np_380n_354p,   2
	.byte	apu_cmd_noise_play|noise_period::np_508n_472p,   2
	.byte	apu_cmd_noise_play|noise_period::np_762n_708p,   2
	.byte	apu_cmd_noise_play|noise_period::np_1016n_944p,  2
	.byte	apu_cmd_noise_play|noise_period::np_2034n_1890p, 2
	.byte	apu_cmd_noise_play|noise_period::np_4068n_3778p, 2

	.byte	APU_DATA_SFX_DONE
