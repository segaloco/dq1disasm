; sfx_text - text scroll sound

.include	"system/apu.i"

.include	"apu_commands.i"

	.export	apu_sfx_text
apu_sfx_text:
	.byte	apu_cmd_noise_vol_change, channel_vol::counter_halt|channel_vol::constant_vol|2
	.byte	apu_cmd_wave_vol_change,  channel_vol::duty_125|channel_vol::envelope|0

	.byte	apu_cmd_note_load|apu_notes::A5
	.byte	apu_cmd_noise_play|noise_period::np_4068n_3778p, 2

	.byte	APU_DATA_SFX_DONE
