; sfx_ffdamage - fire damage

.include	"system/apu.i"

.include	"apu_commands.i"
	
	.export	apu_sfx_ffdamage
apu_sfx_ffdamage:
	.byte	apu_cmd_noise_vol_change, channel_vol::envelope|15

	.byte	apu_cmd_noise_play|noise_period::np_160n_148p, 4
	.byte	apu_cmd_noise_play|noise_period::np_202n_188p, 4
	.byte	apu_cmd_noise_play|noise_period::np_254n_236p, 4

	.byte	APU_DATA_SFX_DONE
