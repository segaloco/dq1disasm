; sfx_glow - glow spell

.include	"system/apu.i"

.include	"apu_commands.i"

apu_sfx_unused:
	.byte	apu_cmd_next_byte

	.export	apu_sfx_glow
apu_sfx_glow:
	.byte	apu_cmd_noise_vol_change, channel_vol::counter_halt|channel_vol::constant_vol|15

	.byte	apu_cmd_noise_play|apu_notes::Dx3, 3
	.byte	apu_cmd_noise_play|apu_notes::D3,  2
	.byte	apu_cmd_noise_play|apu_notes::Cx3, 1
	.byte	apu_cmd_noise_play|apu_notes::C3,  2

	.byte	APU_DATA_SFX_DONE
