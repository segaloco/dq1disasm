; sfx_door - open door

.include	"system/apu.i"

.include	"apu_commands.i"

	.export	apu_sfx_door
apu_sfx_door:
	.byte	apu_cmd_wave_vol_change, channel_vol::duty_125|channel_vol::envelope|0

	.byte	apu_cmd_note_load|apu_notes::C6,  2
	.byte	apu_cmd_note_load|apu_notes::Cx5, 2
	.byte	apu_cmd_note_load|apu_notes::D6,  2
	.byte	apu_cmd_note_load|apu_notes::Dx5, 2
	.byte	apu_cmd_note_load|apu_notes::E6,  6
	.byte	apu_cmd_note_load|apu_notes::D5,  2
	.byte	apu_cmd_note_load|apu_notes::Dx6, 2
	.byte	apu_cmd_note_load|apu_notes::E5,  2
	.byte	apu_cmd_note_load|apu_notes::F6,  6

	.byte	APU_DATA_SFX_DONE
