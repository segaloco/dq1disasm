; sfx_run - run from battle

.include	"system/apu.i"

.include	"apu_commands.i"

	.export	apu_sfx_run
apu_sfx_run:
	.byte	apu_cmd_noise_vol_change, channel_vol::counter_halt|channel_vol::constant_vol|15

	.byte	apu_cmd_noise_play|noise_period::np_2034n_1890p, 2
	.byte	apu_cmd_noise_play|noise_period::np_1016n_944p,  2

	.byte	apu_cmd_noise_vol_change, channel_vol::counter_halt|channel_vol::constant_vol|0, 3

	.byte	apu_cmd_noise_vol_change, channel_vol::counter_halt|channel_vol::constant_vol|15

	.byte	apu_cmd_noise_play|noise_period::np_1016n_944p, 2
	.byte	apu_cmd_noise_play|noise_period::np_762n_708p,  2

	.byte	apu_cmd_noise_vol_change, channel_vol::counter_halt|channel_vol::constant_vol|0, 3

	.byte	apu_cmd_noise_vol_change, channel_vol::counter_halt|channel_vol::constant_vol|15

	.byte	apu_cmd_noise_play|noise_period::np_2034n_1890p, 2
	.byte	apu_cmd_noise_play|noise_period::np_1016n_944p,  2

	.byte	apu_cmd_noise_vol_change, channel_vol::counter_halt|channel_vol::constant_vol|0, 3

	.byte	apu_cmd_noise_vol_change, channel_vol::counter_halt|channel_vol::constant_vol|15

	.byte	apu_cmd_noise_play|noise_period::np_1016n_944p, 2
	.byte	apu_cmd_noise_play|noise_period::np_762n_708p,  2

	.byte	apu_cmd_noise_vol_change, channel_vol::counter_halt|channel_vol::constant_vol|0, 3

	.byte	apu_cmd_noise_vol_change, channel_vol::counter_halt|channel_vol::constant_vol|15

	.byte	apu_cmd_noise_play|noise_period::np_2034n_1890p, 2
	.byte	apu_cmd_noise_play|noise_period::np_1016n_944p,  2

	.byte	apu_cmd_noise_vol_change, channel_vol::counter_halt|channel_vol::constant_vol|0, 3

	.byte	apu_cmd_noise_vol_change, channel_vol::counter_halt|channel_vol::constant_vol|15

	.byte	apu_cmd_noise_play|noise_period::np_1016n_944p, 2
	.byte	apu_cmd_noise_play|noise_period::np_762n_708p,  2

	.byte	apu_cmd_noise_vol_change, channel_vol::counter_halt|channel_vol::constant_vol|0

	.byte	APU_DATA_SFX_DONE
