; sfx_stairs - stairs

.include	"system/apu.i"

.include	"apu_commands.i"

	.export	apu_sfx_stairs
apu_sfx_stairs:
	.byte	apu_cmd_noise_vol_change, channel_vol::counter_halt|channel_vol::constant_vol|15

	.byte	apu_cmd_noise_play|noise_period::np_2034n_1890p, 2
	.byte	apu_cmd_noise_play|noise_period::np_1016n_944p,  2

	.byte	apu_cmd_noise_vol_change, channel_vol::counter_halt|channel_vol::constant_vol|0, 12

	.byte	apu_cmd_noise_vol_change, channel_vol::counter_halt|channel_vol::constant_vol|15

	.byte	apu_cmd_noise_play|noise_period::np_1016n_944p, 2
	.byte	apu_cmd_noise_play|noise_period::np_762n_708p,  2

	.byte	apu_cmd_noise_vol_change, channel_vol::counter_halt|channel_vol::constant_vol|0, 12

	.byte	apu_cmd_noise_vol_change, channel_vol::counter_halt|channel_vol::constant_vol|15

	.byte	apu_cmd_noise_play|noise_period::np_2034n_1890p, 2
	.byte	apu_cmd_noise_play|noise_period::np_1016n_944p,  2

	.byte	apu_cmd_noise_vol_change, channel_vol::counter_halt|channel_vol::constant_vol|0, 12
	
	.byte	apu_cmd_noise_vol_change, channel_vol::counter_halt|channel_vol::constant_vol|15

	.byte	apu_cmd_noise_play|noise_period::np_1016n_944p, 2
	.byte	apu_cmd_noise_play|noise_period::np_762n_708p,  2

	.byte	apu_cmd_noise_vol_change, channel_vol::counter_halt|channel_vol::constant_vol|0

	.byte	APU_DATA_SFX_DONE
