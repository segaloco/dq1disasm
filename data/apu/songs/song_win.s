.include	"system/apu.i"

.include	"apu_commands.i"

	.export song_win_pulse1, song_win_pulse2
song_win_pulse2:
	.byte	$6
	.byte	apu_cmd_song_jsr
	.addr	song_win_common

	.byte	apu_cmd_wave_vol_change, $30
	.byte	APU_DATA_SFX_DONE

song_win_pulse1:
	.byte	apu_cmd_tempo_change, $78
	.byte	apu_cmd_song_jsr
	.addr	song_win_common

	.byte	apu_cmd_note_load|apu_notes::C6, $2f
	.byte	APU_DATA_SFX_DONE

	.byte	apu_cmd_next_byte

song_win_common:
	.byte	apu_cmd_wave_vol_change, $8F
	.byte	apu_cmd_note_load|apu_notes::C3, $7

	.byte	apu_cmd_note_load|apu_notes::G3, $6

	.byte	apu_cmd_note_load|apu_notes::C4, $6

	.byte	apu_cmd_trough, $1
	.byte	apu_cmd_note_load|apu_notes::D4
	.byte	apu_cmd_note_load|apu_notes::E4
	.byte	apu_cmd_note_load|apu_notes::F4
	.byte	apu_cmd_note_load|apu_notes::G4
	.byte	apu_cmd_note_load|apu_notes::A4
	.byte	apu_cmd_note_load|apu_notes::Ax4
	.byte	apu_cmd_note_load|apu_notes::C5
	.byte	apu_cmd_note_load|apu_notes::D5
	.byte	apu_cmd_note_load|apu_notes::E5
	.byte	apu_cmd_note_load|apu_notes::F5
	.byte	apu_cmd_note_load|apu_notes::G5
	.byte	apu_cmd_note_load|apu_notes::A5
	.byte	apu_cmd_note_load|apu_notes::Ax5
	.byte	apu_cmd_song_ret
