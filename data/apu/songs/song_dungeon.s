.include	"system/apu.i"

.include	"apu_commands.i"

	.export song_dungeon_pulse, song_dungeon_tri0, song_dungeon_tri1
	.export song_dungeon_tri2, song_dungeon_tri3, song_dungeon_tri4
	.export song_dungeon_tri5, song_dungeon_tri6, song_dungeon_tri7
song_dungeon_pulse:
	.byte	apu_cmd_song_jsr
	.addr	:+

	.byte	apu_cmd_song_jsr
	.addr	:+

	.byte	apu_cmd_song_jsr
	.addr	:+

	.byte	apu_cmd_song_jsr
	.addr	:+

	.byte	apu_cmd_song_jsr
	.addr	:++

	.byte	apu_cmd_song_jsr
	.addr	:++

	.byte	apu_cmd_song_jsr
	.addr	:+++

	.byte	apu_cmd_song_jsr
	.addr	:+++

	.byte	apu_cmd_song_jsr
	.addr	:++++

	.byte	apu_cmd_song_jsr
	.addr	:++++

	.byte	apu_cmd_song_jsr
	.addr	:++++

	.byte	apu_cmd_song_jsr
	.addr	:++++

	.byte	apu_cmd_song_jsr
	.addr	:+++++

	.byte	apu_cmd_song_jsr
	.addr	:+++++

	.byte	apu_cmd_note_load|apu_notes::Ax3, $c

	.byte	apu_cmd_wave_vol_change, $30
	.byte	$24

	.byte	apu_cmd_wave_vol_change, $b6
	.byte	apu_cmd_song_jsr
	.addr	:++++++

	.byte	apu_cmd_song_jsr
	.addr	:++++++

	.byte	apu_cmd_note_load|apu_notes::A3, $c

	.byte	apu_cmd_wave_vol_change, $30
	.byte	$24

	.byte	apu_cmd_song_jsr
	.addr	song_dungeon_pulse

:
	.byte	apu_cmd_wave_vol_change, $30
	.byte	$5

	.byte	apu_cmd_wave_vol_change, $b6
	.byte	apu_cmd_note_load|apu_notes::B3, $7

	.byte	apu_cmd_note_load|apu_notes::D4, $6

	.byte	apu_cmd_note_load|apu_notes::G4, $6

	.byte	apu_cmd_wave_vol_change, $30
	.byte	$5

	.byte	apu_cmd_wave_vol_change, $b6
	.byte	apu_cmd_note_load|apu_notes::B3, $7

	.byte	apu_cmd_note_load|apu_notes::D4, $6

	.byte	apu_cmd_note_load|apu_notes::G4, $6

	.byte	apu_cmd_song_ret

:
	.byte	apu_cmd_wave_vol_change, $30
	.byte	$5

	.byte	apu_cmd_wave_vol_change, $b6
	.byte	apu_cmd_note_load|apu_notes::Ax3, $7

	.byte	apu_cmd_note_load|apu_notes::Cx4, $6

	.byte	apu_cmd_note_load|apu_notes::E4, $6

	.byte	apu_cmd_song_ret

:
	.byte	apu_cmd_wave_vol_change, $30
	.byte	$5

	.byte	apu_cmd_wave_vol_change, $b6
	.byte	apu_cmd_note_load|apu_notes::B3, $7

	.byte	apu_cmd_note_load|apu_notes::D4, $6

	.byte	apu_cmd_note_load|apu_notes::F4, $6

	.byte	apu_cmd_song_ret

:
	.byte	apu_cmd_wave_vol_change, $30
	.byte	$5

	.byte	apu_cmd_wave_vol_change, $b6
	.byte	apu_cmd_note_load|apu_notes::F3, $7

	.byte	apu_cmd_note_load|apu_notes::Ab3, $6

	.byte	apu_cmd_note_load|apu_notes::C4, $6

	.byte	apu_cmd_song_ret
	
:
	.byte	apu_cmd_trough, $3
	.byte	apu_cmd_note_load|apu_notes::Ax3
	.byte	apu_cmd_note_load|apu_notes::C4
	.byte	apu_cmd_note_load|apu_notes::Ax3
	.byte	apu_cmd_note_load|apu_notes::C4
	.byte	apu_cmd_note_load|apu_notes::Ax3
	.byte	apu_cmd_note_load|apu_notes::C4
	.byte	apu_cmd_note_load|apu_notes::Ax3
	.byte	apu_cmd_note_load|apu_notes::C4
	.byte	apu_cmd_trough_end

	.byte	apu_cmd_song_ret

:
	.byte	apu_cmd_trough, $3
	.byte	apu_cmd_note_load|apu_notes::A3
	.byte	apu_cmd_note_load|apu_notes::B3
	.byte	apu_cmd_note_load|apu_notes::A3
	.byte	apu_cmd_note_load|apu_notes::B3
	.byte	apu_cmd_note_load|apu_notes::A3
	.byte	apu_cmd_note_load|apu_notes::B3
	.byte	apu_cmd_note_load|apu_notes::A3
	.byte	apu_cmd_note_load|apu_notes::B3
	.byte	apu_cmd_trough_end

	.byte	apu_cmd_song_ret

song_dungeon_tri0:
	.byte	apu_cmd_note_get, $9
	.byte	apu_cmd_tempo_change, $69
	.byte	apu_cmd_song_jsr
	.addr	:+

song_dungeon_tri1:
	.byte	apu_cmd_note_get, $6
	.byte	apu_cmd_tempo_change, $64
	.byte	apu_cmd_song_jsr
	.addr	:+

song_dungeon_tri2:
	.byte	apu_cmd_note_get, $3
	.byte	apu_cmd_tempo_change, $5f
	.byte	apu_cmd_song_jsr
	.addr	:+

song_dungeon_tri3:
	.byte	apu_cmd_tempo_change, $5a
	.byte	apu_cmd_song_jsr
	.addr	:+

song_dungeon_tri4:
	.byte	apu_cmd_note_get, $fd
	.byte	apu_cmd_tempo_change, $55
	.byte	apu_cmd_song_jsr
	.addr	:+

song_dungeon_tri5:
	.byte	apu_cmd_note_get, $fa
	.byte	apu_cmd_tempo_change, $50
	.byte	apu_cmd_song_jsr
	.addr	:+

song_dungeon_tri6:
	.byte	apu_cmd_note_get, $f7
	.byte	apu_cmd_tempo_change, $4b
	.byte	apu_cmd_song_jsr
	.addr	:+

song_dungeon_tri7:
	.byte	apu_cmd_note_get, $f4
	.byte	apu_cmd_tempo_change, $46

:
	.byte	apu_cmd_wave_vol_change, $ff
	.byte	apu_cmd_note_load|apu_notes::Ab5, $18

	.byte	apu_cmd_note_load|apu_notes::Ax5, $18

	.byte	apu_cmd_note_load|apu_notes::Ab5, $18

	.byte	apu_cmd_note_load|apu_notes::Ax5, $18

	.byte	apu_cmd_note_load|apu_notes::Cx6, $18

	.byte	apu_cmd_note_load|apu_notes::Ax5, $18

	.byte	apu_cmd_note_load|apu_notes::Ab5, $18

	.byte	apu_cmd_note_load|apu_notes::Ax5, $c

	.byte	apu_cmd_note_load|apu_notes::Ab5, $c

	.byte	apu_cmd_note_load|apu_notes::G5, $18

	.byte	apu_cmd_note_load|apu_notes::F5, $c

	.byte	apu_cmd_note_load|apu_notes::G5, $c

	.byte	apu_cmd_note_load|apu_notes::Ax5, $c

	.byte	apu_cmd_note_load|apu_notes::Ab5, $c

	.byte	apu_cmd_note_load|apu_notes::G5, $c

	.byte	apu_cmd_note_load|apu_notes::F5, $c

	.byte	apu_cmd_note_load|apu_notes::E5, $18

	.byte	apu_cmd_note_load|apu_notes::D5, $c

	.byte	apu_cmd_note_load|apu_notes::E5, $c

	.byte	apu_cmd_note_load|apu_notes::G5, $18

	.byte	apu_cmd_note_load|apu_notes::C5, $18

	.byte	apu_cmd_wave_vol_change, $30
	.byte	apu_cmd_song_jsr
	.addr	:+

	.byte	apu_cmd_song_jsr
	.addr	:+

	.byte	apu_cmd_note_load|apu_notes::Fx5, $30

	.byte	apu_cmd_song_jsr
	.addr	:++

	.byte	apu_cmd_song_jsr
	.addr	:++

	.byte	apu_cmd_note_load|apu_notes::F5, $30

	.byte	apu_cmd_song_jsr
	.addr	:-

:
	.byte	apu_cmd_trough, $3
	.byte	apu_cmd_note_load|apu_notes::Fx5
	.byte	apu_cmd_note_load|apu_notes::E5
	.byte	apu_cmd_note_load|apu_notes::Fx5
	.byte	apu_cmd_note_load|apu_notes::E5
	.byte	apu_cmd_note_load|apu_notes::Fx5
	.byte	apu_cmd_note_load|apu_notes::E5
	.byte	apu_cmd_note_load|apu_notes::Fx5
	.byte	apu_cmd_note_load|apu_notes::E5
	.byte	apu_cmd_trough_end

	.byte	apu_cmd_song_ret

:
	.byte	apu_cmd_trough, $3
	.byte	apu_cmd_note_load|apu_notes::F5
	.byte	apu_cmd_note_load|apu_notes::Dx5
	.byte	apu_cmd_note_load|apu_notes::F5
	.byte	apu_cmd_note_load|apu_notes::Dx5
	.byte	apu_cmd_note_load|apu_notes::F5
	.byte	apu_cmd_note_load|apu_notes::Dx5
	.byte	apu_cmd_note_load|apu_notes::F5
	.byte	apu_cmd_note_load|apu_notes::Dx5
	.byte	apu_cmd_trough_end

	.byte	apu_cmd_song_ret
