.include	"system/apu.i"

.include	"apu_commands.i"

	.export song_overworld_pulse, song_overworld_triangle
song_overworld_triangle:
	.byte	apu_cmd_tempo_change, $96
	.byte	apu_cmd_trough, $10
		: .byte	apu_cmd_wave_vol_change, $ff
		.byte	apu_cmd_note_load|apu_notes::D6, $10

		.byte	apu_cmd_note_load|apu_notes::A6, $10

		.byte	apu_cmd_note_load|apu_notes::G6, $50

		.byte	apu_cmd_note_load|apu_notes::F6
		.byte	apu_cmd_note_load|apu_notes::E6
		.byte	apu_cmd_note_load|apu_notes::D6, $10

		.byte	apu_cmd_note_load|apu_notes::C6
		.byte	apu_cmd_note_load|apu_notes::Ax5
		.byte	apu_cmd_note_load|apu_notes::C6
		.byte	apu_cmd_note_load|apu_notes::A5
		.byte	apu_cmd_note_load|apu_notes::E6, $10

		.byte	apu_cmd_note_load|apu_notes::D6, $30

		.byte	$40
		.byte	$40

		.byte	apu_cmd_note_load|apu_notes::A6, $10

		.byte	apu_cmd_note_load|apu_notes::C7, $10

		.byte	apu_cmd_note_load|apu_notes::B6, $50

		.byte	apu_cmd_note_load|apu_notes::G6
		.byte	apu_cmd_note_load|apu_notes::F6
		.byte	apu_cmd_note_load|apu_notes::E6, $10

		.byte	apu_cmd_note_load|apu_notes::F6
		.byte	apu_cmd_note_load|apu_notes::G6
		.byte	apu_cmd_note_load|apu_notes::A6, $70

		.byte	$40
		.byte	$40

		.byte	apu_cmd_song_jsr
		.addr	:-

song_overworld_pulse:
	.byte	apu_cmd_trough, $10
	.byte	apu_cmd_wave_vol_change, $81
		: .byte	apu_cmd_note_load|apu_notes::D4
		.byte	apu_cmd_note_load|apu_notes::A4
		.byte	apu_cmd_note_load|apu_notes::F4
		.byte	apu_cmd_note_load|apu_notes::A4
		.byte	apu_cmd_note_load|apu_notes::D4
		.byte	apu_cmd_note_load|apu_notes::B4
		.byte	apu_cmd_note_load|apu_notes::G4
		.byte	apu_cmd_note_load|apu_notes::B4
		.byte	apu_cmd_note_load|apu_notes::D4
		.byte	apu_cmd_note_load|apu_notes::C5
		.byte	apu_cmd_note_load|apu_notes::A4
		.byte	apu_cmd_note_load|apu_notes::C5
		.byte	apu_cmd_note_load|apu_notes::D4
		.byte	apu_cmd_note_load|apu_notes::Ax4
		.byte	apu_cmd_note_load|apu_notes::F4
		.byte	apu_cmd_note_load|apu_notes::Ax4
		.byte	apu_cmd_note_load|apu_notes::E4
		.byte	apu_cmd_note_load|apu_notes::C5
		.byte	apu_cmd_note_load|apu_notes::A4
		.byte	apu_cmd_note_load|apu_notes::C5
		.byte	apu_cmd_note_load|apu_notes::D4
		.byte	apu_cmd_note_load|apu_notes::A4
		.byte	apu_cmd_note_load|apu_notes::Fx4
		.byte	apu_cmd_note_load|apu_notes::A4
		.byte	apu_cmd_note_load|apu_notes::D4
		.byte	apu_cmd_note_load|apu_notes::A4
		.byte	apu_cmd_note_load|apu_notes::Fx4
		.byte	apu_cmd_note_load|apu_notes::A4
		.byte	apu_cmd_note_load|apu_notes::G4
		.byte	apu_cmd_note_load|apu_notes::Ax4
		.byte	apu_cmd_note_load|apu_notes::A4
		.byte	apu_cmd_note_load|apu_notes::C5
		.byte	apu_cmd_note_load|apu_notes::D4
		.byte	apu_cmd_note_load|apu_notes::C5
		.byte	apu_cmd_note_load|apu_notes::A4
		.byte	apu_cmd_note_load|apu_notes::C5
		.byte	apu_cmd_note_load|apu_notes::D4
		.byte	apu_cmd_note_load|apu_notes::B4
		.byte	apu_cmd_note_load|apu_notes::G4
		.byte	apu_cmd_note_load|apu_notes::B4
		.byte	apu_cmd_note_load|apu_notes::D4
		.byte	apu_cmd_note_load|apu_notes::B4
		.byte	apu_cmd_note_load|apu_notes::G4
		.byte	apu_cmd_note_load|apu_notes::B4
		.byte	apu_cmd_note_load|apu_notes::D4
		.byte	apu_cmd_note_load|apu_notes::Ax4
		.byte	apu_cmd_note_load|apu_notes::Ab4
		.byte	apu_cmd_note_load|apu_notes::Ax4
		.byte	apu_cmd_note_load|apu_notes::Cx4
		.byte	apu_cmd_note_load|apu_notes::A4
		.byte	apu_cmd_note_load|apu_notes::E4
		.byte	apu_cmd_note_load|apu_notes::A4
		.byte	apu_cmd_note_load|apu_notes::D4
		.byte	apu_cmd_note_load|apu_notes::A4
		.byte	apu_cmd_note_load|apu_notes::E4
		.byte	apu_cmd_note_load|apu_notes::A4
		.byte	apu_cmd_note_load|apu_notes::Cx4
		.byte	apu_cmd_note_load|apu_notes::A4
		.byte	apu_cmd_note_load|apu_notes::E4
		.byte	apu_cmd_note_load|apu_notes::A4
		.byte	apu_cmd_note_load|apu_notes::B3
		.byte	apu_cmd_note_load|apu_notes::A4
		.byte	apu_cmd_note_load|apu_notes::Cx4
		.byte	apu_cmd_note_load|apu_notes::A4
		.byte	apu_cmd_song_jsr
		.addr	:-
