.include	"system/apu.i"

.include	"apu_commands.i"

	.export song_curse_pulse1, song_curse_pulse2
song_curse_pulse2:
	.byte	$1

song_curse_pulse1:
	.byte	apu_cmd_tempo_change, $96
	.byte	apu_cmd_wave_vol_change, $45
	.byte	apu_cmd_trough, $6
	.byte	apu_cmd_song_jsr
	.addr	:+

	.byte	apu_cmd_song_jsr
	.addr	:+

	.byte	apu_cmd_song_jsr
	.addr	:+

	.byte	apu_cmd_song_jsr
	.addr	:+

	.byte	apu_cmd_trough_end

	.byte	apu_cmd_note_load|apu_notes::E3, $14
	.byte	apu_cmd_note_load|apu_notes::F3, $2
	.byte	apu_cmd_note_load|apu_notes::Fx3, $2
	.byte	apu_cmd_note_load|apu_notes::Ax2, $30
	.byte	APU_DATA_SFX_DONE

	.byte	apu_cmd_next_byte

		: .byte	apu_cmd_note_load|apu_notes::C3
		.byte	apu_cmd_note_load|apu_notes::B3
		.byte	apu_cmd_note_load|apu_notes::B2
		.byte	apu_cmd_note_load|apu_notes::Ax3
		.byte	apu_cmd_song_ret
