.include	"system/apu.i"

.include	"apu_commands.i"

	.export song_lora_pulse1, song_lora_pulse2, song_lora_triangle
song_lora_pulse1:
	.byte	apu_cmd_tempo_change, $6e
	.byte	apu_cmd_wave_vol_change, $30
	.byte	$6

	.byte	apu_cmd_wave_vol_change, $8f
	.byte	apu_cmd_note_load|apu_notes::C6, $12

	.byte	apu_cmd_note_load|apu_notes::A5, $6

	.byte	apu_cmd_note_load|apu_notes::G5, $6

	.byte	apu_cmd_note_load|apu_notes::F5, $6

	.byte	apu_cmd_note_load|apu_notes::E5, $c

	.byte	apu_cmd_note_load|apu_notes::D5, $18

	.byte	apu_cmd_note_load|apu_notes::Ax5, $12

	.byte	apu_cmd_note_load|apu_notes::G5, $6

	.byte	apu_cmd_note_load|apu_notes::E5, $6

	.byte	apu_cmd_note_load|apu_notes::D5, $6

	.byte	apu_cmd_note_load|apu_notes::Cx5, $18

	.byte	apu_cmd_note_load|apu_notes::A5, $c

	.byte	apu_cmd_wave_vol_change, $bf
	.byte	apu_cmd_note_load|apu_notes::C6, $30

	.byte	apu_cmd_wave_vol_change, $8f
	.byte	$3c
	.byte	APU_DATA_SFX_DONE

	.byte	apu_cmd_next_byte

song_lora_pulse2:
	.byte	apu_cmd_wave_vol_change, $30
	.byte	$12

	.byte	apu_cmd_wave_vol_change, $8f
	.byte	apu_cmd_note_load|apu_notes::C5, $c

	.byte	apu_cmd_note_load|apu_notes::A4, $c

	.byte	apu_cmd_wave_vol_change, $30
	.byte	$c

	.byte	apu_cmd_wave_vol_change, $8f
	.byte	apu_cmd_note_load|apu_notes::Ab4, $c

	.byte	apu_cmd_note_load|apu_notes::F4, $c

	.byte	apu_cmd_wave_vol_change, $30
	.byte	$c

	.byte	apu_cmd_wave_vol_change, $8f
	.byte	apu_cmd_note_load|apu_notes::D5, $c

	.byte	apu_cmd_note_load|apu_notes::Ax4, $c

	.byte	apu_cmd_wave_vol_change, $30
	.byte	$c

	.byte	apu_cmd_wave_vol_change, $8f
	.byte	apu_cmd_trough, $6

	.byte	apu_cmd_note_load|apu_notes::E4
	.byte	apu_cmd_note_load|apu_notes::G4
	.byte	apu_cmd_note_load|apu_notes::Ax4
	.byte	apu_cmd_note_load|apu_notes::E4
	.byte	apu_cmd_note_load|apu_notes::A4
	.byte	apu_cmd_note_load|apu_notes::C5
	.byte	apu_cmd_note_load|apu_notes::E5
	.byte	apu_cmd_note_load|apu_notes::C5
	.byte	apu_cmd_note_load|apu_notes::A4
	.byte	apu_cmd_note_load|apu_notes::C5
	.byte	apu_cmd_note_load|apu_notes::A4, $6

	.byte	apu_cmd_wave_vol_change, $8f
	.byte	APU_DATA_SFX_DONE

song_lora_triangle:
	.byte	apu_cmd_wave_vol_change, $0
	.byte	$6

	.byte	apu_cmd_wave_vol_change, $ff
	.byte	apu_cmd_trough, $c

	.byte	apu_cmd_note_load|apu_notes::F4
	.byte	apu_cmd_note_load|apu_notes::A4
	.byte	apu_cmd_note_load|apu_notes::C5
	.byte	apu_cmd_note_load|apu_notes::Ab4
	.byte	apu_cmd_note_load|apu_notes::B4
	.byte	apu_cmd_note_load|apu_notes::Ab4
	.byte	apu_cmd_note_load|apu_notes::G4
	.byte	apu_cmd_note_load|apu_notes::Ax4
	.byte	apu_cmd_note_load|apu_notes::G4
	.byte	apu_cmd_note_load|apu_notes::C4
	.byte	apu_cmd_note_load|apu_notes::C5
	.byte	apu_cmd_note_load|apu_notes::C4
	.byte	apu_cmd_trough, $6

	.byte	apu_cmd_note_load|apu_notes::F4
	.byte	apu_cmd_note_load|apu_notes::A4
	.byte	apu_cmd_note_load|apu_notes::C5
	.byte	apu_cmd_note_load|apu_notes::A4
	.byte	apu_cmd_note_load|apu_notes::F4
	.byte	apu_cmd_note_load|apu_notes::C4
	.byte	apu_cmd_note_load|apu_notes::F3, $12

	.byte	apu_cmd_wave_vol_change, $0
	.byte	APU_DATA_SFX_DONE
