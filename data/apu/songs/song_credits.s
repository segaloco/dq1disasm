.include	"system/apu.i"
.include	"bssmap.i"
.include	"apu_commands.i"

.segment	"CHR2DATA"

	.export song_credits_addr, song_credits_start, song_credits_end
	.export song_credits_pulse, song_credits_pulse2, song_credits_triangle
song_credits_addr:
.org		GAME_BUFFER
song_credits_start:

	.export song_credits_pulse, song_credits_pulse2, song_credits_triangle
song_credits_pulse:
	.byte	apu_cmd_tempo_change, $82
	.byte	apu_cmd_wave_vol_change, $30
	.byte	$30

	.byte	apu_cmd_wave_vol_change, $46
	.byte	apu_cmd_note_load|apu_notes::G5, $13

	.byte	apu_cmd_note_load|apu_notes::G5, $5

	.byte	apu_cmd_trough, $c
	.byte	apu_cmd_note_load|apu_notes::G5
	.byte	apu_cmd_note_load|apu_notes::G5
	.byte	apu_cmd_note_load|apu_notes::G5
	.byte	apu_cmd_note_load|apu_notes::G5
	.byte	apu_cmd_note_load|apu_notes::G5
	.byte	apu_cmd_note_load|apu_notes::G5
	.byte	apu_cmd_note_load|apu_notes::G5
	.byte	apu_cmd_note_load|apu_notes::G5
	.byte	apu_cmd_note_load|apu_notes::G5
	.byte	apu_cmd_note_load|apu_notes::G5
	.byte	apu_cmd_note_load|apu_notes::F5
	.byte	apu_cmd_note_load|apu_notes::G5
	.byte	apu_cmd_note_load|apu_notes::A5
	.byte	apu_cmd_note_load|apu_notes::G5
	.byte	apu_cmd_note_load|apu_notes::F5
	.byte	apu_cmd_note_load|apu_notes::E5
	.byte	apu_cmd_note_load|apu_notes::F5
	.byte	apu_cmd_note_load|apu_notes::G5
	.byte	apu_cmd_note_load|apu_notes::A5
	.byte	apu_cmd_note_load|apu_notes::G5
	.byte	apu_cmd_note_load|apu_notes::F5
	.byte	apu_cmd_note_load|apu_notes::E5

	.byte	apu_cmd_tempo_change, $7d
	.byte	apu_cmd_note_load|apu_notes::F5
	.byte	apu_cmd_note_load|apu_notes::F5
	.byte	apu_cmd_note_load|apu_notes::F5

	.byte	apu_cmd_tempo_change, $7a
	.byte	apu_cmd_note_load|apu_notes::Ab5
	.byte	apu_cmd_note_load|apu_notes::Ab5
	.byte	apu_cmd_note_load|apu_notes::Ab5

	.byte	apu_cmd_tempo_change, $76
	.byte	apu_cmd_note_load|apu_notes::B5
	.byte	apu_cmd_note_load|apu_notes::B5
	.byte	apu_cmd_note_load|apu_notes::B5

	.byte	apu_cmd_tempo_change, $71
	.byte	apu_cmd_note_load|apu_notes::D6
	.byte	apu_cmd_note_load|apu_notes::D6
	.byte	apu_cmd_note_load|apu_notes::D6

	.byte	apu_cmd_wave_vol_change, $7f
	.byte	apu_cmd_note_load|apu_notes::G6, $54

	.byte	apu_cmd_wave_vol_change, $30
	.byte	$10

	.byte	apu_cmd_tempo_change, $6e
	.byte	apu_cmd_song_jsr
	.addr	:+++
	.byte	apu_cmd_song_jsr
	.addr	:++++
	.byte	apu_cmd_song_jsr
	.addr	:+++
	.byte	apu_cmd_song_jsr
	.addr	:++++
	.byte	apu_cmd_song_jsr
	.addr	:+++

	.byte	apu_cmd_note_load|apu_notes::E5
	.byte	apu_cmd_note_load|apu_notes::F5
	.byte	apu_cmd_note_load|apu_notes::G5, $24

	.byte	apu_cmd_note_load|apu_notes::E5
	.byte	apu_cmd_note_load|apu_notes::Cx5
	.byte	apu_cmd_note_load|apu_notes::D5
	.byte	apu_cmd_note_load|apu_notes::E5
	.byte	apu_cmd_note_load|apu_notes::F5, $c

	.byte	apu_cmd_note_load|apu_notes::G5, $c

	.byte	apu_cmd_note_load|apu_notes::A5, $c

	.byte	apu_cmd_note_load|apu_notes::G5
	.byte	apu_cmd_note_load|apu_notes::F5

	.byte	apu_cmd_wave_vol_change, $49
	.byte	apu_cmd_note_load|apu_notes::E5
	.byte	apu_cmd_note_load|apu_notes::E4

	.byte	apu_cmd_tempo_change, $71
	.byte	apu_cmd_note_load|apu_notes::E4
	.byte	apu_cmd_note_load|apu_notes::A4

	.byte	apu_cmd_tempo_change, $74
	.byte	apu_cmd_note_load|apu_notes::A4
	.byte	apu_cmd_note_load|apu_notes::C5

	.byte	apu_cmd_tempo_change, $77
	.byte	apu_cmd_note_load|apu_notes::E5
	.byte	apu_cmd_note_load|apu_notes::A5

	.byte	apu_cmd_tempo_change, $79
	.byte	apu_cmd_note_load|apu_notes::D5
	.byte	apu_cmd_note_load|apu_notes::F4

	.byte	apu_cmd_tempo_change, $7c
	.byte	apu_cmd_note_load|apu_notes::F4
	.byte	apu_cmd_note_load|apu_notes::G4

	.byte	apu_cmd_tempo_change, $7f
	.byte	apu_cmd_note_load|apu_notes::G4
	.byte	apu_cmd_note_load|apu_notes::B4

	.byte	apu_cmd_tempo_change, $82
	.byte	apu_cmd_note_load|apu_notes::D5
	.byte	apu_cmd_note_load|apu_notes::G5
	.byte	apu_cmd_trough_end

	.byte	apu_cmd_tempo_change, $78
	.byte	apu_cmd_note_load|apu_notes::C6, $18

	.byte	apu_cmd_note_load|apu_notes::C5, $8

	.byte	apu_cmd_note_load|apu_notes::C5, $8

	.byte	apu_cmd_note_load|apu_notes::C5, $8

	.byte	apu_cmd_note_load|apu_notes::C5, $18

	.byte	apu_cmd_note_load|apu_notes::D5, $18

	.byte	apu_cmd_tempo_change, $64
	.byte	apu_cmd_wave_vol_change, $7e
	.byte	apu_cmd_trough, $3
	.byte	apu_cmd_song_jsr
	.addr	:+
	.byte	apu_cmd_song_jsr
	.addr	:+
	.byte	apu_cmd_song_jsr
	.addr	:+
	.byte	apu_cmd_song_jsr
	.addr	:+

	.byte	apu_cmd_tempo_change, $69
	.byte	apu_cmd_song_jsr
	.addr	:++
	.byte	apu_cmd_song_jsr
	.addr	:++
	.byte	apu_cmd_song_jsr
	.addr	:++
	.byte	apu_cmd_song_jsr
	.addr	:++

	.byte	apu_cmd_tempo_change, $66
	.byte	apu_cmd_wave_vol_change, $49
	.byte	apu_cmd_trough, $8
	.byte	apu_cmd_note_load|apu_notes::E5, $10

	.byte	apu_cmd_note_load|apu_notes::C4
	.byte	apu_cmd_note_load|apu_notes::C4
	.byte	apu_cmd_note_load|apu_notes::C4
	.byte	apu_cmd_note_load|apu_notes::C4, $0

:
	.byte	apu_cmd_note_load|apu_notes::C5
	.byte	apu_cmd_note_load|apu_notes::Ab4
	.byte	apu_cmd_note_load|apu_notes::C5
	.byte	apu_cmd_note_load|apu_notes::Ab4
	.byte	apu_cmd_note_load|apu_notes::C5
	.byte	apu_cmd_note_load|apu_notes::Ab4
	.byte	apu_cmd_note_load|apu_notes::C5
	.byte	apu_cmd_note_load|apu_notes::Ab4
	.byte	apu_cmd_song_ret

:
	.byte	apu_cmd_note_load|apu_notes::E5
	.byte	apu_cmd_note_load|apu_notes::C5
	.byte	apu_cmd_note_load|apu_notes::E5
	.byte	apu_cmd_note_load|apu_notes::C5
	.byte	apu_cmd_note_load|apu_notes::E5
	.byte	apu_cmd_note_load|apu_notes::C5
	.byte	apu_cmd_note_load|apu_notes::E5
	.byte	apu_cmd_note_load|apu_notes::C5
	.byte	apu_cmd_song_ret

:
	.byte	apu_cmd_trough_end

	.byte	apu_cmd_wave_vol_change, $8f
	.byte	apu_cmd_note_load|apu_notes::G4, $18

	.byte	apu_cmd_note_load|apu_notes::E5, $14

	.byte	apu_cmd_note_load|apu_notes::E5, $2

	.byte	apu_cmd_note_load|apu_notes::Dx5, $2

	.byte	apu_cmd_note_load|apu_notes::E5, $c

	.byte	apu_cmd_note_load|apu_notes::G5, $c

	.byte	apu_cmd_note_load|apu_notes::D5, $14

	.byte	apu_cmd_note_load|apu_notes::D5, $2

	.byte	apu_cmd_note_load|apu_notes::Cx5, $2

	.byte	apu_cmd_trough, $c
	.byte	apu_cmd_note_load|apu_notes::D5
	.byte	apu_cmd_note_load|apu_notes::G5
	.byte	apu_cmd_note_load|apu_notes::C5, $30

	.byte	apu_cmd_note_load|apu_notes::C5
	.byte	apu_cmd_note_load|apu_notes::D5
	.byte	apu_cmd_note_load|apu_notes::E5
	.byte	apu_cmd_note_load|apu_notes::F5, $c

	.byte	apu_cmd_note_load|apu_notes::G5
	.byte	apu_cmd_note_load|apu_notes::F5
	.byte	apu_cmd_note_load|apu_notes::E5, $c

	.byte	apu_cmd_note_load|apu_notes::F5
	.byte	apu_cmd_note_load|apu_notes::E5
	.byte	apu_cmd_note_load|apu_notes::D5, $30

	.byte	apu_cmd_note_load|apu_notes::D5
	.byte	apu_cmd_note_load|apu_notes::E5
	.byte	apu_cmd_note_load|apu_notes::F5
	.byte	apu_cmd_note_load|apu_notes::G5, $24

	.byte	apu_cmd_note_load|apu_notes::E5
	.byte	apu_cmd_note_load|apu_notes::Cx5
	.byte	apu_cmd_note_load|apu_notes::D5
	.byte	apu_cmd_note_load|apu_notes::E5
	.byte	apu_cmd_note_load|apu_notes::F5, $c

	.byte	apu_cmd_note_load|apu_notes::G5, $c

	.byte	apu_cmd_note_load|apu_notes::A5, $c

	.byte	apu_cmd_note_load|apu_notes::G5
	.byte	apu_cmd_note_load|apu_notes::F5
	.byte	apu_cmd_note_load|apu_notes::E5, $18

	.byte	apu_cmd_note_load|apu_notes::E5
	.byte	apu_cmd_note_load|apu_notes::C5, $c

	.byte	apu_cmd_note_load|apu_notes::A4, $c

	.byte	apu_cmd_note_load|apu_notes::D5, $3c

	.byte	apu_cmd_song_ret

:
	.byte	apu_cmd_wave_vol_change, $bf
	.byte	apu_cmd_note_load|apu_notes::G5, $c

	.byte	apu_cmd_note_load|apu_notes::Ab5, $30

	.byte	apu_cmd_note_load|apu_notes::D5
	.byte	apu_cmd_note_load|apu_notes::Dx5
	.byte	apu_cmd_note_load|apu_notes::F5
	.byte	apu_cmd_note_load|apu_notes::Ax5, $24

	.byte	apu_cmd_note_load|apu_notes::G5, $c

	.byte	apu_cmd_note_load|apu_notes::Dx5, $c

	.byte	apu_cmd_note_load|apu_notes::F5, $30

	.byte	apu_cmd_note_load|apu_notes::Ab5
	.byte	apu_cmd_note_load|apu_notes::G5
	.byte	apu_cmd_note_load|apu_notes::F5

	.byte	apu_cmd_wave_vol_change, $8f
	.byte	apu_cmd_note_load|apu_notes::Dx5, $c

	.byte	apu_cmd_note_load|apu_notes::F5, $c

	.byte	apu_cmd_note_load|apu_notes::G5, $c

	.byte	apu_cmd_wave_vol_change, $bf
	.byte	apu_cmd_note_load|apu_notes::Ab5
	.byte	apu_cmd_note_load|apu_notes::Ax5
	.byte	apu_cmd_note_load|apu_notes::C6, $30

	.byte	apu_cmd_note_load|apu_notes::Dx6
	.byte	apu_cmd_note_load|apu_notes::D6
	.byte	apu_cmd_note_load|apu_notes::C6

	.byte	apu_cmd_wave_vol_change, $8e
	.byte	apu_cmd_trough, $10
	.byte	apu_cmd_note_load|apu_notes::Ax5
	.byte	apu_cmd_note_load|apu_notes::Ab5
	.byte	apu_cmd_note_load|apu_notes::G5
	.byte	apu_cmd_note_load|apu_notes::G5
	.byte	apu_cmd_note_load|apu_notes::F5
	.byte	apu_cmd_note_load|apu_notes::Dx5

	.byte	apu_cmd_trough, $c
	.byte	apu_cmd_wave_vol_change, $bf
	.byte	apu_cmd_note_load|apu_notes::F5, $30

	.byte	apu_cmd_note_load|apu_notes::C5
	.byte	apu_cmd_note_load|apu_notes::D5
	.byte	apu_cmd_note_load|apu_notes::Dx5
	.byte	apu_cmd_note_load|apu_notes::Dx5, $54

	.byte	apu_cmd_note_load|apu_notes::D5, $30

	.byte	apu_cmd_wave_vol_change, $8f
	.byte	$c

	.byte	apu_cmd_song_ret

song_credits_pulse2:
	.byte	apu_cmd_wave_vol_change, $30
	.byte	$30

	.byte	apu_cmd_wave_vol_change, $46
	.byte	apu_cmd_note_load|apu_notes::B4, $13

	.byte	apu_cmd_note_load|apu_notes::B4, $5

	.byte	apu_cmd_trough, $c
	.byte	apu_cmd_note_load|apu_notes::B4
	.byte	apu_cmd_note_load|apu_notes::F5
	.byte	apu_cmd_note_load|apu_notes::F5
	.byte	apu_cmd_note_load|apu_notes::F5
	.byte	apu_cmd_note_load|apu_notes::E5
	.byte	apu_cmd_note_load|apu_notes::E5
	.byte	apu_cmd_note_load|apu_notes::E5
	.byte	apu_cmd_note_load|apu_notes::D5
	.byte	apu_cmd_note_load|apu_notes::C5
	.byte	apu_cmd_note_load|apu_notes::B4
	.byte	apu_cmd_note_load|apu_notes::A4
	.byte	apu_cmd_note_load|apu_notes::B4
	.byte	apu_cmd_note_load|apu_notes::C5
	.byte	apu_cmd_note_load|apu_notes::B4
	.byte	apu_cmd_note_load|apu_notes::A4
	.byte	apu_cmd_note_load|apu_notes::G4
	.byte	apu_cmd_note_load|apu_notes::A4
	.byte	apu_cmd_note_load|apu_notes::B4
	.byte	apu_cmd_note_load|apu_notes::C5
	.byte	apu_cmd_note_load|apu_notes::B4
	.byte	apu_cmd_note_load|apu_notes::A4
	.byte	apu_cmd_note_load|apu_notes::G4
	.byte	apu_cmd_note_load|apu_notes::C5
	.byte	apu_cmd_note_load|apu_notes::C5
	.byte	apu_cmd_note_load|apu_notes::C5
	.byte	apu_cmd_note_load|apu_notes::Dx5
	.byte	apu_cmd_note_load|apu_notes::Dx5
	.byte	apu_cmd_note_load|apu_notes::Dx5
	.byte	apu_cmd_note_load|apu_notes::Fx5
	.byte	apu_cmd_note_load|apu_notes::Fx5
	.byte	apu_cmd_note_load|apu_notes::Fx5
	.byte	apu_cmd_note_load|apu_notes::A5
	.byte	apu_cmd_note_load|apu_notes::A5
	.byte	apu_cmd_note_load|apu_notes::A5

	.byte	apu_cmd_wave_vol_change, $7f
	.byte	apu_cmd_note_load|apu_notes::C6, $3c

	.byte	apu_cmd_note_load|apu_notes::B5, $c

	.byte	apu_cmd_wave_vol_change, $30
	.byte	$28

	.byte	apu_cmd_song_jsr
	.addr	:+++

	.byte	apu_cmd_wave_vol_change, $8f
	.byte	apu_cmd_song_jsr
	.addr	:++++
	.byte	apu_cmd_song_jsr
	.addr	:+++

	.byte	apu_cmd_wave_vol_change, $4f
	.byte	apu_cmd_song_jsr
	.addr	:++++
	.byte	apu_cmd_song_jsr
	.addr	:+++

	.byte	apu_cmd_note_load|apu_notes::C5, $24

	.byte	apu_cmd_note_load|apu_notes::G4, $24

	.byte	apu_cmd_note_load|apu_notes::A4, $54

	.byte	apu_cmd_wave_vol_change, $49
	.byte	apu_cmd_note_load|apu_notes::A4
	.byte	apu_cmd_note_load|apu_notes::A3
	.byte	apu_cmd_note_load|apu_notes::A3
	.byte	apu_cmd_note_load|apu_notes::E4
	.byte	apu_cmd_note_load|apu_notes::E4
	.byte	apu_cmd_note_load|apu_notes::A4
	.byte	apu_cmd_note_load|apu_notes::A4
	.byte	apu_cmd_note_load|apu_notes::E5
	.byte	apu_cmd_note_load|apu_notes::F4
	.byte	apu_cmd_note_load|apu_notes::B3
	.byte	apu_cmd_note_load|apu_notes::B3
	.byte	apu_cmd_note_load|apu_notes::D4
	.byte	apu_cmd_note_load|apu_notes::D4
	.byte	apu_cmd_note_load|apu_notes::F4
	.byte	apu_cmd_note_load|apu_notes::B4
	.byte	apu_cmd_note_load|apu_notes::D5
	.byte	apu_cmd_trough_end

	.byte	apu_cmd_note_load|apu_notes::E5, $18

	.byte	apu_cmd_note_load|apu_notes::Dx4, $8

	.byte	apu_cmd_note_load|apu_notes::Dx4, $8

	.byte	apu_cmd_note_load|apu_notes::Dx4, $8

	.byte	apu_cmd_note_load|apu_notes::Dx4, $18

	.byte	apu_cmd_note_load|apu_notes::F4, $18

	.byte	apu_cmd_wave_vol_change, $7e
	.byte	apu_cmd_trough, $3
	.byte	apu_cmd_song_jsr
	.addr	:+
	.byte	apu_cmd_song_jsr
	.addr	:+
	.byte	apu_cmd_song_jsr
	.addr	:+
	.byte	apu_cmd_song_jsr
	.addr	:+
	.byte	apu_cmd_song_jsr
	.addr	:++
	.byte	apu_cmd_song_jsr
	.addr	:++
	.byte	apu_cmd_song_jsr
	.addr	:++
	.byte	apu_cmd_song_jsr
	.addr	:++

	.byte	apu_cmd_wave_vol_change, $49
	.byte	apu_cmd_trough, $8
	.byte	apu_cmd_note_load|apu_notes::G4, $10

	.byte	apu_cmd_note_load|apu_notes::C3
	.byte	apu_cmd_note_load|apu_notes::C3
	.byte	apu_cmd_note_load|apu_notes::C3
	.byte	apu_cmd_note_load|apu_notes::C3, $0

:
	.byte	apu_cmd_note_load|apu_notes::Dx4
	.byte	apu_cmd_note_load|apu_notes::C4
	.byte	apu_cmd_note_load|apu_notes::Dx4
	.byte	apu_cmd_note_load|apu_notes::C4
	.byte	apu_cmd_note_load|apu_notes::Dx4
	.byte	apu_cmd_note_load|apu_notes::C4
	.byte	apu_cmd_note_load|apu_notes::Dx4
	.byte	apu_cmd_note_load|apu_notes::C4
	.byte	apu_cmd_song_ret

:
	.byte	apu_cmd_note_load|apu_notes::G4
	.byte	apu_cmd_note_load|apu_notes::E4
	.byte	apu_cmd_note_load|apu_notes::G4
	.byte	apu_cmd_note_load|apu_notes::E4
	.byte	apu_cmd_note_load|apu_notes::G4
	.byte	apu_cmd_note_load|apu_notes::E4
	.byte	apu_cmd_note_load|apu_notes::G4
	.byte	apu_cmd_note_load|apu_notes::E4
	.byte	apu_cmd_song_ret

:
	.byte	apu_cmd_trough_end
	.byte	apu_cmd_wave_vol_change, $8f
	.byte	apu_cmd_note_load|apu_notes::G4, $14

	.byte	apu_cmd_note_load|apu_notes::G4, $2

	.byte	apu_cmd_note_load|apu_notes::Fx4, $2

	.byte	apu_cmd_note_load|apu_notes::G4, $6

	.byte	apu_cmd_note_load|apu_notes::G4, $6

	.byte	apu_cmd_note_load|apu_notes::E4, $6

	.byte	apu_cmd_note_load|apu_notes::G4, $6

	.byte	apu_cmd_note_load|apu_notes::G4, $1e

	.byte	apu_cmd_note_load|apu_notes::G4, $6

	.byte	apu_cmd_note_load|apu_notes::F4, $6

	.byte	apu_cmd_note_load|apu_notes::G4, $6

	.byte	apu_cmd_note_load|apu_notes::E4, $14

	.byte	apu_cmd_note_load|apu_notes::E4, $2

	.byte	apu_cmd_note_load|apu_notes::Dx4, $2

	.byte	apu_cmd_trough, $c
	.byte	apu_cmd_note_load|apu_notes::E4
	.byte	apu_cmd_note_load|apu_notes::G4
	.byte	apu_cmd_note_load|apu_notes::F4, $24

	.byte	apu_cmd_note_load|apu_notes::C5, $24

	.byte	apu_cmd_note_load|apu_notes::C5, $c

	.byte	apu_cmd_note_load|apu_notes::Cx5, $18

	.byte	apu_cmd_note_load|apu_notes::C5
	.byte	apu_cmd_note_load|apu_notes::B4
	.byte	apu_cmd_note_load|apu_notes::A4
	.byte	apu_cmd_note_load|apu_notes::B4, $24

	.byte	apu_cmd_note_load|apu_notes::C5, $24

	.byte	apu_cmd_note_load|apu_notes::G4, $24

	.byte	apu_cmd_note_load|apu_notes::A4, $54

	.byte	apu_cmd_note_load|apu_notes::A4
	.byte	apu_cmd_note_load|apu_notes::E4
	.byte	apu_cmd_note_load|apu_notes::C4
	.byte	apu_cmd_note_load|apu_notes::E4
	.byte	apu_cmd_note_load|apu_notes::A4
	.byte	apu_cmd_note_load|apu_notes::E4
	.byte	apu_cmd_note_load|apu_notes::C4
	.byte	apu_cmd_note_load|apu_notes::E4, $c

	.byte	apu_cmd_note_load|apu_notes::C5
	.byte	apu_cmd_note_load|apu_notes::A4
	.byte	apu_cmd_note_load|apu_notes::C5
	.byte	apu_cmd_note_load|apu_notes::B4

	.byte	apu_cmd_wave_vol_change, $89
	.byte	$24

	.byte	apu_cmd_song_ret

:
	.byte	apu_cmd_trough, $6
	.byte	apu_cmd_note_load|apu_notes::Dx4
	.byte	apu_cmd_note_load|apu_notes::C4
	.byte	apu_cmd_note_load|apu_notes::Dx4
	.byte	apu_cmd_note_load|apu_notes::C4
	.byte	apu_cmd_note_load|apu_notes::Dx4
	.byte	apu_cmd_note_load|apu_notes::C4
	.byte	apu_cmd_note_load|apu_notes::Dx4
	.byte	apu_cmd_note_load|apu_notes::C4
	.byte	apu_cmd_note_load|apu_notes::Ab4
	.byte	apu_cmd_note_load|apu_notes::F4
	.byte	apu_cmd_note_load|apu_notes::Ab4
	.byte	apu_cmd_note_load|apu_notes::F4
	.byte	apu_cmd_note_load|apu_notes::Ab4
	.byte	apu_cmd_note_load|apu_notes::F4
	.byte	apu_cmd_note_load|apu_notes::D4
	.byte	apu_cmd_note_load|apu_notes::F4
	.byte	apu_cmd_note_load|apu_notes::G4
	.byte	apu_cmd_note_load|apu_notes::D4
	.byte	apu_cmd_note_load|apu_notes::G4
	.byte	apu_cmd_note_load|apu_notes::D4
	.byte	apu_cmd_note_load|apu_notes::G4
	.byte	apu_cmd_note_load|apu_notes::D4
	.byte	apu_cmd_note_load|apu_notes::G4
	.byte	apu_cmd_note_load|apu_notes::D4
	.byte	apu_cmd_note_load|apu_notes::Dx4
	.byte	apu_cmd_note_load|apu_notes::C4
	.byte	apu_cmd_note_load|apu_notes::Dx4
	.byte	apu_cmd_note_load|apu_notes::C4
	.byte	apu_cmd_note_load|apu_notes::Dx4
	.byte	apu_cmd_note_load|apu_notes::C4
	.byte	apu_cmd_note_load|apu_notes::Dx4
	.byte	apu_cmd_note_load|apu_notes::C4
	.byte	apu_cmd_note_load|apu_notes::C5
	.byte	apu_cmd_note_load|apu_notes::A4
	.byte	apu_cmd_note_load|apu_notes::C5
	.byte	apu_cmd_note_load|apu_notes::A4
	.byte	apu_cmd_note_load|apu_notes::C5
	.byte	apu_cmd_note_load|apu_notes::A4
	.byte	apu_cmd_note_load|apu_notes::C5
	.byte	apu_cmd_note_load|apu_notes::A4
	.byte	apu_cmd_note_load|apu_notes::D5
	.byte	apu_cmd_note_load|apu_notes::B4
	.byte	apu_cmd_note_load|apu_notes::D5
	.byte	apu_cmd_note_load|apu_notes::B4
	.byte	apu_cmd_note_load|apu_notes::D5
	.byte	apu_cmd_note_load|apu_notes::B4
	.byte	apu_cmd_note_load|apu_notes::D5
	.byte	apu_cmd_note_load|apu_notes::B4
	.byte	apu_cmd_note_load|apu_notes::Ax4
	.byte	apu_cmd_note_load|apu_notes::G4
	.byte	apu_cmd_note_load|apu_notes::Ax4
	.byte	apu_cmd_note_load|apu_notes::G4
	.byte	apu_cmd_note_load|apu_notes::Ax4
	.byte	apu_cmd_note_load|apu_notes::F4
	.byte	apu_cmd_note_load|apu_notes::Ax4
	.byte	apu_cmd_note_load|apu_notes::F4
	.byte	apu_cmd_note_load|apu_notes::Ax4
	.byte	apu_cmd_note_load|apu_notes::G4
	.byte	apu_cmd_note_load|apu_notes::Ax4
	.byte	apu_cmd_note_load|apu_notes::G4
	.byte	apu_cmd_note_load|apu_notes::C5
	.byte	apu_cmd_note_load|apu_notes::Ax4
	.byte	apu_cmd_note_load|apu_notes::C5
	.byte	apu_cmd_note_load|apu_notes::Ax4
	.byte	apu_cmd_note_load|apu_notes::Dx4
	.byte	apu_cmd_note_load|apu_notes::Ab4
	.byte	apu_cmd_note_load|apu_notes::C5
	.byte	apu_cmd_note_load|apu_notes::Ab4
	.byte	apu_cmd_note_load|apu_notes::Dx4
	.byte	apu_cmd_note_load|apu_notes::Ab4
	.byte	apu_cmd_note_load|apu_notes::C5
	.byte	apu_cmd_note_load|apu_notes::Ab4
	.byte	apu_cmd_note_load|apu_notes::Dx4
	.byte	apu_cmd_note_load|apu_notes::Ab4
	.byte	apu_cmd_note_load|apu_notes::C5
	.byte	apu_cmd_note_load|apu_notes::Ab4
	.byte	apu_cmd_note_load|apu_notes::F4
	.byte	apu_cmd_note_load|apu_notes::Ax4
	.byte	apu_cmd_note_load|apu_notes::Dx4
	.byte	apu_cmd_note_load|apu_notes::Ab4
	.byte	apu_cmd_note_load|apu_notes::G4
	.byte	apu_cmd_note_load|apu_notes::D4
	.byte	apu_cmd_note_load|apu_notes::Ax3
	.byte	apu_cmd_note_load|apu_notes::D4
	.byte	apu_cmd_note_load|apu_notes::G4
	.byte	apu_cmd_note_load|apu_notes::D4
	.byte	apu_cmd_note_load|apu_notes::Ax3
	.byte	apu_cmd_note_load|apu_notes::D4
	.byte	apu_cmd_note_load|apu_notes::G4
	.byte	apu_cmd_note_load|apu_notes::Dx4
	.byte	apu_cmd_note_load|apu_notes::C4
	.byte	apu_cmd_note_load|apu_notes::Dx4
	.byte	apu_cmd_note_load|apu_notes::G4
	.byte	apu_cmd_note_load|apu_notes::Dx4
	.byte	apu_cmd_note_load|apu_notes::C4
	.byte	apu_cmd_note_load|apu_notes::Dx4
	.byte	apu_cmd_note_load|apu_notes::C4
	.byte	apu_cmd_note_load|apu_notes::A3
	.byte	apu_cmd_note_load|apu_notes::C4
	.byte	apu_cmd_note_load|apu_notes::F4
	.byte	apu_cmd_note_load|apu_notes::C4
	.byte	apu_cmd_note_load|apu_notes::A3
	.byte	apu_cmd_note_load|apu_notes::C4
	.byte	apu_cmd_note_load|apu_notes::F4
	.byte	apu_cmd_note_load|apu_notes::C4
	.byte	apu_cmd_note_load|apu_notes::A3
	.byte	apu_cmd_note_load|apu_notes::C4
	.byte	apu_cmd_note_load|apu_notes::F4
	.byte	apu_cmd_note_load|apu_notes::C4
	.byte	apu_cmd_note_load|apu_notes::A3
	.byte	apu_cmd_note_load|apu_notes::C4
	.byte	apu_cmd_note_load|apu_notes::Dx4
	.byte	apu_cmd_note_load|apu_notes::B4
	.byte	apu_cmd_note_load|apu_notes::G4
	.byte	apu_cmd_note_load|apu_notes::F4
	.byte	apu_cmd_note_load|apu_notes::G4
	.byte	apu_cmd_note_load|apu_notes::B4
	.byte	apu_cmd_note_load|apu_notes::G4
	.byte	apu_cmd_note_load|apu_notes::F4
	.byte	apu_cmd_note_load|apu_notes::G4
	.byte	apu_cmd_note_load|apu_notes::B4
	.byte	apu_cmd_note_load|apu_notes::G4
	.byte	apu_cmd_note_load|apu_notes::F4
	.byte	apu_cmd_note_load|apu_notes::G4
	.byte	apu_cmd_note_load|apu_notes::B4
	.byte	apu_cmd_note_load|apu_notes::G4
	.byte	apu_cmd_note_load|apu_notes::F4
	.byte	apu_cmd_note_load|apu_notes::G4

	.byte	apu_cmd_trough, $c
	.byte	apu_cmd_note_load|apu_notes::C5, $c

	.byte	apu_cmd_note_load|apu_notes::B4
	.byte	apu_cmd_note_load|apu_notes::A4
	.byte	apu_cmd_note_load|apu_notes::B4, $c

	.byte	apu_cmd_wave_vol_change, $89
	.byte	$18

	.byte	apu_cmd_song_ret

song_credits_triangle:
	.byte	apu_cmd_wave_vol_change, $0
	.byte	$30

	.byte	apu_cmd_wave_vol_change, $30
	.byte	apu_cmd_note_load|apu_notes::G3, $6c

	.byte	apu_cmd_trough, $c
	.byte	apu_cmd_note_load|apu_notes::G3
	.byte	apu_cmd_note_load|apu_notes::D4
	.byte	apu_cmd_note_load|apu_notes::G4
	.byte	apu_cmd_note_load|apu_notes::C5
	.byte	apu_cmd_note_load|apu_notes::D5
	.byte	apu_cmd_note_load|apu_notes::F5
	.byte	apu_cmd_note_load|apu_notes::D5
	.byte	apu_cmd_note_load|apu_notes::C5
	.byte	apu_cmd_note_load|apu_notes::B4
	.byte	apu_cmd_note_load|apu_notes::C5
	.byte	apu_cmd_note_load|apu_notes::D5
	.byte	apu_cmd_note_load|apu_notes::F5
	.byte	apu_cmd_note_load|apu_notes::D5
	.byte	apu_cmd_note_load|apu_notes::C5
	.byte	apu_cmd_note_load|apu_notes::B4
	.byte	apu_cmd_note_load|apu_notes::A4
	.byte	apu_cmd_note_load|apu_notes::A4
	.byte	apu_cmd_note_load|apu_notes::A4
	.byte	apu_cmd_note_load|apu_notes::C5
	.byte	apu_cmd_note_load|apu_notes::C5
	.byte	apu_cmd_note_load|apu_notes::C5
	.byte	apu_cmd_note_load|apu_notes::Dx5
	.byte	apu_cmd_note_load|apu_notes::Dx5
	.byte	apu_cmd_note_load|apu_notes::Dx5
	.byte	apu_cmd_note_load|apu_notes::Fx5
	.byte	apu_cmd_note_load|apu_notes::Fx5
	.byte	apu_cmd_note_load|apu_notes::Fx5
	.byte	apu_cmd_note_load|apu_notes::G3
	.byte	apu_cmd_note_load|apu_notes::C4
	.byte	apu_cmd_note_load|apu_notes::D4
	.byte	apu_cmd_note_load|apu_notes::G4
	.byte	apu_cmd_note_load|apu_notes::C5
	.byte	apu_cmd_note_load|apu_notes::D5

	.byte	apu_cmd_wave_vol_change, $60
	.byte	apu_cmd_note_load|apu_notes::G5, $34

	.byte	apu_cmd_next_byte

	.byte	apu_cmd_song_jsr
	.addr	:+
	.byte	apu_cmd_song_jsr
	.addr	:++
	.byte	apu_cmd_song_jsr
	.addr	:+
	.byte	apu_cmd_song_jsr
	.addr	:++
	.byte	apu_cmd_song_jsr
	.addr	:+

	.byte	apu_cmd_wave_vol_change, $ff
	.byte	apu_cmd_note_load|apu_notes::E5
	.byte	apu_cmd_note_load|apu_notes::E4
	.byte	apu_cmd_note_load|apu_notes::A4

	.byte	apu_cmd_next_byte

	.byte	apu_cmd_note_load|apu_notes::A3
	.byte	apu_cmd_note_load|apu_notes::D4
	.byte	apu_cmd_note_load|apu_notes::E4
	.byte	apu_cmd_note_load|apu_notes::F4

	.byte	apu_cmd_next_byte

	.byte	apu_cmd_wave_vol_change, $0
	.byte	$18

	.byte	apu_cmd_wave_vol_change, $30
	.byte	apu_cmd_trough, $c
	.byte	apu_cmd_note_load|apu_notes::Fx3
	.byte	apu_cmd_note_load|apu_notes::Fx4
	.byte	apu_cmd_note_load|apu_notes::Fx4
	.byte	apu_cmd_note_load|apu_notes::C5
	.byte	apu_cmd_note_load|apu_notes::C5

	.byte	apu_cmd_next_byte

	.byte	apu_cmd_note_load|apu_notes::E5
	.byte	apu_cmd_note_load|apu_notes::Fx4
	.byte	apu_cmd_note_load|apu_notes::C6
	.byte	apu_cmd_note_load|apu_notes::G3
	.byte	apu_cmd_note_load|apu_notes::G4
	.byte	apu_cmd_note_load|apu_notes::G4
	.byte	apu_cmd_note_load|apu_notes::B4
	.byte	apu_cmd_note_load|apu_notes::B4

	.byte	apu_cmd_next_byte

	.byte	apu_cmd_note_load|apu_notes::D5
	.byte	apu_cmd_note_load|apu_notes::F5
	.byte	apu_cmd_note_load|apu_notes::B5
	.byte	apu_cmd_trough_end

	.byte	apu_cmd_note_load|apu_notes::C4, $18

	.byte	apu_cmd_note_load|apu_notes::Ab4, $8

	.byte	apu_cmd_note_load|apu_notes::Ab4, $8

	.byte	apu_cmd_note_load|apu_notes::Ab4, $8

	.byte	apu_cmd_note_load|apu_notes::Ab4, $18

	.byte	apu_cmd_next_byte

	.byte	apu_cmd_note_load|apu_notes::Ax4, $18

	.byte	apu_cmd_trough, $c
	.byte	apu_cmd_note_load|apu_notes::Ab4
	.byte	apu_cmd_note_load|apu_notes::Ab3
	.byte	apu_cmd_note_load|apu_notes::Ab4
	.byte	apu_cmd_note_load|apu_notes::Ab3

	.byte	apu_cmd_note_get, $2
	.byte	apu_cmd_note_load|apu_notes::Ab4

	.byte	apu_cmd_next_byte

	.byte	apu_cmd_note_load|apu_notes::Ab3
	.byte	apu_cmd_note_load|apu_notes::Ab4
	.byte	apu_cmd_note_load|apu_notes::Ab3

	.byte	apu_cmd_note_get, $0
	.byte	apu_cmd_note_load|apu_notes::C4
	.byte	apu_cmd_note_load|apu_notes::G3
	.byte	apu_cmd_note_load|apu_notes::C4
	.byte	apu_cmd_note_load|apu_notes::G3
	.byte	apu_cmd_note_load|apu_notes::C4
	.byte	apu_cmd_note_load|apu_notes::G3
	.byte	apu_cmd_note_load|apu_notes::C4

	.byte	apu_cmd_next_byte

	.byte	apu_cmd_note_load|apu_notes::G3

	.byte	apu_cmd_trough, $8
	.byte	apu_cmd_note_load|apu_notes::C4, $10

	.byte	apu_cmd_note_load|apu_notes::C4
	.byte	apu_cmd_note_load|apu_notes::C4
	.byte	apu_cmd_note_load|apu_notes::C4
	.byte	apu_cmd_note_load|apu_notes::C4, $0

:
	.byte	apu_cmd_wave_vol_change, $ff
	.byte	apu_cmd_trough, $18
	.byte	apu_cmd_note_load|apu_notes::C4
	.byte	apu_cmd_note_load|apu_notes::C5
	.byte	apu_cmd_note_load|apu_notes::B4

	.byte	apu_cmd_next_byte

	.byte	apu_cmd_note_load|apu_notes::B3
	.byte	apu_cmd_note_load|apu_notes::Ax3
	.byte	apu_cmd_note_load|apu_notes::Ax4
	.byte	apu_cmd_note_load|apu_notes::A4

	.byte	apu_cmd_next_byte

	.byte	apu_cmd_note_load|apu_notes::A3
	.byte	apu_cmd_note_load|apu_notes::Ab3
	.byte	apu_cmd_note_load|apu_notes::Ab4
	.byte	apu_cmd_note_load|apu_notes::G4

	.byte	apu_cmd_next_byte

	.byte	apu_cmd_note_load|apu_notes::G3
	.byte	apu_cmd_note_load|apu_notes::Fx3
	.byte	apu_cmd_note_load|apu_notes::Fx4
	.byte	apu_cmd_note_load|apu_notes::F4

	.byte	apu_cmd_next_byte

	.byte	apu_cmd_note_load|apu_notes::F5
	.byte	apu_cmd_note_load|apu_notes::E5
	.byte	apu_cmd_note_load|apu_notes::E4
	.byte	apu_cmd_note_load|apu_notes::A4

	.byte	apu_cmd_next_byte

	.byte	apu_cmd_note_load|apu_notes::A3
	.byte	apu_cmd_note_load|apu_notes::D4
	.byte	apu_cmd_note_load|apu_notes::E4
	.byte	apu_cmd_note_load|apu_notes::F4

	.byte	apu_cmd_next_byte
	.byte	apu_cmd_wave_vol_change, $0
	.byte	$18

	.byte	apu_cmd_wave_vol_change, $ff
	.byte	apu_cmd_note_load|apu_notes::Fx4
	.byte	apu_cmd_note_load|apu_notes::Fx3
	.byte	apu_cmd_note_load|apu_notes::Fx4

	.byte	apu_cmd_next_byte
	
	.byte	$18
	.byte	apu_cmd_note_load|apu_notes::G4
	.byte	apu_cmd_note_load|apu_notes::D4
	.byte	apu_cmd_note_load|apu_notes::G3

	.byte	apu_cmd_next_byte

	.byte	apu_cmd_wave_vol_change, $0
	.byte	$18

	.byte	apu_cmd_song_ret

:
	.byte	apu_cmd_trough, $c
	.byte	apu_cmd_wave_vol_change, $60
	.byte	apu_cmd_note_load|apu_notes::F4, $18

	.byte	apu_cmd_note_load|apu_notes::F4

	.byte	apu_cmd_wave_vol_change, $30
	.byte	apu_cmd_note_load|apu_notes::Ax3

	.byte	apu_cmd_next_byte

	.byte	apu_cmd_note_load|apu_notes::Ax3, $c

	.byte	apu_cmd_note_load|apu_notes::Ax3

	.byte	apu_cmd_wave_vol_change, $60
	.byte	apu_cmd_note_load|apu_notes::Dx4, $18

	.byte	apu_cmd_note_load|apu_notes::Dx4

	.byte	apu_cmd_wave_vol_change, $30
	.byte	apu_cmd_note_load|apu_notes::Ab4

	.byte	apu_cmd_next_byte

	.byte	apu_cmd_note_load|apu_notes::Ab4, $c

	.byte	apu_cmd_note_load|apu_notes::Ab3

	.byte	apu_cmd_wave_vol_change, $60
	.byte	apu_cmd_note_load|apu_notes::D4, $18

	.byte	apu_cmd_note_load|apu_notes::D4

	.byte	apu_cmd_wave_vol_change, $30
	.byte	apu_cmd_note_load|apu_notes::G4

	.byte	apu_cmd_next_byte

	.byte	apu_cmd_note_load|apu_notes::G4, $c

	.byte	apu_cmd_note_load|apu_notes::G3
	.byte	apu_cmd_note_load|apu_notes::C4
	.byte	apu_cmd_note_load|apu_notes::C5
	.byte	apu_cmd_note_load|apu_notes::D4
	.byte	apu_cmd_note_load|apu_notes::D5
	.byte	apu_cmd_note_load|apu_notes::Dx4

	.byte	apu_cmd_next_byte

	.byte	apu_cmd_note_load|apu_notes::Dx5
	.byte	apu_cmd_note_load|apu_notes::E4
	.byte	apu_cmd_note_load|apu_notes::E5

	.byte	apu_cmd_wave_vol_change, $60
	.byte	apu_cmd_note_load|apu_notes::F4, $18

	.byte	apu_cmd_note_load|apu_notes::F4

	.byte	apu_cmd_wave_vol_change, $30
	.byte	apu_cmd_note_load|apu_notes::Ax4

	.byte	apu_cmd_next_byte

	.byte	apu_cmd_note_load|apu_notes::Ax4, $c

	.byte	apu_cmd_note_load|apu_notes::Ax3

	.byte	apu_cmd_wave_vol_change, $60
	.byte	apu_cmd_note_load|apu_notes::Dx4, $18

	.byte	apu_cmd_note_load|apu_notes::Dx4

	.byte	apu_cmd_wave_vol_change, $30
	.byte	apu_cmd_note_load|apu_notes::Ab4

	.byte	apu_cmd_next_byte

	.byte	apu_cmd_note_load|apu_notes::Ab4, $c

	.byte	apu_cmd_note_load|apu_notes::Ab3

	.byte	apu_cmd_wave_vol_change, $60
	.byte	apu_cmd_note_load|apu_notes::D4, $18

	.byte	apu_cmd_wave_vol_change, $30
	.byte	apu_cmd_note_load|apu_notes::D4

	.byte	apu_cmd_trough, $18
	.byte	apu_cmd_wave_vol_change, $ff
	.byte	apu_cmd_note_load|apu_notes::D4

	.byte	apu_cmd_next_byte

	.byte	apu_cmd_note_load|apu_notes::Fx4
	.byte	apu_cmd_note_load|apu_notes::G4, $18

	.byte	apu_cmd_note_load|apu_notes::D4

	.byte	apu_cmd_next_byte

	.byte	apu_cmd_note_load|apu_notes::G4
	.byte	apu_cmd_note_load|apu_notes::G4
	.byte	apu_cmd_note_load|apu_notes::D4
	.byte	apu_cmd_note_load|apu_notes::G3

	.byte	apu_cmd_next_byte

	.byte	apu_cmd_wave_vol_change, $0
	.byte	$18

	.byte	apu_cmd_song_ret

song_credits_end:
    .end