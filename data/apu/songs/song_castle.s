.include	"system/apu.i"

.include	"apu_commands.i"

	.export castle_throneroom_pulse, castle_throneroom_triangle
	.export castle_courtyard_pulse, castle_courtyard_triangle
castle_throneroom_pulse:
	.byte	apu_cmd_tempo_change, $7e
		: .byte	apu_cmd_wave_vol_change, $8f
		.byte	apu_cmd_song_jsr
		.addr	castle_common_pulse

		.byte	apu_cmd_wave_vol_change, $82
		.byte	apu_cmd_trough, $6
		.byte   apu_cmd_note_load|apu_notes::A3
		.byte	apu_cmd_song_jsr
		.addr	:+

		.byte   apu_cmd_note_load|apu_notes::G3
		.byte	apu_cmd_song_jsr
		.addr	:+

		.byte	apu_cmd_wave_vol_change, $87
		.byte	apu_cmd_trough, $c
		.byte   apu_cmd_note_load|apu_notes::B4
		.byte   apu_cmd_note_load|apu_notes::G4
		.byte   apu_cmd_note_load|apu_notes::C5
		.byte	apu_cmd_note_load|apu_notes::G4
		.byte	apu_cmd_note_load|apu_notes::F5
		.byte	apu_cmd_note_load|apu_notes::A4
		.byte	apu_cmd_note_load|apu_notes::C5
		.byte	apu_cmd_note_load|apu_notes::A4
		.byte	apu_cmd_note_load|apu_notes::E5
		.byte	apu_cmd_note_load|apu_notes::E4
		.byte	apu_cmd_note_load|apu_notes::Ab4
		.byte	apu_cmd_note_load|apu_notes::B4
		.byte	apu_cmd_note_load|apu_notes::E5
		.byte	apu_cmd_note_load|apu_notes::G4
		.byte	apu_cmd_note_load|apu_notes::Cx5
		.byte	apu_cmd_note_load|apu_notes::E5

		.byte	apu_cmd_wave_vol_change, $82
		.byte	apu_cmd_trough, $6
		.byte	apu_cmd_note_load|apu_notes::D3
		.byte	apu_cmd_song_jsr
		.addr	:++

		.byte	apu_cmd_note_load|apu_notes::C3
		.byte	apu_cmd_song_jsr
		.addr	:++

		.byte	apu_cmd_wave_vol_change, $87
		.byte	apu_cmd_trough, $c
		.byte	apu_cmd_note_load|apu_notes::B4
		.byte	apu_cmd_note_load|apu_notes::G4
		.byte	apu_cmd_note_load|apu_notes::C5
		.byte	apu_cmd_note_load|apu_notes::G4
		.byte	apu_cmd_note_load|apu_notes::F5
		.byte	apu_cmd_note_load|apu_notes::A4
		.byte	apu_cmd_note_load|apu_notes::C5
		.byte	apu_cmd_note_load|apu_notes::F5
		.byte	apu_cmd_note_load|apu_notes::E5
		.byte	apu_cmd_note_load|apu_notes::A4
		.byte	apu_cmd_note_load|apu_notes::Ab4
		.byte	apu_cmd_note_load|apu_notes::F4
		.byte	apu_cmd_note_load|apu_notes::E4
		.byte	apu_cmd_note_load|apu_notes::D4
		.byte	apu_cmd_note_load|apu_notes::C4
		.byte	apu_cmd_note_load|apu_notes::B3
		.byte	apu_cmd_trough_end

		.byte	apu_cmd_song_jsr
		.addr	:-

		: .byte	$6
		.byte	apu_cmd_note_load|apu_notes::A5
		.byte	apu_cmd_note_load|apu_notes::Ab5
		.byte	apu_cmd_note_load|apu_notes::A5, $6
		
		.byte	apu_cmd_note_load|apu_notes::E5
		.byte	apu_cmd_note_load|apu_notes::Dx5
		.byte	apu_cmd_note_load|apu_notes::E5,$6

		.byte	apu_cmd_note_load|apu_notes::C5
		.byte	apu_cmd_note_load|apu_notes::B4
		.byte	apu_cmd_note_load|apu_notes::C5,$6

		.byte	apu_cmd_note_load|apu_notes::A4,$6

		.byte	apu_cmd_song_ret

		: .byte	$6
		.byte	apu_cmd_note_load|apu_notes::A5
		.byte	apu_cmd_note_load|apu_notes::Ab5
		.byte	apu_cmd_note_load|apu_notes::A5,$6

		.byte	apu_cmd_note_load|apu_notes::F5
		.byte	apu_cmd_note_load|apu_notes::E5
		.byte	apu_cmd_note_load|apu_notes::F5,$6

		.byte	apu_cmd_note_load|apu_notes::D5
		.byte	apu_cmd_note_load|apu_notes::Cx5
		.byte	apu_cmd_note_load|apu_notes::D5,$6

		.byte	apu_cmd_note_load|apu_notes::A4,$6

		.byte	apu_cmd_song_ret

castle_throneroom_triangle:
		: .byte	apu_cmd_song_jsr
		.addr	castle_common_triangle

		.byte	apu_cmd_wave_vol_change, $18
		.byte	apu_cmd_trough, $6
		.byte	apu_cmd_note_load|apu_notes::A3,$6

		.byte	apu_cmd_note_load|apu_notes::C5
		.byte	apu_cmd_note_load|apu_notes::B4
		.byte	apu_cmd_note_load|apu_notes::C5,$6

		.byte	apu_cmd_note_load|apu_notes::C5
		.byte	apu_cmd_note_load|apu_notes::B4
		.byte	apu_cmd_note_load|apu_notes::C5,$6

		.byte	apu_cmd_note_load|apu_notes::E5
		.byte	apu_cmd_note_load|apu_notes::Dx5
		.byte	apu_cmd_note_load|apu_notes::E5,$6

		.byte	apu_cmd_note_load|apu_notes::C5,$6

		.byte	apu_cmd_note_load|apu_notes::G3,$6

		.byte	apu_cmd_note_load|apu_notes::C5
		.byte	apu_cmd_note_load|apu_notes::B4
		.byte	apu_cmd_note_load|apu_notes::C5,$6

		.byte	apu_cmd_note_load|apu_notes::C5
		.byte	apu_cmd_note_load|apu_notes::B4
		.byte	apu_cmd_note_load|apu_notes::Fx4,$6

		.byte	apu_cmd_note_load|apu_notes::E5
		.byte	apu_cmd_note_load|apu_notes::Dx5
		.byte	apu_cmd_note_load|apu_notes::E5,$6

		.byte	apu_cmd_note_load|apu_notes::C5,$6

		.byte	apu_cmd_wave_vol_change, $ff
		.byte	apu_cmd_note_load|apu_notes::F4,$12

		.byte	apu_cmd_note_load|apu_notes::E4,$12

		.byte	apu_cmd_note_load|apu_notes::D4,$12

		.byte	apu_cmd_note_load|apu_notes::Dx4,$12

		.byte	apu_cmd_note_load|apu_notes::E4,$2a

		.byte	apu_cmd_note_load|apu_notes::A4,$2a

		.byte	apu_cmd_wave_vol_change, $18
		.byte	apu_cmd_note_load|apu_notes::D4,$6

		.byte	apu_cmd_note_load|apu_notes::F5
		.byte	apu_cmd_note_load|apu_notes::E5
		.byte	apu_cmd_note_load|apu_notes::F5,$6

		.byte	apu_cmd_note_load|apu_notes::D5
		.byte	apu_cmd_note_load|apu_notes::Cx5
		.byte	apu_cmd_note_load|apu_notes::D5,$6

		.byte	apu_cmd_note_load|apu_notes::F5
		.byte	apu_cmd_note_load|apu_notes::E5
		.byte	apu_cmd_note_load|apu_notes::F5,$6

		.byte	apu_cmd_note_load|apu_notes::D5,$6

		.byte	apu_cmd_note_load|apu_notes::C4,$6

		.byte	apu_cmd_note_load|apu_notes::F5
		.byte	apu_cmd_note_load|apu_notes::E5
		.byte	apu_cmd_note_load|apu_notes::F5,$6

		.byte	apu_cmd_note_load|apu_notes::D5
		.byte	apu_cmd_note_load|apu_notes::Cx5
		.byte	apu_cmd_note_load|apu_notes::B3,$6

		.byte	apu_cmd_note_load|apu_notes::F5
		.byte	apu_cmd_note_load|apu_notes::E5
		.byte	apu_cmd_note_load|apu_notes::F5,$6

		.byte	apu_cmd_note_load|apu_notes::D5,$6

		.byte	apu_cmd_wave_vol_change, $ff
		.byte	apu_cmd_note_load|apu_notes::F4,$12

		.byte	apu_cmd_note_load|apu_notes::E4,$12

		.byte	apu_cmd_note_load|apu_notes::D4,$2a

		.byte	apu_cmd_trough, $c
		.byte	apu_cmd_note_load|apu_notes::E4
		.byte	apu_cmd_note_load|apu_notes::F5
		.byte	apu_cmd_note_load|apu_notes::E5
		.byte	apu_cmd_note_load|apu_notes::D5
		.byte	apu_cmd_note_load|apu_notes::C5
		.byte	apu_cmd_note_load|apu_notes::B4
		.byte	apu_cmd_note_load|apu_notes::A4
		.byte	apu_cmd_note_load|apu_notes::Ab4
		.byte	apu_cmd_song_jsr
		.addr	:-

    .export castle_courtyard_pulse, castle_courtyard_triangle
castle_courtyard_pulse:
	.byte	apu_cmd_tempo_change, $7d
	.byte	apu_cmd_wave_vol_change, $4f
	.byte	apu_cmd_song_jsr
	.addr	castle_common_pulse

	.byte	apu_cmd_song_jsr
	.addr	castle_courtyard_pulse

castle_courtyard_triangle:
	.byte	apu_cmd_song_jsr
	.addr	castle_common_triangle

	.byte	apu_cmd_song_jsr
	.addr	castle_courtyard_triangle

castle_common_pulse:
	.byte	apu_cmd_trough, $c
	.byte	apu_cmd_note_load|apu_notes::A3
	.byte	apu_cmd_note_load|apu_notes::E5
	.byte	apu_cmd_note_load|apu_notes::D5
	.byte	apu_cmd_note_load|apu_notes::E5
	.byte	apu_cmd_note_load|apu_notes::C5
	.byte	apu_cmd_note_load|apu_notes::E5
	.byte	apu_cmd_note_load|apu_notes::B4
	.byte	apu_cmd_note_load|apu_notes::E5
	.byte	apu_cmd_note_load|apu_notes::A4, $54

	.byte	apu_cmd_note_load|apu_notes::D3
	.byte	apu_cmd_note_load|apu_notes::F5
	.byte	apu_cmd_note_load|apu_notes::E5
	.byte	apu_cmd_note_load|apu_notes::F5
	.byte	apu_cmd_note_load|apu_notes::D5
	.byte	apu_cmd_note_load|apu_notes::F5
	.byte	apu_cmd_note_load|apu_notes::C5
	.byte	apu_cmd_note_load|apu_notes::F5
	.byte	apu_cmd_note_load|apu_notes::B4, $54

	.byte	apu_cmd_note_load|apu_notes::A3
	.byte	apu_cmd_note_load|apu_notes::G5
	.byte	apu_cmd_note_load|apu_notes::F5
	.byte	apu_cmd_note_load|apu_notes::G5
	.byte	apu_cmd_note_load|apu_notes::E5
	.byte	apu_cmd_note_load|apu_notes::G5
	.byte	apu_cmd_note_load|apu_notes::Cx5
	.byte	apu_cmd_note_load|apu_notes::G5
	.byte	apu_cmd_note_load|apu_notes::F5, $c

	.byte	apu_cmd_note_load|apu_notes::G5, $c

	.byte	apu_cmd_note_load|apu_notes::A5, $c

	.byte	apu_cmd_note_load|apu_notes::G5
	.byte	apu_cmd_note_load|apu_notes::F5
	.byte	apu_cmd_note_load|apu_notes::E5, $c

	.byte	apu_cmd_note_load|apu_notes::C5
	.byte	apu_cmd_note_load|apu_notes::E5
	.byte	apu_cmd_note_load|apu_notes::D5, $c
	
	.byte	apu_cmd_note_load|apu_notes::Dx5, $c

	.byte	apu_cmd_note_load|apu_notes::E5, $54

	.byte	apu_cmd_song_ret

castle_common_triangle:
	.byte	apu_cmd_trough, $c
	.byte	apu_cmd_wave_vol_change, $60
	.byte	apu_cmd_note_load|apu_notes::A3, $54
	
	.byte	apu_cmd_note_load|apu_notes::A4
	.byte	apu_cmd_note_load|apu_notes::A3
	.byte	apu_cmd_note_load|apu_notes::C4
	.byte	apu_cmd_note_load|apu_notes::E4
	.byte	apu_cmd_note_load|apu_notes::A4
	.byte	apu_cmd_note_load|apu_notes::G4
	.byte	apu_cmd_note_load|apu_notes::F4
	.byte	apu_cmd_note_load|apu_notes::E4
	.byte	apu_cmd_note_load|apu_notes::D4
	.byte	apu_cmd_note_load|apu_notes::D5
	.byte	apu_cmd_note_load|apu_notes::C5
	.byte	apu_cmd_note_load|apu_notes::D5
	.byte	apu_cmd_note_load|apu_notes::B4
	.byte	apu_cmd_note_load|apu_notes::D5
	.byte	apu_cmd_note_load|apu_notes::A4
	.byte	apu_cmd_note_load|apu_notes::D5
	.byte	apu_cmd_note_load|apu_notes::Ab4
	.byte	apu_cmd_note_load|apu_notes::E4
	.byte	apu_cmd_note_load|apu_notes::Ab4
	.byte	apu_cmd_note_load|apu_notes::B4
	.byte	apu_cmd_note_load|apu_notes::E5
	.byte	apu_cmd_note_load|apu_notes::E4
	.byte	apu_cmd_note_load|apu_notes::Fx4
	.byte	apu_cmd_note_load|apu_notes::Ab4
	.byte	apu_cmd_note_load|apu_notes::A4
	.byte	apu_cmd_note_load|apu_notes::E5
	.byte	apu_cmd_note_load|apu_notes::D5
	.byte	apu_cmd_note_load|apu_notes::E5
	.byte	apu_cmd_note_load|apu_notes::Cx5
	.byte	apu_cmd_note_load|apu_notes::E5
	.byte	apu_cmd_note_load|apu_notes::A4
	.byte	apu_cmd_note_load|apu_notes::E5
	.byte	apu_cmd_note_load|apu_notes::D5
	.byte	apu_cmd_note_load|apu_notes::A4
	.byte	apu_cmd_note_load|apu_notes::E5
	.byte	apu_cmd_note_load|apu_notes::A4
	.byte	apu_cmd_note_load|apu_notes::F5
	.byte	apu_cmd_note_load|apu_notes::A4
	.byte	apu_cmd_note_load|apu_notes::E5
	.byte	apu_cmd_note_load|apu_notes::D5
	.byte	apu_cmd_note_load|apu_notes::C5
	.byte	apu_cmd_note_load|apu_notes::G4
	.byte	apu_cmd_note_load|apu_notes::E5
	.byte	apu_cmd_note_load|apu_notes::G4
	.byte	apu_cmd_note_load|apu_notes::B4
	.byte	apu_cmd_note_load|apu_notes::F5
	.byte	apu_cmd_note_load|apu_notes::A4
	.byte	apu_cmd_note_load|apu_notes::Fx5
	.byte	apu_cmd_note_load|apu_notes::Ab5
	.byte	apu_cmd_note_load|apu_notes::E4
	.byte	apu_cmd_note_load|apu_notes::Ab4
	.byte	apu_cmd_note_load|apu_notes::B4
	.byte	apu_cmd_note_load|apu_notes::E5
	.byte	apu_cmd_note_load|apu_notes::D5
	.byte	apu_cmd_note_load|apu_notes::C5
	.byte	apu_cmd_note_load|apu_notes::B4
	.byte	apu_cmd_song_ret
