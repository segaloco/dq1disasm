.include	"system/apu.i"

.include	"apu_commands.i"

	.export song_lyre_pulse1, song_lyre_pulse2
song_lyre_pulse2:
	.byte	$3

song_lyre_pulse1:
	.byte	apu_cmd_wave_vol_change, $30
	.byte	$6

	.byte	apu_cmd_wave_vol_change, $89
	.byte	apu_cmd_tempo_change, $3c
	.byte	apu_cmd_note_load|apu_notes::A6, $6

	.byte	apu_cmd_note_load|apu_notes::F6, $5

	.byte	apu_cmd_note_load|apu_notes::D6, $4

	.byte	apu_cmd_trough, $3
	.byte	apu_cmd_note_load|apu_notes::B5

	.byte	apu_cmd_tempo_change, $46
	.byte	apu_cmd_note_load|apu_notes::F6
	.byte	apu_cmd_note_load|apu_notes::D6
	.byte	apu_cmd_note_load|apu_notes::B5
	.byte	apu_cmd_note_load|apu_notes::A5

	.byte	apu_cmd_tempo_change, $50
	.byte	apu_cmd_note_load|apu_notes::D6
	.byte	apu_cmd_note_load|apu_notes::B5
	.byte	apu_cmd_note_load|apu_notes::A5
	.byte	apu_cmd_note_load|apu_notes::F5

	.byte	apu_cmd_tempo_change, $5a
	.byte	apu_cmd_note_load|apu_notes::A5
	.byte	apu_cmd_note_load|apu_notes::F5
	.byte	apu_cmd_note_load|apu_notes::D5
	.byte	apu_cmd_note_load|apu_notes::B4

	.byte	apu_cmd_tempo_change, $64
	.byte	apu_cmd_note_load|apu_notes::F5
	.byte	apu_cmd_note_load|apu_notes::D5
	.byte	apu_cmd_note_load|apu_notes::B4
	.byte	apu_cmd_note_load|apu_notes::A4

	.byte	apu_cmd_tempo_change, $6d
	.byte	apu_cmd_note_load|apu_notes::D5
	.byte	apu_cmd_note_load|apu_notes::B4
	.byte	apu_cmd_note_load|apu_notes::A4
	.byte	apu_cmd_note_load|apu_notes::F4

	.byte	apu_cmd_tempo_change, $76
	.byte	apu_cmd_note_load|apu_notes::A4
	.byte	apu_cmd_note_load|apu_notes::F4
	.byte	apu_cmd_note_load|apu_notes::D4
	.byte	apu_cmd_note_load|apu_notes::B3

	.byte	apu_cmd_tempo_change, $7f
	.byte	apu_cmd_note_load|apu_notes::F4
	.byte	apu_cmd_note_load|apu_notes::D4
	.byte	apu_cmd_note_load|apu_notes::B3
	.byte	apu_cmd_note_load|apu_notes::A3

	.byte	apu_cmd_tempo_change, $88
	.byte	apu_cmd_note_load|apu_notes::D4
	.byte	apu_cmd_note_load|apu_notes::B3
	.byte	apu_cmd_note_load|apu_notes::A3
	.byte	apu_cmd_note_load|apu_notes::F3

	.byte	apu_cmd_tempo_change, $90
	.byte	apu_cmd_note_load|apu_notes::G2
	.byte	apu_cmd_note_load|apu_notes::D3
	.byte	apu_cmd_note_load|apu_notes::F3
	.byte	apu_cmd_note_load|apu_notes::A3
	.byte	apu_cmd_note_load|apu_notes::B3
	.byte	apu_cmd_note_load|apu_notes::D4
	.byte	apu_cmd_note_load|apu_notes::B3
	.byte	apu_cmd_note_load|apu_notes::D4
	.byte	apu_cmd_note_load|apu_notes::F4
	.byte	apu_cmd_note_load|apu_notes::A4
	.byte	apu_cmd_note_load|apu_notes::B4
	.byte	apu_cmd_note_load|apu_notes::D5
	.byte	apu_cmd_note_load|apu_notes::B4
	.byte	apu_cmd_note_load|apu_notes::D5
	.byte	apu_cmd_note_load|apu_notes::F5
	.byte	apu_cmd_note_load|apu_notes::A5
	.byte	apu_cmd_note_load|apu_notes::B5
	.byte	apu_cmd_note_load|apu_notes::D6
	.byte	apu_cmd_trough_end

	.byte	apu_cmd_wave_vol_change, $8f
	.byte	apu_cmd_note_load|apu_notes::G6, $30

	.byte	APU_DATA_SFX_DONE

	.byte	apu_cmd_next_byte
