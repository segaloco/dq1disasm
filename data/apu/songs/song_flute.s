.include	"system/apu.i"

.include	"apu_commands.i"

	.export song_flute_triangle
song_flute_triangle:
	.byte	apu_cmd_note_get, $c
	.byte	apu_cmd_tempo_change, $78
	.byte	apu_cmd_wave_vol_change, $ff
	.byte	apu_cmd_note_load|apu_notes::G4, $18

	.byte	apu_cmd_trough, $3
	.byte	apu_cmd_note_load|apu_notes::A4
	.byte	apu_cmd_note_load|apu_notes::B4
	.byte	apu_cmd_note_load|apu_notes::C5
	.byte	apu_cmd_note_load|apu_notes::D5
	.byte	apu_cmd_note_load|apu_notes::E5
	.byte	apu_cmd_note_load|apu_notes::F5
	.byte	apu_cmd_note_load|apu_notes::G5
	.byte	apu_cmd_note_load|apu_notes::A5
	.byte	apu_cmd_note_load|apu_notes::B5
	.byte	apu_cmd_note_load|apu_notes::C6
	.byte	apu_cmd_note_load|apu_notes::D6
	.byte	apu_cmd_note_load|apu_notes::E6
	.byte	apu_cmd_note_load|apu_notes::F6
	.byte	apu_cmd_trough_end

	.byte	apu_cmd_wave_vol_change, $20
	.byte	apu_cmd_note_load|apu_notes::G6, $11

	.byte	apu_cmd_note_load|apu_notes::G6, $10
	
	.byte	apu_cmd_note_load|apu_notes::G6, $10

	.byte	apu_cmd_wave_vol_change, $ff
	.byte	apu_cmd_trough, $2
	.byte	apu_cmd_note_load|apu_notes::G6
	.byte	apu_cmd_note_load|apu_notes::A6
	.byte	apu_cmd_note_load|apu_notes::G6
	.byte	apu_cmd_note_load|apu_notes::A6
	.byte	apu_cmd_note_load|apu_notes::G6
	.byte	apu_cmd_note_load|apu_notes::A6
	.byte	apu_cmd_note_load|apu_notes::G6
	.byte	apu_cmd_note_load|apu_notes::A6
	.byte	apu_cmd_note_load|apu_notes::G6
	.byte	apu_cmd_note_load|apu_notes::A6
	.byte	apu_cmd_note_load|apu_notes::G6
	.byte	apu_cmd_note_load|apu_notes::A6
	.byte	apu_cmd_note_load|apu_notes::G6
	.byte	apu_cmd_note_load|apu_notes::A6
	.byte	apu_cmd_note_load|apu_notes::G6
	.byte	apu_cmd_note_load|apu_notes::A6
	.byte	apu_cmd_note_load|apu_notes::G6
	.byte	apu_cmd_note_load|apu_notes::A6
	.byte	apu_cmd_trough_end

	.byte	apu_cmd_note_load|apu_notes::G6, $d

	.byte	apu_cmd_note_load|apu_notes::F6, $d

	.byte	apu_cmd_note_load|apu_notes::D6, $8

	.byte	apu_cmd_note_load|apu_notes::B5, $8

	.byte	apu_cmd_note_load|apu_notes::A5, $8

	.byte	apu_cmd_note_load|apu_notes::G5, $30

	.byte	apu_cmd_wave_vol_change, $0
	.byte	APU_DATA_SFX_DONE

	.byte	apu_cmd_next_byte
