.include	"system/apu.i"

.include	"apu_commands.i"

	.export song_levelup_pulse1, song_levelup_pulse2
song_levelup_pulse1:
	.byte	apu_cmd_tempo_change, $50
	.byte	apu_cmd_wave_vol_change, $42
	.byte	apu_cmd_note_load|apu_notes::F5, $4
	.byte	apu_cmd_note_load|apu_notes::F5, $4
	.byte	apu_cmd_note_load|apu_notes::F5, $4
	.byte	apu_cmd_note_load|apu_notes::F5, $8
	.byte	apu_cmd_note_load|apu_notes::Dx5, $8
	.byte	apu_cmd_note_load|apu_notes::G5, $8

	.byte	apu_cmd_wave_vol_change, $4f
	.byte	apu_cmd_note_load|apu_notes::F5, $18
	.byte	APU_DATA_SFX_DONE

	.byte	apu_cmd_next_byte

song_levelup_pulse2:
	.byte	apu_cmd_wave_vol_change, $42
	.byte	apu_cmd_note_load|apu_notes::C5, $4
	.byte	apu_cmd_note_load|apu_notes::B4, $4
	.byte	apu_cmd_note_load|apu_notes::Ax4, $4
	.byte	apu_cmd_note_load|apu_notes::A4, $8
	.byte	apu_cmd_note_load|apu_notes::G4, $8
	.byte	apu_cmd_note_load|apu_notes::Ax4, $8

	.byte	apu_cmd_wave_vol_change, $4f
	.byte	apu_cmd_note_load|apu_notes::A4, $18
	.byte	APU_DATA_SFX_DONE
