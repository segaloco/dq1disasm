.include	"system/apu.i"

.include	"apu_commands.i"

	.export song_lose_pulse1, song_lose_pulse2
song_lose_pulse2:
	.byte	$2

song_lose_pulse1:
	.byte	apu_cmd_tempo_change, $96
	.byte	apu_cmd_wave_vol_change, $30
	.byte	$18

	.byte	apu_cmd_wave_vol_change, $4f
	.byte	apu_cmd_trough, $c
	.byte	apu_cmd_note_load|apu_notes::F5
	.byte	apu_cmd_note_load|apu_notes::D4
	.byte	apu_cmd_note_load|apu_notes::A4
	.byte	apu_cmd_note_load|apu_notes::F5
	.byte	apu_cmd_note_load|apu_notes::E5
	.byte	apu_cmd_note_load|apu_notes::Cx4
	.byte	apu_cmd_note_load|apu_notes::A4
	.byte	apu_cmd_note_load|apu_notes::E5
	.byte	apu_cmd_note_load|apu_notes::D5
	.byte	apu_cmd_note_load|apu_notes::Ax3
	.byte	apu_cmd_note_load|apu_notes::G4
	.byte	apu_cmd_note_load|apu_notes::D5
	.byte	apu_cmd_trough_end

	.byte	apu_cmd_note_load|apu_notes::Cx5, $d
	.byte	apu_cmd_note_load|apu_notes::Ax4, $e
	.byte	apu_cmd_note_load|apu_notes::A4, $f
	.byte	apu_cmd_note_load|apu_notes::Ab4, $f
	.byte	apu_cmd_note_load|apu_notes::A4, $4
	.byte	apu_cmd_note_load|apu_notes::Ab4, $4
	.byte	apu_cmd_note_load|apu_notes::A4, $30
	.byte	APU_DATA_SFX_DONE

	.byte	apu_cmd_next_byte
