.include	"system/apu.i"

.include	"apu_commands.i"

	.export song_inn_pulse1, song_inn_pulse2
song_inn_pulse1:
	.byte	apu_cmd_tempo_change, $78
	.byte	apu_cmd_wave_vol_change, $30
	.byte	$18

	.byte	apu_cmd_wave_vol_change, $82
	.byte	apu_cmd_note_load|apu_notes::C5, $6

	.byte	apu_cmd_note_load|apu_notes::D5, $6

	.byte	apu_cmd_note_load|apu_notes::C5, $6

	.byte	apu_cmd_note_load|apu_notes::D5, $6

	.byte	apu_cmd_note_load|apu_notes::E5, $c

	.byte	apu_cmd_note_load|apu_notes::G5, $c

	.byte	apu_cmd_note_load|apu_notes::C5, $2

	.byte	apu_cmd_note_load|apu_notes::E5, $2

	.byte	apu_cmd_note_load|apu_notes::G5, $2

	.byte	apu_cmd_wave_vol_change, $8f
	.byte	apu_cmd_note_load|apu_notes::C6, $42

	.byte	APU_DATA_SFX_DONE

	.byte	apu_cmd_next_byte

song_inn_pulse2:
	.byte	apu_cmd_wave_vol_change, $30
	.byte	$18

	.byte	apu_cmd_wave_vol_change, $82

	.byte	apu_cmd_note_load|apu_notes::E4, $6

	.byte	apu_cmd_note_load|apu_notes::F4, $6

	.byte	apu_cmd_note_load|apu_notes::E4, $6

	.byte	apu_cmd_note_load|apu_notes::F4, $6

	.byte	apu_cmd_note_load|apu_notes::G4, $c

	.byte	apu_cmd_note_load|apu_notes::Ax4, $c

	.byte	apu_cmd_note_load|apu_notes::E4, $2

	.byte	apu_cmd_note_load|apu_notes::G4, $2

	.byte	apu_cmd_note_load|apu_notes::Ax4, $2

	.byte	apu_cmd_wave_vol_change, $8f
	.byte	apu_cmd_note_load|apu_notes::E5, $42

	.byte	APU_DATA_SFX_DONE
