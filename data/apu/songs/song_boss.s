.include	"system/apu.i"

.include	"apu_commands.i"

	.export song_boss_pulse1, song_boss_pulse2, song_boss_triangle
song_boss_pulse1:
	.byte	apu_cmd_tempo_change, $50

		: .byte	apu_cmd_song_jsr
		.addr	:+

		.byte	apu_cmd_song_jsr
		.addr	:++

		.byte	apu_cmd_song_jsr
		.addr	:+

		.byte	apu_cmd_trough, $6
		.byte	apu_cmd_wave_vol_change, $f
		.byte	apu_cmd_note_load|apu_notes::Cx4
		.byte	apu_cmd_note_load|apu_notes::Dx4
		.byte	apu_cmd_note_load|apu_notes::E4
		.byte	apu_cmd_note_load|apu_notes::Fx4
		.byte	apu_cmd_note_load|apu_notes::G4, $1e

		.byte	apu_cmd_note_load|apu_notes::Ax4
		.byte	apu_cmd_note_load|apu_notes::G4
		.byte	apu_cmd_note_load|apu_notes::Cx5, $6

		.byte	apu_cmd_note_load|apu_notes::C5
		.byte	apu_cmd_note_load|apu_notes::Ax4
		.byte	apu_cmd_note_load|apu_notes::A4, $6

		.byte	apu_cmd_note_load|apu_notes::G4, $6

		.byte	apu_cmd_note_load|apu_notes::A4, $6

		.byte	apu_cmd_note_load|apu_notes::Ax4, $12

		.byte	apu_cmd_note_load|apu_notes::C5
		.byte	apu_cmd_note_load|apu_notes::Ax4
		.byte	apu_cmd_note_load|apu_notes::A4, $6

		.byte	apu_cmd_note_load|apu_notes::G4, $6

		.byte	apu_cmd_note_load|apu_notes::A4
		.byte	apu_cmd_note_load|apu_notes::Ax4
		.byte	apu_cmd_note_load|apu_notes::A4
		.byte	apu_cmd_note_load|apu_notes::G4
		.byte	apu_cmd_note_load|apu_notes::Fx4, $6

		.byte	apu_cmd_note_load|apu_notes::E4, $6

		.byte	apu_cmd_note_load|apu_notes::A4
		.byte	apu_cmd_note_load|apu_notes::G4
		.byte	apu_cmd_note_load|apu_notes::Fx4
		.byte	apu_cmd_note_load|apu_notes::E4

		.byte	apu_cmd_wave_vol_change, $3f
		.byte	apu_cmd_note_load|apu_notes::Dx4, $2a

		.byte	apu_cmd_wave_vol_change, $f
		.byte	$18

		.byte	apu_cmd_song_jsr
		.addr	:-

		: .byte	apu_cmd_trough, $c
		.byte	apu_cmd_wave_vol_change, $82
		.byte	apu_cmd_note_load|apu_notes::A3
		.byte	apu_cmd_note_load|apu_notes::B3

		.byte	apu_cmd_wave_vol_change, $2
		.byte	apu_cmd_note_load|apu_notes::A3
		.byte	apu_cmd_note_load|apu_notes::B3

		.byte	apu_cmd_wave_vol_change, $82
		.byte	apu_cmd_note_load|apu_notes::A3
		.byte	apu_cmd_note_load|apu_notes::B3

		.byte	apu_cmd_wave_vol_change, $2
		.byte	apu_cmd_note_load|apu_notes::A3
		.byte	apu_cmd_note_load|apu_notes::B3
		.byte	apu_cmd_song_ret

		: .byte	apu_cmd_trough, $6
		.byte	apu_cmd_wave_vol_change, $f
		.byte	apu_cmd_note_load|apu_notes::A3
		.byte	apu_cmd_note_load|apu_notes::B3
		.byte	apu_cmd_note_load|apu_notes::C4
		.byte	apu_cmd_note_load|apu_notes::D4
		.byte	apu_cmd_note_load|apu_notes::Dx4, $1e

		.byte	apu_cmd_note_load|apu_notes::Fx4
		.byte	apu_cmd_note_load|apu_notes::Dx4
		.byte	apu_cmd_note_load|apu_notes::A4, $6

		.byte	apu_cmd_note_load|apu_notes::Ab4
		.byte	apu_cmd_note_load|apu_notes::Fx4
		.byte	apu_cmd_note_load|apu_notes::F4, $6

		.byte	apu_cmd_note_load|apu_notes::Dx4, $6

		.byte	apu_cmd_note_load|apu_notes::F4, $6

		.byte	apu_cmd_note_load|apu_notes::Fx4, $12

		.byte	apu_cmd_note_load|apu_notes::Ab4
		.byte	apu_cmd_note_load|apu_notes::Fx4
		.byte	apu_cmd_note_load|apu_notes::F4, $6

		.byte	apu_cmd_note_load|apu_notes::Dx4, $6

		.byte	apu_cmd_note_load|apu_notes::F4
		.byte	apu_cmd_note_load|apu_notes::Fx4
		.byte	apu_cmd_note_load|apu_notes::F4
		.byte	apu_cmd_note_load|apu_notes::Dx4
		.byte	apu_cmd_note_load|apu_notes::D4, $6

		.byte	apu_cmd_note_load|apu_notes::C4, $6

		.byte	apu_cmd_note_load|apu_notes::F4
		.byte	apu_cmd_note_load|apu_notes::Dx4
		.byte	apu_cmd_note_load|apu_notes::D4
		.byte	apu_cmd_note_load|apu_notes::C4

		.byte	apu_cmd_wave_vol_change, $3f
		.byte	apu_cmd_note_load|apu_notes::B3, $2a

		.byte	apu_cmd_wave_vol_change, $f
		.byte	$18

		.byte	apu_cmd_song_ret

song_boss_pulse2:
		: .byte	apu_cmd_trough, $c
		.byte	apu_cmd_song_jsr
		.addr	:+

		.byte	apu_cmd_song_jsr
		.addr	:++

		.byte	apu_cmd_song_jsr
		.addr	:++

		.byte	apu_cmd_song_jsr
		.addr	:++

		.byte	apu_cmd_song_jsr
		.addr	:+++

		.byte	apu_cmd_song_jsr
		.addr	:+

		.byte	apu_cmd_song_jsr
		.addr	:--

		.byte	apu_cmd_song_jsr
		.addr	:-

		: .byte	apu_cmd_wave_vol_change, $82
		.byte	apu_cmd_note_load|apu_notes::E3
		.byte	apu_cmd_note_load|apu_notes::D3

		.byte	apu_cmd_wave_vol_change, $2
		.byte	apu_cmd_note_load|apu_notes::A2
		.byte	apu_cmd_note_load|apu_notes::D3

		.byte	apu_cmd_wave_vol_change, $82
		.byte	apu_cmd_note_load|apu_notes::E3
		.byte	apu_cmd_note_load|apu_notes::D3

		.byte	apu_cmd_wave_vol_change, $2
		.byte	apu_cmd_note_load|apu_notes::A2
		.byte	apu_cmd_note_load|apu_notes::D3
		.byte	apu_cmd_song_ret

		: .byte	apu_cmd_note_load|apu_notes::Fx3
		.byte	apu_cmd_note_load|apu_notes::Dx3
		.byte	apu_cmd_note_load|apu_notes::Fx3
		.byte	apu_cmd_note_load|apu_notes::Dx3

		: .byte	apu_cmd_note_load|apu_notes::Fx3
		.byte	apu_cmd_note_load|apu_notes::Dx3
		.byte	apu_cmd_note_load|apu_notes::Fx3
		.byte	apu_cmd_note_load|apu_notes::Dx3
		.byte	apu_cmd_song_ret

song_boss_triangle:
	.byte	apu_cmd_wave_vol_change, $30
	.byte	apu_cmd_trough, $c
		: .byte	apu_cmd_note_load|apu_notes::D4
		.byte	apu_cmd_note_load|apu_notes::C4
		.byte	apu_cmd_note_load|apu_notes::D4
		.byte	apu_cmd_note_load|apu_notes::C4
		.byte	apu_cmd_note_load|apu_notes::D4
		.byte	apu_cmd_note_load|apu_notes::C4
		.byte	apu_cmd_note_load|apu_notes::D4
		.byte	apu_cmd_note_load|apu_notes::C4

		.byte	apu_cmd_song_jsr
		.addr	:+

		.byte	apu_cmd_song_jsr
		.addr	:+

		.byte	apu_cmd_song_jsr
		.addr	:+

		.byte	apu_cmd_song_jsr
		.addr	:++

		.byte	apu_cmd_song_jsr
		.addr	:-

		: .byte	apu_cmd_note_load|apu_notes::Fx4
		.byte	apu_cmd_note_load|apu_notes::Dx4
		.byte	apu_cmd_note_load|apu_notes::Fx4
		.byte	apu_cmd_note_load|apu_notes::Dx4

		: .byte	apu_cmd_note_load|apu_notes::Fx4
		.byte	apu_cmd_note_load|apu_notes::Dx4
		.byte	apu_cmd_note_load|apu_notes::Fx4
		.byte	apu_cmd_note_load|apu_notes::Dx4
		.byte	apu_cmd_song_ret
