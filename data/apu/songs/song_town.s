.include	"system/apu.i"

.include	"apu_commands.i"

	.export song_town_pulse, song_town_triangle
song_town_pulse:
	.byte	apu_cmd_tempo_change, $73

		: .byte	apu_cmd_trough, $c
		.byte	apu_cmd_wave_vol_change, $8f
		.byte   apu_cmd_note_load|apu_notes::A4
		.byte   apu_cmd_note_load|apu_notes::Ax4
		.byte   apu_cmd_note_load|apu_notes::C5
		.byte   apu_cmd_note_load|apu_notes::F5
		.byte   apu_cmd_note_load|apu_notes::E5
		.byte   apu_cmd_note_load|apu_notes::D5
		.byte   apu_cmd_note_load|apu_notes::C5, $c
	
		.byte   apu_cmd_note_load|apu_notes::D5
		.byte   apu_cmd_note_load|apu_notes::A4
		.byte   apu_cmd_note_load|apu_notes::Ax4, $3c
	
		.byte   apu_cmd_note_load|apu_notes::G4
		.byte   apu_cmd_note_load|apu_notes::A4
		.byte   apu_cmd_note_load|apu_notes::Ax4
		.byte   apu_cmd_note_load|apu_notes::E5
		.byte   apu_cmd_note_load|apu_notes::D5
		.byte   apu_cmd_note_load|apu_notes::C5
		.byte   apu_cmd_note_load|apu_notes::C5
		.byte   apu_cmd_note_load|apu_notes::Ax4
		.byte   apu_cmd_note_load|apu_notes::G4
		.byte   apu_cmd_note_load|apu_notes::Ax4
		.byte   apu_cmd_note_load|apu_notes::A4, $c

		.byte   apu_cmd_note_load|apu_notes::Ax4, $c

		.byte	apu_cmd_song_jsr
		.addr	:+

		.byte	$c
		.byte   apu_cmd_note_load|apu_notes::F4
		.byte   apu_cmd_note_load|apu_notes::A4
		.byte   apu_cmd_note_load|apu_notes::G4, $c

		.byte   apu_cmd_note_load|apu_notes::F5, $c

		.byte   apu_cmd_note_load|apu_notes::E5
		.byte   apu_cmd_note_load|apu_notes::F5
		.byte   apu_cmd_note_load|apu_notes::D5
		.byte   apu_cmd_note_load|apu_notes::E5
		.byte	apu_cmd_song_jsr
		.addr	:+

		.byte   apu_cmd_note_load|apu_notes::Ax4
		.byte   apu_cmd_note_load|apu_notes::B4
		.byte   apu_cmd_note_load|apu_notes::D5
		.byte   apu_cmd_note_load|apu_notes::C5
		.byte   apu_cmd_note_load|apu_notes::Ax4
		.byte   apu_cmd_note_load|apu_notes::A4
		.byte   apu_cmd_note_load|apu_notes::G4

		.byte	apu_cmd_wave_vol_change, $85
		.byte   apu_cmd_note_load|apu_notes::A4, $c
		
		.byte   apu_cmd_note_load|apu_notes::G4, $c

		.byte   apu_cmd_note_load|apu_notes::F4, $c
		.byte	apu_cmd_song_jsr
		.addr	:-
	:

	.byte   apu_cmd_note_load|apu_notes::C5, $c
	.byte   apu_cmd_note_load|apu_notes::D5
	.byte   apu_cmd_note_load|apu_notes::E5

	.byte	apu_cmd_wave_vol_change, $82
	.byte   apu_cmd_note_load|apu_notes::F5
	.byte	apu_cmd_trough_end

	.byte	apu_cmd_wave_vol_change, $82
	.byte   apu_cmd_note_load|apu_notes::Ax4, $6

	.byte   apu_cmd_note_load|apu_notes::Ax4, $6

	.byte   apu_cmd_note_load|apu_notes::Ax4, $c

	.byte   apu_cmd_note_load|apu_notes::D5, $c

	.byte	apu_cmd_wave_vol_change, $8f
	.byte   apu_cmd_note_load|apu_notes::F5, $18

	.byte   apu_cmd_note_load|apu_notes::E5, $c

	.byte   apu_cmd_note_load|apu_notes::D5, $c

	.byte	apu_cmd_wave_vol_change, $82
	.byte   apu_cmd_note_load|apu_notes::C5, $c

	.byte	apu_cmd_wave_vol_change, $82
	.byte   apu_cmd_note_load|apu_notes::A4, $6

	.byte   apu_cmd_note_load|apu_notes::A4, $6

	.byte	apu_cmd_trough, $c
	.byte   apu_cmd_note_load|apu_notes::A4
	.byte   apu_cmd_note_load|apu_notes::C5

	.byte	apu_cmd_wave_vol_change, $8f
	.byte   apu_cmd_note_load|apu_notes::F5, $c

	.byte   apu_cmd_note_load|apu_notes::C5
	.byte   apu_cmd_note_load|apu_notes::Ax4
	.byte   apu_cmd_note_load|apu_notes::A4
	.byte	apu_cmd_song_ret

song_town_triangle:
		: .byte	apu_cmd_wave_vol_change, $00
		.byte	$18

		.byte	apu_cmd_wave_vol_change, $ff
		.byte	apu_cmd_trough, $c
		.byte   apu_cmd_note_load|apu_notes::F4
		.byte   apu_cmd_note_load|apu_notes::A4
		.byte   apu_cmd_note_load|apu_notes::C5
		.byte   apu_cmd_note_load|apu_notes::F5
		.byte   apu_cmd_note_load|apu_notes::Fx4
		.byte   apu_cmd_note_load|apu_notes::A4
		.byte   apu_cmd_note_load|apu_notes::C5
		.byte   apu_cmd_note_load|apu_notes::D5
		.byte   apu_cmd_note_load|apu_notes::G4
		.byte   apu_cmd_note_load|apu_notes::Ax4
		.byte   apu_cmd_note_load|apu_notes::D5
		.byte   apu_cmd_note_load|apu_notes::G5
		.byte   apu_cmd_note_load|apu_notes::Fx4
		.byte   apu_cmd_note_load|apu_notes::D5
		.byte	apu_cmd_note_load|apu_notes::F4
		.byte	apu_cmd_note_load|apu_notes::D5
		.byte	apu_cmd_note_load|apu_notes::E4
		.byte	apu_cmd_note_load|apu_notes::C5
		.byte	apu_cmd_note_load|apu_notes::Ax4
		.byte	apu_cmd_note_load|apu_notes::C5
		.byte	apu_cmd_note_load|apu_notes::C4
		.byte	apu_cmd_note_load|apu_notes::G4
		.byte	apu_cmd_note_load|apu_notes::E4
		.byte	apu_cmd_note_load|apu_notes::G4
		.byte	apu_cmd_note_load|apu_notes::F4
		.byte	apu_cmd_note_load|apu_notes::C5
		.byte	apu_cmd_note_load|apu_notes::G4
		.byte	apu_cmd_note_load|apu_notes::C5
		.byte	apu_cmd_note_load|apu_notes::A4
		.byte	apu_cmd_note_load|apu_notes::Dx5
		.byte	apu_cmd_song_jsr
		.addr	:+

		.byte	apu_cmd_note_load|apu_notes::B4, $c

		.byte	apu_cmd_note_load|apu_notes::D5, $c

		.byte	apu_cmd_note_load|apu_notes::B4, $c

		.byte	apu_cmd_note_load|apu_notes::C5
		.byte	apu_cmd_note_load|apu_notes::D5
		.byte	apu_cmd_note_load|apu_notes::C5, $c

		.byte	apu_cmd_note_load|apu_notes::D5, $c

		.byte	apu_cmd_note_load|apu_notes::E5, $c

		.byte	apu_cmd_song_jsr
		.addr	:+

		.byte	apu_cmd_note_load|apu_notes::F4
		.byte	apu_cmd_note_load|apu_notes::G4
		.byte	apu_cmd_note_load|apu_notes::Ab4
		.byte	apu_cmd_note_load|apu_notes::B4
		.byte	apu_cmd_note_load|apu_notes::C5
		.byte	apu_cmd_note_load|apu_notes::Cx5
		.byte	apu_cmd_note_load|apu_notes::D5
		.byte	apu_cmd_note_load|apu_notes::E5

		.byte	apu_cmd_wave_vol_change, $18
		.byte	apu_cmd_note_load|apu_notes::F5, $c

		.byte	apu_cmd_note_load|apu_notes::C5, $c

		.byte	apu_cmd_note_load|apu_notes::A4, $c

		.byte	apu_cmd_song_jsr
		.addr	:-
	:

	.byte	apu_cmd_note_load|apu_notes::Ax5
	.byte	apu_cmd_note_load|apu_notes::C6

	.byte	apu_cmd_wave_vol_change, $18
	.byte	apu_cmd_note_load|apu_notes::D6
	.byte	apu_cmd_trough_end

	.byte	apu_cmd_note_load|apu_notes::F4, $6

	.byte	apu_cmd_note_load|apu_notes::F4, $6

	.byte	apu_cmd_note_load|apu_notes::F4, $c

	.byte	apu_cmd_note_load|apu_notes::Ax4, $c

	.byte	apu_cmd_wave_vol_change, $ff
	.byte	apu_cmd_note_load|apu_notes::D5, $18

	.byte	apu_cmd_note_load|apu_notes::C6, $c

	.byte	apu_cmd_note_load|apu_notes::Ax5, $c

	.byte	apu_cmd_wave_vol_change, $18
	.byte	apu_cmd_note_load|apu_notes::C4, $c

	.byte	apu_cmd_note_load|apu_notes::F4, $6

	.byte	apu_cmd_note_load|apu_notes::F4, $6

	.byte	apu_cmd_trough, $c
	.byte	apu_cmd_note_load|apu_notes::F4
	.byte	apu_cmd_note_load|apu_notes::A4

	.byte	apu_cmd_wave_vol_change, $60
	.byte	apu_cmd_note_load|apu_notes::C5, $24

	.byte	apu_cmd_wave_vol_change, $ff
	.byte	apu_cmd_song_ret
