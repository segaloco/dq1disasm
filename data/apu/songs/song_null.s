.include	"system/apu.i"

.include	"apu_commands.i"

	.export apu_pulse_null, apu_triangle_null
apu_pulse_null:
	.byte	apu_cmd_wave_vol_change, channel_vol::counter_halt|channel_vol::constant_vol|0

	.byte	APU_DATA_SFX_DONE

apu_triangle_null:
	.byte	apu_cmd_note_get, apu_notes::C2
	.byte	apu_cmd_wave_vol_change, channel_vol::envelope|0

	.byte	APU_DATA_SFX_DONE
