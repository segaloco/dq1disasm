.include	"system/apu.i"

.include	"apu_commands.i"

	.export song_battle_pulse, song_battle_triangle
	.export song_battle_cont_pulse, song_battle_cont_triangle
song_battle_pulse:
	.byte	apu_cmd_tempo_change, $50
	.byte	apu_cmd_wave_vol_change, $4f

	.byte	apu_cmd_song_jsr
	.addr	:++

	.byte	apu_cmd_song_jsr
	.addr	:++

		.byte	apu_cmd_tempo_change, $78
		.byte	apu_cmd_note_load|apu_notes::C4, $24

		.byte	apu_cmd_note_load|apu_notes::C4, $6

		.byte	apu_cmd_note_load|apu_notes::Cx4, $6

		.byte	apu_cmd_note_load|apu_notes::D4, $6

		.byte	apu_cmd_note_load|apu_notes::E4, $6

	song_battle_cont_pulse:
		.byte	apu_cmd_tempo_change, $78

			: .byte	apu_cmd_wave_vol_change, $7f
			.byte	apu_cmd_note_load|apu_notes::F4, $18

			.byte	apu_cmd_wave_vol_change, $4f
			.byte	$10

			.byte	apu_cmd_wave_vol_change, $7f
			.byte	apu_cmd_note_load|apu_notes::G4, $2

			.byte	apu_cmd_note_load|apu_notes::Ab4, $2

			.byte	apu_cmd_note_load|apu_notes::A4, $2

			.byte	apu_cmd_note_load|apu_notes::Ax4, $2

			.byte	apu_cmd_note_load|apu_notes::B4, $18

			.byte	apu_cmd_wave_vol_change, $4f
			.byte	$10

			.byte	apu_cmd_song_jsr
			.addr	:++

			.byte	apu_cmd_wave_vol_change, $7f
			.byte	apu_cmd_note_load|apu_notes::F4, $2

			.byte	apu_cmd_note_load|apu_notes::Fx4, $2

			.byte	apu_cmd_note_load|apu_notes::G4, $2

			.byte	apu_cmd_note_load|apu_notes::Ab4, $2

			.byte	apu_cmd_note_load|apu_notes::A4, $18

			.byte	apu_cmd_wave_vol_change, $4f
			.byte	$10

			.byte	apu_cmd_wave_vol_change, $7f
			.byte	apu_cmd_note_load|apu_notes::Ab4, $2

			.byte	apu_cmd_note_load|apu_notes::A4, $2

			.byte	apu_cmd_note_load|apu_notes::Ax4, $2

			.byte	apu_cmd_note_load|apu_notes::B4, $2

			.byte	apu_cmd_note_load|apu_notes::C5, $18

			.byte	apu_cmd_wave_vol_change, $4f
			.byte	$10

			.byte	apu_cmd_song_jsr
			.addr	:++

			.byte	apu_cmd_song_jsr
			.addr	:++

			.byte	apu_cmd_wave_vol_change, $7f
			.byte	apu_cmd_note_load|apu_notes::F4 ,$2

			.byte	apu_cmd_note_load|apu_notes::Fx4, $2

			.byte	apu_cmd_note_load|apu_notes::G4, $2

			.byte	apu_cmd_note_load|apu_notes::Ab4, $2

			.byte	apu_cmd_note_load|apu_notes::A4, $18

			.byte	apu_cmd_wave_vol_change, $4f
			.byte	$10

			.byte	apu_cmd_wave_vol_change, $7f
			.byte	apu_cmd_note_load|apu_notes::Dx4, $2

			.byte	apu_cmd_note_load|apu_notes::E4, $2

			.byte	apu_cmd_note_load|apu_notes::F4, $2

			.byte	apu_cmd_note_load|apu_notes::Fx4, $2

			.byte	apu_cmd_note_load|apu_notes::G4, $18

			.byte	apu_cmd_wave_vol_change, $4f
			.byte	$10

			.byte	apu_cmd_wave_vol_change, $7f
			.byte	apu_cmd_note_load|apu_notes::Cx5, $2

			.byte	apu_cmd_note_load|apu_notes::D5, $2

			.byte	apu_cmd_note_load|apu_notes::Dx5, $2

			.byte	apu_cmd_note_load|apu_notes::E5, $2

			.byte	apu_cmd_note_load|apu_notes::F5, $18

			.byte	apu_cmd_wave_vol_change, $4f
			.byte	$10

			.byte	apu_cmd_wave_vol_change, $7f
			.byte	apu_cmd_note_load|apu_notes::C5, $2

			.byte	apu_cmd_note_load|apu_notes::Cx5, $2

			.byte	apu_cmd_note_load|apu_notes::D5, $2

			.byte	apu_cmd_note_load|apu_notes::Dx5, $2

			.byte	apu_cmd_note_load|apu_notes::E5, $18
			.byte	apu_cmd_wave_vol_change, $4f
			.byte	$10

			.byte	apu_cmd_wave_vol_change, $7f
			.byte	apu_cmd_note_load|apu_notes::B4, $2

			.byte	apu_cmd_note_load|apu_notes::C5, $2

			.byte	apu_cmd_note_load|apu_notes::Cx5, $2

			.byte	apu_cmd_note_load|apu_notes::D5, $2

			.byte	apu_cmd_note_load|apu_notes::Dx5, $18

			.byte	apu_cmd_wave_vol_change, $4f
			.byte	$10

			.byte	apu_cmd_wave_vol_change, $7f
			.byte	apu_cmd_note_load|apu_notes::B4, $2

			.byte	apu_cmd_note_load|apu_notes::C5, $2

			.byte	apu_cmd_note_load|apu_notes::Cx5, $2

			.byte	apu_cmd_note_load|apu_notes::D5, $2

			.byte	apu_cmd_note_load|apu_notes::Dx5, $18

			.byte	apu_cmd_wave_vol_change, $4f
			.byte	$10

			.byte	apu_cmd_wave_vol_change, $7f
			.byte	apu_cmd_note_load|apu_notes::Ax4, $2

			.byte	apu_cmd_note_load|apu_notes::B4, $2

			.byte	apu_cmd_note_load|apu_notes::C5, $2

			.byte	apu_cmd_note_load|apu_notes::Cx5, $2

			.byte	apu_cmd_note_load|apu_notes::D5, $18

			.byte	apu_cmd_wave_vol_change, $4f
			.byte	$10

			.byte	apu_cmd_wave_vol_change, $7f
			.byte	apu_cmd_note_load|apu_notes::A4, $2

			.byte	apu_cmd_note_load|apu_notes::Ax4, $2

			.byte	apu_cmd_note_load|apu_notes::B4, $2

			.byte	apu_cmd_note_load|apu_notes::C5, $2

			.byte	apu_cmd_note_load|apu_notes::Cx5, $18

			.byte	apu_cmd_wave_vol_change, $4f
			.byte	$10

			.byte	apu_cmd_wave_vol_change, $7f
			.byte	apu_cmd_note_load|apu_notes::Ax4, $2

			.byte	apu_cmd_note_load|apu_notes::B4, $2

			.byte	apu_cmd_note_load|apu_notes::C5, $2

			.byte	apu_cmd_note_load|apu_notes::Cx5, $2

			.byte	apu_cmd_note_load|apu_notes::D5, $18

			.byte	apu_cmd_wave_vol_change, $4f
			.byte	$10

			.byte	apu_cmd_note_load|apu_notes::Dx5, $2
			.byte	apu_cmd_note_load|apu_notes::E5, $2
			.byte	apu_cmd_note_load|apu_notes::F5, $2
			.byte	apu_cmd_note_load|apu_notes::Fx5, $2

			.byte	apu_cmd_trough, $8
			.byte	apu_cmd_note_load|apu_notes::G5
			.byte	apu_cmd_note_load|apu_notes::D5
			.byte	apu_cmd_note_load|apu_notes::G5
			.byte	apu_cmd_note_load|apu_notes::Fx5
			.byte	apu_cmd_note_load|apu_notes::Dx5
			.byte	apu_cmd_note_load|apu_notes::Fx5
			.byte	apu_cmd_note_load|apu_notes::F5
			.byte	apu_cmd_note_load|apu_notes::D5
			.byte	apu_cmd_note_load|apu_notes::F5
			.byte	apu_cmd_note_load|apu_notes::E5
			.byte	apu_cmd_note_load|apu_notes::Cx5
			.byte	apu_cmd_note_load|apu_notes::E5
			.byte	apu_cmd_note_load|apu_notes::Dx5
			.byte	apu_cmd_note_load|apu_notes::C5
			.byte	apu_cmd_note_load|apu_notes::A4
			.byte	apu_cmd_note_load|apu_notes::Fx4
			.byte	apu_cmd_note_load|apu_notes::Dx4
			.byte	apu_cmd_note_load|apu_notes::C4
			.byte	apu_cmd_note_load|apu_notes::Cx4
			.byte	apu_cmd_note_load|apu_notes::E3
			.byte	apu_cmd_note_load|apu_notes::G3
			.byte	apu_cmd_note_load|apu_notes::Ax3
			.byte	apu_cmd_note_load|apu_notes::Cx4
			.byte	apu_cmd_note_load|apu_notes::E4
			.byte	apu_cmd_trough_end

			.byte	apu_cmd_song_jsr
			.addr	:-

		: .byte	apu_cmd_trough, $1
		.byte	apu_cmd_note_load|apu_notes::C4
		.byte	apu_cmd_note_load|apu_notes::E4
		.byte	apu_cmd_note_load|apu_notes::G4
		.byte	apu_cmd_note_load|apu_notes::Ax4
		.byte	apu_cmd_note_load|apu_notes::Cx5
		.byte	apu_cmd_note_load|apu_notes::E5
		.byte	apu_cmd_note_load|apu_notes::G5
		.byte	apu_cmd_note_load|apu_notes::Ax5
		.byte	apu_cmd_note_load|apu_notes::Cx6
		.byte	apu_cmd_note_load|apu_notes::Ax5
		.byte	apu_cmd_note_load|apu_notes::G5
		.byte	apu_cmd_note_load|apu_notes::E5
		.byte	apu_cmd_note_load|apu_notes::C5
		.byte	apu_cmd_note_load|apu_notes::Ax4
		.byte	apu_cmd_note_load|apu_notes::G4
		.byte	apu_cmd_note_load|apu_notes::E4
		.byte	apu_cmd_trough_end

		.byte	apu_cmd_song_ret

	: .byte	apu_cmd_wave_vol_change, $7f
	.byte	apu_cmd_note_load|apu_notes::Fx4, $2

	.byte	apu_cmd_note_load|apu_notes::G4, $2

	.byte	apu_cmd_note_load|apu_notes::Ab4, $2

	.byte	apu_cmd_note_load|apu_notes::A4, $2

	.byte	apu_cmd_note_load|apu_notes::Ax4, $18

	.byte	apu_cmd_wave_vol_change, $4f
	.byte	$10

	.byte	apu_cmd_song_ret

song_battle_triangle:
	.byte	apu_cmd_wave_vol_change, $30
	.byte	apu_cmd_song_jsr
	.addr	:--

	.byte	apu_cmd_song_jsr
	.addr	:--

	.byte	apu_cmd_note_load|apu_notes::C4, $24
	.byte	apu_cmd_note_load|apu_notes::C4, $6
	.byte	apu_cmd_note_load|apu_notes::Cx4, $6
	.byte	apu_cmd_note_load|apu_notes::D4, $6
	.byte	apu_cmd_note_load|apu_notes::E4, $6

song_battle_cont_triangle:
	.byte	apu_cmd_wave_vol_change, $10
		: .byte	apu_cmd_trough, $6
		.byte	apu_cmd_note_load|apu_notes::F4
		.byte	apu_cmd_note_load|apu_notes::Ab4
		.byte	apu_cmd_note_load|apu_notes::C5, $12

		.byte	apu_cmd_note_load|apu_notes::D5, $6

		.byte	apu_cmd_note_load|apu_notes::F4
		.byte	apu_cmd_note_load|apu_notes::Ab4
		.byte	apu_cmd_note_load|apu_notes::B4, $12

		.byte	apu_cmd_note_load|apu_notes::D5, $6

		.byte	apu_cmd_note_load|apu_notes::G4
		.byte	apu_cmd_note_load|apu_notes::Ax4
		.byte	apu_cmd_note_load|apu_notes::Cx5, $12

		.byte	apu_cmd_note_load|apu_notes::E5, $6

		.byte	apu_cmd_note_load|apu_notes::Dx4
		.byte	apu_cmd_note_load|apu_notes::Fx4
		.byte	apu_cmd_note_load|apu_notes::A4, $12

		.byte	apu_cmd_note_load|apu_notes::C5, $6

		.byte	apu_cmd_note_load|apu_notes::A4
		.byte	apu_cmd_note_load|apu_notes::C5
		.byte	apu_cmd_note_load|apu_notes::Dx5, $12

		.byte	apu_cmd_note_load|apu_notes::G5, $6

		.byte	apu_cmd_note_load|apu_notes::Dx4
		.byte	apu_cmd_note_load|apu_notes::G4
		.byte	apu_cmd_note_load|apu_notes::Cx5, $12

		.byte	apu_cmd_note_load|apu_notes::E5, $6
		
		.byte	apu_cmd_note_load|apu_notes::Fx4
		.byte	apu_cmd_note_load|apu_notes::Ax4
		.byte	apu_cmd_note_load|apu_notes::D5, $12

		.byte	apu_cmd_note_load|apu_notes::Fx5, $6

		.byte	apu_cmd_note_load|apu_notes::D4
		.byte	apu_cmd_note_load|apu_notes::Fx4
		.byte	apu_cmd_note_load|apu_notes::A4, $12

		.byte	apu_cmd_note_load|apu_notes::D5, $6

		.byte	apu_cmd_note_load|apu_notes::G4
		.byte	apu_cmd_note_load|apu_notes::Ax4
		.byte	apu_cmd_note_load|apu_notes::D5, $12

		.byte	apu_cmd_note_load|apu_notes::E5, $6

		.byte	apu_cmd_note_load|apu_notes::G4
		.byte	apu_cmd_note_load|apu_notes::B4
		.byte	apu_cmd_note_load|apu_notes::D5, $12

		.byte	apu_cmd_note_load|apu_notes::F5, $6

		.byte	apu_cmd_note_load|apu_notes::C4
		.byte	apu_cmd_note_load|apu_notes::G4
		.byte	apu_cmd_note_load|apu_notes::Ax4, $12

		.byte	apu_cmd_note_load|apu_notes::Cx5, $6

		.byte	apu_cmd_note_load|apu_notes::F4
		.byte	apu_cmd_note_load|apu_notes::Fx4
		.byte	apu_cmd_note_load|apu_notes::A4, $12

		.byte	apu_cmd_note_load|apu_notes::C5, $6

		.byte	apu_cmd_note_load|apu_notes::Ax3
		.byte	apu_cmd_note_load|apu_notes::F4
		.byte	apu_cmd_note_load|apu_notes::Ab4, $12

		.byte	apu_cmd_note_load|apu_notes::Dx5, $6

		.byte	apu_cmd_note_load|apu_notes::Ax3
		.byte	apu_cmd_note_load|apu_notes::F4
		.byte	apu_cmd_note_load|apu_notes::Ab4, $12

		.byte	apu_cmd_note_load|apu_notes::D5, $6

		.byte	apu_cmd_note_load|apu_notes::A3
		.byte	apu_cmd_note_load|apu_notes::E4
		.byte	apu_cmd_note_load|apu_notes::G4, $12

		.byte	apu_cmd_note_load|apu_notes::Ax4, $6

		.byte	apu_cmd_note_load|apu_notes::D4
		.byte	apu_cmd_note_load|apu_notes::Fx4
		.byte	apu_cmd_note_load|apu_notes::A4, $12

		.byte	apu_cmd_note_load|apu_notes::D5, $6

		.byte	apu_cmd_trough, $8
		.byte	apu_cmd_note_load|apu_notes::Ax5
		.byte	apu_cmd_note_load|apu_notes::G5
		.byte	apu_cmd_note_load|apu_notes::Ax5
		.byte	apu_cmd_note_load|apu_notes::A5
		.byte	apu_cmd_note_load|apu_notes::Fx5
		.byte	apu_cmd_note_load|apu_notes::A5
		.byte	apu_cmd_note_load|apu_notes::Ab5
		.byte	apu_cmd_note_load|apu_notes::F5
		.byte	apu_cmd_note_load|apu_notes::Ab5
		.byte	apu_cmd_note_load|apu_notes::G5
		.byte	apu_cmd_note_load|apu_notes::E5
		.byte	apu_cmd_note_load|apu_notes::G5
		.byte	apu_cmd_note_load|apu_notes::Fx5
		.byte	apu_cmd_note_load|apu_notes::A5
		.byte	apu_cmd_note_load|apu_notes::Fx5
		.byte	apu_cmd_note_load|apu_notes::Dx5
		.byte	apu_cmd_note_load|apu_notes::C5
		.byte	apu_cmd_note_load|apu_notes::A4
		.byte	apu_cmd_note_load|apu_notes::Ax4
		.byte	apu_cmd_note_load|apu_notes::Cx4
		.byte	apu_cmd_note_load|apu_notes::E4
		.byte	apu_cmd_note_load|apu_notes::G4
		.byte	apu_cmd_note_load|apu_notes::Ax4
		.byte	apu_cmd_note_load|apu_notes::Cx5
		.byte	apu_cmd_song_jsr
		.addr	:-
