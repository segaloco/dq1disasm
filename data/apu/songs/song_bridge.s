.include	"system/apu.i"

.include	"apu_commands.i"

	.export song_bridge_pulse1, song_bridge_pulse2
song_bridge_pulse2:
	.byte	$3

song_bridge_pulse1:
	.byte	apu_cmd_tempo_change, $50
	.byte	apu_cmd_wave_vol_change, $8f
	.byte	apu_cmd_note_load|apu_notes::C3, $9

	.byte	apu_cmd_note_load|apu_notes::G3, $8

	.byte	apu_cmd_note_load|apu_notes::B3, $7

	.byte	apu_cmd_note_load|apu_notes::E4, $6

	.byte	apu_cmd_note_load|apu_notes::D3, $5
	
	.byte	apu_cmd_note_load|apu_notes::A3, $4

	.byte	apu_cmd_trough, $3
	.byte	apu_cmd_note_load|apu_notes::C4
	.byte	apu_cmd_note_load|apu_notes::F4

	.byte	apu_cmd_tempo_change, $58
	.byte	apu_cmd_note_load|apu_notes::E3
	.byte	apu_cmd_note_load|apu_notes::B3
	.byte	apu_cmd_note_load|apu_notes::D4
	.byte	apu_cmd_note_load|apu_notes::G4

	.byte	apu_cmd_tempo_change, $60
	.byte	apu_cmd_note_load|apu_notes::F3
	.byte	apu_cmd_note_load|apu_notes::C4
	.byte	apu_cmd_note_load|apu_notes::E4
	.byte	apu_cmd_note_load|apu_notes::A4

	.byte	apu_cmd_tempo_change, $68
	.byte	apu_cmd_note_load|apu_notes::G2
	.byte	apu_cmd_note_load|apu_notes::D3
	.byte	apu_cmd_note_load|apu_notes::G3
	.byte	apu_cmd_note_load|apu_notes::C4

	.byte	apu_cmd_tempo_change, $70
	.byte	apu_cmd_note_load|apu_notes::E4
	.byte	apu_cmd_note_load|apu_notes::A4
	.byte	apu_cmd_note_load|apu_notes::C5
	.byte	apu_cmd_note_load|apu_notes::F5
	.byte	apu_cmd_note_load|apu_notes::A5
	.byte	apu_cmd_note_load|apu_notes::C6
	.byte	apu_cmd_note_load|apu_notes::F6
	.byte	apu_cmd_note_load|apu_notes::E6

	.byte	apu_cmd_tempo_change, $64
	.byte	apu_cmd_note_load|apu_notes::B5
	.byte	apu_cmd_note_load|apu_notes::G5
	.byte	apu_cmd_note_load|apu_notes::E5
	.byte	apu_cmd_note_load|apu_notes::B4

	.byte	apu_cmd_tempo_change, $5a
	.byte	apu_cmd_note_load|apu_notes::G4
	.byte	apu_cmd_note_load|apu_notes::E4
	.byte	apu_cmd_note_load|apu_notes::B3
	.byte	apu_cmd_note_load|apu_notes::G3
	.byte	apu_cmd_trough_end

	.byte	apu_cmd_note_load|apu_notes::C3, $30

	.byte	APU_DATA_SFX_DONE

	.byte	apu_cmd_next_byte
