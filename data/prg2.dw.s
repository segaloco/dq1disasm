.segment	"PRG2CODE"

; ------------------------------------------------------------
	.byte	$B0
	.byte	$BC
	plp
	.byte	$80
	stx	$82
	ora	$1385, y
	.byte	$87
	jmp	$1289

	sta	L906E
	.byte	$42
	sty	$1E, x
	tya
	dey
	.byte	$9C
	.byte	$3F
	.byte	$9F
	txa
	ldx	#$DC
	ldx	$2E
	tax
	adc	($AC, x)
	plp
	ldx	LAFEE
	.byte	$8B
	ldx	$65, y
	tsx
	.byte	$F4
	.byte	$5F
	ora	($0A), y
	ora	$5F11, x
	jsr	$1418
	asl	$5F17
	asl	$4719, x
	.byte	$FC
	rts

	sbc	$1137, x
	clc
	asl	$A5F, x
	.byte	$1B
	ora	$D5F, x
	asl	$0D0A
	.byte	$47
	.byte	$FC
	bvc	L8083
	ora	($18), y
	asl	$A5F, x
	.byte	$1B
	ora	$1C5F, x
	ora	$181B, x
	.byte	$17
	bpl	L80BA
	asl	$1817
	asl	$1110, x
	jmp	$3AFD

	ora	($22), y
	.byte	$5F
	.byte	$0C
	asl	a
	.byte	$17
	.byte	$5F
	ora	$1811, x
	asl	$175F, x
	clc
	ora	$D5F, x
	asl	$0E0F
	asl	a
	ora	$1D5F, x
	ora	($0E), y
	.byte	$5F
	.byte	$27
	.byte	$1B
	asl	a
	.byte	$10
L8083:	clc
	.byte	$17
	ora	$18, x
	.byte	$1B
	ora	$404B
	.byte	$FB
	.byte	$FC
	bvc	L80BB
	.byte	$0F
	.byte	$5F
	ora	$1811, x
	asl	$A5F, x
	.byte	$1B
	ora	$195F, x
	ora	$0A, x
	.byte	$17
	.byte	$17
	.byte	$12
	.byte	$17
	bpl	L8102
	ora	$5F18, x
	ora	$140A, x
	asl	$A5F
	.byte	$5F
	.byte	$1B
	asl	$1D1C
	pha
	.byte	$5F
	.byte	$0F
	.byte	$12
	.byte	$1B
	.byte	$1C
	ora	$1C5F, x
L80BA:	.byte	$0E
L80BB:	asl	$2E5F
	.byte	$12
	.byte	$17
	bpl	L8121
	.byte	$2F
	clc
	.byte	$1B
	.byte	$12
	.byte	$14
	.byte	$52
	.byte	$FC
	sed
	.byte	$5F
	ora	($0E), y
	ora	$0D, x
	.byte	$5F
	ora	$E11, x
	.byte	$5F
	and	$0A, x
	.byte	$12
	.byte	$17
	.byte	$0B
	clc
	jsr	$275F
	.byte	$1B
	clc
	ora	$1D5F, y
	clc
	jsr	$1B0A
	ora	$1D5F
	ora	($0E), y
	.byte	$5F
	.byte	$1C
	.byte	$14
	.byte	$22
	.byte	$47
	sbc	$FB60, x
	.byte	$FC
	and	$1E
	ora	$175F, x
	clc
	.byte	$5F
	.byte	$1B
	asl	a
	.byte	$12
	.byte	$17
	.byte	$0B
	clc
	.byte	$20
L8102:	.byte	$5F
	asl	a
	ora	$0E19, y
	asl	a
	.byte	$1B
	asl	$5F0D
	ora	($0E), y
	.byte	$1B
	asl	$FC47
	bvc	L813E
	clc
	clc
	ora	$165F
	clc
	.byte	$1B
	.byte	$17
	.byte	$12
	.byte	$17
	bpl	L8167
	.byte	$FD
L8121:	.byte	$37
	ora	($18), y
	asl	$115F, x
	asl	a
	.byte	$1C
	ora	$115F, x
	asl	a
	ora	$A5F
	.byte	$5F
	bpl	L814B
	clc
	ora	$175F
	.byte	$12
	bpl	L814B
	ora	$1C53, x
	.byte	$5F
L813E:	.byte	$1C
	ora	$0E, x
	asl	$5F19
	bit	$115F
	clc
	ora	$520E, y
L814B:	.byte	$FB
	.byte	$FC
	bvc	L817B
	.byte	$5F
	.byte	$1C
	ora	($0A), y
	ora	$15, x
	.byte	$5F
	.byte	$1C
	asl	$5F0E
	ora	$E11, x
	asl	$A5F
	bpl	L816C
	.byte	$12
	.byte	$17
	.byte	$52
	.byte	$FC
	.byte	$50
L8167:	rol	a
	clc
	clc
	.byte	$0D
	.byte	$5F
L816C:	asl	$18, x
	.byte	$1B
	.byte	$17
	.byte	$12
	.byte	$17
	bpl	L81BB
	sbc	$1137, x
	clc
	asl	$1C5F, x
L817B:	asl	$160E
	.byte	$1C
	.byte	$5F
	ora	$5F18, x
	ora	($0A), y
	.byte	$1F
	asl	$1C5F
	ora	$170E, y
	ora	$A5F, x
	.byte	$5F
	bpl	L81AA
	clc
	ora	$175F
	.byte	$12
	bpl	L81AA
	ora	$FB52, x
	.byte	$FC
	bvc	L81C9
	clc
	clc
	ora	$175F
	.byte	$12
	bpl	L81B8
	ora	$FD52, x
L81AA:	.byte	$FC
	bvc	L81DF
	.byte	$14
	asl	a
	.byte	$22
	.byte	$47
	sbc	$182A, x
	clc
	ora	$0B49
L81B8:	.byte	$22
	.byte	$0E
	pha
L81BB:	.byte	$5F
	ora	$0A1B, x
	.byte	$1F
	asl	$0E15
	.byte	$1B
	.byte	$52
	.byte	$FC
	bvc	L8202
	.byte	$0E
L81C9:	ora	$0C, x
	clc
	asl	$0E, x
	.byte	$5F
	ora	$5F18, x
	ora	$E11, x
	.byte	$5F
	ora	$0A1B, x
	.byte	$1F
	asl	$0E15
	.byte	$1B
	.byte	$53
L81DF:	.byte	$1C
	.byte	$5F
	bit	$1717
	.byte	$47
	sbc	$1835, x
	clc
	asl	$5F, x
	asl	a
	.byte	$17
	ora	$0B5F
	clc
	asl	a
	.byte	$1B
	ora	$125F
	.byte	$1C
	.byte	$5F
	sbc	$5F, x
	rol	a
	.byte	$32
	.byte	$2F
	.byte	$27
	.byte	$5F
	ora	$1B0E, y
L8202:	.byte	$5F
	.byte	$17
	.byte	$12
	bpl	L8218
	ora	$FD47, x
	.byte	$27
	clc
	.byte	$1C
	ora	$1D5F, x
	ora	($18), y
	asl	$205F, x
	asl	a
	.byte	$17
	.byte	$1D
L8218:	.byte	$5F
	asl	a
	.byte	$5F
	.byte	$1B
	clc
	clc
	asl	$4B, x
	rti

	sbc	$50FC, x
	bit	$15
	ora	$5F, x
	ora	$E11, x
	.byte	$5F
	.byte	$0B
	asl	$1D1C
	.byte	$5F
	ora	$5F18, x
	ora	$E11, x
	asl	$FC52
	bvc	L8273
	ora	($0E), y
	.byte	$1B
	asl	$A5F
	.byte	$1B
	asl	$175F
	clc
	.byte	$5F
	.byte	$1C
	ora	$120A, x
	.byte	$1B
	.byte	$1C
	.byte	$5F
	ora	($0E), y
	.byte	$1B
	asl	$FC52
	bvc	L828E
	ora	($18), y
	asl	$0C5F, x
	asl	a
	.byte	$17
	.byte	$17
	clc
	ora	$0E5F, x
	.byte	$17
	ora	$1B0E, x
	.byte	$5F
	ora	($0E), y
	.byte	$1B
	asl	$FC52
	bvc	L82A7
	ora	($0E), y
	.byte	$1B
L8273:	asl	$125F
	.byte	$1C
	.byte	$5F
	.byte	$17
	clc
	.byte	$5F
	clc
	.byte	$17
	asl	$1D5F
	ora	($0E), y
	.byte	$1B
	asl	$FC52
	bvc	L82B4
	.byte	$5F
	ora	$A11, x
	.byte	$17
	.byte	$14
L828E:	.byte	$5F
	ora	$E11, x
	asl	$FD47
	.byte	$3A
	clc
	.byte	$17
	.byte	$53
	ora	$1D5F, x
	ora	($18), y
	asl	$0B5F, x
	asl	$5F22, x
	clc
	.byte	$17
	.byte	$0E
L82A7:	.byte	$5F
	asl	$18, x
	.byte	$1B
	asl	$0B5F
	clc
	ora	$151D, x
	.byte	$0E
	.byte	$4B
L82B4:	rti

	sbc	$50FC, x
	.byte	$3A
	.byte	$12
	ora	$15, x
	.byte	$5F
	ora	$1811, x
	asl	$0B5F, x
	asl	$5F22, x
	.byte	$1C
	clc
	asl	$0E, x
	.byte	$5F
	and	#$0A
	.byte	$12
	.byte	$1B
	.byte	$22
	.byte	$5F
	.byte	$3A
	asl	a
	ora	$1B0E, x
	.byte	$5F
	.byte	$0F
	clc
	.byte	$1B
	.byte	$5F
	.byte	$03
	php
	.byte	$5F
	rol	a
	.byte	$32
	.byte	$2F
	.byte	$27
	.byte	$5F
	ora	$5F18, x
	.byte	$14
	asl	$190E
	.byte	$5F
	ora	$E11, x
	.byte	$5F
	.byte	$27
	.byte	$1B
	asl	a
	bpl	L830C
	.byte	$17
	ora	$18, x
	.byte	$1B
	ora	$1C53
	.byte	$5F
	asl	$12, x
	.byte	$17
	.byte	$12
	clc
	.byte	$17
	.byte	$1C
	.byte	$5F
	asl	a
	jsr	$220A
	.byte	$4B
	rti

	.byte	$FD
	.byte	$FC
L830C:	bvc	L833A
	.byte	$5F
	jsr	$1512
	ora	$5F, x
	.byte	$1C
	asl	$5F0E
	ora	$E11, x
	asl	$155F
	asl	a
	ora	$1B0E, x
	.byte	$52
	.byte	$FC
	bvc	L835D
	ora	($18), y
	asl	$115F, x
	asl	a
	.byte	$1C
	ora	$175F, x
	clc
	ora	$0E5F, x
	.byte	$17
	clc
	asl	$1110, x
	.byte	$5F
L833A:	asl	$18, x
	.byte	$17
	asl	$5222
	.byte	$FB
	.byte	$FC
	bvc	L8370
	.byte	$5F
	asl	a
	asl	$5F, x
	.byte	$1C
	clc
	.byte	$1B
	.byte	$1B
	.byte	$22
	pha
	.byte	$5F
	.byte	$0B
	asl	$5F1D, x
	bit	$0C5F
	asl	a
	.byte	$17
	.byte	$17
	clc
	ora	$1C5F, x
L835D:	asl	$1515
	.byte	$5F
	ora	$E11, x
	asl	$A5F
	.byte	$17
	.byte	$22
	asl	$18, x
	.byte	$1B
	asl	$FC52
	.byte	$50
L8370:	.byte	$2B
	asl	$0E1B
	pha
	ora	$140A, x
	asl	$1D5F
	ora	($12), y
	.byte	$1C
	.byte	$5F
	.byte	$14
	asl	$4722
	sbc	$1827, x
	.byte	$1C
	ora	$1D5F, x
	ora	($18), y
	asl	$205F, x
	.byte	$12
	.byte	$1C
	ora	($5F), y
	ora	$5F18, x
	ora	$1B1E, y
	.byte	$0C
	ora	($0A), y
	.byte	$1C
	asl	$165F
	clc
	.byte	$1B
	asl	$404B
	sbc	$50FC, x
	bmi	L83B4
	bpl	L83BE
	.byte	$0C
	.byte	$5F
	.byte	$14
	asl	$1C22
	.byte	$4C
	.byte	$FD
L83B4:	.byte	$37
	ora	($0E), y
	.byte	$22
	.byte	$5F
	jsr	$1512
	ora	$5F, x
L83BE:	asl	$1517, x
	clc
	.byte	$0C
	.byte	$14
	.byte	$5F
	asl	a
	.byte	$17
	.byte	$22
	.byte	$5F
	ora	$1818
	.byte	$1B
	.byte	$47
	sbc	$1827, x
	.byte	$1C
	ora	$1D5F, x
	ora	($18), y
	asl	$205F, x
	.byte	$12
	.byte	$1C
	ora	($5F), y
	ora	$5F18, x
	ora	$1B1E, y
	.byte	$0C
	ora	($0A), y
	.byte	$1C
	asl	$185F
	.byte	$17
	asl	$0F5F
	clc
	.byte	$1B
	.byte	$5F
	sbc	$5F, x
	rol	a
	.byte	$32
	.byte	$2F
	.byte	$27
	.byte	$4B
	rti

	sbc	$50FC, x
	bit	$A5F
	asl	$5F, x
	.byte	$1C
	clc
	.byte	$1B
	.byte	$1B
	.byte	$22
	.byte	$52
	.byte	$FB
	.byte	$FC
	bit	$5F
	.byte	$0C
	asl	$1C1B, x
	asl	$125F
	.byte	$1C
	.byte	$5F
	asl	$1819, x
	.byte	$17
	.byte	$5F
	ora	$2211, x
	.byte	$5F
	.byte	$0B
	clc
	ora	$4722
	.byte	$FB
	.byte	$FC
	bvc	L845E
	ora	($18), y
	asl	$115F, x
	asl	a
	.byte	$1C
	ora	$175F, x
	clc
	.byte	$5F
	ora	$1C18, y
	.byte	$1C
	asl	$1C1C
	.byte	$12
	clc
	.byte	$17
	.byte	$1C
	.byte	$52
	.byte	$FC
	bvc	L847C
	.byte	$12
	ora	$1D, x
	.byte	$5F
	ora	$1811, x
	asl	$1C5F, x
	asl	$1515
	.byte	$5F
	asl	a
	.byte	$17
	.byte	$22
	ora	$1211, x
	.byte	$17
	bpl	L84B8
	asl	$1C15
	.byte	$0E
	.byte	$4B
L845E:	rti

	sbc	$50FC, x
	bit	$0C5F
	asl	a
	.byte	$17
	.byte	$17
	clc
	ora	$0B5F, x
	asl	$5F22, x
	.byte	$12
	ora	$FC52, x
	bvc	L84AC
	ora	($18), y
	asl	$1C5F, x
	asl	a
	.byte	$12
L847C:	ora	$1D5F
	ora	($0E), y
	.byte	$5F
	.byte	$F7
	.byte	$47
	sbc	$5F2C, x
	jsr	$1512
	ora	$5F, x
	.byte	$0B
	asl	$5F22, x
	ora	$2211, x
	.byte	$5F
	.byte	$F7
	.byte	$5F
	.byte	$0F
	clc
	.byte	$1B
	.byte	$5F
	sbc	$5F, x
	rol	a
	.byte	$32
	.byte	$2F
	.byte	$27
	.byte	$47
	sbc	$1C2C, x
	.byte	$5F
	ora	$A11, x
	ora	$A5F, x
	.byte	$15
L84AC:	ora	$5F, x
	.byte	$1B
	.byte	$12
	bpl	L84C3
	ora	$404B, x
	sbc	$50FC, x
L84B8:	.byte	$3A
	ora	($0A), y
	ora	$A5F, x
	.byte	$1B
	ora	$1D5F, x
	.byte	$11
L84C3:	clc
	asl	$1C5F, x
	asl	$1515
	.byte	$12
	.byte	$17
	bpl	L8519
	rti

	sbc	$50FC, x
	bit	$205F
	.byte	$12
	ora	$15, x
	.byte	$5F
	.byte	$0B
	asl	$205F
	asl	a
	.byte	$12
	ora	$1712, x
	bpl	L8543
	.byte	$0F
	clc
	.byte	$1B
	.byte	$5F
	ora	$2211, x
	.byte	$5F
	.byte	$17
	asl	$1D21
	.byte	$5F
	.byte	$1F
	.byte	$12
	.byte	$1C
	.byte	$12
	ora	$FC52, x
	bvc	L8521
	clc
	.byte	$1C
	ora	$1D5F, x
	ora	($18), y
	asl	$205F, x
	asl	a
	.byte	$17
	ora	$A5F, x
	.byte	$17
	.byte	$22
	ora	$1211, x
	.byte	$17
	bpl	L8570
	asl	$1C15
	asl	$404B
	.byte	$FD
	.byte	$FC
L8519:	bvc	L8552
	ora	($18), y
	asl	$0C5F, x
	asl	a
L8521:	.byte	$17
	.byte	$17
	clc
	ora	$115F, x
	clc
	ora	$0D, x
	.byte	$5F
	asl	$18, x
	.byte	$1B
	asl	$2B5F
	asl	$0B1B
	.byte	$1C
	.byte	$52
	.byte	$FB
	.byte	$FC
	bvc	L8571
	ora	($18), y
	asl	$0C5F, x
	asl	a
	.byte	$17
	.byte	$17
	clc
L8543:	ora	$0C5F, x
	asl	a
	.byte	$1B
	.byte	$1B
	.byte	$22
	.byte	$5F
	asl	a
	.byte	$17
	.byte	$22
	asl	$18, x
	.byte	$1B
	.byte	$0E
L8552:	.byte	$52
	.byte	$FB
	.byte	$FC
	bvc	L858E
	ora	($18), y
	asl	$115F, x
	asl	a
	.byte	$1C
	ora	$175F, x
	clc
	ora	$0E5F, x
	.byte	$17
	clc
	asl	$1110, x
	.byte	$5F
	asl	$18, x
	.byte	$17
	.byte	$0E
	.byte	$22
L8570:	.byte	$52
L8571:	.byte	$FB
	.byte	$FC
	bvc	L85AC
	ora	($0E), y
	.byte	$5F
	.byte	$F7
	.byte	$4B
	sbc	$1137, x
	asl	a
	.byte	$17
	.byte	$14
	.byte	$5F
	.byte	$22
	clc
	asl	$1F5F, x
	asl	$221B
	.byte	$5F
	asl	$1E, x
	.byte	$0C
	.byte	$11
L858E:	.byte	$52
	.byte	$FB
	.byte	$FC
	bvc	L85CD
	ora	($0A), y
	ora	$D5F, x
	clc
	.byte	$1C
	ora	$1D5F, x
	ora	($18), y
	asl	$205F, x
	asl	a
	.byte	$17
	ora	$404B, x
	sbc	$50FC, x
	.byte	$3A
	.byte	$0E
L85AC:	ora	$0C, x
	clc
	asl	$0E, x
	.byte	$47
	sbc	$0E3A, x
	.byte	$5F
	ora	$A0E
	ora	$5F, x
	.byte	$12
	.byte	$17
	.byte	$5F
	ora	$1818, x
	ora	$1C, x
	.byte	$47
	sbc	$113A, x
	asl	a
	ora	$0C5F, x
	asl	a
	.byte	$17
L85CD:	.byte	$5F
	bit	$D5F
	clc
	.byte	$5F
	.byte	$0F
	clc
	.byte	$1B
	.byte	$5F
	ora	$E11, x
	asl	$404B
	sbc	$50FC, x
	.byte	$32
	ora	($48), y
	.byte	$5F
	.byte	$22
	asl	$4B1C
	sbc	$1137, x
	asl	a
	ora	$1C53, x
	.byte	$5F
	ora	$1818, x
	.byte	$5F
	.byte	$0B
	asl	a
	ora	$FB52
	.byte	$FC
	bvc	L8628
	.byte	$1C
	.byte	$5F
	ora	$A11, x
	ora	$325F, x
	.byte	$14
	asl	a
	.byte	$22
	.byte	$47
	.byte	$4B
	rti

	sbc	$FC57, x
	bvc	L8649
	asl	$D5F
	asl	$150A
	.byte	$5F
	.byte	$12
	.byte	$17
	.byte	$5F
	jsr	$A0E
	ora	$1718, y
	.byte	$1C
	.byte	$5F
	asl	a
	.byte	$17
	ora	$A5F
	.byte	$1B
	.byte	$16
L8628:	clc
	.byte	$1B
	.byte	$47
	sbc	$1827, x
	.byte	$1C
	ora	$1D5F, x
	ora	($18), y
	asl	$205F, x
	.byte	$12
	.byte	$1C
	ora	($5F), y
	ora	$5F18, x
	.byte	$0B
	asl	$5F22, x
	asl	a
	.byte	$17
	.byte	$22
	ora	$1211, x
	.byte	$17
L8649:	bpl	L86AA
	ora	$0D18, x
	asl	a
	.byte	$22
	.byte	$4B
	rti

	sbc	$50FC, x
	.byte	$37
	ora	($0E), y
	.byte	$5F
	.byte	$F7
	.byte	$4B
	rti

	.byte	$FB
	.byte	$FC
	bvc	L8697
	ora	($0E), y
	.byte	$17
	.byte	$5F
	bit	$205F
	.byte	$12
	ora	$15, x
	.byte	$5F
	.byte	$0B
	asl	$5F22, x
	ora	$2211, x
	.byte	$5F
	.byte	$F7
	.byte	$5F
	.byte	$0F
	clc
	.byte	$1B
	.byte	$5F
	sbc	$5F, x
	rol	a
	.byte	$32
	.byte	$2F
	.byte	$27
	.byte	$52
	.byte	$FB
	.byte	$FC
	bvc	L86BA
	clc
	.byte	$1B
	.byte	$1B
	.byte	$22
	.byte	$47
	sbc	$1137, x
	clc
	asl	$115F, x
	asl	a
	.byte	$1C
	ora	$175F, x
	clc
	.byte	$1D
L8697:	.byte	$5F
	asl	$1817
	asl	$1110, x
	.byte	$5F
	asl	$18, x
	.byte	$17
	asl	$5222
	.byte	$FB
	.byte	$FC
	bvc	L86D0
	clc
L86AA:	.byte	$1C
	ora	$1D5F, x
	ora	($18), y
	asl	$205F, x
	.byte	$12
	.byte	$1C
	ora	($5F), y
	ora	$5F18, x
L86BA:	.byte	$0B
	asl	$5F22, x
	asl	a
	.byte	$17
	.byte	$22
	ora	$1211, x
	.byte	$17
	bpl	L8726
	asl	$18, x
	.byte	$1B
	asl	$404B
	sbc	$50FC, x
L86D0:	.byte	$3A
	ora	($0A), y
	ora	$D5F, x
	clc
	.byte	$1C
	ora	$1D5F, x
	ora	($18), y
	asl	$205F, x
	.byte	$12
	.byte	$1C
	ora	($5F), y
	ora	$5F18, x
	.byte	$0B
	asl	$4B22, x
	rti

	sbc	$50FC, x
	bit	$1D5F
	ora	($0A), y
	.byte	$17
	.byte	$14
	.byte	$5F
	ora	$E11, x
	asl	$FD52
	.byte	$FC
	bvc	L8733
	ora	$0E, x
	asl	a
	.byte	$1C
	asl	$5F48
	.byte	$0C
	clc
	asl	$0E, x
	.byte	$5F
	asl	a
	bpl	L8719
	.byte	$12
	.byte	$17
	.byte	$52
	.byte	$FC
	sed
	.byte	$5F
	.byte	$0C
	ora	($0A), y
	.byte	$17
L8719:	ora	$D0E, x
	.byte	$5F
	ora	$E11, x
	.byte	$5F
	.byte	$1C
	ora	$150E, y
	.byte	$15
L8726:	.byte	$5F
	clc
	.byte	$0F
	.byte	$5F
	inc	$47, x
	.byte	$FC
	sed
	.byte	$5F
	.byte	$0C
	asl	a
	.byte	$17
	.byte	$17
L8733:	clc
	ora	$225F, x
	asl	$5F1D
	asl	$0E1C, x
	.byte	$5F
	ora	$E11, x
	.byte	$5F
	.byte	$1C
	ora	$150E, y
	ora	$47, x
	.byte	$FC
	.byte	$37
	ora	($22), y
	.byte	$5F
	bmi	L8782
	.byte	$5F
	.byte	$12
	.byte	$1C
	.byte	$5F
	ora	$1818, x
	.byte	$5F
	ora	$18, x
	jsr	$FC47
	rts

	sbc	$1E25, x
	ora	$175F, x
	clc
	ora	$1211, x
	.byte	$17
	bpl	L87C9
	ora	($0A), y
	ora	$0E19, y
	.byte	$17
	asl	$470D
	.byte	$FC
	and	$28, x
	.byte	$33
	plp
	.byte	$2F
	.byte	$5F
	ora	($0A), y
	.byte	$1C
	.byte	$5F
	ora	$18, x
	.byte	$1C
	.byte	$1D
L8782:	.byte	$5F
	.byte	$12
	ora	$5F1C, x
	asl	$0F0F
	asl	$1D0C
	.byte	$47
	.byte	$FC
	bit	$5F
	ora	$1B18, x
	.byte	$0C
	ora	($5F), y
	.byte	$0C
	asl	a
	.byte	$17
	.byte	$5F
	.byte	$0B
	asl	$1E5F
	.byte	$1C
	asl	$5F0D
	clc
	.byte	$17
	ora	$22, x
	.byte	$5F
	.byte	$12
	.byte	$17
	.byte	$5F
	ora	$1B0A
	.byte	$14
	.byte	$5F
	ora	$0A15, y
	.byte	$0C
	asl	$471C
	.byte	$FC
	sed
	.byte	$5F
	.byte	$1C
	ora	$121B, y
	.byte	$17
	.byte	$14
	ora	$0E, x
	ora	$1D5F
	ora	($0E), y
	.byte	$5F
	.byte	$29
L87C9:	asl	a
	.byte	$12
	.byte	$1B
	.byte	$22
	.byte	$5F
	.byte	$3A
	asl	a
	ora	$1B0E, x
	.byte	$5F
	clc
	.byte	$1F
	asl	$5F1B
	ora	($12), y
	.byte	$1C
	.byte	$5F
	.byte	$0B
	clc
	ora	$4722
	.byte	$FC
	.byte	$37
	ora	($0E), y
	.byte	$5F
	and	#$0A
	.byte	$12
	.byte	$1B
	.byte	$22
	.byte	$5F
	.byte	$3A
	asl	a
	ora	$1B0E, x
	.byte	$5F
	ora	($0A), y
	.byte	$1C
	.byte	$5F
	ora	$18, x
	.byte	$1C
	ora	$125F, x
	ora	$5F1C, x
	asl	$0F0F
	asl	$1D0C
	.byte	$47
	.byte	$FC
	.byte	$37
	ora	($0E), y
	.byte	$5F
	.byte	$3A
	.byte	$12
	.byte	$17
	bpl	L882D
	.byte	$5F
	clc
	.byte	$0F
	.byte	$5F
	ora	$E11, x
	.byte	$5F
	.byte	$3A
	.byte	$22
	.byte	$1F
	asl	$171B
	.byte	$5F
	.byte	$0C
	asl	a
	.byte	$17
	.byte	$17
	clc
	ora	$0B5F, x
	asl	$1E5F
	.byte	$1C
	.byte	$0E
L882D:	ora	$115F
	asl	$0E1B
	.byte	$47
	.byte	$FC
	sed
	.byte	$5F
	ora	$1B11, x
	asl	$5F20
	.byte	$37
	ora	($0E), y
	.byte	$5F
	.byte	$3A
	.byte	$12
	.byte	$17
	bpl	L8862
	.byte	$5F
	clc
	.byte	$0F
	.byte	$5F
	ora	$E11, x
	.byte	$5F
	.byte	$3A
	.byte	$22
	.byte	$1F
	asl	$171B
	.byte	$5F
	asl	$5F19, x
	.byte	$12
	.byte	$17
	ora	$5F18, x
	ora	$E11, x
	.byte	$5F
	.byte	$1C
L8862:	.byte	$14
	.byte	$22
	.byte	$47
	.byte	$FC
	sed
	.byte	$5F
	ora	$1718
	.byte	$17
	asl	$5F0D
	ora	$E11, x
	.byte	$5F
	.byte	$1C
	.byte	$0C
	asl	a
	ora	$0E, x
	.byte	$5F
	clc
	.byte	$0F
	.byte	$5F
	ora	$E11, x
	.byte	$5F
	ora	$0A1B
	bpl	L889D
	.byte	$17
	.byte	$47
	.byte	$FC
	.byte	$37
	ora	($18), y
	asl	$A5F, x
	.byte	$1B
	ora	$A5F, x
	ora	$1B, x
	asl	$0D0A
	.byte	$22
	.byte	$5F
	jsr	$A0E
	.byte	$1B
L889D:	.byte	$12
	.byte	$17
	bpl	L8900
	ora	$E11, x
	.byte	$5F
	.byte	$1C
	.byte	$0C
	asl	a
	ora	$0E, x
	.byte	$5F
	clc
	.byte	$0F
	.byte	$5F
	ora	$E11, x
	.byte	$5F
	ora	$0A1B
	bpl	L88CF
	.byte	$17
	.byte	$47
	.byte	$FC
	sed
	.byte	$5F
	.byte	$0B
	ora	$0E, x
	jsr	$1D5F
	ora	($0E), y
	.byte	$5F
	and	#$0A
	.byte	$12
	.byte	$1B
	.byte	$12
	asl	$531C
	.byte	$5F
	.byte	$29
L88CF:	ora	$1E, x
	ora	$470E, x
	.byte	$FC
	and	($18), y
	ora	$1211, x
	.byte	$17
	bpl	L893C
	clc
	.byte	$0F
	.byte	$5F
	asl	$0E1C, x
	.byte	$5F
	ora	($0A), y
	.byte	$1C
	.byte	$5F
	.byte	$22
	asl	$5F1D
	.byte	$0B
	asl	$170E
	.byte	$5F
	bpl	L8905
	.byte	$1F
	asl	$5F17
	ora	$5F18, x
	ora	$E11, x
	asl	$FC47
L8900:	sed
	.byte	$5F
	ora	$1D1E, y
L8905:	.byte	$5F
	clc
	.byte	$17
	.byte	$5F
	ora	$E11, x
	.byte	$5F
	and	#$12
	bpl	L8922
	ora	$1B0E, x
	.byte	$53
	.byte	$1C
	.byte	$5F
	and	$12, x
	.byte	$17
	bpl	L8963
	.byte	$FC
	sed
	.byte	$5F
	asl	a
	.byte	$0D
	.byte	$13
L8922:	asl	$1D1C, x
	asl	$5F0D
	ora	$E11, x
	.byte	$5F
	ora	$1C18, y
	.byte	$12
	ora	$1812, x
	.byte	$17
	.byte	$5F
	clc
	.byte	$0F
	.byte	$5F
	ora	$E11, x
	.byte	$5F
L893C:	and	#$12
	bpl	L8951
	ora	$1B0E, x
	.byte	$53
	.byte	$1C
	.byte	$5F
	and	$12, x
	.byte	$17
	bpl	L8992
	.byte	$FC
	sed
	.byte	$5F
	ora	($0E), y
	.byte	$15
L8951:	ora	$1D5F
	ora	($0E), y
	.byte	$5F
	.byte	$F7
	.byte	$5F
	ora	$1012, x
	ora	($1D), y
	ora	$22, x
	.byte	$47
	.byte	$FC
	sed
L8963:	.byte	$5F
	ora	$0A15, y
	.byte	$22
	asl	$5F0D
	asl	a
	.byte	$5F
	.byte	$1C
	jsr	$E0E
	ora	$165F, x
	asl	$1815
	ora	$5F22
	clc
	.byte	$17
	.byte	$5F
	ora	$E11, x
	.byte	$5F
	ora	($0A), y
	.byte	$1B
	ora	$FC47, y
	sed
	.byte	$5F
	ora	$1D1E, y
	.byte	$5F
	clc
	.byte	$17
	.byte	$5F
	.byte	$1D
	.byte	$11
L8992:	asl	$F75F
	.byte	$5F
	asl	a
	.byte	$17
	ora	$205F
	asl	a
	.byte	$1C
	.byte	$5F
	.byte	$0C
	asl	$1C1B, x
	asl	$4C0D
	.byte	$47
	.byte	$FB
	sbc	$1137, x
	.byte	$22
	.byte	$5F
	.byte	$0B
	clc
	ora	$5F22
	.byte	$12
	.byte	$1C
	.byte	$5F
	.byte	$0B
	asl	$1712
	bpl	L8A19
	.byte	$1C
	.byte	$1A
	asl	$E0E, x
	.byte	$23
	asl	$470D
	.byte	$FC
	.byte	$37
	ora	($0E), y
	.byte	$5F
	.byte	$F7
	.byte	$5F
	.byte	$12
	.byte	$1C
	.byte	$5F
	.byte	$1C
	.byte	$1A
	asl	$E0E, x
	.byte	$23
	.byte	$12
	.byte	$17
	bpl	L8A36
	ora	$2211, x
	.byte	$5F
	.byte	$0B
	clc
	ora	$4722
	.byte	$FC
	bvc	L8A09
	asl	$1C1B, x
	asl	$5F0D
	clc
	.byte	$17
	asl	$0B48
	asl	$105F
	clc
	.byte	$17
	asl	$404C
	.byte	$FC
	bvc	L8A25
	.byte	$5F
	asl	a
	asl	$5F, x
	ora	$18, x
	clc
	.byte	$14
	.byte	$12
	.byte	$17
	bpl	L8A64
	.byte	$0F
	clc
	.byte	$1B
	.byte	$5F
L8A09:	ora	$E11, x
	.byte	$5F
	.byte	$0C
	asl	a
	.byte	$1C
	ora	$0E15, x
	.byte	$5F
	.byte	$0C
	asl	$1515
	asl	a
L8A19:	.byte	$1B
	.byte	$47
	sbc	$5F2C, x
	ora	($0E), y
	asl	a
	.byte	$1B
	ora	$125F
L8A25:	ora	$125F, x
	.byte	$1C
	.byte	$5F
	.byte	$17
	clc
	ora	$0E5F, x
	asl	a
	.byte	$1C
	.byte	$12
	ora	$22, x
	.byte	$5F
	.byte	$0F
L8A36:	clc
	asl	$0D17, x
	.byte	$52
	.byte	$FC
	bvc	L8A75
	ora	($18), y
	asl	$165F, x
	asl	$1D1C, x
	.byte	$5F
	ora	($0A), y
	.byte	$1F
	asl	$A5F
	.byte	$5F
	.byte	$14
	asl	$5F22
	ora	$5F18, x
	clc
	ora	$170E, y
	.byte	$5F
	asl	a
	.byte	$5F
	ora	$1818
	.byte	$1B
	.byte	$52
	.byte	$FC
	bvc	L8A9B
L8A64:	clc
	.byte	$5F
	.byte	$0B
	asl	$180C
	asl	$0E, x
	.byte	$5F
	.byte	$1C
	ora	$181B, x
	.byte	$17
	bpl	L8AD3
	.byte	$0E
L8A75:	.byte	$17
	clc
	asl	$1110, x
	.byte	$5F
	ora	$5F18, x
	.byte	$0F
	asl	a
	.byte	$0C
	asl	$0F5F
	asl	$1E1D, x
	.byte	$1B
	asl	$1D5F
	.byte	$1B
	.byte	$12
	asl	a
	ora	$1C, x
	.byte	$5F
	ora	$1811, x
	asl	$165F, x
	asl	$1D1C, x
	.byte	$5F
L8A9B:	.byte	$0F
	.byte	$12
	.byte	$1B
	.byte	$1C
	ora	$0B5F, x
	asl	a
	ora	$151D, x
	asl	$165F
	asl	a
	.byte	$17
	.byte	$22
	.byte	$5F
	.byte	$0F
	clc
	asl	$521C
	.byte	$FC
	bvc	L8AE3
	.byte	$12
	.byte	$17
	bpl	L8B18
	.byte	$2F
	clc
	.byte	$1B
	.byte	$12
	.byte	$14
	.byte	$5F
	jsr	$1512
	ora	$5F, x
	.byte	$1B
	asl	$180C
	.byte	$1B
	ora	$1D5F
	ora	($22), y
	.byte	$5F
	ora	$E0E
	.byte	$0D
L8AD3:	.byte	$1C
	.byte	$5F
	.byte	$12
	.byte	$17
	.byte	$5F
	ora	($12), y
	.byte	$1C
	.byte	$5F
	bit	$1916
	asl	$121B
	asl	a
L8AE3:	ora	$5F, x
	rol	$0C, x
	.byte	$1B
	clc
	ora	$15, x
	.byte	$5F
	.byte	$1C
	clc
	.byte	$5F
	ora	$1811, x
	asl	$165F, x
	asl	a
	.byte	$22
	.byte	$5F
	.byte	$1B
	asl	$1E1D
	.byte	$1B
	.byte	$17
	.byte	$5F
	ora	$5F18, x
	ora	$2211, x
	.byte	$5F
	.byte	$1A
	asl	$1C0E, x
	ora	$155F, x
	asl	a
	ora	$1B0E, x
	.byte	$52
	.byte	$FC
	bvc	L8B4F
	ora	($0E), y
	.byte	$17
L8B18:	.byte	$5F
	ora	$E11, x
	.byte	$5F
	.byte	$1C
	asl	$5F17, x
	asl	a
	.byte	$17
	ora	$1B5F
	asl	a
	.byte	$12
	.byte	$17
	.byte	$5F
	asl	$0E, x
	asl	$481D
	.byte	$5F
	asl	a
	.byte	$5F
	and	$0A, x
	.byte	$12
	.byte	$17
	.byte	$0B
	clc
	jsr	$255F
	.byte	$1B
	.byte	$12
	ora	$0E10
	.byte	$5F
	.byte	$1C
	ora	($0A), y
	ora	$15, x
	.byte	$5F
	asl	a
	ora	$0E19, y
	asl	a
	.byte	$1B
	.byte	$52
	.byte	$FC
L8B4F:	bvc	L8B82
	asl	$0E1F
	.byte	$1B
	.byte	$5F
	ora	$0E18
	.byte	$1C
	.byte	$5F
	asl	a
	.byte	$5F
	.byte	$0B
	.byte	$1B
	asl	a
	.byte	$1F
	asl	$195F
	asl	$1C1B
	clc
	.byte	$17
	.byte	$5F
	.byte	$1C
	ora	$A0E, x
	ora	$52, x
	.byte	$FC
	bvc	L8BAA
	ora	($0E), y
	.byte	$1B
	asl	$205F
	asl	a
	.byte	$1C
	.byte	$5F
	asl	a
	.byte	$5F
	ora	$1612, x
	.byte	$0E
L8B82:	.byte	$5F
	jsr	$E11
	.byte	$17
	.byte	$5F
	and	$1B
	asl	$0C0C
	clc
	.byte	$17
	asl	a
	.byte	$1B
	.byte	$22
	.byte	$5F
	jsr	$1C0A
	.byte	$5F
	asl	a
	.byte	$5F
	ora	$1B0A, y
	asl	a
	ora	$1C12
	asl	$FD47
	.byte	$37
	ora	($0E), y
	.byte	$17
	.byte	$5F
	.byte	$1D
	.byte	$11
L8BAA:	asl	$275F
	.byte	$1B
	asl	a
	bpl	L8BC9
	.byte	$17
	ora	$18, x
	.byte	$1B
	ora	$1C53
	.byte	$5F
	asl	$12, x
	.byte	$17
	.byte	$12
	clc
	.byte	$17
	.byte	$1C
	.byte	$5F
	.byte	$0C
	asl	a
	asl	$0E, x
	.byte	$52
	.byte	$FC
	bvc	L8BF8
L8BC9:	asl	$5F1D
	asl	$5F1C, x
	jsr	$1C12
	ora	($5F), y
	ora	$E11, x
	.byte	$5F
	jsr	$1B0A
	.byte	$1B
	.byte	$12
	clc
	.byte	$1B
	.byte	$5F
	jsr	$150E
	ora	$4C, x
	rti

	.byte	$FB
	sbc	$3050, x
	asl	a
	.byte	$22
	.byte	$5F
	ora	$E11, x
	.byte	$5F
	ora	$12, x
	bpl	L8C06
	ora	$0B5F, x
L8BF8:	asl	$1D5F
	ora	($22), y
	.byte	$5F
	.byte	$1C
	ora	$0E1B, x
	.byte	$17
	bpl	L8C22
	.byte	$11
L8C06:	jmp	$FC40

	bvc	L8C37
	.byte	$0F
	.byte	$5F
	ora	$2211, x
	.byte	$5F
	.byte	$2B
	.byte	$12
	ora	$335F, x
	clc
	.byte	$12
	.byte	$17
	ora	$5F1C, x
	asl	a
	.byte	$1B
	asl	$115F
	.byte	$12
L8C22:	bpl	L8C35
	.byte	$5F
	asl	$1817
	asl	$1110, x
	pha
	.byte	$5F
	.byte	$0B
	.byte	$22
	.byte	$5F
	asl	a
	ora	$15, x
	.byte	$5F
	.byte	$16
L8C35:	.byte	$0E
	asl	a
L8C37:	.byte	$17
	.byte	$1C
	pha
	.byte	$5F
	asl	$1D17
	asl	$521B
	.byte	$FC
	bvc	L8C7E
	asl	$A5F
	.byte	$1B
	asl	$165F
	asl	$0C1B
	ora	($0A), y
	.byte	$17
	ora	$5F1C, x
	jsr	$1811
	.byte	$5F
	ora	($0A), y
	.byte	$1F
	asl	$1D5F
	.byte	$1B
	asl	a
	.byte	$1F
	asl	$0E15
	ora	$165F
	asl	$110C, x
	.byte	$5F
	.byte	$12
	.byte	$17
	.byte	$5F
	ora	$1211, x
	.byte	$1C
	.byte	$5F
	ora	$0A, x
	.byte	$17
	ora	$FD47
	bmi	L8C85
	.byte	$17
	.byte	$22
	.byte	$5F
L8C7E:	clc
	.byte	$0F
	.byte	$5F
	clc
	asl	$5F1B, x
L8C85:	.byte	$0C
	clc
	ora	$15, x
	asl	$100A
	asl	$1C0E, x
	.byte	$5F
	ora	($0A), y
	.byte	$1F
	asl	$0B5F
	asl	$170E
	.byte	$5F
	.byte	$14
	.byte	$12
	ora	$15, x
	asl	$5F0D
	.byte	$0B
	.byte	$22
	.byte	$5F
	.byte	$1C
	asl	$1F1B
	asl	a
	.byte	$17
	ora	$5F1C, x
	clc
	.byte	$0F
	.byte	$5F
	ora	$E11, x
	.byte	$5F
	.byte	$27
	.byte	$1B
	asl	a
	bpl	L8CD1
	.byte	$17
	ora	$18, x
	.byte	$1B
	ora	$FC52
	bvc	L8CF7
	asl	$1816, x
	.byte	$1B
	.byte	$5F
	ora	($0A), y
	.byte	$1C
	.byte	$5F
	.byte	$12
	ora	$1D5F, x
	ora	($0A), y
L8CD1:	ora	$0E5F, x
	.byte	$17
	ora	$1B12, x
	asl	$1D5F
	clc
	jsr	$1C17
	.byte	$5F
	ora	($0A), y
	.byte	$1F
	asl	$0B5F
	asl	$170E
	.byte	$5F
	ora	$1C0E
	ora	$181B, x
	.byte	$22
	asl	$5F0D
	.byte	$0B
	.byte	$22
	.byte	$5F
L8CF7:	ora	$E11, x
	.byte	$5F
	.byte	$27
	.byte	$1B
	asl	a
	bpl	L8D18
	.byte	$17
	ora	$18, x
	.byte	$1B
	ora	$1C53
	.byte	$5F
	.byte	$1C
	asl	$1F1B
	asl	a
	.byte	$17
	ora	$521C, x
	.byte	$FC
	bvc	L8D4E
	asl	$0C15
	clc
L8D18:	asl	$0E, x
	.byte	$5F
	ora	$5F18, x
	.byte	$37
	asl	a
	.byte	$17
	ora	$100E, x
	asl	$5F15
	rol	$0A
	.byte	$1C
	ora	$0E15, x
	.byte	$52
	.byte	$FC
	bvc	L8D5D
	.byte	$17
	.byte	$5F
	rol	a
	asl	a
	.byte	$1B
	.byte	$12
	.byte	$17
	ora	($0A), y
	asl	$48, x
	ora	$18, x
	clc
	.byte	$14
	.byte	$5F
	.byte	$0F
	clc
	.byte	$1B
	.byte	$5F
	ora	$E11, x
	.byte	$5F
	bpl	L8D66
	asl	a
	.byte	$1F
	.byte	$0E
L8D4E:	.byte	$5F
	clc
	.byte	$0F
	.byte	$5F
	rol	a
	asl	a
	.byte	$1B
	.byte	$12
	.byte	$17
	.byte	$47
	sbc	$1137, x
	clc
	.byte	$1E
L8D5D:	.byte	$5F
	asl	$1E, x
	.byte	$1C
	ora	$195F, x
	.byte	$1E
	.byte	$1C
L8D66:	ora	($5F), y
	clc
	.byte	$17
	.byte	$5F
	asl	a
	.byte	$5F
	jsr	$150A
	ora	$5F, x
	clc
	.byte	$0F
	.byte	$5F
	ora	$1B0A
	.byte	$14
	.byte	$17
	asl	$1C1C
	.byte	$5F
	ora	$E11, x
	.byte	$1B
	asl	$FC52
	bvc	L8DAB
	.byte	$5F
	jsr	$1B18
	ora	$185F
	.byte	$0F
	.byte	$5F
	asl	a
	ora	$121F
	.byte	$0C
	asl	$FB52
	sbc	$3650, x
	asl	a
	.byte	$1F
	asl	$1D5F
	ora	($22), y
	.byte	$5F
	asl	$18, x
	.byte	$17
	asl	$5F22
	.byte	$0F
	clc
L8DAB:	.byte	$1B
	.byte	$5F
	asl	$18, x
	.byte	$1B
	asl	$0E5F
	and	($19, x)
	asl	$1C17
	.byte	$12
	.byte	$1F
	asl	$A5F
	.byte	$1B
	asl	$18, x
	.byte	$1B
	.byte	$52
	.byte	$FC
	bvc	L8DF4
	.byte	$12
	.byte	$1C
	ora	$170E, x
	.byte	$5F
	ora	$5F18, x
	jsr	$A11
	ora	$195F, x
	asl	$1918
	ora	$0E, x
	.byte	$5F
	.byte	$1C
	asl	a
	.byte	$22
	.byte	$47
	sbc	$1D2C, x
	.byte	$5F
	.byte	$0C
	asl	a
	.byte	$17
	.byte	$5F
	.byte	$0B
	asl	$185F
	.byte	$0F
	.byte	$5F
	bpl	L8E09
	asl	$1D0A
	.byte	$5F
	ora	($0E), y
L8DF4:	ora	$19, x
	.byte	$52
	.byte	$FC
	bvc	L8E1F
	asl	$0A20
	.byte	$1B
	asl	$1D5F
	ora	($0E), y
	.byte	$5F
	.byte	$0B
	.byte	$1B
	.byte	$12
	.byte	$0D
	.byte	$10
L8E09:	asl	$4C1C
	rti

	.byte	$FB
	sbc	$2750, x
	asl	a
	.byte	$17
	bpl	L8E23
	.byte	$1B
	.byte	$5F
	bpl	L8E34
	clc
	jsr	$5F1C
	.byte	$20
	.byte	$11
L8E1F:	asl	$5F17
	.byte	$1D
L8E23:	ora	($18), y
	asl	$0C5F, x
	.byte	$1B
	clc
	.byte	$1C
	.byte	$1C
	asl	$521C
	.byte	$FC
	bvc	L8E69
	ora	($0E), y
L8E34:	.byte	$1B
	asl	$125F
	.byte	$1C
	.byte	$5F
	asl	a
	.byte	$5F
	ora	$2018, x
	.byte	$17
	.byte	$5F
	jsr	$E11
	.byte	$1B
	asl	$165F
	asl	a
	bpl	L8E5D
	.byte	$0C
	.byte	$5F
	.byte	$14
	asl	$1C22
	.byte	$5F
	.byte	$0C
	asl	a
	.byte	$17
	.byte	$5F
	.byte	$0B
	asl	$195F
	asl	$0C1B, x
L8E5D:	ora	($0A), y
	.byte	$1C
	asl	$520D
	.byte	$FC
	bvc	L8E9C
	clc
	asl	$0E, x
L8E69:	.byte	$5F
	.byte	$1C
	asl	a
	.byte	$22
	.byte	$5F
	ora	$A11, x
	ora	$2A5F, x
	asl	a
	.byte	$1B
	.byte	$12
	.byte	$17
	.byte	$53
	.byte	$1C
	.byte	$5F
	bpl	L8E98
	asl	a
	.byte	$1F
	asl	$125F
	.byte	$1C
	.byte	$5F
	ora	($18), y
	asl	$0E, x
	.byte	$5F
	ora	$5F18, x
	asl	a
	.byte	$5F
	rol	$12, x
	ora	$1F, x
	asl	$5F1B
	.byte	$2B
	asl	a
	.byte	$1B
L8E98:	ora	$FC52, y
	.byte	$50
L8E9C:	plp
	.byte	$17
	ora	$1B0E, x
	.byte	$5F
	jsr	$E11
	.byte	$1B
	asl	$1D5F
	ora	($18), y
	asl	$0C5F, x
	asl	a
	.byte	$17
	.byte	$52
	.byte	$FC
	bvc	L8EEE
	asl	$0C15
	clc
	asl	$0E, x
	jmp	$28FD

	.byte	$17
	ora	$1B0E, x
	.byte	$5F
	ora	$E11, x
	.byte	$5F
	.byte	$1C
	ora	($18), y
	ora	$A5F, y
	.byte	$17
	ora	$1C5F
	ora	$A0E, y
	.byte	$14
	.byte	$5F
	ora	$5F18, x
	.byte	$12
	ora	$5F1C, x
	.byte	$14
	asl	$190E
	asl	$5F1B
	asl	a
	.byte	$0C
	.byte	$1B
	clc
	.byte	$1C
	.byte	$1C
	.byte	$5F
	ora	$E11, x
	.byte	$5F
L8EEE:	ora	$1C0E
	.byte	$14
	.byte	$52
	.byte	$FC
	bvc	L8F2D
	ora	($18), y
	asl	$A5F, x
	.byte	$1B
	ora	$165F, x
	clc
	.byte	$1C
	ora	$205F, x
	asl	$0C15
	clc
	asl	$0E, x
	.byte	$5F
	.byte	$12
	.byte	$17
	.byte	$5F
	and	$1B
	asl	$0C0C
	clc
	.byte	$17
	asl	a
	.byte	$1B
	.byte	$22
	.byte	$52
	.byte	$FC
	bvc	L8F56
	asl	a
	ora	$110C, x
	.byte	$5F
	ora	$2211, x
	.byte	$5F
	.byte	$2B
	.byte	$12
	ora	$335F, x
	clc
	.byte	$12
	.byte	$17
L8F2D:	ora	$5F1C, x
	jsr	$E11
	.byte	$17
	.byte	$5F
	.byte	$12
	.byte	$17
	.byte	$5F
	ora	$E11, x
	.byte	$5F
	.byte	$33
	clc
	.byte	$12
	.byte	$1C
	clc
	.byte	$17
	clc
	asl	$5F1C, x
	bmi	L8F52
	.byte	$1B
	.byte	$1C
	ora	($52), y
	.byte	$FC
	bvc	L8F79
	clc
	.byte	$5F
	.byte	$17
L8F52:	clc
	.byte	$1B
	.byte	$1D
	.byte	$11
L8F56:	.byte	$5F
	ora	$5F18, x
	ora	$E11, x
	.byte	$5F
	.byte	$1C
	asl	$1C0A
	ora	($18), y
	.byte	$1B
	asl	$5F48
	ora	$E11, x
	.byte	$17
	.byte	$5F
	.byte	$0F
	clc
	ora	$15, x
	clc
	jsr	$1D5F
	ora	($0E), y
	.byte	$5F
	.byte	$0C
L8F79:	clc
	asl	a
	.byte	$1C
	ora	$1215, x
	.byte	$17
	asl	$205F
	asl	$1D1C
	.byte	$5F
	asl	$1D17, x
	.byte	$12
	ora	$5F, x
	ora	$1811, x
	asl	$115F, x
	asl	a
	ora	$5F11, x
	.byte	$1B
	asl	$0C0A
	ora	($0E), y
	ora	$2A5F
	asl	a
	.byte	$1B
	.byte	$12
	.byte	$17
	ora	($0A), y
	asl	$52, x
	.byte	$FC
	bvc	L8FDC
	clc
	pha
	bit	$A5F
	asl	$5F, x
	.byte	$17
	clc
	ora	$335F, x
	.byte	$1B
	.byte	$12
	.byte	$17
	.byte	$0C
	asl	$1C1C
	.byte	$5F
	rol	a
	jsr	$E0A
	ora	$12, x
	.byte	$17
	.byte	$52
	.byte	$FC
	bvc	L8FFD
	ora	$0E, x
	asl	a
	.byte	$1C
	asl	$1C48
	asl	a
	.byte	$1F
	asl	$1E5F
	.byte	$1C
	.byte	$5F
	.byte	$0F
	.byte	$1B
	clc
	.byte	$16
L8FDC:	.byte	$5F
	ora	$E11, x
	.byte	$5F
	asl	$12, x
	.byte	$17
	.byte	$12
	clc
	.byte	$17
	.byte	$1C
	.byte	$5F
	clc
	.byte	$0F
	.byte	$5F
	ora	$E11, x
	.byte	$5F
	.byte	$27
	.byte	$1B
	asl	a
	bpl	L900D
	.byte	$17
	ora	$18, x
	.byte	$1B
	ora	$FC52
	.byte	$50
L8FFD:	rol	$0E, x
	asl	$2E5F
	.byte	$12
	.byte	$17
	bpl	L9065
	.byte	$2F
	clc
	.byte	$1B
	.byte	$12
	.byte	$14
	.byte	$5F
	.byte	$20
L900D:	ora	($0E), y
	.byte	$17
	.byte	$5F
	ora	$2211, x
	.byte	$5F
	asl	$1921
	asl	$121B
	asl	$0C17
	asl	$155F
	asl	$0E1F
	ora	$1C, x
	.byte	$5F
	asl	a
	.byte	$1B
	asl	$1B5F
	asl	a
	.byte	$12
	.byte	$1C
	asl	$520D
	.byte	$FC
	bvc	L9059
	.byte	$1B
	ora	$1D5F, x
	ora	($18), y
	asl	$1D5F, x
	ora	($0E), y
	.byte	$5F
	ora	$1C0E
	.byte	$0C
	asl	$0D17
	asl	a
	.byte	$17
	ora	$185F, x
	.byte	$0F
	.byte	$5F
	plp
	.byte	$1B
	ora	$121B
	.byte	$0C
	.byte	$14
	.byte	$4B
	.byte	$FD
	.byte	$2B
L9059:	asl	a
	.byte	$1C
	ora	$1D5F, x
	ora	($18), y
	asl	$A5F, x
	.byte	$17
	.byte	$22
L9065:	.byte	$5F
	ora	$181B, y
	clc
	.byte	$0F
	.byte	$4B
	rti

	.byte	$FC
L906E:	bvc	L90AA
	.byte	$12
	ora	$1211, x
	.byte	$17
	.byte	$5F
	.byte	$1C
	.byte	$12
	bpl	L908B
	ora	$185F, x
	.byte	$0F
	.byte	$5F
	.byte	$37
	asl	a
	.byte	$17
	ora	$100E, x
	asl	$5F15
	rol	$0A
	.byte	$1C
L908B:	ora	$0E15, x
	.byte	$5F
	ora	$5F18, x
	ora	$E11, x
	.byte	$5F
	.byte	$1C
	clc
	asl	$111D, x
	.byte	$5F
	.byte	$12
	.byte	$1C
	.byte	$5F
	rol	$11
	asl	a
	.byte	$1B
	ora	$18, x
	.byte	$0C
	.byte	$14
	pha
	rti

	.byte	$FB
L90AA:	sbc	$3750, x
	ora	($0E), y
	.byte	$5F
	.byte	$0F
	clc
	.byte	$1B
	ora	$0E1B, x
	.byte	$1C
	.byte	$1C
	.byte	$5F
	clc
	.byte	$0F
	.byte	$5F
	ora	$E11, x
	.byte	$5F
	.byte	$27
	.byte	$1B
	asl	a
	bpl	L90DD
	.byte	$17
	ora	$18, x
	.byte	$1B
	ora	$FC52
	bvc	L9105
	ora	($12), y
	.byte	$1C
	.byte	$5F
	.byte	$0B
	asl	a
	ora	$5F11, x
	.byte	$0C
	asl	$0E1B, x
	.byte	$1C
	.byte	$5F
L90DD:	.byte	$1B
	ora	($0E), y
	asl	$0A16, x
	ora	$1C12, x
	asl	$52, x
	.byte	$FC
	bvc	L9113
	asl	a
	.byte	$1C
	ora	$185F, x
	.byte	$0F
	.byte	$5F
	.byte	$2B
	asl	a
	asl	$1C14, x
	.byte	$17
	asl	$1C1C
	.byte	$5F
	ora	$E11, x
	.byte	$1B
	asl	$125F
	.byte	$1C
	.byte	$5F
L9105:	asl	a
	.byte	$5F
	ora	$2018, x
	.byte	$17
	pha
	.byte	$5F
	.byte	$54
	ora	$1C12, x
	.byte	$5F
	.byte	$1C
L9113:	asl	a
	.byte	$12
	ora	$5F48
	jsr	$E11
	.byte	$1B
	asl	$185F
	.byte	$17
	asl	$165F
	asl	a
	.byte	$22
	.byte	$5F
	ora	$1B1E, y
	.byte	$0C
	ora	($0A), y
	.byte	$1C
	asl	$205F
	asl	$190A
	clc
	.byte	$17
	.byte	$1C
	.byte	$5F
	clc
	.byte	$0F
	.byte	$5F
	asl	$1D21
	.byte	$1B
	asl	a
	clc
	.byte	$1B
	ora	$1712
	asl	a
	.byte	$1B
	.byte	$22
	.byte	$5F
	.byte	$1A
	asl	$150A, x
	.byte	$12
	ora	$5222, x
	.byte	$FC
	bvc	L9188
	.byte	$12
	asl	$1E, x
	ora	$0D, x
	asl	a
	.byte	$1B
	.byte	$5F
	.byte	$12
	.byte	$1C
	.byte	$5F
	ora	$E11, x
	.byte	$5F
	ora	$0A15, y
	.byte	$0C
	asl	$1D5F
	clc
	.byte	$5F
	.byte	$0B
	asl	$5F22, x
	.byte	$14
	asl	$1C22
	.byte	$52
	.byte	$FC
	bvc	L91A2
	asl	a
	.byte	$1C
	ora	$1D5F, x
	ora	($18), y
	asl	$1C5F, x
	asl	$170E
	.byte	$5F
	and	($0E), y
	.byte	$1C
L9188:	ora	$1B0E, x
	.byte	$4B
	sbc	$5F2C, x
	ora	$1211, x
	.byte	$17
	.byte	$14
	.byte	$5F
	ora	($0E), y
	.byte	$5F
	asl	$0A, x
	.byte	$22
	.byte	$5F
	.byte	$17
	asl	$D0E
	.byte	$5F
	.byte	$11
L91A2:	asl	$1915
	.byte	$52
	.byte	$FC
	bvc	L91D0
	.byte	$1B
	asl	$0D0A
	.byte	$0F
	asl	$5F15, x
	.byte	$12
	.byte	$1C
	.byte	$5F
	ora	$E11, x
	.byte	$5F
	rol	$18, x
	asl	$111D, x
	.byte	$5F
	bit	$151C
	asl	a
	.byte	$17
	ora	$FB52
	sbc	$2A50, x
	.byte	$1B
	asl	$1D0A
	.byte	$5F
	.byte	$1C
	.byte	$1D
L91D0:	.byte	$1B
	asl	$1017
	ora	$5F11, x
	asl	a
	.byte	$17
	ora	$1C5F
	.byte	$14
	.byte	$12
	ora	$15, x
	.byte	$5F
	asl	a
	.byte	$17
	ora	$205F
	.byte	$12
	ora	$185F, x
	.byte	$17
	ora	$22, x
	.byte	$5F
	jsr	$1512
	ora	$5F, x
	.byte	$0B
	.byte	$1B
	.byte	$12
	.byte	$17
	bpl	L9258
	ora	$E11, x
	asl	$0B5F
	asl	a
	.byte	$0C
	.byte	$14
	.byte	$5F
	.byte	$0F
	.byte	$1B
	clc
	asl	$5F, x
	ora	$A11, x
	ora	$195F, x
	ora	$0A, x
	.byte	$0C
	asl	$FC52
	bvc	L9240
	clc
	ora	$0E, x
	asl	$5F, x
	.byte	$12
	.byte	$1C
	.byte	$5F
	asl	a
	.byte	$0F
	.byte	$1B
	asl	a
	.byte	$12
	ora	$185F
	.byte	$0F
	.byte	$5F
	ora	$E11, x
	.byte	$5F
	asl	$1E, x
	.byte	$1C
	.byte	$12
	.byte	$0C
	.byte	$5F
	clc
	.byte	$0F
	.byte	$5F
	ora	$E11, x
	.byte	$5F
	.byte	$0F
	ora	$1E, x
	ora	$480E, x
	.byte	$5F
L9240:	.byte	$1C
	clc
	.byte	$5F
	.byte	$54
	ora	$1C12, x
	.byte	$5F
	.byte	$1C
	asl	a
	.byte	$12
	ora	$FC52
	bvc	L9287
	ora	($12), y
	.byte	$1C
	.byte	$5F
	.byte	$12
	.byte	$1C
	.byte	$5F
	.byte	$1D
L9258:	ora	($0E), y
	.byte	$5F
	.byte	$1F
	.byte	$12
	ora	$15, x
	asl	a
	bpl	L9270
	.byte	$5F
	clc
	.byte	$0F
	.byte	$5F
	rol	$1518
	.byte	$52
	.byte	$FC
	bvc	L9299
	.byte	$17
	.byte	$5F
	.byte	$15
L9270:	asl	$0E10
	.byte	$17
	ora	$5F1C
	.byte	$12
	ora	$125F, x
	.byte	$1C
	.byte	$5F
	.byte	$1C
	asl	a
	.byte	$12
	ora	$1D5F
	ora	($0A), y
	.byte	$1D
	.byte	$5F
L9287:	.byte	$0F
	asl	a
	.byte	$12
	.byte	$1B
	.byte	$12
	asl	$5F1C
	.byte	$14
	.byte	$17
	clc
	jsr	$115F
	clc
	jsr	$1D5F
L9299:	clc
	.byte	$5F
	ora	$1D1E, y
	.byte	$5F
	rol	a
	clc
	ora	$0E, x
	asl	$5F, x
	ora	$5F18, x
	.byte	$1C
	ora	$0E, x
	asl	$5219
	.byte	$FC
	bvc	L92E8
	ora	($0E), y
	.byte	$5F
	ora	($0A), y
	.byte	$1B
	ora	$A5F, y
	ora	$1B1D, x
	asl	a
	.byte	$0C
	ora	$5F1C, x
	asl	$0E17
	asl	$12, x
	asl	$471C
	sbc	$1D36, x
	asl	a
	.byte	$22
	.byte	$5F
	asl	a
	jsr	$220A
	.byte	$5F
	.byte	$0F
	.byte	$1B
	clc
	asl	$5F, x
	ora	$E11, x
	.byte	$5F
	bpl	L92FB
	asl	a
	.byte	$1F
	asl	$125F
	.byte	$17
	.byte	$5F
	rol	a
L92E8:	asl	a
	.byte	$1B
	.byte	$12
	.byte	$17
	ora	($0A), y
	asl	$52, x
	.byte	$FC
	bvc	L931F
	.byte	$53
	asl	$5F, x
	ora	$1818, x
	.byte	$5F
	.byte	$0B
L92FB:	asl	$221C, x
	.byte	$47
	sbc	$1C24, x
	.byte	$14
	.byte	$5F
	ora	$E11, x
	.byte	$5F
	clc
	ora	$E11, x
	.byte	$1B
	.byte	$5F
	bpl	L932E
	asl	a
	.byte	$1B
	ora	$FC52
	bvc	L9343
	.byte	$5F
	.byte	$1C
	asl	$1010, x
	asl	$1D1C
L931F:	.byte	$5F
	asl	$0A, x
	.byte	$14
	.byte	$12
	.byte	$17
	bpl	L9386
	asl	a
	.byte	$5F
	asl	$0A, x
	ora	$125F, y
L932E:	.byte	$0F
	.byte	$5F
	ora	$2211, x
	.byte	$5F
	ora	$1D0A, y
	ora	($5F), y
	ora	$0E, x
	asl	a
	ora	$5F1C
	.byte	$12
	.byte	$17
	.byte	$1D
	clc
L9343:	.byte	$5F
	ora	$E11, x
	.byte	$5F
	ora	$1B0A
	.byte	$14
	.byte	$17
	asl	$1C1C
	.byte	$52
	.byte	$FC
	bvc	L9386
	.byte	$17
	.byte	$0C
	asl	$1D5F
	ora	($0E), y
	.byte	$1B
	asl	$205F
	asl	a
	.byte	$1C
	.byte	$5F
	asl	a
	.byte	$5F
	ora	$2018, x
	.byte	$17
	.byte	$5F
	.byte	$0C
	asl	a
	ora	$15, x
	asl	$5F0D
	.byte	$2B
	asl	a
	asl	$1C14, x
	.byte	$17
	asl	$1C1C
	.byte	$5F
	.byte	$0F
	asl	a
	.byte	$1B
	.byte	$5F
	ora	$5F18, x
	ora	$E11, x
	.byte	$5F
	.byte	$1C
L9386:	clc
	asl	$111D, x
	pha
	.byte	$0B
	asl	$5F1D, x
	bit	$D5F
	clc
	.byte	$5F
	.byte	$17
	clc
	ora	$145F, x
	.byte	$17
	clc
	jsr	$125F
	.byte	$0F
	.byte	$5F
	.byte	$12
	ora	$1C5F, x
	ora	$1512, x
	ora	$5F, x
	asl	$1221
	.byte	$1C
	ora	$521C, x
	.byte	$FC
	bvc	L93DF
	.byte	$5F
	ora	($0A), y
	ora	$5F0E, x
	ora	$180E, y
	ora	$0E15, y
	jmp	$2AFD

	clc
	jmp	$2F5F

	asl	$1F0A
	asl	$165F
	asl	$404C
	.byte	$FC
	bvc	L9409
	ora	($0E), y
	.byte	$22
	.byte	$5F
	.byte	$1C
	asl	a
	.byte	$22
	.byte	$5F
	ora	$A11, x
	.byte	$1D
	.byte	$5F
L93DF:	plp
	.byte	$1B
	ora	$121B
	.byte	$0C
	.byte	$14
	.byte	$53
	.byte	$1C
	.byte	$5F
	asl	a
	.byte	$1B
	asl	$18, x
	.byte	$1B
	.byte	$5F
	jsr	$1C0A
	.byte	$5F
	ora	($12), y
	ora	$0E0D
	.byte	$17
	.byte	$5F
	ora	$18, x
	.byte	$17
	bpl	L945E
	asl	a
	bpl	L941A
	.byte	$52
	.byte	$FC
	bvc	L9436
	asl	a
	.byte	$17
	.byte	$22
L9409:	.byte	$5F
	.byte	$0B
	asl	$1215
	asl	$0E1F
	.byte	$5F
	ora	$A11, x
	ora	$335F, x
	.byte	$1B
	.byte	$12
L941A:	.byte	$17
	.byte	$0C
	asl	$1C1C
	.byte	$5F
	rol	a
	jsr	$E0A
	ora	$12, x
	.byte	$17
	.byte	$5F
	.byte	$12
	.byte	$1C
	.byte	$5F
	ora	($12), y
	ora	$0E0D
	.byte	$17
	.byte	$5F
	asl	a
	jsr	$220A
L9436:	.byte	$5F
	.byte	$12
	.byte	$17
	.byte	$5F
	asl	a
	.byte	$5F
	.byte	$0C
	asl	a
	.byte	$1F
	asl	$FC52
	bvc	L9470
	.byte	$5F
	ora	($0A), y
	.byte	$1F
	asl	$115F
	asl	$1B0A
	ora	$185F
	.byte	$0F
	.byte	$5F
	clc
	.byte	$17
	asl	$175F
	asl	a
	asl	$0E, x
	ora	$315F
L945E:	asl	$1D1C
	asl	$471B
	sbc	$1827, x
	.byte	$1C
	ora	$1D5F, x
	ora	($18), y
	asl	$145F, x
L9470:	.byte	$17
	clc
	jsr	$1C5F
	asl	$110C, x
	.byte	$5F
	asl	a
	.byte	$5F
	clc
	.byte	$17
	asl	$404B
	.byte	$FC
	bvc	L94AD
	asl	a
	.byte	$1B
	.byte	$12
	.byte	$17
	pha
	.byte	$5F
	asl	a
	.byte	$5F
	jsr	$170A
	ora	$1B0E
	.byte	$12
	.byte	$17
	bpl	L94F4
	asl	$12, x
	.byte	$17
	.byte	$1C
	ora	$0E1B, x
	ora	$5F, x
	clc
	.byte	$0F
	.byte	$5F
	ora	$0E, x
	bpl	L94B3
	.byte	$17
	ora	$1B0A
	.byte	$22
	.byte	$5F
	.byte	$0F
	asl	a
L94AD:	asl	$0E, x
	pha
	.byte	$5F
	.byte	$12
	.byte	$1C
L94B3:	.byte	$5F
	.byte	$1C
	asl	a
	.byte	$12
	ora	$1D5F
	clc
	.byte	$5F
	ora	($0A), y
	.byte	$1F
	asl	$0B5F
	asl	$1512, x
	ora	$1D5F, x
	ora	($12), y
	.byte	$1C
	.byte	$5F
	ora	$2018, x
	.byte	$17
	.byte	$52
	.byte	$FC
	bvc	L950E
	asl	$0C15
	clc
	asl	$0E, x
	.byte	$5F
	ora	$5F18, x
	rol	a
	asl	a
	.byte	$1B
	.byte	$12
	.byte	$17
	ora	($0A), y
	asl	$47, x
	sbc	$0A30, x
	.byte	$22
	.byte	$5F
	ora	$2211, x
	.byte	$5F
	.byte	$1C
	ora	$220A, x
L94F4:	.byte	$5F
	.byte	$0B
	asl	$A5F
	.byte	$5F
	ora	$A0E, y
	.byte	$0C
	asl	$1E0F
	ora	$5F, x
	clc
	.byte	$17
	asl	$FC52
	bvc	L9536
	ora	$125F, x
	.byte	$1C
L950E:	.byte	$5F
	.byte	$1C
	asl	a
	.byte	$12
	ora	$1D5F
	ora	($0A), y
	ora	$1D5F, x
	ora	($0E), y
	.byte	$5F
	.byte	$33
	.byte	$1B
	.byte	$12
	.byte	$17
	.byte	$0C
	asl	$1C1C
	.byte	$5F
	jsr	$1C0A
	.byte	$5F
	.byte	$14
	.byte	$12
	ora	$0A17
	ora	$0E19, y
	ora	$A5F
	.byte	$17
L9536:	ora	$1D5F
	asl	a
	.byte	$14
	asl	$5F17
	asl	$1C0A
	ora	$0A20, x
	.byte	$1B
	ora	$FC52
	bvc	L9570
	clc
	asl	$0E, x
	.byte	$5F
	.byte	$0B
	asl	$5F22, x
	asl	$22, x
	.byte	$5F
	.byte	$1B
	asl	a
	ora	$1C12
	ora	($0E), y
	.byte	$1C
	jmp	$375F

	ora	($0E), y
	.byte	$22
	.byte	$5F
	asl	a
	.byte	$1B
	asl	$0F5F
	.byte	$1B
	asl	$111C
	.byte	$5F
	asl	a
	.byte	$17
L9570:	ora	$0C5F
	ora	($0E), y
	asl	a
	ora	$FD47, y
	and	$1E
	.byte	$22
	.byte	$5F
	ora	$2211, x
	.byte	$5F
	.byte	$1B
	asl	a
	ora	$1C12
	ora	($0E), y
	.byte	$1C
	.byte	$5F
	ora	$0D18, x
	asl	a
	.byte	$22
	jmp	$FC40

	bvc	L95CB
	clc
	.byte	$5F
	ora	$0E, x
	asl	a
	.byte	$1B
	.byte	$17
	.byte	$5F
	ora	($18), y
	jsr	$195F
	.byte	$1B
	clc
	clc
	.byte	$0F
	.byte	$5F
	asl	$0A, x
	.byte	$22
	.byte	$5F
	.byte	$0B
	asl	$185F
	.byte	$0B
	ora	$120A, x
	.byte	$17
	asl	$5F0D
	ora	$A11, x
	ora	$1D5F, x
	ora	($22), y
	.byte	$5F
	asl	a
	.byte	$17
	.byte	$0C
	asl	$1D1C
	clc
	.byte	$1B
	.byte	$5F
	jsr	$1C0A
L95CB:	.byte	$5F
	ora	$E11, x
	.byte	$5F
	bpl	L95ED
	asl	$1D0A
	.byte	$5F
	plp
	.byte	$1B
	ora	$121B
	.byte	$0C
	.byte	$14
	pha
	.byte	$5F
	.byte	$1C
	asl	$5F0E
	asl	a
	.byte	$5F
	asl	$0A, x
	.byte	$17
	.byte	$5F
	.byte	$12
	.byte	$17
	.byte	$5F
	.byte	$1D
L95ED:	ora	($12), y
	.byte	$1C
	.byte	$5F
	.byte	$1F
	asl	$221B
	.byte	$5F
	ora	$2018, x
	.byte	$17
	.byte	$52
	.byte	$FC
	bvc	L9652
	.byte	$37
	.byte	$12
	.byte	$1C
	.byte	$5F
	.byte	$1C
	asl	a
	.byte	$12
	ora	$1D5F
	ora	($0A), y
	ora	$285F, x
	.byte	$1B
	ora	$121B
	.byte	$0C
	.byte	$14
	.byte	$53
	.byte	$1C
	.byte	$5F
	.byte	$1C
	jsr	$1B18
	ora	$0C5F
	clc
	asl	$0D15, x
	.byte	$5F
	.byte	$0C
	ora	$0E, x
	asl	a
	.byte	$1F
	asl	$1C5F
	ora	$E0E, x
	ora	$52, x
	.byte	$FC
	bvc	L966C
	asl	$0C15
	clc
	asl	$0E, x
	.byte	$5F
	ora	$5F18, x
	rol	$0A
	.byte	$17
	ora	$1215, x
	.byte	$17
	pha
	.byte	$5F
	ora	$E11, x
	.byte	$5F
	.byte	$0C
	asl	a
	.byte	$1C
	ora	$0E15, x
	.byte	$5F
	.byte	$1D
	clc
L9652:	jsr	$5217
	.byte	$FC
	bvc	L9692
	ora	($0A), y
	ora	$1C5F, x
	ora	($0A), y
	ora	$15, x
	.byte	$5F
	bit	$105F
	asl	$5F1D
	.byte	$0F
	clc
	.byte	$1B
	.byte	$5F
L966C:	ora	$2211, x
	.byte	$5F
	ora	$1712
	.byte	$17
	asl	$4B1B
	rti

	.byte	$FC
	bvc	L96A7
	.byte	$5F
	.byte	$14
	.byte	$17
	clc
	jsr	$175F
	clc
	ora	$1211, x
	.byte	$17
	bpl	L96DB
	.byte	$FC
	bvc	L96B8
	.byte	$53
	asl	$5F, x
	and	($0E), y
	.byte	$1C
L9692:	ora	$1B0E, x
	.byte	$47
	sbc	$0E2B, x
	.byte	$22
	pha
	.byte	$5F
	jsr	$E11
	.byte	$1B
	asl	$A5F
	asl	$5F, x
	.byte	$2C
	.byte	$4B
L96A7:	.byte	$5F
	and	($18), y
	pha
	.byte	$5F
	ora	$1718
	.byte	$53
	ora	$1D5F, x
	asl	$1515
	.byte	$5F
	.byte	$16
L96B8:	asl	$404C
	.byte	$FC
	bvc	L96E8
	.byte	$1B
	asl	a
	.byte	$17
	ora	$0A0F
	ora	$E11, x
	.byte	$1B
	.byte	$5F
	asl	$0E1C, x
	ora	$1D5F
	clc
	.byte	$5F
	.byte	$1C
	asl	a
	.byte	$22
	.byte	$5F
	ora	$A11, x
	ora	$115F, x
L96DB:	.byte	$12
	.byte	$1C
	.byte	$5F
	.byte	$0F
	.byte	$1B
	.byte	$12
	asl	$0D17
	pha
	.byte	$5F
	.byte	$3A
	.byte	$22
L96E8:	.byte	$17
	.byte	$17
	pha
	.byte	$5F
	ora	($0A), y
	ora	$0B5F
	asl	$121B, x
	asl	$5F0D
	.byte	$1C
	clc
	asl	$0E, x
	ora	$1211, x
	.byte	$17
	bpl	L9760
	clc
	.byte	$0F
	.byte	$5F
	bpl	L9721
	asl	$1D0A
	.byte	$5F
	.byte	$1F
	asl	a
	ora	$1E, x
	asl	$A5F
	ora	$1D5F, x
	ora	($0E), y
	.byte	$5F
	.byte	$0F
	clc
	clc
	ora	$185F, x
	.byte	$0F
	.byte	$5F
	asl	a
	.byte	$5F
L9721:	ora	$0E1B, x
	asl	$0B5F
	asl	$1211
	.byte	$17
	ora	$115F
	.byte	$12
	.byte	$1C
	.byte	$5F
	.byte	$1C
	ora	($18), y
	ora	$FC52, y
	bvc	L9765
	ora	$125F, x
	.byte	$1C
	.byte	$5F
	.byte	$1C
	asl	a
	.byte	$12
	ora	$1D5F
	ora	($0A), y
	ora	$165F, x
	asl	a
	.byte	$17
	.byte	$22
	.byte	$5F
	ora	($0A), y
	.byte	$1F
	asl	$115F
	asl	$0D15
	.byte	$5F
	plp
	.byte	$1B
	ora	$121B
	.byte	$0C
	.byte	$14
	.byte	$53
	.byte	$1C
L9760:	.byte	$5F
	asl	a
	.byte	$1B
	asl	$18, x
L9765:	.byte	$1B
	.byte	$52
	.byte	$FB
	sbc	$3750, x
	ora	($0E), y
	.byte	$5F
	ora	$0A, x
	.byte	$1C
	ora	$1D5F, x
	clc
	.byte	$5F
	ora	($0A), y
	.byte	$1F
	asl	$125F
	ora	$205F, x
	asl	a
	.byte	$1C
	.byte	$5F
	asl	a
	.byte	$5F
	.byte	$0F
	asl	$1515
	clc
	jsr	$175F
	asl	a
	asl	$0E, x
	ora	$3A5F
	.byte	$22
	.byte	$17
	.byte	$17
	.byte	$52
	.byte	$FC
	bvc	L97C9
	.byte	$22
	.byte	$5F
	rol	a
	.byte	$1B
	asl	a
	.byte	$17
	ora	$0A0F
	ora	$E11, x
	.byte	$1B
	.byte	$5F
	.byte	$3A
	.byte	$22
	.byte	$17
	.byte	$17
	.byte	$5F
	clc
	.byte	$17
	.byte	$0C
	asl	$115F
	asl	a
	ora	$A5F
	.byte	$5F
	.byte	$1C
	ora	($18), y
	ora	$185F, y
	.byte	$17
	.byte	$5F
	ora	$E11, x
	.byte	$5F
	asl	$1C0A
	ora	$1C5F, x
L97C9:	.byte	$12
	ora	$5F0E
	clc
	.byte	$0F
	.byte	$5F
	.byte	$2B
	asl	a
	asl	$1C14, x
	.byte	$17
	asl	$1C1C
	.byte	$52
	.byte	$FC
	bvc	L9817
	asl	$0C15
	clc
	asl	$0E, x
	jmp	$FC40

	bvc	L9822
	ora	($18), y
	.byte	$5F
	asl	a
	.byte	$1B
	ora	$1D5F, x
	ora	($18), y
	asl	$FD4B, x
	.byte	$2F
	asl	$1F0A
	asl	$A5F
	ora	$185F, x
	.byte	$17
	.byte	$0C
	asl	$185F
	.byte	$1B
	.byte	$5F
	bit	$205F
	.byte	$12
	ora	$15, x
	.byte	$5F
	.byte	$0C
	asl	a
	ora	$15, x
	.byte	$5F
	asl	$22, x
	.byte	$5F
	.byte	$0F
	.byte	$1B
L9817:	.byte	$12
	asl	$0D17
	.byte	$1C
	.byte	$52
	.byte	$FC
	bvc	L984C
	.byte	$5F
	asl	a
L9822:	asl	$5F, x
	.byte	$32
	.byte	$1B
	jsr	$C12
	.byte	$14
	pha
	.byte	$5F
	asl	a
	.byte	$17
	ora	$2C5F
	.byte	$5F
	asl	a
	asl	$5F, x
	jsr	$120A
	ora	$1712, x
	bpl	L989C
	.byte	$0F
	clc
	.byte	$1B
	.byte	$5F
	asl	$22, x
	.byte	$5F
	bpl	L9858
	.byte	$1B
	ora	$5F, x
	.byte	$0F
	.byte	$1B
	.byte	$12
L984C:	asl	$0D17
	.byte	$52
	.byte	$FC
	bvc	L988A
	ora	($0E), y
	.byte	$5F
	.byte	$1C
	.byte	$0C
L9858:	asl	a
	ora	$0E, x
	.byte	$1C
	.byte	$5F
	clc
	.byte	$0F
	.byte	$5F
	ora	$E11, x
	.byte	$5F
	.byte	$27
	.byte	$1B
	asl	a
	bpl	L9881
	.byte	$17
	ora	$18, x
	.byte	$1B
	ora	$A5F
	.byte	$1B
	asl	$A5F
	.byte	$1C
	.byte	$5F
	ora	($0A), y
	.byte	$1B
	ora	$A5F
	.byte	$1C
	.byte	$5F
	.byte	$1C
	.byte	$1D
	.byte	$0E
L9881:	asl	$5215
	.byte	$FC
	bvc	L98B9
	.byte	$1F
	.byte	$0E
	.byte	$1B
L988A:	.byte	$5F
	ora	$E11, x
	.byte	$5F
	jsr	$1C0E
	ora	$1B0E, x
	.byte	$17
	.byte	$5F
	ora	$1B0A, y
	.byte	$1D
	.byte	$5F
L989C:	clc
	.byte	$0F
	.byte	$5F
	ora	$1211, x
	.byte	$1C
	.byte	$5F
	.byte	$12
	.byte	$1C
	ora	$0A, x
	.byte	$17
	ora	$285F
	.byte	$1B
	ora	$121B
	.byte	$0C
	.byte	$14
	.byte	$5F
	.byte	$0C
	.byte	$1B
	asl	$1D0A
	.byte	$0E
L98B9:	ora	$A5F
	.byte	$5F
	.byte	$1B
	asl	a
	.byte	$12
	.byte	$17
	.byte	$0B
	clc
	jsr	$FB52
	sbc	$5450, x
	.byte	$37
	.byte	$12
	.byte	$1C
	.byte	$5F
	asl	a
	ora	$1C, x
	clc
	.byte	$5F
	.byte	$1C
	asl	a
	.byte	$12
	ora	$1D5F
	ora	($0A), y
	ora	$115F, x
	asl	$0E5F
	.byte	$17
	ora	$1B0E, x
	asl	$5F0D
	ora	$E11, x
	.byte	$5F
	ora	$1B0A
	.byte	$14
	.byte	$17
	asl	$1C1C
	.byte	$5F
	.byte	$0F
	.byte	$1B
	clc
	asl	$5F, x
	asl	a
	.byte	$5F
	ora	($12), y
	ora	$0E0D
	.byte	$17
	.byte	$5F
	asl	$1D17
	.byte	$1B
	asl	a
	.byte	$17
	.byte	$0C
	asl	$125F
	.byte	$17
	.byte	$5F
	ora	$E11, x
	.byte	$5F
	.byte	$1B
	clc
	clc
	asl	$5F, x
	clc
	.byte	$0F
	.byte	$5F
	ora	$E11, x
	.byte	$5F
	.byte	$27
	.byte	$1B
	asl	a
	bpl	L993B
	.byte	$17
	ora	$18, x
	.byte	$1B
	ora	$FC52
	bvc	L9963
	ora	($18), y
	asl	$1C5F, x
	ora	($0A), y
	ora	$1D, x
	.byte	$5F
	.byte	$0F
	.byte	$12
	.byte	$17
	.byte	$0D
	.byte	$5F
L993B:	ora	$E11, x
	.byte	$5F
	rol	$1D, x
	clc
	.byte	$17
	asl	$5F1C
	clc
	.byte	$0F
	.byte	$5F
	rol	$1E, x
	.byte	$17
	ora	$12, x
	bpl	L9961
	ora	$125F, x
	.byte	$17
	.byte	$5F
	.byte	$37
	asl	a
	.byte	$17
	ora	$100E, x
	asl	$5F15
	rol	$0A
	.byte	$1C
L9961:	.byte	$1D
	.byte	$15
L9963:	asl	$FD48
	.byte	$12
	.byte	$0F
	.byte	$5F
	ora	$1811, x
	asl	$115F, x
	asl	a
	.byte	$1C
	.byte	$5F
	.byte	$17
	clc
	ora	$0F5F, x
	clc
	asl	$0D17, x
	.byte	$5F
	ora	$E11, x
	asl	$5F, x
	.byte	$22
	asl	$521D
	.byte	$FC
	bvc	L99C2
	asl	$0C15
	clc
	asl	$0E, x
	.byte	$5F
	ora	$5F18, x
	ora	$E11, x
	.byte	$5F
	ora	$2018, x
	.byte	$17
	.byte	$5F
	clc
	.byte	$0F
	.byte	$5F
	and	$12, x
	asl	$1E, x
	ora	$0D, x
	asl	a
	.byte	$1B
	.byte	$52
	.byte	$FC
	bvc	L99DB
	clc
	pha
	.byte	$5F
	bit	$115F
	asl	a
	.byte	$1F
	asl	$175F
	clc
	.byte	$5F
	ora	$1618, x
	asl	a
	ora	$0E18, x
	.byte	$1C
	.byte	$47
	.byte	$FD
	.byte	$2C
L99C2:	.byte	$5F
	ora	($0A), y
	.byte	$1F
	asl	$175F
	clc
	.byte	$5F
	ora	$1618, x
	asl	a
	ora	$0E18, x
	.byte	$1C
	.byte	$5F
	ora	$0D18, x
	asl	a
	.byte	$22
	.byte	$52
	.byte	$FC
L99DB:	bvc	L9A19
	clc
	asl	$A5F, x
	.byte	$1B
	asl	$F85F
	.byte	$4B
	sbc	$1D2C, x
	.byte	$5F
	ora	($0A), y
	.byte	$1C
	.byte	$5F
	.byte	$0B
	asl	$170E
	.byte	$5F
	ora	$18, x
	.byte	$17
	bpl	L9A57
	.byte	$1C
	.byte	$12
	.byte	$17
	.byte	$0C
	asl	$155F
	asl	a
	.byte	$1C
	ora	$205F, x
	asl	$165F
	asl	$521D
	.byte	$FC
	bvc	L9A37
	clc
	clc
	ora	$D5F
	asl	a
	.byte	$22
	pha
	bit	$A5F
	.byte	$16
L9A19:	.byte	$5F
	.byte	$2B
	clc
	jsr	$1B0A
	ora	$5F47
	and	#$18
	asl	$5F1B, x
	.byte	$1C
	ora	$190E, x
	.byte	$1C
	.byte	$5F
	.byte	$1C
	clc
	asl	$111D, x
	.byte	$5F
	clc
	.byte	$0F
	.byte	$5F
	.byte	$1D
L9A37:	ora	($0E), y
	.byte	$5F
	.byte	$0B
	asl	a
	ora	$5F11, x
	.byte	$12
	.byte	$17
	.byte	$5F
	rol	$1518
	.byte	$5F
	ora	$1811, x
	asl	$1C5F, x
	ora	($0A), y
	ora	$1D, x
	.byte	$5F
	.byte	$0F
	.byte	$12
	.byte	$17
	ora	$A5F
L9A57:	.byte	$5F
	asl	$0A, x
	bpl	L9A6E
	.byte	$0C
	.byte	$5F
	.byte	$12
	ora	$160E, x
	.byte	$52
	.byte	$FC
	bvc	L9A8B
	asl	$180F
	.byte	$1B
	asl	$155F
	clc
L9A6E:	.byte	$17
	bpl	L9AD0
	ora	$E11, x
	.byte	$5F
	asl	$0E17
	asl	$22, x
	.byte	$5F
	jsr	$1512
	ora	$5F, x
	asl	a
	.byte	$1B
	.byte	$1B
	.byte	$12
	.byte	$1F
	asl	$FC52
	bvc	L9AB5
	.byte	$0E
L9A8B:	asl	$5F0D
	asl	$22, x
	.byte	$5F
	jsr	$1B0A
	.byte	$17
	.byte	$12
	.byte	$17
	bpl	L9AE5
	.byte	$5F
	.byte	$37
	.byte	$1B
	asl	a
	.byte	$1F
	asl	$5F15
	.byte	$17
	clc
	ora	$1D5F, x
	clc
	.byte	$5F
	ora	$E11, x
	.byte	$5F
	.byte	$1C
	clc
	asl	$111D, x
	.byte	$5F
	.byte	$0F
	clc
	.byte	$1B
L9AB5:	.byte	$5F
	ora	$E11, x
	.byte	$1B
	asl	$1D5F
	ora	($0E), y
	.byte	$5F
	asl	$18, x
	.byte	$17
	.byte	$1C
	ora	$1B0E, x
	.byte	$1C
	.byte	$5F
	asl	a
	.byte	$1B
	asl	$0F5F
	.byte	$12
	.byte	$0E
L9AD0:	.byte	$1B
	.byte	$0C
	asl	$A5F
	.byte	$17
	ora	$1D5F
	asl	$1B1B
	.byte	$12
	.byte	$0B
	ora	$0E, x
	.byte	$52
	.byte	$FC
	bvc	L9B10
	.byte	$17
L9AE5:	.byte	$5F
	ora	$1211, x
	.byte	$1C
	.byte	$5F
	jsr	$1B18
	ora	$0D, x
	.byte	$5F
	.byte	$12
	.byte	$1C
	.byte	$5F
	ora	$E11, x
	.byte	$1B
	asl	$A5F
	.byte	$17
	.byte	$22
	.byte	$5F
	.byte	$1C
	jsr	$1B18
	ora	$1D5F
	ora	($0A), y
	ora	$0C5F, x
	asl	a
	.byte	$17
	.byte	$5F
	ora	$0E12, y
L9B10:	.byte	$1B
	.byte	$0C
	asl	$1D5F
	ora	($0E), y
	.byte	$5F
	.byte	$1C
	.byte	$0C
	asl	a
	ora	$0E, x
	.byte	$1C
	.byte	$5F
	clc
	.byte	$0F
	.byte	$5F
	ora	$E11, x
	.byte	$5F
	.byte	$27
	.byte	$1B
	asl	a
	bpl	L9B43
	.byte	$17
	ora	$18, x
	.byte	$1B
	ora	$404B
	.byte	$FC
	bvc	L9B67
	.byte	$1B
	jsr	$C12
	.byte	$14
	.byte	$5F
	.byte	$12
	.byte	$1C
	.byte	$5F
	ora	$0A, x
	ora	$5F0E, x
L9B43:	asl	a
	bpl	L9B50
	.byte	$12
	.byte	$17
	.byte	$47
	.byte	$5F
	bit	$1653
	.byte	$5F
	.byte	$1C
	.byte	$1D
L9B50:	asl	a
	.byte	$1B
	.byte	$1F
	.byte	$12
	.byte	$17
	bpl	L9BA9
	.byte	$FC
	bvc	L9B8A
	asl	a
	.byte	$17
	.byte	$22
	.byte	$5F
	ora	($0A), y
	.byte	$1F
	asl	$0B5F
	asl	$170E
L9B67:	.byte	$5F
	ora	$E11, x
	.byte	$5F
	jsr	$1B0A
	.byte	$1B
	.byte	$12
	clc
	.byte	$1B
	.byte	$1C
	.byte	$5F
	jsr	$1811
	.byte	$5F
	ora	($0A), y
	.byte	$1F
	asl	$195F
	asl	$121B
	.byte	$1C
	ora	($0E), y
	ora	$185F
	.byte	$17
	.byte	$5F
L9B8A:	ora	$1211, x
	.byte	$1C
	.byte	$5F
	.byte	$1A
	asl	$1C0E, x
	ora	$FB52, x
	sbc	$2550, x
	asl	$5F1D, x
	.byte	$0F
	clc
	.byte	$1B
	.byte	$5F
	ora	$E11, x
	asl	$2C5F
	.byte	$5F
	.byte	$20
	.byte	$12
L9BA9:	.byte	$1C
	ora	($5F), y
	.byte	$1C
	asl	$0C0C, x
	asl	$1C1C
	pha
	.byte	$5F
	sed
	.byte	$52
	.byte	$FC
	bvc	L9BE5
	asl	a
	.byte	$1C
	ora	$1D5F, x
	ora	($18), y
	asl	$0F5F, x
	clc
	asl	$0D17, x
	.byte	$5F
	ora	$E11, x
	.byte	$5F
	.byte	$0F
	ora	$1E, x
	ora	$4B0E, x
	rti

	sbc	$50FC, x
	.byte	$2B
	asl	a
	.byte	$1C
	ora	$1D5F, x
	ora	($18), y
	asl	$0B5F, x
	asl	$170E
L9BE5:	.byte	$5F
	ora	$5F18, x
	ora	$E11, x
	.byte	$5F
	.byte	$1C
	clc
	asl	$111D, x
	asl	$171B
	.byte	$5F
	.byte	$12
	.byte	$1C
	ora	$0A, x
	.byte	$17
	ora	$404B
	sbc	$50FC, x
	.byte	$54
	.byte	$37
	.byte	$12
	.byte	$1C
	.byte	$5F
	.byte	$1C
	asl	a
	.byte	$12
	ora	$1D5F
	ora	($0A), y
	ora	$1D5F, x
	ora	($0E), y
	.byte	$5F
	.byte	$27
	.byte	$1B
	asl	a
	bpl	L9C31
	.byte	$17
	ora	$18, x
	.byte	$1B
	ora	$115F
	asl	a
	ora	$5F11, x
	.byte	$0C
	ora	$0A, x
	jsr	$5F1C
	ora	$A11, x
	ora	$0C5F, x
	asl	a
L9C31:	.byte	$17
	.byte	$5F
	.byte	$0C
	ora	$0E, x
	asl	a
	.byte	$1F
	asl	$125F
	.byte	$1B
	clc
	.byte	$17
	.byte	$5F
	asl	a
	.byte	$17
	ora	$0F5F
	.byte	$12
	asl	$221B
	.byte	$5F
	.byte	$0B
	.byte	$1B
	asl	$1D0A
	ora	($5F), y
	ora	$A11, x
	ora	$0C5F, x
	asl	a
	.byte	$17
	.byte	$5F
	asl	$0E, x
	ora	$1D, x
	.byte	$5F
	.byte	$1C
	ora	$1718, x
	asl	$FB52
	sbc	$2750, x
	clc
	.byte	$1C
	ora	$1D5F, x
	ora	($18), y
	asl	$1C5F, x
	ora	$1512, x
	ora	$5F, x
	jsr	$1C12
	ora	($5F), y
	ora	$5F18, x
	bpl	L9C99
	.byte	$5F
	clc
	.byte	$17
	.byte	$4B
	rti

	sbc	$50FC, x
	.byte	$37
	ora	($12), y
	.byte	$1C
	.byte	$5F
	.byte	$12
	.byte	$1C
	.byte	$5F
	asl	a
	.byte	$5F
	asl	$0A, x
	bpl	L9CA9
	.byte	$0C
	.byte	$5F
L9C99:	ora	$0A15, y
	.byte	$0C
	asl	$FD47
	.byte	$2B
	asl	a
	.byte	$1C
	ora	$1D5F, x
	ora	($18), y
	.byte	$1E
L9CA9:	.byte	$5F
	.byte	$0F
	clc
	asl	$0D17, x
	.byte	$5F
	asl	a
	.byte	$5F
	asl	$0A, x
	bpl	L9CC8
	.byte	$0C
	.byte	$5F
	ora	$160E, x
	ora	$0E15, y
	.byte	$4B
	rti

	sbc	$50FC, x
	.byte	$3A
	ora	($0E), y
	.byte	$17
	.byte	$5F
L9CC8:	asl	$1D17
	asl	$121B
	.byte	$17
	bpl	L9D30
	ora	$E11, x
	.byte	$5F
	.byte	$0C
	asl	a
	.byte	$1F
	asl	$5F48
	ora	$140A, x
	asl	$205F
	.byte	$12
	ora	$5F11, x
	ora	$E11, x
	asl	$A5F
	.byte	$5F
	ora	$1B18, x
	.byte	$0C
	ora	($52), y
	.byte	$FC
	bvc	L9D1F
	clc
	.byte	$5F
	ora	$5F18, x
	ora	$E11, x
	.byte	$5F
	ora	$2018, x
	.byte	$17
	.byte	$5F
	clc
	.byte	$0F
	.byte	$5F
	rol	$0A
	.byte	$17
	ora	$1215, x
	.byte	$17
	.byte	$52
	.byte	$FC
	bvc	L9D3D
	.byte	$5F
	ora	($0A), y
	.byte	$1F
	asl	$115F
	asl	$1B0A
	ora	$1D5F
	.byte	$11
L9D1F:	asl	a
	ora	$195F, x
	clc
	jsr	$1B0E
	.byte	$0F
	asl	$5F15, x
	asl	$0E17
	asl	$12, x
L9D30:	asl	$5F1C
	ora	$12, x
	.byte	$1F
	asl	$1D5F
	ora	($0E), y
	.byte	$1B
	.byte	$0E
L9D3D:	.byte	$52
	.byte	$FC
	bvc	L9D78
	ora	($18), y
	asl	$A5F, x
	.byte	$1B
	ora	$1D5F, x
	.byte	$1B
	asl	$2215, x
	.byte	$5F
	.byte	$0B
	.byte	$1B
	asl	a
	.byte	$1F
	asl	$FC52
	bvc	L9D84
	.byte	$17
	.byte	$5F
	ora	$1211, x
	.byte	$1C
	.byte	$5F
	ora	$160E, x
	ora	$0E15, y
	.byte	$5F
	ora	$5F18
	ora	$E11, x
	.byte	$5F
	.byte	$1C
	asl	$5F17, x
	asl	a
	.byte	$17
	ora	$1B5F
	asl	a
	.byte	$12
L9D78:	.byte	$17
	.byte	$5F
	asl	$0E, x
	asl	$521D
	.byte	$FC
	.byte	$FC
	bvc	L9DAE
	clc
L9D84:	jsr	$1B0A
	ora	$115F
	asl	a
	ora	$125F
	ora	$5F48, x
	.byte	$0B
	asl	$5F1D, x
	ora	($0E), y
	.byte	$5F
	jsr	$170E
	ora	$1D5F, x
	clc
	.byte	$5F
	and	$12, x
	asl	$1E, x
	ora	$0D, x
	asl	a
	.byte	$1B
	.byte	$5F
	asl	a
	.byte	$17
	ora	$175F
L9DAE:	asl	$0E1F
	.byte	$1B
	.byte	$5F
	.byte	$1B
	asl	$1E1D
	.byte	$1B
	.byte	$17
	asl	$520D
	.byte	$FC
	bvc	L9DF6
	clc
	.byte	$5F
	ora	$E11, x
	.byte	$5F
	.byte	$1C
	clc
	asl	$111D, x
	pha
	.byte	$5F
	bit	$0B5F
	asl	$1215
	asl	$0E1F
	pha
	.byte	$5F
	ora	$E11, x
	.byte	$1B
	asl	$125F
	.byte	$1C
	.byte	$5F
	asl	a
	.byte	$5F
	ora	$2018, x
	.byte	$17
	.byte	$5F
	.byte	$0C
	asl	a
	ora	$15, x
	asl	$5F0D
	and	$12, x
	asl	$1E, x
	ora	$0D, x
	asl	a
	.byte	$1B
L9DF6:	.byte	$52
	.byte	$FC
	bvc	L9E31
	ora	($0A), y
	ora	$125F, x
	.byte	$1C
	.byte	$5F
	bpl	L9E1B
	clc
	ora	$FD52
	bvc	L9E3A
	clc
	.byte	$5F
	clc
	.byte	$17
	asl	$205F
	.byte	$12
	ora	$15, x
	.byte	$5F
	.byte	$1C
	asl	a
	.byte	$22
	.byte	$5F
	ora	$1811, x
L9E1B:	asl	$A5F, x
	.byte	$1B
	ora	$A5F, x
	.byte	$0F
	.byte	$1B
	asl	a
	.byte	$12
	ora	$FC52
	bvc	L9E55
	clc
	.byte	$5F
	ora	$5F18, x
	.byte	$1D
L9E31:	ora	($0E), y
	.byte	$5F
	.byte	$1C
	clc
	asl	$111D, x
	.byte	$52
L9E3A:	.byte	$FC
	bvc	L9E77
	ora	($0E), y
	.byte	$1B
	asl	$185F
	ora	($5F), y
	jsr	$E11
	.byte	$1B
	asl	$0C5F
	asl	a
	.byte	$17
	.byte	$5F
	bit	$0F5F
	.byte	$12
	.byte	$17
	.byte	$0D
L9E55:	.byte	$5F
	.byte	$33
	.byte	$1B
	.byte	$12
	.byte	$17
	.byte	$0C
	asl	$1C1C
	.byte	$5F
	rol	a
	jsr	$E0A
	ora	$12, x
	.byte	$17
	.byte	$4B
	rti

	.byte	$FC
	bvc	L9EA2
	ora	($0A), y
	.byte	$17
	.byte	$14
	.byte	$5F
	.byte	$22
	clc
	asl	$0F5F, x
	clc
	.byte	$1B
L9E77:	.byte	$5F
	.byte	$1C
	asl	a
	.byte	$1F
	.byte	$12
	.byte	$17
	bpl	L9EDE
	ora	$E11, x
	.byte	$5F
	.byte	$33
	.byte	$1B
	.byte	$12
	.byte	$17
	.byte	$0C
	asl	$1C1C
	.byte	$52
	.byte	$FC
	bvc	L9EC1
	ora	($48), y
	.byte	$5F
	asl	$22, x
	.byte	$5F
	ora	$A0E
	.byte	$1B
	asl	$1D1C
	.byte	$5F
	rol	a
	jsr	$E0A
	.byte	$15
L9EA2:	.byte	$12
	.byte	$17
	jmp	$FD40

	bvc	L9ED5
	.byte	$5F
	ora	($0A), y
	ora	$5F0E, x
	ora	$E11, x
	asl	$5F48
	sed
	.byte	$52
	.byte	$FC
	bvc	L9EF1
	asl	$1515
	.byte	$5F
	rol	$1712
L9EC1:	bpl	L9F22
	.byte	$2F
	clc
	.byte	$1B
	.byte	$12
	.byte	$14
	.byte	$5F
	ora	$A11, x
	ora	$1D5F, x
	ora	($0E), y
	.byte	$5F
	.byte	$1C
	.byte	$0E
	asl	a
L9ED5:	.byte	$1B
	.byte	$0C
	ora	($5F), y
	.byte	$0F
	clc
	.byte	$1B
	.byte	$5F
	.byte	$11
L9EDE:	.byte	$12
	.byte	$1C
	.byte	$5F
	ora	$1E0A
	bpl	L9EF7
	ora	$1B0E, x
	.byte	$5F
	ora	($0A), y
	ora	$5F11, x
	.byte	$0F
	asl	a
L9EF1:	.byte	$12
	ora	$0E, x
	ora	$FB52
L9EF7:	sbc	$2C50, x
	.byte	$5F
	asl	a
	asl	$5F, x
	asl	a
	ora	$16, x
	clc
	.byte	$1C
	ora	$105F, x
	clc
	.byte	$17
	asl	$4545
	rti

	.byte	$FC
	bvc	L9F49
	ora	($18), y
	.byte	$5F
	ora	$1E18, x
	.byte	$0C
	ora	($0E), y
	.byte	$1C
	.byte	$5F
	asl	$0E, x
	.byte	$4B
	rti

	sbc	$2C50, x
	.byte	$5F
L9F22:	.byte	$1C
	asl	$5F0E
	.byte	$17
	clc
	ora	$1211, x
	.byte	$17
	bpl	L9F76
	.byte	$5F
	.byte	$17
	clc
	.byte	$1B
	.byte	$5F
	.byte	$0C
	asl	a
	.byte	$17
	.byte	$5F
	bit	$115F
	asl	$1B0A
	.byte	$52
	.byte	$FC
	bvc	L9F68
	clc
	.byte	$1C
	ora	$1D5F, x
	ora	($18), y
	.byte	$1E
L9F49:	.byte	$5F
	.byte	$14
	.byte	$17
	clc
	jsr	$A5F
	.byte	$0B
	clc
	asl	$5F1D, x
	.byte	$33
	.byte	$1B
	.byte	$12
	.byte	$17
	.byte	$0C
	asl	$1C1C
	.byte	$5F
	rol	a
	jsr	$E0A
	ora	$12, x
	.byte	$17
	.byte	$4B
	rti

	.byte	$FD
L9F68:	.byte	$FC
	bvc	L9F96
	asl	a
	ora	$0F, x
	.byte	$5F
	asl	a
	.byte	$5F
	.byte	$22
	asl	$1B0A
	.byte	$5F
L9F76:	.byte	$17
	clc
	jsr	$115F
	asl	a
	ora	$5F11, x
	ora	$1C0A, y
	.byte	$1C
	asl	$5F0D
	.byte	$1C
	.byte	$12
	.byte	$17
	.byte	$0C
	asl	$1D5F
	ora	($0E), y
	.byte	$5F
	.byte	$33
	.byte	$1B
	.byte	$12
	.byte	$17
	.byte	$0C
	.byte	$0E
L9F96:	.byte	$1C
	.byte	$1C
	.byte	$5F
	jsr	$1C0A
	.byte	$5F
	.byte	$14
	.byte	$12
	ora	$0A17
	ora	$0E19, y
	ora	$0B5F
	.byte	$22
	.byte	$5F
	ora	$E11, x
	.byte	$5F
	asl	$0E17
	asl	$22, x
	.byte	$52
	.byte	$FB
	sbc	$3150, x
	asl	$0E1F
	.byte	$1B
	.byte	$5F
	ora	$0E18
	.byte	$1C
	.byte	$5F
	ora	$E11, x
	.byte	$5F
	rol	$1712
	bpl	LA02A
	.byte	$1C
	ora	$A0E, y
	.byte	$14
	.byte	$5F
	clc
	.byte	$0F
	.byte	$5F
	.byte	$12
	ora	$5F48, x
	.byte	$0B
	asl	$5F1D, x
	ora	($0E), y
	.byte	$5F
	asl	$1E, x
	.byte	$1C
	ora	$0B5F, x
	asl	$1C5F
	asl	$0F0F, x
	asl	$121B
	.byte	$17
	bpl	LA050
	.byte	$16
L9FF2:	asl	$110C, x
	.byte	$52
	.byte	$FB
	.byte	$FC
	bvc	L9FF2
	pha
	.byte	$5F
	ora	$0E15, y
	asl	a
	.byte	$1C
	asl	$1C5F
	asl	a
	.byte	$1F
	asl	$1D5F
	ora	($0E), y
	.byte	$5F
	.byte	$33
	.byte	$1B
	.byte	$12
	.byte	$17
	.byte	$0C
	asl	$1C1C
	.byte	$52
	.byte	$FC
	bvc	LA04A
	ora	($48), y
	.byte	$5F
	.byte	$0B
	.byte	$1B
	asl	a
	.byte	$1F
	asl	$F85F
	.byte	$52
	.byte	$FC
	bvc	LA052
	.byte	$5F
	ora	($0A), y
	.byte	$1F
LA02A:	asl	$0B5F
	asl	$170E
	.byte	$5F
	jsr	$120A
	ora	$1712, x
	bpl	LA098
	ora	$18, x
	.byte	$17
	bpl	LA09D
	.byte	$0F
	clc
	.byte	$1B
	.byte	$5F
	clc
	.byte	$17
	asl	$1C5F
	asl	$110C, x
LA04A:	.byte	$5F
	asl	a
	.byte	$1C
	.byte	$5F
	.byte	$1D
	.byte	$11
LA050:	.byte	$0E
	.byte	$0E
LA052:	.byte	$52
	.byte	$FB
	.byte	$FC
	bvc	LA08E
	ora	($18), y
	asl	$115F, x
	asl	a
	.byte	$1C
	ora	$175F, x
	clc
	.byte	$5F
	.byte	$0B
	asl	$121C, x
	.byte	$17
	asl	$1C1C
	.byte	$5F
	ora	($0E), y
	.byte	$1B
	asl	$FD47
	rol	a
	clc
	.byte	$5F
	asl	a
	jsr	$220A
	.byte	$52
	.byte	$FC
	bvc	LA0A9
	.byte	$0F
	.byte	$5F
	ora	$1811, x
	asl	$A5F, x
	.byte	$1B
	ora	$0C5F, x
	asl	$1C1B, x
	.byte	$0E
	.byte	$0D
LA08E:	pha
	.byte	$5F
	.byte	$0C
	clc
	asl	$0E, x
	.byte	$5F
	asl	a
	bpl	LA0A2
LA098:	.byte	$12
	.byte	$17
	.byte	$52
	.byte	$FC
	.byte	$50
LA09D:	bit	$205F
	.byte	$12
	.byte	$15
LA0A2:	ora	$5F, x
	.byte	$0F
	.byte	$1B
	asl	$5F0E
LA0A9:	ora	$E11, x
	asl	$0F5F
	.byte	$1B
	clc
	asl	$5F, x
	ora	$2211, x
	.byte	$5F
	.byte	$0C
	asl	$1C1B, x
	asl	$FB52
	.byte	$FC
	bvc	LA0F2
	clc
	jsr	$5F48
	bpl	LA0DF
	.byte	$52
	.byte	$FC
	bvc	LA102
	ora	($18), y
	asl	$1110, x
	.byte	$5F
	ora	$1811, x
	asl	$A5F, x
	.byte	$1B
	ora	$A5F, x
	.byte	$1C
	.byte	$5F
	.byte	$0B
	.byte	$1B
LA0DF:	asl	a
	.byte	$1F
	asl	$A5F
	.byte	$1C
	.byte	$5F
	ora	$2211, x
	.byte	$5F
	asl	a
	.byte	$17
	.byte	$0C
	asl	$1D1C
	clc
	.byte	$1B
LA0F2:	pha
	.byte	$5F
	sed
	pha
	.byte	$5F
	ora	$1811, x
	asl	$0C5F, x
	asl	a
	.byte	$17
	.byte	$17
	clc
	.byte	$1D
LA102:	.byte	$5F
	ora	$0F0E
	asl	$1D0A
	.byte	$5F
	ora	$E11, x
	.byte	$5F
	bpl	LA12B
	asl	$1D0A
	.byte	$5F
	.byte	$27
	.byte	$1B
	asl	a
	bpl	LA131
	.byte	$17
	ora	$18, x
	.byte	$1B
	ora	$205F
	.byte	$12
	ora	$5F11, x
	.byte	$1C
	asl	$110C, x
	.byte	$5F
	.byte	$20
	.byte	$0E
LA12B:	asl	a
	ora	$1718, y
	.byte	$1C
	.byte	$52
LA131:	.byte	$FB
	sbc	$3750, x
	ora	($18), y
	asl	$1C5F, x
	ora	($18), y
	asl	$0D15, x
	.byte	$1C
	ora	$0C5F, x
	clc
	asl	$0E, x
	.byte	$5F
	ora	($0E), y
	.byte	$1B
	asl	$A5F
	bpl	LA159
	.byte	$12
	.byte	$17
	.byte	$52
	.byte	$FC
	bvc	LA17E
	.byte	$12
	.byte	$17
	asl	a
	.byte	$15
LA159:	ora	$22, x
	.byte	$5F
	ora	$1811, x
	asl	$115F, x
	asl	a
	.byte	$1C
	ora	$185F, x
	.byte	$0B
	ora	$120A, x
	.byte	$17
	asl	$5F0D
	.byte	$12
	ora	$5F48, x
	sed
	.byte	$52
	.byte	$FC
	bvc	LA1A4
	.byte	$1C
	.byte	$5F
	ora	$A11, x
	.byte	$1D
LA17E:	.byte	$5F
	asl	a
	.byte	$5F
	jsr	$D0E
	ora	$1712
	bpl	LA1E8
	.byte	$1B
	.byte	$12
	.byte	$17
	bpl	LA1D9
	rti

	sbc	$3750, x
	ora	($18), y
	asl	$1C5F, x
	asl	$160E
	.byte	$1C
	.byte	$5F
	ora	$1818, x
	.byte	$5F
	.byte	$22
	clc
	.byte	$1E
	.byte	$17
LA1A4:	bpl	LA205
	ora	$5F18, x
	.byte	$0B
	asl	$165F
	asl	a
	.byte	$1B
	.byte	$1B
	.byte	$12
	asl	$520D
	.byte	$FC
	bvc	LA1DB
	ora	$15, x
	.byte	$5F
	ora	$1E1B, x
	asl	$205F
	asl	a
	.byte	$1B
	.byte	$1B
	.byte	$12
	clc
	.byte	$1B
	.byte	$1C
	.byte	$5F
	jsr	$A0E
	.byte	$1B
	.byte	$5F
	asl	a
	.byte	$5F
LA1CF:	.byte	$1B
	.byte	$12
	.byte	$17
	bpl	LA226
	.byte	$FC
	bvc	LA1CF
	.byte	$53
	.byte	$1C
LA1D9:	.byte	$5F
	.byte	$0C
LA1DB:	clc
	asl	$12, x
	.byte	$17
	bpl	LA240
	jsr	$1C0A
	.byte	$5F
	.byte	$0F
	clc
	.byte	$1B
LA1E8:	asl	$181D
	ora	$0D, x
	.byte	$5F
	.byte	$0B
	.byte	$22
	.byte	$5F
	ora	$0E, x
	bpl	LA203
	.byte	$17
	ora	$FD47
	bmi	LA205
	.byte	$22
	.byte	$5F
	ora	$E11, x
	.byte	$5F
	ora	$12, x
LA203:	bpl	LA216
LA205:	ora	$1C5F, x
	ora	($12), y
	.byte	$17
	asl	$1E5F
	ora	$1718, y
	.byte	$5F
	ora	$1211, x
	.byte	$1C
LA216:	.byte	$5F
	.byte	$0B
	.byte	$1B
	asl	a
	.byte	$1F
	asl	$205F
	asl	a
	.byte	$1B
	.byte	$1B
	.byte	$12
	clc
	.byte	$1B
	.byte	$52
	.byte	$FC
LA226:	.byte	$FB
	bvc	LA260
	ora	($18), y
	asl	$165F, x
	asl	a
	.byte	$22
	.byte	$5F
	bpl	LA24B
	.byte	$5F
	asl	a
	.byte	$17
	ora	$1C5F
	asl	$1B0A
	.byte	$0C
	ora	($52), y
	.byte	$FC
LA240:	.byte	$FB
	bvc	LA26C
	.byte	$1B
	clc
	asl	$5F, x
	.byte	$37
	asl	a
	.byte	$17
	.byte	$1D
LA24B:	asl	$0E10
	ora	$5F, x
	rol	$0A
	.byte	$1C
	ora	$0E15, x
	.byte	$5F
	ora	$0A1B, x
	.byte	$1F
	asl	$5F15
	.byte	$07
	brk
LA260:	.byte	$5F
	ora	$0E, x
	asl	a
	bpl	LA284
	asl	$5F1C
	ora	$5F18, x
LA26C:	ora	$E11, x
	.byte	$5F
	.byte	$1C
	clc
	asl	$111D, x
	.byte	$5F
	asl	a
	.byte	$17
	ora	$045F
	brk
	.byte	$5F
	ora	$5F18, x
	ora	$E11, x
	.byte	$5F
LA284:	asl	$1C0A
	ora	$FC52, x
	.byte	$FB
	bvc	LA2B9
	ora	$1C53, x
	.byte	$5F
	asl	a
	.byte	$5F
	ora	$0E, x
	bpl	LA2A5
	.byte	$17
	ora	$FC52
	bvc	LA2D4
	ora	($22), y
	.byte	$5F
	.byte	$0B
	.byte	$1B
	asl	a
	.byte	$1F
	.byte	$0E
LA2A5:	.byte	$1B
	.byte	$22
	.byte	$5F
	asl	$1E, x
	.byte	$1C
	ora	$0B5F, x
	asl	$195F
	.byte	$1B
	clc
	.byte	$1F
	asl	$5217
	.byte	$FB
	.byte	$FD
LA2B9:	bvc	LA2F2
	ora	($1E), y
	.byte	$1C
	pha
	.byte	$5F
	bit	$195F
	.byte	$1B
	clc
	ora	$1C18, y
	asl	$A5F
	.byte	$5F
	ora	$1C0E, x
	ora	$FB52, x
	.byte	$FD
	.byte	$50
LA2D4:	.byte	$37
	ora	($0E), y
	.byte	$1B
	asl	$125F
	.byte	$1C
	.byte	$5F
	asl	a
	.byte	$5F
	rol	$12, x
	ora	$1F, x
	asl	$5F1B
	.byte	$2B
	asl	a
	.byte	$1B
	ora	$1D5F, y
	ora	($0A), y
	ora	$0B5F, x
	.byte	$0E
LA2F2:	.byte	$0C
	.byte	$14
	clc
	.byte	$17
	.byte	$1C
	.byte	$5F
	ora	$5F18, x
	ora	$E11, x
	.byte	$5F
	.byte	$0C
	.byte	$1B
	asl	$1D0A
	asl	$0E1B, x
	.byte	$1C
	.byte	$5F
	clc
	.byte	$0F
	.byte	$5F
	ora	$E11, x
	.byte	$5F
	.byte	$27
	.byte	$1B
	asl	a
	bpl	LA32D
	.byte	$17
	ora	$18, x
	.byte	$1B
	ora	$FB52
	sbc	$2550, x
	.byte	$1B
	.byte	$12
	.byte	$17
	bpl	LA383
	ora	$1211, x
	.byte	$1C
	.byte	$5F
	ora	$5F18, x
	.byte	$16
LA32D:	asl	$A5F
	.byte	$17
	ora	$2C5F
	.byte	$5F
	jsr	$1512
	ora	$5F, x
	.byte	$1B
	asl	$0A20
	.byte	$1B
	ora	$1D5F
	ora	($0E), y
	asl	$205F
	.byte	$12
	ora	$5F11, x
	ora	$E11, x
	.byte	$5F
	rol	$1D, x
	asl	a
	.byte	$0F
	.byte	$0F
	.byte	$5F
	clc
	.byte	$0F
	.byte	$5F
	and	$0A, x
	.byte	$12
	.byte	$17
	.byte	$52
	.byte	$FC
	bvc	LA397
	ora	($18), y
	asl	$115F, x
	asl	a
	.byte	$1C
	ora	$0B5F, x
	.byte	$1B
	clc
	asl	$1110, x
	ora	$1D5F, x
	ora	($0E), y
	.byte	$5F
	ora	($0A), y
	.byte	$1B
	ora	$5F47, y
	rol	a
	clc
	clc
	ora	$FB52
	.byte	$FC
	.byte	$50
LA383:	bit	$5F17
	ora	$2211, x
	.byte	$5F
	ora	$1C0A, x
	.byte	$14
	.byte	$5F
	ora	$1811, x
	asl	$115F, x
	asl	a
	.byte	$1C
LA397:	ora	$0F5F, x
	asl	a
	.byte	$12
	ora	$0E, x
	ora	$5F47
	bit	$15
	asl	a
	.byte	$1C
	pha
	.byte	$5F
	bit	$0F5F
	asl	$1B0A
	.byte	$5F
	ora	$1811, x
	asl	$A5F, x
	.byte	$1B
	ora	$175F, x
	clc
	ora	$1D5F, x
	ora	($0E), y
	.byte	$5F
	clc
	.byte	$17
	asl	$285F
	.byte	$1B
	ora	$121B
	.byte	$0C
	.byte	$14
	.byte	$5F
	ora	$0E1B, y
	ora	$C12
	ora	$D0E, x
	.byte	$5F
	jsr	$1E18
	ora	$0D, x
	.byte	$5F
	.byte	$1C
	asl	a
	.byte	$1F
	asl	$1E5F
	.byte	$1C
	.byte	$52
	.byte	$FB
	sbc	$2A50, x
	clc
	.byte	$5F
	.byte	$17
	clc
	jsr	$404C
	.byte	$FC
	bvc	LA422
	clc
	jsr	$1D5F
	ora	($0E), y
	.byte	$5F
	.byte	$1C
	asl	$5F17, x
	asl	a
	.byte	$17
	ora	$1B5F
	asl	a
	.byte	$12
	.byte	$17
	.byte	$5F
	.byte	$1C
	ora	($0A), y
	ora	$15, x
	.byte	$5F
	asl	$0E, x
	asl	$5F1D
	asl	a
	.byte	$17
	ora	$1D5F
	ora	($0E), y
	.byte	$5F
	and	$0A, x
	.byte	$12
	.byte	$17
	.byte	$0B
	clc
	jsr	$275F
	.byte	$1B
LA422:	clc
	ora	$195F, y
	asl	a
	.byte	$1C
	.byte	$1C
	asl	$5F1C
	ora	$5F18, x
	ora	$2211, x
	.byte	$5F
	.byte	$14
	asl	$190E
	.byte	$12
	.byte	$17
	bpl	LA48D
	.byte	$FC
	bvc	LA475
	ora	($18), y
	asl	$A5F, x
	.byte	$1B
	ora	$0B5F, x
	.byte	$1B
	asl	a
	.byte	$1F
	asl	$125F
	.byte	$17
	ora	$E0E
	ora	$1D5F
	clc
	.byte	$5F
	.byte	$1B
	asl	$0C1C
	asl	$5F0E, x
	asl	$0E, x
	pha
	.byte	$5F
	sed
	.byte	$52
	.byte	$FB
	sbc	$2C50, x
	.byte	$5F
	asl	a
	asl	$5F, x
	rol	a
	jsr	$E0A
	ora	$12, x
	.byte	$17
	pha
	.byte	$5F
	.byte	$0D
LA475:	asl	a
	asl	$1110, x
	ora	$1B0E, x
	.byte	$5F
	clc
	.byte	$0F
	.byte	$5F
	.byte	$2F
	clc
	.byte	$1B
	.byte	$12
	.byte	$14
	.byte	$52
	.byte	$FB
	.byte	$FC
	bvc	LA4AF
	asl	$5F1D, x
LA48D:	ora	$1811, x
	asl	$165F, x
	asl	$1D1C, x
	.byte	$52
	.byte	$FB
	.byte	$FC
	.byte	$33
	.byte	$1B
	.byte	$12
	.byte	$17
	.byte	$0C
	asl	$1C1C
	.byte	$5F
	rol	a
	jsr	$E0A
	ora	$12, x
	.byte	$17
	.byte	$5F
	asl	$0B16
	.byte	$1B
	asl	a
LA4AF:	.byte	$0C
	asl	$5F1C
	ora	$E11, x
	asl	$FD47
	rts

	.byte	$FC
	bvc	LA4E9
	.byte	$53
	asl	$5F, x
	.byte	$1C
	clc
	.byte	$5F
	ora	($0A), y
	ora	$2219, y
	jmp	$FC40

	bvc	LA4F6
	clc
	.byte	$1B
	asl	$0E1F
	.byte	$1B
	.byte	$5F
	.byte	$1C
	ora	($0A), y
	ora	$15, x
	.byte	$5F
	bit	$0B5F
	asl	$105F
	.byte	$1B
	asl	a
	ora	$0F0E, x
	asl	$5F15, x
	.byte	$0F
LA4E9:	clc
	.byte	$1B
	.byte	$5F
	ora	$E11, x
	.byte	$5F
	bpl	LA504
	.byte	$0F
	ora	$185F, x
LA4F6:	.byte	$0F
	.byte	$5F
	asl	$22, x
	.byte	$5F
	ora	$1E0A
	bpl	LA511
	ora	$1B0E, x
	.byte	$5F
LA504:	.byte	$1B
	asl	$1E1D
	.byte	$1B
	.byte	$17
	asl	$5F0D
	ora	$5F18, x
	.byte	$11
LA511:	asl	$5F1B
	ora	($18), y
	asl	$0E, x
	pha
	.byte	$5F
	sed
	.byte	$47
	sbc	$0C24, x
	.byte	$0C
	asl	$1D19
	.byte	$5F
	asl	$22, x
	.byte	$5F
	ora	$A11, x
	.byte	$17
	.byte	$14
	.byte	$1C
	.byte	$52
	.byte	$FB
	sbc	$3150, x
	clc
	jsr	$5F48
	rol	a
	jsr	$E0A
	ora	$12, x
	.byte	$17
	pha
	.byte	$5F
	.byte	$0C
	clc
	asl	$0E, x
	.byte	$5F
	ora	$5F18, x
	asl	$22, x
	.byte	$5F
	.byte	$1C
	.byte	$12
	ora	$520E
	.byte	$FB
	sbc	$202A, x
	asl	a
	asl	$1215
	.byte	$17
	.byte	$5F
	ora	$E11, x
	.byte	$17
	.byte	$5F
	jsr	$1211
	.byte	$1C
	ora	$1B0E, y
	.byte	$1C
	.byte	$44
	sbc	$3A50, x
	asl	a
	.byte	$12
	ora	$A5F, x
	.byte	$5F
	asl	$18, x
	asl	$0E, x
	.byte	$17
	ora	$1948, x
	ora	$0E, x
	asl	a
	.byte	$1C
	asl	$FD47
	bit	$205F
	clc
	asl	$0D15, x
	.byte	$5F
	bpl	LA59B
	.byte	$1F
	asl	$A5F
	.byte	$5F
	ora	$0E1B, y
	.byte	$1C
	asl	$1D17
	.byte	$5F
	ora	$5F18, x
	sed
	.byte	$52
LA59B:	.byte	$FB
	sbc	$3350, x
	ora	$0E, x
	asl	a
	.byte	$1C
	asl	$A5F
	.byte	$0C
	.byte	$0C
	asl	$1D19
	.byte	$5F
	asl	$22, x
	.byte	$5F
	ora	$18, x
	.byte	$1F
	asl	$5F48
	sed
	.byte	$52
	.byte	$FB
	.byte	$FC
	bvc	LA5DF
	.byte	$17
	ora	$2C5F
	.byte	$5F
	jsr	$1E18
	ora	$0D, x
	.byte	$5F
	ora	$12, x
	.byte	$14
	asl	$1D5F
	clc
	.byte	$5F
	ora	($0A), y
	.byte	$1F
	asl	$1C5F
	clc
	asl	$0E, x
	ora	$1211, x
	.byte	$17
	bpl	LA63C
	clc
	.byte	$0F
LA5DF:	.byte	$5F
	ora	$1211, x
	.byte	$17
	asl	$4949
	asl	a
	.byte	$5F
	ora	$1418, x
	asl	$5217
	.byte	$FB
	sbc	$3350, x
	ora	$0E, x
	asl	a
	.byte	$1C
	asl	$105F
	.byte	$12
	.byte	$1F
	asl	$165F
	asl	$1D5F
	ora	($22), y
	.byte	$5F
	.byte	$F7
	.byte	$52
	.byte	$FC
	bvc	LA632
	.byte	$1F
	asl	$5F17
	jsr	$E11
	.byte	$17
	.byte	$5F
	jsr	$5F0E
	ora	$1820, x
	.byte	$5F
	asl	a
	.byte	$1B
	asl	$195F
	asl	a
	.byte	$1B
	ora	$D0E, x
	.byte	$5F
	.byte	$0B
	.byte	$22
	.byte	$5F
	bpl	LA645
	asl	$1D0A
	.byte	$5F
	ora	$1C12
	.byte	$1D
LA632:	asl	a
	.byte	$17
	.byte	$0C
	asl	$481C
	.byte	$5F
	bit	$1C5F
LA63C:	ora	($0A), y
	ora	$15, x
	.byte	$5F
	.byte	$0B
	asl	$205F
LA645:	.byte	$12
	ora	$5F11, x
	ora	$E11, x
	asl	$FC52
	.byte	$FB
	bvc	LA67B
	asl	a
	.byte	$1B
	asl	$0E20
	ora	$15, x
	pha
	.byte	$5F
	sed
	.byte	$52
	.byte	$FB
	.byte	$FC
	bvc	LA68D
	.byte	$5F
	ora	$18, x
	.byte	$1F
	asl	$1D5F
	ora	($0E), y
	asl	$5F48
	sed
	.byte	$52
	sbc	$50FC, x
	.byte	$27
	clc
	.byte	$1C
	ora	$1D5F, x
	ora	($18), y
	.byte	$1E
LA67B:	.byte	$5F
	ora	$18, x
	.byte	$1F
	asl	$165F
	asl	$5F48
	sed
	.byte	$4B
	rti

	sbc	$50FC, x
	.byte	$3A
	.byte	$11
LA68D:	asl	$5F17
	ora	$1811, x
	asl	$A5F, x
	.byte	$1B
	ora	$0F5F, x
	.byte	$12
	.byte	$17
	.byte	$12
	.byte	$1C
	ora	($0E), y
	ora	$195F
	.byte	$1B
	asl	$0A19
	.byte	$1B
	.byte	$12
	.byte	$17
	bpl	LA70B
	.byte	$0F
	clc
	.byte	$1B
	.byte	$5F
	ora	$2211, x
	.byte	$5F
	ora	$190E
	asl	a
	.byte	$1B
	ora	$1B1E, x
	asl	$5F48
	ora	$0E15, y
	asl	a
	.byte	$1C
	asl	$1C5F
	asl	$5F0E
	asl	$0E, x
	.byte	$47
	sbc	$5F2C, x
	.byte	$1C
	ora	($0A), y
	ora	$15, x
	.byte	$5F
	jsr	$120A
	ora	$FC52, x
	bvc	LA70A
	.byte	$5F
	asl	a
	asl	$5F, x
	bpl	LA6FF
	asl	$1D0A
	ora	$22, x
	.byte	$5F
	ora	$0E15, y
	asl	a
	.byte	$1C
	asl	$5F0D
	ora	$A11, x
	ora	$1D5F, x
	ora	($18), y
	asl	$115F, x
	asl	a
	.byte	$1C
LA6FF:	ora	$1B5F, x
	asl	$1E1D
	.byte	$1B
	.byte	$17
	asl	$480D
LA70A:	.byte	$5F
LA70B:	sed
	.byte	$52
	.byte	$FB
	.byte	$FC
	bvc	LA736
	asl	$180F
	.byte	$1B
	asl	$1B5F
	asl	$0C0A
	ora	($12), y
	.byte	$17
	bpl	LA77F
	ora	$2211, x
	.byte	$5F
	.byte	$17
	asl	$1D21
	.byte	$5F
	ora	$0E, x
	.byte	$1F
	asl	$5F15
	clc
	.byte	$0F
	.byte	$5F
	asl	$1921
	.byte	$0E
LA736:	.byte	$1B
	.byte	$12
	asl	$0C17
	asl	$1D5F
	ora	($18), y
	asl	$165F, x
	asl	$1D1C, x
	.byte	$5F
	bpl	LA753
	.byte	$12
	.byte	$17
	.byte	$5F
	.byte	$F3
	.byte	$52
	.byte	$FB
	.byte	$FC
	bvc	LA77E
	.byte	$0F
LA753:	.byte	$5F
	ora	$1811, x
	asl	$D5F, x
	.byte	$12
	asl	$5F1C
	bit	$0C5F
	asl	a
	.byte	$17
	.byte	$5F
	.byte	$0B
	.byte	$1B
	.byte	$12
	.byte	$17
	bpl	LA7C9
	ora	$E11, x
	asl	$0B5F
	asl	a
	.byte	$0C
	.byte	$14
	.byte	$5F
	.byte	$0F
	clc
	.byte	$1B
	.byte	$5F
	asl	a
	.byte	$17
	clc
	ora	$E11, x
LA77E:	.byte	$1B
LA77F:	.byte	$5F
	asl	a
	ora	$0E1D, x
	asl	$19, x
	ora	$205F, x
	.byte	$12
	ora	$1811, x
	asl	$5F1D, x
	ora	$18, x
	.byte	$1C
	.byte	$1C
	.byte	$5F
	clc
	.byte	$0F
	.byte	$5F
	ora	$2211, x
	.byte	$5F
	ora	$E0E
	ora	$5F1C
	ora	$5F18, x
	ora	$1D0A
	asl	$FB52
	.byte	$FC
	.byte	$FC
	bvc	LA7D9
	clc
	clc
	ora	$220B
	asl	$175F
	clc
	jsr	$5F48
	sed
	.byte	$47
	sbc	$0A37, x
	.byte	$14
	asl	$0C5F
	asl	a
	.byte	$1B
	asl	$A5F
LA7C9:	.byte	$17
	ora	$1D5F
	asl	$1916
	ora	$175F, x
	clc
	ora	$1D5F, x
	ora	($0E), y
LA7D9:	.byte	$5F
	and	#$0A
	ora	$1C0E, x
	.byte	$52
	.byte	$FC
	bvc	LA81D
	.byte	$12
	ora	$15, x
	.byte	$5F
	ora	$1811, x
	asl	$1D5F, x
	asl	a
	.byte	$14
	asl	$165F
	asl	$1D5F
	clc
	.byte	$5F
	ora	$E11, x
	.byte	$5F
	.byte	$0C
	asl	a
	.byte	$1C
	ora	$0E15, x
	.byte	$4B
	rti

	sbc	$50FC, x
	.byte	$37
	asl	a
	.byte	$14
	asl	$1D5F
	ora	($0E), y
	.byte	$5F
	.byte	$37
	.byte	$1B
	asl	$1C0A
	asl	$0E1B, x
	.byte	$5F
	rol	$11
	asl	$1D1C
LA81D:	.byte	$52
	.byte	$FC
	bvc	LA85B
	asl	$0C15
	clc
	asl	$0E, x
	pha
	.byte	$5F
	sed
	.byte	$47
	sbc	$5F2C, x
	asl	a
	asl	$5F, x
	ora	$E11, x
	.byte	$5F
	.byte	$27
	.byte	$1B
	asl	a
	bpl	LA852
	.byte	$17
	ora	$18, x
	.byte	$1B
	ora	$4949
	rol	$1712
	bpl	LA8A5
	clc
	.byte	$0F
	.byte	$5F
	rol	$1712
	bpl	LA86A
	.byte	$52
	.byte	$FB
	.byte	$FC
	.byte	$50
LA852:	bit	$105F
	.byte	$12
	.byte	$1F
	asl	$1D5F
	.byte	$11
LA85B:	asl	$5F0E
	.byte	$17
	clc
	jsr	$A5F
	.byte	$5F
	.byte	$0C
	ora	($0A), y
	.byte	$17
	.byte	$0C
	.byte	$0E
LA86A:	.byte	$5F
	ora	$5F18, x
	.byte	$1C
	ora	($0A), y
	.byte	$1B
	asl	$1D5F
	ora	($12), y
	.byte	$1C
	.byte	$5F
	jsr	$1B18
	ora	$0D, x
	.byte	$5F
	asl	a
	.byte	$17
	ora	$1D5F
	clc
	.byte	$5F
	.byte	$1B
	asl	$0E15, x
	.byte	$5F
	ora	($0A), y
	ora	$0F, x
	.byte	$5F
	clc
	.byte	$0F
	.byte	$5F
	.byte	$12
	ora	$125F, x
	.byte	$0F
	.byte	$5F
	ora	$1811, x
	asl	$205F, x
	.byte	$12
	ora	$15, x
	.byte	$5F
	.byte	$17
	clc
LA8A5:	jsr	$1C5F
	ora	$170A, x
	ora	$0B5F
	asl	$121C
	ora	$5F0E
	asl	$0E, x
	.byte	$52
	.byte	$FB
	sbc	$3A50, x
	ora	($0A), y
	ora	$1C5F, x
	asl	a
	.byte	$22
	asl	$1D1C
	.byte	$5F
	ora	$1811, x
	asl	$FD4B, x
	.byte	$3A
	.byte	$12
	ora	$15, x
	.byte	$5F
	ora	$E11, x
	.byte	$5F
	bpl	LA8F2
	asl	$1D0A
	.byte	$5F
	jsr	$1B0A
	.byte	$1B
	.byte	$12
	clc
	.byte	$1B
	.byte	$5F
	.byte	$1C
	ora	$170A, x
	ora	$205F
	.byte	$12
	ora	$5F11, x
	asl	$0E, x
	.byte	$4B
	rti

LA8F2:	sbc	$50FC, x
	.byte	$37
	ora	($18), y
	asl	$A5F, x
	.byte	$1B
	ora	$A5F, x
	.byte	$5F
	.byte	$0F
	clc
	clc
	ora	$4C, x
	.byte	$FC
	.byte	$57
	sbc	$3750, x
	ora	($0E), y
	.byte	$17
	.byte	$5F
	ora	($0A), y
	ora	$0F, x
	.byte	$5F
	clc
	.byte	$0F
	.byte	$5F
	ora	$1211, x
	.byte	$1C
	.byte	$5F
	jsr	$1B18
	ora	$0D, x
	.byte	$5F
	.byte	$12
	.byte	$1C
	.byte	$5F
	ora	$1211, x
	.byte	$17
	asl	$5F48
	ora	($0A), y
	ora	$0F, x
	.byte	$5F
	clc
	.byte	$0F
	.byte	$5F
	ora	$E11, x
	.byte	$5F
	ora	$1B0A
	.byte	$14
	.byte	$17
	asl	$1C1C
	pha
	.byte	$5F
	asl	a
	.byte	$17
	ora	$4545
	rti

	.byte	$FB
	.byte	$FC
	.byte	$37
	ora	($22), y
	.byte	$5F
	.byte	$13
	clc
	asl	$171B, x
	asl	$5F22
	.byte	$12
	.byte	$1C
	.byte	$5F
	clc
	.byte	$1F
	asl	$471B
	sbc	$0A37, x
	.byte	$14
	asl	$175F
	clc
	jsr	$A5F
	.byte	$5F
	ora	$18, x
	.byte	$17
	bpl	LA9B6
	.byte	$5F
	ora	$18, x
	.byte	$17
	bpl	LA9D3
	.byte	$1B
	asl	$1D1C
	.byte	$47
	sbc	$0A2B, x
	ora	($0A), y
	ora	($0A), y
	ora	($0A), y
	eor	$45
	.byte	$FC
	.byte	$FB
	bit	$5F0F
	ora	$1811, x
	asl	$205F, x
	clc
	asl	$0D15, x
	.byte	$5F
	ora	$140A, x
	asl	$1D5F
	ora	($0E), y
	.byte	$5F
	.byte	$F7
	pha
	.byte	$5F
	ora	$1811, x
	asl	$165F, x
	asl	$1D1C, x
	.byte	$5F
	.byte	$17
	clc
	jsr	$D5F
	.byte	$12
	.byte	$1C
	.byte	$0C
	asl	a
	.byte	$1B
	.byte	$0D
	.byte	$5F
LA9B6:	.byte	$1C
	clc
	asl	$0E, x
	.byte	$5F
	clc
	ora	$E11, x
	.byte	$1B
	.byte	$5F
	.byte	$12
	ora	$160E, x
	.byte	$47
	sbc	$1827, x
	.byte	$1C
	ora	$1D5F, x
	ora	($18), y
	asl	$205F, x
	.byte	$12
LA9D3:	.byte	$1C
	ora	($5F), y
	ora	$5F18, x
	ora	($0A), y
	.byte	$1F
	asl	$1D5F
	ora	($0E), y
	.byte	$5F
	.byte	$F7
	.byte	$4B
	.byte	$FC
	.byte	$37
	ora	($18), y
	asl	$115F, x
	asl	a
	.byte	$1C
	ora	$105F, x
	.byte	$12
	.byte	$1F
	asl	$5F17
	asl	$5F19, x
	ora	$2211, x
	.byte	$5F
	.byte	$F7
	.byte	$47
	.byte	$FC
	.byte	$3A
	ora	($0A), y
	ora	$1C5F, x
	ora	($0A), y
	ora	$15, x
	.byte	$5F
	ora	$1811, x
	asl	$D5F, x
	.byte	$1B
	clc
	ora	$FC4B, y
	.byte	$37
	ora	($18), y
	asl	$115F, x
	asl	a
	.byte	$1C
	ora	$D5F, x
	.byte	$1B
	clc
	ora	$0E19, y
	ora	$1D5F
	ora	($22), y
	.byte	$5F
	.byte	$F7
	.byte	$47
	.byte	$FC
	bit	$17
	ora	$185F
	.byte	$0B
	ora	$120A, x
	.byte	$17
	asl	$5F0D
	ora	$E11, x
	.byte	$5F
	.byte	$F7
	.byte	$47
	.byte	$FC
	.byte	$37
	ora	($0A), y
	ora	$125F, x
	.byte	$1C
	.byte	$5F
	asl	$1E, x
	.byte	$0C
	ora	($5F), y
	ora	$1818, x
	.byte	$5F
	.byte	$12
	asl	$19, x
	clc
	.byte	$1B
	ora	$170A, x
	ora	$1D5F, x
	clc
	.byte	$5F
	ora	$1B11, x
	clc
	jsr	$A5F
	jsr	$220A
	.byte	$47
	.byte	$FC
	sed
	.byte	$5F
	.byte	$1C
	asl	$1B0A
	.byte	$0C
	ora	($0E), y
	ora	$1D5F
	ora	($0E), y
	.byte	$5F
	bpl	LAA98
	clc
	asl	$0D17, x
	.byte	$5F
	asl	a
	ora	$15, x
	.byte	$5F
	asl	a
	.byte	$0B
	clc
	asl	$471D, x
	.byte	$FB
	sbc	$25FC, x
	asl	$5F1D, x
	ora	$E11, x
	.byte	$1B
	.byte	$0E
LAA98:	.byte	$5F
	.byte	$0F
	clc
	asl	$0D17, x
	.byte	$5F
	.byte	$17
	clc
	ora	$1211, x
	.byte	$17
	bpl	LAAEE
	.byte	$FC
	.byte	$37
	ora	($0E), y
	.byte	$1B
	asl	$125F
	.byte	$1C
	.byte	$5F
	asl	a
	.byte	$5F
	ora	$0E1B, x
	asl	a
	.byte	$1C
	asl	$0E1B, x
	.byte	$5F
	.byte	$0B
	clc
	and	($47, x)
	.byte	$FC
	sed
	.byte	$5F
	ora	$1C12
	.byte	$0C
	clc
	.byte	$1F
	asl	$1C1B
	.byte	$5F
	ora	$E11, x
	.byte	$5F
	.byte	$F7
	.byte	$47
	.byte	$FC
	and	#$0E
	asl	$5F15
	ora	$E11, x
	.byte	$5F
	jsr	$1712
	ora	$0B5F
	ora	$18, x
	jsr	$1712
	bpl	LAB49
	.byte	$0F
	.byte	$1B
	clc
	.byte	$16
LAAEE:	.byte	$5F
	.byte	$0B
	asl	$1211
	.byte	$17
	ora	$1D5F
	ora	($0E), y
	.byte	$5F
	ora	$1B11, x
	clc
	.byte	$17
	asl	$FC47
	.byte	$37
	ora	($0E), y
	.byte	$1B
	asl	$125F
	.byte	$1C
	.byte	$5F
	.byte	$17
	clc
	ora	$1211, x
	.byte	$17
	bpl	LAB72
	ora	$5F18, x
	ora	$140A, x
	asl	$115F
	asl	$0E1B
	pha
	.byte	$5F
	sed
	.byte	$47
	.byte	$FC
	.byte	$32
	.byte	$0F
	.byte	$5F
	rol	a
	.byte	$32
	.byte	$2F
	.byte	$27
	.byte	$5F
	ora	$1811, x
	asl	$115F, x
	asl	a
	.byte	$1C
	ora	$105F, x
	asl	a
	.byte	$12
	.byte	$17
	asl	$5F0D
	sbc	$FC, x
	and	#$18
	.byte	$1B
	ora	$171E, x
	asl	$1C5F
	.byte	$16
LAB49:	.byte	$12
	ora	$0E, x
	.byte	$1C
	.byte	$5F
	asl	$1819, x
	.byte	$17
	.byte	$5F
	ora	$E11, x
	asl	$5F48
	sed
	.byte	$47
	sbc	$1137, x
	clc
	asl	$115F, x
	asl	a
	.byte	$1C
	ora	$0F5F, x
	clc
	asl	$0D17, x
	.byte	$5F
	ora	$E11, x
	.byte	$5F
	.byte	$F7
	.byte	$47
LAB72:	.byte	$FC
	sec
	.byte	$17
	.byte	$0F
	clc
	.byte	$1B
	ora	$171E, x
	asl	a
	ora	$150E, x
	.byte	$22
	pha
	.byte	$5F
	.byte	$12
	ora	$125F, x
	.byte	$1C
	.byte	$5F
	asl	$1916
	ora	$4722, x
	.byte	$FC
	.byte	$2B
	asl	$D0E
	.byte	$5F
	asl	$22, x
	.byte	$5F
	.byte	$1F
LAB98:	clc
	.byte	$12
	.byte	$0C
	asl	$FD48
	bvc	LAB98
	pha
	.byte	$5F
	.byte	$0F
	clc
	.byte	$1B
	.byte	$5F
	ora	$1211, x
	.byte	$1C
	.byte	$5F
	.byte	$12
	.byte	$1C
	.byte	$5F
	rol	a
	jsr	$E0A
	ora	$12, x
	.byte	$17
	.byte	$47
	sbc	$1837, x
	.byte	$5F
	.byte	$1B
	asl	$0C0A
	ora	($5F), y
	ora	$E11, x
	.byte	$5F
	.byte	$17
	asl	$1D21
	.byte	$5F
	ora	$0E, x
	.byte	$1F
	asl	$5F15
	ora	$1811, x
	asl	$165F, x
	asl	$1D1C, x
	.byte	$5F
	.byte	$1B
	asl	a
	.byte	$12
	.byte	$1C
	asl	$1D5F
	ora	($22), y
	.byte	$5F
	plp
	and	($19, x)
	asl	$121B
	asl	$0C17
	asl	$5FF0
	.byte	$0B
	.byte	$22
	.byte	$5F
	sbc	$47, x
	sbc	$2230, x
	.byte	$5F
	ora	($18), y
	ora	$5F0E, y
	.byte	$12
	.byte	$1C
	.byte	$5F
	jsr	$1D12
	ora	($5F), y
	ora	$E11, x
	asl	$FB52
	.byte	$FC
	bvc	LAC37
	.byte	$1B
	clc
	asl	$5F, x
	jsr	$E11
	.byte	$1B
	asl	$1D5F
	ora	($18), y
	asl	$A5F, x
	.byte	$1B
	ora	$175F, x
	clc
	jsr	$5F48
	asl	$22, x
	.byte	$5F
	.byte	$0C
	asl	a
	.byte	$1C
	ora	$0E15, x
	.byte	$5F
	ora	$12, x
	asl	$451C
	.byte	$FC
	.byte	$FC
LAC37:	.byte	$57
	sbc	$5F, x
	ora	$5F18, x
	ora	$E11, x
	.byte	$5F
	.byte	$17
	clc
	.byte	$1B
	ora	$5F11, x
	asl	a
	.byte	$17
	ora	$FC45
	.byte	$57
	sbc	$5F, x
	ora	$5F18, x
	ora	$E11, x
	.byte	$5F
	.byte	$1C
	clc
	asl	$111D, x
	.byte	$5F
	asl	a
	.byte	$17
	ora	$FC45
	.byte	$57
	sbc	$5F, x
	ora	$5F18, x
	ora	$E11, x
	.byte	$5F
	asl	$1C0A
	ora	$FB52, x
	.byte	$FC
	.byte	$57
	sbc	$5F, x
	ora	$5F18, x
	ora	$E11, x
	.byte	$5F
	jsr	$1C0E
	ora	$FB52, x
	.byte	$FC
	bit	$F1
	.byte	$5F
	ora	$0A1B
	jsr	$5F1C
	.byte	$17
	asl	$1B0A
	jmp	$60FC

	sbc	$1137, x
	asl	$F45F
	.byte	$5F
	.byte	$12
	.byte	$1C
	.byte	$5F
	.byte	$1B
	asl	$1717, x
	.byte	$12
	.byte	$17
	bpl	LAD04
	asl	a
	jsr	$220A
	.byte	$47
	.byte	$FC
	rts

	sbc	$1137, x
	asl	$F45F
	.byte	$5F
	asl	a
	ora	$0A1D, x
	.byte	$0C
	.byte	$14
	asl	$5F0D
	.byte	$0B
	asl	$180F
	.byte	$1B
	asl	$F85F
	.byte	$5F
	jsr	$1C0A
	.byte	$5F
	.byte	$1B
	asl	$0D0A
	.byte	$22
	.byte	$47
	.byte	$FC
	sed
	.byte	$5F
	asl	a
	ora	$0A1D, x
	.byte	$0C
	.byte	$14
	.byte	$1C
	jmp	$37FC

	ora	($0E), y
	.byte	$5F
	.byte	$F4
	.byte	$53
	.byte	$1C
	.byte	$5F
	.byte	$2B
	.byte	$12
	ora	$5FF0, x
	ora	($0A), y
	.byte	$1F
	asl	$0B5F
	asl	$170E
	.byte	$5F
	.byte	$1B
	asl	$1E0D
	.byte	$0C
	asl	$5F0D
	.byte	$0B
	.byte	$22
	.byte	$5F
	sbc	$47, x
	.byte	$FC
	.byte	$37
	ora	($0E), y
	.byte	$5F
LAD04:	asl	a
	ora	$0A1D, x
	.byte	$0C
	.byte	$14
	.byte	$5F
	.byte	$0F
	asl	a
	.byte	$12
	ora	$0E, x
	ora	$A5F
	.byte	$17
	ora	$1D5F
	ora	($0E), y
	.byte	$1B
	asl	$205F
	asl	a
	.byte	$1C
	.byte	$5F
	.byte	$17
	clc
	.byte	$5F
	ora	$18, x
	.byte	$1C
	.byte	$1C
	.byte	$5F
	clc
	.byte	$0F
	.byte	$5F
	.byte	$2B
	.byte	$12
	ora	$335F, x
	clc
	.byte	$12
	.byte	$17
	ora	$4C1C, x
	.byte	$FC
	rts

	sbc	$1826, x
	asl	$16, x
	asl	a
	.byte	$17
	ora	$FC4B
	.byte	$37
	ora	($0A), y
	ora	$0C5F, x
	asl	a
	.byte	$17
	.byte	$17
	clc
	ora	$0B5F, x
	asl	$1E5F
	.byte	$1C
	asl	$5F0D
	.byte	$12
	.byte	$17
	.byte	$5F
	.byte	$0B
	asl	a
	ora	$151D, x
	asl	$FC47
	rts

	sbc	$1E25, x
	ora	$1D5F, x
	ora	($0A), y
	ora	$1C5F, x
	ora	$150E, y
	ora	$5F, x
	ora	($0A), y
	ora	$5F11, x
	.byte	$0B
	asl	$170E
	.byte	$5F
	.byte	$0B
	ora	$18, x
	.byte	$0C
	.byte	$14
	asl	$470D
	.byte	$FC
	.byte	$37
	ora	($0E), y
	.byte	$5F
	.byte	$1C
	ora	$150E, y
	ora	$5F, x
	jsr	$1512
	ora	$5F, x
	.byte	$17
	clc
	ora	$205F, x
	clc
	.byte	$1B
	.byte	$14
	.byte	$47
	.byte	$FC
	.byte	$37
	ora	($18), y
	asl	$115F, x
	asl	a
	.byte	$1C
	ora	$195F, x
	asl	$5F1D, x
	ora	$E11, x
	.byte	$5F
	.byte	$F4
	.byte	$5F
	ora	$5F18, x
	.byte	$1C
	ora	$0E, x
	asl	$4719
	.byte	$FC
	.byte	$37
	ora	($0E), y
	.byte	$5F
	.byte	$F4
	.byte	$53
	.byte	$1C
	.byte	$5F
	.byte	$1C
	ora	$150E, y
	ora	$5F, x
	ora	($0A), y
	ora	$5F11, x
	.byte	$0B
	asl	$170E
	.byte	$5F
	.byte	$0B
	ora	$18, x
	.byte	$0C
	.byte	$14
	asl	$470D
	.byte	$FC
	rts

	sbc	$1137, x
	clc
	asl	$115F, x
	asl	a
	.byte	$1C
	ora	$D5F, x
	clc
	.byte	$17
	asl	$205F
	asl	$1515
	.byte	$5F
	.byte	$12
	.byte	$17
	.byte	$5F
	ora	$0F0E
	asl	$1D0A
	.byte	$12
	.byte	$17
	bpl	LAE5F
	ora	$E11, x
	.byte	$5F
	.byte	$F4
	.byte	$47
	sbc	$FC60, x
	.byte	$37
	ora	($22), y
	.byte	$5F
	plp
	and	($19, x)
	asl	$121B
	asl	$0C17
	asl	$12FD
	.byte	$17
	.byte	$0C
	.byte	$1B
	asl	$1C0A
	asl	$5F1C
	.byte	$0B
	.byte	$22
	.byte	$5F
	sbc	$47, x
	.byte	$FC
	.byte	$37
	ora	($22), y
	.byte	$5F
	rol	a
	.byte	$32
	.byte	$2F
	.byte	$27
	sbc	$1712, x
	.byte	$0C
	.byte	$1B
	asl	$1C0A
	asl	$5F1C
	.byte	$0B
	.byte	$22
	.byte	$5F
	sbc	$47, x
	.byte	$FC
	rts

	.byte	$FB
	sbc	$1826, x
	asl	$0A1B, x
	bpl	LAE59
	.byte	$5F
	asl	a
	.byte	$17
	ora	$205F
	.byte	$12
	ora	$115F, x
	asl	a
	.byte	$1F
	.byte	$0E
	.byte	$5F
LAE59:	.byte	$1C
	asl	$1F1B
	.byte	$0E
	.byte	$0D
LAE5F:	.byte	$5F
	ora	$E11, x
	asl	$205F
	asl	$1515
	.byte	$47
	sbc	$1137, x
	clc
	asl	$115F, x
	asl	a
	.byte	$1C
	ora	$0B5F, x
	asl	$170E
	.byte	$5F
	ora	$181B, y
	asl	$18, x
	ora	$D0E, x
	.byte	$5F
	ora	$5F18, x
	ora	$E11, x
	.byte	$5F
	.byte	$17
	asl	$1D21
	.byte	$5F
	ora	$0E, x
	.byte	$1F
	asl	$4715
	.byte	$FB
	.byte	$FC
	rts

	sbc	$1137, x
	clc
	asl	$115F, x
	asl	a
	.byte	$1C
	ora	$155F, x
	asl	$1B0A
	.byte	$17
	asl	$5F0D
	asl	a
	.byte	$5F
	.byte	$17
	asl	$5F20
	.byte	$1C
	ora	$150E, y
	ora	$47, x
	.byte	$FC
	.byte	$34
	asl	$0E12, x
	ora	$2215, x
	.byte	$5F
	rol	a
	clc
	ora	$0E, x
	asl	$5F, x
	.byte	$0C
	ora	$18, x
	.byte	$1C
	asl	$5F1C
	ora	($12), y
	.byte	$1C
	.byte	$5F
	asl	$0E22
	.byte	$1C
	.byte	$5F
	asl	a
	.byte	$17
	ora	$1C5F
	asl	$1D1D
	ora	$0E, x
	.byte	$1C
	.byte	$5F
	.byte	$12
	.byte	$17
	ora	$5F18, x
	.byte	$1C
	ora	$0E, x
	asl	$4719
	.byte	$FC
	.byte	$F4
	.byte	$5F
	ora	$18, x
	clc
	.byte	$14
	.byte	$1C
	.byte	$5F
	ora	($0A), y
	ora	$2219, y
	.byte	$47
	.byte	$FC
	rts

	sbc	$5FF8, x
	.byte	$1C
	ora	$1B0A, x
	ora	$D0E, x
	.byte	$5F
	ora	$5F18, x
	.byte	$1B
	asl	$5F17, x
	asl	a
	jsr	$220A
	.byte	$47
	.byte	$FC
	and	$1E
	ora	$205F, x
	asl	a
	.byte	$1C
	.byte	$5F
	.byte	$0B
	ora	$18, x
	.byte	$0C
	.byte	$14
	asl	$5F0D
	.byte	$12
	.byte	$17
	.byte	$5F
	.byte	$0F
	.byte	$1B
	clc
	.byte	$17
	ora	$FC47, x
	sed
	.byte	$5F
	asl	$0E1C, x
	ora	$1D5F
	ora	($0E), y
	.byte	$5F
	.byte	$2B
	asl	$0B1B
	.byte	$47
	.byte	$FC
	rts

	sbc	$1137, x
	asl	$F45F
	.byte	$5F
	.byte	$12
	.byte	$1C
	.byte	$5F
	asl	a
	.byte	$1C
	ora	$0E, x
	asl	$4719
	.byte	$FC
	rts

	sbc	$1137, x
	asl	$F45F
	.byte	$5F
	asl	a
	ora	$0A1D, x
	.byte	$0C
	.byte	$14
	.byte	$1C
	jmp	$37FC

	ora	($22), y
	.byte	$5F
	.byte	$2B
	.byte	$12
	ora	$5FF0, x
	ora	$0C0E
	.byte	$1B
	asl	$1C0A
	asl	$5F0D
	.byte	$0B
	.byte	$22
	.byte	$5F
	sbc	$47, x
	.byte	$FC
	bit	$5F
	asl	$12, x
	.byte	$1C
	.byte	$1C
	jmp	$315F

	clc
	.byte	$5F
	ora	$160A
	asl	a
	bpl	LAF9D
	.byte	$5F
	ora	($0A), y
	ora	$5F11, x
	.byte	$0B
	asl	$170E
	.byte	$5F
	.byte	$1C
	.byte	$0C
	clc
LAF9D:	.byte	$1B
	asl	$4C0D
	.byte	$FC
	rts

	sbc	$FCF4, x
	.byte	$0C
	ora	($0A), y
	.byte	$17
	ora	$5F1C, x
	ora	$E11, x
	.byte	$5F
	.byte	$1C
	ora	$150E, y
	ora	$5F, x
	clc
	.byte	$0F
	.byte	$5F
	inc	$47, x
	.byte	$FC
	sed
	.byte	$53
	.byte	$1C
	.byte	$5F
	.byte	$1C
	ora	$150E, y
	ora	$5F, x
	.byte	$12
	.byte	$1C
	.byte	$5F
	.byte	$0B
	ora	$18, x
	.byte	$0C
	.byte	$14
	asl	$470D
	.byte	$FC
	rts

	sbc	$1137, x
	asl	$F45F
	.byte	$5F
	.byte	$12
	.byte	$1C
	.byte	$5F
	.byte	$0B
	.byte	$1B
	asl	$1D0A
	ora	($12), y
	.byte	$17
	bpl	LB047
	.byte	$0F
	.byte	$12
	.byte	$1B
	asl	$FC47
LAFEE:	bvc	LB01C
	.byte	$0F
	.byte	$5F
	ora	$1811, x
	asl	$115F, x
	asl	a
	.byte	$1C
	ora	$0C5F, x
	clc
	ora	$15, x
	asl	$1D0C
	asl	$5F0D
	asl	a
	ora	$15, x
	.byte	$5F
	ora	$E11, x
	.byte	$5F
	.byte	$37
	.byte	$1B
	asl	$1C0A
	asl	$0E1B, x
	.byte	$5F
	rol	$11
	asl	$1D1C
LB01C:	.byte	$1C
	pha
	sbc	$5F0A, x
	.byte	$14
	asl	$5F22
	jsr	$1512
	ora	$5F, x
	.byte	$0B
	asl	$0F5F
	clc
	asl	$0D17, x
	.byte	$52
	.byte	$FB
	sbc	$3250, x
	.byte	$17
	.byte	$0C
	asl	$1E5F
	.byte	$1C
	asl	$480D
	.byte	$5F
	ora	$E11, x
	.byte	$5F
	.byte	$14
	.byte	$0E
LB047:	.byte	$22
	.byte	$5F
	jsr	$1512
	ora	$5F, x
	ora	$1C12
	asl	a
	ora	$0E19, y
	asl	a
	.byte	$1B
	pha
	.byte	$5F
	.byte	$0B
	asl	$5F1D, x
	ora	$E11, x
	.byte	$5F
	ora	$1818
	.byte	$1B
	.byte	$5F
	jsr	$1512
	ora	$5F, x
	.byte	$0B
	asl	$185F
	ora	$170E, y
	.byte	$5F
	asl	a
	.byte	$17
	ora	$1D5F
	ora	($18), y
	asl	$165F, x
	asl	a
	.byte	$22
	.byte	$5F
	ora	$1C0A, y
	.byte	$1C
	.byte	$5F
	ora	$1B11, x
	clc
	asl	$1110, x
	.byte	$52
	.byte	$FC
	bvc	LB0B8
	asl	a
	.byte	$1C
	ora	$185F, x
	.byte	$0F
	.byte	$5F
	ora	$1211, x
	.byte	$1C
	.byte	$5F
	.byte	$0C
	asl	a
	.byte	$1C
	ora	$0E15, x
	.byte	$5F
	.byte	$12
	.byte	$1C
	.byte	$5F
	asl	a
	.byte	$5F
	ora	$2018, x
	.byte	$17
	.byte	$5F
	jsr	$E11
	.byte	$1B
	asl	$A5F
	.byte	$1B
	asl	$18, x
	.byte	$1B
LB0B8:	pha
	.byte	$5F
	jsr	$A0E
	ora	$1718, y
	.byte	$1C
	pha
	.byte	$5F
	asl	a
	.byte	$17
	ora	$165F
	asl	a
	.byte	$17
	.byte	$22
	.byte	$5F
	clc
	ora	$E11, x
	.byte	$1B
	.byte	$5F
	.byte	$12
	ora	$160E, x
	.byte	$1C
	.byte	$5F
	asl	$0A, x
	.byte	$22
	.byte	$5F
	.byte	$0B
	asl	$195F
	asl	$0C1B, x
	ora	($0A), y
	.byte	$1C
	asl	$520D
	.byte	$FB
	sbc	$3550, x
	asl	$1E1D
	.byte	$1B
	.byte	$17
	.byte	$5F
	ora	$5F18, x
	ora	$E11, x
	.byte	$5F
	bit	$1717
	.byte	$5F
	.byte	$0F
	clc
	.byte	$1B
	.byte	$5F
	asl	a
	.byte	$5F
	.byte	$1B
	asl	$1D1C
	.byte	$5F
	.byte	$12
	.byte	$0F
	.byte	$5F
	ora	$1811, x
	asl	$A5F, x
	.byte	$1B
	ora	$205F, x
	clc
	asl	$0D17, x
	asl	$5F0D
	.byte	$12
	.byte	$17
	.byte	$5F
	.byte	$0B
	asl	a
	ora	$151D, x
	asl	$5F48
	sed
	.byte	$52
	.byte	$FB
	sbc	$3650, x
	ora	$0E, x
	asl	$5F19
	ora	($0E), y
	asl	a
	ora	$1C, x
	.byte	$5F
	asl	a
	ora	$15, x
	.byte	$52
	.byte	$FC
	bvc	LB167
	asl	$0C1C
	asl	$0D17
	asl	a
	.byte	$17
	ora	$185F, x
	.byte	$0F
	.byte	$5F
	plp
	.byte	$1B
	ora	$121B
	.byte	$0C
	.byte	$14
	pha
	.byte	$5F
	ora	$12, x
	.byte	$1C
	ora	$170E, x
	.byte	$5F
	.byte	$17
	clc
	jsr	$1D5F
	clc
	.byte	$5F
	asl	$22, x
	.byte	$5F
LB167:	jsr	$1B18
	ora	$521C
	.byte	$FB
	sbc	$2C50, x
	ora	$125F, x
	.byte	$1C
	.byte	$5F
	ora	$1518, x
	ora	$1D5F
	ora	($0A), y
	ora	$125F, x
	.byte	$17
	.byte	$5F
	asl	a
	bpl	LB194
	.byte	$1C
	.byte	$5F
	ora	$1C0A, y
	ora	$285F, x
	.byte	$1B
	ora	$121B
	.byte	$0C
	.byte	$14
LB194:	.byte	$5F
	.byte	$0F
	clc
	asl	$1110, x
	ora	$D5F, x
	asl	$1816
	.byte	$17
	.byte	$1C
	.byte	$5F
	jsr	$1D12
	ora	($5F), y
	asl	a
	.byte	$5F
	and	$0A
	ora	$15, x
	.byte	$5F
	clc
	.byte	$0F
	.byte	$5F
	.byte	$2F
	.byte	$12
	bpl	LB1C7
	ora	$FB52, x
	sbc	$3750, x
	ora	($0E), y
	.byte	$17
	.byte	$5F
	.byte	$0C
	asl	a
	asl	$0E, x
	.byte	$5F
	.byte	$1D
	.byte	$11
LB1C7:	asl	$275F
	.byte	$1B
	asl	a
	bpl	LB1E6
	.byte	$17
	ora	$18, x
	.byte	$1B
	ora	$205F
	ora	($18), y
	.byte	$5F
	.byte	$1C
	ora	$1518, x
	asl	$1D5F
	ora	($0E), y
	.byte	$5F
	ora	$0E1B, y
	.byte	$0C
LB1E6:	.byte	$12
	clc
	asl	$5F1C, x
	bpl	LB202
	clc
	.byte	$0B
	asl	$A5F
	.byte	$17
	ora	$115F
	.byte	$12
	ora	$125F
	ora	$125F, x
	.byte	$17
	.byte	$5F
	ora	$E11, x
LB202:	.byte	$5F
	ora	$1B0A
	.byte	$14
	.byte	$17
	asl	$1C1C
	.byte	$52
	.byte	$FB
	sbc	$3150, x
	clc
	jsr	$5F48
	sed
	pha
	.byte	$5F
	ora	$1811, x
	asl	$165F, x
	asl	$1D1C, x
	.byte	$5F
	ora	($0E), y
	ora	$19, x
	.byte	$5F
	asl	$5F1C, x
	.byte	$1B
	asl	$180C
	.byte	$1F
	asl	$5F1B
	ora	$E11, x
	.byte	$5F
	and	$0A
	ora	$15, x
	.byte	$5F
	clc
	.byte	$0F
	.byte	$5F
	.byte	$2F
	.byte	$12
	bpl	LB252
	ora	$A5F, x
	.byte	$17
	ora	$1B5F
	asl	$1D1C
	clc
	.byte	$1B
	asl	$195F
	.byte	$0E
	asl	a
LB252:	.byte	$0C
	asl	$1D5F
	clc
	.byte	$5F
	clc
	asl	$5F1B, x
	ora	$0A, x
	.byte	$17
	ora	$FB52
	sbc	$3750, x
	ora	($0E), y
	.byte	$5F
	.byte	$27
	.byte	$1B
	asl	a
	bpl	LB285
	.byte	$17
	ora	$18, x
	.byte	$1B
	ora	$165F
	asl	$1D1C, x
	.byte	$5F
	.byte	$0B
	asl	$D5F
	asl	$0E0F
	asl	a
	ora	$D0E, x
	.byte	$52
	.byte	$FB
LB285:	sbc	$3750, x
	asl	a
	.byte	$14
	asl	$175F
	clc
	jsr	$205F
	ora	($0A), y
	ora	$1F0E, x
	asl	$5F1B
	ora	$1811, x
	asl	$165F, x
	asl	a
	.byte	$22
	.byte	$5F
	.byte	$0F
	.byte	$12
	.byte	$17
	ora	$125F
	.byte	$17
	.byte	$5F
	ora	$E11, x
	.byte	$1C
	asl	$375F
	.byte	$1B
	asl	$1C0A
	asl	$0E1B, x
	.byte	$5F
	rol	$11
	asl	$1D1C
	.byte	$1C
	.byte	$5F
	ora	$5F18, x
	asl	a
	.byte	$12
	ora	$1D5F
	ora	($0E), y
	asl	$125F
	.byte	$17
	.byte	$5F
	ora	$2211, x
	.byte	$5F
	.byte	$1A
	asl	$1C0E, x
	ora	$FB52, x
	sbc	$3750, x
	ora	($0E), y
	.byte	$17
	.byte	$5F
	.byte	$1C
	ora	$A0E, y
	.byte	$14
	.byte	$5F
	jsr	$1D12
	ora	($5F), y
	ora	$E11, x
	.byte	$5F
	bpl	LB310
	asl	a
	.byte	$1B
	ora	$481C
	.byte	$5F
	.byte	$0F
	clc
	.byte	$1B
	.byte	$5F
	ora	$E11, x
	.byte	$22
	.byte	$5F
	ora	($0A), y
	.byte	$1F
	asl	$165F
	asl	$110C, x
	.byte	$5F
	.byte	$14
	.byte	$17
	clc
	.byte	$20
	.byte	$15
LB310:	asl	$100D
	asl	$1D5F
	ora	($0A), y
	ora	$165F, x
	asl	a
	.byte	$22
	.byte	$5F
	asl	a
	.byte	$12
	ora	$1D5F
	ora	($0E), y
	asl	$FB52
	sbc	$3050, x
	asl	a
	.byte	$22
	.byte	$5F
	ora	$E11, x
	.byte	$5F
	ora	$12, x
	bpl	LB347
	ora	$1C5F, x
	ora	($12), y
	.byte	$17
	asl	$1E5F
	ora	$1718, y
	.byte	$5F
	ora	$E11, x
	.byte	$0E
LB347:	pha
	.byte	$5F
	sed
	.byte	$52
	.byte	$FC
	.byte	$FB
	.byte	$37
	ora	($0E), y
	.byte	$5F
	ora	$0B0A, x
	ora	$0E, x
	ora	$1B5F, x
	asl	$0D0A
	.byte	$1C
	.byte	$5F
	asl	a
	.byte	$1C
	.byte	$5F
	.byte	$0F
	clc
	ora	$15, x
	clc
	jsr	$441C
	.byte	$FB
	sbc	$FD60, x
	.byte	$5F
	bvc	LB39C
	.byte	$5F
	asl	a
	asl	$5F, x
	plp
	.byte	$1B
	ora	$121B
	.byte	$0C
	.byte	$14
	.byte	$5F
	asl	a
	.byte	$17
	ora	$1D5F
	ora	($18), y
	asl	$A5F, x
	.byte	$1B
	ora	$165F, x
	.byte	$22
	.byte	$5F
	ora	$1C0E
	.byte	$0C
	asl	$0D17
	asl	a
	.byte	$17
	ora	$FB52, x
	sbc	$505F, x
	.byte	$37
LB39C:	ora	($1B), y
	asl	$5F0E
	.byte	$12
	ora	$160E, x
	.byte	$1C
	.byte	$5F
	jsr	$1B0E
	asl	$175F
	asl	$D0E
	asl	$5F0D
	ora	$5F18, x
	.byte	$1B
	asl	$0C0A
	ora	($5F), y
	ora	$E11, x
	.byte	$5F
	bit	$151C
	asl	$185F
	.byte	$0F
	.byte	$5F
	.byte	$27
	.byte	$1B
	asl	a
	bpl	LB3E5
	.byte	$17
	.byte	$1C
	pha
	.byte	$5F
	jsr	$1211
	.byte	$0C
	ora	($5F), y
	.byte	$12
	.byte	$1C
	.byte	$5F
	.byte	$1C
	clc
	asl	$111D, x
	.byte	$5F
	clc
	.byte	$0F
	.byte	$5F
	and	$1B
LB3E5:	asl	$0C0C
	clc
	.byte	$17
	asl	a
	.byte	$1B
	.byte	$22
	.byte	$52
	.byte	$FB
	sbc	$505F, x
	bit	$105F
	asl	a
	ora	$E11, x
	.byte	$1B
	asl	$5F0D
	ora	$E11, x
	.byte	$1C
	asl	$125F
	ora	$160E, x
	.byte	$1C
	pha
	.byte	$5F
	.byte	$1B
	asl	$0C0A
	ora	($0E), y
	ora	$1D5F
	ora	($0E), y
	.byte	$5F
	.byte	$12
	.byte	$1C
	ora	$0A, x
	.byte	$17
	ora	$5F48
	asl	a
	.byte	$17
	ora	$1D5F
	ora	($0E), y
	.byte	$1B
	asl	$D5F
	asl	$0E0F
	asl	a
	ora	$D0E, x
	.byte	$5F
	asl	a
	.byte	$5F
	.byte	$0C
	.byte	$1B
	asl	$1D0A
	asl	$0E1B, x
	.byte	$5F
	clc
	.byte	$0F
	.byte	$5F
	bpl	LB45C
	asl	$1D0A
	.byte	$5F
	asl	$121F
	ora	$52, x
	.byte	$FB
	sbc	$505F, x
	and	($18), y
	jsr	$2C5F
	.byte	$5F
	ora	($0A), y
	.byte	$1F
	asl	$0E5F
	.byte	$17
	.byte	$1D
LB45C:	.byte	$1B
	asl	$1D1C, x
	asl	$5F0D
	ora	$E11, x
	.byte	$5F
	ora	$1B11, x
	asl	$5F0E
	.byte	$12
	ora	$160E, x
	.byte	$1C
	.byte	$5F
	ora	$5F18, x
	ora	$1B11, x
	asl	$5F0E
	jsr	$1B18
	ora	$2211, x
	.byte	$5F
	.byte	$14
	asl	$190E
	asl	$1C1B
	.byte	$52
	.byte	$FB
	sbc	$505F, x
	.byte	$37
	ora	($0E), y
	.byte	$12
	.byte	$1B
	.byte	$5F
	ora	$1C0E
	.byte	$0C
	asl	$0D17
	asl	a
	.byte	$17
	ora	$5F1C, x
	jsr	$1512
	ora	$5F, x
	ora	$181B, y
	ora	$0C0E, x
	ora	$1D5F, x
	ora	($0E), y
	.byte	$5F
	.byte	$12
	ora	$160E, x
	.byte	$1C
	.byte	$5F
	asl	$1D17, x
	.byte	$12
	ora	$5F, x
	ora	$2211, x
	.byte	$5F
	.byte	$1A
	asl	$1C0E, x
	ora	$155F, x
	asl	$0D0A
	.byte	$1C
	.byte	$5F
	ora	$E11, x
	asl	$1D5F
	clc
	.byte	$5F
	.byte	$1C
	asl	$140E
	.byte	$5F
	ora	$E11, x
	asl	$5F, x
	clc
	asl	$521D, x
	.byte	$FB
	sbc	$505F, x
	.byte	$3A
	ora	($0E), y
	.byte	$17
	.byte	$5F
	asl	a
	.byte	$5F
	.byte	$17
	asl	$5F20
	asl	$121F
	ora	$5F, x
	asl	a
	.byte	$1B
	.byte	$12
	.byte	$1C
	asl	$481C
	.byte	$5F
	.byte	$0F
	.byte	$12
	.byte	$17
	ora	$1D5F
	ora	($0E), y
	.byte	$5F
	ora	$1B11, x
	asl	$5F0E
	.byte	$12
	ora	$160E, x
	.byte	$1C
	pha
	.byte	$5F
	ora	$E11, x
	.byte	$17
	.byte	$5F
	.byte	$0F
	.byte	$12
	bpl	LB530
	ora	$404C, x
	.byte	$FC
	plp
	and	($0C, x)
	asl	$1515
	asl	$1D17
	.byte	$5F
LB52D:	asl	$18, x
	.byte	$1F
LB530:	asl	$FC4C
	bvc	LB52D
	.byte	$4B
	sbc	$1137, x
	.byte	$12
	.byte	$1C
	.byte	$5F
	.byte	$12
	.byte	$1C
	.byte	$5F
	rol	a
	jsr	$E0A
	ora	$12, x
	.byte	$17
	.byte	$47
	sbc	$172E, x
	clc
	jsr	$1D5F
	ora	($0A), y
	ora	$1D5F, x
	ora	($18), y
	asl	$115F, x
	asl	a
	ora	$5F11, x
	.byte	$1B
	asl	$0C0A
	ora	($0E), y
	ora	$1D5F
	ora	($0E), y
	.byte	$5F
	.byte	$0F
	.byte	$12
	.byte	$17
	asl	a
	ora	$5F, x
	ora	$0E, x
	.byte	$1F
	asl	$5215
	.byte	$FB
	.byte	$FC
	.byte	$37
	ora	($18), y
	asl	$A5F, x
	.byte	$1B
	ora	$A5F, x
	.byte	$1C
	ora	$0E, x
	asl	$4719
	.byte	$FC
	rts

	sbc	$1137, x
	clc
	asl	$A5F, x
	.byte	$1B
	ora	$1C5F, x
	ora	$1512, x
	ora	$5F, x
	asl	a
	.byte	$1C
	ora	$0E, x
	asl	$4719
	.byte	$FC
	rts

	sbc	$5FF8, x
	asl	a
	jsr	$140A
	asl	$471C
	.byte	$FC
	.byte	$37
	ora	($0E), y
	.byte	$5F
	.byte	$F4
	.byte	$5F
	ora	($0A), y
	ora	$5F11, x
	.byte	$1B
	asl	$180C
	.byte	$1F
	asl	$0E1B
	ora	$FC47
	bit	$5F1D
	.byte	$12
	.byte	$1C
	.byte	$5F
	ora	$0D18
	bpl	LB5DF
	.byte	$17
	bpl	LB61C
	.byte	$FC
	.byte	$37
	ora	($0E), y
	.byte	$1B
	asl	$125F
	.byte	$1C
	.byte	$5F
	.byte	$17
	clc
	.byte	$5F
	.byte	$0D
	clc
LB5DF:	clc
	.byte	$1B
	.byte	$5F
	ora	($0E), y
	.byte	$1B
	asl	$FC47
	.byte	$37
	ora	($18), y
	asl	$115F, x
	asl	a
	.byte	$1C
	ora	$175F, x
	clc
	ora	$A5F, x
	.byte	$5F
	.byte	$14
	asl	$5F22
	ora	$5F18, x
	asl	$0E1C, x
	.byte	$47
	.byte	$FC
	bvc	LB62D
	asl	$1D0A
	ora	($5F), y
	.byte	$1C
	ora	($18), y
	asl	$0D15, x
	.byte	$5F
	.byte	$17
	clc
	ora	$115F, x
	asl	a
	.byte	$1F
	asl	$1D5F
LB61C:	asl	a
	.byte	$14
	asl	$5F17
	ora	$E11, x
	asl	$5F48
	sed
	.byte	$52
	.byte	$FB
	sbc	$2C50, x
LB62D:	.byte	$5F
	jsr	$1512
	ora	$5F, x
	bpl	LB647
	.byte	$1F
	asl	$1D5F
	ora	($0E), y
	asl	$A5F
	.byte	$17
	clc
	ora	$E11, x
	.byte	$1B
	.byte	$5F
	.byte	$0C
	.byte	$11
LB647:	asl	a
	.byte	$17
	.byte	$0C
	asl	$FB52
	.byte	$FC
	.byte	$37
	ora	($22), y
	.byte	$5F
	ora	$2018, y
	asl	$5F1B
	.byte	$12
	.byte	$17
	.byte	$0C
	.byte	$1B
	asl	$1C0A
	asl	$5F1C
	.byte	$0B
	.byte	$22
	.byte	$5F
	sbc	$47, x
	.byte	$FC
	.byte	$37
	ora	($22), y
	.byte	$5F
	and	$0E, x
	.byte	$1C
	ora	$1718, y
	.byte	$1C
	asl	$365F
	ora	$E0E, y
	ora	$125F
	.byte	$17
	.byte	$0C
	.byte	$1B
	asl	$1C0A
	asl	$5F1C
	.byte	$0B
	.byte	$22
	.byte	$5F
	sbc	$47, x
	.byte	$FC
	.byte	$FB
	.byte	$5F
	.byte	$37
	ora	($22), y
	.byte	$5F
	bmi	LB69D
	and	($12, x)
	asl	$1E, x
	asl	$5F, x
	.byte	$2B
	.byte	$12
	.byte	$1D
	.byte	$F0
LB69D:	.byte	$5F
	.byte	$12
	.byte	$17
	.byte	$0C
	.byte	$1B
	asl	$1C0A
	asl	$0B5F
	.byte	$22
	.byte	$5F
	sbc	$47, x
	.byte	$FC
	.byte	$FB
	.byte	$5F
	.byte	$37
	ora	($22), y
	.byte	$5F
	bmi	LB6BF
	and	($12, x)
	asl	$1E, x
	asl	$5F, x
	bmi	LB6C7
	bpl	LB6D1
LB6BF:	.byte	$0C
	beq	LB721
	.byte	$12
	.byte	$17
	.byte	$0C
	.byte	$1B
	.byte	$0E
LB6C7:	asl	a
	.byte	$1C
	asl	$0B5F
	.byte	$22
	.byte	$5F
	sbc	$47, x
	.byte	$FC
LB6D1:	bvc	LB70A
	clc
	.byte	$5F
	.byte	$1B
	asl	$0C0A
	ora	($5F), y
	ora	$E11, x
	.byte	$5F
	.byte	$17
	asl	$1D21
	.byte	$5F
	ora	$0E, x
	.byte	$1F
	asl	$4815
	.byte	$5F
	ora	$2211, x
	.byte	$5F
	plp
	and	($19, x)
	asl	$121B
	asl	$0C17
	asl	$5FF0
	asl	$1E, x
	.byte	$1C
	ora	$125F, x
	.byte	$17
	.byte	$0C
	.byte	$1B
	asl	$1C0A
	asl	$0B5F
LB70A:	.byte	$22
	.byte	$5F
	sbc	$52, x
	.byte	$FB
	.byte	$FC
	bvc	LB743
	clc
	jsr	$5F48
	bpl	LB730
	pha
	.byte	$5F
	sed
	jmp	$FC40

	bvc	LB757
	.byte	$11
LB721:	clc
	asl	$115F, x
	asl	a
	.byte	$1C
	ora	$0F5F, x
	asl	a
	.byte	$12
	ora	$0E, x
	.byte	$0D
	.byte	$5F
LB730:	asl	a
	.byte	$17
	ora	$1D5F
	ora	($18), y
	asl	$A5F, x
	.byte	$1B
	ora	$0C5F, x
	asl	$1C1B, x
	.byte	$0E
	.byte	$0D
LB743:	.byte	$52
	.byte	$FB
	sbc	$2F50, x
	asl	$1F0A
	asl	$A5F
	ora	$185F, x
	.byte	$17
	.byte	$0C
	asl	$404C
	.byte	$FC
LB757:	bvc	LB79E
	eor	$40
	.byte	$FC
	bvc	LB793
	asl	$150A
	ora	$22, x
	.byte	$4B
	rti

	sbc	$50FC, x
	bit	$A5F
	asl	$5F, x
	bpl	LB784
	asl	a
	ora	$1D5F
	ora	($18), y
	asl	$115F, x
	asl	a
	.byte	$1C
	ora	$1B5F, x
	asl	$1E1D
	.byte	$1B
	.byte	$17
	.byte	$0E
	.byte	$0D
LB784:	.byte	$47
	sbc	$1524, x
	ora	$5F, x
	clc
	asl	$5F1B, x
	ora	($18), y
	ora	$1C0E, y
LB793:	.byte	$5F
	asl	a
	.byte	$1B
	asl	$1B5F
	.byte	$12
	ora	$1712
	.byte	$10
LB79E:	.byte	$5F
	clc
	.byte	$17
	.byte	$5F
	ora	$E11, x
	asl	$FB52
	.byte	$FC
	bvc	LB7E1
	asl	$5F0E
	asl	$0E, x
	.byte	$5F
	asl	a
	bpl	LB7BE
	.byte	$12
	.byte	$17
	.byte	$5F
	jsr	$E11
	.byte	$17
	.byte	$5F
	.byte	$1D
	.byte	$11
LB7BE:	.byte	$22
	.byte	$5F
	ora	$0E, x
	.byte	$1F
	asl	$5F15
	ora	($0A), y
	.byte	$1C
	.byte	$5F
	.byte	$12
	.byte	$17
	.byte	$0C
	.byte	$1B
	asl	$1C0A
	asl	$520D
	.byte	$FB
	.byte	$FC
	.byte	$37
	ora	($0E), y
	.byte	$5F
	.byte	$27
	.byte	$1B
	asl	a
	bpl	LB7F7
	.byte	$17
	.byte	$15
LB7E1:	clc
	.byte	$1B
	ora	$1B5F
	asl	$0E1F
	asl	a
	ora	$0E, x
	ora	$115F
	.byte	$12
	.byte	$1C
	.byte	$5F
	ora	$1E1B, x
	.byte	$0E
	.byte	$5F
LB7F7:	.byte	$1C
	asl	$0F15
	jmp	$FBFC

	.byte	$37
	ora	($18), y
	asl	$115F, x
	asl	a
	.byte	$1C
	ora	$0F5F, x
	clc
	asl	$0D17, x
	.byte	$5F
	ora	$E11, x
	.byte	$5F
	and	$0A
	ora	$15, x
	.byte	$5F
	clc
	.byte	$0F
	.byte	$5F
	.byte	$2F
	.byte	$12
	bpl	LB82F
	ora	$FB47, x
	sbc	$0A35, x
	ora	$0A12
	.byte	$17
	.byte	$0C
	asl	$1C5F
	ora	$0E1B, x
LB82F:	asl	a
	asl	$1C, x
	.byte	$5F
	.byte	$0F
	clc
	.byte	$1B
	ora	$5F11, x
	asl	a
	.byte	$1C
	.byte	$5F
	ora	$2211, x
	.byte	$5F
	ora	($0A), y
	.byte	$17
	ora	$5F1C
	ora	$1E18, x
	.byte	$0C
	ora	($5F), y
	ora	$E11, x
	.byte	$5F
	clc
	.byte	$0B
	.byte	$13
	asl	$1D0C
	.byte	$5F
	asl	a
	.byte	$17
	ora	$115F
	clc
	ora	$0D, x
	.byte	$5F
	.byte	$12
	ora	$A5F, x
	ora	$18, x
	.byte	$0F
	ora	$FB47, x
	sbc	$0C24, x
	.byte	$1B
	clc
	.byte	$1C
	.byte	$1C
	.byte	$5F
	ora	$E11, x
	.byte	$5F
	ora	$0A, x
	.byte	$17
	ora	$1C5F
	ora	$0E1B, y
	asl	a
	ora	$5F1C
	ora	$E11, x
	.byte	$5F
	.byte	$0B
	.byte	$1B
	.byte	$12
	ora	$15, x
	.byte	$12
	asl	a
	.byte	$17
	.byte	$0C
	asl	$1E5F
	.byte	$17
	ora	$1512, x
	.byte	$5F
	asl	a
	ora	$15, x
	.byte	$5F
	.byte	$1C
	ora	($0A), y
	ora	$2018
	.byte	$1C
	.byte	$5F
	asl	a
	.byte	$1B
	asl	$0B5F
	asl	a
	.byte	$17
	.byte	$12
	.byte	$1C
	ora	($0E), y
	ora	$A5F
	.byte	$17
	ora	$195F
	asl	$0C0A
	asl	$125F
	.byte	$1C
	.byte	$5F
	.byte	$1B
	asl	$1D1C
	clc
	.byte	$1B
	asl	$470D
	.byte	$FC
	bvc	LB901
	ora	($0E), y
	.byte	$5F
	ora	$0E, x
	bpl	LB8DF
	.byte	$17
	ora	$5F1C
	ora	($0A), y
	.byte	$1F
	asl	$195F
	.byte	$1B
	clc
	.byte	$1F
	.byte	$0E
LB8DF:	.byte	$17
	.byte	$5F
	ora	$1E1B, x
	asl	$FB52
	sbc	$3750, x
	ora	($18), y
	asl	$A5F, x
	.byte	$1B
	ora	$125F, x
	.byte	$17
	ora	$E0E
	ora	$185F
	.byte	$0F
	.byte	$5F
	ora	$E11, x
	.byte	$5F
	.byte	$15
LB901:	.byte	$12
	.byte	$17
	asl	$185F
	.byte	$0F
	.byte	$5F
	plp
	.byte	$1B
	ora	$121B
	.byte	$0C
	.byte	$14
	.byte	$52
	.byte	$FB
	sbc	$2C50, x
	ora	$125F, x
	.byte	$1C
	.byte	$5F
	ora	$2211, x
	.byte	$5F
	.byte	$1B
	.byte	$12
	bpl	LB932
	ora	$1D5F, x
	clc
	.byte	$5F
	.byte	$1B
	asl	$0E15, x
	.byte	$5F
	clc
	.byte	$1F
	asl	$5F1B
	.byte	$1D
	.byte	$11
LB932:	.byte	$12
	.byte	$1C
	.byte	$5F
	ora	$0A, x
	.byte	$17
	ora	$FB52
	sbc	$3A50, x
	.byte	$12
	ora	$15, x
	.byte	$5F
	ora	$1811, x
	asl	$1D5F, x
	asl	a
	.byte	$14
	asl	$165F
	.byte	$22
	.byte	$5F
	ora	$0A15, y
	.byte	$0C
	asl	$404B
	.byte	$FB
	sbc	$5FF8, x
	ora	$1811, x
	asl	$1110, x
	ora	$0C5F, x
	asl	a
	.byte	$1B
	asl	$1E0F
	ora	$15, x
	.byte	$22
	.byte	$5F
	.byte	$0B
	asl	$180F
	.byte	$1B
	asl	$A5F
	.byte	$17
	.byte	$1C
	jsr	$1B0E
	.byte	$12
	.byte	$17
	bpl	LB9C4
	.byte	$FB
	sbc	$2C50, x
	.byte	$5F
	.byte	$0C
	asl	a
	.byte	$17
	.byte	$17
	clc
	ora	$4048, x
	sbc	$0A1C, x
	.byte	$12
	ora	$F85F
	.byte	$47
	.byte	$FB
	sbc	$2C50, x
	.byte	$0F
	.byte	$5F
	asl	$0E1F
	.byte	$1B
	.byte	$5F
	bit	$A5F
	asl	$5F, x
	ora	$5F18, x
	.byte	$1B
	asl	$0E15, x
	.byte	$5F
	asl	a
	.byte	$5F
	.byte	$0C
	clc
	asl	$1D17, x
	.byte	$1B
	.byte	$22
	pha
	.byte	$5F
	.byte	$12
	ora	$165F, x
	asl	$1D1C, x
	.byte	$5F
	.byte	$0B
	asl	$A5F
	.byte	$5F
	ora	$0A, x
LB9C4:	.byte	$17
	ora	$1D5F
	ora	($0A), y
	ora	$2C5F, x
	.byte	$5F
	asl	$22, x
	.byte	$1C
	asl	$0F15
	.byte	$5F
	.byte	$0F
	.byte	$12
	.byte	$17
	ora	$FC52
	.byte	$FB
	rol	a
	jsr	$E0A
	ora	$12, x
	.byte	$17
	.byte	$5F
	.byte	$1C
	asl	a
	.byte	$12
	ora	$FD44
	bvc	LBA1F
	ora	$0E, x
	asl	a
	.byte	$1C
	asl	$5F48
	jsr	$120A
	ora	$FD52, x
	.byte	$FC
	bvc	LBA28
	.byte	$5F
	jsr	$1C12
	ora	($5F), y
	ora	$5F18, x
	bpl	LBA1F
	.byte	$5F
	jsr	$1D12
	ora	($5F), y
	ora	$E11, x
	asl	$185F
	.byte	$17
	.byte	$5F
	ora	$2211, x
	.byte	$5F
	.byte	$13
	clc
	asl	$171B, x
	.byte	$0E
LBA1F:	.byte	$22
	.byte	$52
	.byte	$FB
	.byte	$FC
	bvc	LBA55
	asl	a
	.byte	$22
	.byte	$5F
LBA28:	bit	$1D5F
	.byte	$1B
	asl	a
	.byte	$1F
	asl	$5F15
	asl	a
	.byte	$1C
	.byte	$5F
	ora	$2211, x
	.byte	$5F
	.byte	$0C
	clc
	asl	$19, x
	asl	a
	.byte	$17
	.byte	$12
	clc
	.byte	$17
	.byte	$4B
	rti

	sbc	$50FC, x
	.byte	$2B
	asl	$1B1B, x
	asl	a
	ora	($4C), y
	sbc	$1E2B, x
	.byte	$1B
	.byte	$1B
	asl	a
	ora	($4C), y
LBA55:	sbc	$182F, x
	.byte	$17
	bpl	LBABA
	ora	$12, x
	.byte	$1F
	asl	$F85F
	jmp	$FD40

	.byte	$FC
	bvc	LBA9E
	ora	($18), y
	asl	$115F, x
	asl	a
	.byte	$1C
	ora	$0B5F, x
	.byte	$1B
	clc
	asl	$1110, x
	ora	$1E5F, x
	.byte	$1C
	.byte	$5F
	ora	$A0E, y
	.byte	$0C
	asl	$5F48
	asl	a
	bpl	LBA8F
	.byte	$12
	.byte	$17
	.byte	$52
	.byte	$FC
	bvc	LBAB1
	clc
	asl	$0E, x
	.byte	$5F
LBA8F:	.byte	$17
	clc
	jsr	$5F48
	rol	$1712
	bpl	LBAF8
	.byte	$2F
	clc
	.byte	$1B
	.byte	$12
	.byte	$14
LBA9E:	.byte	$5F
	asl	a
	jsr	$120A
	ora	$521C, x
	.byte	$FC
	.byte	$FB
	sbc	$FD60, x
	rts

	sbc	$FD60, x
	rts

	.byte	$FD
LBAB1:	rts

	sbc	$FD60, x
	bit	$17
	ora	$1D5F
LBABA:	ora	($1E), y
	.byte	$1C
	.byte	$5F
	ora	$E11, x
	.byte	$5F
	ora	$150A, x
	asl	$0C5F
	clc
	asl	$0E, x
	.byte	$1C
	.byte	$5F
	ora	$5F18, x
	asl	a
	.byte	$17
	.byte	$5F
	asl	$0D17
	eor	$45
	sbc	$171E, x
	ora	$0E, x
	.byte	$1C
	.byte	$1C
	.byte	$5F
	ora	$E11, x
	.byte	$5F
	ora	$0A1B
	bpl	LBB01
	.byte	$17
	.byte	$1C
	.byte	$5F
	.byte	$1B
	asl	$1E1D
	.byte	$1B
	.byte	$17
	.byte	$5F
	asl	a
	bpl	LBB00
	.byte	$12
	.byte	$17
LBAF8:	.byte	$47
	sbc	$FD60, x
	rts

	sbc	$FD60, x
LBB00:	.byte	$FC
LBB01:	bvc	LBB3D
	.byte	$12
	ora	$15, x
	.byte	$5F
	ora	$1811, x
	asl	$1D5F, x
	asl	$1515
	.byte	$5F
	asl	$0E, x
	.byte	$5F
	.byte	$17
	clc
	jsr	$185F
	.byte	$0F
	.byte	$5F
	ora	$2211, x
	.byte	$5F
	ora	$E0E
	ora	$5F1C
	.byte	$1C
	clc
	.byte	$5F
	ora	$E11, x
	.byte	$22
	.byte	$5F
	jsr	$1718
	.byte	$53
	ora	$0B5F, x
	asl	$0F5F
	clc
	.byte	$1B
	bpl	LBB53
	.byte	$1D
	.byte	$1D
LBB3D:	asl	$4B17
	rti

	sbc	$50FC, x
	.byte	$37
	ora	($22), y
	.byte	$5F
	ora	$E0E
	ora	$5F1C
	ora	($0A), y
	.byte	$1F
	.byte	$0E
	.byte	$5F
LBB53:	.byte	$0B
	asl	$170E
	.byte	$5F
	.byte	$1B
	asl	$180C
	.byte	$1B
	ora	$D0E
	.byte	$5F
	clc
	.byte	$17
	.byte	$5F
	ora	$E11, x
	.byte	$5F
	bit	$1916
	asl	$121B
	asl	a
	ora	$5F, x
	rol	$0C, x
	.byte	$1B
	clc
	ora	$15, x
	.byte	$1C
	.byte	$5F
	clc
	.byte	$0F
	.byte	$5F
	.byte	$2B
	clc
	.byte	$17
	clc
	.byte	$1B
	.byte	$52
	.byte	$FB
	.byte	$FC
	bvc	LBBAD
	clc
	.byte	$1C
	ora	$1D5F, x
	ora	($18), y
	asl	$205F, x
	.byte	$12
	.byte	$1C
	ora	($5F), y
	ora	$5F18, x
	.byte	$0C
	clc
	.byte	$17
	ora	$1712, x
	asl	$5F0E, x
	ora	$2211, x
	.byte	$5F
	.byte	$1A
	asl	$1C0E, x
	ora	$404B, x
	.byte	$FD
	.byte	$FC
LBBAD:	bvc	LBBE4
	asl	$1D1C
	.byte	$5F
	ora	$E11, x
	.byte	$17
	.byte	$5F
	.byte	$0F
	clc
	.byte	$1B
	.byte	$5F
	asl	a
	jsr	$1211
	ora	$0E, x
	.byte	$52
	.byte	$FB
	.byte	$FC
	bvc	LBBF1
	clc
	.byte	$5F
	sed
	jmp	$FC40

	.byte	$33
	ora	$0E, x
	asl	a
	.byte	$1C
	asl	$195F
	asl	$111C, x
	.byte	$5F
	and	$28, x
	rol	$28, x
	.byte	$37
	pha
	.byte	$5F
	ora	($18), y
	ora	$0D, x
LBBE4:	.byte	$5F
	.byte	$12
	ora	$125F, x
	.byte	$17
	pha
	.byte	$5F
	ora	$E11, x
	.byte	$17
	.byte	$5F
LBBF1:	ora	$1B1E, x
	.byte	$17
	.byte	$5F
	clc
	.byte	$0F
	.byte	$0F
	.byte	$5F
	ora	$E11, x
	.byte	$5F
	.byte	$33
	.byte	$32
	.byte	$3A
	plp
	and	$47, x
	.byte	$FB
	sbc	$0F2C, x
	.byte	$5F
	.byte	$22
	clc
	asl	$1D5F, x
	asl	$171B, x
	.byte	$5F
	ora	$E11, x
	.byte	$5F
	ora	$2018, y
	asl	$5F1B
	clc
	.byte	$0F
	.byte	$0F
	.byte	$5F
	.byte	$0F
	.byte	$12
	.byte	$1B
	.byte	$1C
	ora	$5F48, x
	ora	$E11, x
	.byte	$5F
	bit	$1916
	asl	$121B
	asl	a
	ora	$5F, x
	rol	$0C, x
	.byte	$1B
	clc
	ora	$15, x
	.byte	$5F
	clc
	.byte	$0F
	.byte	$5F
	.byte	$2B
	clc
	.byte	$17
	clc
	.byte	$1B
	.byte	$5F
	.byte	$0C
	clc
	.byte	$17
	ora	$120A, x
	.byte	$17
	.byte	$12
	.byte	$17
	bpl	LBCAE
	.byte	$22
	clc
	asl	$5F1B, x
	ora	$E0E
	ora	$5F1C
	asl	$0A, x
	.byte	$22
	.byte	$5F
	.byte	$0B
	asl	$155F
	clc
	.byte	$1C
	ora	$FD47, x
	rts

	sbc	$FD60, x
	.byte	$FC
	sec
	.byte	$17
	.byte	$0F
	clc
	.byte	$1B
	ora	$171E, x
	asl	a
	ora	$150E, x
	.byte	$22
	pha
	.byte	$5F
	and	($32), y
	.byte	$5F
	ora	$E0E
	ora	$5F1C
	jsr	$1B0E
	asl	$1B5F
	asl	$180C
	.byte	$1B
	ora	$D0E
	.byte	$5F
	clc
	.byte	$17
	.byte	$5F
	bit	$1916
	asl	$121B
	asl	a
	ora	$5F, x
	rol	$0C, x
	.byte	$1B
	clc
	ora	$15, x
	.byte	$5F
	.byte	$17
	asl	$0B16, x
	asl	$5F1B
	sbc	$47, x
LBCAE:	.byte	$FB
	.byte	$FC
	jsr	LFF74
	lda	LBDEF
	sta	$0C
	lda	LBDF0
	sta	$0D
	lda	#$00
	sta	$3C
	jsr	LC63D
	jsr	LBDE0
	lda	#$01
	brk
	.byte	$04
	.byte	$17
	jsr	LFF74
LBCCF:	jsr	LC608
	lda	$47
	and	#$0C
	bne	LBCE1
	lda	$602E
	cmp	#$FC
	beq	LBCE1
	bne	LBCCF
LBCE1:	jsr	LFF74
	lda	LBDFD
	sta	$0C
	lda	LBDFE
	sta	$0D
	lda	#$00
	sta	$3C
	jsr	LC63D
	lda	#$37
	sta	$08
	lda	#$17
	sta	$0A
	lda	#$3F
	sta	$0B
	jsr	LC690
	jsr	LBDE0
	jsr	LFF74
	ldx	#$00
LBD0C:	lda	LBE0B, x
	sta	$0200, x
	inx
	cpx	#$24
	bne	LBD0C
	lda	#$00
	sta	$91
	lda	#$FF
	sta	$93
	lda	#$0B
	sta	$8E
	lda	#$0C
	sta	$90
	lda	#$10
	sta	$8F
	lda	#$17
	sta	$92
	lda	#$01
	sta	$E5
LBD33:	jsr	LFF74
	ldx	#$00
	lda	$93
	cmp	#$FF
	beq	LBD8D
	ldy	$93
	lda	LBE2F, y
	sta	$99
	lda	LBE30, y
	sta	$9A
	ldy	#$00
LBD4C:	lda	($99), y
	cmp	#$FF
	beq	LBD76
	sta	$0240, x
	iny
	inx
	lda	($99), y
	sta	$0240, x
	iny
	inx
	lda	($99), y
	and	#$C0
	ora	#$01
	sta	$0240, x
	inx
	lda	($99), y
	and	#$3F
	clc
	adc	#$B4
	sta	$0240, x
	iny
	inx
	bne	LBD4C
LBD76:	lda	$91
	cmp	#$80
	bcs	LBD7F
	lsr	a
	bcc	LBD8D
LBD7F:	inc	$93
	inc	$93
	lda	$93
	cmp	#$18
	bne	LBD8D
	lda	#$FF
	sta	$93
LBD8D:	lda	#$F0
LBD8F:	sta	$0240, x
	inx
	cpx	#$C0
	bne	LBD8F
	inc	$91
	lda	$91
	cmp	#$20
	beq	LBDA7
	cmp	#$A0
	beq	LBDA7
	cmp	#$C0
	bne	LBDAB
LBDA7:	lda	#$00
	sta	$93
LBDAB:	lda	$47
	pha
	jsr	LC608
	pla
	bne	LBDDD
	lda	$47
	and	#$08
	beq	LBDDD
	lda	LBEC3
	sta	$0C
	lda	LBEC4
	sta	$0D
	lda	#$00
	sta	$3C
	jsr	LC63D
	lda	LBEC3
	sta	$0C
	lda	LBEC4
	sta	$0D
	lda	#$00
	sta	$3C
	jsr	LC632
	rts

LBDDD:	jmp	LBD33

LBDE0:	lda	#$0F
	sta	$08
	lda	#$00
	sta	$0A
	lda	#$3F
	sta	$0B
	jmp	LC690

LBDEF:	.byte	$F1
LBDF0:	lda	$1030, x
	brk
	.byte	$27
	.byte	$37
	.byte	$17
	.byte	$0F
	.byte	$0F
	.byte	$0F
	.byte	$0F
	.byte	$0F
	.byte	$0F
LBDFD:	.byte	$FF
LBDFE:	lda	$1030, x
	brk
	.byte	$27
	.byte	$37
	.byte	$17
	.byte	$0F
	.byte	$27
	.byte	$27
	.byte	$0F
	bit	$24
LBE0B:	.byte	$3F
	asl	$00, x
	.byte	$27
	.byte	$3F
	.byte	$17
	brk
	.byte	$2F
	.byte	$3F
	clc
	brk
	.byte	$37
	.byte	$3C
	ora	$3F00, y
	.byte	$47
	.byte	$1A
	brk
	.byte	$2F
	.byte	$47
	.byte	$1B
	brk
	.byte	$37
	.byte	$44
	.byte	$1C
	brk
	.byte	$3F
	.byte	$4F
	ora	$3000, x
	.byte	$4F
	asl	$3800, x
LBE2F:	.byte	$47
LBE30:	ldx	LBE4E, y
	lsr	$64BE
	ldx	LBE64, y
	.byte	$8F
	ldx	LBE8F, y
	.byte	$8F
	ldx	LBE64, y
	.byte	$64
	ldx	LBE4E, y
	.byte	$47
	ldx	a:$4F, y
	ora	a:$57
	.byte	$CD
	.byte	$FF
LBE4E:	.byte	$47
	ora	($12, x)
	.byte	$4F
	.byte	$02
	ora	#$4F
	.byte	$03
	ora	($57), y
	.byte	$03
	cmp	#$57
	.byte	$02
	cmp	($5F), y
	ora	($C8, x)
	.byte	$5B
	ora	($8F, x)
	.byte	$FF
LBE64:	.byte	$3F
	ora	$15
	.byte	$47
	.byte	$07
	.byte	$0B
	.byte	$47
	asl	$15
	.byte	$4F
	asl	a
	.byte	$03
	.byte	$4F
	.byte	$0B
	.byte	$0B
	.byte	$4F
	.byte	$0C
	.byte	$13
	.byte	$4F
	ora	#$1A
	.byte	$57
	.byte	$0C
	.byte	$C7
	.byte	$57
	.byte	$0B
	.byte	$CF
LBE7F:	.byte	$57
	asl	a
	.byte	$D7
	.byte	$5F
	asl	$C5
	.byte	$5F
	.byte	$07
	.byte	$CF
	adc	($08, x)
	.byte	$0F
	.byte	$67
	ora	$C5
	.byte	$FF
LBE8F:	.byte	$37
	ora	($1A, x)
	.byte	$3F
	ora	$47D9
	.byte	$14
	iny
	.byte	$47
	.byte	$0F
	.byte	$D4
	.byte	$4B
	ora	$00, x
	.byte	$4F
	.byte	$12
	iny
	.byte	$4F
	ora	($D0), y
	.byte	$4F
	bpl	LBE7F
	.byte	$57
	bpl	LBEAC
	.byte	$57
	.byte	$11
LBEAC:	asl	a
	.byte	$57
	.byte	$12
	.byte	$12
	.byte	$5B
	.byte	$13
	.byte	$1A
	.byte	$5C
	.byte	$04
	sbc	($5F, x)
	.byte	$0F
	asl	$5F
	.byte	$14
	.byte	$12
	.byte	$67
	ora	$6F01
	asl	$01
	.byte	$FF
LBEC3:	.byte	$C5
LBEC4:	ldx	$E0E, y
	asl	$E0E
	asl	$E0E
	asl	$E0E
	.byte	$0E
; ------------------------------------------------------------
	.res	263, $FF
; ------------------------------------------------------------
reset_banked:
	sei
	inc	LBFDF
	jmp	start_banked
; ------------------------------------------------------------
LBFDF:	.byte	$80
; ------------------------------------------------------------
LBFE0:	.byte	"DRAGON WARRIOR  "
; ------------------------------------------------------------
	.byte	$56
	.byte	$DE
	.byte	$30
	.byte	$70
	.byte	$01
	.byte	$04
	.byte	$01
	.byte	$0F
	.byte	$07
	.byte	$00
; ------------------------------------------------------------
	.addr	reset_banked
	.addr	reset_banked
	.addr	reset_banked
