#!/bin/sh

# magic number
printf "NES\x1A"

# 64kb PRG (4 banks)
printf "\x04"

# 16kb CHR (2 banks)
printf "\x02"

# metadata - mapper 01 (MMC1), horizontal mirroring, BURAM
printf "\x12\x00"

# padding
printf "\0\0\0\0\0\0\0\0"
