.segment	"PRG1CODE"

; ------------------------------------------------------------
	bit	$AF
	cpy	$AB
	sei
	sta	($5E, x)
	sta	($A0, x)
	sta	($62, x)
	.byte	$93
	brk
	brk
	brk
	brk
	.byte	$4F
	sta	$00, y
	sta	($99, x)
	cpx	$99
	adc	($99, x)
	ldy	$99, x
	txs
	.byte	$93
	brk
	brk
	sty	$A1, x
	.byte	$02
	ldx	LB51D
	brk
	brk
; ------------------------------------------------------------
	.export	L8028
L8028:
	pha
	txa
	pha
	tya
	pha
	ldx	#$06
	ldy	#$04
	lda	$FA
	beq	L805F
	lda	$FD
	pha
	lda	#$00
	sta	$FD
	jsr	L80CB
	tax
	pla
	sta	$FD
	txa
	bne	L805F
	lda	#$05
	sta	$4015
L804B:	lda	#$0F
	sta	$4015
	lda	$FF
	sta	$4004
	lda	#$08
	sta	$4005
	lda	#$30
	sta	$400C
L805F:	lda	$FC
	clc
	adc	$FB
	sta	$FC
	bcc	L8087
	sbc	#$96
	sta	$FC
	ldx	#$04
	ldy	#$08
	jsr	L80CB
	ldx	#$02
	ldy	#$04
	lda	$FA
	beq	L807D
	ldy	#$10
L807D:	jsr	L80CB
L8080:	ldx	#$00
	ldy	#$00
	jsr	L80CB
L8087:	ldy	#$00
	lda	($E6), y
	sta	$602E
	pla
	tay
	pla
	tax
	pla
	rts

L8094:	lda	$EE, x
	sta	$E6, x
	lda	$EF, x
	sta	$E7, x
	bne	L80D3
L809E:	clc
	adc	$FD
	asl	a
	stx	$FE
	tax
	lda	L8205, x
	sta	$4002, y
	lda	L8206, x
	sta	$4003, y
	ldx	$FE
	cpx	#$06
	beq	L80D3
	lda	$F5, x
	beq	L80D3
	bne	L80FC
L80BD:	jsr	L8155
	sta	$F5, x
	jmp	L80D3

L80C5:	lda	#$00
	sta	$F5, x
	beq	L80D3
L80CB:	lda	$F4, x
	beq	L80FE
	dec	$F4, x
	bne	L80FE
L80D3:	jsr	L8155
	cmp	#$FE
	beq	L8107
	bcs	L80FF
	cmp	#$FC
	beq	L80D3
	bcs	L8094
	cmp	#$FA
	beq	L814C
	bcs	L8120
	cmp	#$F8
	beq	L812F
	bcs	L8144
	cmp	#$F6
	beq	L80C5
	bcs	L80BD
	cmp	#$E0
	bcs	L8138
	cmp	#$80
	bcs	L809E
L80FC:	sta	$F4, x
L80FE:	rts

L80FF:	jsr	L8155
	sta	$FB
	jmp	L80D3

L8107:	jsr	L8155
	pha
	jsr	L8155
	pha
	lda	$E6, x
	sta	$EE, x
	lda	$E7, x
	sta	$EF, x
	pla
	sta	$E7, x
	pla
	sta	$E6, x
	jmp	L80D3

L8120:	jsr	L8155
	cpx	#$02
	bne	L8129
	sta	$FF
L8129:	sta	$4000, y
	jmp	L80D3

L812F:	.byte	$20
	.byte	$55
L8131:	sta	($8D, x)
	.byte	$0C
	rti

	jmp	L80D3

L8138:	and	#$0F
	sta	$400E
	lda	#$08
	sta	$400F
	bne	L80D3
L8144:	jsr	L8155
	sta	$FD
	jmp	L80D3

L814C:	jsr	L8155
	sta	$4001, y
	jmp	L80D3

L8155:	lda	($E6, x)
L8157:	inc	$E6, x
	bne	L815D
	inc	$E7, x
L815D:	rts

L815E:	jsr	LFF74
	lda	#$FC
	ldx	#$00
	cmp	($E6, x)
	beq	L8175
	ldx	#$06
	cmp	($E6, x)
	beq	L8175
	ldx	#$04
	cmp	($E6, x)
	bne	L815E
L8175:	jmp	L8157

	jsr	LFF74
	lda	#$00
	sta	$4010
	sta	$4017
	sta	$4015
	sta	$F4
	sta	$F6
	sta	$F8
	sta	$FA
	lda	#$0F
	sta	$4015
	lda	#$FF
	sta	$FB
	lda	#$08
	sta	$4001
	sta	$4005
	rts

	ldx	#$FF
	stx	$6005
	tax
	bmi	L81E2
	asl	a
	sta	$FE
	asl	a
	adc	$FE
	adc	#$04
	tay
	ldx	#$04
L81B3:	lda	L8298, y
	bne	L81C3
	lda	L8298, x
	sta	$E7, x
	lda	L8297, x
	jmp	L81C8

L81C3:	sta	$E7, x
	lda	L8297, y
L81C8:	sta	$E6, x
	lda	#$01
	sta	$F4, x
	dey
	dey
	dex
	dex
	bpl	L81B3
	lda	#$00
	sta	$FD
	sta	$F5
	sta	$F7
	sta	$F9
	sta	$6005
	rts

L81E2:	asl	a
	tax
	lda	#$01
	sta	$FA
	lda	L8339, x
	sta	$EC
	lda	L833A, x
	sta	$ED
	lda	#$08
	sta	$4005
	lda	#$30
	sta	$4004
	sta	$400C
	lda	#$00
	sta	$6005
	rts

L8205:	.byte	$AD
L8206:	asl	$0E4D
	.byte	$F3
	ora	$0D9D
	jmp	$0D

	ora	$0CB8
	.byte	$74
	.byte	$0C
	.byte	$34
	.byte	$0C
	sed
	.byte	$0B
	.byte	$BF
	.byte	$0B
	.byte	$89
	.byte	$0B
	lsr	$0B, x
	rol	$0B
	sbc	$CE0A, y
	asl	a
	ldx	$0A
	.byte	$80
	asl	a
	.byte	$5C
	asl	a
	.byte	$3A
	asl	a
	.byte	$1A
	asl	a
	.byte	$FB
	ora	#$DF
	ora	#$C4
	ora	#$AB
	ora	#$93
	ora	#$7C
	ora	#$67
	ora	#$52
	ora	#$3F
	ora	#$2D
	ora	#$1C
	ora	#$0C
	ora	#$FD
	php
	.byte	$EF
	php
	sbc	($08, x)
	cmp	$08, x
	cmp	#$08
	lda	LB308, x
	php
	lda	#$08
	.byte	$9F
	php
	stx	$08, y
	stx	L8608
	php
	ror	$7708, x
	php
	bvs	L826D
	ror	a
	php
	.byte	$64
	php
	lsr	$5908, x
	php
L826D:	.byte	$54
	php
	.byte	$4F
	php
	.byte	$4B
	php
	lsr	$08
	.byte	$42
	php
	.byte	$3F
	php
	.byte	$3B
	php
	sec
	php
	.byte	$34
	php
	.byte	$31
L8280:	php
	.byte	$2F
	php
	bit	$2908
	php
	.byte	$27
	php
	and	$08
	.byte	$23
	php
	and	($08, x)
	.byte	$1F
	php
	ora	$1B08, x
	php
	.byte	$1A
	php
L8297:	.byte	$CB
L8298:	sty	$CB
	sty	$CE
	sty	$6D
	sta	L8E3D
	asl	$8F
	.byte	$D3
	sty	$00
	brk
	rol	LAA85, x
	sta	$00
	brk
	ldy	$85, x
	.byte	$2F
	.byte	$87
	brk
	brk
	ldx	#$87
	.byte	$44
	dey
	brk
	brk
	.byte	$17
	dey
	.byte	$8B
	dey
	brk
	brk
	ora	L8B89, x
	dey
	brk
	brk
	bit	$89
	.byte	$8B
L82C8:	dey
	brk
	brk
	.byte	$2B
	.byte	$89
	.byte	$8B
	dey
	brk
	brk
	.byte	$32
	.byte	$89
	.byte	$8B
	dey
	brk
	brk
	.byte	$37
	.byte	$89
	.byte	$8B
	dey
	brk
	brk
	rol	L8B89, x
	dey
	brk
	brk
	eor	$89
	.byte	$8B
	dey
	brk
	brk
	jmp	LA989

	.byte	$89
	brk
	brk
	.byte	$CF
	txa
	.byte	$62
	.byte	$8B
	inc	$8B
	.byte	$1A
	sty	L8F62
	.byte	$B2
	.byte	$90
L82FB:	rol	$3F92
	.byte	$8C
	.byte	$3E
L8300:	sty	a:$00
	brk
	brk
	brk
	brk
	txs
	sty	L8CE2
	sbc	($8C, x)
	brk
	brk
	bit	$8D
	.byte	$23
	sta	a:$00
	cpy	$EB86
	stx	$00
	brk
	.byte	$53
	stx	$7B
	stx	$AC
	stx	$4B
	sta	L8D4A
	brk
	brk
	.byte	$BF
	.byte	$89
	brk
L832A:	brk
	sbc	($8A, x)
	asl	$0787
	.byte	$87
	brk
	brk
	rol	a
	stx	$40
	stx	$00
	brk
L8339:	.byte	$6E
L833A:	.byte	$83
	.byte	$77
	.byte	$83
	.byte	$9E
	.byte	$83
	.byte	$C2
	.byte	$83
	sed
	.byte	$83
	ora	($84, x)
	asl	$84
	ora	($84), y
	jsr	$3B84
	sty	$4A
	sty	$4A
	sty	$59
	sty	$68
	sty	$71
	sty	$7A
	sty	$86
	sty	$8E
	sty	$A0
	sty	$AB
	sty	$B6
	sty	$65
	.byte	$83
	.byte	$FB
	.byte	$B3
	sed
	.byte	$3F
	sbc	$EE0C
	bmi	L836E
L836E:	sed
	.byte	$0F
	.byte	$E7
	.byte	$04
	inx
	.byte	$04
	sbc	#$04
	brk
	sed
	ora	($FB, x)
	.byte	$7F
	.byte	$FA
	.byte	$9B
	sty	$06EF
	inc	$0C06
	.byte	$FA
	sty	$06, x
	.byte	$FA
L8387:	.byte	$9B
	sed
	ora	($8C, x)
	.byte	$EF
	asl	$EE
	asl	$0C
	.byte	$FA
	sty	$06, x
	.byte	$FA
	.byte	$9B
	sed
	ora	($8C, x)
	.byte	$EF
	asl	$EE
	asl	$0C
	brk
	sed
	.byte	$3F
	inc	$ED02
	.byte	$02
	sed
	bmi	L83B3
	sed
	.byte	$3F
	sbc	$EC02
	.byte	$02
	sed
	bmi	L83BC
	sed
	.byte	$3F
	.byte	$EE
L83B3:	.byte	$02
	sbc	$F802
	bmi	L83C5
	sed
	.byte	$3F
	.byte	$ED
L83BC:	.byte	$02
	cpx	$F802
	bmi	L83C2
L83C2:	sed
	.byte	$3F
	.byte	$EE
L83C5:	.byte	$02
	sbc	$F802
	bmi	L83CE
	sed
	.byte	$3F
	.byte	$ED
L83CE:	.byte	$02
	cpx	$F802
	bmi	L83D7
	sed
	.byte	$3F
	.byte	$EE
L83D7:	.byte	$02
	sbc	$F802
	bmi	L83E0
	sed
	.byte	$3F
	.byte	$ED
L83E0:	.byte	$02
	cpx	$F802
	bmi	L83E9
	sed
	.byte	$3F
	.byte	$EE
L83E9:	.byte	$02
	sbc	$F802
	bmi	L83F2
	sed
	.byte	$3F
	.byte	$ED
L83F2:	.byte	$02
	cpx	$F802
	bmi	L83F8
L83F8:	sed
	ora	($EF, x)
	asl	$ED
	asl	$F8
	bmi	L8401
L8401:	.byte	$FB
	.byte	$89
	cmp	$06
	brk
	.byte	$FB
	.byte	$89
	ldy	$C204, x
	.byte	$04
	ldy	$C204, x
	.byte	$04
	brk
	sed
	.byte	$0F
	nop
	.byte	$02
	.byte	$EB
	.byte	$02
	cpx	$ED02
	.byte	$02
	inc	$EF02
	.byte	$02
	brk
	sed
	.byte	$0F
	inx
	.byte	$02
	sbc	#$02
	nop
	.byte	$02
	.byte	$EB
	.byte	$02
	inx
	.byte	$02
	sbc	#$02
	nop
	.byte	$02
	.byte	$EB
	.byte	$02
	nop
	.byte	$02
	sbc	#$02
	inx
	.byte	$02
	.byte	$E7
	.byte	$02
	brk
	.byte	$FB
	.byte	$43
	.byte	$B7
	.byte	$02
	clv
	.byte	$02
	ldx	$02, y
	.byte	$B7
	.byte	$02
	clv
	.byte	$02
	ldx	$02, y
	brk
	sed
	.byte	$0F
	.byte	$EF
	.byte	$02
	inc	$ED02
	.byte	$02
	cpx	$EB02
	.byte	$02
	nop
	.byte	$02
	brk
	.byte	$FB
	.byte	$43
	ldx	$02
	.byte	$A3
	.byte	$02
	.byte	$A7
	.byte	$02
	ldx	$02
	.byte	$A3
	.byte	$02
	.byte	$A7
	.byte	$02
	brk
	.byte	$FB
	.byte	$0F
	lda	LAB04
	.byte	$04
	.byte	$A7
	.byte	$04
	brk
	.byte	$FB
	.byte	$0F
	.byte	$AF
	.byte	$04
	lda	LA904
	.byte	$04
	brk
	.byte	$FB
	.byte	$8F
	sed
	brk
	.byte	$EE
	.byte	$8F
L8480:	.byte	$03
	stx	L8C03
	.byte	$03
	brk
	sed
	.byte	$32
	.byte	$FB
	brk
	lda	$08EE
	brk
	.byte	$FB
	.byte	$4F
	tya
	asl	$9A
	asl	$99
	asl	$9C
	asl	$9B
	asl	$9D
	asl	$9E
	asl	$00
	.byte	$FC
	sed
	.byte	$3F
	.byte	$EF
	.byte	$03
	inc	$ED02
	ora	($EC, x)
	.byte	$02
	brk
	.byte	$FB
	.byte	$8F
	.byte	$92
L84AE:	.byte	$03
	tya
	.byte	$03
	.byte	$93
	.byte	$03
	sta	$03, y
	.byte	$FB
	brk
	bcs	L84BC
	lda	$02
L84BC:	.byte	$B2
	.byte	$02
	.byte	$A7
	.byte	$02
	ldy	$06, x
	ldx	$02
	.byte	$B3
	.byte	$02
	tay
	.byte	$02
	lda	$06, x
	brk
	.byte	$FB
	bmi	L84CE
L84CE:	sbc	$FB00, y
	brk
	brk
	.byte	$FF
	ror	L8FFB, x
	inc	L85BA, x
	.byte	$FB
	.byte	$82
	.byte	$F7
	asl	$95
	inc	L851E, x
	.byte	$93
	inc	L851E, x
	.byte	$FB
	.byte	$87
	.byte	$F7
	.byte	$0C
	.byte	$A3
	.byte	$9F
	ldy	$9F
	lda	#$A1
	ldy	$A1
	tay
	.byte	$9C
	ldy	#$A3
	tay
	.byte	$9F
	lda	$A8
	.byte	$FB
	.byte	$82
	.byte	$F7
	asl	$8E
	inc	L852E, x
	sty	$2EFE
	sta	$FB
	.byte	$87
	.byte	$F7
	.byte	$0C
	.byte	$A3
	.byte	$9F
	ldy	$9F
	lda	#$A1
	ldy	$A9
	tay
	lda	($A0, x)
	sta	L9A9C, x
	tya
	.byte	$97
	inc	$FE, x
	cmp	$84, x
L851E:	asl	$AD
	ldy	$06AD
	tay
	.byte	$A7
	tay
	asl	$A4
	.byte	$A3
	ldy	$06
	lda	($06, x)
	.byte	$FD
L852E:	asl	$AD
	ldy	$06AD
	lda	#$A8
	lda	#$06
	ldx	$A5
	ldx	$06
	lda	($06, x)
	.byte	$FD
L853E:	inc	L85EB, x
	.byte	$FB
	clc
	.byte	$F7
	asl	$95
	asl	$A4
	.byte	$A3
	ldy	$06
	ldy	$A3
	ldy	$06
	tay
	.byte	$A7
	tay
	asl	$A4
	asl	$93
	asl	$A4
	.byte	$A3
	ldy	$06
	ldy	$A3
	.byte	$9E
	asl	$A8
	.byte	$A7
	tay
	asl	$A4
	asl	$FB
	.byte	$FF
	sta	L9C12, x
	.byte	$12
	txs
	.byte	$12
	.byte	$9B
	.byte	$12
	.byte	$9C
	rol	a
	lda	($2A, x)
	.byte	$FB
	clc
	txs
	asl	$A9
	tay
	lda	#$06
	ldx	$A5
	ldx	$06
	lda	#$A8
	lda	#$06
	ldx	$06
	tya
	asl	$A9
	tay
	lda	#$06
	ldx	$A5
	.byte	$97
	asl	$A9
	tay
	lda	#$06
	ldx	$06
	.byte	$FB
	.byte	$FF
	sta	L9C12, x
	.byte	$12
	txs
	rol	a
	.byte	$F7
	.byte	$0C
	.byte	$9C
	lda	#$A8
	ldx	$A4
	.byte	$A3
	lda	($A0, x)
	inc	L853E, x
L85AA:	.byte	$FF
	adc	$4FFB, x
	inc	L85BA, x
	inc	L85AA, x
L85B4:	inc	L85EB, x
	inc	L85B4, x
L85BA:	.byte	$F7
	.byte	$0C
	sta	$A8, x
	ldx	$A8
	ldy	$A8
	.byte	$A3
	tay
	lda	($54, x)
	stx	LA8A9
	lda	#$A6
	lda	#$A4
	lda	#$A3
	.byte	$54
	sta	$AB, x
	lda	#$AB
	tay
	.byte	$AB
	lda	$AB
	lda	#$0C
	.byte	$AB
	.byte	$0C
	lda	LAB0C
	lda	#$A8
	.byte	$0C
	ldy	$A8
	ldx	$0C
	.byte	$A7
	.byte	$0C
	tay
	.byte	$54
	.byte	$FD
L85EB:	.byte	$F7
	.byte	$0C
	.byte	$FB
	rts

	sta	$54, x
	lda	($95, x)
	tya
	.byte	$9C
	lda	($9F, x)
	sta	L9A9C, x
	ldx	$A4
	ldx	$A3
	ldx	$A1
	ldx	$A0
	.byte	$9C
	ldy	#$A3
	tay
	.byte	$9C
	.byte	$9E
L8608:	ldy	#$A1
	tay
	ldx	$A8
	lda	$A8
	lda	($A8, x)
	ldx	$A1
	tay
	lda	($A9, x)
	lda	($A8, x)
	ldx	$A4
	.byte	$9F
	tay
	.byte	$9F
	.byte	$A3
	lda	#$A1
	tax
	ldy	LA09C
	.byte	$A3
	tay
	ldx	$A4
	.byte	$A3
	sbc	$50FF, x
	.byte	$FB
	.byte	$42
	lda	#$04
	lda	#$04
	lda	#$04
	lda	#$08
	.byte	$A7
	php
	.byte	$AB
	php
	.byte	$FB
	.byte	$4F
	lda	#$18
	brk
L863F:	.byte	$FC
	.byte	$FB
	.byte	$42
	ldy	$04
	.byte	$A3
	.byte	$04
	ldx	#$04
	lda	($08, x)
	.byte	$9F
	php
	ldx	#$08
	.byte	$FB
	.byte	$4F
	lda	($18, x)
	brk
	.byte	$FF
	ror	$30FB
	asl	$FB
	.byte	$8F
	bcs	L866E
	lda	LAB06
	asl	$A9
	asl	$A8
	.byte	$0C
	ldx	$18
	ldx	LAB12
	asl	$A8
	asl	$A6
	.byte	$06
L866E:	lda	$18
	lda	$FB0C
	.byte	$BF
	bcs	L86A6
	.byte	$FB
	.byte	$8F
	.byte	$3C
	brk
	.byte	$FC
	.byte	$FB
	bmi	L8690
	.byte	$FB
	.byte	$8F
	ldy	$0C
	lda	($0C, x)
	.byte	$FB
	bmi	L8693
	.byte	$FB
	.byte	$8F
	ldy	#$0C
	sta	$FB0C, x
	bmi	L869C
L8690:	.byte	$FB
	.byte	$8F
	.byte	$A6
L8693:	.byte	$0C
	ldx	#$0C
	.byte	$FB
	bmi	L86A5
	.byte	$FB
	.byte	$8F
	.byte	$F7
L869C:	asl	$9C
	.byte	$9F
	ldx	#$9C
	lda	($A4, x)
	tay
	.byte	$A4
L86A5:	.byte	$A1
L86A6:	ldy	$A1
	asl	$FB
	.byte	$8F
	brk
	.byte	$FB
	brk
	asl	$FB
	.byte	$FF
	.byte	$F7
	.byte	$0C
	sta	LA4A1, x
	ldy	#$A3
	ldy	#$9F
	ldx	#$9F
	tya
	ldy	$98
	.byte	$F7
	asl	$9D
	lda	($A4, x)
	lda	($9D, x)
	tya
	sta	($12), y
	.byte	$FB
	brk
	brk
	.byte	$FF
	sei
	.byte	$FB
	bmi	L86E9
	.byte	$FB
	.byte	$82
	ldy	$06
	ldx	$06
	ldy	$06
	ldx	$06
	tay
	.byte	$0C
	.byte	$AB
	.byte	$0C
	ldy	$02
	tay
	.byte	$02
	.byte	$AB
	.byte	$02
	.byte	$FB
	.byte	$8F
	bcs	L872B
L86E9:	brk
	.byte	$FC
	.byte	$FB
	bmi	L8706
	.byte	$FB
	.byte	$82
	.byte	$9C
	asl	$9D
	asl	$9C
	asl	$9D
	asl	$9F
	.byte	$0C
	ldx	#$0C
	.byte	$9C
	.byte	$02
	.byte	$9F
	.byte	$02
	ldx	#$02
	.byte	$FB
	.byte	$8F
	tay
	.byte	$42
L8706:	brk
	asl	$FE
	.byte	$17
	.byte	$87
	.byte	$FB
	bmi	L870E
L870E:	.byte	$FF
	sei
	inc	L8717, x
	bcs	L8744
	brk
	.byte	$FC
L8717:	.byte	$FB
	.byte	$8F
	sty	L9307
	asl	$98
	asl	$F7
	ora	($9A, x)
	.byte	$9C
	sta	LA19F, x
	ldx	#$A4
	ldx	$A8
	.byte	$A9
L872B:	.byte	$AB
	lda	$FDAE
	.byte	$FF
	.byte	$73
	.byte	$F7
	.byte	$0C
	.byte	$FB
	.byte	$8F
	lda	($A2, x)
	ldy	$A9
	tay
	ldx	$A4
	.byte	$0C
	ldx	$A1
	ldx	#$3C
	.byte	$9F
	lda	($A2, x)
L8744:	tay
	ldx	$A4
	ldy	$A2
	.byte	$9F
	ldx	#$A1
	.byte	$0C
	ldx	#$0C
	inc	L8772, x
	.byte	$0C
	sta	L9FA1, x
	.byte	$0C
	lda	#$0C
	tay
	lda	#$A6
	tay
	inc	L8772, x
	ldx	#$A3
	ldx	$A4
	ldx	#$A1
	.byte	$9F
	.byte	$FB
	sta	$A1
	.byte	$0C
	.byte	$9F
	.byte	$0C
	sta	$FE0C, x
	and	($87), y
L8772:	ldy	$0C
	ldx	$A8
	.byte	$FB
	.byte	$82
	lda	#$F6
	.byte	$FB
	.byte	$82
	ldx	#$06
	ldx	#$06
	ldx	#$0C
	ldx	$0C
	.byte	$FB
	.byte	$8F
	lda	#$18
	tay
	.byte	$0C
	ldx	$0C
	.byte	$FB
	.byte	$82
	ldy	$0C
	.byte	$FB
	.byte	$82
	lda	($06, x)
	lda	($06, x)
	.byte	$F7
	.byte	$0C
	lda	($A4, x)
	.byte	$FB
	.byte	$8F
	lda	#$0C
	ldy	$A2
	lda	($FD, x)
L87A2:	.byte	$FB
	brk
	clc
	.byte	$FB
	.byte	$FF
	.byte	$F7
	.byte	$0C
	sta	LA4A1, x
	lda	#$9E
	lda	($A4, x)
	ldx	$9F
	ldx	#$A6
	.byte	$AB
	.byte	$9E
	ldx	$9D
	ldx	$9C
	ldy	$A2
	ldy	$98
	.byte	$9F
	.byte	$9C
	.byte	$9F
	sta	L9FA4, x
	ldy	$A1
	.byte	$A7
	inc	L87EE, x
	.byte	$A3
	.byte	$0C
	ldx	$0C
	.byte	$A3
	.byte	$0C
	ldy	$A6
	ldy	$0C
	ldx	$0C
	tay
	.byte	$0C
L87D8:	.byte	$FE
L87D9:	inc	$9D87
	.byte	$9F
	ldy	#$A3
	ldy	$A5
	ldx	$A8
	.byte	$FB
	clc
	lda	#$0C
	ldy	$0C
	lda	($0C, x)
	inc	L87A2, x
L87EE:	.byte	$AE
L87EF:	.byte	$B0
	.byte	$FB
L87F1:	clc
	.byte	$B2
	inc	$9D, x
	asl	$9D
	asl	$9D
	.byte	$0C
	ldx	#$0C
	.byte	$FB
	.byte	$FF
	ldx	$18
	bcs	L880E
	ldx	$FB0C
	clc
	tya
	.byte	$0C
	sta	L9D06, x
	asl	$F7
	.byte	$0C
L880E:	sta	$FBA1, x
	rts

	ldy	$24
	.byte	$FB
	.byte	$FF
	.byte	$FD
L8817:	.byte	$FF
	stx	$F7, y
	.byte	$10
L881B:	.byte	$FB
	.byte	$FF
	.byte	$B2
	bpl	L87D9
	bpl	L87D9
	bvc	L87D9
	ldy	$B2, x
	bpl	L87D8
	ldx	LADB0
	ldy	$10, x
	.byte	$B2
	bmi	L8870
	rti

	lda	LBC10, y
	bpl	L87F1
	bvc	L87EF
	lda	$B4, x
	bpl	L87F1
	.byte	$B7
L883D:	lda	$4070, y
	rti

	.byte	$FE
L8842:	.byte	$1B
	dey
	.byte	$F7
	bpl	L8842
	sta	($9A, x)
	lda	($9D, x)
	lda	($9A, x)
	.byte	$A3
	.byte	$9F
	.byte	$A3
	txs
	ldy	$A1
	ldy	$9A
	ldx	#$9D
	ldx	#$9C
	ldy	$A1
	ldy	$9A
	lda	($9E, x)
	lda	($9A, x)
	lda	($9E, x)
	lda	($9F, x)
	ldx	#$A1
	ldy	$9A
	ldy	$A1
	ldy	$9A
	.byte	$A3
	.byte	$9F
	.byte	$A3
L8870:	txs
	.byte	$A3
	.byte	$9F
	.byte	$A3
	txs
	ldx	#$A0
	ldx	#$99
	lda	($9C, x)
	lda	($9A, x)
	lda	($9C, x)
	lda	($99, x)
	lda	($9C, x)
	lda	($97, x)
	lda	($99, x)
	lda	($FE, x)
	pha
	dey
L888B:	inc	L88CA, x
	inc	L88CA, x
	inc	L88CA, x
	inc	L88CA, x
	inc	L88E1, x
	inc	L88E1, x
	inc	L88ED, x
	inc	L88ED, x
	inc	L88F9, x
	inc	L88F9, x
	inc	L88F9, x
	.byte	$FE
L88AD:	sbc	$FE88, y
	ora	$89
	inc	L8905, x
	stx	$0C, y
	.byte	$FB
	bmi	L88DE
	.byte	$FB
	ldx	$FE, y
	ora	($89), y
	inc	L8911, x
	sta	$0C, x
	.byte	$FB
	bmi	L88EB
L88C7:	inc	L888B, x
L88CA:	.byte	$FB
	bmi	L88D2
	.byte	$FB
L88CE:	ldx	$97, y
	.byte	$07
	txs
L88D2:	asl	$9F
	asl	$FB
	bmi	L88DD
	.byte	$FB
	ldx	$97, y
	.byte	$07
	txs
L88DD:	.byte	$06
L88DE:	.byte	$9F
	asl	$FD
L88E1:	.byte	$FB
	bmi	L88E9
	.byte	$FB
	ldx	$96, y
	.byte	$07
	.byte	$99
L88E9:	asl	$9C
L88EB:	asl	$FD
L88ED:	.byte	$FB
	bmi	L88F5
	.byte	$FB
	ldx	$97, y
	.byte	$07
	txs
L88F5:	asl	$9D
	asl	$FD
L88F9:	.byte	$FB
	bmi	L8901
	.byte	$FB
	ldx	$91, y
	.byte	$07
	.byte	$94
L8901:	asl	$98
	asl	$FD
L8905:	.byte	$F7
	.byte	$03
	stx	$98, y
	stx	$98, y
	stx	$98, y
	stx	$98, y
	inc	$FD, x
L8911:	.byte	$F7
	.byte	$03
	sta	$97, x
	sta	$97, x
	sta	$97, x
	.byte	$95
L891A:	.byte	$97
	inc	$FD, x
	sbc	$FF09, y
	adc	#$FE
	bvc	L88AD
	sbc	$FF06, y
	.byte	$64
	inc	L8950, x
	sbc	$FF03, y
	.byte	$5F
	inc	L8950, x
	.byte	$FF
	.byte	$5A
	inc	L8950, x
	sbc	$FFFD, y
	eor	$FE, x
	bvc	L88C7
	sbc	$FFFA, y
L8941:	bvc	L8941
	bvc	L88CE
	sbc	$FFF7, y
	.byte	$4B
	inc	L8950, x
	sbc	$FFF4, y
	.byte	$46
L8950:	.byte	$FB
	.byte	$FF
	ldy	LAE18
	clc
	ldy	LAE18
	clc
	lda	($18), y
	ldx	LAC18
	clc
	ldx	LAC0C
	.byte	$0C
	.byte	$AB
	clc
	lda	#$0C
	.byte	$AB
	.byte	$0C
	ldx	LAC0C
	.byte	$0C
	.byte	$AB
	.byte	$0C
	lda	#$0C
	tay
	clc
	ldx	$0C
	tay
	.byte	$0C
	.byte	$AB
	clc
	ldy	$18
	.byte	$FB
L897D:	bmi	L897D
	sta	($89), y
	inc	L8991, x
	tax
L8985:	bmi	L8985
	sta	$FE89, x
	sta	LA989, x
L898D:	bmi	L898D
	bvc	L891A
L8991:	.byte	$F7
	.byte	$03
	tax
	tay
	tax
	tay
	tax
	tay
	tax
	tay
	inc	$FD, x
	.byte	$F7
	.byte	$03
	lda	#$A7
	lda	#$A7
	lda	#$A7
	lda	#$A7
L89A7:	inc	$FD, x
	.byte	$FF
	bvc	L89A7
	.byte	$4F
	inc	L8AAB, x
	inc	L8AAB, x
	.byte	$FF
	sei
	tya
	bit	$98
	asl	$99
	asl	$9A
	asl	$9C
	asl	$FF
	sei
	.byte	$FB
	.byte	$7F
	.byte	$9D
L89C4:	clc
	.byte	$FB
	.byte	$4F
	bpl	L89C4
	.byte	$7F
	.byte	$9F
	.byte	$02
	ldy	#$02
	lda	($02, x)
	ldx	#$02
	.byte	$A3
	clc
	.byte	$FB
	.byte	$4F
L89D6:	bpl	L89D6
	.byte	$BF
	txa
	.byte	$FB
	.byte	$7F
	sta	L9E02, x
	.byte	$02
	.byte	$9F
	.byte	$02
	ldy	#$02
	.byte	$A1
L89E5:	clc
	.byte	$FB
	.byte	$4F
	bpl	L89E5
	.byte	$7F
	ldy	#$02
	lda	($02, x)
	ldx	#$02
	.byte	$A3
	.byte	$02
	ldy	$18
	.byte	$FB
	.byte	$4F
L89F7:	bpl	L89F7
	.byte	$BF
	txa
	inc	L8ABF, x
	.byte	$FB
	.byte	$7F
	sta	L9E02, x
	.byte	$02
	.byte	$9F
	.byte	$02
	ldy	#$02
	.byte	$A1
L8A09:	clc
	.byte	$FB
	.byte	$4F
	bpl	L8A09
	.byte	$7F
	.byte	$9B
	.byte	$02
	.byte	$9C
	.byte	$02
	sta	L9E02, x
	.byte	$02
	.byte	$9F
L8A18:	clc
	.byte	$FB
	.byte	$4F
	bpl	L8A18
	.byte	$7F
	lda	$02
	ldx	$02
	.byte	$A7
	.byte	$02
	tay
	.byte	$02
	.byte	$A9
L8A27:	clc
	.byte	$FB
	.byte	$4F
	bpl	L8A27
	.byte	$7F
L8A2D:	ldy	$02
	lda	$02
	ldx	$02
	.byte	$A7
	.byte	$02
	tay
L8A36:	clc
	.byte	$FB
	.byte	$4F
	bpl	L8A36
	.byte	$7F
	.byte	$A3
	.byte	$02
	ldy	$02
	lda	$02
	ldx	$02
	.byte	$A7
L8A45:	clc
	.byte	$FB
	.byte	$4F
	bpl	L8A45
	.byte	$7F
	.byte	$A3
	.byte	$02
	ldy	$02
	lda	$02
	ldx	$02
	.byte	$A7
L8A54:	clc
	.byte	$FB
	.byte	$4F
	bpl	L8A54
	.byte	$7F
	ldx	#$02
	.byte	$A3
	.byte	$02
	ldy	$02
	lda	$02
	.byte	$A6
L8A63:	clc
	.byte	$FB
	.byte	$4F
	bpl	L8A63
	.byte	$7F
	lda	($02, x)
	ldx	#$02
	.byte	$A3
	.byte	$02
	ldy	$02
	.byte	$A5
L8A72:	clc
	.byte	$FB
	.byte	$4F
	bpl	L8A72
	.byte	$7F
	ldx	#$02
	.byte	$A3
L8A7B:	.byte	$02
	ldy	$02
	lda	$02
	ldx	$18
	.byte	$FB
	.byte	$4F
	bpl	L8A2D
	.byte	$02
	tay
	.byte	$02
	lda	#$02
	tax
	.byte	$02
	.byte	$F7
	php
	.byte	$AB
	ldx	$AB
	tax
	.byte	$A7
	tax
	lda	#$A6
	lda	#$A8
	lda	$A8
	.byte	$A7
	ldy	$A1
	.byte	$9E
	.byte	$9B
	tya
	sta	L9390, y
	stx	$99, y
	.byte	$9C
	inc	$FE, x
	cmp	($89, x)
L8AAB:	.byte	$F7
	ora	($98, x)
	.byte	$9C
	.byte	$9F
	ldx	#$A5
	tay
	.byte	$AB
	ldx	LAEB1
	.byte	$AB
	tay
	ldy	$A2
	.byte	$9F
	.byte	$9C
	inc	$FD, x
L8ABF:	.byte	$FB
	.byte	$7F
	.byte	$9E
	.byte	$02
	.byte	$9F
	.byte	$02
	ldy	#$02
	lda	($02, x)
	ldx	#$18
	.byte	$FB
L8ACC:	.byte	$4F
	bpl	L8ACC
	.byte	$FB
L8AD0:	bmi	L8AD0
	.byte	$AB
	txa
	inc	L8AAB, x
	tya
	bit	$98
	.byte	$06
L8ADB:	sta	$9A06, y
	asl	$9C
	asl	$FB
	.byte	$10
L8AE3:	.byte	$F7
	asl	$9D
	ldy	#$A4
	.byte	$12
	ldx	$06
	sta	LA3A0, x
	.byte	$12
	ldx	$06
	.byte	$9F
	ldx	#$A5
	.byte	$12
	tay
	asl	$9B
	.byte	$9E
	lda	($12, x)
	ldy	$06
	lda	($A4, x)
	.byte	$A7
	.byte	$12
	.byte	$AB
	asl	$9B
	.byte	$9F
	lda	$12
	tay
	asl	$9E
	ldx	#$A6
	.byte	$12
	tax
	asl	$9A
	.byte	$9E
	lda	($12, x)
	ldx	$06
	.byte	$9F
	ldx	#$A6
	.byte	$12
	tay
	asl	$9F
	.byte	$A3
	ldx	$12
	lda	#$06
	tya
	.byte	$9F
	ldx	#$12
	lda	$06
	sta	LA19E, x
	.byte	$12
	ldy	$06
	stx	$9D, y
	ldy	#$12
	.byte	$A7
	asl	$96
	sta	$12A0, x
	ldx	$06
	sta	$9C, x
	.byte	$9F
	.byte	$12
	ldx	#$06
	txs
	.byte	$9E
	lda	($12, x)
	ldx	$06
	.byte	$F7
	php
	ldx	LAEAB
	lda	LADAA
	ldy	LACA9
	.byte	$AB
	tay
	.byte	$AB
	tax
	lda	LA7AA
	ldy	$A1
	ldx	#$99
	.byte	$9C
	.byte	$9F
	ldx	#$A5
	inc	L8AE3, x
	.byte	$FF
L8B63:	.byte	$50
L8B64:	inc	L8BA1, x
	inc	L8BB4, x
	inc	L8BA1, x
	.byte	$F7
	asl	$FB
	.byte	$0F
	sta	L9C9B, y
	.byte	$9E
	.byte	$9F
	asl	L9FA2, x
	lda	$06
	ldy	$A2
	lda	($06, x)
	.byte	$9F
	asl	$A1
	asl	$A2
	.byte	$12
	ldy	$A2
	lda	($06, x)
L8B89:	.byte	$9F
	asl	$A1
	ldx	#$A1
	.byte	$9F
	.byte	$9E
	asl	$9C
	asl	$A1
	.byte	$9F
	.byte	$9E
	.byte	$9C
L8B97:	.byte	$FB
	.byte	$3F
	.byte	$9B
L8B9A:	rol	a
	.byte	$FB
	.byte	$0F
	clc
	inc	L8B64, x
L8BA1:	.byte	$F7
	.byte	$0C
	.byte	$FB
	.byte	$82
	sta	$97, x
	.byte	$FB
	.byte	$02
	sta	$97, x
	.byte	$FB
	.byte	$82
	sta	$97, x
	.byte	$FB
	.byte	$02
	sta	$97, x
	.byte	$FD
L8BB4:	.byte	$F7
	asl	$FB
	.byte	$0F
	sta	$97, x
	tya
	txs
	.byte	$9B
	asl	L9B9E, x
	lda	($06, x)
	ldy	#$9E
	sta	L9B06, x
	asl	$9D
	asl	$9E
	.byte	$12
	ldy	#$9E
	sta	L9B06, x
	asl	$9D
	.byte	$9E
	sta	L9A9B, x
	asl	$98
	asl	$9D
	.byte	$9B
	txs
	tya
	.byte	$FB
	.byte	$3F
	.byte	$97
	rol	a
	.byte	$FB
	.byte	$0F
	clc
	.byte	$FD
L8BE6:	.byte	$F7
	.byte	$0C
	inc	L8C00, x
	inc	L8C11, x
	inc	L8C11, x
	inc	L8C11, x
	inc	L8C15, x
	inc	L8C00, x
	inc	L8BB4, x
	inc	L8BE6, x
L8C00:	.byte	$FB
	.byte	$82
	.byte	$90
L8C03:	stx	$02FB
	.byte	$89
	.byte	$8E
	.byte	$FB
L8C09:	.byte	$82
	bcc	L8B9A
	.byte	$FB
	.byte	$02
	.byte	$89
	.byte	$8E
	.byte	$FD
L8C11:	.byte	$92
	.byte	$8F
	.byte	$92
	.byte	$8F
L8C15:	.byte	$92
	.byte	$8F
	.byte	$92
	.byte	$8F
	sbc	$30FB, x
	.byte	$F7
	.byte	$0C
L8C1E:	txs
	tya
	txs
	tya
	txs
	tya
	txs
	tya
	inc	L8C35, x
	inc	L8C35, x
	inc	L8C35, x
	inc	L8C39, x
	inc	L8C1E, x
L8C35:	.byte	$9E
	.byte	$9B
	.byte	$9E
	.byte	$9B
L8C39:	.byte	$9E
	.byte	$9B
	.byte	$9E
	.byte	$9B
	.byte	$FD
L8C3E:	.byte	$03
	.byte	$FB
	bmi	L8C48
	.byte	$FB
	.byte	$89
	.byte	$FF
	.byte	$3C
	.byte	$B9
	.byte	$06
L8C48:	lda	$05, x
	.byte	$B2
	.byte	$04
	.byte	$F7
	.byte	$03
	.byte	$AF
	.byte	$FF
	lsr	$B5
	.byte	$B2
	.byte	$AF
	lda	$50FF
	.byte	$B2
	.byte	$AF
	lda	$FFA9
	.byte	$5A
	lda	LA6A9
	.byte	$A3
	.byte	$FF
	.byte	$64
	lda	#$A6
	.byte	$A3
	lda	($FF, x)
	adc	LA3A6
	lda	($9D, x)
	.byte	$FF
	ror	$A1, x
L8C70:	sta	L979A, x
	.byte	$FF
	.byte	$7F
	sta	L979A, x
	sta	$FF, x
	dey
	txs
	.byte	$97
	sta	$91, x
	.byte	$FF
	bcc	L8C09
	stx	L9591
	.byte	$97
	txs
	.byte	$97
	txs
	sta	LA3A1, x
L8C8C:	ldx	$A3
	ldx	$A9
	.byte	$AD
L8C91:	.byte	$AF
	.byte	$B2
	inc	$FB, x
	.byte	$8F
	.byte	$B7
	bmi	L8C99
L8C99:	.byte	$FC
	sbc	$FF0C, y
	sei
	.byte	$FB
	.byte	$FF
	.byte	$9F
	clc
	.byte	$F7
	.byte	$03
	lda	($A3, x)
	.byte	$A4
L8CA7:	ldx	$A8
	lda	#$AB
	lda	LB0AF
	.byte	$B2
	ldy	$B5, x
	inc	$FB, x
	jsr	$11B7
L8CB6:	.byte	$B7
	bpl	L8C70
	bpl	L8CB6
	.byte	$FF
	.byte	$F7
	.byte	$02
	.byte	$B7
	lda	LB9B7, y
	.byte	$B7
	lda	LB9B7, y
L8CC6:	.byte	$B7
	lda	LB9B7, y
	.byte	$B7
	lda	LB9B7, y
	.byte	$B7
	lda	LB7F6, y
	ora	$0DB5
	.byte	$B2
	php
	.byte	$AF
	php
L8CD9:	lda	LAB08
	bmi	L8CD9
	brk
	brk
L8CE0:	.byte	$FC
	.byte	$03
L8CE2:	.byte	$FF
	bvc	L8CE0
	.byte	$8F
	sty	L9309
	php
	.byte	$97
	.byte	$07
	.byte	$9C
	asl	$8E
	ora	$95
	.byte	$04
	.byte	$F7
	.byte	$03
	tya
	sta	$58FF, x
	bcc	L8C91
	txs
	.byte	$9F
	.byte	$FF
	rts

	sta	($98), y
	.byte	$9C
	lda	($FF, x)
	pla
	.byte	$87
	stx	L9893
	.byte	$FF
	bvs	L8CA7
	lda	($A4, x)
	lda	#$AD
	bcs	L8CC6
	ldy	$FF, x
	.byte	$64
	.byte	$AF
	.byte	$AB
	tay
	.byte	$A3
	.byte	$FF
	.byte	$5A
	.byte	$9F
	.byte	$9C
	.byte	$97
	.byte	$93
	inc	$8C, x
	bmi	L8D22
L8D22:	.byte	$FC
	.byte	$02
	.byte	$FF
	stx	$FB, y
	bmi	L8D41
	.byte	$FB
	.byte	$4F
	.byte	$F7
	.byte	$0C
	lda	#$9A
	lda	($A9, x)
	tay
	sta	LA8A1, y
	ldx	$96
	.byte	$9F
	ldx	$F6
	lda	$0D
	ldx	#$0E
	lda	($0F, x)
	.byte	$A0
L8D41:	.byte	$0F
	lda	($04, x)
	ldy	#$04
	lda	($30, x)
	brk
	.byte	$FC
L8D4A:	ora	($FF, x)
	stx	$FB, y
	eor	$F7
	asl	$FE
	pla
	sta	$68FE
	sta	$68FE
	sta	$68FE
	sta	L90F6
	.byte	$14
	sta	($02), y
	.byte	$92
	.byte	$02
	txa
	bmi	L8D67
L8D67:	.byte	$FC
	sty	L8B97
	stx	$FD, y
	.byte	$FF
	adc	$06FB, x
	lda	($13, x)
	lda	($05, x)
	.byte	$F7
	.byte	$0C
	lda	($9F, x)
	.byte	$9F
	.byte	$9F
	sta	LA19F, x
	ldx	#$A1
	.byte	$9F
	lda	($A2, x)
	ldy	$A6
	lda	#$A6
	ldy	$A2
	lda	($F6, x)
	.byte	$9F
	.byte	$13
	.byte	$9F
	ora	$9F
	.byte	$0C
	lda	($13, x)
	lda	($05, x)
	lda	($0C, x)
	lda	($0C, x)
	sta	LA10C, x
	.byte	$0C
	.byte	$FB
	.byte	$3F
	.byte	$9F
	rts

	.byte	$FB
	bmi	L8DBC
	.byte	$FF
	sei
L8DA6:	.byte	$FB
	.byte	$4F
	ldy	$13
	ldy	$05
	lda	#$18
	.byte	$FC
	.byte	$AB
	clc
	lda	LAE18
	clc
	.byte	$FB
	.byte	$7F
	bcs	L8DD1
	lda	$30, x
	.byte	$FB
L8DBC:	.byte	$4F
	ldy	$13, x
	.byte	$B2
	ora	$B2
	bit	$B0
	.byte	$0C
	.byte	$FB
	rti

	.byte	$0C
	.byte	$FB
	.byte	$4F
	.byte	$AF
	.byte	$0C
	.byte	$AF
	.byte	$0C
	.byte	$B2
	.byte	$0C
	.byte	$B0
L8DD1:	clc
	lda	LA130
	.byte	$13
	lda	($05, x)
	lda	($18, x)
	lda	($18, x)
	.byte	$A3
	clc
	.byte	$A5
L8DDF:	clc
	.byte	$FB
	.byte	$7F
	ldx	$30
	.byte	$FB
	.byte	$4F
	.byte	$0C
	.byte	$F7
	.byte	$0C
	ldx	$A8
	lda	#$FB
	.byte	$7F
	.byte	$AB
	bit	$FB
	.byte	$4F
	.byte	$0C
	ldx	$A6
	lda	#$A9
	.byte	$0C
	tay
	.byte	$0C
	ldx	$0C
	ldy	$0C
	.byte	$FB
	.byte	$7F
	lda	$FB30
	.byte	$4F
	ldx	LABAD
	.byte	$FB
	.byte	$7F
	lda	#$24
	ldx	$0C
L8E0D:	lda	#$0C
	.byte	$AB
	bmi	L8E0D
	.byte	$4F
	lda	LA9AB
	.byte	$FB
	.byte	$7F
	lda	#$24
	tay
	.byte	$0C
	ldy	$0C
	bcs	L8E50
	.byte	$FB
	.byte	$4F
	lda	LB0AE
L8E25:	.byte	$FB
	.byte	$7F
	.byte	$B2
	bmi	L8E25
	.byte	$4F
	ldx	$A8
	lda	#$F6
	.byte	$FB
	.byte	$7F
	ldx	LAD30
	bmi	L8DDF
	.byte	$3C
	.byte	$FB
	eor	$0C
	inc	L8DA6, x
L8E3D:	.byte	$FB
	asl	$98
	.byte	$13
	sta	$F705, x
	.byte	$0C
	sta	L9898, x
	tya
	sta	$98, x
	sta	L9D9F, x
	tya
	.byte	$9D
L8E50:	.byte	$9F
	lda	($A2, x)
	ldx	$A2
	lda	($9F, x)
	sta	L98F6, x
	.byte	$13
	tya
	ora	$98
	.byte	$0C
	sta	L9D13, x
	ora	$9D
	.byte	$0C
	sta	L950C, x
	.byte	$0C
	sta	$FB0C, x
	.byte	$3F
	tya
	rts

	.byte	$FB
	bmi	L8E8A
L8E72:	.byte	$FB
	.byte	$4F
	ldx	#$13
	ldx	#$05
	lda	($18, x)
	ldy	$18
	lda	#$18
	lda	#$18
	.byte	$FB
	.byte	$7F
	lda	#$30
	lda	#$30
	ldx	LAD24
	.byte	$0C
L8E8A:	.byte	$FB
	rti

	.byte	$0C
	.byte	$FB
	.byte	$4F
	ldy	LAC0C
	.byte	$0C
	.byte	$AF
	.byte	$0C
	.byte	$FB
	.byte	$7F
L8E97:	lda	LA918
	bmi	L8E97
	.byte	$4F
	sta	$13, x
	.byte	$95
L8EA0:	ora	$99
	clc
	sta	$9A18, y
	clc
	.byte	$9C
	clc
	.byte	$FB
	.byte	$7F
	sta	$FB30, x
	.byte	$4F
	.byte	$0C
	.byte	$F7
	.byte	$0C
	sta	LA19F, x
	.byte	$FB
	.byte	$7F
	ldx	$24
	.byte	$FB
	.byte	$4F
	.byte	$0C
	.byte	$A3
	.byte	$A3
	ldx	$A6
	.byte	$0C
	ldy	$0C
	ldx	#$0C
	ldx	#$0C
	.byte	$FB
	.byte	$7F
	lda	$30
	.byte	$FB
	.byte	$4F
	lda	$A5
	lda	$FB
	.byte	$7F
	ldx	$24
	lda	($0C, x)
	lda	($0C, x)
	ldx	$30
	.byte	$FB
	.byte	$4F
	.byte	$A3
	.byte	$A3
	.byte	$A3
	.byte	$FB
	.byte	$7F
	ldx	#$24
	ldx	#$0C
L8EE5:	ldx	#$0C
	tax
	bmi	L8EE5
	.byte	$4F
	tax
	.byte	$AB
	lda	$7FFB
	ldx	$FB30
	.byte	$4F
	ldx	#$A4
	ldx	$F6
	.byte	$FB
	.byte	$7F
	ldx	$30
	tay
	bmi	L8EA0
	.byte	$3C
	.byte	$FB
	eor	$0C
	inc	L8E72, x
	.byte	$FB
	rts

	sta	$6060, x
	rts

	rts

	rts

	clc
	.byte	$FB
	rts

	.byte	$F7
	clc
L8F13:	sta	L9B9C, x
	txs
	.byte	$FB
	.byte	$7F
	sta	$18, x
	stx	$18, y
	.byte	$FB
	rts

	sta	L9198, x
	sta	L959D, x
	tya
	sta	$7FFB, x
	sta	$18, x
	sta	$18, x
	.byte	$FB
	rts

	txs
	sta	$9D, x
	txs
	.byte	$97
	txs
	.byte	$9F
	.byte	$93
	.byte	$FB
	.byte	$7F
	tya
	clc
	.byte	$FB
	rts

	tya
	.byte	$9C
	sta	$99, x
	.byte	$9C
	sta	$9A, x
	.byte	$9C
	sta	L979A, x
	txs
	.byte	$9F
	.byte	$93
	tya
	tya
	.byte	$9F
	ldx	#$A1
	.byte	$9E
	txs
	sta	$93, x
	sta	$96, x
	.byte	$93
	.byte	$9F
	tya
	.byte	$A2
L8F5A:	tya
	sta	L9D98, x
	clc
	inc	L8F13, x
L8F62:	.byte	$FF
	.byte	$82
	.byte	$FB
	bmi	L8F97
	.byte	$FB
	lsr	$AB
	.byte	$13
	.byte	$AB
	ora	$F7
	.byte	$0C
	.byte	$AB
	.byte	$AB
	.byte	$AB
	.byte	$AB
	.byte	$AB
	.byte	$AB
	.byte	$AB
	.byte	$AB
	.byte	$AB
	.byte	$AB
	lda	#$AB
	lda	LA9AB
	tay
	lda	#$AB
	lda	LA9AB
	tay
	.byte	$FF
	adc	LA9A9, x
	lda	#$FF
	.byte	$7A
	ldy	LACAC
	.byte	$FF
	ror	$AF, x
	.byte	$AF
	.byte	$AF
	.byte	$FF
	adc	($B2), y
L8F97:	.byte	$B2
	.byte	$B2
	.byte	$FB
	.byte	$7F
	.byte	$B7
	.byte	$54
	.byte	$FB
	bmi	L8FB0
	.byte	$FF
	ror	$2FFE
L8FA4:	bcc	L8FA4
	.byte	$72
L8FA7:	bcc	L8FA7
	.byte	$2F
L8FAA:	bcc	L8FAA
	.byte	$72
L8FAD:	bcc	L8FAD
	.byte	$2F
L8FB0:	.byte	$90
L8FB1:	tay
	lda	#$AB
	bit	$A8
	lda	$A6
	tay
	lda	#$0C
	.byte	$AB
	.byte	$0C
	lda	LAB0C
	lda	#$FB
	eor	#$A8
	.byte	$9C
	.byte	$FF
	adc	($9C), y
	lda	($FF, x)
	.byte	$74
	lda	($A4, x)
	.byte	$FF
	.byte	$77
	tay
	lda	$79FF
	ldx	$9D
	.byte	$FF
	.byte	$7C
	sta	$FF9F, x
	.byte	$7F
	.byte	$9F
	.byte	$A3
	.byte	$FF
	.byte	$82
	ldx	$AB
	inc	$FF, x
	sei
	bcs	L8FFE
	ldy	$08
	ldy	$08
	ldy	$08
	ldy	$18
	ldx	$18
	.byte	$FF
	.byte	$64
	.byte	$FB
	ror	$03F7, x
	inc	L901D, x
	.byte	$FE
	.byte	$1D
L8FFB:	bcc	L8FFB
	.byte	$1D
L8FFE:	bcc	L8FFE
	ora	$FF90, x
	adc	#$FE
	rol	$90
	inc	L9026, x
	inc	L9026, x
	inc	L9026, x
	.byte	$FF
	ror	$FB
	eor	#$F7
	php
	tay
	bpl	L8FB1
	tya
	tya
	tya
	brk
L901D:	ldy	$A0
	ldy	$A0
	ldy	$A0
	ldy	$A0
	.byte	$FD
L9026:	tay
	ldy	$A8
L9029:	ldy	$A8
	ldy	$A8
	ldy	$FD
	inc	$FB, x
	.byte	$8F
	.byte	$9F
	clc
	tay
	.byte	$14
	tay
	.byte	$02
	.byte	$A7
	.byte	$02
	tay
	.byte	$0C
	.byte	$AB
	.byte	$0C
	ldx	$14
	ldx	$02
	lda	$02
	.byte	$F7
	.byte	$0C
	ldx	$AB
L9048:	ldy	$30
	ldy	$A6
	tay
	lda	#$0C
	.byte	$AB
	lda	#$A8
	.byte	$0C
	lda	#$A8
	ldx	$30
	ldx	$A8
	lda	#$AB
	bit	$A8
	lda	$A6
	tay
	lda	#$0C
	.byte	$AB
	.byte	$0C
	lda	LAB0C
	lda	#$A8
	clc
	tay
	ldy	$0C
	lda	($0C, x)
	ldx	$3C
	sbc	$BFFB, x
	.byte	$AB
	.byte	$0C
	ldy	LA630
	.byte	$A7
	lda	#$AE
	bit	$AB
	.byte	$0C
	.byte	$A7
	.byte	$0C
	lda	#$30
	ldy	LA9AB
	.byte	$FB
	.byte	$8F
	.byte	$A7
	.byte	$0C
	lda	#$0C
	.byte	$AB
	.byte	$0C
	.byte	$FB
	.byte	$BF
	ldy	LB0AE
L9093:	bmi	L9048
	.byte	$B2
	bcs	L9093
	stx	$10F7
	ldx	LABAC
	.byte	$AB
	lda	#$A7
	.byte	$F7
	.byte	$0C
	.byte	$FB
	.byte	$BF
	lda	#$30
	ldy	$A6
	.byte	$A7
	.byte	$A7
	.byte	$54
	ldx	$30
	.byte	$FB
	.byte	$8F
	.byte	$0C
L90B1:	sbc	$30FB, x
	bmi	L90B1
	lsr	$A3
	.byte	$13
	.byte	$A3
	ora	$F7
	.byte	$0C
	.byte	$A3
	lda	#$A9
	lda	#$A8
	tay
	tay
	ldx	$A4
	.byte	$A3
	lda	($A3, x)
	ldy	$A3
	lda	($9F, x)
	lda	($A3, x)
L90CF:	ldy	$A3
	lda	($9F, x)
	ldy	$A4
	ldy	$A7
	.byte	$A7
	.byte	$A7
	tax
	tax
	tax
	lda	LADAD
	.byte	$FB
	.byte	$7F
	bcs	L911F
	.byte	$AF
	.byte	$0C
	.byte	$FB
	bmi	L9110
	inc	L9159, x
	.byte	$FB
	.byte	$8F
	inc	L91A0, x
	inc	L9159, x
	.byte	$FB
	.byte	$4F
	.byte	$FE
L90F6:	ldy	#$91
	inc	L9159, x
	ldy	$24
	.byte	$9F
	bit	$A1
	.byte	$54
	.byte	$FB
	eor	#$A1
	sta	$95, x
	.byte	$9C
	.byte	$9C
	lda	($A1, x)
	tay
	sta	L9797, x
	txs
	txs
L9110:	sta	LA6A3, x
	inc	$A8, x
	clc
	.byte	$9B
	php
	.byte	$9B
	php
	.byte	$9B
	php
	.byte	$9B
	clc
	.byte	$9D
L911F:	clc
	.byte	$FB
	ror	$03F7, x
	inc	L9147, x
	inc	L9147, x
	inc	L9147, x
	inc	L9147, x
	inc	L9150, x
	inc	L9150, x
	inc	L9150, x
	inc	L9150, x
	.byte	$FB
	eor	#$F7
	php
	.byte	$9F
	bpl	L90CF
	sty	L8C8C
	brk
L9147:	.byte	$9B
	tya
	.byte	$9B
	tya
	.byte	$9B
	tya
	.byte	$9B
	tya
	.byte	$FD
L9150:	.byte	$9F
	.byte	$9C
	.byte	$9F
	.byte	$9C
	.byte	$9F
	.byte	$9C
	.byte	$9F
	.byte	$9C
	.byte	$FD
L9159:	inc	$FB, x
	.byte	$8F
	.byte	$9F
	.byte	$14
	.byte	$9F
	.byte	$02
	.byte	$9E
	.byte	$02
	.byte	$9F
	asl	$9F
	asl	$9C
	asl	$9F
	asl	$9F
	asl	$069F, x
	sta	L9F06, x
	asl	$9C
	.byte	$14
	.byte	$9C
	.byte	$02
	.byte	$9B
	.byte	$02
	.byte	$F7
	.byte	$0C
	.byte	$9C
	.byte	$9F
	sta	LA424, x
	bit	$A4
	.byte	$0C
	lda	$18
	ldy	$A3
	lda	($A3, x)
	bit	$A4
	bit	$9F
	bit	$A1
	.byte	$54
	lda	($9C, x)
	tya
	.byte	$9C
	lda	($9C, x)
	tya
	.byte	$9C
	.byte	$0C
L9198:	ldy	$A1
	ldy	$A3
	.byte	$FB
	.byte	$89
	bit	$FD
L91A0:	.byte	$F7
	asl	$9B
	tya
	.byte	$9B
	tya
	.byte	$9B
	tya
	.byte	$9B
	tya
	ldy	#$9D
	ldy	#$9D
	ldy	#$9D
	txs
	sta	L9A9F, x
	.byte	$9F
	txs
	.byte	$9F
	txs
	.byte	$9F
	txs
	.byte	$9B
	tya
	.byte	$9B
	tya
	.byte	$9B
	tya
	.byte	$9B
	tya
	ldy	$A1
	ldy	$A1
	.byte	$A4
L91C7:	lda	($A4, x)
	lda	($A6, x)
	.byte	$A3
	ldx	$A3
	ldx	$A3
	ldx	$A3
	ldx	#$9F
	ldx	#$9F
	ldx	#$9D
	ldx	#$9D
	ldx	#$9F
	ldx	#$9F
	ldy	$A2
	ldy	$A2
	.byte	$9B
	ldy	#$A4
	ldy	#$9B
	ldy	#$A4
	ldy	#$9B
	ldy	#$A4
	ldy	#$9D
	ldx	#$9B
	ldy	#$9F
	txs
	stx	$9A, y
	.byte	$9F
	txs
	stx	$9A, y
	.byte	$9F
	.byte	$9B
	tya
	.byte	$9B
	.byte	$9F
	.byte	$9B
	tya
	.byte	$9B
	tya
	sta	$98, x
	sta	L9598, x
	tya
	sta	L9598, x
	tya
	sta	L9598, x
	tya
	.byte	$9B
	.byte	$A3
	.byte	$9F
	sta	LA39F, x
	.byte	$9F
	sta	LA39F, x
L921B:	.byte	$9F
	sta	LA39F, x
	.byte	$9F
	sta	$F79F, x
	.byte	$0C
	ldy	$0C
	.byte	$A3
	lda	($A3, x)
	.byte	$0C
	.byte	$FB
	.byte	$89
	clc
L922D:	sbc	a:$FB, x
	bmi	L922D
	bmi	L91C7
	jmp	($CF7)

	.byte	$93
	txs
	.byte	$9F
	ldy	$A6
	lda	#$A6
	ldy	$A3
	ldy	$A6
	lda	#$A6
	ldy	$A3
	lda	($A1, x)
	lda	($A4, x)
	ldy	$A4
	.byte	$A7
	.byte	$A7
	.byte	$A7
	tax
	tax
	tax
	.byte	$93
L9253:	tya
	txs
	.byte	$9F
	ldy	$A6
	.byte	$FB
	rts

	.byte	$AB
	.byte	$34
	.byte	$FC
	inc	L92BF, x
	inc	L92F2, x
	inc	L92BF, x
	inc	L92F2, x
	inc	L92BF, x
	.byte	$FB
	.byte	$FF
	tay
	.byte	$9C
	lda	($FC, x)
	sta	$9A, x
L9274:	.byte	$9C
	sta	$FBFC, x
	brk
	clc
	.byte	$FB
	bmi	L9274
	.byte	$0C
	.byte	$92
	.byte	$9E
	.byte	$9E
	ldy	$A4
	.byte	$FC
	tay
	.byte	$9E
	bcs	L921B
	.byte	$9F
	.byte	$9F
	.byte	$A3
	.byte	$A3
	.byte	$FC
	ldx	$A9
	.byte	$AF
	inc	$98, x
L9292:	clc
	ldy	#$08
	ldy	#$08
	ldy	#$08
	ldy	#$18
	.byte	$FC
	ldx	#$18
	.byte	$F7
	.byte	$0C
	ldy	#$94
	ldy	#$94
	sbc	LA002, y
	.byte	$FC
	sty	$A0, x
	sty	$F9, x
	brk
	tya
	.byte	$93
	tya
	.byte	$93
	tya
	.byte	$93
L92B3:	tya
	.byte	$FC
	.byte	$93
	.byte	$F7
	php
	tya
	bpl	L9253
	tya
	tya
	tya
	brk
L92BF:	.byte	$FB
	.byte	$FF
	.byte	$F7
	clc
	tya
	ldy	$A3
	.byte	$FC
	.byte	$97
	stx	$A2, y
	.byte	$A1
L92CB:	.byte	$FC
	sta	$94, x
	ldy	#$9F
	.byte	$FC
	.byte	$93
	.byte	$92
	.byte	$9E
	.byte	$9D
L92D5:	.byte	$FC
	lda	#$A8
	.byte	$9C
	.byte	$A1
L92DA:	.byte	$FC
	sta	$9A, x
	.byte	$9C
	sta	$FBFC, x
	brk
	clc
	.byte	$FB
	.byte	$FF
	.byte	$9E
	.byte	$92
	.byte	$9E
	.byte	$FC
	clc
	.byte	$9F
	txs
	.byte	$93
	.byte	$FC
	.byte	$FB
	brk
	clc
	.byte	$FD
L92F2:	.byte	$F7
	.byte	$0C
	.byte	$FB
	rts

L92F6:	sta	L9D18, x
	.byte	$FB
	bmi	L9292
	.byte	$FC
	stx	$0C, y
	stx	$FB, y
	rts

	.byte	$9B
	clc
	.byte	$9B
	.byte	$FB
	.byte	$30
L9307:	ldy	#$FC
L9309:	ldy	#$0C
	sty	$FB, x
	rts

	txs
	clc
	txs
	.byte	$FB
	bmi	L92B3
	.byte	$FC
	.byte	$9F
	.byte	$0C
	.byte	$93
	tya
	ldy	$9A
	ldx	$9B
	.byte	$FC
	.byte	$A7
	.byte	$9C
	tay
	.byte	$FB
	rts

	sta	L9D18, x
	.byte	$FB
	bmi	L92CB
	.byte	$FC
	ldx	#$0C
	stx	$FB, y
	rts

	.byte	$9B
	clc
	.byte	$9B
	.byte	$FB
	bmi	L92D5
	.byte	$FC
	ldy	#$0C
	sty	$FB, x
	rts

	txs
	clc
	.byte	$FB
	bmi	L92DA
	.byte	$F7
	clc
	.byte	$FB
	.byte	$FF
	txs
	.byte	$FC
	.byte	$9E
	.byte	$9F
	clc
	txs
	.byte	$FC
	.byte	$9F
	.byte	$9F
	txs
	.byte	$93
	.byte	$FC
	.byte	$FB
	brk
	clc
	.byte	$FD
L9354:	lda	#$00
	sta	$2001
	jsr	LC17A
	lda	#$18
	sta	$2001
	rts

	lda	#$00
	brk
	.byte	$04
	.byte	$17
	brk
	asl	$07
	lda	#$00
	sta	$BA
	sta	$05
	sta	$07
	sta	$06
	sta	$8D
	ldx	#$3B
L9378:	sta	$51, x
	dex
	bpl	L9378
	lda	#$27
	sta	$E0
	jsr	LC6BB
	jsr	LFF74
	jsr	L9354
	lda	#$FF
	sta	$C5
	brk
	.byte	$01
L9390:	.byte	$07
	jsr	LC6F0
	.byte	$02
	jsr	$C7C5
	plp
	rts

	jsr	LFF74
	lda	#$10
	brk
	.byte	$04
	.byte	$17
	brk
	.byte	$03
	.byte	$17
	brk
	asl	$07
	jsr	LC6BB
	lda	#$00
	sta	$2001
	jsr	LFCA3
	lda	#$00
	sta	$BA
	sta	$05
	sta	$07
	sta	$06
	lda	#$27
	sta	$E0
	jsr	LFF74
	jsr	L9354
	lda	#$23
	sta	$0B
	lda	#$C8
	sta	$0A
	lda	#$55
	sta	$08
	ldy	#$08
L93D5:	jsr	LC690
	dey
	bne	L93D5
	lda	#$AA
	sta	$08
	ldy	#$20
L93E1:	jsr	LC690
	dey
	bne	L93E1
	jsr	LFF74
	lda	L948B
	sta	$99
	lda	L948C
	sta	$9A
	jmp	L93FA

L93F7:	jsr	L9354
L93FA:	ldy	#$00
	lda	($99), y
	sta	$0A
	iny
	lda	($99), y
	sta	$0B
	ldy	#$02
L9407:	lda	($99), y
	sta	$08
	cmp	#$F7
	bne	L9423
	iny
	lda	($99), y
	sta	$3C
	iny
	lda	($99), y
	sta	$08
L9419:	jsr	LC690
	dec	$3C
	bne	L9419
	iny
	bne	L9407
L9423:	cmp	#$FC
	beq	L9431
	cmp	#$FD
	beq	L9431
	jsr	LC690
	iny
	bne	L9407
L9431:	iny
	tya
	clc
	adc	$99
	sta	$99
	bcc	L943C
	inc	$9A
L943C:	lda	$08
	cmp	#$FC
	beq	L93FA
	jsr	LFF74
	brk
	.byte	$07
	.byte	$07
	lda	$BA
	bne	L9450
	ldy	#$08
	bne	L947D
L9450:	cmp	#$01
	bne	L9458
	ldy	#$02
	bne	L947D
L9458:	cmp	#$02
	beq	L946A
	cmp	#$03
	beq	L946A
	cmp	#$04
	beq	L946A
	cmp	#$0D
	beq	L947B
	bcc	L947B
L946A:	cmp	#$12
	bcc	L9477
	ldy	#$A0
L9470:	jsr	LFF74
	dey
	bne	L9470
	rts

L9477:	ldy	#$03
	bne	L947D
L947B:	ldy	#$02
L947D:	brk
	.byte	$03
	.byte	$17
	dey
	bne	L947D
	inc	$BA
	brk
	php
	.byte	$07
	jmp	L93F7

L948B:	.byte	$8D
L948C:	sty	$E8, x
	jsr	$3226
	and	($2A), y
	and	$24, x
	.byte	$37
	sec
	.byte	$2F
	bit	$37
	bit	$3132
	rol	$60, x
	.byte	$FC
	.byte	$47
	and	($37, x)
	.byte	$2B
	.byte	$32
	sec
	.byte	$5F
	.byte	$2B
	bit	$36
	.byte	$37
	.byte	$5F
	and	$28, x
	rol	$37, x
	.byte	$32
	and	$28, x
	.byte	$27
	.byte	$FC
	stx	$21
	.byte	$33
	plp
	bit	$26
	plp
	.byte	$5F
	sec
	and	($37), y
	.byte	$32
	.byte	$5F
	.byte	$37
	.byte	$2B
	plp
	.byte	$5F
	.byte	$3A
	.byte	$32
	and	$2F, x
	.byte	$27
	rts

	.byte	$FC
	cpx	$21
	and	$38
	.byte	$37
	.byte	$5F
	.byte	$37
	.byte	$2B
	plp
	and	$28, x
	.byte	$5F
	bit	$35
	plp
	.byte	$5F
	bmi	L9503
	and	($3C), y
	.byte	$5F
	and	$32, x
	bit	$27
	rol	$FC, x
	and	#$22
	.byte	$3C
	plp
	.byte	$37
	.byte	$5F
	.byte	$37
	.byte	$32
	.byte	$5F
	.byte	$37
	and	$24, x
	and	$2F28, y
	adc	($FC, x)
	.byte	$89
	.byte	$22
	bmi	L9521
	.byte	$3C
	.byte	$5F
	.byte	$37
	.byte	$2B
	plp
	.byte	$5F
L9503:	.byte	$2F
	bit	$2B2A
	.byte	$37
	.byte	$FC
	iny
	.byte	$22
	.byte	$36
L950C:	.byte	$2B
	bit	$2831
	.byte	$5F
	sec
	.byte	$33
	.byte	$32
	and	($5F), y
	.byte	$37
	.byte	$2B
	plp
	plp
	adc	($FD, x)
	dey
	and	($27, x)
	and	$24, x
L9521:	rol	a
	.byte	$32
	and	($5F), y
	.byte	$3A
	bit	$35
	and	$2C, x
	.byte	$32
	and	$FC, x
	sbc	$3621
	.byte	$37
	bit	$29
	and	#$FC
	cpy	#$23
	.byte	$F7
	jsr	$FDFF
L953B:	stx	$21
	rol	$26, x
	plp
	and	($24), y
	and	$2C, x
	.byte	$32
	.byte	$5F
	.byte	$3A
	and	$2C, x
	.byte	$37
	.byte	$37
	plp
	and	($5F), y
	and	$3C
	.byte	$FC
	.byte	$EB
	and	($3C, x)
	sec
	and	$5F2C
	.byte	$2B
	.byte	$32
	and	$2C, x
	bit	$C0FC
	.byte	$23
	.byte	$F7
	jsr	$FD05
	sta	$21
	rol	$2B
	bit	$35
	bit	$26
	.byte	$37
	plp
	and	$5F, x
	.byte	$27
	plp
	rol	$2C, x
	rol	a
	and	($28), y
	.byte	$27
	.byte	$5F
	and	$3C
	.byte	$FC
	sbc	#$21
	bit	$2E
	bit	$2435
	.byte	$5F
	.byte	$37
	.byte	$32
	and	$2C, x
	.byte	$3C
	bit	$30
	bit	$FC
	cpy	#$23
	.byte	$F7
	.byte	$20
L9591:	asl	a
	sbc	$2187, x
	bmi	L95CF
	.byte	$36
L9598:	bit	$5F26
	rol	$32
L959D:	bmi	L95D2
	.byte	$32
	rol	$28, x
	.byte	$27
L95A3:	.byte	$5F
	and	$3C
	.byte	$FC
	inx
	and	($2E, x)
	.byte	$32
	bit	$2B26
	bit	$365F
	sec
	rol	a
	bit	$243C
	bmi	L95DC
	.byte	$FC
	cpy	#$23
	.byte	$F7
	jsr	$FD0F
	rol	a
	and	($33, x)
	and	$32, x
	rol	a
	and	$24, x
	bmi	L95F1
	.byte	$27
	.byte	$5F
	and	$3C
	.byte	$FC
	tay
L95CF:	and	($2E, x)
	.byte	$32
L95D2:	bit	$2B26
	bit	$315F
	bit	$2E
	bit	$30
L95DC:	sec
	and	$24, x
	.byte	$FC
	asl	a
	.byte	$22
	rol	$2D32
	bit	$3C5F
	.byte	$32
	rol	$2B, x
	bit	$2427
	.byte	$FC
	.byte	$67
	.byte	$22
L95F1:	.byte	$37
	bit	$2E
	plp
	and	($32), y
	and	$2C, x
	.byte	$5F
	.byte	$3C
	bit	$30
	bit	$30
	.byte	$32
	and	$2C, x
	.byte	$FC
	bne	L9628
	.byte	$F7
	php
	ora	$F7
	bpl	L960B
L960B:	sbc	$2189, x
	rol	$2A
	.byte	$5F
	.byte	$27
	plp
	rol	$2C, x
	rol	a
	and	($28), y
	.byte	$27
	.byte	$5F
	and	$3C
	.byte	$FC
	sbc	#$21
	.byte	$37
	bit	$2E
	bit	$36
	.byte	$2B
	bit	$3C5F
L9628:	bit	$36
	sec
	and	($32), y
	.byte	$FC
	cld
	.byte	$23
	.byte	$F7
	php
	asl	a
	sbc	$2186, x
	rol	$26, x
	plp
	and	($24), y
	and	$2C, x
	.byte	$32
	.byte	$5F
	bit	$36
	rol	$2C, x
	rol	$37, x
	plp
	.byte	$27
	.byte	$5F
	and	$3C
	.byte	$FC
	inx
	and	($2B, x)
	bit	$3235
	rol	$2B, x
	bit	$305F
	bit	$243C
	.byte	$32
	rol	$FC24
	cpy	#$23
	.byte	$F7
	jsr	$FD0F
	lsr	a
	and	($24, x)
	rol	$36, x
	bit	$3736
	plp
	.byte	$27
	.byte	$5F
	and	$3C
	.byte	$FC
	dex
	and	($35, x)
	bit	$242E
	.byte	$5F
	rol	$38, x
	and	$2E38, x
	bit	$28FC
	.byte	$22
	.byte	$37
	bit	$27
	bit	$36
	.byte	$2B
	bit	$295F
	sec
	rol	$3D38
	.byte	$24
L968F:	.byte	$3A
	bit	$FC
	bne	L96B7
	.byte	$F7
	php
	bvc	L968F
	bpl	L969A
L969A:	sbc	$2187, x
	rol	$33, x
	plp
	rol	$2C
	bit	$2F
	.byte	$5F
	.byte	$37
	.byte	$2B
	bit	$31
	rol	$5F36
	.byte	$37
	.byte	$32
	.byte	$FC
	.byte	$E7
	and	($2E, x)
	bit	$3D
	sec
	.byte	$2B
	.byte	$2C
L96B7:	rol	$5F32
	.byte	$37
	.byte	$32
	and	$2C, x
	rol	$2B, x
	bit	$2430
	.byte	$FC
	cpy	#$23
	.byte	$F7
	jsr	$FD05
	txa
	and	($37, x)
	and	$24, x
	and	($36), y
	.byte	$2F
	bit	$37
	bit	$3132
	.byte	$FC
	sbc	$3621
	.byte	$37
	bit	$29
	and	#$FC
	cpy	#$23
	.byte	$F7
	jsr	$FDFF
	dec	$20
	.byte	$37
	and	$24, x
	and	($36), y
	.byte	$2F
	bit	$37
	plp
	.byte	$27
	.byte	$5F
	and	$3C
	.byte	$FC
	php
	and	($37, x)
	.byte	$32
	rol	$2B, x
	bit	$322E
L96FF:	.byte	$5F
	.byte	$3A
	bit	$37
	rol	$32, x
	and	($FC), y
	stx	$21
	and	$28, x
	and	$362C, y
	plp
	.byte	$27
	.byte	$5F
	.byte	$37
	plp
	.byte	$3B
	.byte	$37
	.byte	$5F
	and	$3C
	.byte	$FC
	iny
	and	($36, x)
	rol	$32
	.byte	$37
	.byte	$37
	.byte	$5F
	.byte	$33
	plp
	.byte	$2F
	.byte	$2F
	bit	$31
	.byte	$27
	.byte	$FC
	lsr	$22
	.byte	$37
	plp
	rol	$2B
	and	($2C), y
	rol	$24
	.byte	$2F
	.byte	$5F
	rol	$38, x
	.byte	$33
	.byte	$33
	.byte	$32
	and	$37, x
	.byte	$5F
	and	$3C
	.byte	$FC
	dey
	.byte	$22
	.byte	$27
	.byte	$32
	sec
	rol	a
	.byte	$5F
	and	$24
	.byte	$2E
	plp
L974B:	and	$FC, x
	cpy	#$23
	.byte	$F7
	.byte	$10
L9751:	.byte	$FF
	.byte	$F7
	php
	brk
	.byte	$F7
	php
	.byte	$0F
	.byte	$F7
	bpl	L974B
	sbc	$2148, x
	.byte	$33
	and	$32, x
	rol	a
	and	$24, x
	bmi	L978E
	.byte	$27
	.byte	$5F
	and	$3C
	.byte	$FC
	dex
	and	($2E, x)
	plp
	and	($2C), y
	rol	$2B
	bit	$305F
	bit	$36
	sec
	.byte	$37
	bit	$FC
	rol	a
	.byte	$22
	bmi	L97A4
	and	($24), y
	and	$38
	.byte	$5F
	.byte	$3C
	bit	$30
	.byte	$24
L9789:	and	($24), y
	.byte	$FC
	bne	L97B1
L978E:	.byte	$F7
	php
	bvc	L9789
	bpl	L9794
L9794:	sbc	$2125, x
L9797:	rol	$2A
	.byte	$5F
L979A:	.byte	$27
	plp
	rol	$2C, x
	rol	a
	and	($28), y
	.byte	$27
	.byte	$5F
	.byte	$25
L97A4:	.byte	$3C
	.byte	$FC
	txa
	and	($36, x)
	bit	$37
	.byte	$32
	rol	$2B, x
	bit	$295F
L97B1:	sec
	.byte	$27
	bit	$25
	bit	$FC
	ora	$22
	rol	$33, x
	plp
	rol	$2C
	bit	$2F
	.byte	$5F
	.byte	$37
	.byte	$2B
	bit	$31
	rol	$5F36
	.byte	$37
	.byte	$32
	.byte	$FC
	ror	a
	.byte	$22
	.byte	$2B
	.byte	$32
	.byte	$3A
	bit	$35
	.byte	$27
	.byte	$5F
	.byte	$33
	.byte	$2B
	bit	$2F2F
	bit	$3633
	.byte	$FC
	bne	L9802
	.byte	$F7
	php
	asl	a
	.byte	$F7
	php
	brk
	.byte	$F7
	php
	.byte	$0F
	sbc	$218A, x
	.byte	$27
	bit	$2835
	rol	$37
	plp
	.byte	$27
	.byte	$5F
	and	$3C
	.byte	$FC
	inx
	and	($2E, x)
	.byte	$32
	bit	$2B26
	bit	$315F
	.byte	$24
L9802:	rol	$3024
	sec
	and	$24, x
	.byte	$FC
	cpy	#$23
	.byte	$F7
	jsr	$FD0A
	txa
	and	($33, x)
	and	$32, x
	.byte	$27
	sec
	rol	$28
	.byte	$27
	.byte	$5F
	and	$3C
	.byte	$FC
	sbc	#$21
	.byte	$3C
	sec
	rol	$312C
	.byte	$32
	and	$38
	.byte	$5F
	rol	$2B
	bit	$2427
	.byte	$FC
	cpy	#$23
	.byte	$F7
	jsr	$FD0F
	sta	$20
	and	$24
	rol	$28, x
	.byte	$27
	.byte	$5F
	.byte	$32
	and	($5F), y
	.byte	$27
	and	$24, x
	rol	a
	.byte	$32
	and	($5F), y
	.byte	$34
	sec
	plp
	rol	$37, x
	.byte	$FC
	.byte	$0B
	and	($26, x)
	.byte	$32
	.byte	$33
	.byte	$3C
	and	$2C, x
	rol	a
	.byte	$2B
	.byte	$37
	.byte	$FC
	.byte	$63
	and	($24, x)
	and	$30, x
	.byte	$32
	and	$5F, x
	.byte	$33
	and	$32, x
	and	$2628
	.byte	$37
	.byte	$FC
	.byte	$74
	and	($01, x)
	ora	#$08
	asl	$5F
	ora	($09, x)
	php
	ora	#$FC
	.byte	$C3
	and	($25, x)
	bit	$2735
	.byte	$5F
	rol	$37, x
	sec
	.byte	$27
	bit	$FC32
	.byte	$D4
	and	($01, x)
	ora	#$08
	asl	$5F
	ora	($09, x)
	php
	ora	#$FC
	.byte	$23
	.byte	$22
	rol	$2C32
L9893:	rol	$2B
	bit	$365F
L9898:	sec
	rol	a
	bit	$243C
	bmi	L98C3
	.byte	$FC
	.byte	$34
	.byte	$22
	ora	($09, x)
	php
	asl	$5F
	ora	($09, x)
	php
	ora	#$FC
	.byte	$83
	.byte	$22
	.byte	$0C
	ora	$0F0E
	.byte	$5F
	rol	$32, x
	and	#$37
	.byte	$FC
	sty	$22, x
	ora	($09, x)
	php
	asl	$5F
	ora	($09, x)
	php
	.byte	$09
L98C3:	.byte	$FC
	ora	#$23
	plp
	and	($2C), y
	.byte	$3B
	.byte	$FC
	bpl	L98F0
	ora	($09, x)
	php
	asl	$5F
	ora	($09, x)
	php
	ora	#$FC
	iny
	.byte	$23
	.byte	$F7
	.byte	$03
	.byte	$FF
	.byte	$07
	.byte	$F7
	asl	$05
	.byte	$F7
	.byte	$03
	.byte	$0F
	.byte	$F7
	.byte	$03
	tax
	.byte	$F7
	ora	$00
	.byte	$F7
	ora	$AA
	.byte	$FC
	cpx	#$23
	.byte	$F7
L98F0:	ora	$00
	.byte	$F7
	.byte	$03
	tax
	.byte	$04
L98F6:	.byte	$F7
	.byte	$04
	brk
	.byte	$F7
	.byte	$03
	tax
	.byte	$F7
	.byte	$04
	brk
	.byte	$F7
	.byte	$03
	tax
	sbc	$218F, x
	bpl	L9918
	.byte	$12
	.byte	$FC
	ldx	$1321
	.byte	$14
	ora	$16, x
	.byte	$FC
	dec	$1721
	clc
	ora	$FC1A, y
	.byte	$EE
L9918:	and	($1B, x)
	.byte	$1C
	ora	$FC1E, x
	asl	$1F22
	jsr	$2221
	.byte	$FC
	cld
	.byte	$23
	.byte	$F7
	.byte	$10
L9929:	.byte	$FF
	sbc	$21AA, x
	rol	$403F, x
	eor	($42, x)
	.byte	$43
	.byte	$44
	eor	$46
	.byte	$47
	pha
	eor	#$FC
	dex
	and	($4A, x)
	.byte	$4B
	jmp	$4E4D

	.byte	$4F
	bvc	L9995
	.byte	$52
	.byte	$53
	.byte	$54
	eor	$FC, x
	bne	L996E
	.byte	$F7
	jsr	LFD00
	pha
	txa
	pha
	ldx	#$7B
L9954:	lda	L9DCD, x
	sta	$0320, x
	dex
	bpl	L9954
	pla
	tax
	pla
	rts

	pha
	tya
	pha
	ldy	#$0F
	lda	$40
	clc
	adc	L9E49
	sta	$3C
L996E:	lda	$41
	adc	L9E4A
	sta	$3D
L9975:	lda	($3C), y
	sta	$0100, y
	dey
	bpl	L9975
	pla
	tay
	pla
	rts

	pha
	tya
	pha
	ldy	#$00
	lda	$40
	ora	$41
	beq	L99B0
L998C:	lda	($3C), y
	sta	($3E), y
	lda	$40
	sec
	sbc	#$01
L9995:	sta	$40
	lda	$41
	sbc	#$00
	sta	$41
	ora	$40
	beq	L99B0
	inc	$3C
	bne	L99A7
L99A5:	inc	$3D
L99A7:	inc	$3E
	bne	L99AD
	inc	$3F
L99AD:	jmp	L998C

L99B0:	pla
	tay
L99B2:	pla
	rts

	tya
	pha
	lda	LA0CB
	sta	$22
	lda	LA0CC
	sta	$23
	ldy	$3C
	lda	($22), y
	sta	$C8
	iny
	lda	($22), y
	sta	$C9
	iny
	lda	($22), y
	sta	$CA
	iny
	lda	($22), y
	sta	$CB
	iny
	lda	($22), y
	ora	$CF
	sta	$CF
	iny
	lda	($22), y
	sta	$CE
	pla
	tay
	rts

	asl	$0E1B
	.byte	$1B
	cpy	$1A
	tax
	.byte	$1B
	bmi	L9A09
	cpy	$1A
	cmp	($1C), y
	cpx	#$1A
	tax
	.byte	$1B
	.byte	$87
	.byte	$1A
	cpy	$1A
	rol	$249A, x
	.byte	$1B
	cmp	($9C), y
	ora	$1C, x
	rol	$0E1A, x
	.byte	$1B
	tax
	.byte	$9B
	.byte	$15
L9A09:	.byte	$9C
	cpx	#$9A
	.byte	$87
	.byte	$1A
	cmp	$1B, x
	cmp	($1C), y
	.byte	$32
	txs
	bvs	L99B2
	bvs	L9A34
	jsr	$D51D
	.byte	$9B
	.byte	$32
	txs
	ora	$1C, x
	sta	($1D, x)
	cmp	$1B, x
	bit	$9B
	asl	$7B9D
	ora	$1C70, x
	.byte	$02
	ora	$1D7B, x
	.byte	$67
	.byte	$1B
	bmi	L9A5E
L9A34:	.byte	$9F
	.byte	$2F
	.byte	$27
	.byte	$7F
	.byte	$2F
	.byte	$23
	.byte	$5F
	rol	$3F1F
	jsr	$7017
	and	($1F, x)
	bvs	L9A68
	asl	$263B, x
	rol	$3A
	.byte	$22
	asl	$2759, x
	rol	$61
	plp
	rol	$2961
	.byte	$33
	eor	$2A, x
	.byte	$37
	lsr	$22, x
	lsr	$2389, x
	.byte	$5E
L9A5E:	.byte	$AB
	.byte	$27
	ror	$81
	plp
	ror	$2481
	rol	$A6
L9A68:	and	$2D
	ldx	$2B
	.byte	$33
	sta	$3B2C, x
	stx	$3B2D
	ldx	$3BFE
	lsr	a
	.byte	$FF
	.byte	$3B
	ror	a
	inc	L8A7B, x
	inc	LAA7D, x
	inc	L863F, x
	inc	LA67F, x
	brk
	and	($59), y
	.byte	$62
	.byte	$32
	adc	($63, x)
	.byte	$33
	adc	#$62
	.byte	$34
	adc	($61), y
	and	$77, x
	rts

	and	($19), y
	.byte	$82
	.byte	$32
	.byte	$21
L9A9B:	.byte	$83
L9A9C:	.byte	$33
	and	#$82
L9A9F:	.byte	$34
	and	($81), y
	and	$37, x
	.byte	$80
	rol	$5E, x
	rti

	sec
	ror	$42
	and	$436E, y
	.byte	$3A
	ror	$40, x
	rol	$1E, x
	ldy	#$38
	rol	$A2
	and	LA32E, y
	.byte	$3A
	rol	$A0, x
	inc	$22
	.byte	$5A
	inc	$62
	txa
	brk
	.byte	$3B
	.byte	$1F
	bvc	L9B04
	.byte	$27
	bvc	L9B08
	.byte	$1F
	bvs	L9B0C
	.byte	$27
	bvs	L9B0C
	.byte	$5F
	bcc	L9B10
	.byte	$67
	bcc	L9B16
	bit	$FE7C
	and	$FE62, y
	adc	$82, y
	.byte	$42
	.byte	$1C
	rti

	eor	$24
	rti

	lsr	a
	and	#$41
	eor	$4131
	.byte	$43
	.byte	$1C
	rts

	lsr	$24
	rts

	.byte	$4B
	and	#$61
	lsr	$6131
	rti

	clc
	jmp	($1841)

	sty	$1C44
	.byte	$80
	.byte	$47
	bit	$82
L9B04:	.byte	$4C
	rol	a
L9B06:	sta	($48, x)
L9B08:	bit	$A1
	eor	#$28
L9B0C:	cmp	($00, x)
	eor	$32, x
L9B10:	.byte	$64
	.byte	$53
	.byte	$2B
	rts

	.byte	$54
	.byte	$33
L9B16:	rts

	.byte	$53
	.byte	$6B
	.byte	$7C
	.byte	$54
	.byte	$73
	.byte	$7C
	.byte	$FF
	and	$72, x
	inc	L92F6, x
	brk
	.byte	$5C
	ora	$5D96, y
	jsr	$5D96
	rol	$5D9A
	and	$9E, x
	.byte	$5A
	.byte	$1B
	adc	($5B, x)
	.byte	$23
	adc	($5A, x)
	.byte	$5B
	sta	($5B, x)
	.byte	$63
	sta	($56, x)
	bit	$30
	.byte	$57
	.byte	$23
	bvc	L9B9B
	.byte	$2B
	bvc	L9B9F
	.byte	$33
	bvc	L9BA8
	.byte	$23
	bcc	L9BAC
	.byte	$2B
	bcc	L9BB0
	.byte	$33
	bcc	L9BB0
	.byte	$33
	bvs	L9BB3
	bit	$FF70
	.byte	$A7
	.byte	$73
	inc	$4837, x
	.byte	$FF
	.byte	$37
	pla
	.byte	$FF
	.byte	$77
	dey
	inc	LA877, x
	brk
	.byte	$62
	asl	$639F, x
	rol	$9F
	.byte	$63
	.byte	$74
	.byte	$9B
	eor	L953B, x
	.byte	$67
	.byte	$1C
	.byte	$62
	pla
	.byte	$23
	adc	($69, x)
	.byte	$23
	.byte	$5A
	ror	a
	.byte	$2B
	.byte	$63
	.byte	$67
	.byte	$5C
	.byte	$82
	pla
	.byte	$63
	sta	($69, x)
	.byte	$63
	txa
	ror	a
	.byte	$6B
	.byte	$83
	.byte	$64
	and	#$50
	adc	$31
	bvc	L9BF8
	and	$5F50, y
	and	#$90
	rts

	and	($90), y
	.byte	$61
L9B9B:	.byte	$39
L9B9C:	bcc	L9BFC
L9B9E:	.byte	$39
L9B9F:	bvs	L9BFF
	.byte	$32
	bvs	L9C02
	.byte	$2B
	.byte	$80
	.byte	$5E
	.byte	$2B
L9BA8:	bvs	L9BAA
L9BAA:	bvs	L9BD3
L9BAC:	.byte	$52
	.byte	$73
	.byte	$2F
	.byte	$52
L9BB0:	adc	($27), y
	.byte	$72
L9BB3:	.byte	$74
	.byte	$2F
	.byte	$73
	.byte	$72
	.byte	$27
	sta	($75), y
	.byte	$2F
	.byte	$92
	adc	$5021
	ror	$7021
	.byte	$6F
	and	($90, x)
	.byte	$6B
	ora	$6C70, y
	ora	$FE90, y
	.byte	$3C
	eor	$FF, x
	.byte	$3C
	adc	$7CFE
L9BD3:	sta	L8300
	.byte	$1A
	.byte	$4F
	sta	($15, x)
	rts

	.byte	$82
	ora	$7F60, x
	clc
	.byte	$42
	.byte	$80
	jsr	$7C41
	.byte	$12
	.byte	$9C
	.byte	$7A
	.byte	$1A
	.byte	$80
	.byte	$7B
	.byte	$22
	.byte	$80
	adc	LA01A, x
	ror	LA022, x
	ror	$2D, x
	bit	$2D77
L9BF8:	jmp	$2D78

	.byte	$6C
L9BFC:	adc	$6025, y
L9BFF:	.byte	$6B
	and	$70
L9C02:	eor	$1F, x
	and	$2155, y
	adc	($63, x)
	lda	($45, x)
	inc	$3E3D, x
	.byte	$FF
	and	$FE5E, x
L9C12:	adc	a:$7E, x
	rol	a
	.byte	$37
	lsr	$8D, x
	and	($98, x)
	stx	L9029
	sty	$1A
	sta	($85, x)
	.byte	$22
	sta	($86, x)
	rol	a
	.byte	$80
	.byte	$87
	and	($83), y
	dey
	and	($A0, x)
L9C2D:	.byte	$8F
	and	#$A0
	txa
	and	($A3), y
	.byte	$8B
	.byte	$22
	cpy	#$8C
	rol	a
	cpy	#$2C
	and	$2D8E, y
	and	L84AE, y
	.byte	$5A
	adc	($85, x)
	.byte	$62
	adc	($86, x)
	ror	a
	rts

	.byte	$87
	adc	($63), y
	dey
	adc	($40, x)
	.byte	$89
	adc	#$40
	bcc	L9CC4
	.byte	$43
	.byte	$8B
	.byte	$62
	jsr	$6A8C
	jsr	$2AFF
L9C5C:	.byte	$72
	sta	($1D), y
	tay
	inc	$4039, x
	.byte	$FF
	and	$FF60, y
	.byte	$7A
	.byte	$80
	inc	L883D, x
	inc	LA87D, x
	brk
	asl	$241C
	ldx	$24, y
	bit	$BD
	.byte	$3C
	bit	$BB
	.byte	$34
	sec
	.byte	$B7
	.byte	$14
	.byte	$44
	clv
	.byte	$1C
	.byte	$44
	lda	$4424, y
	tsx
	bit	LBE44
	.byte	$3C
	.byte	$44
	ldy	$5834, x
	cmp	($1C, x)
	.byte	$64
	.byte	$C2
	bit	$64
	.byte	$C3
	bit	$BF64
	.byte	$3C
	.byte	$64
	.byte	$C0
L9C9B:	.byte	$14
	jmp	($18C4)

	sty	$C5
	jsr	$C684
	plp
	sty	$C7
	bmi	L9C2D
	.byte	$CF
	sec
	dey
	iny
	clc
L9CAE:	ldy	$C9
	.byte	$20
	.byte	$A4
L9CB2:	dex
	plp
	ldy	$CB
L9CB6:	bmi	L9C5C
	bne	L9CF2
	tay
	cpy	$C420
L9CBE:	.byte	$CD
	plp
L9CC0:	cpy	$CE
L9CC2:	.byte	$30
	.byte	$C4
L9CC4:	cli
	.byte	$96
L9CC6:	.byte	$64
	.byte	$FF
	clv
	sei
	inc	L943C, x
	inc	LB47C, x
	brk
	.byte	$D4
	sec
	sec
	cmp	$32, x
	bvc	L9CAE
	.byte	$2F
	bvs	L9CB2
	.byte	$37
	bvs	L9CB6
	.byte	$3F
	sty	$2FD9
	bcc	L9CBE
	.byte	$37
	bcc	L9CC2
	.byte	$2F
	bcs	L9CC6
	.byte	$37
	bcs	L9CC0
	.byte	$27
	tay
	.byte	$D2
	.byte	$23
	.byte	$9C
L9CF2:	cmp	($23), y
	.byte	$7C
	.byte	$3F
	.byte	$63
	.byte	$74
	.byte	$FF
	.byte	$32
	adc	($FE), y
	.byte	$37
	.byte	$54
	inc	$C073, x
	brk
	inc	$19, x
	dec	$F7
L9D06:	and	($C6, x)
	sed
	and	#$C6
	sbc	$C631, y
	.byte	$FA
	ora	($1E), y
	.byte	$FB
	.byte	$19
L9D13:	asl	$15FC, x
	.byte	$3E
	.byte	$FD
L9D18:	jsr	$5D2E
	clc
	.byte	$32
	lda	$2E, x
	rol	$B3
	and	($6E), y
	ldy	$31, x
	stx	$1737
	.byte	$6B
	.byte	$9C
	ora	L9F8B, y
	.byte	$1F
	.byte	$54
	sta	$741F, x
	.byte	$9E
	.byte	$1F
	sty	$A0, x
	.byte	$1F
	ldy	$A1, x
	.byte	$27
	and	#$A2
	.byte	$27
	pha
	.byte	$A3
	.byte	$27
	pla
	ldy	$27
	dey
	lda	$27
	tay
	.byte	$A7
	and	#$B5
	tay
	.byte	$2F
	.byte	$5C
	lda	#$2F
	.byte	$7C
	tax
	.byte	$2F
	.byte	$9C
	.byte	$AB
	.byte	$33
	.byte	$3C
	lda	$5C37
	ldx	$7C37
	.byte	$AF
	.byte	$37
	.byte	$9C
	lda	($37), y
	ldy	$3BAC, x
	eor	($B0, x)
	.byte	$3F
	.byte	$9C
	.byte	$B2
	.byte	$3F
	lda	$3AB2, x
	eor	$27A6
	iny
	inc	$CC7F, x
	inc	$5C3D, x
	.byte	$FF
	and	a:$7C, x
	.byte	$F3
	.byte	$3F
	ldx	$F4, y
	.byte	$3F
	dec	$E6, x
	.byte	$34
	brk
	cpx	$0C3C
	.byte	$E2
	bit	$E720
	.byte	$34
	jsr	$3CED
	bit	$1CDD
	and	$24DE, y
	and	$2CE3, y
L9D98:	.byte	$43
	inx
	.byte	$34
	rti

	.byte	$DF
	bit	$5B
L9D9F:	beq	L9DD7
	eor	$2CE4, x
	.byte	$63
	sbc	#$34
	rts

	inc	$6C3C
	cpx	#$24
	adc	$2CE5, y
	.byte	$80
	nop
	.byte	$34
	.byte	$80
	sbc	($30), y
	stx	$3CEF
	sty	$3AF2
	.byte	$92
	sbc	($23, x)
	sta	$34EB, y
	ldy	#$F5
	bit	$29
	sbc	$BC, x
	jmp	$EBFE

	sty	$00, x
L9DCD:	.byte	$04
	ora	($0D, x)
	.byte	$13
	.byte	$04
	ora	($0F, x)
	.byte	$13
	.byte	$04
	.byte	$02
L9DD7:	asl	$0413
	.byte	$03
	.byte	$0F
	.byte	$13
	ora	$04
	.byte	$04
	asl	$05, x
	ora	$04
	.byte	$04
	ora	$06
	ora	($03, x)
	.byte	$0B
	clc
	.byte	$17
	asl	$09
	php
	ora	$14
	ora	#$08
	asl	$02
	ora	#$09
	ora	$04
	asl	$0B
	.byte	$0B
	.byte	$02
	asl	$0B
	.byte	$0C
	ora	$06, x
L9E02:	.byte	$0B
	ora	$0606
	.byte	$0C
	.byte	$0C
	.byte	$03
	asl	$0C
	ora	$060C
	ora	$020D
	.byte	$0C
	.byte	$04
	ora	$0F
	ora	$0403
	bpl	L9E32
	.byte	$0B
	brk
	.byte	$02
	clc
	.byte	$0C
	brk
	.byte	$12
	clc
	ora	$1300
	.byte	$1A
	ora	($01, x)
	.byte	$0C
	.byte	$1A
	ora	$0D06
	bpl	L9E34
	ora	$11
	.byte	$17
L9E32:	ora	($06, x)
L9E34:	asl	$0317
	.byte	$02
	.byte	$04
	.byte	$17
	.byte	$02
	.byte	$02
	ora	#$17
	asl	a
	ora	#$14
	asl	$0D, x
	ora	$02
	ora	$0309, x
	.byte	$17
L9E49:	.byte	$4B
L9E4A:	.byte	$9E
	ora	$03
	.byte	$03
	brk
	.byte	$0F
	ora	($01, x)
	.byte	$02
	adc	#$40
	lsr	a
	eor	$FAFA
	.byte	$FA
	.byte	$FA
	.byte	$07
	.byte	$03
	.byte	$04
	brk
	.byte	$0F
	ora	($01, x)
	.byte	$03
	adc	#$40
	lsr	a
	eor	$F826
	adc	#$FA
	ora	#$06
	asl	$00
	.byte	$0F
	ora	($02, x)
	.byte	$03
	.byte	$42
	rti

	ror	a
	lsr	$FAFA
	.byte	$FA
	.byte	$FA
	.byte	$0B
	php
	.byte	$07
	brk
	.byte	$0F
	.byte	$04
	.byte	$03
	ora	$45
	sed
	lsr	$6B69
	.byte	$FA
	.byte	$FA
	.byte	$FA
	.byte	$0B
	.byte	$0C
	ora	a:$02
	ora	($04, x)
	.byte	$0C
	plp
	.byte	$27
	.byte	$0C
	.byte	$1B
	.byte	$0F
	.byte	$0B
	.byte	$FA
	.byte	$FA
	asl	$0F0E
	.byte	$02
	brk
	ora	($05, x)
	.byte	$0C
	.byte	$3F
	lsr	a
	pha
	sed
	.byte	$42
	rti

	ror	a
	lsr	$1012
	.byte	$14
	brk
	.byte	$0F
	ora	($06, x)
	bpl	L9EC2
	asl	$1814
	and	($FA), y
	.byte	$FA
	.byte	$FA
	.byte	$14
	.byte	$12
	asl	$00, x
	.byte	$0F
	.byte	$02
	.byte	$07
L9EC2:	bpl	L9F03
	lsr	$F847
	.byte	$FA
	.byte	$FA
	.byte	$FA
	.byte	$FA
	.byte	$12
	.byte	$14
	.byte	$17
	.byte	$03
	brk
	asl	$08
	.byte	$12
	.byte	$3F
	.byte	$6B
	eor	($45, x)
	sed
	lsr	$6B69
	clc
	clc
	ora	$0E00, y
	.byte	$02
	asl	a
	ora	$4142, y
	.byte	$43
	.byte	$FA
	.byte	$FA
	.byte	$FA
	.byte	$FA
	.byte	$FA
	asl	$1A, x
	.byte	$14
	.byte	$92
	jsr	$B06
	.byte	$14
	.byte	$42
	rti

	ror	a
	lsr	$FA4B
	.byte	$FA
	.byte	$FA
	.byte	$1C
	asl	$1E, x
	brk
	.byte	$0F
	.byte	$04
	.byte	$0B
	.byte	$1E
L9F03:	.byte	$0F
	sed
	.byte	$0B
L9F06:	.byte	$13
	.byte	$1B
	.byte	$FA
	.byte	$FA
	.byte	$FA
	.byte	$1C
	asl	$1E, x
	.byte	$12
	and	($02), y
	ora	$2823
	ora	$0CF8, x
	ora	$FA, x
	.byte	$FA
	.byte	$FA
	bit	$2A
	asl	$00, x
	.byte	$0F
	.byte	$02
	asl	$1C28
	.byte	$1B
	.byte	$22
	.byte	$14
	clc
	and	($FA), y
	.byte	$FA
	plp
	asl	a:$22, x
	.byte	$1F
	.byte	$02
	bpl	L9F65
	and	($4C), y
	lsr	$6B
	.byte	$FA
	.byte	$FA
	.byte	$FA
	.byte	$FA
	bit	$2422
	bcc	L9FB0
	.byte	$04
	ora	($3C), y
	ora	$31, x
	.byte	$3B
	.byte	$0C
	.byte	$FA
	.byte	$FA
	.byte	$FA
	.byte	$FA
	asl	a
	.byte	$FF
	.byte	$04
	.byte	$03
	.byte	$FF
	sbc	($73), y
	asl	$3F
	.byte	$47
	.byte	$43
	adc	#$40
	lsr	a
	eor	$28FA
	rol	$24
	.byte	$13
	and	($04), y
	.byte	$12
	lsr	$26
	.byte	$43
L9F65:	eor	$F8
	lsr	$6B69
	.byte	$FA
	.byte	$32
	bit	$26
	rts

	.byte	$47
	.byte	$02
	.byte	$14
	bvc	L9FA5
	jmp	$6B46

	.byte	$4B
	eor	$FA43
	.byte	$2F
	plp
	.byte	$23
	lda	($F0), y
	.byte	$04
	.byte	$14
	eor	$3F, x
	lsr	$F847
	eor	($4E, x)
	.byte	$42
	.byte	$FA
L9F8B:	.byte	$34
	.byte	$32
	rol	$60
	.byte	$22
	ora	($16, x)
	.byte	$5A
	.byte	$42
	eor	($43, x)
	.byte	$3F
	lsr	a
	pha
	sed
	.byte	$FA
	sec
	bmi	L9FC8
	brk
	.byte	$4F
	.byte	$02
L9FA1:	clc
L9FA2:	.byte	$64
	ror	a
L9FA4:	.byte	$3F
L9FA5:	rti

	.byte	$FA
	.byte	$FA
	.byte	$FA
	.byte	$FA
	.byte	$FA
	.byte	$3C
	.byte	$5A
	.byte	$23
	brk
	.byte	$7F
L9FB0:	.byte	$02
	.byte	$1A
	ror	$2215
	.byte	$14
	clc
	and	($FA), y
	.byte	$FA
	.byte	$FA
	.byte	$44
	sec
	rol	$50B0
	.byte	$34
	.byte	$1C
	sei
	ora	$31, x
	.byte	$3B
	.byte	$0C
	.byte	$22
L9FC8:	bpl	L9FDF
	.byte	$FA
	sei
	.byte	$3C
	lsr	$00
	.byte	$FF
	beq	L9FD7
	asl	a
	eor	$F8
	.byte	$4E
	.byte	$44
L9FD7:	eor	$FAFA
	.byte	$FA
	bmi	LA005
	.byte	$32
	brk
L9FDF:	.byte	$DF
	ora	($06, x)
	iny
	eor	$F8
	lsr	$4243
	.byte	$4B
	lsr	$FA
	jmp	$374E

	rts

	.byte	$67
	ora	($21, x)
	.byte	$82
	.byte	$2F
	.byte	$34
	.byte	$0B
	.byte	$22
	bpl	LA00E
	.byte	$FA
	.byte	$FA
	lsr	$3A44
	jsr	$220
	.byte	$22
LA002:	sty	$4A3F
LA005:	pha
	sed
	ror	a
	.byte	$3F
	rti

	.byte	$FA
	.byte	$4F
	rti

	.byte	$32
LA00E:	brk
	.byte	$FF
	.byte	$FF
	and	$96
	.byte	$0F
	.byte	$12
	sed
	.byte	$22
	bpl	LA02E
	.byte	$FA
LA01A:	.byte	$FA
	lsr	$46, x
	.byte	$3C
	brk
	.byte	$7F
	.byte	$07
	plp
LA022:	.byte	$9B
	ror	a
	rti

	lsr	$4C31
	lsr	$6B
	.byte	$FA
	cli
	lsr	a
	.byte	$41
LA02E:	ora	#$7F
	.byte	$22
	and	$42A0
	rti

	eor	$F8
	lsr	$FA
	.byte	$FA
	.byte	$FA
	lsr	$50, x
	eor	($F9, x)
	.byte	$80
	.byte	$12
	.byte	$2B
	ldy	#$69
	.byte	$47
	lsr	$3F6A
	rti

	.byte	$FA
	.byte	$FA
	bvc	LA093
	eor	($06, x)
	.byte	$F7
	.byte	$F2
	.byte	$32
	.byte	$A5
LA053:	ora	$0BF8, y
	plp
	ora	$0CF8, x
	.byte	$FA
	lsr	$4652, x
	bpl	LA053
	ora	($36), y
	lda	$0A
	ora	($28), y
	.byte	$22
	bpl	LA07E
	.byte	$FA
	.byte	$FA
	.byte	$62
	.byte	$54
	lsr	$09
	.byte	$FF
	.byte	$72
	.byte	$3C
	stx	$6A, y
	lsr	$4269
	rti

	eor	$F8
	lsr	$64
	plp
	.byte	$A0
LA07E:	brk
	.byte	$2F
	adc	($41), y
LA082:	sty	$6B69
	lsr	$4B46
	lsr	$FA
	.byte	$FA
	adc	#$56
	.byte	$5A
	sbc	$F7, x
	.byte	$12
	lsr	$8C
LA093:	ora	$1F, x
	.byte	$0F
	sed
	and	#$22
	bpl	LA0B0
	sei
LA09C:	.byte	$5A
	.byte	$64
	.byte	$19
LA09F:	.byte	$F7
	.byte	$F2
	.byte	$64
	sty	$F847
	lsr	$4269
	rti

	eor	$F8
	.byte	$5A
	.byte	$4B
	.byte	$64
	.byte	$57
	.byte	$FF
LA0B0:	beq	LA0B2
LA0B2:	brk
	and	($3A), y
	.byte	$0C
	asl	$FA0C
	.byte	$FA
	.byte	$FA
	sty	L82C8
	asl	$F0FF
	brk
	brk
	and	($3A), y
	.byte	$0C
	asl	$FA0C
	.byte	$FA
	.byte	$FA
LA0CB:	.byte	$CD
LA0CC:	ldy	#$04
	.byte	$04
	.byte	$0F
	brk
	brk
	brk
	ora	$04
	asl	$00, x
	brk
	brk
	.byte	$07
	asl	$18
	ora	$00
	ora	($07, x)
	php
	.byte	$1F
	bpl	LA0E4
LA0E4:	.byte	$03
	.byte	$0C
	asl	a
	.byte	$23
	.byte	$14
	brk
	.byte	$03
	bpl	LA0F7
	rol	$18
	brk
	.byte	$03
	.byte	$12
	ora	($28), y
	.byte	$1A
	brk
	.byte	$07
LA0F7:	asl	$14, x
	rol	a:$1D
	.byte	$07
	asl	$3216, x
	bit	$00
	.byte	$0F
	.byte	$23
	.byte	$1F
	rol	$28, x
	brk
	.byte	$1F
	plp
	.byte	$23
	.byte	$3E
LA10C:	.byte	$32
	brk
	.byte	$1F
	bmi	LA139
	.byte	$3F
	.byte	$3A
	brk
	.byte	$3F
	.byte	$34
	bmi	LA15E
	rti

	brk
	.byte	$7F
	.byte	$3C
	.byte	$37
	lsr	a:$46
	.byte	$7F
	.byte	$44
	rti

	lsr	$48, x
	brk
	.byte	$FF
	pha
	lsr	$5C
	.byte	$5F
	brk
	.byte	$FF
	pha
	.byte	$4E
	.byte	$64
LA130:	.byte	$64
	ora	($FF, x)
	eor	$54, x
	.byte	$73
	jmp	($FF01)

LA139:	.byte	$57
	lsr	$82, x
	.byte	$73
	.byte	$03
	.byte	$FF
	.byte	$5C
	cli
	txa
	.byte	$80
	.byte	$03
	.byte	$FF
	.byte	$5F
	.byte	$5A
	sta	$87, x
	.byte	$03
	.byte	$FF
	adc	($5A, x)
	.byte	$9E
	.byte	$92
	.byte	$03
	.byte	$FF
	.byte	$63
	lsr	L99A5, x
	.byte	$03
	.byte	$FF
	.byte	$67
	.byte	$62
	tax
	lda	($03, x)
	.byte	$FF
	.byte	$71
LA15E:	.byte	$64
	ldx	$03A1
	.byte	$FF
	adc	$69, x
	ldy	$A8, x
	.byte	$03
	.byte	$FF
	adc	LBD6B, x
	.byte	$AF
	.byte	$03
	.byte	$FF
	.byte	$82
	.byte	$73
	.byte	$C3
	ldy	$03, x
	.byte	$FF
	.byte	$87
	sei
	iny
	ldx	$FF03, y
	sty	$D282
	iny
	.byte	$03
	.byte	$FF
	pla
	clc
	adc	#$01
	sta	$3E
	pla
	adc	#$00
	sta	$3F
	pha
	lda	$3E
	pha
	ldy	#$00
	lda	($3E), y
LA194:	jsr	LAEE1
	jsr	LA19B
	rts

LA19B:	sta	$64DC
LA19E:	.byte	$AD
LA19F:	inc	$64, x
	ora	#$80
	sta	$64F6
	jsr	LA1B1
	jsr	LA879
	lda	#$40
	sta	$64F6
LA1B1:	jsr	LA1D0
	jsr	LA1E4
	jsr	LA230
	bit	$64F6
	bmi	LA1CD
	lda	$64DC
	cmp	#$05
	bcc	LA1CD
	cmp	#$0B
	bcs	LA1CD
	brk
	ora	$07
LA1CD:	lda	$D7
	rts

LA1D0:	lda	#$00
	jsr	LA823
	lda	$64DC
	asl	a
	tay
	lda	($A1), y
	sta	$9F
	iny
	lda	($A1), y
	sta	$A0
	rts

LA1E4:	ldy	#$00
	lda	($9F), y
	sta	$64EC
	iny
	lda	($9F), y
	sta	$64E4
	asl	a
	sta	$64E5
	iny
	lda	($9F), y
	sta	$64E3
	iny
	lda	($9F), y
	sta	$64E6
	pha
	and	#$0F
	asl	a
	sta	$97
	pla
	and	#$F0
	lsr	a
	lsr	a
	lsr	a
	sta	$98
	iny
	lda	$64EC
	bpl	LA221
	lda	($9F), y
	sta	$64E7
	iny
	lda	($9F), y
	sta	$64EB
	iny
LA221:	bit	$64EC
	bvc	LA22C
	lda	($9F), y
	sta	$64F0
	iny
LA22C:	sty	$64E8
	rts

LA230:	jsr	LA248
LA233:	jsr	LA26A
	jsr	LA2B7
	jsr	LA30A
	jsr	LA5CE
	jsr	LA5F9
	bcc	LA233
	jsr	LA63D
	rts

LA248:	jsr	LA646
	lda	#$FF
	sta	$64FB
	lda	#$00
	sta	$64E0
	sta	$64E1
	sta	$64EE
	sta	$64ED
	sta	$64EA
	ldx	#$0F
LA263:	sta	$6496, x
	dex
	bpl	LA263
	rts

LA26A:	lda	#$5F
	sta	$64DD
	ldx	$64E0
	beq	LA291
	inx
	cpx	$64E3
	bne	LA2A8
	ldx	$64E1
	beq	LA289
	inx
	cpx	$64E5
	beq	LA28D
	lda	#$66
	bne	LA2B3
LA289:	lda	#$67
	bne	LA2B3
LA28D:	lda	#$69
	bne	LA2B3
LA291:	ldx	$64E1
	beq	LA2A0
	inx
	cpx	$64E5
	beq	LA2A4
	lda	#$61
	bne	LA2B3
LA2A0:	lda	#$64
	bne	LA2B3
LA2A4:	lda	#$65
	bne	LA2B3
LA2A8:	ldx	$64E1
	inx
	cpx	$64E5
	bne	LA2B6
	lda	#$68
LA2B3:	sta	$64DD
LA2B6:	rts

LA2B7:	lda	$64DD
	cmp	#$5F
	bne	LA304
	lda	$64EC
	and	#$20
	bne	LA2E7
	lda	$64E1
	lsr	a
	bcc	LA2E7
	lda	$64EA
	cmp	#$01
	bne	LA2E3
	lda	#$00
	sta	$64E0
	ldx	$64E1
	inx
	stx	$64E5
	pla
	pla
	jmp	LA233

LA2E3:	lda	#$00
	beq	LA2F1
LA2E7:	ldy	$64E8
	inc	$64E8
	lda	($9F), y
	bpl	LA301
LA2F1:	and	#$7F
	pha
	and	#$07
	sta	$64DF
	pla
	lsr	a
	lsr	a
	lsr	a
	sta	$64DE
	rts

LA301:	sta	$64DD
LA304:	lda	#$10
	sta	$64DE
	rts

LA30A:	lda	$64DE
	asl	a
	tax
	lda	LA6C3, x
	sta	$A1
	lda	LA6C4, x
	sta	$A2
	jmp	($A1)

LA31C:	lda	#$5F
	sta	$64DD
	jsr	LA600
LA324:	bit	$64F6
	bvs	LA32F
	jsr	LA546
	.byte	$4C
	.byte	$32
LA32E:	.byte	$A3
LA32F:	jsr	LA573
	dec	$64E2
	bne	LA324
	rts

	bit	$64EC
	bvc	LA34D
	lda	#$5F
	sta	$64DD
	jsr	LA546
	lda	#$63
	sta	$64DD
	jsr	LA546
LA34D:	lda	#$62
	sta	$64DD
	jsr	LA600
LA355:	jsr	LA546
	dec	$64E2
	bne	LA355
	rts

	lda	#$03
	sta	$64E2
	ldx	#$C5
	lda	$64DF
	and	#$04
	beq	LA36E
	ldx	#$C6
LA36E:	ldy	#$01
	jmp	LA61C

	lda	#$05
	sta	$64E2
	jsr	LA8BA
	jmp	LA62B

	lda	$64DF
	bne	LA38F
LA383:	lda	#$02
	sta	$64E2
	ldx	#$C7
	ldy	#$01
	jmp	LA61C

LA38F:	jsr	LF685
	jmp	LA383

	lda	#$05
	sta	$64E2
	ldx	#$BA
	ldy	#$02
	.byte	$4C
LA39F:	.byte	$1C
LA3A0:	.byte	$A6
LA3A1:	lda	$64DF
	cmp	#$01
LA3A6:	beq	LA3C6
	cmp	#$04
	beq	LA3F4
	cmp	#$05
	bcs	LA3E0
LA3B0:	lda	#$04
	sta	$64E2
	ldx	#$00
	ldy	$64E2
LA3BA:	lda	$B5, x
	sta	$64C9, y
	inx
	dey
	bne	LA3BA
	jmp	LA62B

LA3C6:	jsr	LA3B0
	lda	#$04
	sta	$64E2
	ldx	#$00
	ldy	$64E2
LA3D3:	lda	$64C6, x
	sta	$64C9, y
	inx
	dey
	bne	LA3D3
	jmp	LA62B

LA3E0:	lda	#$04
	sta	$64E2
	lda	$64DF
	sec
	sbc	#$05
	sta	$6030
	jsr	LF685
	jmp	LA3B0

LA3F4:	lda	#$08
	sta	$64E2
	jsr	LF685
	jmp	LA3C6

	lda	#$09
	sta	$64E2
	lda	$64DF
	cmp	#$03
	bcs	LA41F
	lda	$64DF
	adc	#$08
	tax
	lda	$A3, x
	jsr	LA685
	jsr	LA790
	jsr	LA62B
	jmp	LA7D7

LA41F:	jsr	LA651
	.byte	$20
	.byte	$57
LA424:	ldx	$48
	lda	$64DF
	cmp	#$03
	bne	LA43A
	pla
	pha
	cmp	#$12
	beq	LA437
	cmp	#$16
	bne	LA43A
LA437:	dec	$64E2
LA43A:	pla
	jsr	LA790
	jsr	LA62B
	lda	$64ED
	bne	LA458
	lda	$64DF
	cmp	#$03
	bne	LA458
	lda	$64EF
	cmp	#$12
	beq	LA45B
	cmp	#$16
	beq	LA45F
LA458:	jmp	LA7D7

LA45B:	lda	$C0
	bne	LA461
LA45F:	lda	$BF
LA461:	sta	$1A
	lda	#$00
	sta	$1B
	sta	$1C
	jsr	LA776
	lda	#$01
	sta	$64E2
	jsr	LA625
	jsr	LA62B
	jmp	LA7D7

	lda	#$09
	sta	$64E2
	jsr	LA651
	sec
	sbc	#$01
	jsr	LA7EB
	jsr	LA62B
	inc	$64EE
	rts

	jsr	LA776
	lda	#$05
	sta	$64E2
	lda	#$06
	jsr	LA823
	lda	$64EF
	beq	LA4B3
LA4A1:	asl	a
	tay
	lda	($A1), y
	sta	$1A
	iny
	lda	($A1), y
	sta	$1B
	lda	#$00
	sta	$1C
	jsr	LA625
LA4B3:	jmp	LA62B

	lda	#$00
	sta	$64EE
	lda	#$00
	sta	$64ED
	jsr	LA4CD
	sta	$64EA
	lda	$64E8
	sta	$64E9
	rts

LA4CD:	ldx	#$01
LA4CF:	lda	$A3, x
	cmp	#$FF
	beq	LA4D8
	inx
	bne	LA4CF
LA4D8:	dex
	lda	$A3
	cmp	#$01
	beq	LA4E5
	inx
	cmp	#$02
	beq	LA4E5
	inx
LA4E5:	txa
	rts

	lda	$64DF
	cmp	#$02
	beq	LA502
	and	#$03
	bne	LA503
	lda	$64EA
	beq	LA502
	dec	$64EA
	beq	LA502
	lda	$64E9
	sta	$64E8
LA502:	rts

LA503:	lda	#$00
	sta	$64E0
	sta	$64DF
	lda	$64E1
	and	#$01
	eor	#$01
	clc
	adc	#$01
	adc	$64E1
	sta	$64E5
	lsr	a
	sta	$64E4
	lda	$64E1
	and	#$01
	bne	LA531
	lda	#$61
	sta	$64DD
	jsr	LA546
	jmp	LA31C

LA531:	rts

	ldx	$64DF
	lda	LA6E5, x
	tax
	lda	#$03
	sta	$64E2
	ldy	#$01
	jmp	LA61C

	jmp	LA546

LA546:	lda	$64E1
	and	#$01
	beq	LA550
	lda	$64E3
LA550:	clc
	adc	$64E0
	tax
	lda	$64DD
	sta	$6436, x
	jsr	LA58B
	cmp	#$61
	bcs	LA573
	lda	$6435, x
	cmp	#$62
	bne	LA573
	lda	$64E0
	beq	LA573
	lda	#$63
	sta	$6435, x
LA573:	inc	$64E0
	lda	$64E0
	cmp	$64E3
	bcc	LA58A
	ldx	#$01
	stx	$64E2
	dex
	stx	$64E0
	inc	$64E1
LA58A:	rts

LA58B:	pha
	txa
	pha
	tya
	pha
	bit	$64F6
	bvs	LA5C8
	jsr	LA8AD
	lda	#$20
	sta	$1B
	lda	#$00
	sta	$1C
	lda	$64E1
	ldx	#$1B
	jsr	LA6EB
	lda	$1B
	clc
	adc	$64E0
	sta	$1B
	bcc	LA5B4
	inc	$1C
LA5B4:	clc
	lda	$1B
	adc	$0A
	sta	$0A
	lda	$1C
	adc	$0B
	sta	$0B
	ldy	#$00
	lda	$64DD
	sta	($0A), y
LA5C8:	pla
	tay
	pla
	tax
	pla
	rts

LA5CE:	lda	$64E1
	and	#$01
	ora	$64E0
	bne	LA5F8
	lda	$64F6
	bmi	LA5F8
	lda	$64E3
	lsr	a
	ora	#$10
	sta	$64A6
	lda	$64E6
	sta	$64A7
	clc
	adc	#$10
	sta	$64E6
	jsr	LABC4
	jsr	LA646
LA5F8:	rts

LA5F9:	lda	$64E1
	cmp	$64E5
	rts

LA600:	lda	$64DF
	bne	LA607
	lda	#$FF
LA607:	sta	$64E2
	clc
	lda	$64E3
	sbc	$64E0
	bcc	LA61B
	cmp	$64E2
	bcs	LA61B
	sta	$64E2
LA61B:	rts

LA61C:	jsr	LA622
	jmp	LA62B

LA622:	jsr	LA741
LA625:	jsr	LA753
	jmp	LA764

LA62B:	ldx	$64E2
	.byte	$BD
	.byte	$C9
LA630:	.byte	$64
	sta	$64DD
	jsr	LA546
	dec	$64E2
	bne	LA62B
	rts

LA63D:	lda	$64EC
	bpl	LA645
	jsr	LA8D1
LA645:	rts

LA646:	lda	#$5F
	ldx	#$3B
LA64A:	sta	$6436, x
	dex
	bpl	LA64A
	rts

LA651:	ldx	$64EE
	lda	$A4, x
	rts

	pha
	lda	$64DF
	cmp	#$03
	beq	LA665
	cmp	#$04
	beq	LA66B
	pla
	rts

LA665:	pla
	tax
	lda	LA688, x
	rts

LA66B:	pla
	cmp	#$13
	bcs	LA675
	tax
	lda	LA69C, x
	rts

LA675:	sec
	sbc	#$13
	cmp	#$05
	beq	LA682
	lsr	a
	tax
	.byte	$BD
LA67F:	txs
	ldx	$60
LA682:	lda	#$14
	rts

LA685:	tax
	.byte	$BD
	.byte	$A6
LA688:	ldx	$60
	.byte	$12
	asl	$13, x
	.byte	$17
	ora	$14, x
	.byte	$1A
	.byte	$22
	.byte	$23
	asl	$1B20, x
	and	($1D, x)
	.byte	$1C
	.byte	$1F
	.byte	$12
	.byte	$13
LA69C:	ora	$14, x
	ora	($02, x)
	.byte	$03
	.byte	$04
	.byte	$05
LA6A3:	asl	$07
	php
	ora	#$0A
	.byte	$0B
LA6A9:	.byte	$0C
	ora	$0F0E
	bpl	LA6C0
	brk
	ora	($02, x)
	.byte	$03
	.byte	$04
	ora	$06
	.byte	$07
	brk
	php
	ora	#$0A
	.byte	$0B
	.byte	$0C
	ora	a:$0E
LA6C0:	.byte	$0F
	bpl	LA6D4
LA6C3:	.byte	$1C
LA6C4:	.byte	$A3
	sec
	.byte	$A3
	lsr	$73A3, x
	.byte	$A3
	ror	L95A3, x
	.byte	$A3
	lda	($A3, x)
	.byte	$FF
	.byte	$A3
	.byte	$7A
LA6D4:	ldy	$8F
	ldy	$B6
	ldy	$32
	lda	$43
	lda	$E7
	ldy	$43
	lda	$43
	lda	$43
	.byte	$A5
LA6E5:	iny
	cmp	#$CC
	cmp	$CBCA
LA6EB:	sta	$64AF
	lda	#$00
	sta	$64B0
	sta	$64B1
LA6F6:	lsr	$64AF
	bcc	LA70C
	lda	$00, x
	clc
	adc	$64B0
	sta	$64B0
	lda	$01, x
	adc	$64B1
	sta	$64B1
LA70C:	asl	$00, x
	rol	$01, x
	lda	$64AF
	bne	LA6F6
	lda	$64B0
	sta	$00, x
	lda	$64B1
	sta	$01, x
	rts

LA720:	txa
	pha
	lda	#$00
	sta	$1D
	ldx	#$18
LA728:	asl	$1A
	rol	$1B
	rol	$1C
	rol	$1D
	sec
	lda	$1D
	sbc	#$0A
	bcc	LA73B
	sta	$1D
	inc	$1A
LA73B:	dex
	bne	LA728
	pla
	tax
	rts

LA741:	lda	#$00
	sta	$1C
	sta	$1B
	lda	$00, x
	sta	$1A
	dey
	beq	LA752
	lda	$01, x
	sta	$1B
LA752:	rts

LA753:	ldy	#$00
LA755:	jsr	LA720
	lda	$1D
	sta	$64CA, y
	iny
	cpy	$64E2
	bne	LA755
	rts

LA764:	ldx	$64E2
	dex
LA768:	lda	$64CA, x
	bne	LA775
	lda	#$5F
	sta	$64CA, x
	dex
	bne	LA768
LA775:	rts

LA776:	pha
	txa
	pha
	ldx	#$0C
	lda	#$5F
LA77D:	sta	$64CA, x
	dex
	bpl	LA77D
	pla
	tax
	pla
	rts

	jsr	LA7AE
	cpx	#$FF
	beq	LA7AD
	lda	$A3, x
LA790:	sta	$64EF
	jsr	LA7AE
	lda	$64ED
	and	#$01
	beq	LA79F
	lda	#$01
LA79F:	tay
	lda	$64EF
	and	#$3F
	sta	$64EF
	beq	LA7AD
LA7AA:	jsr	LA7BD
LA7AD:	rts

LA7AE:	jsr	LA776
	lda	$64ED
	lsr	a
	bcc	LA7BC
	lda	#$08
	sta	$64E2
LA7BC:	rts

LA7BD:	pha
	cmp	#$20
	bcc	LA7CB
	pla
	sbc	#$1F
	pha
	tya
	clc
	adc	#$02
	tay
LA7CB:	iny
	iny
	tya
	jsr	LA823
	pla
	beq	LA7BC
	jmp	LA842

LA7D7:	lda	$64ED
	eor	#$01
	bne	LA7E1
	inc	$64EE
LA7E1:	sta	$64ED
	rts

	sta	$64DD
	jmp	LA546

LA7EB:	pha
	jsr	LA776
	pla
	sta	$1A
	cmp	#$FF
	beq	LA800
	lda	#$01
	jsr	LA823
	.byte	$A5
LA7FC:	.byte	$1A
	jmp	LA842

LA800:	rts

LA801:	sta	$1A
	ldy	#$07
	lda	$64ED
	lsr	a
	bcc	LA80C
	iny
LA80C:	lda	$1A
	pha
	cmp	#$33
	bcc	LA818
	pla
	sbc	#$32
	pha
	iny
LA818:	tya
	jsr	LA823
	jsr	LA776
	pla
	jmp	LA842

LA823:	asl	a
	tax
	lda	LA830, x
	sta	$A1
	lda	LA831, x
	sta	$A2
	rts

LA830:	.byte	$6C
LA831:	.byte	$AF
	lsr	$BE, x
	.byte	$B7
	tsx
	.byte	$B7
	.byte	$BB
	.byte	$8F
	.byte	$BB
	.byte	$4F
	ldy	LBE0E, x
	bvs	LA7FC
	ldx	#$BD
LA842:	tax
	ldy	#$00
LA845:	dex
	beq	LA85C
LA848:	lda	($A1), y
	cmp	#$FF
	beq	LA855
	iny
	bne	LA848
	inc	$A2
	bne	LA848
LA855:	iny
	bne	LA845
	inc	$A2
	bne	LA845
LA85C:	tya
	clc
	adc	$A1
	sta	$A1
	bcc	LA866
	inc	$A2
LA866:	ldy	#$00
	ldx	$64E2
	lda	($A1), y
	cmp	#$FF
	beq	LA878
	sta	$64C9, x
	iny
	dex
	.byte	$D0
LA877:	.byte	$F3
LA878:	rts

LA879:	jsr	LA8AD
	.byte	$AD
LA87D:	sbc	$64
	sta	$1A
LA881:	ldy	#$00
	lda	$64E3
	sta	$1B
LA888:	lda	($0A), y
	cmp	#$FF
	bne	LA892
	lda	#$FE
	sta	($0A), y
LA892:	iny
	dec	$1B
	bne	LA888
	clc
	lda	$0A
	adc	#$20
	sta	$0A
	bcc	LA8A2
	.byte	$E6
LA8A1:	.byte	$0B
LA8A2:	dec	$1A
	bne	LA881
	brk
	.byte	$04
	.byte	$07
LA8A9:	jsr	LFF74
	rts

LA8AD:	lda	$97
	lsr	a
	sta	$3C
	lda	$98
	lsr	a
	sta	$3E
	jmp	LC596

LA8BA:	lda	#$05
	sta	$64E2
	lda	$BC
	sta	$1A
	lda	$BD
	sta	$1B
	lda	#$00
	sta	$1C
	jsr	LA753
	jmp	LA764

LA8D1:	lda	$64F6
	bmi	LA8E3
	jsr	LA918
	lda	#$80
	sta	$6507
	sta	$47
LA8E0:	jsr	LA8E4
LA8E3:	rts

LA8E4:	jsr	LA8ED
	jsr	LA992
	jmp	LA8E4

LA8ED:	jsr	LFF74
	jsr	LA96C
	lda	$47
	beq	LA8FD
	lda	$4F
	and	#$0F
	bne	LA900
LA8FD:	sta	$6507
LA900:	jsr	LC608
	.byte	$AD
LA904:	.byte	$07
	adc	$D0
	sbc	$AD
	.byte	$07
	adc	$25
	.byte	$47
	sta	$6507
	eor	$47
	sta	$64B8
	beq	LA8ED
	rts

LA918:	lda	#$00
	sta	$D8
	sta	$D9
	sta	$D7
	sta	$64F2
	sta	$64F3
	sta	$6507
	lda	$64E7
	lsr	a
	lsr	a
	lsr	a
	lsr	a
	tax
	lda	LABBE, x
	sta	$6506
	lda	$64DC
	cmp	#$0C
	bne	LA947
	ldx	$E5
	stx	$D9
	txa
	asl	a
	sta	$64F3
LA947:	lda	$64EB
	pha
	and	#$0F
	sta	$64F4
	clc
	adc	$64F2
	sta	$64F2
	pla
	and	#$F0
	lsr	a
	lsr	a
	lsr	a
	lsr	a
	sta	$64F5
	adc	$64F3
	sta	$64F3
	lda	#$05
	sta	$4F
	rts

LA96C:	ldx	#$5F
	lda	$4F
	and	#$1F
	cmp	#$10
	bcs	LA978
LA976:	ldx	#$42
LA978:	stx	$08
	lda	$97
	clc
	adc	$64F2
	sta	$64B6
	lda	$98
	clc
	adc	$64F3
LA989:	sta	$64B7
	jsr	LADC0
	jmp	LC690

LA992:	lda	$64B8
	lsr	a
	bcs	LA9AF
	lsr	a
	bcs	LA9CC
	lsr	a
	lsr	a
	lsr	a
	bcs	LA9DB
	lsr	a
	bcs	LAA06
	lsr	a
	bcs	LA9AC
	lsr	a
	bcc	LA9F4
LA9A9:	.byte	$4C
	iny
LA9AB:	tax
LA9AC:	jmp	LAA67

LA9AF:	lda	#$01
	sta	$6507
	jsr	LAB35
	lda	#$85
	brk
	.byte	$04
	.byte	$17
	lda	$D8
	sta	$20
	lda	$D9
	sta	$21
	jsr	LAB64
	pla
	pla
	lda	$D7
	rts

LA9CC:	lda	#$02
	sta	$6507
	jsr	LAB35
	pla
	pla
	lda	#$FF
	sta	$D7
	rts

LA9DB:	lda	#$10
	sta	$6507
	lda	$D9
	beq	LA9F4
	jsr	LAB30
	lda	$64DC
	cmp	#$05
	beq	LA9F5
	jsr	LABB2
	jsr	LAB35
LA9F4:	rts

LA9F5:	lda	#$03
	sta	$64F2
	lda	#$02
LA9FC:	sta	$64F3
	lda	#$00
	sta	$D9
	jmp	LAB35

LAA06:	lda	#$20
	sta	$6507
	lda	$64DC
	cmp	#$05
	beq	LAA4D
	cmp	#$0C
	bne	LAA1C
	lda	$D9
	cmp	#$02
	beq	LAA4C
LAA1C:	sec
	lda	$64E5
	sbc	#$03
	lsr	a
	cmp	$D9
	beq	LAA4C
	jsr	LAB30
	lda	$64DC
	cmp	#$0B
	bne	LAA34
	jsr	LAB3F
LAA34:	lda	$64F3
	bne	LAA41
	lda	$64F5
	sta	$64F3
	bne	LAA49
LAA41:	clc
	adc	#$02
	sta	$64F3
	inc	$D9
LAA49:	jsr	LAB35
LAA4C:	rts

LAA4D:	lda	$D9
	cmp	#$02
	beq	LAA4C
	jsr	LAB30
	lda	#$02
	sta	$D9
	lda	#$03
	sta	$64F2
	lda	#$06
	sta	$64F3
	jmp	LAB35

LAA67:	lda	#$40
	sta	$6507
	lda	$64DC
	cmp	#$05
	beq	LAAAE
	lda	$D8
	beq	LAAAD
	lda	$64DC
	cmp	#$0B
	.byte	$D0
LAA7D:	.byte	$17
	lda	$D9
	cmp	#$05
	bne	LAA95
	.byte	$A5
LAA85:	cld
	cmp	#$09
	bne	LAA95
	lda	#$06
	sta	$D8
	jsr	LAB30
	lda	#$0D
	bne	LAAA7
LAA95:	jsr	LAB30
	dec	$D8
	lda	$64E7
	and	#$0F
	sta	$1E
	lda	$64F2
	sec
	sbc	$1E
LAAA7:	sta	$64F2
	jsr	LAB35
LAAAD:	rts

LAAAE:	lda	$D9
	cmp	#$03
	beq	LAAAD
	jsr	LAB30
	lda	#$03
	sta	$D9
	lda	#$01
	sta	$64F2
	lda	#$04
	sta	$64F3
	jmp	LAB35

	lda	#$80
	sta	$6507
	lda	$64DC
	cmp	#$05
	beq	LAB16
	lda	$64E7
	beq	LAB15
	lda	$64DC
	cmp	#$0B
	bne	LAAF9
	lda	$D9
	cmp	#$05
	bne	LAAF9
	lda	$D8
	cmp	#$06
	bcc	LAAF9
	bne	LAB15
	jsr	LAB30
	lda	#$09
	sta	$D8
	lda	#$13
	bne	LAB0F
LAAF9:	ldx	$6506
	dex
	cpx	$D8
	beq	LAB15
	jsr	LAB30
LAB04:	inc	$D8
LAB06:	.byte	$AD
	.byte	$E7
LAB08:	.byte	$64
	and	#$0F
	clc
LAB0C:	adc	$64F2
LAB0F:	sta	$64F2
LAB12:	jsr	LAB35
LAB15:	rts

LAB16:	lda	$D9
	cmp	#$01
	beq	LAB15
	jsr	LAB30
	lda	#$01
	sta	$D9
	lda	#$07
	sta	$64F2
	lda	#$04
	sta	$64F3
	jmp	LAB35

LAB30:	ldx	#$5F
	jmp	LA978

LAB35:	lda	#$05
	sta	$4F
	jsr	LA976
	jmp	LFF74

LAB3F:	lda	$D9
	cmp	#$04
	bne	LAB63
	lda	$D8
	cmp	#$07
	beq	LAB5A
	cmp	#$08
	bcc	LAB63
	lda	#$09
	sta	$D8
	lda	#$13
	sta	$64F2
	bne	LAB63
LAB5A:	lda	#$06
	sta	$D8
	lda	#$0D
	sta	$64F2
LAB63:	rts

LAB64:	lda	$64DC
	cmp	#$0B
	beq	LAB8A
	lda	$20
	sta	$1E
	lda	#$00
	sta	$1F
	sec
	lda	$64E5
	sbc	#$03
	lsr	a
	tax
	inx
	txa
	ldx	#$1E
	jsr	LA6EB
	lda	$1E
	clc
	adc	$21
	sta	$D7
	rts

LAB8A:	lda	$21
	ldx	$64E7
	beq	LABA6
	and	#$0F
	sta	$1E
	lda	#$00
	sta	$1F
	ldx	#$1E
	lda	$6506
	jsr	LA6EB
	lda	$1E
	clc
	adc	$20
LABA6:	sta	$D7
	rts

	lda	$D8
	.byte	$85
LABAC:	.byte	$20
LABAD:	lda	$D9
	sta	$21
	rts

LABB2:	lda	$64F3
	sec
	sbc	#$02
	sta	$64F3
	dec	$D9
	rts

LABBE:	.byte	$02
	.byte	$0B
	lda	#$00
	bne	LABC6
LABC4:	lda	#$00
LABC6:	jsr	LABCC
	jmp	LADFA

LABCC:	pha
	lda	a:$03
	beq	LABD5
	jsr	LADFA
LABD5:	lda	#$00
	sta	$64AB
	pla
	jsr	LAD10
	lda	#$00
	sta	$64AC
	sta	$64AD
	lda	$64A6
	pha
	and	#$F0
	lsr	a
	lsr	a
	lsr	a
	sta	$64A9
	pla
	and	#$0F
	asl	a
	sta	$64AA
	sta	$64AE
	ldx	a:$04
LABFF:	lda	$0B
	sta	$64BB
	lda	$0A
	sta	$64BA
	and	#$1F
	.byte	$8D
LAC0C:	ldx	$64, y
	lda	#$20
	sec
	sbc	$64B6
	sta	$64B8
	.byte	$AD
LAC18:	tax
	.byte	$64
	sec
	sbc	$64B8
	sta	$64B9
	beq	LAC25
	bcs	LAC2E
LAC25:	lda	$64AA
	sta	$64B8
	jmp	LAC51

LAC2E:	jsr	LAC83
	lda	$64BB
	eor	#$04
	sta	$64BB
	lda	$64BA
	and	#$1F
	sta	$64B6
	lda	$64BA
	sec
	sbc	$64B6
	sta	$64BA
	lda	$64B9
	sta	$64B8
LAC51:	jsr	LAC83
	lda	$0B
	and	#$FB
	cmp	#$23
	bcc	LAC6D
	lda	$0A
	cmp	#$A0
	bcc	LAC6D
	and	#$1F
	sta	$0A
	lda	$0B
	and	#$FC
	jmp	LAC78

LAC6D:	lda	$0A
	clc
	adc	#$20
	sta	$0A
	lda	$0B
	adc	#$00
LAC78:	sta	$0B
	dec	$64A9
	bne	LABFF
	stx	a:$04
	rts

LAC83:	lda	$64BB
	ora	#$80
	sta	$0300, x
	lda	$64B8
	sta	$0301, x
	lda	$64BA
	sta	$0302, x
	inx
	inx
	inx
	lda	$64B8
	pha
	ldy	$64AC
LACA1:	lda	$6436, y
	sta	$0300, x
	inx
	iny
LACA9:	dec	$64B8
LACAC:	bne	LACA1
	sty	$64AC
	pla
	lsr	a
	sta	$64B8
	lda	$64A9
	and	#$01
	beq	LAD0C
	ldy	$64AD
	lda	$64BB
	sta	$64C3
	lda	$64BA
	sta	$64C2
LACCC:	txa
	pha
	tya
	pha
	lda	$64BB
	pha
	lda	$6496, y
	jsr	LAD36
	sta	$64B6
	pla
	sta	$64BB
	pla
	tay
	pla
	tax
	lda	$64C5
	sta	$0300, x
	inx
	lda	$64C4
	sta	$0300, x
	inx
	lda	$64B6
	sta	$0300, x
	inx
	iny
	inc	$64C2
	inc	$64C2
	inc	a:$03
	dec	$64B8
	bne	LACCC
	sty	$64AD
LAD0C:	inc	a:$03
	rts

LAD10:	pha
	jsr	LAD1F
	pla
	bne	LAD18
	rts

LAD18:	lda	$0B
	eor	#$04
	sta	$0B
	rts

LAD1F:	lda	$64A7
	asl	a
	.byte	$29
LAD24:	asl	LB68D, x
	.byte	$64
	lda	$64A7
	lsr	a
	lsr	a
	lsr	a
	and	#$1E
LAD30:	sta	$64B7
	jmp	LADC0

LAD36:	sta	$64C1
	lda	#$1F
	and	$64C2
	lsr	a
	lsr	a
	sta	$64BB
	lda	#$80
	and	$64C2
	lsr	a
	lsr	a
	lsr	a
	lsr	a
	ora	$64BB
	sta	$64BB
	lda	#$03
	and	$64C3
	asl	a
	asl	a
	asl	a
	asl	a
	ora	#$C0
	ora	$64BB
	sta	$64C4
	ldx	#$23
	lda	$64C3
	cmp	#$24
	bcc	LAD6E
	ldx	#$27
LAD6E:	stx	$64C5
	lda	$64C2
	and	#$40
	lsr	a
	lsr	a
	lsr	a
	lsr	a
	sta	$64BE
	lda	$64C2
	and	#$02
	ora	$64BE
	sta	$64BE
	lda	$64C4
	sta	$A1
	lda	$64C5
	and	#$07
	sta	$A2
	lda	$E0
	cmp	#$27
	bne	LAD9E
	lda	#$07
	sta	$A2
LAD9E:	ldy	#$00
	lda	($A1), y
	sta	$64BF
	lda	#$03
	ldy	$64BE
LADAA:	beq	LADB3
LADAC:	asl	a
LADAD:	asl	$64C1
LADB0:	dey
	bne	LADAC
LADB3:	eor	#$FF
	and	$64BF
	ora	$64C1
	ldy	#$00
	sta	($A1), y
	rts

LADC0:	lda	$06
	asl	a
	asl	a
	and	#$04
	ora	#$20
	sta	$0B
	lda	$64B6
	asl	a
	asl	a
	asl	a
	clc
	adc	$05
	sta	$0A
	bcc	LADDD
	lda	$0B
	eor	#$04
	sta	$0B
LADDD:	lda	$07
	lsr	a
	lsr	a
	lsr	a
	clc
	adc	$64B7
	cmp	#$1E
	bcc	LADEC
	sbc	#$1E
LADEC:	lsr	a
	ror	$0A
	lsr	a
	ror	$0A
	lsr	a
	ror	$0A
	ora	$0B
	sta	$0B
	rts

LADFA:	lda	#$80
	sta	$602C
	jmp	LFF74

	jsr	LAE2C
	jsr	LAEB8
	jsr	LA8D1
	jsr	LAE53
	jsr	LAEB2
	bcs	LAE19
	jsr	LA8E0
	.byte	$4C
	.byte	$0B
LAE18:	.byte	$AE
LAE19:	ldx	#$00
LAE1B:	lda	$64CA, x
	sta	$B5, x
	lda	$64CE, x
	sta	$64C6, x
	inx
	cpx	#$04
	bne	LAE1B
	rts

LAE2C:	lda	#$00
	sta	$6504
	sta	$6505
	lda	#$0E
	jsr	LA194
	lda	#$0B
	jsr	LA194
	lda	#$12
	sta	$64E7
	lda	#$21
	sta	$64EB
	lda	#$60
	ldx	#$0C
LAE4C:	sta	$64CA, x
	dex
	bpl	LAE4C
	rts

LAE53:	cmp	#$FF
	beq	LAE87
	cmp	#$1A
	bcc	LAE71
	cmp	#$21
	bcc	LAE7B
	cmp	#$3B
	bcc	LAE76
	cmp	#$3D
	bcc	LAE81
	cmp	#$3D
	beq	LAE87
	lda	#$08
	sta	$6504
	rts

LAE71:	clc
	adc	#$24
	bne	LAE96
LAE76:	sec
	sbc	#$17
	bne	LAE96
LAE7B:	tax
	lda	LAEBE, x
	bne	LAE96
LAE81:	tax
	lda	LAEA4, x
	bne	LAE96
LAE87:	lda	$6504
	beq	LAE95
	jsr	LAEBC
	dec	$6504
	jsr	LAEB8
LAE95:	rts

LAE96:	pha
	jsr	LAEBC
	pla
	ldx	$6504
	sta	$64CA, x
	jsr	LAEC2
LAEA4:	inc	$6504
	lda	$6504
	.byte	$C9
LAEAB:	php
	bcs	LAEB1
	jsr	LAEB8
LAEB1:	rts

LAEB2:	lda	$6504
	cmp	#$08
	rts

LAEB8:	lda	#$62
	bne	LAEBE
LAEBC:	lda	#$5F
LAEBE:	ldx	#$09
	bne	LAEC4
LAEC2:	ldx	#$08
LAEC4:	stx	$64B7
	sta	$08
	lda	$6504
	clc
	adc	#$0C
	sta	$64B6
	jsr	LADC0
	jmp	LC690

	eor	#$40
	jmp	$4F4B

	lsr	$4860
	.byte	$47
LAEE1:	pha
	ldx	#$40
	stx	$64F6
	ldx	#$03
LAEE9:	cmp	LAF20, x
	beq	LAEF3
	dex
	bpl	LAEE9
	bmi	LAEF8
LAEF3:	lda	#$00
	sta	$64F6
LAEF8:	pla
	pha
	cmp	#$03
	beq	LAF15
	cmp	#$04
	beq	LAF15
	cmp	#$09
	beq	LAF19
	cmp	#$02
	bne	LAF13
LAF0A:	lda	#$00
	sta	$D2
	sta	$D3
	jsr	LB850
LAF13:	pla
	rts

LAF15:	lda	#$85
	bne	LAF1B
LAF19:	lda	#$86
LAF1B:	brk
	.byte	$04
	.byte	$17
	pla
	rts

LAF20:	.byte	$03
	.byte	$04
	.byte	$02
	brk
	cmp	#$0B
	beq	LAF4C
	cmp	#$FF
	beq	LAF5C
	asl	a
	tay
	lda	LAF6C, y
LAF31:	sta	$3E
LAF33:	lda	LAF6D, y
	sta	$3F
	ldy	#$01
	lda	($3E), y
	sta	$6007
LAF3F:	iny
	lda	($3E), y
	sta	$6008
	iny
	.byte	$B1
LAF47:	rol	$098D, x
	rts

	rts

LAF4C:	lda	#$07
	sta	$6007
	lda	#$16
	sta	$6008
	.byte	$A9
LAF57:	and	($8D, x)
	ora	#$60
	rts

LAF5C:	lda	#$0C
	sta	$6007
	lda	#$1A
	sta	$6008
	lda	#$22
	.byte	$8D
	.byte	$09
LAF6A:	rts

	rts

LAF6C:	.byte	$B0
LAF6D:	.byte	$AF
	.byte	$C7
	.byte	$AF
	.byte	$4B
	bcs	LAFC7
	bcs	LAF0A
LAF75:	bcs	LAF31
	bcs	LAF33
	bcs	LAF47
	.byte	$B0
LAF7C:	.byte	$DA
	bcs	LAF6A
	bcs	LAF7C
	bcs	LAF90
LAF83:	lda	($94), y
	lda	($E0), y
	lda	($F7), y
	lda	($0B), y
	.byte	$B2
	eor	#$B2
	tay
	.byte	$B2
LAF90:	.byte	$C2
	.byte	$B2
	.byte	$DA
	.byte	$B2
	.byte	$F2
	.byte	$B2
	.byte	$1B
	.byte	$B3
	.byte	$33
	.byte	$B3
	.byte	$5C
	.byte	$B3
	sta	$B3
	.byte	$BF
	.byte	$B3
	cmp	$F3B3, y
	.byte	$B3
	jsr	$3AB4
	ldy	$67, x
	ldy	$94, x
	ldy	$D4, x
	ldy	$0D, x
	lda	$01, x
	asl	$08
	and	($89, x)
	bcs	LAF3F
	.byte	$2F
	and	LA082, y
	.byte	$2B
	.byte	$33
	sta	($90, x)
	bmi	LAFF4
	sta	($94, x)
	rol	a
	tya
	plp
	tay
LAFC7:	and	($0B, x)
	.byte	$14
	and	$88, x
	sta	$31
	bit	$30
	plp
	.byte	$44
	lda	($80), y
	stx	$36
	.byte	$37
	and	$28, x
	and	($2A), y
	.byte	$37
	.byte	$2B
	.byte	$44
	cld
	.byte	$80
	.byte	$87
	bit	$2A
	bit	$2C2F
	.byte	$37
	.byte	$3C
	.byte	$44
	cmp	L8480, y
	bmi	LB012
	.byte	$3B
	bit	$3830
	bmi	LAF75
LAFF4:	.byte	$2B
	.byte	$33
	.byte	$44
	.byte	$DC
	.byte	$80
	sty	$30
LAFFB:	bit	$3B
	bit	$3830
	bmi	LAF83
	bmi	LB037
	.byte	$44
	cmp	L8280, x
	bit	$37
	.byte	$37
	bit	$26
	rol	$3381
	.byte	$32
	.byte	$3A
LB012:	plp
	and	$44, x
	.byte	$DA
	.byte	$80
	sta	($27, x)
	plp
	and	#$28
	and	($36), y
	plp
	sta	($33, x)
	.byte	$32
	.byte	$3A
	plp
	and	$44, x
	.byte	$DB
	.byte	$80
	.byte	$82
	.byte	$3A
	plp
	bit	$33
	.byte	$32
	and	($44), y
	clv
	.byte	$87
	.byte	$83
	clv
	.byte	$83
	bit	$35
LB037:	bmi	LB06B
	.byte	$35
LB03A:	.byte	$44
	lda	L8387, y
	lda	$3682, y
	.byte	$2B
	bit	$2F28
	.byte	$27
	.byte	$44
	tsx
	.byte	$87
	.byte	$83
	tsx
	ora	($05, x)
	clc
	.byte	$92
	dey
	.byte	$80
	.byte	$80
	.byte	$80
	.byte	$80
	.byte	$80
	ora	$10
	asl	$08, x
	and	($8B, x)
	rol	$32
	bmi	LB08F
	bit	$31
	.byte	$27
	dey
	sta	($37, x)
	bit	$2F
	rol	$3684
	.byte	$33
LB06B:	plp
	.byte	$2F
	.byte	$2F
	sta	($36, x)
	.byte	$37
	bit	$37
	sec
	rol	$82, x
	bit	$2837
	bmi	LAFFB
	sta	($36, x)
	.byte	$37
	bit	$2C
	and	$36, x
	.byte	$82
	.byte	$27
	.byte	$32
	.byte	$32
	and	$80, x
	sta	($36, x)
	plp
	bit	$35
	rol	$2B
LB08F:	.byte	$82
	.byte	$37
	bit	$2E
	plp
	.byte	$80
	.byte	$80
	.byte	$03
	bpl	LB0AF
	php
	and	($8B, x)
	rol	$32
	bmi	LB0D0
	bit	$31
	.byte	$27
	dey
	sta	($29, x)
	bit	$2B2A
	.byte	$37
	.byte	$83
	rol	$33, x
	plp
LB0AE:	.byte	$2F
LB0AF:	.byte	$2F
	sta	($35, x)
	sec
	and	($85), y
	bit	$2837
	bmi	LB03A
	.byte	$80
	.byte	$0B
	.byte	$0C
	and	#$00
	and	($8B, x)
	rol	$33, x
	plp
	.byte	$2F
	.byte	$2F
	dey
	dec	$81, x
	cpy	#$E8
	sbc	#$A0
	.byte	$0B
	.byte	$0C
	.byte	$39
LB0D0:	brk
	ora	($88), y
	.byte	$D4
	sta	($BB, x)
	.byte	$82
	.byte	$BB
	inx
	sbc	#$A0
	php
	.byte	$12
	and	$00
	ora	($88), y
	cmp	$81, x
	ldy	$C881, x
	.byte	$82
	ldy	$E880, x
	sbc	#$80
	.byte	$03
	php
	and	$00
	and	($88, x)
	sta	($3C, x)
	plp
	rol	$80, x
	sta	($31, x)
	.byte	$32
	.byte	$80
	.byte	$80
	.byte	$03
	php
	and	$00
	and	($88, x)
	sta	($25, x)
	sec
	.byte	$3C
	.byte	$80
	sta	($36, x)
	plp
	.byte	$2F
	.byte	$2F
	.byte	$80
	ora	($07, x)
	clc
	.byte	$52
	dey
	sta	($24, x)
	sta	($25, x)
	sta	($26, x)
	sta	($27, x)
	sta	($28, x)
	sta	($29, x)
	sta	($2A, x)
	sta	($2B, x)
	sta	($2C, x)
	sta	($2D, x)
	sta	($2E, x)
	sta	($2F, x)
	sta	($30, x)
	sta	($31, x)
	sta	($32, x)
	sta	($33, x)
	sta	($34, x)
	sta	($35, x)
	sta	($36, x)
	sta	($37, x)
	sta	($38, x)
	sta	($39, x)
	sta	($3A, x)
	sta	($3B, x)
	sta	($3C, x)
	sta	($3D, x)
	sta	($49, x)
	sta	($40, x)
	sta	($4C, x)
	sta	($4B, x)
	sta	($4F, x)
	sta	($4E, x)
	.byte	$80
	sta	($0A, x)
	sta	($0B, x)
	sta	($0C, x)
	sta	($0D, x)
	sta	($0E, x)
	sta	($0F, x)
	sta	($10, x)
	sta	($11, x)
	sta	($12, x)
	sta	($13, x)
	sta	($14, x)
	sta	($15, x)
	sta	($16, x)
	sta	($17, x)
	sta	($18, x)
	sta	($19, x)
	sta	($1A, x)
	sta	($1B, x)
	sta	($1C, x)
	sta	($1D, x)
	sta	($1E, x)
	sta	($1F, x)
	sta	($20, x)
	sta	($21, x)
	sta	($22, x)
	sta	($23, x)
	sta	($48, x)
	sta	($47, x)
	sta	($25, x)
	bit	$26
	rol	$2882
	and	($27), y
	lda	($07, x)
	.byte	$12
	.byte	$74
	brk
	stx	$88
	sta	($3A, x)
	ora	($12), y
	.byte	$0C
	ora	($81), y
	bmi	LB1B2
	.byte	$1C
	.byte	$1C
	asl	a
	bpl	LB1B7
	.byte	$80
	.byte	$80
	sta	($36, x)
	ora	$0E0E, y
	.byte	$0D
	.byte	$81
LB1B2:	.byte	$27
	clc
	sta	($3C, x)
	clc
LB1B7:	asl	L8080, x
	sta	($3A, x)
	asl	a
	.byte	$17
	ora	$3781, x
	clc
	sta	($38, x)
	.byte	$1C
	asl	L804B
	.byte	$80
	.byte	$80
	stx	$29
	bit	$36
	.byte	$37
	.byte	$80
	.byte	$80
	stx	$31
	.byte	$32
	and	$30, x
	bit	$2F
	.byte	$80
	.byte	$80
	stx	$36
	.byte	$2F
	.byte	$32
	.byte	$3A
	.byte	$80
	ora	($02, x)
	.byte	$14
	.byte	$73
	dey
	sta	($2C, x)
	and	($33), y
	sec
	.byte	$37
	sta	($3C, x)
	.byte	$32
	sec
	and	$81, x
	and	($24), y
	bmi	LB21D
	jmp	$180

	.byte	$03
	.byte	$0C
	and	$8B, x
	and	($24), y
	bmi	LB228
	dey
	sta	($41, x)
	eor	($41, x)
	eor	($41, x)
	eor	($41, x)
	eor	($80, x)
	sta	($04, x)
	clc
	.byte	$42
	brk
	and	($88, x)
	sta	($26, x)
	.byte	$32
	and	($37), y
	bit	$3831
	plp
	sta	($24, x)
LB21D:	sta	($34, x)
	sec
	plp
	rol	$37, x
	.byte	$80
	sta	($26, x)
	.byte	$2B
	.byte	$24
LB228:	and	($2A), y
	plp
	sta	($30, x)
	plp
	rol	$36, x
	bit	$2A
	plp
	sta	($36, x)
	.byte	$33
	plp
	plp
	.byte	$27
	.byte	$80
	sta	($28, x)
	and	$24, x
	rol	$28, x
	sta	($24, x)
	sta	($34, x)
	sec
	plp
	rol	$37, x
	.byte	$80
	sta	($06, x)
	clc
	.byte	$42
	brk
	and	($88, x)
	sta	($26, x)
	.byte	$32
	and	($37), y
	bit	$3831
	plp
	sta	($24, x)
	sta	($34, x)
	sec
	plp
	rol	$37, x
	.byte	$80
	sta	($26, x)
	.byte	$2B
	bit	$31
	rol	a
	plp
	sta	($30, x)
	plp
	rol	$36, x
	bit	$2A
	plp
	sta	($36, x)
	.byte	$33
	plp
	plp
	.byte	$27
	.byte	$80
	sta	($25, x)
	plp
	rol	a
	bit	L8131
	bit	$81
	and	($28), y
	.byte	$3A
	sta	($34, x)
	sec
	plp
	rol	$37, x
	.byte	$80
	sta	($26, x)
	.byte	$32
	.byte	$33
	.byte	$3C
	sta	($24, x)
	sta	($34, x)
	sec
	plp
	rol	$37, x
	.byte	$80
	sta	($28, x)
	and	$24, x
	rol	$28, x
	sta	($24, x)
	sta	($34, x)
	sec
	plp
	rol	$37, x
	.byte	$80
	sta	($02, x)
	clc
	.byte	$42
	brk
	and	($88, x)
LB2AF:	sta	($25, x)
	plp
	rol	a
	bit	L8131
	bit	$81
	and	($28), y
	.byte	$3A
	sta	($34, x)
	sec
	plp
	rol	$37, x
	.byte	$80
	sta	($02, x)
	.byte	$14
	sta	$00, x
	and	($88, x)
	sta	($24, x)
	.byte	$27
	and	$3128, y
	.byte	$37
	sec
	and	$28, x
	sta	($2F, x)
	.byte	$32
	rol	a
	sta	($01, x)
	.byte	$80
	sta	($02, x)
	.byte	$14
	sta	$00, x
	and	($88, x)
	sta	($24, x)
	.byte	$27
	and	$3128, y
	.byte	$37
	sec
	and	$28, x
	sta	($2F, x)
	.byte	$32
	rol	a
	sta	($02, x)
	.byte	$80
	sta	($03, x)
	.byte	$14
	sta	$00, x
	and	($88, x)
	sta	($24, x)
	.byte	$27
	and	$3128, y
	.byte	$37
	sec
	and	$28, x
	sta	($2F, x)
	.byte	$32
	rol	a
	.byte	$81
LB308:	ora	($80, x)
	sta	($24, x)
	.byte	$27
	and	$3128, y
	.byte	$37
	sec
	and	$28, x
	sta	($2F, x)
	.byte	$32
	rol	a
	sta	($02, x)
	.byte	$80
	sta	($02, x)
	.byte	$14
	sta	$00, x
	and	($88, x)
	sta	($24, x)
	.byte	$27
	and	$3128, y
	.byte	$37
	sec
	and	$28, x
	sta	($2F, x)
	.byte	$32
	rol	a
	sta	($03, x)
	.byte	$80
	sta	($03, x)
	.byte	$14
	sta	$00, x
	and	($88, x)
	sta	($24, x)
	.byte	$27
	and	$3128, y
	.byte	$37
	sec
	and	$28, x
	sta	($2F, x)
	.byte	$32
	rol	a
	sta	($01, x)
	.byte	$80
	sta	($24, x)
	.byte	$27
	and	$3128, y
	.byte	$37
	sec
	and	$28, x
	sta	($2F, x)
	.byte	$32
	rol	a
	sta	($03, x)
	.byte	$80
	sta	($03, x)
	.byte	$14
	sta	$00, x
	and	($88, x)
	sta	($24, x)
	.byte	$27
	and	$3128, y
	.byte	$37
	sec
	and	$28, x
	sta	($2F, x)
	.byte	$32
	rol	a
	sta	($02, x)
	.byte	$80
	sta	($24, x)
	.byte	$27
	and	$3128, y
	.byte	$37
	sec
	and	$28, x
	sta	($2F, x)
	.byte	$32
	rol	a
	sta	($03, x)
	.byte	$80
	sta	($04, x)
	.byte	$14
	sta	$00, x
	and	($88, x)
	sta	($24, x)
	.byte	$27
	and	$3128, y
	.byte	$37
	sec
	and	$28, x
	sta	($2F, x)
	.byte	$32
	rol	a
	sta	($01, x)
	.byte	$80
	sta	($24, x)
	.byte	$27
	and	$3128, y
	.byte	$37
	sec
	and	$28, x
	sta	($2F, x)
	.byte	$32
	rol	a
	sta	($02, x)
	.byte	$80
	sta	($24, x)
	.byte	$27
	and	$3128, y
	.byte	$37
	sec
	and	$28, x
	sta	($2F, x)
	.byte	$32
	rol	a
	sta	($03, x)
	.byte	$80
	sta	($02, x)
	clc
	.byte	$63
	brk
	and	($88, x)
	sta	($24, x)
	.byte	$27
	and	$3128, y
	.byte	$37
	sec
	and	$28, x
	sta	($2F, x)
	.byte	$32
	rol	a
	sta	($01, x)
	.byte	$44
	lda	$80, x
	sta	($02, x)
	clc
	.byte	$63
	brk
	and	($88, x)
	sta	($24, x)
	.byte	$27
	and	$3128, y
	.byte	$37
	sec
	and	$28, x
	sta	($2F, x)
	.byte	$32
	rol	a
	sta	($02, x)
	.byte	$44
	ldx	$80, y
	sta	($03, x)
	clc
	.byte	$63
	brk
	and	($88, x)
	sta	($24, x)
	.byte	$27
	and	$3128, y
	.byte	$37
	sec
	and	$28, x
	sta	($2F, x)
	.byte	$32
	rol	a
	sta	($01, x)
	.byte	$44
	lda	$80, x
	sta	($24, x)
	.byte	$27
	and	$3128, y
	.byte	$37
	sec
	and	$28, x
	sta	($2F, x)
	.byte	$32
	rol	a
	sta	($02, x)
	.byte	$44
	ldx	$80, y
	sta	($02, x)
	clc
	.byte	$63
	brk
	and	($88, x)
	sta	($24, x)
	.byte	$27
	and	$3128, y
	.byte	$37
	sec
	and	$28, x
	sta	($2F, x)
	.byte	$32
	rol	a
	sta	($03, x)
	.byte	$44
	.byte	$B7
	.byte	$80
	sta	($03, x)
	clc
	.byte	$63
	brk
	and	($88, x)
	sta	($24, x)
	.byte	$27
	and	$3128, y
	.byte	$37
	sec
	and	$28, x
	sta	($2F, x)
	.byte	$32
	rol	a
	sta	($01, x)
	.byte	$44
	lda	$80, x
	sta	($24, x)
	.byte	$27
	and	$3128, y
	.byte	$37
	sec
	and	$28, x
	sta	($2F, x)
	.byte	$32
	rol	a
	sta	($03, x)
	.byte	$44
	.byte	$B7
	.byte	$80
	sta	($03, x)
	clc
	.byte	$63
	brk
	and	($88, x)
	sta	($24, x)
	.byte	$27
	and	$3128, y
	.byte	$37
	sec
	and	$28, x
	sta	($2F, x)
	.byte	$32
	rol	a
LB47C:	sta	($02, x)
	.byte	$44
	ldx	$80, y
	sta	($24, x)
	.byte	$27
	and	$3128, y
	.byte	$37
	sec
	and	$28, x
	sta	($2F, x)
	.byte	$32
	rol	a
	sta	($03, x)
	.byte	$44
	.byte	$B7
	.byte	$80
	sta	($04, x)
	clc
	.byte	$63
	brk
	and	($88, x)
	sta	($24, x)
	.byte	$27
	and	$3128, y
	.byte	$37
	sec
	and	$28, x
	sta	($2F, x)
	.byte	$32
	rol	a
	sta	($01, x)
	.byte	$44
	lda	$80, x
	sta	($24, x)
	.byte	$27
	and	$3128, y
	.byte	$37
	sec
	and	$28, x
	sta	($2F, x)
	.byte	$32
	rol	a
	sta	($02, x)
	.byte	$44
	ldx	$80, y
	sta	($24, x)
	.byte	$27
	and	$3128, y
	.byte	$37
	sec
	and	$28, x
	sta	($2F, x)
	.byte	$32
	rol	a
	sta	($03, x)
	.byte	$44
	.byte	$B7
	.byte	$80
	ora	($06, x)
	.byte	$14
	.byte	$73
	dey
	sta	($B4, x)
	.byte	$80
	sta	($2F, x)
	plp
	and	$2F28, y
	.byte	$82
	lda	($80, x)
	sta	($27, x)
	clc
	sta	($3C, x)
	clc
	asl	$3A81, x
	asl	a
	.byte	$17
	ora	$3781, x
	clc
	.byte	$80
	sta	($28, x)
	.byte	$1B
	asl	a
	.byte	$1C
	asl	$3781
	ora	($12), y
	.byte	$1C
	.byte	$80
	sta	($26, x)
	ora	($0A), y
	.byte	$1B
	asl	a
	.byte	$0C
	ora	$1B0E, x
	.byte	$4B
	.byte	$80
	.byte	$80
	.byte	$03
	php
	.byte	$3A
	brk
	and	($88, x)
	sta	($3C, x)
	plp
	rol	$80, x
	sta	($31, x)
	.byte	$32
	.byte	$80
LB51D:	jsr	LB532
	jsr	LB576
LB523:	jsr	LB5AF
	jsr	LB5E6
	jsr	LB594
	bcc	LB523
	jsr	LB85D
	rts

LB532:	sta	$1A
	and	#$F0
	lsr	a
	lsr	a
	lsr	a
	lsr	a
	sta	$1B
	txa
	asl	a
	asl	a
	asl	a
	asl	a
	adc	$1B
	clc
	adc	#$01
	sta	$31
	lda	#$02
	sta	$30
	ldx	#$9F
	jsr	LFD00
	lda	$1A
	and	#$0F
	sta	$1A
	tax
	beq	LB575
	ldy	#$00
LB55C:	ldx	#$9F
	lda	#$02
	jsr	LFD1C
	inc	$9F
	bne	LB569
	inc	$A0
LB569:	cmp	#$FC
	beq	LB571
	cmp	#$FF
	bne	LB55C
LB571:	dec	$1A
	bne	LB55C
LB575:	rts

LB576:	lda	#$00
	sta	$650A
	sta	$650C
	sta	$650E
	sta	$650B
	lda	#$08
	sta	$6509
	lda	$D2
	sta	$6510
	lda	$D3
	sta	$6511
	rts

LB594:	lda	$650E
	bne	LB59B
	clc
	rts

LB59B:	ldx	$D3
	lda	$6512
	bne	LB5A5
	stx	$6512
LB5A5:	lda	$6513
	bne	LB5AD
	stx	$6513
LB5AD:	sec
	rts

LB5AF:	jsr	LB635
	bit	$650C
	bmi	LB5E5
	lda	$D2
	sta	$64E0
	lda	#$00
	sta	$64B4
LB5C1:	ldx	$64B4
	lda	$6514, x
	inc	$64B4
	cmp	#$5F
	beq	LB5DA
	cmp	#$FA
	bcs	LB5DA
	inc	$64E0
	jsr	LB8F9
	bcs	LB5C1
LB5DA:	ldx	$64E0
	beq	LB5E2
	dec	$64E0
LB5E2:	jsr	LB915
LB5E5:	rts

LB5E6:	ldx	#$00
	stx	$650D
LB5EB:	ldx	$650D
	lda	$6514, x
	inc	$650D
	cmp	#$FA
	bcs	LB603
	pha
	jsr	LB9C7
	pla
	jsr	LB8F9
	bcs	LB5EB
	rts

LB603:	cmp	#$FB
	beq	LB61C
	cmp	#$FC
	beq	LB62B
	cmp	#$FD
	beq	LB619
	cmp	#$FE
	beq	LB619
LB613:	lda	#$FF
	sta	$650E
	rts

LB619:	jmp	LB91D

LB61C:	jsr	LB91D
	jsr	LBA59
	lda	$650A
	bne	LB62A
	jsr	LB924
LB62A:	rts

LB62B:	jsr	LB91D
	lda	#$00
	sta	$D2
	jmp	LB613

LB635:	lda	#$00
	sta	$650D
LB63A:	jsr	LB662
	cmp	#$FE
	bne	LB646
	bit	$650C
	bpl	LB63A
LB646:	cmp	#$50
	beq	LB64E
	cmp	#$57
	bne	LB653
LB64E:	ldx	#$01
	stx	$650A
LB653:	ldx	$650D
	sta	$6514, x
	inc	$650D
	jsr	LB8F9
	bcs	LB63A
	rts

LB662:	ldx	$650B
	beq	LB679
LB667:	lda	$652C, x
	inc	$650B
	cmp	#$FA
	bne	LB678
	ldx	#$00
	stx	$650B
	beq	LB679
LB678:	rts

LB679:	lda	#$02
	ldx	#$9F
	ldy	#$00
	jsr	LFD1C
	jsr	LBA9F
	cmp	#$EF
	beq	LB6FE
	cmp	#$F2
	beq	LB6BE
LB68D:	cmp	#$F0
	beq	LB6C7
	cmp	#$F3
	beq	LB6CA
	cmp	#$F5
	beq	LB6CD
	cmp	#$F6
	beq	LB6BB
	cmp	#$F8
	beq	LB6B5
	cmp	#$F7
	beq	LB6C4
	cmp	#$F9
	beq	LB6B2
	cmp	#$F4
	beq	LB6B8
	cmp	#$F1
	beq	LB6C1
	rts

LB6B2:	jmp	LB7E8

LB6B5:	jmp	LB7F9

LB6B8:	jmp	LB804

LB6BB:	jmp	LB7D8

LB6BE:	jmp	LB794

LB6C1:	jmp	LB80F

LB6C4:	jmp	LB757

LB6C7:	jmp	LB71E

LB6CA:	jmp	LB724

LB6CD:	jsr	LB6DA
LB6D0:	lda	#$FA
	sta	$652C, y
	ldx	#$00
	jmp	LB667

LB6DA:	lda	#$05
	sta	$64E2
	lda	$00
	sta	$1A
	lda	$01
	sta	$1B
	lda	#$00
	sta	$1C
	jsr	LA753
	jsr	LA764
	ldy	#$00
LB6F3:	lda	$64CA, x
	sta	$652C, y
	iny
	dex
	bpl	LB6F3
	rts

LB6FE:	lda	#$01
	sta	$64E2
	lda	$01
	bne	LB70C
	ldx	$00
	dex
	beq	LB719
LB70C:	lda	#$1C
	sta	$652C
	ldy	#$01
	inc	$64E2
	jmp	LB6D0

LB719:	ldy	#$00
	jmp	LB6D0

LB71E:	ldy	#$00
	lda	#$05
	bne	LB72D
LB724:	jsr	LB6DA
	lda	$64E2
	clc
	adc	#$06
LB72D:	sta	$64E2
	ldx	#$05
LB732:	lda	LB751, x
	sta	$652C, y
	iny
	dex
	bpl	LB732
	lda	$01
	bne	LB745
	ldx	$00
	dex
	beq	LB74E
LB745:	lda	#$1C
	sta	$652C, y
	iny
	inc	$64E2
LB74E:	jmp	LB6D0

LB751:	ora	$1217, x
	clc
	.byte	$33
	.byte	$5F
LB757:	jsr	LB75D
	jmp	LB6D0

LB75D:	lda	#$00
	sta	$64ED
	jsr	LB77E
	jsr	LB82B
	lda	#$5F
	sta	$652C, y
	iny
	tya
	pha
	inc	$64ED
	jsr	LB77E
	sty	$64AF
	pla
	tay
	jmp	LB830

LB77E:	lda	#$09
	sta	$64E2
	lda	#$20
	sta	$64EC
	lda	#$04
	sta	$64DF
	lda	$A3
	and	#$3F
	jmp	LA790

LB794:	jsr	LB75D
	jsr	LB79D
	jmp	LB6D0

LB79D:	jsr	LB7CB
	lda	$652C
	cmp	#$24
	beq	LB7BD
	cmp	#$2C
	beq	LB7BD
	cmp	#$38
	beq	LB7BD
	cmp	#$28
	beq	LB7BD
	cmp	#$32
	bne	LB7B7
LB7B7:	lda	#$5F
	sta	$652C
	rts

LB7BD:	jsr	LB7CB
	lda	#$17
	sta	$652C
	lda	#$5F
	sta	$652D
	rts

LB7CB:	ldx	#$26
LB7CD:	lda	$652C, x
	sta	$652D, x
	dex
	bpl	LB7CD
	iny
	rts

LB7D8:	lda	#$09
	sta	$64E2
	lda	$A3
	jsr	LA7EB
	jsr	LB82B
	jmp	LB6D0

LB7E8:	ldx	#$00
LB7EA:	lda	$A3, x
	sta	$652C, x
	inx
	cmp	#$FA
	bne	LB7EA
	ldx	#$00
LB7F6:	jmp	LB667

LB7F9:	jsr	LB87F
	jsr	LB81D
LB7FF:	ldx	#$00
	jmp	LB667

LB804:	lda	$E0
	jsr	LB89F
	jsr	LB81D
	jmp	LB7FF

LB80F:	lda	$E0
	jsr	LB89F
	jsr	LB81D
	jsr	LB79D
	jmp	LB7FF

LB81D:	ldx	#$00
LB81F:	lda	$6554, x
	sta	$652C, x
	inx
	cmp	#$FA
	bne	LB81F
	rts

LB82B:	sty	$64AF
	ldy	#$00
LB830:	ldx	$64AF
	beq	LB84E
	lda	#$00
	sta	$1A
	ldx	$64E2
LB83C:	lda	$64C9, x
	sta	$652C, y
	dex
	iny
	inc	$1A
	lda	$1A
	cmp	$64AF
	bne	LB83C
	rts

LB84E:	dey
	rts

LB850:	ldx	#$00
	lda	#$5F
LB854:	sta	$657C, x
	inx
	cpx	#$B0
	bcc	LB854
	rts

LB85D:	lda	#$08
	sta	$1A
	ldx	#$00
	ldy	#$00
LB865:	lda	#$16
	sta	$1C
LB869:	lda	$657C, y
	sta	$0665, x
	inx
	iny
	dec	$1C
	bne	LB869
	txa
	clc
	adc	#$0A
	tax
	dec	$1A
	bne	LB865
	rts

LB87F:	ldy	#$00
	ldx	#$00
LB883:	lda	$B5, x
	sta	$6554, y
	lda	$64C6, x
	sta	$6558, y
	inx
	iny
	cpy	#$04
	bne	LB883
	ldy	#$08
	jsr	LB8D9
LB899:	lda	#$FA
	sta	$6554, y
	rts

LB89F:	clc
	adc	#$01
	pha
	lda	#$00
	sta	$64ED
	lda	#$0B
	sta	$64E2
	pla
	jsr	LA801
	ldy	#$00
	jsr	LB8EA
	jsr	LB8D9
	lda	#$5F
	sta	$6554, y
	iny
	tya
	pha
	inc	$64ED
	lda	#$09
	sta	$64E2
	lda	$1A
	jsr	LA801
	pla
	tay
	jsr	LB8EA
	jsr	LB8D9
	jmp	LB899

LB8D9:	lda	$6553, y
	cmp	#$60
	beq	LB8E4
	cmp	#$5F
	bne	LB8E9
LB8E4:	dey
	bmi	LB8E9
	bne	LB8D9
LB8E9:	rts

LB8EA:	ldx	$64E2
LB8ED:	lda	$64C9, x
	sta	$6554, y
	iny
	dex
	bne	LB8ED
	rts

	rts

LB8F9:	cmp	#$FA
	bcs	LB913
	cmp	#$5F
	beq	LB913
	cmp	#$47
	beq	LB913
	cmp	#$48
	beq	LB913
	cmp	#$40
	beq	LB913
	cmp	#$52
	beq	LB913
	sec
	rts

LB913:	clc
	rts

LB915:	lda	$64E0
	cmp	#$16
	bcs	LB924
	rts

LB91D:	lda	$D2
	sta	$64E0
	beq	LB940
LB924:	ldx	$D3
	inx
	cpx	#$08
	bcs	LB941
	lda	$6509
	lsr	a
	lsr	a
	eor	#$03
	clc
	adc	$D3
	sta	$D3
LB937:	lda	$650A
	sta	$64E0
	sta	$D2
	clc
LB940:	rts

LB941:	jsr	LB967
	lda	$6509
	cmp	#$04
	bne	LB94E
	jsr	LB967
LB94E:	lda	#$13
	sta	$64AF
	lda	#$00
	sta	$64B0
	jsr	LFF74
LB95B:	jsr	LB990
	lda	$64AF
	cmp	#$1B
	bcc	LB95B
	bcs	LB937
LB967:	ldx	#$00
LB969:	lda	$6592, x
	and	#$7F
	cmp	#$76
	bcs	LB980
	pha
	lda	$657C, x
	and	#$7F
	cmp	#$76
	pla
	bcs	LB980
	.byte	$9D
	.byte	$7C
LB97F:	.byte	$65
LB980:	inx
	cpx	#$9A
	bne	LB969
	lda	#$5F
LB987:	sta	$657C, x
	inx
	cpx	#$B0
	bne	LB987
	rts

LB990:	jsr	LB9A0
	inc	$64AF
	jsr	LB9A0
	jsr	LFF74
	inc	$64AF
	rts

LB9A0:	lda	$64AF
	sta	$64B7
	lda	#$05
	sta	$64B6
LB9AB:	ldx	$64B0
	lda	$657C, x
	sta	$08
	jsr	LADC0
	.byte	$20
LB9B7:	bcc	LB97F
	inc	$64B0
	inc	$64B6
	lda	$64B6
	cmp	#$1B
	bne	LB9AB
	rts

LB9C7:	pha
	lda	$D2
	sta	$64E0
	jsr	LB915
	lda	$D3
	jsr	LBAA6
	adc	$D2
	tax
	pla
	cmp	#$5F
	beq	LB9EE
	cmp	#$50
	bne	LB9F5
	ldy	$D2
	cpy	#$01
	bne	LB9F5
	dey
	sty	$D2
	dex
	jmp	LB9F5

LB9EE:	ldy	$D2
	cpy	$650A
	beq	LBA3B
LB9F5:	pha
	lda	$657C, x
	sta	$08
	tay
	pla
	cpy	#$5F
	bne	LBA06
	sta	$657C, x
	sta	$08
LBA06:	lda	$650A
	beq	LBA1B
	lda	$08
	cmp	#$5F
	bcs	LBA1B
	lda	$D2
	lsr	a
	bcc	LBA1B
	lda	#$90
	brk
	.byte	$04
	.byte	$17
LBA1B:	lda	$D2
	clc
	adc	#$05
	sta	$64B6
	lda	$D3
	clc
	adc	#$13
	sta	$64B7
	jsr	LADC0
	jsr	LC690
	ldx	$E5
LBA33:	jsr	LFF74
	dex
	bpl	LBA33
	inc	$D2
LBA3B:	rts

	lda	$08
	ldx	#$04
LBA40:	cmp	LBA54, x
	beq	LBA52
	dex
	bpl	LBA40
	cmp	#$24
	bcc	LBA50
	cmp	#$56
	bcc	LBA52
LBA50:	clc
	rts

LBA52:	sec
	rts

LBA54:	asl	a
	.byte	$12
	asl	$180E, x
LBA59:	jsr	LBA97
	bne	LBA6D
	lda	#$10
	sta	$4F
LBA62:	jsr	LBA76
	jsr	LFF74
	jsr	LBA97
	beq	LBA62
LBA6D:	jsr	LBA80
	lda	$650A
	sta	$D2
	rts

LBA76:	ldx	#$43
	lda	$4F
	and	#$1F
	cmp	#$10
	bcs	LBA82
LBA80:	ldx	#$5F
LBA82:	stx	$08
	lda	#$10
	sta	$64B6
	lda	$D3
	clc
	adc	#$13
	sta	$64B7
	jsr	LADC0
	jmp	LC690

LBA97:	jsr	LC608
	lda	$47
	and	#$03
	rts

LBA9F:	inc	$9F
	bne	LBAA5
	inc	$A0
LBAA5:	rts

LBAA6:	sta	$1A
	lda	#$00
	sta	$1B
	ldx	#$1A
	lda	#$16
	jsr	LA6EB
	lda	$1A
	clc
	rts

	and	$0A
	asl	$0B, x
	clc
	clc
	.byte	$FF
	rol	$15
	asl	$FF0B, x
	rol	$18
	ora	$0E19, y
	.byte	$1B
	.byte	$FF
	.byte	$2B
	asl	a
	.byte	$17
	ora	$25FF
	.byte	$1B
	clc
	asl	a
	ora	$29FF
	ora	$0A, x
	asl	$0E, x
	.byte	$FF
	plp
	.byte	$1B
	ora	$121B
	.byte	$0C
	.byte	$14
	rti

	.byte	$1C
	.byte	$FF
	rol	$15
	clc
	ora	$0E11, x
	.byte	$1C
	.byte	$FF
	.byte	$2F
	asl	$1D0A
	ora	($0E), y
	.byte	$1B
	.byte	$FF
	rol	$11
	asl	a
	.byte	$12
	.byte	$17
	.byte	$FF
	.byte	$2B
	asl	a
	ora	$0F, x
	.byte	$FF
	and	#$1E
	ora	$15, x
	.byte	$FF
	bmi	LBB11
	bpl	LBB1B
	.byte	$0C
	.byte	$FF
	plp
	.byte	$1B
	ora	$121B
	.byte	$0C
LBB11:	.byte	$14
	rti

	.byte	$1C
	.byte	$FF
	rol	$16, x
	asl	a
	ora	$15, x
	.byte	$FF
LBB1B:	.byte	$2F
	asl	a
	.byte	$1B
	bpl	LBB2E
	.byte	$FF
	rol	$12, x
	ora	$1F, x
	asl	$FF1B
	.byte	$2B
	asl	$0B1B
	.byte	$FF
	.byte	$37
LBB2E:	clc
	.byte	$1B
	.byte	$0C
	ora	($FF), y
	.byte	$27
	.byte	$1B
	asl	a
	bpl	LBB50
	.byte	$17
	rti

	.byte	$1C
	.byte	$FF
	.byte	$3A
	.byte	$12
	.byte	$17
	bpl	LBB5D
	.byte	$FF
	bmi	LBB4E
	bpl	LBB58
	.byte	$0C
	.byte	$FF
	and	#$0A
	.byte	$12
	.byte	$1B
	.byte	$22
	.byte	$FF
LBB4E:	and	$0A
LBB50:	ora	$15, x
	.byte	$5F
	clc
	.byte	$0F
	.byte	$FF
	.byte	$37
	asl	a
LBB58:	.byte	$0B
	ora	$0E, x
	.byte	$1D
	.byte	$FF
LBB5D:	and	#$0A
	.byte	$12
	.byte	$1B
	.byte	$22
	.byte	$FF
	rol	$12, x
	ora	$1F, x
	asl	$FF1B
	rol	$1D, x
	asl	a
	.byte	$0F
	.byte	$0F
	.byte	$5F
	clc
	.byte	$0F
	.byte	$FF
	rol	$1D, x
	clc
	.byte	$17
	asl	$5F1C
	clc
	.byte	$0F
	.byte	$FF
	rol	a
	jsr	$E0A
	ora	$12, x
	.byte	$17
	rti

	.byte	$1C
	.byte	$FF
	and	$0A, x
	.byte	$12
	.byte	$17
	.byte	$0B
	clc
	jsr	$26FF
	asl	$1C1B, x
	asl	$FF0D
	.byte	$27
	asl	$1D0A
	ora	($FF), y
	and	#$12
	bpl	LBBB1
	ora	$1B0E, x
	rti

	.byte	$1C
	.byte	$FF
	plp
	.byte	$1B
	ora	$121B
	.byte	$0C
	.byte	$14
	rti

	.byte	$1C
	.byte	$FF
	.byte	$36
LBBB1:	asl	$1B0C
	asl	$FF1D
	.byte	$33
	clc
	ora	$0E, x
	.byte	$FF
	.byte	$FF
	rol	$20, x
	clc
	.byte	$1B
	ora	$24FF
	and	($0E, x)
	.byte	$FF
	rol	$20, x
	clc
	.byte	$1B
	ora	$36FF
	jsr	$1B18
	ora	$36FF
	jsr	$1B18
	ora	$FFFF
	bit	$1B
	asl	$18, x
	.byte	$1B
	.byte	$FF
	bmi	LBBEC
	.byte	$12
	ora	$FF, x
	.byte	$33
	ora	$0A, x
	ora	$FF0E, x
	.byte	$33
LBBEC:	ora	$0A, x
	ora	$FF0E, x
	bit	$1B
	asl	$18, x
	.byte	$1B
	.byte	$FF
	bit	$1B
	asl	$18, x
	.byte	$1B
	.byte	$FF
	rol	$11, x
	.byte	$12
	asl	$0D15
	.byte	$FF
	rol	$11, x
	.byte	$12
	asl	$0D15
	.byte	$FF
	rol	$11, x
	.byte	$12
	.byte	$0E
	.byte	$15
LBC10:	ora	$FFFF
	.byte	$FF
	rol	$0C, x
	asl	a
	ora	$0E, x
	.byte	$FF
	.byte	$FF
	rol	$220E
	.byte	$FF
	.byte	$3A
	asl	a
	ora	$1B0E, x
	.byte	$FF
	.byte	$2F
	.byte	$12
	bpl	LBC3A
	ora	$FFFF, x
	and	#$15
	asl	$0E1D, x
	.byte	$FF
	.byte	$2B
	asl	a
	.byte	$1B
	ora	$35FF, y
	asl	a
	.byte	$12
LBC3A:	.byte	$17
	.byte	$FF
	rol	$1E, x
	.byte	$17
	ora	$12, x
	bpl	LBC54
	ora	$2FFF, x
	clc
	.byte	$1F
	asl	$27FF
	.byte	$1B
	clc
	ora	$25FF, y
	asl	$1D15
	.byte	$FF
LBC54:	and	($0E), y
	.byte	$0C
	.byte	$14
	ora	$0A, x
	.byte	$0C
	asl	$35FF
	.byte	$12
	.byte	$17
	.byte	$10
LBC61:	.byte	$FF
	.byte	$37
	clc
	.byte	$14
	asl	$FF17
	.byte	$33
	asl	a
	.byte	$1C
	.byte	$1C
	asl	a
	bpl	LBC7D
	.byte	$FF
	rol	$15, x
	.byte	$12
	asl	$0E, x
	.byte	$FF
	and	$0E, x
	ora	$27FF
	.byte	$1B
	asl	a
LBC7D:	.byte	$14
	asl	$FF0E
	rol	a
	ora	($18), y
	.byte	$1C
	ora	$30FF, x
	asl	a
	bpl	LBC9D
	.byte	$0C
	.byte	$12
	asl	a
	.byte	$17
	.byte	$FF
	bmi	LBC9C
	bpl	LBCA6
	ora	$0A1B
	.byte	$14
	asl	$FF0E
	.byte	$36
LBC9C:	.byte	$0C
LBC9D:	clc
	.byte	$1B
	ora	$1812, y
	.byte	$17
	.byte	$FF
	.byte	$27
	.byte	$1B
LBCA6:	asl	$1712, x
	.byte	$FF
	.byte	$33
	clc
	ora	$1D, x
	asl	$101B
	asl	$1C12
	ora	$27FF, x
	.byte	$1B
	clc
	ora	$15, x
	.byte	$FF
	.byte	$27
	.byte	$1B
	asl	a
	.byte	$14
	asl	$160E
	asl	a
	.byte	$FF
	rol	$14, x
	asl	$0E15
	ora	$1718, x
	.byte	$FF
	.byte	$3A
	asl	a
	.byte	$1B
	ora	$18, x
	.byte	$0C
	.byte	$14
	.byte	$FF
	bmi	LBCE6
	ora	$150A, x
	.byte	$FF
	.byte	$3A
	clc
	ora	$0F, x
	.byte	$FF
	.byte	$3A
	.byte	$1B
	asl	a
	.byte	$12
	.byte	$1D
LBCE6:	ora	($FF), y
	bmi	LBCF8
	ora	$150A, x
	.byte	$FF
	rol	$19, x
	asl	$1D0C
	asl	$FF1B
	.byte	$3A
	clc
LBCF8:	ora	$0F, x
	ora	$18, x
	.byte	$1B
	ora	$27FF
	.byte	$1B
	asl	$1712, x
	ora	$18, x
	.byte	$1B
	ora	$27FF
	.byte	$1B
	clc
	ora	$15, x
	asl	$0A, x
	bpl	LBD24
	.byte	$FF
	.byte	$3A
	.byte	$22
	.byte	$1F
	asl	$171B
	.byte	$FF
	and	$18, x
	bpl	LBD3C
	asl	$3AFF
	.byte	$1B
	asl	a
	.byte	$12
LBD24:	ora	$FF11, x
	rol	a
	clc
	ora	$0E, x
	asl	$FF, x
	rol	a
	clc
	ora	$0D, x
	asl	$0A, x
	.byte	$17
	.byte	$FF
	rol	$1217
	bpl	LBD4B
	.byte	$1D
	.byte	$FF
LBD3C:	bmi	LBD48
	bpl	LBD52
	jsr	$1F22
	asl	$171B
	.byte	$FF
	.byte	$27
LBD48:	asl	$1816
LBD4B:	.byte	$17
	.byte	$FF
	.byte	$3A
	asl	$0E1B
	.byte	$20
LBD52:	clc
	ora	$0F, x
	.byte	$FF
	rol	a
	.byte	$1B
	asl	$170E
	.byte	$FF
	rol	$1D, x
	asl	a
	.byte	$1B
	jsr	$1F22
	asl	$171B
	.byte	$FF
	.byte	$3A
	.byte	$12
	.byte	$23
	asl	a
LBD6B:	.byte	$1B
	ora	$24FF
	and	($0E, x)
	.byte	$FF
	and	$15
	asl	$FF0E, x
	rol	$1D, x
	clc
	.byte	$17
	asl	$0A16
	.byte	$17
	.byte	$FF
	bit	$1B
	asl	$18, x
	.byte	$1B
	asl	$FF0D
	and	$0E, x
	ora	$27FF
	.byte	$1B
	asl	a
	bpl	LBDA9
	.byte	$17
	ora	$18, x
	.byte	$1B
	ora	$27FF
	.byte	$1B
	asl	a
	bpl	LBDB4
	.byte	$17
	ora	$18, x
	.byte	$1B
	ora	$FFFF
	rol	$15, x
	.byte	$12
	asl	$0E, x
	.byte	$FF
LBDA9:	.byte	$FF
	.byte	$FF
	.byte	$FF
	.byte	$FF
	.byte	$FF
	.byte	$FF
	.byte	$FF
	.byte	$FF
	.byte	$FF
	.byte	$FF
	.byte	$FF
LBDB4:	rol	$0C, x
	clc
	.byte	$1B
	ora	$1812, y
	.byte	$17
	.byte	$FF
	.byte	$FF
	.byte	$FF
	rol	$15, x
	.byte	$12
	asl	$0E, x
	.byte	$FF
	.byte	$FF
	.byte	$FF
	.byte	$FF
	.byte	$FF
	.byte	$FF
	rol	$0C, x
	clc
	.byte	$1B
LBDCE:	ora	$1812, y
	.byte	$17
	.byte	$FF
	rol	$1217
	bpl	LBDE9
	ora	$FFFF, x
	.byte	$FF
	.byte	$FF
	.byte	$FF
	rol	$1217
	bpl	LBDF4
	ora	$FFFF, x
	.byte	$27
	.byte	$1B
	asl	a
LBDE9:	bpl	LBE03
	.byte	$17
	.byte	$FF
	.byte	$FF
	.byte	$FF
	rol	$1217
	bpl	LBE05
LBDF4:	ora	$27FF, x
	.byte	$1B
	asl	a
	bpl	LBE13
	.byte	$17
	.byte	$FF
	.byte	$FF
	rol	$1217
	bpl	LBE14
LBE03:	.byte	$1D
	.byte	$FF
LBE05:	.byte	$27
	.byte	$1B
	asl	a
	bpl	LBE22
	.byte	$17
	.byte	$FF
	.byte	$FF
	.byte	$FF
LBE0E:	bpl	LBDCE
	asl	a
	brk
	.byte	$3C
LBE13:	brk
LBE14:	ldy	$00, x
	bmi	LBE1A
	.byte	$DC
	.byte	$05
LBE1A:	pha
	rol	$02
	brk
	.byte	$14
	brk
	lsr	$00
LBE22:	bit	$E801
	.byte	$03
	clv
	.byte	$0B
	.byte	$14
	asl	a:$02, x
	.byte	$5A
	brk
	jsr	$D003
	and	$18, y
	php
	brk
	.byte	$14
	brk
	lsr	$00
	and	$00, x
	rol	$00
	brk
	brk
	brk
	brk
	brk
	brk
LBE44:	brk
	brk
	brk
	brk
	brk
	brk
	brk
	brk
	brk
	brk
	pla
	ora	($60, x)
	ora	#$1E
	brk
	brk
	brk
	.byte	$2B
	plp
	bit	$2F
	.byte	$FF
	.byte	$2B
	sec
	and	$37, x
	.byte	$FF
	rol	$2F, x
	plp
	plp
	.byte	$33
	.byte	$FF
	and	$24, x
	.byte	$27
	bit	$3124
	.byte	$37
	.byte	$FF
	rol	$37, x
	.byte	$32
	.byte	$33
	rol	$33, x
	plp
	.byte	$2F
	.byte	$2F
	.byte	$FF
	.byte	$32
	sec
	.byte	$37
	rol	$2C, x
	.byte	$27
	plp
	.byte	$FF
	and	$28, x
	.byte	$37
	sec
	and	$31, x
	.byte	$FF
	and	$28, x
	.byte	$33
	plp
	.byte	$2F
	.byte	$FF
	.byte	$2B
	plp
	bit	$2F
	.byte	$30
	.byte	$32
	and	$28, x
	.byte	$FF
	.byte	$2B
	sec
	and	$37, x
	.byte	$30
	.byte	$32
	and	$28, x
; ------------------------------------------------------------
	.res	314, $FF
; ------------------------------------------------------------
reset_banked:
	sei
	inc	LBFDF
	jmp	start_banked
; ------------------------------------------------------------
LBFDF:	.byte	$80
; ------------------------------------------------------------
LBFE0:	.byte	"DRAGON WARRIOR  "
; ------------------------------------------------------------
	.byte	$56
	.byte	$DE
	.byte	$30
	.byte	$70
	.byte	$01
	.byte	$04
	.byte	$01
	.byte	$0F
	.byte	$07
	.byte	$00
; ------------------------------------------------------------
	.addr	reset_banked
	.addr	reset_banked
	.addr	reset_banked
