.include	"system/ppu.i"

.segment	"RODATA"

TILE_ART_BASE	= <((tile_battle_bg_start_chr00-chr00_base)/PPU_TILE_SIZE)

	.export map_battle_field, map_battle_field_end
map_battle_field:
line_1:
	.byte	TILE_ART_BASE+$0D
	.byte	TILE_ART_BASE+$0F
	.byte	TILE_ART_BASE+$0F
	.byte	TILE_ART_BASE+$0F
	.byte	TILE_ART_BASE+$0F
	.byte	TILE_ART_BASE+$0F
	.byte	TILE_ART_BASE+$0F
	.byte	TILE_ART_BASE+$0F
	.byte	TILE_ART_BASE+$0F
	.byte	TILE_ART_BASE+$0F
	.byte	TILE_ART_BASE+$0F
	.byte	TILE_ART_BASE+$0F
	.byte	TILE_ART_BASE+$0F
	.byte	TILE_ART_BASE+$10

line_2:
	.byte	TILE_ART_BASE+$0E
	.byte	TILE_ART_BASE+$05
	.byte	TILE_ART_BASE+$02
	.byte	TILE_ART_BASE+$00
	.byte	TILE_ART_BASE+$01
	.byte	TILE_ART_BASE+$05
	.byte	TILE_ART_BASE+$05
	.byte	TILE_ART_BASE+$05
	.byte	TILE_ART_BASE+$05
	.byte	TILE_ART_BASE+$05
	.byte	TILE_ART_BASE+$05
	.byte	TILE_ART_BASE+$00
	.byte	TILE_ART_BASE+$04
	.byte	TILE_ART_BASE+$11

line_3:
	.byte	TILE_ART_BASE+$0E
	.byte	TILE_ART_BASE+$05
	.byte	TILE_ART_BASE+$41
	.byte	TILE_ART_BASE+$34
	.byte	TILE_ART_BASE+$05
	.byte	TILE_ART_BASE+$05
	.byte	TILE_ART_BASE+$05
	.byte	TILE_ART_BASE+$02
	.byte	TILE_ART_BASE+$03
	.byte	TILE_ART_BASE+$04
	.byte	TILE_ART_BASE+$05
	.byte	TILE_ART_BASE+$05
	.byte	TILE_ART_BASE+$05
	.byte	TILE_ART_BASE+$11

line_4:
	.byte	TILE_ART_BASE+$0E
	.byte	TILE_ART_BASE+$05
	.byte	TILE_ART_BASE+$31
	.byte	TILE_ART_BASE+$35
	.byte	TILE_ART_BASE+$05
	.byte	TILE_ART_BASE+$05
	.byte	TILE_ART_BASE+$05
	.byte	TILE_ART_BASE+$05
	.byte	TILE_ART_BASE+$05
	.byte	TILE_ART_BASE+$05
	.byte	TILE_ART_BASE+$05
	.byte	TILE_ART_BASE+$08
	.byte	TILE_ART_BASE+$05
	.byte	TILE_ART_BASE+$11

line_5:
	.byte	TILE_ART_BASE+$3E
	.byte	TILE_ART_BASE+$3D
	.byte	TILE_ART_BASE+$1D
	.byte	TILE_ART_BASE+$21
	.byte	TILE_ART_BASE+$38
	.byte	TILE_ART_BASE+$36
	.byte	TILE_ART_BASE+$36
	.byte	TILE_ART_BASE+$36
	.byte	TILE_ART_BASE+$32
	.byte	TILE_ART_BASE+$37
	.byte	TILE_ART_BASE+$06
	.byte	TILE_ART_BASE+$09
	.byte	TILE_ART_BASE+$0B
	.byte	TILE_ART_BASE+$11

line_6:
	.byte	TILE_ART_BASE+$3F
	.byte	TILE_ART_BASE+$40
	.byte	TILE_ART_BASE+$1F
	.byte	TILE_ART_BASE+$23
	.byte	TILE_ART_BASE+$39
	.byte	TILE_ART_BASE+$3C
	.byte	TILE_ART_BASE+$3C
	.byte	TILE_ART_BASE+$3A
	.byte	TILE_ART_BASE+$33
	.byte	TILE_ART_BASE+$3B
	.byte	TILE_ART_BASE+$07
	.byte	TILE_ART_BASE+$0A
	.byte	TILE_ART_BASE+$0C
	.byte	TILE_ART_BASE+$12

line_7:
	.byte	TILE_ART_BASE+$2F
	.byte	TILE_ART_BASE+$24
	.byte	TILE_ART_BASE+$25
	.byte	TILE_ART_BASE+$2A
	.byte	TILE_ART_BASE+$2E
	.byte	TILE_ART_BASE+$20
	.byte	TILE_ART_BASE+$20
	.byte	TILE_ART_BASE+$20
	.byte	TILE_ART_BASE+$20
	.byte	TILE_ART_BASE+$26
	.byte	TILE_ART_BASE+$1E
	.byte	TILE_ART_BASE+$22
	.byte	TILE_ART_BASE+$22
	.byte	TILE_ART_BASE+$27

line_8:
	.byte	TILE_ART_BASE+$30
	.byte	TILE_ART_BASE+$20
	.byte	TILE_ART_BASE+$20
	.byte	TILE_ART_BASE+$20
	.byte	TILE_ART_BASE+$20
	.byte	TILE_ART_BASE+$20
	.byte	TILE_ART_BASE+$20
	.byte	TILE_ART_BASE+$20
	.byte	TILE_ART_BASE+$20
	.byte	TILE_ART_BASE+$20
	.byte	TILE_ART_BASE+$20
	.byte	TILE_ART_BASE+$20
	.byte	TILE_ART_BASE+$26
	.byte	TILE_ART_BASE+$27

line_9:
	.byte	TILE_ART_BASE+$1C
	.byte	TILE_ART_BASE+$15
	.byte	TILE_ART_BASE+$17
	.byte	TILE_ART_BASE+$19
	.byte	TILE_ART_BASE+$20
	.byte	TILE_ART_BASE+$20
	.byte	TILE_ART_BASE+$20
	.byte	TILE_ART_BASE+$20
	.byte	TILE_ART_BASE+$20
	.byte	TILE_ART_BASE+$20
	.byte	TILE_ART_BASE+$19
	.byte	TILE_ART_BASE+$19
	.byte	TILE_ART_BASE+$20
	.byte	TILE_ART_BASE+$28

line_10:
	.byte	TILE_ART_BASE+$14
	.byte	TILE_ART_BASE+$16
	.byte	TILE_ART_BASE+$18
	.byte	TILE_ART_BASE+$19
	.byte	TILE_ART_BASE+$20
	.byte	TILE_ART_BASE+$20
	.byte	TILE_ART_BASE+$20
	.byte	TILE_ART_BASE+$20
	.byte	TILE_ART_BASE+$20
	.byte	TILE_ART_BASE+$20
	.byte	TILE_ART_BASE+$19
	.byte	TILE_ART_BASE+$1B
	.byte	TILE_ART_BASE+$20
	.byte	TILE_ART_BASE+$28

line_11:
	.byte	TILE_ART_BASE+$2C
	.byte	TILE_ART_BASE+$20
	.byte	TILE_ART_BASE+$1A
	.byte	TILE_ART_BASE+$19
	.byte	TILE_ART_BASE+$20
	.byte	TILE_ART_BASE+$20
	.byte	TILE_ART_BASE+$20
	.byte	TILE_ART_BASE+$20
	.byte	TILE_ART_BASE+$19
	.byte	TILE_ART_BASE+$19
	.byte	TILE_ART_BASE+$20
	.byte	TILE_ART_BASE+$20
	.byte	TILE_ART_BASE+$20
	.byte	TILE_ART_BASE+$28

line_12:
	.byte	TILE_ART_BASE+$2C
	.byte	TILE_ART_BASE+$20
	.byte	TILE_ART_BASE+$19
	.byte	TILE_ART_BASE+$1B
	.byte	TILE_ART_BASE+$20
	.byte	TILE_ART_BASE+$20
	.byte	TILE_ART_BASE+$20
	.byte	TILE_ART_BASE+$20
	.byte	TILE_ART_BASE+$19
	.byte	TILE_ART_BASE+$19
	.byte	TILE_ART_BASE+$20
	.byte	TILE_ART_BASE+$20
	.byte	TILE_ART_BASE+$20
	.byte	TILE_ART_BASE+$28

line_13:
	.byte	TILE_ART_BASE+$2C
	.byte	TILE_ART_BASE+$20
	.byte	TILE_ART_BASE+$20
	.byte	TILE_ART_BASE+$20
	.byte	TILE_ART_BASE+$20
	.byte	TILE_ART_BASE+$20
	.byte	TILE_ART_BASE+$20
	.byte	TILE_ART_BASE+$20
	.byte	TILE_ART_BASE+$20
	.byte	TILE_ART_BASE+$20
	.byte	TILE_ART_BASE+$20
	.byte	TILE_ART_BASE+$20
	.byte	TILE_ART_BASE+$20
	.byte	TILE_ART_BASE+$28

line_14:
	.byte	TILE_ART_BASE+$2B
	.byte	TILE_ART_BASE+$2D
	.byte	TILE_ART_BASE+$2D
	.byte	TILE_ART_BASE+$2D
	.byte	TILE_ART_BASE+$2D
	.byte	TILE_ART_BASE+$2D
	.byte	TILE_ART_BASE+$2D
	.byte	TILE_ART_BASE+$2D
	.byte	TILE_ART_BASE+$2D
	.byte	TILE_ART_BASE+$2D
	.byte	TILE_ART_BASE+$2D
	.byte	TILE_ART_BASE+$2D
	.byte	TILE_ART_BASE+$2D
	.byte	TILE_ART_BASE+$29
map_battle_field_end:
