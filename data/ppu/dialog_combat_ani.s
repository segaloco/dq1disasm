.include	"bssmap.i"

.segment	"CHR3CODE"
	.export dialog_combat_addr, dialog_combat_ani, dialog_combat_ani_end
dialog_combat_addr:
.org	CNROM_BUFFER
dialog_combat_ani:
	.byte   $66
        .byte   $68
        .byte   $48
        .byte   $46
        .byte   $44
        .byte   $64
        .byte   $84
        .byte   $86
        .byte   $88
        .byte   $8A
        .byte   $6A
        .byte   $4A
        .byte   $2A
        .byte   $28
        .byte   $26
        .byte   $24
        .byte   $22
        .byte   $42
        .byte   $62
        .byte   $82
        .byte   $A2
        .byte   $A4
        .byte   $A6
        .byte   $A8
        .byte   $AA
        .byte   $AC
        .byte   $8C
        .byte   $6C
        .byte   $4C
        .byte   $2C
        .byte   $0C
        .byte   $0A
        .byte   $08
        .byte   $06
        .byte   $04
        .byte   $02
        .byte   $00
        .byte   $20
        .byte   $40
        .byte   $60
        .byte   $80
        .byte   $A0
        .byte   $C0
        .byte   $C2
        .byte   $C4
        .byte   $C6
        .byte   $C8
        .byte   $CA
        .byte   $CC
dialog_combat_ani_end:
