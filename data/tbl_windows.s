.include	"window.i"
.include	"charmap.i"
.include	"rpg.i"

.include	"./windows/tbl_windows.i"

.segment	"RODATA"

	.export tbl_windows
	.export	window_main_field, window_stats
	.export window_dialog, window_cmd_field
	.export	window_cmd_battle, window_talk_dir
	.export window_spell, window_tool
	.export window_shop, window_decision
	.export window_buysell, window_main_battle
tbl_windows:
window_main_field:
	.byte	FUNC_WINDOW_MIRROR
	.addr	func_window_main
	.byte	MAIN_X, MAIN_Y
	.byte	MAIN_FIELD_WIDTH, (MAIN_FIELD_HEIGHT-1)*tbl_windows_HEIGHT_SIZE

window_stats:
	.byte	FUNC_WINDOW_NORMAL
	.addr	func_window_stats
	.byte	STATS_X, STATS_Y
	.byte	STATS_WIDTH, (STATS_HEIGHT-1)*tbl_windows_HEIGHT_SIZE

window_dialog:
	.byte	FUNC_WINDOW_MIRROR
	.addr	func_window_dialog
	.byte	DIALOG_X, DIALOG_Y
	.byte	DIALOG_WIDTH, (DIALOG_HEIGHT-1)*tbl_windows_HEIGHT_SIZE

window_cmd_field:
	.byte	FUNC_WINDOW_MIRROR
	.addr	func_window_command_field
	.byte	CMD_X, CMD_Y
	.byte	CMD_WIDTH, (CMD_FIELD_HEIGHT-1)*tbl_windows_HEIGHT_SIZE

window_cmd_battle:
	.byte	FUNC_WINDOW_MIRROR
	.addr	func_window_command_battle
	.byte	CMD_X, CMD_Y
	.byte	CMD_WIDTH, (CMD_BATTLE_HEIGHT-1)*tbl_windows_HEIGHT_SIZE

window_talk_dir:
	.byte	FUNC_WINDOW_NORMAL
	.addr	func_window_talk_dir
	.byte	TALK_DIR_X, TALK_DIR_Y
	.byte	TALK_DIR_WIDTH, (TALK_DIR_HEIGHT-1)*tbl_windows_HEIGHT_SIZE

window_spell:
	.byte	FUNC_WINDOW_NORMAL
	.addr	func_window_spell
	.byte	SPELL_X, SPELL_Y
	.byte	SPELL_WIDTH, (SPELL_HEIGHT-1)*tbl_windows_HEIGHT_SIZE

window_tool:
	.byte	FUNC_WINDOW_NORMAL
	.addr	func_window_tool
	.byte	TOOL_X, TOOL_Y
	.byte	TOOL_WIDTH, (TOOL_HEIGHT-1)*tbl_windows_HEIGHT_SIZE

window_shop:
	.byte	FUNC_WINDOW_NORMAL
	.addr	func_window_shop
	.byte	SHOP_X, SHOP_Y
	.byte	SHOP_WIDTH, (SHOP_HEIGHT-1)*tbl_windows_HEIGHT_SIZE

window_decision:
	.byte	FUNC_WINDOW_NORMAL
	.addr	func_window_decision
	.byte	DECISION_X, DECISION_Y
	.byte	DECISION_WIDTH, (DECISION_HEIGHT-1)*tbl_windows_HEIGHT_SIZE

window_buysell:
	.byte	FUNC_WINDOW_NORMAL
	.addr	func_window_buysell
	.byte	BUYSELL_X, BUYSELL_Y
	.byte	BUYSELL_WIDTH, (BUYSELL_HEIGHT-1)*tbl_windows_HEIGHT_SIZE

window_main_battle:
	.byte	FUNC_WINDOW_MIRROR
	.addr	func_window_main
	.byte	MAIN_X, MAIN_Y
	.byte	MAIN_BATTLE_WIDTH, (MAIN_BATTLE_HEIGHT-1)*tbl_windows_HEIGHT_SIZE
