.include	"flags.i"

.segment	"CHR1DATA"

	.export tbl_levelstats, tbl_levelstats_end
tbl_levelstats:
	;       str  agi  maxhp maxmp highspell lowspell
	.byte	4,   4,   15,   0,    0,						0
	.byte	5,   4,   22,   0,    0,						0
	.byte	7,   6,   24,   5,    0,						state_flags::heal
	.byte	7,   8,   31,   16,   0,						state_flags::sizz|state_flags::heal
	.byte	12,  10,  35,   20,   0,						state_flags::sizz|state_flags::heal
	.byte	16,  10,  38,   24,   0,						state_flags::sizz|state_flags::heal
	.byte	18,  17,  40,   26,   0,						state_flags::snooze|state_flags::sizz|state_flags::heal
	.byte	22,  20,  46,   29,   0,						state_flags::snooze|state_flags::sizz|state_flags::heal
	.byte	30,  22,  50,   36,   0,						state_flags::glow|state_flags::snooze|state_flags::sizz|state_flags::heal
	.byte	35,  31,  54,   40,   0,						state_flags::fizzle|state_flags::glow|state_flags::snooze|state_flags::sizz|state_flags::heal
	.byte	40,  35,  62,   50,   0,						state_flags::fizzle|state_flags::glow|state_flags::snooze|state_flags::sizz|state_flags::heal
	.byte	48,  40,  63,   58,   0,						state_flags::evac|state_flags::fizzle|state_flags::glow|state_flags::snooze|state_flags::sizz|state_flags::heal
	.byte	52,  48,  70,   64,   0,						state_flags::zoom|state_flags::evac|state_flags::fizzle|state_flags::glow|state_flags::snooze|state_flags::sizz|state_flags::heal
	.byte	60,  55,  78,   70,   0,						state_flags::zoom|state_flags::evac|state_flags::fizzle|state_flags::glow|state_flags::snooze|state_flags::sizz|state_flags::heal
	.byte	68,  64,  86,   72,   0,						state_flags::protection|state_flags::zoom|state_flags::evac|state_flags::fizzle|state_flags::glow|state_flags::snooze|state_flags::sizz|state_flags::heal
	.byte	72,  70,  92,   95,   0, 						state_flags::protection|state_flags::zoom|state_flags::evac|state_flags::fizzle|state_flags::glow|state_flags::snooze|state_flags::sizz|state_flags::heal
	.byte	72,  78,  100,  100,  (state_flags::midheal)>>8,			state_flags::protection|state_flags::zoom|state_flags::evac|state_flags::fizzle|state_flags::glow|state_flags::snooze|state_flags::sizz|state_flags::heal
	.byte	85,  84,  115,  108,  (state_flags::midheal)>>8,			state_flags::protection|state_flags::zoom|state_flags::evac|state_flags::fizzle|state_flags::glow|state_flags::snooze|state_flags::sizz|state_flags::heal
	.byte	87,  86,  130,  115,  (state_flags::sizzle|state_flags::midheal)>>8,	state_flags::protection|state_flags::zoom|state_flags::evac|state_flags::fizzle|state_flags::glow|state_flags::snooze|state_flags::sizz|state_flags::heal
	.byte	92,  88,  138,  128,  (state_flags::sizzle|state_flags::midheal)>>8,	state_flags::protection|state_flags::zoom|state_flags::evac|state_flags::fizzle|state_flags::glow|state_flags::snooze|state_flags::sizz|state_flags::heal
	.byte	95,  90,  149,  135,  (state_flags::sizzle|state_flags::midheal)>>8,	state_flags::protection|state_flags::zoom|state_flags::evac|state_flags::fizzle|state_flags::glow|state_flags::snooze|state_flags::sizz|state_flags::heal
	.byte	97,  90,  158,  146,  (state_flags::sizzle|state_flags::midheal)>>8,	state_flags::protection|state_flags::zoom|state_flags::evac|state_flags::fizzle|state_flags::glow|state_flags::snooze|state_flags::sizz|state_flags::heal
	.byte	99,  94,  165,  153,  (state_flags::sizzle|state_flags::midheal)>>8,	state_flags::protection|state_flags::zoom|state_flags::evac|state_flags::fizzle|state_flags::glow|state_flags::snooze|state_flags::sizz|state_flags::heal
	.byte	103, 98,  170,  161,  (state_flags::sizzle|state_flags::midheal)>>8,	state_flags::protection|state_flags::zoom|state_flags::evac|state_flags::fizzle|state_flags::glow|state_flags::snooze|state_flags::sizz|state_flags::heal
	.byte	113, 100, 174,  161,  (state_flags::sizzle|state_flags::midheal)>>8,	state_flags::protection|state_flags::zoom|state_flags::evac|state_flags::fizzle|state_flags::glow|state_flags::snooze|state_flags::sizz|state_flags::heal
	.byte	117, 105, 180,  168,  (state_flags::sizzle|state_flags::midheal)>>8,	state_flags::protection|state_flags::zoom|state_flags::evac|state_flags::fizzle|state_flags::glow|state_flags::snooze|state_flags::sizz|state_flags::heal
	.byte	125, 107, 189,  175,  (state_flags::sizzle|state_flags::midheal)>>8,	state_flags::protection|state_flags::zoom|state_flags::evac|state_flags::fizzle|state_flags::glow|state_flags::snooze|state_flags::sizz|state_flags::heal
	.byte	130, 115, 195,  180,  (state_flags::sizzle|state_flags::midheal)>>8,	state_flags::protection|state_flags::zoom|state_flags::evac|state_flags::fizzle|state_flags::glow|state_flags::snooze|state_flags::sizz|state_flags::heal
	.byte	135, 120, 200,  190,  (state_flags::sizzle|state_flags::midheal)>>8,	state_flags::protection|state_flags::zoom|state_flags::evac|state_flags::fizzle|state_flags::glow|state_flags::snooze|state_flags::sizz|state_flags::heal
	.byte	140, 130, 210,  200,  (state_flags::sizzle|state_flags::midheal)>>8,	state_flags::protection|state_flags::zoom|state_flags::evac|state_flags::fizzle|state_flags::glow|state_flags::snooze|state_flags::sizz|state_flags::heal
tbl_levelstats_end:
