.include	"system/ppu.i"

.include	"bssmap.i"
.include	"sprite.i"

.segment	"CHR3CODE"
	.export tbl_enemy_spr_addr, tbl_enemy_spr
	.export enemy_spr_mknight
	.export enemy_spr_skeleton
	.export enemy_spr_droll
	.export enemy_spr_drakee
	.export enemy_spr_druin
	.export enemy_spr_slime
	.export enemy_spr_warlock
	.export enemy_spr_magician
	.export enemy_spr_dragonlord_weak
	.export enemy_spr_ghost
	.export enemy_spr_chimera
	.export enemy_spr_wolf
	.export enemy_spr_golem
	.export enemy_spr_scorpion
	.export enemy_spr_knight_death
	.export enemy_spr_knight_axe
	.export enemy_spr_knight
	.export enemy_spr_dragon_hard
	.export enemy_spr_dragon_green
tbl_enemy_spr_addr:
.org	CNROM_BUFFER
tbl_enemy_spr:

enemy_spr_mknight:
	.byte	$30, $2A, ($27<<2)|obj_attribute::color3
	.byte	$2F, $27, ($1F<<2)|obj_attribute::color3
	.byte	$2F, $23, ($17<<2)|obj_attribute::color3
	.byte	$2E, $1F, ($0F<<2)|obj_attribute::color3

enemy_spr_skeleton:
	.byte   $20, $17, ($1C<<2)|obj_attribute::color0
        .byte   $21, $1F, ($1C<<2)|obj_attribute::color0
        .byte   $23, $1E, ($0E<<2)|obj_attribute::color3
        .byte   $26, $26, ($0E<<2)|obj_attribute::color2
        .byte   $22, $1E, ($16<<2)|obj_attribute::color1
        .byte   $27, $26, ($18<<2)|obj_attribute::color1
        .byte   $28, $2E, ($18<<2)|obj_attribute::color1
        .byte   $29, $33, ($15<<2)|obj_attribute::color1
        .byte   $2A, $37, ($15<<2)|obj_attribute::color2
        .byte   $22, obj_attribute::h_flip|$1E, ($22<<2)|obj_attribute::color1
        .byte   $23, obj_attribute::h_flip|$1E, ($2A<<2)|obj_attribute::color3
        .byte   $27, obj_attribute::h_flip|$26, ($20<<2)|obj_attribute::color1
        .byte   $28, obj_attribute::h_flip|$2E, ($20<<2)|obj_attribute::color1
        .byte   $24, $26, ($29<<2)|obj_attribute::color2
        .byte   $25, $2D, ($29<<2)|obj_attribute::color2
        .byte   $2B, $33, ($27<<2)|obj_attribute::color1
        .byte   $2C, $3B, ($23<<2)|obj_attribute::color2
        .byte   $2D, $3B, ($2B<<2)|obj_attribute::color2
        .byte   $FE, $3B, ($12<<2)|obj_attribute::color2
        .byte   $FF, $3B, ($1A<<2)|obj_attribute::color2
        .byte   $FE, obj_attribute::h_flip|$3B, ($22<<2)|obj_attribute::color2
        .byte   $FE, obj_attribute::h_flip|$3D, ($2A<<2)|obj_attribute::color2
        .byte   $FE, $3F, ($21<<2)|obj_attribute::color2
        .byte   $FE, obj_attribute::h_flip|$3F, ($29<<2)|obj_attribute::color2
	.byte	SPRITE_END

enemy_spr_droll:
	.byte   $31, obj_attribute::h_flip|$19, ($18<<2)|obj_attribute::color2
        .byte   $32, obj_attribute::h_flip|$21, ($18<<2)|obj_attribute::color3
        .byte   $33, obj_attribute::h_flip|$29, ($18<<2)|obj_attribute::color2
        .byte   $34, obj_attribute::h_flip|$31, ($18<<2)|obj_attribute::color1
        .byte   $35, obj_attribute::h_flip|$37, ($18<<2)|obj_attribute::color0
        .byte   $31, $19, ($20<<2)|obj_attribute::color2
        .byte   $32, $21, ($20<<2)|obj_attribute::color3
        .byte   $33, $29, ($20<<2)|obj_attribute::color2
        .byte   $34, $31, ($20<<2)|obj_attribute::color1
        .byte   $35, $37, ($20<<2)|obj_attribute::color0
        .byte   $36, obj_attribute::h_flip|$1E, ($10<<2)|obj_attribute::color0
        .byte   $38, obj_attribute::h_flip|$26, ($10<<2)|obj_attribute::color2
        .byte   $39, obj_attribute::h_flip|$2E, ($10<<2)|obj_attribute::color3
        .byte   $3A, obj_attribute::h_flip|$36, ($10<<2)|obj_attribute::color0
        .byte   $36, $1E, ($28<<2)|obj_attribute::color0
        .byte   $38, $26, ($28<<2)|obj_attribute::color2
        .byte   $39, $2E, ($28<<2)|obj_attribute::color3
        .byte   $3A, $36, ($28<<2)|obj_attribute::color0
        .byte   $E6, $22, ($16<<2)|obj_attribute::color2
        .byte   $E6, obj_attribute::h_flip|$22, ($22<<2)|obj_attribute::color2
	.byte	SPRITE_END

enemy_spr_drakee:
        .byte   $3B, $1F, ($14<<2)|obj_attribute::color0
        .byte   $3C, $27, ($14<<2)|obj_attribute::color0
        .byte   $3D, $1F, ($1C<<2)|obj_attribute::color0
        .byte   $3E, $27, ($1C<<2)|obj_attribute::color0
        .byte   $3B, obj_attribute::h_flip|$1F, ($24<<2)|obj_attribute::color0
        .byte   $3C, obj_attribute::h_flip|$27, ($24<<2)|obj_attribute::color0
        .byte   $3F, $2C, ($1F<<2)|obj_attribute::color0
        .byte   $FE, $39, ($18<<2)|obj_attribute::color2
        .byte   $FE, obj_attribute::h_flip|$39, ($20<<2)|obj_attribute::color2
	.byte	SPRITE_END

enemy_spr_druin:
	.byte   $42, $1C, ($10<<2)|obj_attribute::color0
        .byte   $45, $24, ($10<<2)|obj_attribute::color0
        .byte   $4A, $29, ($10<<2)|obj_attribute::color1
        .byte   $4D, $31, ($10<<2)|obj_attribute::color1
        .byte   $43, $1C, ($18<<2)|obj_attribute::color0
        .byte   $46, $24, ($18<<2)|obj_attribute::color0
        .byte   $4B, $29, ($18<<2)|obj_attribute::color1
        .byte   $4E, $31, ($18<<2)|obj_attribute::color1
        .byte   $40, $18, ($1B<<2)|obj_attribute::color0
        .byte   $41, $18, ($23<<2)|obj_attribute::color0
        .byte   $44, $1C, ($20<<2)|obj_attribute::color0
        .byte   $47, $24, ($20<<2)|obj_attribute::color2
        .byte   $4C, $2A, ($20<<2)|obj_attribute::color1
        .byte   $48, $24, ($28<<2)|obj_attribute::color1
        .byte   $49, $28, ($30<<2)|obj_attribute::color1
	.byte	SPRITE_END

enemy_spr_slime:
        .byte   $55, $32, ($19<<2)|obj_attribute::color0
        .byte   $53, $2B, ($18<<2)|obj_attribute::color0
        .byte   $54, $33, ($18<<2)|obj_attribute::color0
        .byte   $53, obj_attribute::h_flip|$2B, ($1F<<2)|obj_attribute::color0
        .byte   $54, obj_attribute::h_flip|$33, ($1F<<2)|obj_attribute::color0
        .byte   $FF, $35, ($1C<<2)|obj_attribute::color2
        .byte   $FE, obj_attribute::v_flip|obj_attribute::h_flip|$36, ($24<<2)|obj_attribute::color2
	.byte	SPRITE_END

enemy_spr_warlock:
	.byte   $5C, $19, ($25<<2)|obj_attribute::color2
        .byte   $5D, $20, ($25<<2)|obj_attribute::color2
        .byte   $5D, $2E, ($26<<2)|obj_attribute::color2
        .byte   $5D, $35, ($27<<2)|obj_attribute::color2

enemy_spr_magician:
	.byte   $5A, $1B, ($18<<2)|obj_attribute::color1
        .byte   $5B, $23, ($18<<2)|obj_attribute::color1
        .byte   $5A, obj_attribute::h_flip|$1B, ($20<<2)|obj_attribute::color1
        .byte   $5B, obj_attribute::h_flip|$23, ($20<<2)|obj_attribute::color1
        .byte   $56, $24, ($0C<<2)|obj_attribute::color0
        .byte   $57, $23, ($14<<2)|obj_attribute::color0
        .byte   $58, $2B, ($14<<2)|obj_attribute::color0
        .byte   $59, $33, ($14<<2)|obj_attribute::color0
        .byte   $5F, $23, ($24<<2)|obj_attribute::color0
        .byte   $60, $2B, ($24<<2)|obj_attribute::color0
        .byte   $61, $33, ($24<<2)|obj_attribute::color0
        .byte   $5E, $33, ($1C<<2)|obj_attribute::color0
        .byte   $5E, $2C, ($1C<<2)|obj_attribute::color0
        .byte   $FF, obj_attribute::v_flip|$27, ($1C<<2)|obj_attribute::color3
        .byte   $FE, $37, ($12<<2)|obj_attribute::color0
        .byte   $FF, $37, ($1A<<2)|obj_attribute::color0
        .byte   $FF, obj_attribute::h_flip|$37, ($22<<2)|obj_attribute::color0
        .byte   $FE, obj_attribute::h_flip|$37, ($2A<<2)|obj_attribute::color0
	.byte	SPRITE_END

enemy_spr_dragonlord_weak:
	.byte	$62, $1E, ($27<<2)|obj_attribute::color3
	.byte	$63, $26, ($27<<2)|obj_attribute::color3
	.byte	$63, obj_attribute::h_flip|$34, ($26<<2)|obj_attribute::color3
	.byte	$5D, $3B, ($25<<2)|obj_attribute::color1
	.byte	$67, $1C, ($18<<2)|obj_attribute::color2
	.byte	$68, $23, ($18<<2)|obj_attribute::color1
	.byte	$69, $23, ($16<<2)|obj_attribute::color2
	.byte	$6A, $2B, ($18<<2)|obj_attribute::color3
	.byte	$67, obj_attribute::h_flip|$1C, ($20<<2)|obj_attribute::color2
	.byte	$68, obj_attribute::h_flip|$23, ($20<<2)|obj_attribute::color1
	.byte	$69, obj_attribute::h_flip|$23, ($22<<2)|obj_attribute::color2
	.byte	$6A, obj_attribute::h_flip|$2B, ($20<<2)|obj_attribute::color3
	.byte	$64, $29, ($14<<2)|obj_attribute::color0
	.byte	$65, $31, ($14<<2)|obj_attribute::color0
	.byte	$66, $39, ($14<<2)|obj_attribute::color0
	.byte	$5F, $29, ($24<<2)|obj_attribute::color0
	.byte	$60, $31, ($24<<2)|obj_attribute::color0
	.byte	$61, $39, ($24<<2)|obj_attribute::color0
	.byte	$5E, $39, ($1C<<2)|obj_attribute::color0
	.byte	$5E, $32, ($1C<<2)|obj_attribute::color0
	.byte	$5E, $2B, ($20<<2)|obj_attribute::color0
	.byte	$5E, $2B, ($1C<<2)|obj_attribute::color0
	.byte	SPRITE_END

enemy_spr_ghost:
	.byte	$70, $27, ($14<<2)|obj_attribute::color2
	.byte	$73, $2F, ($14<<2)|obj_attribute::color2
	.byte	$71, $27, ($1C<<2)|obj_attribute::color2
	.byte	$74, $2F, ($1C<<2)|obj_attribute::color3
	.byte	$72, $27, ($24<<2)|obj_attribute::color1
	.byte	$75, $2F, ($24<<2)|obj_attribute::color2
	.byte	$6D, $21, ($14<<2)|obj_attribute::color0
	.byte	$6E, $21, ($1C<<2)|obj_attribute::color0
	.byte	$6F, $21, ($24<<2)|obj_attribute::color0
	.byte	$6B, $19, ($1C<<2)|obj_attribute::color0
	.byte	$6C, $19, ($24<<2)|obj_attribute::color0
	.byte	$FE, $3C, ($15<<2)|obj_attribute::color1
	.byte	$FF, $3C, ($1B<<2)|obj_attribute::color1
	.byte	$FE, obj_attribute::h_flip|$3C, ($23<<2)|obj_attribute::color1
	.byte	SPRITE_END

enemy_spr_chimera:
	.byte	$83, $1A, ($13<<2)|obj_attribute::color3
	.byte	$81, $15, ($18<<2)|obj_attribute::color0
	.byte	$82, $1D, ($18<<2)|obj_attribute::color0
	.byte	$7F, $18, ($10<<2)|obj_attribute::color2
	.byte	$80, $20, ($10<<2)|obj_attribute::color1
	.byte	$7C, $12, ($27<<2)|obj_attribute::color0
	.byte	$7A, $1A, ($20<<2)|obj_attribute::color0
	.byte	$7B, $22, ($20<<2)|obj_attribute::color0
	.byte	$7D, $1A, ($28<<2)|obj_attribute::color0
	.byte	$7E, $22, ($28<<2)|obj_attribute::color0
	.byte	$76, $2D, ($0B<<2)|obj_attribute::color0
	.byte	$77, $2D, ($13<<2)|obj_attribute::color0
	.byte	$78, $2D, ($1B<<2)|obj_attribute::color0
	.byte	$79, $25, ($18<<2)|obj_attribute::color0
	.byte	$6B, $25, ($1C<<2)|obj_attribute::color0
	.byte	$55, $1F, ($0E<<2)|obj_attribute::color1
	.byte	$55, $21, ($18<<2)|obj_attribute::color1
	.byte	$63, obj_attribute::v_flip|$21, ($11<<2)|obj_attribute::color1
	.byte	$FE, $3D, ($0F<<2)|obj_attribute::color2
	.byte	$FF, $3D, ($17<<2)|obj_attribute::color2
	.byte	$FE, obj_attribute::h_flip|$3D, ($1F<<2)|obj_attribute::color2
	.byte	SPRITE_END

enemy_spr_wolf:
	.byte	$2A, $37, ($15<<2)|obj_attribute::color2
	.byte	$8D, $21, ($26<<2)|obj_attribute::color0
	.byte	$8E, $29, ($24<<2)|obj_attribute::color0
	.byte	$84, $1A, ($20<<2)|obj_attribute::color1
	.byte	$85, $22, ($20<<2)|obj_attribute::color1
	.byte	$86, $2A, ($20<<2)|obj_attribute::color0
	.byte	$87, $31, ($20<<2)|obj_attribute::color3
	.byte	$88, $21, ($28<<2)|obj_attribute::color0
	.byte	$8F, $29, ($28<<2)|obj_attribute::color0
	.byte	$8A, $31, ($28<<2)|obj_attribute::color3
	.byte	$8B, $22, ($30<<2)|obj_attribute::color0
	.byte	$8C, $2A, ($30<<2)|obj_attribute::color0
	.byte	$2C, $39, ($23<<2)|obj_attribute::color2
	.byte	$2D, $39, ($2B<<2)|obj_attribute::color2
	.byte	$84, obj_attribute::h_flip|$1A, ($18<<2)|obj_attribute::color1
	.byte	$85, obj_attribute::h_flip|$22, ($18<<2)|obj_attribute::color1
	.byte	$86, obj_attribute::h_flip|$2A, ($18<<2)|obj_attribute::color0
	.byte	$87, obj_attribute::h_flip|$31, ($18<<2)|obj_attribute::color3
	.byte	$88, obj_attribute::h_flip|$21, ($10<<2)|obj_attribute::color0
	.byte	$89, obj_attribute::h_flip|$29, ($10<<2)|obj_attribute::color0
	.byte	$90, obj_attribute::h_flip|$31, ($10<<2)|obj_attribute::color3
	.byte	$8B, obj_attribute::h_flip|$22, ($08<<2)|obj_attribute::color0
	.byte	$8C, obj_attribute::h_flip|$2A, ($08<<2)|obj_attribute::color0
	.byte	$FF, $2A, ($1C<<2)|obj_attribute::color2
	.byte	$91, $1D, ($2A<<2)|obj_attribute::color0
	.byte	$FE, $39, ($10<<2)|obj_attribute::color0
	.byte	$FF, $39, ($18<<2)|obj_attribute::color0
	.byte	$FF, obj_attribute::h_flip|$3A, ($20<<2)|obj_attribute::color0
	.byte	$FE, $3D, ($22<<2)|obj_attribute::color0
	.byte	$FE, obj_attribute::h_flip|$3D, ($2A<<2)|obj_attribute::color0
	.byte	SPRITE_END

enemy_spr_golem:
	.byte	$0E, $1C, ($09<<2)|obj_attribute::color0
	.byte	$B6, $24, ($09<<2)|obj_attribute::color0
	.byte	$BD, $3C, ($09<<2)|obj_attribute::color0
	.byte	$BB, $34, ($0E<<2)|obj_attribute::color0
	.byte	$B7, $14, ($11<<2)|obj_attribute::color0
	.byte	$B8, $1C, ($11<<2)|obj_attribute::color0
	.byte	$B9, $24, ($11<<2)|obj_attribute::color0
	.byte	$BA, $2C, ($11<<2)|obj_attribute::color0
	.byte	$BE, $3C, ($11<<2)|obj_attribute::color0
	.byte	$BC, $34, ($16<<2)|obj_attribute::color0
	.byte	$C1, $1C, ($19<<2)|obj_attribute::color0
	.byte	$C2, $24, ($19<<2)|obj_attribute::color0
	.byte	$C3, $2C, ($19<<2)|obj_attribute::color0
	.byte	$BF, $3C, ($19<<2)|obj_attribute::color0
	.byte	$C0, $14, ($1B<<2)|obj_attribute::color0
	.byte	$C4, $18, ($21<<2)|obj_attribute::color0
	.byte	$C5, $20, ($21<<2)|obj_attribute::color0
	.byte	$C6, $28, ($21<<2)|obj_attribute::color0
	.byte	$C7, $30, ($21<<2)|obj_attribute::color0
	.byte	$CF, $38, ($22<<2)|obj_attribute::color0
	.byte	$C8, $18, ($29<<2)|obj_attribute::color0
	.byte	$C9, $20, ($29<<2)|obj_attribute::color0
	.byte	$CA, $28, ($29<<2)|obj_attribute::color0
	.byte	$CB, $30, ($29<<2)|obj_attribute::color0
	.byte	$D0, $38, ($2A<<2)|obj_attribute::color0
	.byte	$CC, $20, ($31<<2)|obj_attribute::color0
	.byte	$CD, $28, ($31<<2)|obj_attribute::color0
	.byte	$CE, $30, ($31<<2)|obj_attribute::color0
	.byte	$58, obj_attribute::v_flip|$16, ($19<<2)|obj_attribute::color0
	.byte	$FF, obj_attribute::v_flip|$38, ($1E<<2)|obj_attribute::color0
	.byte	$FE, $3C, ($25<<2)|obj_attribute::color0
	.byte	$FE, obj_attribute::h_flip|$3C, ($2D<<2)|obj_attribute::color0
	.byte	SPRITE_END

enemy_spr_scorpion:
	.byte	$D4, $38, ($0E<<2)|obj_attribute::color0
	.byte	$D5, $32, ($14<<2)|obj_attribute::color0
	.byte	$D6, $2F, ($1C<<2)|obj_attribute::color0
	.byte	$D7, $37, ($1C<<2)|obj_attribute::color0
	.byte	$D8, $3F, ($23<<2)|obj_attribute::color0
	.byte	$D9, $2F, ($24<<2)|obj_attribute::color0
	.byte	$DA, $37, ($24<<2)|obj_attribute::color0
	.byte	$DB, $2F, ($2C<<2)|obj_attribute::color0
	.byte	$DC, $37, ($2C<<2)|obj_attribute::color0
	.byte	$D3, $27, ($2A<<2)|obj_attribute::color0
	.byte	$D2, $23, ($27<<2)|obj_attribute::color0
	.byte	$D1, $23, ($1F<<2)|obj_attribute::color0
	.byte	$3F, obj_attribute::h_flip|$23, ($1D<<2)|obj_attribute::color0
	.byte	$FF, $32, ($1C<<2)|obj_attribute::color1
	.byte	$FE, $37, ($15<<2)|obj_attribute::color0
	.byte	$FE, obj_attribute::h_flip|$33, ($30<<2)|obj_attribute::color0
	.byte	SPRITE_END

enemy_spr_knight_death:
	.byte	$F6, $19, ($31<<2)|obj_attribute::color2
	.byte	$F7, $21, ($31<<2)|obj_attribute::color2
	.byte	$F8, $29, ($31<<2)|obj_attribute::color2
	.byte	$F9, $31, ($31<<2)|obj_attribute::color2

enemy_spr_knight_axe:
	.byte	$FA, $11, ($07<<2)|obj_attribute::color2
	.byte	$FB, $19, ($07<<2)|obj_attribute::color2
	.byte	$FC, $15, ($0F<<2)|obj_attribute::color2
	.byte	$FD, $20, ($0B<<2)|obj_attribute::color2
	.byte	$5D, $18, ($0C<<2)|obj_attribute::color2
	.byte	$B5, $2E, ($09<<2)|obj_attribute::color2

enemy_spr_knight:
	.byte	$B3, $31, ($1B<<2)|obj_attribute::color2
	.byte	$B4, $31, ($23<<2)|obj_attribute::color2
	.byte	$37, $17, ($1A<<2)|obj_attribute::color3
	.byte	$9C, $19, ($22<<2)|obj_attribute::color3
	.byte	$9F, $1F, ($15<<2)|obj_attribute::color0
	.byte	$9D, $1F, ($1D<<2)|obj_attribute::color0
	.byte	$9E, $1F, ($25<<2)|obj_attribute::color0
	.byte	$A0, $1F, ($2D<<2)|obj_attribute::color0
	.byte	$A1, $27, ($0A<<2)|obj_attribute::color1
	.byte	$A2, $27, ($12<<2)|obj_attribute::color0
	.byte	$A3, $27, ($1A<<2)|obj_attribute::color0
	.byte	$A4, $27, ($22<<2)|obj_attribute::color0
	.byte	$A5, $27, ($2A<<2)|obj_attribute::color0
	.byte	$A7, $29, ($2D<<2)|obj_attribute::color1
	.byte	$A8, $2F, ($17<<2)|obj_attribute::color0
	.byte	$A9, $2F, ($1F<<2)|obj_attribute::color0
	.byte	$AA, $2F, ($27<<2)|obj_attribute::color0
	.byte	$AB, $33, ($0F<<2)|obj_attribute::color0
	.byte	$AD, $37, ($17<<2)|obj_attribute::color0
	.byte	$AE, $37, ($1F<<2)|obj_attribute::color0
	.byte	$AF, $37, ($27<<2)|obj_attribute::color0
	.byte	$B1, $37, ($2F<<2)|obj_attribute::color0
	.byte	$AC, $3B, ($10<<2)|obj_attribute::color1
	.byte	$B0, $3F, ($27<<2)|obj_attribute::color0
	.byte	$B2, $3F, ($2F<<2)|obj_attribute::color1
	.byte	$B2, $3A, ($13<<2)|obj_attribute::color1
	.byte	$A6, $27, ($32<<2)|obj_attribute::color0
	.byte	$FE, obj_attribute::h_flip|$3F, ($33<<2)|obj_attribute::color0
	.byte	$FE, $3D, ($17<<2)|obj_attribute::color0
	.byte	$FF, $3D, ($1F<<2)|obj_attribute::color0
	.byte	SPRITE_END

enemy_spr_dragon_hard:
	.byte	$F3, $3F, ($2D<<2)|obj_attribute::color2
	.byte	$F4, $3F, ($35<<2)|obj_attribute::color2

enemy_spr_dragon_green:
	.byte	$E6, $34, ($00<<2)|obj_attribute::color0
	.byte	$EC, $3C, ($03<<2)|obj_attribute::color0
	.byte	$E2, $2C, ($08<<2)|obj_attribute::color0
	.byte	$E7, $34, ($08<<2)|obj_attribute::color0
	.byte	$ED, $3C, ($0B<<2)|obj_attribute::color0
	.byte	$DD, $1C, ($0E<<2)|obj_attribute::color1
	.byte	$DE, $24, ($0E<<2)|obj_attribute::color1
	.byte	$E3, $2C, ($10<<2)|obj_attribute::color3
	.byte	$E8, $34, ($10<<2)|obj_attribute::color0
	.byte	$DF, $24, ($16<<2)|obj_attribute::color3
	.byte	$F0, $36, ($17<<2)|obj_attribute::color1
	.byte	$E4, $2C, ($18<<2)|obj_attribute::color3
	.byte	$E9, $34, ($18<<2)|obj_attribute::color0
	.byte	$EE, $3C, ($1B<<2)|obj_attribute::color0
	.byte	$E0, $24, ($1E<<2)|obj_attribute::color1
	.byte	$E5, $2C, ($20<<2)|obj_attribute::color0
	.byte	$EA, $34, ($20<<2)|obj_attribute::color0
	.byte	$F1, $30, ($23<<2)|obj_attribute::color2
	.byte	$EF, $3C, ($23<<2)|obj_attribute::color0
	.byte	$F2, $3A, ($24<<2)|obj_attribute::color2
	.byte	$E1, $23, ($26<<2)|obj_attribute::color1
	.byte	$EB, $34, ($28<<2)|obj_attribute::color0
	.byte	$F5, $24, ($0A<<2)|obj_attribute::color1
	.byte	$F5, obj_attribute::v_flip|$3C, ($13<<2)|obj_attribute::color0
	.byte	$FE, obj_attribute::v_flip|obj_attribute::h_flip|$2B, ($25<<2)|obj_attribute::color0
	.byte	SPRITE_END
