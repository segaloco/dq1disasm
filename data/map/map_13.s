.include	"map.i"

.segment	"RODATA"

	.export map_13
map_13:
	.byte	(tile_type_dungeon::stairs_down_2<<4)|(tile_type_dungeon::rocks_2)
	.byte	(tile_type_dungeon::brick_2<<4)|(tile_type_dungeon::rocks_2)
	.byte	(tile_type_dungeon::stairs_up_2<<4)|(tile_type_dungeon::brick_2)
	.byte	(tile_type_dungeon::brick_2<<4)|(tile_type_dungeon::brick_2)
	.byte	(tile_type_dungeon::rocks_2<<4)|(tile_type_dungeon::stairs_up_2)

	.byte	(tile_type_dungeon::brick_2<<4)|(tile_type_dungeon::rocks_2)
	.byte	(tile_type_dungeon::brick_2<<4)|(tile_type_dungeon::rocks_2)
	.byte	(tile_type_dungeon::rocks_2<<4)|(tile_type_dungeon::rocks_2)
	.byte	(tile_type_dungeon::rocks_2<<4)|(tile_type_dungeon::brick_2)
	.byte	(tile_type_dungeon::rocks_2<<4)|(tile_type_dungeon::brick_2)

	.byte	(tile_type_dungeon::brick_2<<4)|(tile_type_dungeon::rocks_2)
	.byte	(tile_type_dungeon::brick_2<<4)|(tile_type_dungeon::brick_2)
	.byte	(tile_type_dungeon::brick_2<<4)|(tile_type_dungeon::brick_2)
	.byte	(tile_type_dungeon::rocks_2<<4)|(tile_type_dungeon::brick_2)
	.byte	(tile_type_dungeon::rocks_2<<4)|(tile_type_dungeon::brick_2)

	.byte	(tile_type_dungeon::brick_2<<4)|(tile_type_dungeon::rocks_2)
	.byte	(tile_type_dungeon::brick_2<<4)|(tile_type_dungeon::rocks_2)
	.byte	(tile_type_dungeon::rocks_2<<4)|(tile_type_dungeon::brick_2)
	.byte	(tile_type_dungeon::rocks_2<<4)|(tile_type_dungeon::brick_2)
	.byte	(tile_type_dungeon::rocks_2<<4)|(tile_type_dungeon::brick_2)

	.byte	(tile_type_dungeon::brick_2<<4)|(tile_type_dungeon::rocks_2)
	.byte	(tile_type_dungeon::brick_2<<4)|(tile_type_dungeon::rocks_2)
	.byte	(tile_type_dungeon::brick_2<<4)|(tile_type_dungeon::brick_2)
	.byte	(tile_type_dungeon::rocks_2<<4)|(tile_type_dungeon::brick_2)
	.byte	(tile_type_dungeon::rocks_2<<4)|(tile_type_dungeon::brick_2)

	.byte	(tile_type_dungeon::brick_2<<4)|(tile_type_dungeon::rocks_2)
	.byte	(tile_type_dungeon::brick_2<<4)|(tile_type_dungeon::rocks_2)
	.byte	(tile_type_dungeon::brick_2<<4)|(tile_type_dungeon::stairs_down_2)
	.byte	(tile_type_dungeon::rocks_2<<4)|(tile_type_dungeon::brick_2)
	.byte	(tile_type_dungeon::rocks_2<<4)|(tile_type_dungeon::brick_2)

	.byte	(tile_type_dungeon::brick_2<<4)|(tile_type_dungeon::rocks_2)
	.byte	(tile_type_dungeon::brick_2<<4)|(tile_type_dungeon::rocks_2)
	.byte	(tile_type_dungeon::rocks_2<<4)|(tile_type_dungeon::rocks_2)
	.byte	(tile_type_dungeon::rocks_2<<4)|(tile_type_dungeon::brick_2)
	.byte	(tile_type_dungeon::rocks_2<<4)|(tile_type_dungeon::brick_2)

	.byte	(tile_type_dungeon::brick_2<<4)|(tile_type_dungeon::rocks_2)
	.byte	(tile_type_dungeon::brick_2<<4)|(tile_type_dungeon::brick_2)
	.byte	(tile_type_dungeon::brick_2<<4)|(tile_type_dungeon::brick_2)
	.byte	(tile_type_dungeon::brick_2<<4)|(tile_type_dungeon::brick_2)
	.byte	(tile_type_dungeon::rocks_2<<4)|(tile_type_dungeon::brick_2)

	.byte	(tile_type_dungeon::brick_2<<4)|(tile_type_dungeon::rocks_2)
	.byte	(tile_type_dungeon::rocks_2<<4)|(tile_type_dungeon::rocks_2)
	.byte	(tile_type_dungeon::rocks_2<<4)|(tile_type_dungeon::rocks_2)
	.byte	(tile_type_dungeon::rocks_2<<4)|(tile_type_dungeon::rocks_2)
	.byte	(tile_type_dungeon::rocks_2<<4)|(tile_type_dungeon::brick_2)

	.byte	(tile_type_dungeon::brick_2<<4)|(tile_type_dungeon::brick_2)
	.byte	(tile_type_dungeon::brick_2<<4)|(tile_type_dungeon::brick_2)
	.byte	(tile_type_dungeon::brick_2<<4)|(tile_type_dungeon::brick_2)
	.byte	(tile_type_dungeon::brick_2<<4)|(tile_type_dungeon::brick_2)
	.byte	(tile_type_dungeon::brick_2<<4)|(tile_type_dungeon::brick_2)
