.include	"map.i"

.segment	"RODATA"

	.export tbl_maps
tbl_maps:
	.addr	0
	.byte	0, 0, 0

	.addr	map_overworld
	.byte	120-1, 120-1, $0F

	.addr	map_02
	.byte	20-1, 20-1, $06

	.addr	map_03
	.byte	20-1, 20-1, $01

	.addr	map_04
	.byte	30-1, 30-1, $00

	.addr	map_05
	.byte	10-1, 10-1, $15

	.addr	map_06
	.byte	30-1, 30-1, $0F

	.addr	map_07
	.byte	24-1, 24-1, $0B

	.addr	map_08
	.byte	30-1, 30-1, $00

	.addr	map_09
	.byte	20-1, 20-1, $00

	.addr	map_0A
	.byte	30-1, 30-1, $04

	.addr	map_0B
	.byte	30-1, 30-1, $00

	.addr	map_0C
	.byte	10-1, 10-1, $10

	.addr	map_0D
	.byte	10-1, 10-1, $10

	.addr	map_0E
	.byte	10-1, 10-1, $10

	.addr	map_0F
	.byte	20-1, 20-1, $10

	.addr	map_10
	.byte	10-1, 10-1, $10

	.addr	map_11
	.byte	10-1, 10-1, $10

	.addr	map_12
	.byte	10-1, 10-1, $10

	.addr	map_13
	.byte	10-1, 10-1, $10

	.addr	map_14
	.byte	10-1, 10-1, $10

	.addr	map_15
	.byte	6-1, 30-1, $10

	.addr	map_16
	.byte	14-1, 14-1, $10

	.addr	map_17
	.byte	14-1, 14-1, $10

	.addr	map_18
	.byte	20-1, 20-1, $10

	.addr	map_19
	.byte	14-1, 12-1, $10

	.addr	map_1A
	.byte	20-1, 20-1, $10

	.addr	map_1B
	.byte	10-1, 10-1, $10

	.addr	map_1C
	.byte	10-1, 10-1, $10

	.addr	map_1D
	.byte	10-1, 10-1, $10
