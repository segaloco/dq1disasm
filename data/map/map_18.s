.include	"map.i"

.segment	"RODATA"

	.export map_18
map_18:
	.byte	(tile_type_dungeon::brick<<4)|(tile_type_dungeon::brick)
	.byte	(tile_type_dungeon::brick<<4)|(tile_type_dungeon::brick)
	.byte	(tile_type_dungeon::brick<<4)|(tile_type_dungeon::brick)
	.byte	(tile_type_dungeon::brick<<4)|(tile_type_dungeon::brick)
	.byte	(tile_type_dungeon::rocks<<4)|(tile_type_dungeon::brick)
	.byte	(tile_type_dungeon::rocks<<4)|(tile_type_dungeon::treasure)
	.byte	(tile_type_dungeon::treasure<<4)|(tile_type_dungeon::treasure)
	.byte	(tile_type_dungeon::rocks<<4)|(tile_type_dungeon::brick)
	.byte	(tile_type_dungeon::brick<<4)|(tile_type_dungeon::brick)
	.byte	(tile_type_dungeon::brick<<4)|(tile_type_dungeon::rocks_2)

	.byte	(tile_type_dungeon::brick_2<<4)|(tile_type_dungeon::rocks_2)
	.byte	(tile_type_dungeon::rocks_2<<4)|(tile_type_dungeon::rocks_2)
	.byte	(tile_type_dungeon::rocks_2<<4)|(tile_type_dungeon::rocks_2)
	.byte	(tile_type_dungeon::rocks_2<<4)|(tile_type_dungeon::brick)
	.byte	(tile_type_dungeon::brick<<4)|(tile_type_dungeon::brick)
	.byte	(tile_type_dungeon::rocks<<4)|(tile_type_dungeon::brick)
	.byte	(tile_type_dungeon::brick<<4)|(tile_type_dungeon::brick)
	.byte	(tile_type_dungeon::brick<<4)|(tile_type_dungeon::brick)
	.byte	(tile_type_dungeon::rocks<<4)|(tile_type_dungeon::rocks)
	.byte	(tile_type_dungeon::brick<<4)|(tile_type_dungeon::brick)

	.byte	(tile_type_dungeon::brick<<4)|(tile_type_dungeon::rocks)
	.byte	(tile_type_dungeon::brick<<4)|(tile_type_dungeon::brick)
	.byte	(tile_type_dungeon::brick<<4)|(tile_type_dungeon::brick)
	.byte	(tile_type_dungeon::rocks<<4)|(tile_type_dungeon::brick)
	.byte	(tile_type_dungeon::rocks<<4)|(tile_type_dungeon::brick_2)
	.byte	(tile_type_dungeon::rocks_2<<4)|(tile_type_dungeon::brick_2)
	.byte	(tile_type_dungeon::brick_2<<4)|(tile_type_dungeon::brick_2)
	.byte	(tile_type_dungeon::rocks_2<<4)|(tile_type_dungeon::brick_2)
	.byte	(tile_type_dungeon::brick_2<<4)|(tile_type_dungeon::brick)
	.byte	(tile_type_dungeon::rocks<<4)|(tile_type_dungeon::brick)

	.byte	(tile_type_dungeon::brick<<4)|(tile_type_dungeon::rocks)
	.byte	(tile_type_dungeon::brick<<4)|(tile_type_dungeon::rocks)
	.byte	(tile_type_dungeon::rocks<<4)|(tile_type_dungeon::brick)
	.byte	(tile_type_dungeon::brick<<4)|(tile_type_dungeon::brick)
	.byte	(tile_type_dungeon::rocks<<4)|(tile_type_dungeon::brick)
	.byte	(tile_type_dungeon::rocks<<4)|(tile_type_dungeon::brick)
	.byte	(tile_type_dungeon::rocks<<4)|(tile_type_dungeon::rocks)
	.byte	(tile_type_dungeon::rocks<<4)|(tile_type_dungeon::rocks)
	.byte	(tile_type_dungeon::rocks<<4)|(tile_type_dungeon::brick)
	.byte	(tile_type_dungeon::rocks<<4)|(tile_type_dungeon::brick_2)

	.byte	(tile_type_dungeon::brick_2<<4)|(tile_type_dungeon::rocks_2)
	.byte	(tile_type_dungeon::brick_2<<4)|(tile_type_dungeon::brick_2)
	.byte	(tile_type_dungeon::brick_2<<4)|(tile_type_dungeon::brick_2)
	.byte	(tile_type_dungeon::rocks_2<<4)|(tile_type_dungeon::rocks)
	.byte	(tile_type_dungeon::rocks<<4)|(tile_type_dungeon::brick)
	.byte	(tile_type_dungeon::rocks<<4)|(tile_type_dungeon::rocks)
	.byte	(tile_type_dungeon::rocks<<4)|(tile_type_dungeon::brick)
	.byte	(tile_type_dungeon::rocks<<4)|(tile_type_dungeon::brick)
	.byte	(tile_type_dungeon::rocks<<4)|(tile_type_dungeon::brick)
	.byte	(tile_type_dungeon::brick<<4)|(tile_type_dungeon::brick)

	.byte	(tile_type_dungeon::brick<<4)|(tile_type_dungeon::rocks)
	.byte	(tile_type_dungeon::rocks<<4)|(tile_type_dungeon::rocks)
	.byte	(tile_type_dungeon::rocks<<4)|(tile_type_dungeon::brick)
	.byte	(tile_type_dungeon::brick<<4)|(tile_type_dungeon::brick)
	.byte	(tile_type_dungeon::rocks<<4)|(tile_type_dungeon::brick_2)
	.byte	(tile_type_dungeon::brick_2<<4)|(tile_type_dungeon::brick_2)
	.byte	(tile_type_dungeon::rocks_2<<4)|(tile_type_dungeon::brick_2)
	.byte	(tile_type_dungeon::rocks_2<<4)|(tile_type_dungeon::brick_2)
	.byte	(tile_type_dungeon::rocks_2<<4)|(tile_type_dungeon::brick)
	.byte	(tile_type_dungeon::rocks<<4)|(tile_type_dungeon::brick)

	.byte	(tile_type_dungeon::brick<<4)|(tile_type_dungeon::brick)
	.byte	(tile_type_dungeon::brick<<4)|(tile_type_dungeon::brick)
	.byte	(tile_type_dungeon::rocks<<4)|(tile_type_dungeon::rocks)
	.byte	(tile_type_dungeon::rocks<<4)|(tile_type_dungeon::rocks)
	.byte	(tile_type_dungeon::rocks<<4)|(tile_type_dungeon::brick)
	.byte	(tile_type_dungeon::rocks<<4)|(tile_type_dungeon::brick)
	.byte	(tile_type_dungeon::rocks<<4)|(tile_type_dungeon::brick)
	.byte	(tile_type_dungeon::rocks<<4)|(tile_type_dungeon::brick)
	.byte	(tile_type_dungeon::rocks<<4)|(tile_type_dungeon::brick)
	.byte	(tile_type_dungeon::rocks<<4)|(tile_type_dungeon::brick_2)

	.byte	(tile_type_dungeon::brick_2<<4)|(tile_type_dungeon::rocks_2)
	.byte	(tile_type_dungeon::brick_2<<4)|(tile_type_dungeon::brick_2)
	.byte	(tile_type_dungeon::rocks_2<<4)|(tile_type_dungeon::brick_2)
	.byte	(tile_type_dungeon::brick_2<<4)|(tile_type_dungeon::brick)
	.byte	(tile_type_dungeon::brick<<4)|(tile_type_dungeon::brick)
	.byte	(tile_type_dungeon::brick<<4)|(tile_type_dungeon::brick)
	.byte	(tile_type_dungeon::brick<<4)|(tile_type_dungeon::brick)
	.byte	(tile_type_dungeon::brick<<4)|(tile_type_dungeon::brick)
	.byte	(tile_type_dungeon::brick<<4)|(tile_type_dungeon::brick)
	.byte	(tile_type_dungeon::brick<<4)|(tile_type_dungeon::brick)

	.byte	(tile_type_dungeon::brick<<4)|(tile_type_dungeon::rocks)
	.byte	(tile_type_dungeon::brick<<4)|(tile_type_dungeon::rocks)
	.byte	(tile_type_dungeon::rocks<<4)|(tile_type_dungeon::brick)
	.byte	(tile_type_dungeon::rocks<<4)|(tile_type_dungeon::rocks)
	.byte	(tile_type_dungeon::rocks<<4)|(tile_type_dungeon::brick_2)
	.byte	(tile_type_dungeon::rocks_2<<4)|(tile_type_dungeon::rocks_2)
	.byte	(tile_type_dungeon::rocks_2<<4)|(tile_type_dungeon::rocks_2)
	.byte	(tile_type_dungeon::rocks_2<<4)|(tile_type_dungeon::brick_2)
	.byte	(tile_type_dungeon::rocks_2<<4)|(tile_type_dungeon::rocks)
	.byte	(tile_type_dungeon::rocks<<4)|(tile_type_dungeon::brick)

	.byte	(tile_type_dungeon::brick<<4)|(tile_type_dungeon::rocks)
	.byte	(tile_type_dungeon::brick<<4)|(tile_type_dungeon::rocks)
	.byte	(tile_type_dungeon::brick<<4)|(tile_type_dungeon::brick)
	.byte	(tile_type_dungeon::brick<<4)|(tile_type_dungeon::brick)
	.byte	(tile_type_dungeon::brick<<4)|(tile_type_dungeon::brick)
	.byte	(tile_type_dungeon::rocks<<4)|(tile_type_dungeon::brick)
	.byte	(tile_type_dungeon::brick<<4)|(tile_type_dungeon::brick)
	.byte	(tile_type_dungeon::brick<<4)|(tile_type_dungeon::brick)
	.byte	(tile_type_dungeon::rocks<<4)|(tile_type_dungeon::brick)
	.byte	(tile_type_dungeon::brick<<4)|(tile_type_dungeon::brick_2)

	.byte	(tile_type_dungeon::brick_2<<4)|(tile_type_dungeon::rocks_2)
	.byte	(tile_type_dungeon::brick_2<<4)|(tile_type_dungeon::rocks_2)
	.byte	(tile_type_dungeon::brick_2<<4)|(tile_type_dungeon::rocks_2)
	.byte	(tile_type_dungeon::brick_2<<4)|(tile_type_dungeon::rocks)
	.byte	(tile_type_dungeon::rocks<<4)|(tile_type_dungeon::brick)
	.byte	(tile_type_dungeon::rocks<<4)|(tile_type_dungeon::rocks)
	.byte	(tile_type_dungeon::rocks<<4)|(tile_type_dungeon::rocks)
	.byte	(tile_type_dungeon::rocks<<4)|(tile_type_dungeon::brick)
	.byte	(tile_type_dungeon::brick<<4)|(tile_type_dungeon::brick)
	.byte	(tile_type_dungeon::brick<<4)|(tile_type_dungeon::brick)

	.byte	(tile_type_dungeon::brick<<4)|(tile_type_dungeon::rocks)
	.byte	(tile_type_dungeon::brick<<4)|(tile_type_dungeon::brick)
	.byte	(tile_type_dungeon::brick<<4)|(tile_type_dungeon::rocks)
	.byte	(tile_type_dungeon::stairs_up<<4)|(tile_type_dungeon::brick)
	.byte	(tile_type_dungeon::brick<<4)|(tile_type_dungeon::brick_2)
	.byte	(tile_type_dungeon::rocks_2<<4)|(tile_type_dungeon::brick_2)
	.byte	(tile_type_dungeon::brick_2<<4)|(tile_type_dungeon::brick_2)
	.byte	(tile_type_dungeon::rocks_2<<4)|(tile_type_dungeon::brick_2)
	.byte	(tile_type_dungeon::rocks_2<<4)|(tile_type_dungeon::rocks)
	.byte	(tile_type_dungeon::brick<<4)|(tile_type_dungeon::brick)

	.byte	(tile_type_dungeon::brick<<4)|(tile_type_dungeon::rocks)
	.byte	(tile_type_dungeon::brick<<4)|(tile_type_dungeon::rocks)
	.byte	(tile_type_dungeon::brick<<4)|(tile_type_dungeon::rocks)
	.byte	(tile_type_dungeon::brick<<4)|(tile_type_dungeon::brick)
	.byte	(tile_type_dungeon::brick<<4)|(tile_type_dungeon::brick)
	.byte	(tile_type_dungeon::rocks<<4)|(tile_type_dungeon::brick)
	.byte	(tile_type_dungeon::rocks<<4)|(tile_type_dungeon::brick)
	.byte	(tile_type_dungeon::brick<<4)|(tile_type_dungeon::brick)
	.byte	(tile_type_dungeon::rocks<<4)|(tile_type_dungeon::rocks)
	.byte	(tile_type_dungeon::brick<<4)|(tile_type_dungeon::rocks)

	.byte	(tile_type_dungeon::brick<<4)|(tile_type_dungeon::rocks)
	.byte	(tile_type_dungeon::brick<<4)|(tile_type_dungeon::rocks)
	.byte	(tile_type_dungeon::rocks<<4)|(tile_type_dungeon::rocks)
	.byte	(tile_type_dungeon::rocks<<4)|(tile_type_dungeon::rocks)
	.byte	(tile_type_dungeon::rocks<<4)|(tile_type_dungeon::brick)
	.byte	(tile_type_dungeon::rocks<<4)|(tile_type_dungeon::rocks)
	.byte	(tile_type_dungeon::rocks<<4)|(tile_type_dungeon::rocks)
	.byte	(tile_type_dungeon::rocks<<4)|(tile_type_dungeon::brick)
	.byte	(tile_type_dungeon::brick<<4)|(tile_type_dungeon::rocks)
	.byte	(tile_type_dungeon::brick<<4)|(tile_type_dungeon::brick)

	.byte	(tile_type_dungeon::brick<<4)|(tile_type_dungeon::rocks)
	.byte	(tile_type_dungeon::brick<<4)|(tile_type_dungeon::brick)
	.byte	(tile_type_dungeon::brick<<4)|(tile_type_dungeon::rocks)
	.byte	(tile_type_dungeon::brick<<4)|(tile_type_dungeon::brick)
	.byte	(tile_type_dungeon::brick<<4)|(tile_type_dungeon::brick)
	.byte	(tile_type_dungeon::brick<<4)|(tile_type_dungeon::brick)
	.byte	(tile_type_dungeon::brick<<4)|(tile_type_dungeon::brick)
	.byte	(tile_type_dungeon::rocks<<4)|(tile_type_dungeon::rocks)
	.byte	(tile_type_dungeon::rocks<<4)|(tile_type_dungeon::rocks)
	.byte	(tile_type_dungeon::rocks<<4)|(tile_type_dungeon::brick)

	.byte	(tile_type_dungeon::rocks<<4)|(tile_type_dungeon::rocks)
	.byte	(tile_type_dungeon::rocks<<4)|(tile_type_dungeon::rocks)
	.byte	(tile_type_dungeon::brick<<4)|(tile_type_dungeon::rocks)
	.byte	(tile_type_dungeon::rocks<<4)|(tile_type_dungeon::rocks)
	.byte	(tile_type_dungeon::rocks<<4)|(tile_type_dungeon::rocks)
	.byte	(tile_type_dungeon::rocks<<4)|(tile_type_dungeon::brick)
	.byte	(tile_type_dungeon::rocks<<4)|(tile_type_dungeon::brick)
	.byte	(tile_type_dungeon::rocks<<4)|(tile_type_dungeon::brick)
	.byte	(tile_type_dungeon::rocks<<4)|(tile_type_dungeon::brick)
	.byte	(tile_type_dungeon::rocks<<4)|(tile_type_dungeon::brick)

	.byte	(tile_type_dungeon::brick<<4)|(tile_type_dungeon::brick)
	.byte	(tile_type_dungeon::brick<<4)|(tile_type_dungeon::rocks)
	.byte	(tile_type_dungeon::rocks<<4)|(tile_type_dungeon::rocks)
	.byte	(tile_type_dungeon::brick<<4)|(tile_type_dungeon::brick)
	.byte	(tile_type_dungeon::brick<<4)|(tile_type_dungeon::brick)
	.byte	(tile_type_dungeon::brick<<4)|(tile_type_dungeon::brick)
	.byte	(tile_type_dungeon::brick<<4)|(tile_type_dungeon::brick)
	.byte	(tile_type_dungeon::rocks<<4)|(tile_type_dungeon::brick)
	.byte	(tile_type_dungeon::brick<<4)|(tile_type_dungeon::brick)
	.byte	(tile_type_dungeon::brick<<4)|(tile_type_dungeon::brick)

	.byte	(tile_type_dungeon::brick<<4)|(tile_type_dungeon::brick)
	.byte	(tile_type_dungeon::rocks<<4)|(tile_type_dungeon::brick)
	.byte	(tile_type_dungeon::rocks<<4)|(tile_type_dungeon::brick)
	.byte	(tile_type_dungeon::brick<<4)|(tile_type_dungeon::rocks)
	.byte	(tile_type_dungeon::brick<<4)|(tile_type_dungeon::rocks)
	.byte	(tile_type_dungeon::rocks<<4)|(tile_type_dungeon::rocks)
	.byte	(tile_type_dungeon::rocks<<4)|(tile_type_dungeon::rocks)
	.byte	(tile_type_dungeon::rocks<<4)|(tile_type_dungeon::rocks)
	.byte	(tile_type_dungeon::rocks<<4)|(tile_type_dungeon::door)
	.byte	(tile_type_dungeon::rocks<<4)|(tile_type_dungeon::rocks)

	.byte	(tile_type_dungeon::brick<<4)|(tile_type_dungeon::stairs_down)
	.byte	(tile_type_dungeon::brick<<4)|(tile_type_dungeon::brick)
	.byte	(tile_type_dungeon::rocks<<4)|(tile_type_dungeon::rocks)
	.byte	(tile_type_dungeon::rocks<<4)|(tile_type_dungeon::rocks)
	.byte	(tile_type_dungeon::rocks<<4)|(tile_type_dungeon::rocks)
	.byte	(tile_type_dungeon::brick<<4)|(tile_type_dungeon::rocks)
	.byte	(tile_type_dungeon::brick<<4)|(tile_type_dungeon::brick)
	.byte	(tile_type_dungeon::brick<<4)|(tile_type_dungeon::rocks)
	.byte	(tile_type_dungeon::brick<<4)|(tile_type_dungeon::brick)
	.byte	(tile_type_dungeon::brick<<4)|(tile_type_dungeon::brick)

	.byte	(tile_type_dungeon::brick<<4)|(tile_type_dungeon::brick)
	.byte	(tile_type_dungeon::brick<<4)|(tile_type_dungeon::brick)
	.byte	(tile_type_dungeon::brick<<4)|(tile_type_dungeon::brick)
	.byte	(tile_type_dungeon::brick<<4)|(tile_type_dungeon::brick)
	.byte	(tile_type_dungeon::brick<<4)|(tile_type_dungeon::brick)
	.byte	(tile_type_dungeon::brick<<4)|(tile_type_dungeon::brick)
	.byte	(tile_type_dungeon::brick<<4)|(tile_type_dungeon::rocks)
	.byte	(tile_type_dungeon::brick<<4)|(tile_type_dungeon::brick)
	.byte	(tile_type_dungeon::brick<<4)|(tile_type_dungeon::rocks)
	.byte	(tile_type_dungeon::brick<<4)|(tile_type_dungeon::brick)
