.include	"map.i"

.segment	"RODATA"

	.export map_05
map_05:
	.byte	(tile_type_town::rocks<<4)|(tile_type_town::rocks)
	.byte	(tile_type_town::rocks<<4)|(tile_type_town::rocks)
	.byte	(tile_type_town::rocks<<4)|(tile_type_town::rocks)
	.byte	(tile_type_town::rocks<<4)|(tile_type_town::rocks)
	.byte	(tile_type_town::rocks<<4)|(tile_type_town::rocks)

	.byte	(tile_type_town::rocks<<4)|(tile_type_town::brick)
	.byte	(tile_type_town::brick<<4)|(tile_type_town::brick)
	.byte	(tile_type_town::brick<<4)|(tile_type_town::brick)
	.byte	(tile_type_town::treasure<<4)|(tile_type_town::brick)
	.byte	(tile_type_town::brick<<4)|(tile_type_town::rocks)

	.byte	(tile_type_town::rocks<<4)|(tile_type_town::brick)
	.byte	(tile_type_town::misc<<4)|(tile_type_town::misc)
	.byte	(tile_type_town::misc<<4)|(tile_type_town::misc)
	.byte	(tile_type_town::misc<<4)|(tile_type_town::misc)
	.byte	(tile_type_town::brick<<4)|(tile_type_town::rocks)

	.byte	(tile_type_town::rocks<<4)|(tile_type_town::brick)
	.byte	(tile_type_town::misc<<4)|(tile_type_town::brick)
	.byte	(tile_type_town::misc<<4)|(tile_type_town::misc)
	.byte	(tile_type_town::brick<<4)|(tile_type_town::misc)
	.byte	(tile_type_town::brick<<4)|(tile_type_town::rocks)

	.byte	(tile_type_town::rocks<<4)|(tile_type_town::brick)
	.byte	(tile_type_town::brick<<4)|(tile_type_town::brick)
	.byte	(tile_type_town::treasure<<4)|(tile_type_town::treasure)
	.byte	(tile_type_town::brick<<4)|(tile_type_town::brick)
	.byte	(tile_type_town::brick<<4)|(tile_type_town::rocks)

	.byte	(tile_type_town::rocks<<4)|(tile_type_town::brick)
	.byte	(tile_type_town::brick<<4)|(tile_type_town::brick)
	.byte	(tile_type_town::brick<<4)|(tile_type_town::brick)
	.byte	(tile_type_town::brick<<4)|(tile_type_town::brick)
	.byte	(tile_type_town::brick<<4)|(tile_type_town::rocks)

	.byte	(tile_type_town::rocks<<4)|(tile_type_town::brick)
	.byte	(tile_type_town::brick<<4)|(tile_type_town::brick)
	.byte	(tile_type_town::brick<<4)|(tile_type_town::brick)
	.byte	(tile_type_town::brick<<4)|(tile_type_town::brick)
	.byte	(tile_type_town::brick<<4)|(tile_type_town::rocks)

	.byte	(tile_type_town::rocks<<4)|(tile_type_town::rocks)
	.byte	(tile_type_town::rocks<<4)|(tile_type_town::rocks)
	.byte	(tile_type_town::door<<4)|(tile_type_town::rocks)
	.byte	(tile_type_town::rocks<<4)|(tile_type_town::rocks)
	.byte	(tile_type_town::rocks<<4)|(tile_type_town::rocks)

	.byte	(tile_type_town::rocks<<4)|(tile_type_town::brick)
	.byte	(tile_type_town::brick<<4)|(tile_type_town::brick)
	.byte	(tile_type_town::brick<<4)|(tile_type_town::brick)
	.byte	(tile_type_town::brick<<4)|(tile_type_town::brick)
	.byte	(tile_type_town::stairs_down<<4)|(tile_type_town::rocks)

	.byte	(tile_type_town::rocks<<4)|(tile_type_town::rocks)
	.byte	(tile_type_town::rocks<<4)|(tile_type_town::rocks)
	.byte	(tile_type_town::rocks<<4)|(tile_type_town::rocks)
	.byte	(tile_type_town::rocks<<4)|(tile_type_town::rocks)
	.byte	(tile_type_town::rocks<<4)|(tile_type_town::rocks)
