.include	"map.i"

.segment	"RODATA"

	.export map_0E
map_0E:
	.byte	(tile_type_sub::rocks_2<<4)|(tile_type_sub::brick)
	.byte	(tile_type_sub::brick_2<<4)|(tile_type_sub::brick_2)
	.byte	(tile_type_sub::brick_2<<4)|(tile_type_sub::brick_2)
	.byte	(tile_type_sub::brick_2<<4)|(tile_type_sub::brick_2)
	.byte	(tile_type_sub::brick<<4)|(tile_type_sub::brick)

	.byte	(tile_type_sub::rocks<<4)|(tile_type_sub::brick)
	.byte	(tile_type_sub::brick<<4)|(tile_type_sub::rocks)
	.byte	(tile_type_sub::brick<<4)|(tile_type_sub::rocks)
	.byte	(tile_type_sub::brick<<4)|(tile_type_sub::rocks)
	.byte	(tile_type_sub::brick<<4)|(tile_type_sub::brick)

	.byte	(tile_type_sub::rocks<<4)|(tile_type_sub::brick)
	.byte	(tile_type_sub::rocks<<4)|(tile_type_sub::rocks)
	.byte	(tile_type_sub::brick<<4)|(tile_type_sub::brick)
	.byte	(tile_type_sub::brick<<4)|(tile_type_sub::rocks)
	.byte	(tile_type_sub::rocks<<4)|(tile_type_sub::brick)

	.byte	(tile_type_sub::rocks<<4)|(tile_type_sub::brick)
	.byte	(tile_type_sub::brick<<4)|(tile_type_sub::brick)
	.byte	(tile_type_sub::rocks<<4)|(tile_type_sub::rocks)
	.byte	(tile_type_sub::rocks<<4)|(tile_type_sub::brick)
	.byte	(tile_type_sub::brick<<4)|(tile_type_sub::brick)

	.byte	(tile_type_sub::stairs_up<<4)|(tile_type_sub::brick)
	.byte	(tile_type_sub::rocks<<4)|(tile_type_sub::brick)
	.byte	(tile_type_sub::rocks<<4)|(tile_type_sub::brick)
	.byte	(tile_type_sub::rocks<<4)|(tile_type_sub::brick)
	.byte	(tile_type_sub::rocks<<4)|(tile_type_sub::brick)

	.byte	(tile_type_sub::brick<<4)|(tile_type_sub::brick)
	.byte	(tile_type_sub::rocks<<4)|(tile_type_sub::brick)
	.byte	(tile_type_sub::brick<<4)|(tile_type_sub::treasure)
	.byte	(tile_type_sub::rocks<<4)|(tile_type_sub::brick)
	.byte	(tile_type_sub::rocks<<4)|(tile_type_sub::brick)

	.byte	(tile_type_sub::rocks<<4)|(tile_type_sub::brick)
	.byte	(tile_type_sub::brick<<4)|(tile_type_sub::brick)
	.byte	(tile_type_sub::rocks<<4)|(tile_type_sub::rocks)
	.byte	(tile_type_sub::rocks<<4)|(tile_type_sub::brick)
	.byte	(tile_type_sub::brick<<4)|(tile_type_sub::brick)

	.byte	(tile_type_sub::rocks<<4)|(tile_type_sub::brick)
	.byte	(tile_type_sub::rocks<<4)|(tile_type_sub::rocks)
	.byte	(tile_type_sub::brick<<4)|(tile_type_sub::brick)
	.byte	(tile_type_sub::brick<<4)|(tile_type_sub::rocks)
	.byte	(tile_type_sub::rocks<<4)|(tile_type_sub::brick)

	.byte	(tile_type_sub::rocks<<4)|(tile_type_sub::brick)
	.byte	(tile_type_sub::brick<<4)|(tile_type_sub::rocks)
	.byte	(tile_type_sub::brick<<4)|(tile_type_sub::rocks)
	.byte	(tile_type_sub::brick<<4)|(tile_type_sub::rocks)
	.byte	(tile_type_sub::brick<<4)|(tile_type_sub::brick)

	.byte	(tile_type_sub::rocks<<4)|(tile_type_sub::brick)
	.byte	(tile_type_sub::brick<<4)|(tile_type_sub::brick)
	.byte	(tile_type_sub::brick<<4)|(tile_type_sub::brick)
	.byte	(tile_type_sub::brick<<4)|(tile_type_sub::brick)
	.byte	(tile_type_sub::brick<<4)|(tile_type_sub::brick)
