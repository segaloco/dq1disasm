.include	"map.i"

.segment	"RODATA"

	.export map_14
map_14:
	.byte	(tile_type_dungeon::stairs_up<<4)|(tile_type_dungeon::brick)
	.byte	(tile_type_dungeon::brick<<4)|(tile_type_dungeon::brick)
	.byte	(tile_type_dungeon::brick<<4)|(tile_type_dungeon::brick)
	.byte	(tile_type_dungeon::brick<<4)|(tile_type_dungeon::brick)
	.byte	(tile_type_dungeon::brick<<4)|(tile_type_dungeon::stairs_down)

	.byte	(tile_type_dungeon::rocks<<4)|(tile_type_dungeon::rocks)
	.byte	(tile_type_dungeon::rocks<<4)|(tile_type_dungeon::rocks)
	.byte	(tile_type_dungeon::rocks<<4)|(tile_type_dungeon::rocks_2)
	.byte	(tile_type_dungeon::rocks_2<<4)|(tile_type_dungeon::rocks)
	.byte	(tile_type_dungeon::rocks_2<<4)|(tile_type_dungeon::rocks_2)

	.byte	(tile_type_dungeon::rocks<<4)|(tile_type_dungeon::rocks)
	.byte	(tile_type_dungeon::rocks<<4)|(tile_type_dungeon::rocks)
	.byte	(tile_type_dungeon::rocks<<4)|(tile_type_dungeon::rocks)
	.byte	(tile_type_dungeon::rocks<<4)|(tile_type_dungeon::rocks)
	.byte	(tile_type_dungeon::rocks<<4)|(tile_type_dungeon::rocks)

	.byte	(tile_type_dungeon::rocks<<4)|(tile_type_dungeon::rocks)
	.byte	(tile_type_dungeon::rocks<<4)|(tile_type_dungeon::rocks)
	.byte	(tile_type_dungeon::rocks<<4)|(tile_type_dungeon::rocks)
	.byte	(tile_type_dungeon::rocks<<4)|(tile_type_dungeon::rocks)
	.byte	(tile_type_dungeon::rocks<<4)|(tile_type_dungeon::rocks)

	.byte	(tile_type_dungeon::rocks<<4)|(tile_type_dungeon::brick)
	.byte	(tile_type_dungeon::brick<<4)|(tile_type_dungeon::brick)
	.byte	(tile_type_dungeon::brick<<4)|(tile_type_dungeon::brick)
	.byte	(tile_type_dungeon::brick<<4)|(tile_type_dungeon::brick)
	.byte	(tile_type_dungeon::brick<<4)|(tile_type_dungeon::rocks)

	.byte	(tile_type_dungeon::rocks<<4)|(tile_type_dungeon::brick)
	.byte	(tile_type_dungeon::brick<<4)|(tile_type_dungeon::brick)
	.byte	(tile_type_dungeon::brick<<4)|(tile_type_dungeon::brick)
	.byte	(tile_type_dungeon::brick<<4)|(tile_type_dungeon::brick)
	.byte	(tile_type_dungeon::brick<<4)|(tile_type_dungeon::rocks)

	.byte	(tile_type_dungeon::stairs_up<<4)|(tile_type_dungeon::brick)
	.byte	(tile_type_dungeon::brick<<4)|(tile_type_dungeon::brick)
	.byte	(tile_type_dungeon::brick<<4)|(tile_type_dungeon::brick)
	.byte	(tile_type_dungeon::brick<<4)|(tile_type_dungeon::brick)
	.byte	(tile_type_dungeon::brick<<4)|(tile_type_dungeon::stairs_down)

	.byte	(tile_type_dungeon::rocks<<4)|(tile_type_dungeon::brick)
	.byte	(tile_type_dungeon::brick<<4)|(tile_type_dungeon::brick)
	.byte	(tile_type_dungeon::brick<<4)|(tile_type_dungeon::brick)
	.byte	(tile_type_dungeon::brick<<4)|(tile_type_dungeon::brick)
	.byte	(tile_type_dungeon::brick<<4)|(tile_type_dungeon::rocks)

	.byte	(tile_type_dungeon::rocks<<4)|(tile_type_dungeon::brick)
	.byte	(tile_type_dungeon::brick<<4)|(tile_type_dungeon::brick)
	.byte	(tile_type_dungeon::brick<<4)|(tile_type_dungeon::brick)
	.byte	(tile_type_dungeon::brick<<4)|(tile_type_dungeon::brick)
	.byte	(tile_type_dungeon::brick<<4)|(tile_type_dungeon::rocks)

	.byte	(tile_type_dungeon::rocks<<4)|(tile_type_dungeon::rocks)
	.byte	(tile_type_dungeon::rocks<<4)|(tile_type_dungeon::rocks)
	.byte	(tile_type_dungeon::rocks<<4)|(tile_type_dungeon::rocks)
	.byte	(tile_type_dungeon::rocks<<4)|(tile_type_dungeon::rocks)
	.byte	(tile_type_dungeon::rocks<<4)|(tile_type_dungeon::rocks)
