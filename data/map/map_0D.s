.include	"map.i"

.segment	"RODATA"

	.export map_0D
map_0D:
	.byte	(tile_type_sub::rocks<<4)|(tile_type_sub::brick)
	.byte	(tile_type_sub::brick<<4)|(tile_type_sub::brick)
	.byte	(tile_type_sub::brick<<4)|(tile_type_sub::brick)
	.byte	(tile_type_sub::brick<<4)|(tile_type_sub::brick)
	.byte	(tile_type_sub::brick<<4)|(tile_type_sub::rocks)

	.byte	(tile_type_sub::brick_2<<4)|(tile_type_sub::brick_2)
	.byte	(tile_type_sub::brick_2<<4)|(tile_type_sub::brick_2)
	.byte	(tile_type_sub::brick_2<<4)|(tile_type_sub::brick_2)
	.byte	(tile_type_sub::brick_2<<4)|(tile_type_sub::brick_2)
	.byte	(tile_type_sub::brick<<4)|(tile_type_sub::brick)

	.byte	(tile_type_sub::brick<<4)|(tile_type_sub::brick)
	.byte	(tile_type_sub::rocks<<4)|(tile_type_sub::rocks)
	.byte	(tile_type_sub::rocks<<4)|(tile_type_sub::rocks)
	.byte	(tile_type_sub::rocks<<4)|(tile_type_sub::rocks)
	.byte	(tile_type_sub::brick<<4)|(tile_type_sub::brick)

	.byte	(tile_type_sub::brick<<4)|(tile_type_sub::brick)
	.byte	(tile_type_sub::rocks<<4)|(tile_type_sub::brick)
	.byte	(tile_type_sub::brick<<4)|(tile_type_sub::rocks)
	.byte	(tile_type_sub::brick<<4)|(tile_type_sub::rocks)
	.byte	(tile_type_sub::brick<<4)|(tile_type_sub::brick)

	.byte	(tile_type_sub::brick_2<<4)|(tile_type_sub::brick_2)
	.byte	(tile_type_sub::rocks_2<<4)|(tile_type_sub::treasure_2)
	.byte	(tile_type_sub::brick_2<<4)|(tile_type_sub::brick_2)
	.byte	(tile_type_sub::brick_2<<4)|(tile_type_sub::brick_2)
	.byte	(tile_type_sub::brick<<4)|(tile_type_sub::brick)

	.byte	(tile_type_sub::brick<<4)|(tile_type_sub::brick)
	.byte	(tile_type_sub::rocks<<4)|(tile_type_sub::brick)
	.byte	(tile_type_sub::brick<<4)|(tile_type_sub::rocks)
	.byte	(tile_type_sub::brick<<4)|(tile_type_sub::rocks)
	.byte	(tile_type_sub::brick<<4)|(tile_type_sub::brick)

	.byte	(tile_type_sub::brick<<4)|(tile_type_sub::brick)
	.byte	(tile_type_sub::rocks<<4)|(tile_type_sub::rocks)
	.byte	(tile_type_sub::rocks<<4)|(tile_type_sub::rocks)
	.byte	(tile_type_sub::rocks<<4)|(tile_type_sub::rocks)
	.byte	(tile_type_sub::brick<<4)|(tile_type_sub::brick)

	.byte	(tile_type_sub::brick_2<<4)|(tile_type_sub::brick_2)
	.byte	(tile_type_sub::brick_2<<4)|(tile_type_sub::brick_2)
	.byte	(tile_type_sub::brick_2<<4)|(tile_type_sub::brick_2)
	.byte	(tile_type_sub::brick_2<<4)|(tile_type_sub::brick_2)
	.byte	(tile_type_sub::brick<<4)|(tile_type_sub::brick)

	.byte	(tile_type_sub::rocks<<4)|(tile_type_sub::brick)
	.byte	(tile_type_sub::brick<<4)|(tile_type_sub::brick)
	.byte	(tile_type_sub::brick<<4)|(tile_type_sub::brick)
	.byte	(tile_type_sub::brick<<4)|(tile_type_sub::brick)
	.byte	(tile_type_sub::brick<<4)|(tile_type_sub::rocks)

	.byte	(tile_type_sub::rocks<<4)|(tile_type_sub::rocks)
	.byte	(tile_type_sub::rocks<<4)|(tile_type_sub::rocks)
	.byte	(tile_type_sub::stairs_up<<4)|(tile_type_sub::brick)
	.byte	(tile_type_sub::rocks<<4)|(tile_type_sub::rocks)
	.byte	(tile_type_sub::rocks<<4)|(tile_type_sub::rocks)
