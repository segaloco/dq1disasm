.segment	"CHR0DATA"

	.export chr00_base
chr00_base:
	.incbin	"data/chr00.dw/tile_bg_00.bin"
	.incbin	"data/chr00.dw/tile_bg_01.bin"
	.incbin	"data/chr00.dw/tile_bg_02.bin"
	.incbin	"data/chr00.dw/tile_bg_03.bin"
	.incbin	"data/chr00.dw/tile_bg_04.bin"
	.incbin	"data/chr00.dw/tile_bg_05.bin"
	.incbin	"data/chr00.dw/tile_bg_06.bin"
	.incbin	"data/chr00.dw/tile_bg_07.bin"
	.incbin	"data/chr00.dw/tile_bg_08.bin"
	.incbin	"data/chr00.dw/tile_bg_09.bin"
	.incbin	"data/chr00.dw/tile_bg_0A.bin"
	.incbin	"data/chr00.dw/tile_bg_0B.bin"
	.incbin	"data/chr00.dw/tile_bg_0C.bin"
	.incbin	"data/chr00.dw/tile_bg_0D.bin"
	.incbin	"data/chr00.dw/tile_bg_0E.bin"
	.incbin	"data/chr00.dw/tile_bg_0F.bin"
	.incbin	"data/chr00.dw/tile_bg_10.bin"
	.incbin	"data/chr00.dw/tile_bg_11.bin"
	.incbin	"data/chr00.dw/tile_bg_12.bin"
	.incbin	"data/chr00.dw/tile_bg_13.bin"
	.incbin	"data/chr00.dw/tile_bg_14.bin"
	.incbin	"data/chr00.dw/tile_bg_15.bin"
	.incbin	"data/chr00.dw/tile_bg_16.bin"
	.incbin	"data/chr00.dw/tile_bg_17.bin"
	.incbin	"data/chr00.dw/tile_bg_18.bin"
	.incbin	"data/chr00.dw/tile_bg_19.bin"
	.incbin	"data/chr00.dw/tile_bg_1A.bin"
	.incbin	"data/chr00.dw/tile_bg_1B.bin"
	.incbin	"data/chr00.dw/tile_bg_1C.bin"
	.incbin	"data/chr00.dw/tile_bg_1D.bin"
	.incbin	"data/chr00.dw/tile_bg_1E.bin"
	.incbin	"data/chr00.dw/tile_bg_1F.bin"
	.incbin	"data/chr00.dw/tile_bg_20.bin"
	.incbin	"data/chr00.dw/tile_bg_21.bin"
	.incbin	"data/chr00.dw/tile_bg_22.bin"
	.incbin	"data/chr00.dw/tile_bg_23.bin"
	.incbin	"data/chr00.dw/tile_bg_24.bin"
	.incbin	"data/chr00.dw/tile_bg_25.bin"
	.incbin	"data/chr00.dw/tile_bg_26.bin"
	.incbin	"data/chr00.dw/tile_bg_27.bin"
	.incbin	"data/chr00.dw/tile_bg_28.bin"
	.incbin	"data/chr00.dw/tile_bg_29.bin"
	.incbin	"data/chr00.dw/tile_bg_2A.bin"
	.incbin	"data/chr00.dw/tile_bg_2B.bin"
	.incbin	"data/chr00.dw/tile_bg_2C.bin"
	.incbin	"data/chr00.dw/tile_bg_2D.bin"
	.incbin	"data/chr00.dw/tile_bg_2E.bin"
	.incbin	"data/chr00.dw/tile_bg_2F.bin"
	.incbin	"data/chr00.dw/tile_bg_30.bin"
	.incbin	"data/chr00.dw/tile_bg_31.bin"
	.incbin	"data/chr00.dw/tile_bg_32.bin"
	.incbin	"data/chr00.dw/tile_bg_33.bin"
	.incbin	"data/chr00.dw/tile_bg_34.bin"
	.incbin	"data/chr00.dw/tile_bg_35.bin"
	.incbin	"data/chr00.dw/tile_bg_36.bin"
	.incbin	"data/chr00.dw/tile_bg_37.bin"
	.incbin	"data/chr00.dw/tile_bg_38.bin"
	.incbin	"data/chr00.dw/tile_bg_39.bin"
	.incbin	"data/chr00.dw/tile_bg_3A.bin"
	.incbin	"data/chr00.dw/tile_bg_3B.bin"
	.incbin	"data/chr00.dw/tile_bg_3C.bin"
	.incbin	"data/chr00.dw/tile_bg_3D.bin"
	.incbin	"data/chr00.dw/tile_bg_3E.bin"
	.incbin	"data/chr00.dw/tile_bg_3F.bin"
	.incbin	"data/chr00.dw/tile_bg_40.bin"
	.incbin	"data/chr00.dw/tile_bg_41.bin"
	.incbin	"data/chr00.dw/tile_bg_42.bin"
	.incbin	"data/chr00.dw/tile_bg_43.bin"
	.incbin	"data/chr00.dw/tile_bg_44.bin"
	.incbin	"data/chr00.dw/tile_bg_45.bin"
	.incbin	"data/chr00.dw/tile_bg_46.bin"
	.incbin	"data/chr00.dw/tile_bg_47.bin"
	.incbin	"data/chr00.dw/tile_bg_48.bin"
	.incbin	"data/chr00.dw/tile_bg_49.bin"
	.incbin	"data/chr00.dw/tile_bg_4A.bin"
	.incbin	"data/chr00.dw/tile_bg_4B.bin"
	.incbin	"data/chr00.dw/tile_bg_4C.bin"
	.incbin	"data/chr00.dw/tile_bg_4D.bin"
	.incbin	"data/chr00.dw/tile_bg_4E.bin"
	.incbin	"data/chr00.dw/tile_bg_4F.bin"
	.incbin	"data/chr00.dw/tile_bg_50.bin"
	.incbin	"data/chr00.dw/tile_bg_51.bin"
	.incbin	"data/chr00.dw/tile_bg_52.bin"
	.incbin	"data/chr00.dw/tile_bg_53.bin"
	.incbin	"data/chr00.dw/tile_bg_54.bin"
	.incbin	"data/chr00.dw/tile_bg_55.bin"
	.incbin	"data/chr00.dw/tile_bg_56.bin"
	.incbin	"data/chr00.dw/tile_bg_57.bin"
	.incbin	"data/chr00.dw/tile_bg_58.bin"
	.incbin	"data/chr00.dw/tile_bg_59.bin"
	.incbin	"data/chr00.dw/tile_bg_5A.bin"
	.incbin	"data/chr00.dw/tile_bg_5B.bin"
	.incbin	"data/chr00.dw/tile_bg_5C.bin"
	.incbin	"data/chr00.dw/tile_bg_5D.bin"
	.incbin	"data/chr00.dw/tile_bg_5E.bin"
	.incbin	"data/chr00.dw/tile_bg_5F.bin"
	.incbin	"data/chr00.dw/tile_bg_60.bin"
	.incbin	"data/chr00.dw/tile_bg_61.bin"
	.incbin	"data/chr00.dw/tile_bg_62.bin"
	.incbin	"data/chr00.dw/tile_bg_63.bin"
	.incbin	"data/chr00.dw/tile_bg_64.bin"
	.incbin	"data/chr00.dw/tile_bg_65.bin"
	.incbin	"data/chr00.dw/tile_bg_66.bin"
	.incbin	"data/chr00.dw/tile_bg_67.bin"
	.incbin	"data/chr00.dw/tile_bg_68.bin"
	.incbin	"data/chr00.dw/tile_bg_69.bin"
	.incbin	"data/chr00.dw/tile_bg_6A.bin"
	.incbin	"data/chr00.dw/tile_bg_6B.bin"
	.incbin	"data/chr00.dw/tile_bg_6C.bin"
	.incbin	"data/chr00.dw/tile_bg_6D.bin"
	.incbin	"data/chr00.dw/tile_bg_6E.bin"
	.incbin	"data/chr00.dw/tile_bg_6F.bin"
	.incbin	"data/chr00.dw/tile_bg_70.bin"
	.incbin	"data/chr00.dw/tile_bg_71.bin"
	.incbin	"data/chr00.dw/tile_bg_72.bin"
	.incbin	"data/chr00.dw/tile_bg_73.bin"
	.incbin	"data/chr00.dw/tile_bg_74.bin"
	.incbin	"data/chr00.dw/tile_bg_75.bin"
	.incbin	"data/chr00.dw/tile_bg_76.bin"
	.incbin	"data/chr00.dw/tile_bg_77.bin"
	.incbin	"data/chr00.dw/tile_bg_78.bin"
	.incbin	"data/chr00.dw/tile_bg_79.bin"
	.incbin	"data/chr00.dw/tile_bg_7A.bin"
	.incbin	"data/chr00.dw/tile_bg_7B.bin"
	.incbin	"data/chr00.dw/tile_bg_7C.bin"
	.incbin	"data/chr00.dw/tile_bg_7D.bin"
	.incbin	"data/chr00.dw/tile_bg_7E.bin"
	.incbin	"data/chr00.dw/tile_bg_7F.bin"
	.incbin	"data/chr00.dw/tile_bg_80.bin"
	.incbin	"data/chr00.dw/tile_bg_81.bin"
	.incbin	"data/chr00.dw/tile_bg_82.bin"
	.incbin	"data/chr00.dw/tile_bg_83.bin"
	.incbin	"data/chr00.dw/tile_bg_84.bin"
	.incbin	"data/chr00.dw/tile_bg_85.bin"
	.incbin	"data/chr00.dw/tile_bg_86.bin"
	.incbin	"data/chr00.dw/tile_bg_87.bin"
	.incbin	"data/chr00.dw/tile_bg_88.bin"
	.incbin	"data/chr00.dw/tile_bg_89.bin"
	.incbin	"data/chr00.dw/tile_bg_8A.bin"
	.incbin	"data/chr00.dw/tile_bg_8B.bin"
	.incbin	"data/chr00.dw/tile_bg_8C.bin"
	.incbin	"data/chr00.dw/tile_bg_8D.bin"
	.incbin	"data/chr00.dw/tile_bg_8E.bin"
	.incbin	"data/chr00.dw/tile_bg_8F.bin"
	.incbin	"data/chr00.dw/tile_bg_90.bin"
	.incbin	"data/chr00.dw/tile_bg_91.bin"
	.incbin	"data/chr00.dw/tile_bg_92.bin"
	.incbin	"data/chr00.dw/tile_bg_93.bin"
	.incbin	"data/chr00.dw/tile_bg_94.bin"
	.incbin	"data/chr00.dw/tile_bg_95.bin"
	.incbin	"data/chr00.dw/tile_bg_96.bin"
	.incbin	"data/chr00.dw/tile_bg_97.bin"
	.incbin	"data/chr00.dw/tile_bg_98.bin"
	.incbin	"data/chr00.dw/tile_bg_99.bin"
	.incbin	"data/chr00.dw/tile_bg_9A.bin"
	.incbin	"data/chr00.dw/tile_bg_9B.bin"
	.incbin	"data/chr00.dw/tile_bg_9C.bin"
	.incbin	"data/chr00.dw/tile_bg_9D.bin"
	.incbin	"data/chr00.dw/tile_bg_9E.bin"
	.incbin	"data/chr00.dw/tile_bg_9F.bin"
	.incbin	"data/chr00.dw/tile_bg_A0.bin"
	.incbin	"data/chr00.dw/tile_bg_A1.bin"
	.incbin	"data/chr00.dw/tile_bg_A2.bin"
	.incbin	"data/chr00.dw/tile_bg_A3.bin"
	.incbin	"data/chr00.dw/tile_bg_A4.bin"
	.incbin	"data/chr00.dw/tile_bg_A5.bin"
	.incbin	"data/chr00.dw/tile_bg_A6.bin"
	.incbin	"data/chr00.dw/tile_bg_A7.bin"
	.incbin	"data/chr00.dw/tile_bg_A8.bin"
	.incbin	"data/chr00.dw/tile_bg_A9.bin"
	.incbin	"data/chr00.dw/tile_bg_AA.bin"
	.incbin	"data/chr00.dw/tile_bg_AB.bin"
	.incbin	"data/chr00.dw/tile_bg_AC.bin"
	.incbin	"data/chr00.dw/tile_bg_AD.bin"
	.incbin	"data/chr00.dw/tile_bg_AE.bin"
	.incbin	"data/chr00.dw/tile_bg_AF.bin"
	.incbin	"data/chr00.dw/tile_bg_B0.bin"
	.incbin	"data/chr00.dw/tile_bg_B1.bin"
	.incbin	"data/chr00.dw/tile_bg_B2.bin"
	.incbin	"data/chr00.dw/tile_bg_B3.bin"
	.incbin	"data/chr00.dw/tile_bg_B4.bin"
	.incbin	"data/chr00.dw/tile_bg_B5.bin"
	.incbin	"data/chr00.dw/tile_bg_B6.bin"
	.incbin	"data/chr00.dw/tile_bg_B7.bin"
	.incbin	"data/chr00.dw/tile_bg_B8.bin"
	.incbin	"data/chr00.dw/tile_bg_B9.bin"
	.incbin	"data/chr00.dw/tile_bg_BA.bin"
	.incbin	"data/chr00.dw/tile_bg_BB.bin"
	.incbin	"data/chr00.dw/tile_bg_BC.bin"
	.incbin	"data/chr00.dw/tile_bg_BD.bin"
	.incbin	"data/chr00.dw/tile_bg_BE.bin"
	.incbin	"data/chr00.dw/tile_bg_BF.bin"
	.incbin	"data/chr00.dw/tile_bg_C0.bin"
	.incbin	"data/chr00.dw/tile_bg_C1.bin"
	.incbin	"data/chr00.dw/tile_bg_C2.bin"
	.incbin	"data/chr00.dw/tile_bg_C3.bin"
	.incbin	"data/chr00.dw/tile_bg_C4.bin"
	.incbin	"data/chr00.dw/tile_bg_C5.bin"
	.incbin	"data/chr00.dw/tile_bg_C6.bin"
	.incbin	"data/chr00.dw/tile_bg_C7.bin"
	.incbin	"data/chr00.dw/tile_bg_C8.bin"
	.incbin	"data/chr00.dw/tile_bg_C9.bin"
	.incbin	"data/chr00.dw/tile_bg_CA.bin"
	.incbin	"data/chr00.dw/tile_bg_CB.bin"
	.incbin	"data/chr00.dw/tile_bg_CC.bin"
	.incbin	"data/chr00.dw/tile_bg_CD.bin"
	.incbin	"data/chr00.dw/tile_bg_CE.bin"
	.incbin	"data/chr00.dw/tile_bg_CF.bin"
	.incbin	"data/chr00.dw/tile_bg_D0.bin"
	.incbin	"data/chr00.dw/tile_bg_D1.bin"
	.incbin	"data/chr00.dw/tile_bg_D2.bin"
	.incbin	"data/chr00.dw/tile_bg_D3.bin"
	.incbin	"data/chr00.dw/tile_bg_D4.bin"
	.incbin	"data/chr00.dw/tile_bg_D5.bin"
	.incbin	"data/chr00.dw/tile_bg_D6.bin"
	.incbin	"data/chr00.dw/tile_bg_D7.bin"
	.incbin	"data/chr00.dw/tile_bg_D8.bin"
	.incbin	"data/chr00.dw/tile_bg_D9.bin"
	.incbin	"data/chr00.dw/tile_bg_DA.bin"
	.incbin	"data/chr00.dw/tile_bg_DB.bin"
	.incbin	"data/chr00.dw/tile_bg_DC.bin"
	.incbin	"data/chr00.dw/tile_bg_DD.bin"
	.incbin	"data/chr00.dw/tile_bg_DE.bin"
	.incbin	"data/chr00.dw/tile_bg_DF.bin"
	.incbin	"data/chr00.dw/tile_bg_E0.bin"
	.incbin	"data/chr00.dw/tile_bg_E1.bin"
	.incbin	"data/chr00.dw/tile_bg_E2.bin"
	.incbin	"data/chr00.dw/tile_bg_E3.bin"
	.incbin	"data/chr00.dw/tile_bg_E4.bin"
	.incbin	"data/chr00.dw/tile_bg_E5.bin"
	.incbin	"data/chr00.dw/tile_bg_E6.bin"
	.incbin	"data/chr00.dw/tile_bg_E7.bin"
	.incbin	"data/chr00.dw/tile_bg_E8.bin"
	.incbin	"data/chr00.dw/tile_bg_E9.bin"
	.incbin	"data/chr00.dw/tile_bg_EA.bin"
	.incbin	"data/chr00.dw/tile_bg_EB.bin"
	.incbin	"data/chr00.dw/tile_bg_EC.bin"
	.incbin	"data/chr00.dw/tile_bg_ED.bin"
	.incbin	"data/chr00.dw/tile_bg_EE.bin"
	.incbin	"data/chr00.dw/tile_bg_EF.bin"
	.incbin	"data/chr00.dw/tile_bg_F0.bin"
	.incbin	"data/chr00.dw/tile_bg_F1.bin"
	.incbin	"data/chr00.dw/tile_bg_F2.bin"
	.incbin	"data/chr00.dw/tile_bg_F3.bin"
	.incbin	"data/chr00.dw/tile_bg_F4.bin"
	.incbin	"data/chr00.dw/tile_bg_F5.bin"
	.incbin	"data/chr00.dw/tile_bg_F6.bin"
	.incbin	"data/chr00.dw/tile_bg_F7.bin"
	.incbin	"data/chr00.dw/tile_bg_F8.bin"
	.incbin	"data/chr00.dw/tile_bg_F9.bin"
	.incbin	"data/chr00.dw/tile_bg_FA.bin"
	.incbin	"data/chr00.dw/tile_bg_FB.bin"
	.incbin	"data/chr00.dw/tile_bg_FC.bin"
	.incbin	"data/chr00.dw/tile_bg_FD.bin"
	.incbin	"data/chr00.dw/tile_bg_FE.bin"
	.incbin	"data/chr00.dw/tile_bg_FF.bin"

	.incbin	"data/chr00.dw/tile_obj_00.bin"
	.incbin	"data/chr00.dw/tile_obj_01.bin"
	.incbin	"data/chr00.dw/tile_obj_02.bin"
	.incbin	"data/chr00.dw/tile_obj_03.bin"
	.incbin	"data/chr00.dw/tile_obj_04.bin"
	.incbin	"data/chr00.dw/tile_obj_05.bin"
	.incbin	"data/chr00.dw/tile_obj_06.bin"
	.incbin	"data/chr00.dw/tile_obj_07.bin"
	.incbin	"data/chr00.dw/tile_obj_08.bin"
	.incbin	"data/chr00.dw/tile_obj_09.bin"
	.incbin	"data/chr00.dw/tile_obj_0A.bin"
	.incbin	"data/chr00.dw/tile_obj_0B.bin"
	.incbin	"data/chr00.dw/tile_obj_0C.bin"
	.incbin	"data/chr00.dw/tile_obj_0D.bin"
	.incbin	"data/chr00.dw/tile_obj_0E.bin"
	.incbin	"data/chr00.dw/tile_obj_0F.bin"
	.incbin	"data/chr00.dw/tile_obj_10.bin"
	.incbin	"data/chr00.dw/tile_obj_11.bin"
	.incbin	"data/chr00.dw/tile_obj_12.bin"
	.incbin	"data/chr00.dw/tile_obj_13.bin"
	.incbin	"data/chr00.dw/tile_obj_14.bin"
	.incbin	"data/chr00.dw/tile_obj_15.bin"
	.incbin	"data/chr00.dw/tile_obj_16.bin"
	.incbin	"data/chr00.dw/tile_obj_17.bin"
	.incbin	"data/chr00.dw/tile_obj_18.bin"
	.incbin	"data/chr00.dw/tile_obj_19.bin"
	.incbin	"data/chr00.dw/tile_obj_1A.bin"
	.incbin	"data/chr00.dw/tile_obj_1B.bin"
	.incbin	"data/chr00.dw/tile_obj_1C.bin"
	.incbin	"data/chr00.dw/tile_obj_1D.bin"
	.incbin	"data/chr00.dw/tile_obj_1E.bin"
	.incbin	"data/chr00.dw/tile_obj_1F.bin"
	.incbin	"data/chr00.dw/tile_obj_20.bin"
	.incbin	"data/chr00.dw/tile_obj_21.bin"
	.incbin	"data/chr00.dw/tile_obj_22.bin"
	.incbin	"data/chr00.dw/tile_obj_23.bin"
	.incbin	"data/chr00.dw/tile_obj_24.bin"
	.incbin	"data/chr00.dw/tile_obj_25.bin"
	.incbin	"data/chr00.dw/tile_obj_26.bin"
	.incbin	"data/chr00.dw/tile_obj_27.bin"
	.incbin	"data/chr00.dw/tile_obj_28.bin"
	.incbin	"data/chr00.dw/tile_obj_29.bin"
	.incbin	"data/chr00.dw/tile_obj_2A.bin"
	.incbin	"data/chr00.dw/tile_obj_2B.bin"
	.incbin	"data/chr00.dw/tile_obj_2C.bin"
	.incbin	"data/chr00.dw/tile_obj_2D.bin"
	.incbin	"data/chr00.dw/tile_obj_2E.bin"
	.incbin	"data/chr00.dw/tile_obj_2F.bin"
	.incbin	"data/chr00.dw/tile_obj_30.bin"
	.incbin	"data/chr00.dw/tile_obj_31.bin"
	.incbin	"data/chr00.dw/tile_obj_32.bin"
	.incbin	"data/chr00.dw/tile_obj_33.bin"
	.incbin	"data/chr00.dw/tile_obj_34.bin"
	.incbin	"data/chr00.dw/tile_obj_35.bin"
	.incbin	"data/chr00.dw/tile_obj_36.bin"
	.incbin	"data/chr00.dw/tile_obj_37.bin"
	.incbin	"data/chr00.dw/tile_obj_38.bin"
	.incbin	"data/chr00.dw/tile_obj_39.bin"
	.incbin	"data/chr00.dw/tile_obj_3A.bin"
	.incbin	"data/chr00.dw/tile_obj_3B.bin"
	.incbin	"data/chr00.dw/tile_obj_3C.bin"
	.incbin	"data/chr00.dw/tile_obj_3D.bin"
	.incbin	"data/chr00.dw/tile_obj_3E.bin"
	.incbin	"data/chr00.dw/tile_obj_3F.bin"
	.incbin	"data/chr00.dw/tile_obj_40.bin"
	.incbin	"data/chr00.dw/tile_obj_41.bin"
	.incbin	"data/chr00.dw/tile_obj_42.bin"
	.incbin	"data/chr00.dw/tile_obj_43.bin"
	.incbin	"data/chr00.dw/tile_obj_44.bin"
	.incbin	"data/chr00.dw/tile_obj_45.bin"
	.incbin	"data/chr00.dw/tile_obj_46.bin"
	.incbin	"data/chr00.dw/tile_obj_47.bin"
	.incbin	"data/chr00.dw/tile_obj_48.bin"
	.incbin	"data/chr00.dw/tile_obj_49.bin"
	.incbin	"data/chr00.dw/tile_obj_4A.bin"
	.incbin	"data/chr00.dw/tile_obj_4B.bin"
	.incbin	"data/chr00.dw/tile_obj_4C.bin"
	.incbin	"data/chr00.dw/tile_obj_4D.bin"
	.incbin	"data/chr00.dw/tile_obj_4E.bin"
	.incbin	"data/chr00.dw/tile_obj_4F.bin"
	.incbin	"data/chr00.dw/tile_obj_50.bin"
	.incbin	"data/chr00.dw/tile_obj_51.bin"
	.incbin	"data/chr00.dw/tile_obj_52.bin"
	.incbin	"data/chr00.dw/tile_obj_53.bin"
	.incbin	"data/chr00.dw/tile_obj_54.bin"
	.incbin	"data/chr00.dw/tile_obj_55.bin"
	.incbin	"data/chr00.dw/tile_obj_56.bin"
	.incbin	"data/chr00.dw/tile_obj_57.bin"
	.incbin	"data/chr00.dw/tile_obj_58.bin"
	.incbin	"data/chr00.dw/tile_obj_59.bin"
	.incbin	"data/chr00.dw/tile_obj_5A.bin"
	.incbin	"data/chr00.dw/tile_obj_5B.bin"
	.incbin	"data/chr00.dw/tile_obj_5C.bin"
	.incbin	"data/chr00.dw/tile_obj_5D.bin"
	.incbin	"data/chr00.dw/tile_obj_5E.bin"
	.incbin	"data/chr00.dw/tile_obj_5F.bin"
	.incbin	"data/chr00.dw/tile_obj_60.bin"
	.incbin	"data/chr00.dw/tile_obj_61.bin"
	.incbin	"data/chr00.dw/tile_obj_62.bin"
	.incbin	"data/chr00.dw/tile_obj_63.bin"
	.incbin	"data/chr00.dw/tile_obj_64.bin"
	.incbin	"data/chr00.dw/tile_obj_65.bin"
	.incbin	"data/chr00.dw/tile_obj_66.bin"
	.incbin	"data/chr00.dw/tile_obj_67.bin"
	.incbin	"data/chr00.dw/tile_obj_68.bin"
	.incbin	"data/chr00.dw/tile_obj_69.bin"
	.incbin	"data/chr00.dw/tile_obj_6A.bin"
	.incbin	"data/chr00.dw/tile_obj_6B.bin"
	.incbin	"data/chr00.dw/tile_obj_6C.bin"
	.incbin	"data/chr00.dw/tile_obj_6D.bin"
	.incbin	"data/chr00.dw/tile_obj_6E.bin"
	.incbin	"data/chr00.dw/tile_obj_6F.bin"
	.incbin	"data/chr00.dw/tile_obj_70.bin"
	.incbin	"data/chr00.dw/tile_obj_71.bin"
	.incbin	"data/chr00.dw/tile_obj_72.bin"
	.incbin	"data/chr00.dw/tile_obj_73.bin"
	.incbin	"data/chr00.dw/tile_obj_74.bin"
	.incbin	"data/chr00.dw/tile_obj_75.bin"
	.incbin	"data/chr00.dw/tile_obj_76.bin"
	.incbin	"data/chr00.dw/tile_obj_77.bin"
	.incbin	"data/chr00.dw/tile_obj_78.bin"
	.incbin	"data/chr00.dw/tile_obj_79.bin"
	.incbin	"data/chr00.dw/tile_obj_7A.bin"
	.incbin	"data/chr00.dw/tile_obj_7B.bin"
	.incbin	"data/chr00.dw/tile_obj_7C.bin"
	.incbin	"data/chr00.dw/tile_obj_7D.bin"
	.incbin	"data/chr00.dw/tile_obj_7E.bin"
	.incbin	"data/chr00.dw/tile_obj_7F.bin"
	.incbin	"data/chr00.dw/tile_obj_80.bin"
	.incbin	"data/chr00.dw/tile_obj_81.bin"
	.incbin	"data/chr00.dw/tile_obj_82.bin"
	.incbin	"data/chr00.dw/tile_obj_83.bin"
	.incbin	"data/chr00.dw/tile_obj_84.bin"
	.incbin	"data/chr00.dw/tile_obj_85.bin"
	.incbin	"data/chr00.dw/tile_obj_86.bin"
	.incbin	"data/chr00.dw/tile_obj_87.bin"
	.incbin	"data/chr00.dw/tile_obj_88.bin"
	.incbin	"data/chr00.dw/tile_obj_89.bin"
	.incbin	"data/chr00.dw/tile_obj_8A.bin"
	.incbin	"data/chr00.dw/tile_obj_8B.bin"
	.incbin	"data/chr00.dw/tile_obj_8C.bin"
	.incbin	"data/chr00.dw/tile_obj_8D.bin"
	.incbin	"data/chr00.dw/tile_obj_8E.bin"
	.incbin	"data/chr00.dw/tile_obj_8F.bin"
	.incbin	"data/chr00.dw/tile_obj_90.bin"
	.incbin	"data/chr00.dw/tile_obj_91.bin"
	.incbin	"data/chr00.dw/tile_obj_92.bin"
	.incbin	"data/chr00.dw/tile_obj_93.bin"
	.incbin	"data/chr00.dw/tile_obj_94.bin"
	.incbin	"data/chr00.dw/tile_obj_95.bin"
	.incbin	"data/chr00.dw/tile_obj_96.bin"
	.incbin	"data/chr00.dw/tile_obj_97.bin"
	.incbin	"data/chr00.dw/tile_obj_98.bin"
	.incbin	"data/chr00.dw/tile_obj_99.bin"
	.incbin	"data/chr00.dw/tile_obj_9A.bin"
	.incbin	"data/chr00.dw/tile_obj_9B.bin"
	.incbin	"data/chr00.dw/tile_obj_9C.bin"
	.incbin	"data/chr00.dw/tile_obj_9D.bin"
	.incbin	"data/chr00.dw/tile_obj_9E.bin"
	.incbin	"data/chr00.dw/tile_obj_9F.bin"
	.incbin	"data/chr00.dw/tile_obj_A0.bin"
	.incbin	"data/chr00.dw/tile_obj_A1.bin"
	.incbin	"data/chr00.dw/tile_obj_A2.bin"
	.incbin	"data/chr00.dw/tile_obj_A3.bin"
	.incbin	"data/chr00.dw/tile_obj_A4.bin"
	.incbin	"data/chr00.dw/tile_obj_A5.bin"
	.incbin	"data/chr00.dw/tile_obj_A6.bin"
	.incbin	"data/chr00.dw/tile_obj_A7.bin"
	.incbin	"data/chr00.dw/tile_obj_A8.bin"
	.incbin	"data/chr00.dw/tile_obj_A9.bin"
	.incbin	"data/chr00.dw/tile_obj_AA.bin"
	.incbin	"data/chr00.dw/tile_obj_AB.bin"
	.incbin	"data/chr00.dw/tile_obj_AC.bin"
	.incbin	"data/chr00.dw/tile_obj_AD.bin"
	.incbin	"data/chr00.dw/tile_obj_AE.bin"
	.incbin	"data/chr00.dw/tile_obj_AF.bin"
	.incbin	"data/chr00.dw/tile_obj_B0.bin"
	.incbin	"data/chr00.dw/tile_obj_B1.bin"
	.incbin	"data/chr00.dw/tile_obj_B2.bin"
	.incbin	"data/chr00.dw/tile_obj_B3.bin"
	.incbin	"data/chr00.dw/tile_obj_B4.bin"
	.incbin	"data/chr00.dw/tile_obj_B5.bin"
	.incbin	"data/chr00.dw/tile_obj_B6.bin"
	.incbin	"data/chr00.dw/tile_obj_B7.bin"
	.incbin	"data/chr00.dw/tile_obj_B8.bin"
	.incbin	"data/chr00.dw/tile_obj_B9.bin"
	.incbin	"data/chr00.dw/tile_obj_BA.bin"
	.incbin	"data/chr00.dw/tile_obj_BB.bin"
	.incbin	"data/chr00.dw/tile_obj_BC.bin"
	.incbin	"data/chr00.dw/tile_obj_BD.bin"
	.incbin	"data/chr00.dw/tile_obj_BE.bin"
	.incbin	"data/chr00.dw/tile_obj_BF.bin"
	.incbin	"data/chr00.dw/tile_obj_C0.bin"
	.incbin	"data/chr00.dw/tile_obj_C1.bin"
	.incbin	"data/chr00.dw/tile_obj_C2.bin"
	.incbin	"data/chr00.dw/tile_obj_C3.bin"
	.incbin	"data/chr00.dw/tile_obj_C4.bin"
	.incbin	"data/chr00.dw/tile_obj_C5.bin"
	.incbin	"data/chr00.dw/tile_obj_C6.bin"
	.incbin	"data/chr00.dw/tile_obj_C7.bin"
	.incbin	"data/chr00.dw/tile_obj_C8.bin"
	.incbin	"data/chr00.dw/tile_obj_C9.bin"
	.incbin	"data/chr00.dw/tile_obj_CA.bin"
	.incbin	"data/chr00.dw/tile_obj_CB.bin"
	.incbin	"data/chr00.dw/tile_obj_CC.bin"
	.incbin	"data/chr00.dw/tile_obj_CD.bin"
	.incbin	"data/chr00.dw/tile_obj_CE.bin"
	.incbin	"data/chr00.dw/tile_obj_CF.bin"
	.incbin	"data/chr00.dw/tile_obj_D0.bin"
	.incbin	"data/chr00.dw/tile_obj_D1.bin"
	.incbin	"data/chr00.dw/tile_obj_D2.bin"
	.incbin	"data/chr00.dw/tile_obj_D3.bin"
	.incbin	"data/chr00.dw/tile_obj_D4.bin"
	.incbin	"data/chr00.dw/tile_obj_D5.bin"
	.incbin	"data/chr00.dw/tile_obj_D6.bin"
	.incbin	"data/chr00.dw/tile_obj_D7.bin"
	.incbin	"data/chr00.dw/tile_obj_D8.bin"
	.incbin	"data/chr00.dw/tile_obj_D9.bin"
	.incbin	"data/chr00.dw/tile_obj_DA.bin"
	.incbin	"data/chr00.dw/tile_obj_DB.bin"
	.incbin	"data/chr00.dw/tile_obj_DC.bin"
	.incbin	"data/chr00.dw/tile_obj_DD.bin"
	.incbin	"data/chr00.dw/tile_obj_DE.bin"
	.incbin	"data/chr00.dw/tile_obj_DF.bin"
	.incbin	"data/chr00.dw/tile_obj_E0.bin"
	.incbin	"data/chr00.dw/tile_obj_E1.bin"
	.incbin	"data/chr00.dw/tile_obj_E2.bin"
	.incbin	"data/chr00.dw/tile_obj_E3.bin"
	.incbin	"data/chr00.dw/tile_obj_E4.bin"
	.incbin	"data/chr00.dw/tile_obj_E5.bin"
	.incbin	"data/chr00.dw/tile_obj_E6.bin"
	.incbin	"data/chr00.dw/tile_obj_E7.bin"
	.incbin	"data/chr00.dw/tile_obj_E8.bin"
	.incbin	"data/chr00.dw/tile_obj_E9.bin"
	.incbin	"data/chr00.dw/tile_obj_EA.bin"
	.incbin	"data/chr00.dw/tile_obj_EB.bin"
	.incbin	"data/chr00.dw/tile_obj_EC.bin"
	.incbin	"data/chr00.dw/tile_obj_ED.bin"
	.incbin	"data/chr00.dw/tile_obj_EE.bin"
	.incbin	"data/chr00.dw/tile_obj_EF.bin"
	.incbin	"data/chr00.dw/tile_obj_F0.bin"
	.incbin	"data/chr00.dw/tile_obj_F1.bin"
	.incbin	"data/chr00.dw/tile_obj_F2.bin"
	.incbin	"data/chr00.dw/tile_obj_F3.bin"
	.incbin	"data/chr00.dw/tile_obj_F4.bin"
	.incbin	"data/chr00.dw/tile_obj_F5.bin"
	.incbin	"data/chr00.dw/tile_obj_F6.bin"
	.incbin	"data/chr00.dw/tile_obj_F7.bin"
	.incbin	"data/chr00.dw/tile_obj_F8.bin"
	.incbin	"data/chr00.dw/tile_obj_F9.bin"
	.incbin	"data/chr00.dw/tile_obj_FA.bin"
	.incbin	"data/chr00.dw/tile_obj_FB.bin"
	.incbin	"data/chr00.dw/tile_obj_FC.bin"
	.incbin	"data/chr00.dw/tile_obj_FD.bin"
	.incbin	"data/chr00.dw/tile_obj_FE.bin"
	.incbin	"data/chr00.dw/tile_obj_FF.bin"
