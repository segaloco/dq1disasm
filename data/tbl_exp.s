.segment	"RODATA"

	.export tbl_level_exp
tbl_level_exp:
	.word	0
	.word	7
	.word	23
	.word	47
	.word	110
	.word	220
	.word	450
	.word	800
	.word	1300
	.word	2000
	.word	2900
	.word	4000
	.word	5500
	.word	7500
	.word	10000
	.word	13000
	.word	17000
	.word	21000
	.word	25000
	.word	29000
	.word	33000
	.word	37000
	.word	41000
	.word	45000
	.word	49000
	.word	53000
	.word	57000
	.word	61000
	.word	65000
	.word	65535
tbl_level_exp_end:

	.end
