; ------------------------------------------------------------
	.export	LC000, LC006
LC000:
	jsr	LC5E0
	jmp	LC009
; --------------
LC006:
	jsr	LC5E4
; --------------
LC009:
	tya
	pha

	lda	$3E
	and	#$02
	asl	a
	sta	$3D
	lda	$3C
	and	#$02
	clc
	adc	$3D
	tay
	lda	#$FC
	cpy	#$00
	beq	:++
	: ; for (y; y > 0; y--) {
		sec
		rol	a
		asl	$08

		dey
		bne	:-
	: ; }
	and	($0A), y
	ora	$08
	sta	($0A), y
	sta	$08

	pla
	tay
	rts
; ------------------------------------------------------------
LC032: ; switch (a) {
	cmp	#0
	bne	:+ ; case 0:
		lda	#2
		rts
	: cmp	#1
	bne	:+ ; case 1:
		lda	#3
		rts
	: cmp	#2
	bne	:+ ; case 2:
		lda	#0
		rts
	: ; default:
		lda	#1
		rts
; }
; ------------------------------------------------------------
LC04A:
	sta	$26

	tya
	pha
	txa
	pha

	ldx	$26
	lda	$602F
	jsr	LC032
	sta	$25
	lsr	a
	ror	a
	ror	a
	ror	a
	sta	$24
	lda	$52, x
	and	#$9F
	ora	$24
	sta	$52, x
	lda	#$00
	sta	$27
	ldy	$0200
	ldx	$0203
LC072:	lda	$602F
	bne	LC07F
	tya
	sec
	sbc	#$10
	tay
	jmp	LC09C

LC07F:	cmp	#$01
	bne	LC08B
	txa
	clc
	adc	#$10
	tax
	jmp	LC09C

LC08B:	cmp	#$02
	bne	LC097
	tya
	clc
	adc	#$10
	tay
	jmp	LC09C

LC097:	txa
	sec
	sbc	#$10
	tax
LC09C:
	stx	$22
	sty	$23
	ldy	#$10
LC0A2:	lda	$0200, y
	cmp	$23
	bne	LC0B0
	lda	$0203, y
	cmp	$22
	beq	LC0C6
LC0B0:	tya
	clc
	adc	#$10
	tay
	bne	LC0A2
	ldx	$22
	ldy	$23
	lda	$27
	bne	LC0EF
	lda	#$01
	sta	$27
	jmp	LC072

LC0C6:
	sty	$28
	lda	#$04
	sta	$27
	ldx	$26
	jsr	LC0F4
	tay
	lda	$25
	jsr	LB6C2
	ldx	$28
	: ; for () {
		lda	($22), y
		sta	$0201, x
		iny
		lda	($22), y
		dey
		sta	$0202, x

		inx
		inx
		inx
		inx
		iny
		iny
		dec	$27
		bne	:-
	; }

LC0EF:
	pla
	tax
	pla
	tay
	rts
; ------------------------------------------------------------
	.export	LC0F4
LC0F4:
	lda	$51, x
	and	#$E0
	lsr	a
	sta	$24
	cmp	#$60
	bne	LC117
	lda	$45
	cmp	#$04
	bne	LC10B
	lda	$E4
	and	#$04
	bne	LC111
LC10B:	lda	$45
	cmp	#$05
	bne	LC14B
LC111:	lda	#$D0
	sta	$24
	bne	LC14B
LC117:	lda	$24
	cmp	#$50
	bne	LC147
	lda	$45
	cmp	#$04
	bne	LC13B
	lda	$E4
	and	#$04
	beq	LC13B
	lda	#$F0
	sta	$24
LC12D:	lda	$C7
	cmp	#$FF
	bne	LC153
	lda	$24
	ora	#$08
	sta	$24
	bne	LC153
LC13B:	lda	$45
	cmp	#$06
	bne	LC14B
	lda	#$E0
	sta	$24
	bne	LC14B
LC147:	cmp	#$70
	beq	LC12D
LC14B:	lda	$50
	and	#$08
	ora	$24
	sta	$24
LC153:	lda	$24
	rts
; ------------------------------------------------------------
LC156:
	ldx	#$3A
	lda	#$1E
	sta	$C7
	: ; for () {
		lda	$BA
		sec
		sbc	LF35B, x
		lda	$BB
		sbc	LF35C, x
		bcs	:+ ; if () {
			dec	$C7
			dex
			dex
			bne	:-
		; } else break;
	: ; }

	rts
; ------------------------------------------------------------
LC170:
	sta	$24
	: ; for (byte_24 = a; byte_24 >= 0; byte_24--) {
		jsr	LFF74

		dec	$24
		bpl	:-
	; }

	rts
; ------------------------------------------------------------
	.export	LC17A
LC17A:
	pha
	txa
	pha
	tya
	pha

	jsr	LFF74
	lda	#$08
	sta	$2000
	lda	#$5F
	sta	$08
	lda	$2002
	lda	#$20
	sta	$2006
	lda	#$00
	sta	$2006
	jsr	LC1B9
	lda	$2002
	lda	#$24
	sta	$2006
	lda	#$00
	sta	$2006
	jsr	LC1B9
	lda	#$88
	sta	$2000
	jsr	LFF74

	pla
	tay
	pla
	tax
	pla
	rts
; -----------------------------
LC1B9:
	lda	$08
	ldx	#30
	: ; for (x = 30; x > 0; x--) {
		ldy	#32
		: ; for (y = 32; y > 0; y--) {
			sta	$2007

			dey
			bne	:-
		; }

		dex
		bne	:--
	; }

	rts
; ------------------------------------------------------------
	.export	LC1C9
LC1C9:
	lda	#$00
	sta	$40
	sta	$41
	: ; while (word_3C != 0) {
		lda	$3C
		ora	$3D
		beq	:++ ; if () {
			lsr	$3D
			ror	$3C
			bcc	:+ ; if () {
				lda	$3E
				clc
				adc	$40
				sta	$40
				lda	$3F
				adc	$41
				sta	$41
			: ; }

			asl	$3E
			rol	$3F
			jmp	:--
		; } else break;
	: ; }

	rts
; ------------------------------------------------------------
	.export	LC1F0
LC1F0:
	lda	#$00
	sta	$3D
; --------------
LC1F4:
	ldy	#16
	lda	#$00
	: ; for (y = 16; y > 0; y--) {
		asl	$3C
		rol	$3D
		sta	$40
		adc	$40
		inc	$3C
		sec
		sbc	$3E
		bcs	:+ ; if () {
			clc
			adc	$3E
			dec	$3C
		: ; }

		dey
		bne	:--
	; }

	sta	$40
	rts
; ------------------------------------------------------------
	.export	LC212
LC212:
	lda	#0
	sta	$3C
	: ; for (byte_3C = 0; byte_3C < 80; byte_3C += 16) {
		ldx	#4
		: ; for (x = 4; x > 0; x--) {
			jsr	LFF74

			dex
			bne	:-
		; }

		lda	$3E
		sta	$0C
		lda	$3F
		sta	$0D
		jsr	LC632
		lda	$3D
		beq	:+ ; if () {
			lda	$40
			sta	$0C
			lda	$41
			sta	$0D
			jsr	LC63D
		: ; }

		lda	$3C
		clc
		adc	#16
		sta	$3C
		cmp	#80
		bne	:---
	; }

	rts
; ------------------------------------------------------------
	.export	LC244
LC244:
	lda	$4A
	asl	a
	clc
	adc	$0F
	and	#$3F
	pha

	lda	$4B
	asl	a
	clc
	adc	$10
	clc
	adc	#$1E
	sta	$3C
	lda	#$1E
	sta	$3E
	jsr	LC1F0
	lda	$40
	sta	$3E

	pla
	sta	$3C
	jsr	LC270
	rts
; ------------------------------------------------------------
LC26A:
	jsr	LC5E0
	jmp	LC273
; --------------
LC270:
	jsr	LC5E4
; --------------
LC273:
	tya
	pha

	ldy	#$00
	lda	($0A), y
	sta	$08

	pla
	tay
	lda	$0B
	clc
	adc	#$20
	sta	$0B
	jsr	LC690
	rts
; ------------------------------------------------------------
LC288:
	jsr	LFF74
; --------------
LC28B:
	: ; while () {
		; while () {
			jsr	LC297
			inc	$99
			bne	:-
		; }

		inc	$9A
		jmp	:-
	; }
; -----------------------------
LC297:
	: ; while () {
		lda	$D6
		beq	:+ ; if () {
			dec	$D6
			lda	$99
			dec	$99
			tay
			bne	:+ ; if () {
				dec	$9A
			; }
		: ; }

		ldy	#$00
		lda	($99), y
		cmp	#$F7
		bne	:++ ; if () {
			iny
			lda	($99), y
			sta	$D6
			lda	$99
			clc
			adc	#$03
			sta	$99
			bcc	:+ ; if () {
				inc	$9A
			: ; }

			jmp	:---
		; } else break;
	: ; }

	cmp	#$FF
	beq	LC2F6
	cmp	#$FC
	bne	LC2F9

LC2C9:
	lda	$0C
	clc
	adc	#$40
	sta	$0C
	sta	$42
	lda	$0D
	adc	#$00
	sta	$0D
	sta	$43
	lda	$9D
	sta	$97
	inc	$98
	inc	$98
	lda	$98
	cmp	#$1E
	bne	LC2EE
	lda	#$00
	sta	$98
	beq	LC2F6
LC2EE:	cmp	#$1F
	bne	LC2F6
	lda	#$01
	sta	$98
LC2F6:
	pla
	pla
	rts

LC2F9:	cmp	#$FE
	bne	:+ ; if () {
		jsr	LC2C9
	: ; }

	cmp	#$FB
	bne	LC329
	lda	$E5
	asl	a
	asl	a
	asl	a
	sta	$3C
	asl	a
	adc	$3C
	adc	#$03
	tax
	: ; while () {
		lda	#$01
		sta	$02
		: ; for () {
			lda	$02
			jsr	LFF74
			bne	:-
		; }

		jsr	LC608

		lda	$47
		and	#$08
		bne	:--
		dex
		bne	:--
	; }

	rts
; --------------
LC329:	cmp	#$FD
	bne	LC340
	lda	$99
	sta	$9B
	lda	$9A
	sta	$9C
	lda	#$A3
	sta	$99
	lda	#$00
	sta	$9A
	jmp	LC474

LC340:	cmp	#$FA
	bne	LC34D
	lda	$9B
	sta	$99
	lda	$9C
	sta	$9A
	rts

LC34D:	cmp	#$F0
	bne	LC382
	iny
	lda	($99), y
	sta	$3E
	iny
	lda	($99), y
	sta	$3F
	tya
	pha
	ldy	#$00
	sty	$3D
	lda	($3E), y
	sta	$3C
	pla
	tay
	jsr	LC6C9
	lda	$99
	clc
	adc	#$02
	sta	$9B
	lda	$9A
	adc	#$00
	sta	$9C
	lda	#$00
	sta	$9A
	lda	#$B1
	sta	$99
	jmp	LC474

LC382:	cmp	#$F1
	bne	LC3BE
	jsr	LC38C
	jmp	LC474

LC38C:	iny
	lda	($99), y
	sta	$3E
	iny
	lda	($99), y
	sta	$3F
	tya
	pha
	ldy	#$00
	lda	($3E), y
	sta	$3C
	iny
	lda	($3E), y
	sta	$3D
	pla
	tay
	jsr	LC6C9
	lda	$99
	clc
	adc	#$02
	sta	$9B
	lda	$9A
	adc	#$00
	sta	$9C
	lda	#$00
	sta	$9A
	lda	#$AF
	sta	$99
	rts

LC3BE:	cmp	#$F3
	bne	LC3D5
	jsr	LC38C
	ldy	#$00
LC3C7:	lda	($99), y
	cmp	#$5F
	bne	LC3D2
	inc	$99
	jmp	LC3C7

LC3D2:	jmp	LC474

LC3D5:	cmp	#$F2
	bne	LC3EC
	lda	$99
	sta	$9B
	lda	$9A
	sta	$9C
	lda	#$00
	sta	$9A
	lda	#$B5
	sta	$99
	jmp	LC474

LC3EC:	cmp	#$6D
	bcc	LC42D
	sbc	#$6D
	tax
	inx
	lda	LF150
	sta	$3C
	lda	LF151
	sta	$3D
LC3FE:	ldy	#$00
LC400:	lda	($3C), y
	cmp	#$FA
	beq	LC40A
	iny
	jmp	LC400

LC40A:	dex
	beq	LC41A
	tya
	sec
	adc	$3C
	sta	$3C
	bcc	LC417
	inc	$3D
LC417:	jmp	LC3FE

LC41A:	lda	$99
	sta	$9B
	lda	$9A
	sta	$9C
	lda	$3C
	sta	$99
	lda	$3D
	sta	$9A
	jmp	LC474

LC42D:	cmp	#$57
	beq	LC434
	jmp	LC474

LC434:	lda	$D4
	clc
	adc	#$09
	and	#$3F
	sta	$97
	lda	#$00
	sta	$4F
LC441:	jsr	LC608
	lda	$47
	and	#$03
	beq	LC44E
LC44A:	lda	#$5F
	bne	LC456
LC44E:	lda	$4F
	and	#$10
	bne	LC44A
	lda	#$57
LC456:	sta	$08
	jsr	LFF74
	jsr	LC4F5
	jsr	LC690
	lda	$47
	and	#$03
	beq	LC441
	lda	#$85
	brk
	.byte	$04
	.byte	$17
	lda	$D4
	sta	$97
	jsr	LC7EC
	rts

LC474:	ldy	#$00
	lda	($99), y
	sta	$08

	lda	$09
	beq	:+
	cmp	#$01
	beq	:+ ; if () {
		lda	$08
		sta	($42), y
	: ; }

	jsr	LC4F5
	jsr	LC690
	ldy	#$01
	lda	($99), y
	cmp	#$F8
	beq	LC49E
	cmp	#$F9
	bne	LC4EA
	lda	#$52
	sta	$08
	bne	LC4A2
LC49E:	lda	#$51
	sta	$08
LC4A2:	inc	$99
	bne	LC4A8
	inc	$9A
LC4A8:	lda	$42
	clc
	adc	#$E0
	sta	$42
	bcs	LC4B3
	dec	$43
LC4B3:	lda	$09
	beq	LC4C1
	cmp	#$01
	beq	LC4C1
	lda	$08
	ldy	#$00
	sta	($42), y
LC4C1:	lda	$42
	clc
	adc	#$20
	sta	$42
	bcc	LC4CC
	inc	$43
LC4CC:	dec	$98
	lda	$98
	cmp	#$FF
	bne	LC4D8
	lda	#$1D
	sta	$98
LC4D8:	jsr	LC4F5
	jsr	LC690
	inc	$98
	lda	$98
	cmp	#$1E
	bne	LC4EA
	lda	#$00
	sta	$98
LC4EA:	inc	$42
	inc	$97
	lda	$97
	and	#$3F
	sta	$97
	rts
; -----------------------------
LC4F5:
	lda	$08
	pha

	lda	$09
	cmp	#$01
	beq	:+
	lda	$98
	lsr	a
	bcs	:+
	lda	$97
	lsr	a
	bcs	:+ ; if () {
		lda	$97
		sta	$3C
		lda	$98
		sta	$3E
		lda	#$00
		sta	$08
		jsr	LC006
		jsr	LC273
	: ; }

	pla
	sta	$08
	lda	$97
	sta	$3C
	lda	$98
	sta	$3E
	jsr	LC5AA
	rts
; ------------------------------------------------------------
	.export	LC529
LC529:
	lda	#48
	sta	$3C
	: ; for (byte_3C = 48; byte_3C > -16; byte_3C -= 16) {
		ldx	#4
		: ; for (x = 4; x > 0; x--) {
			jsr	LFF74

			dex
			bne	:-
		; }

		lda	$3E
		sta	$0C
		lda	$3F
		sta	$0D
		jsr	LC632

		lda	$3D
		beq	:+ ; if () {
			lda	$40
			sta	$0C
			lda	$41
			sta	$0D
			jsr	LC63D
		: ; }

		lda	$3C
		sec
		sbc	#16
		sta	$3C
		cmp	#<-16
		bne	:---
	; }

	rts
; ------------------------------------------------------------
	.export	LC55B
LC55B:
	lda	$95
	sta	$3D
	lda	$94
	sta	$3C
	asl	$94
	rol	$95
	clc
	adc	$94
	sta	$94
	lda	$95
	adc	$3D
	sta	$95
	lda	$94
	clc
	adc	$95
	sta	$95
	lda	$94
	clc
	adc	#$81
	sta	$94
	lda	$95
	adc	#$00
	sta	$95
	rts
; ------------------------------------------------------------
LC587:
	sta	$24
	: ; for (byte_24 = a; byte_24 < byte_04;) {
		lda	$04
		cmp	$24
		bcc	:+ ; if (byte_04 >= byte_24) {
			jsr	LFF74
			jmp	:-
		; } else break;
	: ; }

	rts
; ------------------------------------------------------------
	.export	LC596, LC5AA
LC596:
	lda	#$40
	ora	$3C
	sta	$3C
	bne	LC5A6

LC59E:
	lda	#$80
	ora	$3C
	sta	$3C
	bne	LC5AA

LC5A6:
	asl	$3C
	asl	$3E

LC5AA:
	lda	$3E
	sta	$0B
	lda	#$00
	sta	$0A
	lsr	$0B
	ror	$0A
	lsr	$0B
	ror	$0A
	lsr	$0B
	ror	$0A
	lda	$3C
	and	#$1F
	clc
	adc	$0A
	sta	$0A
	php

	lda	$3C
	bpl	:+ ; if () {
		lda	#$04
		bne	:+++
	: and	#$20
	bne	:+ ; } else if () {
		lda	#$20
		bne	:++
	: ; } else {
		lda	#$24
	: ; }

	plp
	adc	$0B
	sta	$0B
	rts
; ------------------------------------------------------------
LC5E0:
	asl	$3C
	asl	$3E
; --------------
LC5E4:
	lda	$3E
	and	#$FC
	asl	a
	sta	$0A
	lda	$3C
	and	#$1F
	lsr	a
	lsr	a
	clc
	adc	$0A
	clc
	adc	#$C0
	sta	$0A

	lda	$3C
	and	#$20
	bne	:+ ; if (!(byte_3C & 0x20)) {
		lda	#$03
		bne	:++
	: ; } else {
		lda	#$07
	: ; }
	sta	$0B
	rts
; ------------------------------------------------------------
	.export	LC608
LC608:
	lda	$3C
	pha
	lda	$3D
	pha
	jsr	LC55B
	pla
	sta	$3D
	pla
	sta	$3C
	lda	#$01
	sta	$4016
	lda	#$00
	sta	$4016

	ldy	#$08
	: ; for (button of joypad) {
		lda	$4016
		sta	$46
		lsr	a
		ora	$46
		lsr	a
		ror	$47

		dey
		bne	:-
	; }

	rts
; ------------------------------------------------------------
	.export	LC632, LC63D
LC632:
	lda	#$31
	jsr	LC587
	lda	#$10
	sta	$0A
	bne	LC646
; --------------
LC63D:
	lda	#$61
	jsr	LC587
	lda	#$00
	sta	$0A
; --------------
LC646:
	lda	#$3F
	sta	$0B

	ldy	#0
	: ; for (y = 0; y < 12;) {
		lda	#$0F
		sta	$08
		jsr	LC690
		jsr	LC661
		jsr	LC661
		jsr	LC661

		cpy	#12
		bne	:-
	; }

	rts
; -----------------------------
LC661:
	lda	$0A
	cmp	#$01
	beq	:+
	cmp	#$03
	bne	:++
	lda	$E0
	cmp	#$27
	bne	:++
	: lda	$CA
	lsr	a
	lsr	a
	clc
	adc	#$01
	cmp	$C5
	bcc	:+ ; if () {
		lda	#$26
		bne	:++
	: ; } else {
		lda	($0C), y
	: ; }

	sec
	sbc	$3C
	bcs	:+ ; if () {
		lda	#$0F
	: ; }
	sta	$08

	jsr	LC690
	iny
	rts
; -----------------------------
	.export	LC690
LC690:
	: ; while () {
		ldx	$04
		cpx	#$B0
		bcc	:+ ; if () {
			jsr	LFF74
			jmp	:-
		; } else break;
	: ; }

	ldx	$04
	lda	$0B
	sta	$0300, x
	inx
	lda	$0A
	sta	$0300, x
	inx
	lda	$08
	sta	$0300, x
	inx
	inc	$03
	stx	$04

	inc	$0A
	bne	:+ ; if () {
		inc	$0B
	: ; }

	rts
; ------------------------------------------------------------
	.export	LC6BB
LC6BB:
	jsr	LFF74

	ldx	#0
	lda	#$F0
	: ; for (byte of OAM) {
		sta	$0200, x

		inx
		bne	:-
	; }

	rts
; ------------------------------------------------------------
LC6C9:
	ldx	#0
	lda	#$5F
	: ; for (x = 0; x < 5; x++) {
		sta	$AF, x

		inx
		cpx	#5
		bne	:-
	; }

	lda	#$FA
	sta	$AF, x

	dex
	: ; for () {
		lda	#$0A
		sta	$3E
		lda	#$00
		sta	$3F
		jsr	LC1F4
		lda	$40
		sta	$AF, x

		dex
		lda	$3C
		ora	$3D
		bne	:-
	; }

	rts
; ------------------------------------------------------------
	.export	LC6F0
LC6F0:
	pla
	clc
	adc	#$01
	sta	$3E
	pla
	adc	#$00
	sta	$3F
	pha
	lda	$3E
	pha
	ldy	#$00
	lda	($3E), y
LC703:	brk
	bpl	LC71D
	rts
; ------------------------------------------------------------
LC707:	lda	$4D
	bne	LC71A
	ldy	#$00
	lda	($99), y
	cmp	#$FF
	beq	LC71A
	cmp	#$FE
	beq	LC71A
	jmp	LC734

LC71A:	lda	#$00
	.byte	$85
LC71D:	jmp	LD185

	jsr	LAD66
	ldy	#$00
	lda	#$FF
	sta	($99), y
	iny
	sta	($99), y
	ldy	#$20
	sta	($99), y
	iny
	sta	($99), y
	rts

LC734:	lda	$4B
	asl	a
	adc	$10
	clc
	adc	#$1E
	sta	$3C
	lda	#$1E
	sta	$3E
	jsr	LC1F0
	lda	$40
	sta	$3E
	sta	$49
	lda	$4A
	asl	a
	clc
	adc	$0F
	and	#$3F
	sta	$3C
	sta	$48
	jsr	LC5AA
	ldy	#$00
	lda	($99), y
	sta	$08
	jsr	LC690
	iny
	lda	($99), y
	sta	$08
	jsr	LC690
	lda	$0A
	clc
	adc	#$1E
	sta	$0A
	bcc	LC776
	inc	$0B
LC776:	ldy	#$20
	lda	($99), y
	sta	$08
	jsr	LC690
	iny
	lda	($99), y
	sta	$08
	jsr	LC690
	lda	$48
	sta	$3C
	lda	$49
	sta	$3E
	ldy	#$00
	lda	($99), y
	cmp	#$C1
	bcs	LC79B
	lda	#$00
	beq	LC7AD
LC79B:	cmp	#$CA
	bcs	LC7A3
	lda	#$01
	bne	LC7AD
LC7A3:	cmp	#$DE
LC7A5:	bcs	LC7AB
	lda	#$02
	bne	LC7AD
LC7AB:	lda	#$03
LC7AD:	sta	$08
	jsr	LC006
	lda	$0B
	clc
	adc	#$20
	sta	$0B
	jsr	LC690
	rts

LC7BD:	sta	$3C
	lda	#$00
	sta	$3D
	beq	LC7E4
LC7C5:	lda	#$01
	sta	$3D
	bne	LC7CF
LC7CB:	lda	#$00
	sta	$3D
LC7CF:	pla
	clc
	adc	#$01
	sta	$3E
	pla
	adc	#$00
	sta	$3F
	pha
	lda	$3E
	pha
	ldy	#$00
	lda	($3E), y
	sta	$3C
LC7E4:	lda	$3C
	ldx	$3D
	brk
	.byte	$12
	.byte	$17
	rts

LC7EC:	lda	$4B
	asl	a
	sta	$3C
	lda	$98
	clc
	adc	#$2C
	sec
	sbc	$3C
	sta	$3C
	lda	#$1E
	sta	$3E
	jsr	LC1F0
	lda	$40
	sta	$3E
	pha
	lda	$4A
	asl	a
	sta	$3C
	lda	$D4
	clc
	adc	#$10
	sec
	sbc	$3C
	sta	$3C
	jsr	LC59E
	lda	$0A
	sta	$0C
	lda	$0B
	sta	$0D
	pla
	sta	$3E
	lda	$4A
	asl	a
	sta	$3C
	lda	$D2
	clc
	adc	#$10
	sec
	sbc	$3C
	sta	$3C
	jsr	LC59E
	lda	$0A
	sta	$42
	lda	$0B
	sta	$43
	rts

LC83F:	lda	#$5F
	sta	$08
	jsr	LC4F5
	jmp	LC690

LC849:	lda	#$56
	sta	$08
	jsr	LC4F5
	jmp	LC690

	lda	#$FF
	sta	$4F
LC857:	jsr	LFF74
	jsr	LC83F
	lda	$47
	pha
	jsr	LC608
	pla
	beq	LC871
	lda	$4F
	and	#$0F
	cmp	#$0C
	beq	LC871
	jmp	LC9A9

LC871:	lda	$47
	and	#$01
	beq	LC893
	jsr	LC849
	lda	$D8
	cmp	#$01
	beq	LC884
	lda	#$00
	sta	$D7
LC884:	lda	$D9
	clc
	adc	$D7
	sta	$D7
	lda	#$85
	brk
	.byte	$04
	.byte	$17
	lda	$D7
	rts

LC893:	lda	$47
	and	#$02
	beq	LC8A6
	jsr	LC849
	lda	#$85
	brk
	.byte	$04
	.byte	$17
	lda	#$FF
	sta	$D7
	rts

LC8A6:	lda	$47
	and	#$10
	beq	LC8EE
	lda	$D8
	cmp	#$05
	beq	LC8CF
	lda	$D9
	bne	LC8B9
	jmp	LC9A9

LC8B9:	dec	$D9
	dec	$98
	dec	$98
	lda	$98
	cmp	#$FE
	beq	LC8C8
	jmp	LC9A5

LC8C8:	lda	#$1C
	sta	$98
	jmp	LC9A5

LC8CF:	lda	$D9
	bne	LC8D6
	jmp	LC9A9

LC8D6:	lda	#$00
	sta	$D9
	lda	$9D
	sta	$97
	lda	$9E
	sec
	sbc	#$02
	cmp	#$FE
	bne	LC8E9
	lda	#$1C
LC8E9:	sta	$98
	jmp	LC9A5

LC8EE:	lda	$47
	and	#$20
	beq	LC93A
	lda	$D8
	cmp	#$05
	beq	LC91B
	inc	$D9
	lda	$D9
	cmp	$D7
	bne	LC907
	dec	$D9
	jmp	LC9A9

LC907:	inc	$98
	inc	$98
	lda	$98
	cmp	#$1E
	beq	LC914
	jmp	LC9A5

LC914:	lda	#$00
	sta	$98
	jmp	LC9A5

LC91B:	lda	#$02
	cmp	$D9
	bne	LC924
	jmp	LC9A9

LC924:	sta	$D9
	lda	$9D
	sta	$97
	lda	$9E
	clc
	adc	#$02
	cmp	#$1E
	bne	LC935
	lda	#$00
LC935:	sta	$98
	jmp	LC9A5

LC93A:	lda	$47
	and	#$40
	beq	LC972
	lda	$D8
	cmp	#$05
	beq	LC95A
	lda	$D8
	cmp	#$01
	bne	LC9A9
	dec	$D8
	lda	$97
	sec
	sbc	#$06
	and	#$3F
	sta	$97
	jmp	LC9A5

LC95A:	lda	#$03
	cmp	$D9
	beq	LC9A9
	sta	$D9
	lda	$9E
	sta	$98
	lda	$9D
	sec
	sbc	#$02
	and	#$3F
	sta	$97
	jmp	LC9A5

LC972:	lda	$47
	and	#$80
	beq	LC9A9
	lda	$D8
	cmp	#$05
	beq	LC990
	lda	$D8
	bne	LC9A9
	inc	$D8
	lda	$97
	clc
	adc	#$06
	and	#$3F
	sta	$97
	jmp	LC9A5

LC990:	lda	#$01
	cmp	$D9
	beq	LC9A9
	sta	$D9
	lda	$9E
	sta	$98
	lda	$9D
	clc
	adc	#$02
	and	#$3F
	sta	$97
LC9A5:	lda	#$00
	sta	$4F
LC9A9:	lda	$4F
	and	#$10
	bne	LC9B2
	jsr	LC849
LC9B2:	jmp	LC857

LC9B5:	lda	#$00
	jsr	LFF91
	lda	#$00
	tax
	sta	$600A
	sta	$602F
LC9C3:	sta	$601C, x
	inx
	cpx	#$10
	bcc	LC9C3
	brk
	.byte	$02
	.byte	$17
	jsr	LFCA3
	jsr	LFCB8
	lda	#$FF
	sta	$C5
	lda	#$08
	sta	$4A
	lda	#$07
	sta	$4B
	jsr	LBD5B
	lda	#$01
	jsr	LC587
	lda	#$18
	sta	$2001
	lda	#$00
	sta	$6005
	brk
	brk
	.byte	$27
	lda	#$00
	sta	$2001
	jsr	LFF74
	jsr	LFC98
	jsr	LFCAD
	lda	#$04
	brk
	.byte	$04
	.byte	$17
	jsr	LF678
	lda	#$FA
	sta	$B9
	lda	#$00
	tax
LCA12:	sta	$600C, x
	inx
	cpx	#$20
	bne	LCA12
	jsr	LCB47
	lda	$DF
	and	#$08
	beq	LCA46
	jsr	LC7C5
	.byte	$17
	lda	$C7
	cmp	#$1E
	bne	LCA34
	jsr	LC7CB
	.byte	$02
	jmp	LCA3F

LCA34:	jsr	LF134
	jsr	LC7CB
	cmp	($20, x)
	cmp	$C7
	clc
LCA3F:	jsr	LC7CB
	cpy	$4C
	lsr	a
	dex
LCA46:	jsr	LC7C5
	.byte	$02
	jsr	LCFE4
	lda	#$02
	jsr	LA7A2
	lda	#$00
	sta	$96
LCA56:	lda	$DA
	beq	LCA6C
	dec	$DA
	bne	LCA6C
	lda	$D0
	cmp	#$01
	beq	LCA6C
	lda	#$3C
	sta	$DA
	dec	$D0
	dec	$D0
LCA6C:	lda	$DB
	beq	LCA9B
	dec	$DB
	dec	$DB
	beq	LCA7C
	lda	$DB
	cmp	#$01
	bne	LCA9B
LCA7C:	jsr	LFF74
	lda	#$FF
	sta	$96
	jsr	LC6F0
	.byte	$02
	lda	$DB
	bne	LCA8F
	lda	#$37
	bne	LCA91
LCA8F:	lda	#$34
LCA91:	jsr	LC7BD
	jsr	LCFD9
	lda	#$00
	sta	$DB
LCA9B:	lda	#$00
	sta	$4F
LCA9F:	jsr	LC608
	lda	$47
	and	#$08
	beq	LCAD7
LCAA8:	jsr	LFF74
	lda	$4F
	and	#$0F
	cmp	#$01
	beq	LCAB9
	jsr	LB6DA
	jmp	LCAA8

LCAB9:	jsr	LC608
	lda	$47
	and	#$08
	bne	LCAB9
LCAC2:	jsr	LC608
	lda	$47
	and	#$08
	beq	LCAC2
LCACB:	jsr	LC608
	lda	$47
	and	#$08
	bne	LCACB
	jmp	LCA9B

LCAD7:	lda	$47
	lsr	a
	bcc	LCAE2
	jsr	LCF49
	jmp	LCA9B

LCAE2:	lda	$47
	and	#$10
	beq	LCAF6
	lda	#$00
	sta	$602F
	jsr	LB504
	jsr	LB219
	jmp	LCA56

LCAF6:	lda	$47
	and	#$20
	beq	LCB0A
	lda	#$02
	sta	$602F
	jsr	LB3D8
	jsr	LB219
	jmp	LCA56

LCB0A:	lda	$47
	and	#$40
	beq	LCB1E
	lda	#$03
	sta	$602F
	jsr	LB34C
	jsr	LB219
	jmp	LCA56

LCB1E:	lda	$47
LCB20:	bpl	LCB30
	lda	#$01
	sta	$602F
	jsr	LB252
	jsr	LB219
	jmp	LCA56
; ------------------------------------------------------------
	.export	LCB30
LCB30:
	jsr	LFF74
	lda	$4F
	cmp	#$31
	bne	LCB41
	jsr	LC6F0
	brk
	lda	#$32
	sta	$4F
LCB41:	jsr	LB6DA
	jmp	LCA9F

LCB47:	jsr	LFF74
	lda	$9A18
	sta	$0C
	lda	$9A19
	sta	$0D
	lda	#$00
	sta	$3C
	jsr	LC632
	lda	#$30
	sta	$3C
	jsr	LC63D
	jsr	LFF74
	jsr	LF050
	jsr	LFC98
	lda	$CF
	and	#$C0
	beq	LCB78
	lda	#$01
	sta	$C5
	jmp	LCB96

LCB78:	lda	$603A
	cmp	#$78
	bne	LCB96
	txa
	pha
	lda	$CA
	sta	$C5
	lda	$CB
	sta	$C6
	ldx	$6039
	lda	#$AB
	sta	$6045, x
	sta	$603A
	pla
	tax
LCB96:	lda	#$03
	sta	$3A
	sta	$8E
	lda	#$04
	sta	$3B
	sta	$8F
	lda	#$30
	sta	$90
	lda	#$40
	sta	$92
	lda	#$00
	sta	$D6
	sta	$DB
	sta	$91
	sta	$93
	lda	#$08
	sta	$4A
	lda	#$07
	sta	$4B
	lda	#$05
	sta	$45
	jsr	LA788
	jsr	LB091
	jsr	LFF74
	lda	#$00
	sta	$4F
LCBCD:	jsr	LC608
	lda	$47
	bne	LCBDD
	jsr	LFF74
	jsr	LB6DA
	jmp	LCBCD

LCBDD:	jsr	LFF74
	lda	$4F
	and	#$0F
	cmp	#$01
	beq	LCBEE
	jsr	LB6DA
	jmp	LCBDD

LCBEE:	lda	#$FF
	sta	$96
	jsr	LC6F0
	.byte	$02
	rts
; ------------------------------------------------------------
	.export	LCBF7
LCBF7:
	lda	$E4
	and	#$04
	bne	LCC00
	jmp	LCCF6

LCC00:
	lda	$45
	cmp	#$04
	bne	LCC16
	lda	$3B
	cmp	#$08
	bne	LCC16
	lda	$3A
	cmp	#$0A
	beq	LCC19
	cmp	#$0B
	beq	LCC19
LCC16:	jmp	LCDA2

LCC19:	lda	#$00
	brk
	.byte	$04
	.byte	$17
	lda	#$FF
	sta	$96
	jsr	LC6F0
	.byte	$02
	jsr	LC7C5
	.byte	$1B
	lda	$DF
	and	#$02
	beq	LCC6A
	lda	#$C7
	sta	$8A
	lda	#$27
	sta	$8B
	lda	#$00
	sta	$8C
	jsr	LFF74
	jsr	LB6DA
	jsr	LC7C5
	.byte	$1C
LCC46:	jsr	LFF74
	jsr	LFF74
	lda	$8C
	clc
	adc	#$10
	sta	$8C
	bcc	LCC57
	inc	$8A
LCC57:	jsr	LB6DA
	lda	$8A
	cmp	#$CA
	bne	LCC46
	lda	#$47
	sta	$8B
	jsr	LB6DA
	jmp	LCC8B

LCC6A:	lda	$DF
	lsr	a
	bcc	LCCB8
	lda	$DF
	and	#$FE
	sta	$DF
	lda	#$CA
	sta	$8A
	lda	#$47
	sta	$8B
	lda	#$00
	sta	$8C
	jsr	LFF74
	jsr	LB6DA
	jsr	LC7C5
	.byte	$1C
LCC8B:	jsr	LC7C5
	ora	$C520, x
	.byte	$C7
	asl	LF020, x
	dec	$09
	beq	LCCA0
	jsr	LC7CB
	ldx	$4C, y
	.byte	$8F
	.byte	$CC
LCCA0:	jsr	LC7CB
	clv
	lda	#$00
	sta	$8A
	sta	$8B
	sta	$8C
	lda	$DF
	ora	#$01
	sta	$DF
	jsr	LFF74
	jsr	LB6DA
LCCB8:	jsr	LC7C5
	.byte	$22
	ldx	#$78
LCCBE:	jsr	LFF74
	dex
	bne	LCCBE
	lda	#$02
	jsr	LA7A2
	lda	#$01
	sta	$602F
	jsr	LB6DA
	lda	#$1E
	jsr	LC170
	lda	#$02
	sta	$602F
	jsr	LB6DA
	ldx	#$1E
LCCE0:	jsr	LFF74
	dex
	bne	LCCE0
	lda	#$FF
	sta	$C7
	jsr	LB6DA
	brk
	asl	$2017
	dey
	.byte	$FC
LCCF3:	jmp	LCCF3

LCCF6:	lda	$BE
	and	#$1C
	cmp	#$1C
	beq	LCD0A
	cmp	#$18
	bne	LCD30
	inc	$E3
	lda	$E3
	and	#$03
	bne	LCD30
LCD0A:	inc	$C5
	lda	$C5
	cmp	$CA
	bcc	LCD16
	lda	$CA
	sta	$C5
LCD16:	lda	$CA
	lsr	a
	lsr	a
	clc
	adc	#$01
	cmp	$C5
	bcs	LCD30
	lda	#$01
	sta	$0A
	lda	#$3F
	sta	$0B
	lda	#$30
	sta	$08
	jsr	LC690
LCD30:	lda	$45
	cmp	#$04
	bne	LCD51
	lda	$CF
	and	#$C0
	beq	LCD51
	lda	$3B
	cmp	#$1B
	bne	LCD51
	lda	#$FF
	sta	$96
	jsr	LC6F0
	.byte	$02
	jsr	LC7CB
	.byte	$44
	jmp	LB228

LCD51:	lda	$45
	cmp	#$03
	bne	LCD68
	lda	$3A
	cmp	#$12
	bne	LCD68
	lda	$3B
	cmp	#$0C
	bne	LCD68
	lda	#$21
	jmp	LE4DF

LCD68:	lda	$45
	cmp	#$15
	bne	LCD85
	lda	$3A
	cmp	#$04
	bne	LCD85
	lda	$3B
	cmp	#$0E
	bne	LCD85
	lda	$E4
	and	#$40
	bne	LCD85
	lda	#$1E
	jmp	LE4DF

LCD85:	lda	$45
	cmp	#$01
	bne	LCDA2
	lda	$3A
	cmp	#$49
	bne	LCDA2
	lda	$3B
	cmp	#$64
	bne	LCDA2
	lda	$E4
	and	#$02
	bne	LCDA2
	lda	#$18
	jmp	LE4DF

LCDA2:	jsr	LC55B
	lda	$3A
	sta	$3C
	lda	$3B
	sta	$3E
	jsr	LAC17
	lda	$3C
	sta	$E0
	cmp	#$07
	bcc	LCDBF
	cmp	#$0A
	bcs	LCDBF
	jmp	LD941

LCDBF:	lda	$E4
	and	#$04
	beq	LCDC6
	rts

LCDC6:	lda	$E0
	cmp	#$06
	bne	LCE02
	lda	$BE
	and	#$1C
	cmp	#$1C
	beq	LCDFB
	lda	#$84
	brk
	.byte	$04
	.byte	$17
	jsr	LEE14
	jsr	LFF74
	lda	$C5
	sec
	sbc	#$02
	bcs	LCDE8
	lda	#$00
LCDE8:	sta	$C5
	jsr	LFF74
	jsr	LEE28
	lda	$C5
	bne	LCDFB
	jsr	LC6F0
	brk
	jmp	LEDA7

LCDFB:	lda	#$0F
LCDFD:	and	$95
	beq	LCE7C
	rts

LCE02:	cmp	#$01
	beq	LCE13
	cmp	#$02
	bne	LCE17
	jsr	LFF74
	jsr	LFF74
	jsr	LFF74
LCE13:	lda	#$07
	bne	LCDFD
LCE17:	cmp	#$0B
	beq	LCE5F
	cmp	#$04
	beq	LCE5F
	cmp	#$0D
	bne	LCE63
	lda	$BE
	and	#$1C
	cmp	#$1C
	beq	LCE5F
	lda	#$80
	brk
	.byte	$04
	.byte	$17
	lda	#$03
	sta	$42
LCE34:	jsr	LFF74
	jsr	LEE14
	jsr	LFF74
	jsr	LEE28
	dec	$42
	bne	LCE34
	lda	$C5
	sec
	sbc	#$0F
	bcs	LCE4F
	.byte	$A9
LCE4C:	brk
	beq	LCE4F
LCE4F:	sta	$C5
	cmp	#$00
	bne	LCE5F
	jsr	LEE28
	jsr	LC6F0
	brk
	jmp	LEDA7

LCE5F:	lda	#$0F
	bne	LCDFD
LCE63:	lda	$3A
	lsr	a
	bcs	LCE6F
	lda	$3B
	lsr	a
	bcc	LCE74
	bcs	LCE78
LCE6F:	lda	$3B
	lsr	a
	bcc	LCE78
LCE74:	lda	#$1F
LCE76:	bne	LCDFD
LCE78:	lda	#$0F
	bne	LCE76
LCE7C:	lda	$45
	cmp	#$01
	bne	LCED8
	lda	$3B
	sta	$3C
	lda	#$0F
	sta	$3E
	jsr	LC1F0
	lda	$3C
	sta	$42
	lda	$3A
	sta	$3C
	lda	#$0F
	sta	$3E
	jsr	LC1F0
	lda	$42
	asl	a
	asl	a
	sta	$3E
	lda	$3C
	lsr	a
	clc
	adc	$3E
	tax
	lda	LF522, x
	sta	$3E
	lda	$3C
	lsr	a
	bcs	LCEBB
	lsr	$3E
	lsr	$3E
	lsr	$3E
	lsr	$3E
LCEBB:	lda	$3E
	and	#$0F
	bne	LCF04
	jsr	LC55B
	lda	$E0
	cmp	#$02
	bne	LCED1
	lda	$95
	and	#$03
	beq	LCF04
	rts

LCED1:	lda	$95
	and	#$01
	beq	LCF04
	rts

LCED8:	cmp	#$02
	bne	LCEE0
	lda	#$10
	bne	LCF04
LCEE0:	cmp	#$03
	bne	LCEE8
	lda	#$0D
	bne	LCF04
LCEE8:	cmp	#$06
	bne	LCEF0
	lda	#$12
	bne	LCF04
LCEF0:	cmp	#$1C
	bcs	LCEFA
	lda	$16
	cmp	#$20
	beq	LCEFB
LCEFA:	rts

LCEFB:	lda	$45
	sec
	sbc	#$0F
	tax
	lda	LF542, x
LCF04:	sta	$3E
	asl	a
	asl	a
	clc
	adc	$3E
	sta	$3E
LCF0D:	jsr	LC55B
	lda	$95
	and	#$07
	cmp	#$05
	bcs	LCF0D
	adc	$3E
	tax
	lda	LF54F, x
	sta	$3C
	lda	$45
	cmp	#$01
	bne	LCF44
	lda	$DB
	beq	LCF44
	lda	$CD
	lsr	a
	sta	$3E
	ldx	$3C
	lda	LF4FA, x
	sec
	sbc	$3E
	bcc	LCF43
	sta	$3E
	lda	LF4FA, x
	lsr	a
	cmp	$3E
	bcc	LCF44
LCF43:	rts

LCF44:	lda	$3C
	jmp	LE4DF

LCF49:	jsr	LFF74
	lda	$4F
	and	#$0F
	cmp	#$01
	beq	LCF5A
	jsr	LB6DA
	jmp	LCF49

LCF5A:	lda	#$FF
	sta	$96
	jsr	LC6F0
	brk
	jsr	LC6F0
	.byte	$03
	cmp	#$FF
	bne	LCF74
LCF6A:	lda	#$03
	jsr	LA7A2
	lda	#$00
	sta	$96
	rts

LCF74:	lda	$D7
	cmp	#$01
	bne	LCFAF
	jsr	LF050
	jsr	LD92E
	lda	$BE
	lsr	a
	lsr	a
	lsr	a
	lsr	a
	lsr	a
	clc
	adc	#$09
	sta	$AB
	lda	$BE
	lsr	a
	lsr	a
	and	#$07
	clc
	adc	#$11
	sta	$AC
	lda	$BE
	and	#$03
	clc
	adc	#$19
	sta	$AD
	jsr	LC6F0
	ora	($20, x)
	cpx	$CF
	lda	#$01
	jsr	LA7A2
	jmp	LCF6A

LCFAF:	cmp	#$00
	beq	LCFF9
	cmp	#$02
	bne	LCFBA
	jmp	LD9AF

LCFBA:	cmp	#$06
	bne	LCFC1
	jmp	LDC42

LCFC1:	cmp	#$04
	bne	LCFC8
	jmp	LDA11

LCFC8:	cmp	#$05
	bne	LCFCF
	jmp	LDC1B

LCFCF:	cmp	#$03
	bne	LCFD6
	jmp	LE103

LCFD6:	jmp	LE1E3

LCFD9:	jsr	LCFE4
	lda	#$02
	jsr	LA7A2
	jmp	LCF6A

LCFE4:	jsr	LFF74
	jsr	LC608
	lda	$47
	bne	LCFE4
LCFEE:	jsr	LFF74
	jsr	LC608
	lda	$47
	beq	LCFEE
	rts

LCFF9:	lda	$602F
	pha
	jsr	LC6F0
	.byte	$02
	lda	$3A
	sta	$3C
	lda	$3B
	sta	$3E
	pla
	bne	LD01C
	dec	$3E
	jsr	LAC17
	lda	$3C
	cmp	#$0E
	bne	LD051
	dec	$43
	jmp	LD051

LD01C:	cmp	#$01
	bne	LD030
	inc	$3C
	jsr	LAC17
	lda	$3C
	cmp	#$0E
	bne	LD051
	inc	$42
	.byte	$4C
LD02E:	eor	($D0), y
LD030:	cmp	#$02
	bne	LD044
	inc	$3E
	jsr	LAC17
	lda	$3C
	cmp	#$0E
	bne	LD051
	inc	$43
	jmp	LD051

LD044:	dec	$3C
	jsr	LAC17
	lda	$3C
	cmp	#$0E
	bne	LD051
	dec	$42
LD051:	lda	$3C
	cmp	#$17
	bne	LD0A9
	lda	$42
	pha
	lda	$43
	pha
	jsr	LC7CB
	lda	$20, x
	.byte	$CB
	.byte	$C7
	cmp	$20
	beq	LD02E
	ora	#$F0
	.byte	$07
	jsr	LC7CB
	ldx	$4C, y
	adc	($D0, x)
	lda	$DF
	ora	#$01
	sta	$DF
	jsr	LFF74
	jsr	LB6DA
	pla
	sec
	sbc	$3B
	asl	a
	sta	$10
	pla
	sec
	sbc	$3A
	asl	a
	sta	$0F
	lda	#$00
	sta	$4C
	jsr	LAD66
	jsr	LC7CB
	.byte	$B7
	lda	#$16
	brk
	.byte	$04
	.byte	$17
	brk
	.byte	$03
	.byte	$17
	lda	#$06
	brk
	.byte	$04
	.byte	$17
	lda	#$B8
	jmp	LD242

LD0A9:	ldy	#$00
LD0AB:	lda	$51, y
	and	#$1F
	cmp	$42
	bne	LD0D2
	lda	$52, y
	and	#$1F
	cmp	$43
	bne	LD0D2
	lda	$51, y
	bne	LD0CF
	lda	$52, y
	bne	LD0CF
	lda	$53, y
	bne	LD0CF
	jmp	LD1ED

LD0CF:	jmp	LD0DC

LD0D2:	iny
	iny
	iny
	cpy	#$3C
	bne	LD0AB
	jmp	LD1ED

LD0DC:	sty	$25
	cpy	#$1E
	bcc	LD102
	tya
	sec
	sbc	#$1C
	tay
	lda	$45
	sec
	sbc	#$04
	cmp	#$0B
	bcc	LD0F3
	jmp	LD1ED

LD0F3:	asl	a
	tax
	lda	$974C, x
	sta	$3C
	lda	$974D, x
	sta	$3D
	jmp	LD11C

LD102:	iny
	iny
	lda	$45
	sec
	sbc	#$04
	cmp	#$0B
	bcc	LD110
	jmp	LD1ED

LD110:	asl	a
	tax
	lda	$9734, x
	sta	$3C
	lda	$9735, x
	sta	$3D
LD11C:	lda	$25
	jsr	LC04A
	tya
	pha
	lda	$3C
	pha
	lda	$3D
	pha
	jsr	LB6DA
	pla
	sta	$3D
	pla
	sta	$3C
	pla
	tay
	lda	$E4
	and	#$04
	beq	LD16A
	lda	$45
	cmp	#$04
	bne	LD147
	jsr	LC7C5
	and	($4C, x)
	.byte	$D9
	.byte	$CF
LD147:	lda	($3C), y
	cmp	#$64
	bne	LD154
	jsr	LC7C5
	ora	$4C, x
	.byte	$D9
	.byte	$CF
LD154:	jsr	LC55B
	lda	$95
	lsr	a
	bcc	LD163
	jsr	LC7C5
	.byte	$1F
	jmp	LCFD9

LD163:	jsr	LC7C5
	jsr	LD94C
	.byte	$CF
LD16A:	lda	($3C), y
	cmp	#$07
	bcs	LD173
	jmp	LD553

LD173:	cmp	#$0C
	bcs	LD17A
	jmp	LD6A7

LD17A:	cmp	#$0F
	bcs	LD181
	jmp	LD7ED

LD181:	cmp	#$11
	bcs	LD188
LD185:	jmp	LD843

LD188:	cmp	#$16
	bcs	LD18F
	jmp	LD895

LD18F:	cmp	#$5E
	bcs	LD1C5
	pha
	lda	$DF
	and	#$08
	bne	LD1B1
	pla
	cmp	#$23
	bne	LD1A6
	jsr	LC7C5
	ora	($4C, x)
	.byte	$D9
	.byte	$CF
LD1A6:	cmp	#$24
	bne	LD1B2
	jsr	LC7C5
	brk
	jmp	LCFD9

LD1B1:	pla
LD1B2:	pha
	clc
	adc	#$2F
	jsr	LC7BD
	pla
	cmp	#$1A
	bne	LD1C2
	jsr	LC7CB
	.byte	$B0
LD1C2:	jmp	LCFD9

LD1C5:	cmp	#$62
	bcs	LD1F4
	clc
	adc	#$2F
	sta	$DE
	jsr	LC7BD
	jsr	LC6F0
	ora	#$D0
	.byte	$0B
	lda	$DE
	clc
	adc	#$05
	jsr	LC7BD
	jmp	LCFD9

	lda	$DE
	clc
	adc	#$0A
	jsr	LC7BD
	jmp	LCFD9

LD1ED:	jsr	LC7CB
	.byte	$0F
	jmp	LCFD9

LD1F4:	bne	LD204
	lda	$DF
	and	#$03
	.byte	$D0
LD1FB:	.byte	$04
LD1FC:	lda	#$9B
	bne	LD242
LD200:	lda	#$9C
	bne	LD242
LD204:	cmp	#$63
	bne	LD212
	lda	$DF
	and	#$03
	beq	LD1FC
	lda	#$9D
	bne	LD242
LD212:	cmp	#$64
	bne	LD224
	lda	$DF
	and	#$03
	bne	LD220
	lda	#$9E
	bne	LD242
LD220:	lda	#$9F
	bne	LD242
LD224:	cmp	#$65
	bne	LD248
	lda	$DF
	and	#$03
	bne	LD240
	jsr	LC7CB
	ldy	#$20
	beq	LD1FB
	ora	#$F0
	.byte	$04
	jsr	LC7CB
	lda	($A9, x)
	ldx	#$D0
	.byte	$02
LD240:	lda	#$A3
LD242:	jsr	LC7BD
	jmp	LCFD9

LD248:	cmp	#$66
	bne	LD26A
	lda	#$0C
	jsr	LE055
	cmp	#$FF
	bne	LD266
	lda	#$0E
	jsr	LE055
	cmp	#$FF
	bne	LD266
	jsr	LC7CB
	ldy	$A9
	dec	$D0
	.byte	$DC
LD266:	lda	#$A5
	bne	LD242
LD26A:	cmp	#$67
	bne	LD298
	lda	$CF
	and	#$C0
	bne	LD278
	lda	#$A6
	bne	LD242
LD278:	jsr	LC7CB
	.byte	$A7
	lda	$CF
	bpl	LD285
	lda	#$0B
	jsr	LE04B
LD285:	bit	$CF
	bvc	LD28E
	lda	#$09
	jsr	LE04B
LD28E:	lda	$CF
	and	#$3F
	sta	$CF
	lda	#$A8
	bne	LD242
LD298:	cmp	#$68
	bne	LD2AC
	lda	$BE
	and	#$E0
	cmp	#$E0
	beq	LD2A8
	lda	#$A9
	bne	LD242
LD2A8:	lda	#$AA
	bne	LD242
LD2AC:	cmp	#$69
	bne	LD2CF
	lda	#$06
	jsr	LE055
	cmp	#$FF
	bne	LD2BF
	lda	$CF
	and	#$DF
	sta	$CF
LD2BF:	lda	$CF
	and	#$20
	bne	LD2CA
	lda	#$AC
	jmp	LD242

LD2CA:	lda	#$AB
	jmp	LD242

LD2CF:	cmp	#$6A
	bne	LD2E5
	jsr	LC7CB
	lda	$3720
	.byte	$DB
	lda	$CB
	sta	$C6
	jsr	LC6F0
	brk
	jmp	LCFD9

LD2E5:	cmp	#$6B
	bne	LD2F8
	jsr	LC7CB
	jmp	LCB20

	.byte	$C7
	ldx	LCB20
	.byte	$C7
	.byte	$AF
	jmp	LCFD9

LD2F8:	cmp	#$6C
	bne	LD345
	lda	#$0E
	jsr	LE055
	cmp	#$FF
	bne	LD30E
	lda	#$0D
	jsr	LE055
	cmp	#$FF
	beq	LD316
LD30E:	lda	#$A5
LD310:	jsr	LC7BD
	jmp	LCFD9

LD316:	lda	#$0A
	jsr	LE055
	cmp	#$FF
	beq	LD341
	jsr	LC7CB
	.byte	$B2
	jsr	LC7CB
	ldy	$20
	.byte	$CB
	.byte	$C7
	dec	$A9
	asl	a
	jsr	LE04B
	lda	#$00
	sta	$6F
	sta	$70
	sta	$71
	jsr	LFF74
	jsr	LB6DA
	jmp	LCFD9

LD341:	lda	#$B1
	bne	LD310
LD345:	cmp	#$6D
	bne	LD3AB
	lda	#$0E
	jsr	LE055
	cmp	#$FF
	bne	LD30E
	lda	#$07
	jsr	LE055
	cmp	#$FF
	bne	LD365
	jsr	LC7CB
	.byte	$B3
	jsr	LDB37
	jmp	LB228

LD365:	lda	#$0C
	jsr	LE055
	cmp	#$FF
	beq	LD3A2
	lda	#$0D
	jsr	LE055
	cmp	#$FF
	beq	LD3A2
	jsr	LC7CB
	ldy	$A9, x
	.byte	$0C
	jsr	LE04B
	lda	#$0D
	jsr	LE04B
	lda	#$0E
	jsr	LE01B
	jsr	LFF74
	lda	#$19
	sta	$2001
	ldx	#$1E
LD394:	jsr	LFF74
	dex
	bne	LD394
	lda	#$18
	sta	$2001
	jmp	LCFD9

LD3A2:	jsr	LC7CB
	eor	#$A9
	ldx	$104C
	.byte	$D3
LD3AB:	cmp	#$6E
	beq	LD3B2
	jmp	LD464

LD3B2:	lda	$DF
	and	#$01
	beq	LD410
	jsr	LC7CB
	lda	$08A9, y
	jsr	LE01B
	cpx	#$04
	bne	LD3EF
	ldx	#$00
LD3C7:	lda	LD54C, x
	jsr	LE055
	cmp	#$FF
	bne	LD3D8
	inx
	cpx	#$07
	bne	LD3C7
	beq	LD3EF
LD3D8:	lda	LD54C, x
	pha
	jsr	LE04B
	pla
	clc
	adc	#$31
	jsr	LDBF0
	lda	#$08
	jsr	LE01B
	jsr	LC7CB
	tsx
LD3EF:	jsr	LC7CB
	.byte	$BB
	jsr	LC7CB
	ldy	LDFA5, x
	and	#$FC
	ora	#$02
	sta	$DF
	lda	#$C6
	sta	$78
	lda	#$43
	sta	$79
	jsr	LFF74
	jsr	LB6DA
	jmp	LD433

LD410:	lda	$DF
	.byte	$29
LD413:	php
	bne	LD41B
	lda	#$BF
	jmp	LD242

LD41B:	jsr	LC7CB
	cpy	#$A5
	.byte	$C7
	cmp	#$1E
	bne	LD42C
	jsr	LC7CB
	.byte	$02
	jmp	LD433

LD42C:	jsr	LF134
	jsr	LC7CB
	.byte	$C1
LD433:	jsr	LC7C5
	.byte	$23
	jsr	LC6F0
	ora	#$C9
	brk
	bne	LD446
	jsr	LF148
	jsr	LC7C5
	.byte	$24
LD446:	jsr	LC7C5
	and	$20
	beq	LD413
	ora	#$C9
	brk
	beq	LD45F
	jsr	LC7C5
	rol	$00
	ora	$17
	jsr	LFC88
LD45C:	jmp	LD45C

LD45F:	lda	#$C4
	jmp	LD242

LD464:	cmp	#$6F
	bne	LD4A4
	jsr	LC55B
	lda	$95
	and	#$60
	bne	LD476
	lda	#$BB
	jmp	LD242

LD476:	cmp	#$60
	bne	LD47F
	lda	#$BD
	jmp	LD242

LD47F:	jsr	LC7CB
	ldx	LF020, y
	dec	$09
	beq	LD490
	.byte	$20
LD48A:	.byte	$CB
	.byte	$C7
	ldx	$4C, y
	.byte	$7F
	.byte	$D4
LD490:	jsr	LC7CB
	clv
	lda	#$16
	brk
	.byte	$04
	.byte	$17
	brk
	.byte	$03
	.byte	$17
	lda	#$02
	brk
	.byte	$04
	.byte	$17
	jmp	LCFD9

LD4A4:	cmp	#$70
	beq	LD4AB
	jmp	LD533

LD4AB:	jsr	LC7CB
	.byte	$C7
	jsr	LC7CB
	ldy	$20
	.byte	$CB
	.byte	$C7
	iny
	jsr	LC6F0
	ora	#$D0
	asl	a
	jsr	LC7C5
	asl	$20, x
	beq	LD48A
	ora	#$F0
	jsr	LCB20
	.byte	$C7
	cmp	#$A2
	plp
LD4CD:	jsr	LFF74
	dex
	bne	LD4CD
	lda	#$02
	jsr	LA7A2
	lda	#$03
	jsr	LA7A2
	lda	#$00
	jsr	LA7A2
	lda	#$26
	jmp	LE4DF

	jsr	LC7CB
	dex
	jsr	LC7CB
	.byte	$C2
	lda	#$00
	sta	$BA
	sta	$BB
	sta	$BC
	sta	$BD
	sta	$C1
	sta	$C2
	sta	$C3
	sta	$C4
	sta	$BF
	sta	$C0
	sta	$BE
	sta	$CF
	sta	$DF
	sta	$E4
	jsr	LC7CB
	.byte	$C3
	jsr	LC7CB
	.byte	$CB
	lda	$9A28
	sta	$0C
	lda	$9A29
	sta	$0D
	lda	#$00
	sta	$3C
	jsr	LC63D
	jsr	LFF74
	jsr	LC6F0
	brk
	jsr	LFF74
LD530:	jmp	LD530

LD533:	cmp	#$71
	beq	LD53E
	cmp	#$72
	beq	LD545
	jmp	LD1ED

LD53E:	jsr	LC7CB
	.byte	$03
	jmp	LCFD9

LD545:	jsr	LC7CB
	sta	($4C), y
	.byte	$D9
	.byte	$CF
LD54C:	ora	($04, x)
	asl	$02
	.byte	$03
	ora	#$0D
LD553:	sta	$DE
	jsr	LC7CB
	plp
	jsr	LC6F0
	ora	#$F0
	.byte	$03
	jmp	LD66B

	jsr	LC7CB
	and	$7220
	dec	$A5, x
	.byte	$D7
	cmp	#$FF
	bne	LD572
	jmp	LD66B

LD572:	lda	$9991, x
	pha
	clc
	adc	#$1B
	jsr	LDBF0
	jsr	LC7CB
	and	#$A9
	brk
	sta	$00
	sta	$01
	pla
	asl	a
	tax
	lda	$BC
	sec
	sbc	$9947, x
	lda	$BD
	sbc	$9948, x
	bcs	LD599
	jmp	LD660

LD599:	txa
	pha
	cmp	#$0E
	bcs	LD5AA
	lda	$BE
	lsr	a
	lsr	a
	lsr	a
	lsr	a
	lsr	a
	beq	LD5E0
	bne	LD5C4
LD5AA:	cmp	#$1C
	bcs	LD5BB
	lda	$BE
	lsr	a
	lsr	a
	and	#$07
	beq	LD5E0
	clc
	adc	#$07
	bne	LD5C4
LD5BB:	lda	$BE
	and	#$03
	beq	LD5E0
	clc
	adc	#$0E
LD5C4:	asl	a
	tay
	lda	$9945, y
	sta	$00
	lda	$9946, y
	sta	$01
	lsr	$01
	ror	$00
	tya
	lsr	a
	clc
	adc	#$1A
	jsr	LDBF0
	jsr	LC7CB
	rol	a
LD5E0:	jsr	LC7CB
	.byte	$27
	jsr	LC6F0
	ora	#$68
	tax
	lda	$D7
	beq	LD5F5
	jsr	LC7CB
	rol	$4C
	.byte	$64
	.byte	$D6
LD5F5:	lda	$BC
	sec
	sbc	$9947, x
	sta	$BC
	lda	$BD
	sbc	$9948, x
	sta	$BD
	lda	$BC
	clc
	adc	$00
	sta	$BC
	lda	$BD
	adc	$01
	sta	$BD
	bcc	LD619
	lda	#$FF
	sta	$BC
	sta	$BD
LD619:	txa
	lsr	a
	cmp	#$07
	bcs	LD63C
	clc
	adc	#$01
	asl	a
	asl	a
	asl	a
	asl	a
	asl	a
	sta	$3C
	lda	$BE
	and	#$1F
	ora	$3C
	sta	$BE
LD631:	jsr	LC7CB
	rol	LF020
	dec	$00
	jmp	LD664

LD63C:	cmp	#$0E
	bcs	LD651
	sec
	sbc	#$06
	asl	a
	asl	a
	sta	$3C
	lda	$BE
	and	#$E3
	ora	$3C
	sta	$BE
	bne	LD631
LD651:	sec
	sbc	#$0D
	sta	$3C
	lda	$BE
	and	#$FC
	ora	$3C
	sta	$BE
	bne	LD631
LD660:	jsr	LC7CB
	.byte	$2B
LD664:	jsr	LC7CB
	bit	$594C
	.byte	$D5
LD66B:	jsr	LC7CB
	.byte	$2F
	jmp	LCFD9

	ldx	#$00
	lda	$DE
LD676:	sta	$3C
	beq	LD686
LD67A:	lda	$9991, x
	inx
	cmp	#$FD
	bne	LD67A
	dec	$3C
	bne	LD67A
LD686:	txa
	pha
	ldy	#$01
	sty	$A3
LD68C:	lda	$9991, x
	clc
	adc	#$02
	sta	$A3, y
	cmp	#$FF
	beq	LD69D
	inx
	iny
	bne	LD68C
LD69D:	jsr	LC6F0
	php
	pla
	clc
	adc	$D7
	tax
	rts

LD6A7:	sta	$DE
	jsr	LC7CB
	and	$20
	beq	LD676
	asl	a
	beq	LD6C1
	cmp	#$01
	bne	LD6BA
	jmp	LD739

LD6BA:	jsr	LC7CB
	asl	LD94C, x
	.byte	$CF
LD6C1:	jsr	LC7CB
	bit	$20
	.byte	$72
	dec	$A5, x
	.byte	$D7
	cmp	#$FF
	beq	LD6BA
	lda	$9991, x
	pha
	clc
	adc	#$1F
	jsr	LDBF0
	pla
	asl	a
	tax
	lda	$BC
	sec
	sbc	$9947, x
	sta	$3C
	lda	$BD
	sbc	$9948, x
	sta	$3D
	bcs	LD6F3
	jsr	LC7CB
	.byte	$22
	jmp	LD716

LD6F3:	cpx	#$22
	bne	LD726
	lda	$C0
	cmp	#$06
	bne	LD704
	jsr	LC7CB
	jsr	$164C
	.byte	$D7
LD704:	inc	$C0
LD706:	lda	$3C
	sta	$BC
	lda	$3D
	sta	$BD
	jsr	LC7CB
	.byte	$23
	jsr	LC6F0
	brk
LD716:	jsr	LC7CB
	.byte	$1F
	jsr	LC6F0
	ora	#$D0
	.byte	$03
	jmp	LD6C1

	jmp	LD6BA

LD726:	txa
	lsr	a
	sec
	sbc	#$12
	jsr	LE01B
	cpx	#$04
	bne	LD706
	jsr	LC7CB
	and	($4C, x)
	asl	$D7, x
LD739:	jsr	LDF77
	cpx	#$01
	bne	LD747
	jsr	LC7CB
	ora	$BA4C, y
	.byte	$D6
LD747:	jsr	LC7CB
	ora	LF020, x
	dec	$07
	cmp	#$FF
	bne	LD756
	jmp	LD6BA

LD756:	tax
	lda	$A4, x
	sta	$DE
	clc
	adc	#$2E
	jsr	LDBF0
	lda	$DE
	clc
	adc	#$0F
	asl	a
	tax
	lda	$9947, x
	sta	$00
	lda	$9948, x
	sta	$01
	ora	$00
	bne	LD78A
	jsr	LC7CB
	.byte	$1B
LD77A:	jsr	LC7CB
	.byte	$1A
	jsr	LC6F0
	ora	#$D0
	.byte	$03
	jmp	LD739

	jmp	LD6BA

LD78A:	lsr	$01
	ror	$00
	jsr	LC7CB
	.byte	$1C
	jsr	LC6F0
	ora	#$D0
	.byte	$E2
	lda	$DE
	cmp	#$03
	bne	LD7BC
	dec	$BF
LD7A0:	lda	$BC
	clc
	adc	$00
	sta	$BC
	lda	$BD
	adc	$01
	sta	$BD
	bcc	LD7B5
	lda	#$FF
	sta	$BC
	sta	$BD
LD7B5:	jsr	LC6F0
	brk
	jmp	LD77A

LD7BC:	cmp	#$02
	bne	LD7C5
	dec	$C0
	jmp	LD7A0

LD7C5:	cmp	#$0C
LD7C7:	bne	LD7DA
	pha
	bit	$CF
	bvc	LD7E3
LD7CE:	pla
	jsr	LC7CB
	clc
	jsr	LC7CB
	.byte	$17
	jmp	LD77A

LD7DA:	cmp	#$0E
	bne	LD7E4
	pha
	lda	$CF
	bmi	LD7CE
LD7E3:	pla
LD7E4:	sec
	sbc	#$03
	jsr	LE04B
	jmp	LD7A0

LD7ED:	sec
	sbc	#$0C
	tax
	lda	$9989, x
	sta	$00
	lda	#$00
	sta	$01
	jsr	LC7CB
	asl	$20, x
	beq	LD7C7
	ora	#$F0
	.byte	$07
LD804:	jsr	LC7CB
	.byte	$12
	jmp	LCFD9

	lda	$BF
	cmp	#$06
	bne	LD818
	jsr	LC7CB
	.byte	$14
	jmp	LD804

LD818:	lda	$BC
	sec
	sbc	$00
	sta	$3C
	lda	$BD
	sbc	$01
	sta	$3D
	bcs	LD82E
	jsr	LC7CB
	.byte	$13
	jmp	LD804

LD82E:	lda	$3C
	sta	$BC
	lda	$3D
	sta	$BD
	inc	$BF
	jsr	LC6F0
	brk
	jsr	LC7CB
	ora	$4C, x
	.byte	$FE
	.byte	$D7
LD843:	lda	#$26
	sta	$00
	lda	#$00
	sta	$01
	jsr	LC7CB
	ora	($20), y
	beq	LD818
	ora	#$F0
	.byte	$07
LD855:	jsr	LC7CB
	.byte	$0C
	jmp	LCFD9

	lda	$BC
	sec
	sbc	$00
	sta	$3C
	lda	$BD
	sbc	$01
	sta	$3D
	bcs	LD872
	jsr	LC7CB
	.byte	$22
	jmp	LD855

LD872:	lda	#$02
	jsr	LE01B
	cpx	#$04
	bne	LD882
	jsr	LC7CB
	and	($4C, x)
	eor	$D8, x
LD882:	lda	$3C
	sta	$BC
	lda	$3D
	sta	$BD
	jsr	LC6F0
	brk
	jsr	LC7CB
	bpl	LD8DF
	.byte	$4F
	cld
LD895:	sec
	sbc	#$11
	tax
	lda	$998C, x
	sta	$00
	lda	#$00
	sta	$01
	jsr	LC7CB
	.byte	$0B
	jsr	LC6F0
	ora	#$F0
	.byte	$07
	jsr	LC7CB
LD8AF:	asl	a
	jmp	LCFD9

	lda	$BC
	sec
	sbc	$00
	sta	$3C
	lda	$BD
	sbc	$01
	sta	$3D
	bcs	LD8C9
	jsr	LC7CB
	.byte	$22
	jmp	LD8AF

LD8C9:	lda	$3C
	sta	$BC
	lda	$3D
	sta	$BD
	jsr	LC6F0
	brk
	jsr	LC7CB
	ora	#$20
	ora	$D9, x
	jsr	LC212
LD8DF:	lda	#$15
	brk
	.byte	$04
	.byte	$17
	lda	$CA
	sta	$C5
	lda	$CB
	sta	$C6
	jsr	LC6F0
	brk
	jsr	LD915
	brk
	.byte	$03
	.byte	$17
	lda	#$04
	brk
	.byte	$04
	.byte	$17
	jsr	LC529
	lda	$DF
	lsr	a
	bcc	LD90A
	jsr	LC7CB
	asl	$4C
	.byte	$0E
	.byte	$D9
LD90A:	jsr	LC7CB
	php
	jsr	LC7CB
	.byte	$07
	jmp	LCFD9

LD915:	lda	$9A24
	sta	$3E
	lda	$9A25
	sta	$3F
	lda	$9A1C
	sta	$40
	lda	$9A1D
	sta	$41
	lda	#$FF
	sta	$3D
	rts

LD92E:	ldx	#$00
	lda	#$01
LD932:	sta	$A3, x
	inx
	clc
	adc	#$01
	cpx	#$0B
	bne	LD932
	rts

LD93D:	lda	#$00
	beq	LD943
LD941:	lda	#$01
LD943:	sta	$2C
	ldx	#$00
	ldy	#$00
LD949:	lda	$45
	.byte	$DD
LD94C:	iny
	.byte	$F3
	bne	LD99C
	lda	$3A
	cmp	LF3C9, x
	bne	LD99C
	lda	$3B
	cmp	LF3CA, x
	bne	LD99C
	lda	LF461, x
	sta	$45
	lda	LF462, x
	sta	$3A
	sta	$8E
	sta	$90
	lda	LF463, x
	sta	$3B
	sta	$8F
	sta	$92
	lda	$2C
	beq	LD97F
	lda	$9914, y
	jmp	LD981

LD97F:	lda	#$01
LD981:	and	#$03
	sta	$602F
	lda	#$00
	sta	$91
	sta	$93
	ldx	#$04
LD98E:	asl	$90
	rol	$91
	asl	$92
	rol	$93
	dex
	bne	LD98E
	jmp	LB097

LD99C:	inx
	inx
	inx
	iny
	cpx	#$99
	bne	LD949
	jsr	LC6F0
	.byte	$02
	jsr	LC7CB
	asl	LD94C
	.byte	$CF
LD9AF:	lda	$3A
	sta	$3C
	lda	$3B
	sta	$3E
	jsr	LAC17
	lda	$3C
	cmp	#$05
	bne	LD9C3
	jmp	LD93D

LD9C3:	cmp	#$03
	bne	LDA06
	ldx	#$00
	ldy	#$00
LD9CB:	lda	$45
	cmp	LF461, x
	bne	LD9FE
	lda	$3A
	cmp	LF462, x
	bne	LD9FE
	lda	$3B
	cmp	LF463, x
	bne	LD9FE
	lda	#$03
	.export	LD9E2
LD9E2:
	pha
	lda	LF3C8, x
	sta	$45
	lda	LF3C9, x
	sta	$3A
	sta	$8E
	sta	$90
	lda	LF3CA, x
	sta	$3B
	sta	$8F
	sta	$92
	pla
	jmp	LD981

LD9FE:	inx
	inx
	inx
	iny
	cpx	#$99
	bne	LD9CB
LDA06:	jsr	LC6F0
	.byte	$02
	jsr	LC7CB
	ora	LD94C
	.byte	$CF
LDA11:	lda	$CE
	sta	$3E
	lda	$CF
	and	#$03
	sta	$3F
	ora	$3E
	bne	LDA2A
	jsr	LC6F0
	.byte	$02
	jsr	LC7CB
	and	($4C), y
	.byte	$D9
	.byte	$CF
LDA2A:	jsr	LDB56
	cmp	#$FF
	bne	LDA34
	jmp	LCF6A

LDA34:	pha
	jsr	LC6F0
	.byte	$02
	pla
	jsr	LDB85
	cmp	#$32
	bne	LDA47
	jsr	LC7BD
	jmp	LCFD9

LDA47:	cmp	#$00
	bne	LDA51
	jsr	LDBB8
	jmp	LCFD9

LDA51:	cmp	#$01
	bne	LDA5C
LDA55:	jsr	LC7CB
	.byte	$33
	jmp	LCFD9

LDA5C:	cmp	#$02
	beq	LDA55
	cmp	#$03
	bne	LDA94
	lda	$16
	cmp	#$20
	bne	LDA55
	lda	#$50
	sta	$DA
	lda	#$02
	jsr	LA7A2
	lda	#$03
	jsr	LA7A2
	lda	#$00
	jsr	LA7A2
LDA7D:	lda	$D0
	cmp	#$07
	bne	LDA84
	rts

LDA84:	clc
	adc	#$02
	sta	$D0
	lda	#$92
	brk
	.byte	$04
	.byte	$17
	jsr	LB30E
	jmp	LDA7D

LDA94:	cmp	#$07
	bne	LDA9F
	lda	#$FF
	sta	$DB
	jmp	LCFD9

LDA9F:	cmp	#$05
	bne	LDAE3
	lda	$45
	cmp	#$1C
	bcc	LDAB0
	ldx	#$27
	lda	#$02
	jmp	LD9E2

LDAB0:	cmp	#$18
	bcc	LDABB
	ldx	#$39
	lda	#$02
	jmp	LD9E2

LDABB:	cmp	#$16
	bcc	LDAC6
	ldx	#$18
	lda	#$02
	jmp	LD9E2

LDAC6:	cmp	#$15
	bne	LDAD1
	ldx	#$0F
	lda	#$02
	jmp	LD9E2

LDAD1:	cmp	#$0F
	bcs	LDADC
	cmp	#$06
	beq	LDADC
	jmp	LDA55

LDADC:	ldx	#$12
	lda	#$02
	jmp	LD9E2

LDAE3:	cmp	#$08
	bne	LDAED
	jsr	LDBD7
	jmp	LCFD9

LDAED:	cmp	#$06
	bne	LDB34
	lda	$16
	cmp	#$20
	beq	LDAFD
	lda	$45
	cmp	#$06
	bne	LDB00
LDAFD:	jmp	LDA55

LDB00:	lda	#$01
	sta	$45
	lda	#$2A
	sta	$3A
	sta	$8E
	sta	$90
	lda	#$2B
	sta	$3B
	sta	$8F
	sta	$92
	lda	#$00
	sta	$91
	sta	$93
	ldx	#$04
LDB1C:	asl	$90
	rol	$91
	asl	$92
	rol	$93
	dex
	bne	LDB1C
	lda	#$81
	brk
	.byte	$04
	.byte	$17
	lda	#$02
	sta	$602F
	jmp	LB08D

LDB34:	jmp	LDA55

LDB37:	ldx	#$08
LDB39:	jsr	LFF74
	jsr	LFF74
	lda	#$19
	sta	$2001
	jsr	LFF74
	jsr	LFF74
	jsr	LFF74
	.byte	$A9
LDB4E:	clc
	sta	$2001
	dex
	bne	LDB39
	rts

LDB56:	jsr	LD92E
	lda	#$02
	sta	$40
	ldx	#$01
LDB5F:	lsr	$3F
	ror	$3E
	bcc	LDB6A
	lda	$40
	sta	$A3, x
	inx
LDB6A:	inc	$40
	lda	$40
	cmp	#$0C
	bne	LDB5F
	lda	#$FF
	sta	$A3, x
	jsr	LC6F0
	asl	$C9
	.byte	$FF
	beq	LDB84
	tax
	lda	$A4, x
	sec
	sbc	#$02
LDB84:	rts

LDB85:	sta	$D7
	ldx	$D7
	lda	$C6
	cmp	$9D53, x
	bcs	LDB93
	lda	#$32
	rts

LDB93:	sbc	$9D53, x
	sta	$C6
	lda	$D7
	clc
	adc	#$10
	jsr	LDBF0
	jsr	LC7CB
	bmi	LDB4E
	sta	($00), y
	.byte	$04
	.byte	$17
	jsr	LDB37
	brk
	.byte	$03
	.byte	$17
	lda	$D7
	pha
	jsr	LC6F0
	brk
	pla
	rts

LDBB8:	jsr	LC55B
	lda	$95
	and	#$07
	clc
	adc	#$0A
LDBC2:	clc
	adc	$C5
	bcs	LDBCB
	cmp	$CA
	bcc	LDBCD
LDBCB:	lda	$CA
LDBCD:	sta	$C5
	jsr	LEE28
	jsr	LC6F0
	brk
	rts

LDBD7:	jsr	LC55B
	lda	$95
	and	#$0F
	clc
	adc	#$55
	jmp	LDBC2

LDBE4:	lda	#$08
	sta	$3C
	lda	#$01
	sta	$3D
	ldy	#$00
	beq	LDC10
LDBF0:	clc
	adc	#$03
	tax
	inx
	lda	LF152
	sta	$3C
	lda	LF153
	sta	$3D
	.byte	$A0
LDC00:	brk
LDC01:	lda	($3C), y
	inc	$3C
	bne	LDC09
	inc	$3D
LDC09:	cmp	#$FA
	bne	LDC01
	dex
	bne	LDC01
LDC10:	lda	($3C), y
	sta	$A3, y
	iny
	cmp	#$FA
	bne	LDC10
	rts

LDC1B:	jsr	LDF77
	cpx	#$01
	bne	LDC2D
	jsr	LC6F0
	.byte	$02
	jsr	LC7CB
	and	LD94C, x
	.byte	$CF
LDC2D:	jsr	LC6F0
	.byte	$07
	cmp	#$FF
	bne	LDC38
	jmp	LCF6A

LDC38:	tax
	lda	$A4, x
	cmp	#$03
	beq	LDC42
	jmp	LDCEA

LDC42:	lda	$3A
	sta	$3C
	lda	$3B
	sta	$3E
	dec	$3E
	jsr	LAC17
	lda	$3C
	cmp	#$11
	beq	LDC99
	lda	$3A
	sta	$3C
	lda	$3B
	sta	$3E
	inc	$3E
	jsr	LAC17
	lda	$3C
	cmp	#$11
	beq	LDC99
	lda	$3A
	sta	$3C
	lda	$3B
	sta	$3E
	dec	$3C
	jsr	LAC17
	lda	$3C
	cmp	#$11
	beq	LDC99
	lda	$3A
	sta	$3C
	lda	$3B
	sta	$3E
	inc	$3C
	jsr	LAC17
	lda	$3C
	cmp	#$11
	beq	LDC99
	jsr	LC6F0
	.byte	$02
	jsr	LC7C5
	.byte	$0B
	jmp	LCFD9

LDC99:	lda	$BF
	bne	LDCA8
	jsr	LC6F0
	.byte	$02
	jsr	LC7C5
	.byte	$0C
	jmp	LCFD9

LDCA8:	dec	$BF
	ldx	#$00
LDCAC:	lda	$600C, x
	beq	LDCBA
	inx
	inx
	cpx	#$10
	bne	LDCAC
	jmp	LCFD9

LDCBA:	lda	$42
	sta	$600C, x
	lda	$43
	sta	$600D, x
	lda	$42
	sec
	sbc	$3A
	asl	a
	sta	$0F
	lda	$43
	sec
	sbc	$3B
	asl	a
	sta	$10
	lda	#$00
	sta	$4C
	lda	#$94
	brk
	.byte	$04
	.byte	$17
	jsr	LAD66
LDCE0:	jsr	LC608
	lda	$47
	bne	LDCE0
	jmp	LCF6A

LDCEA:	cmp	#$02
	bne	LDD1A
	jsr	LC6F0
	.byte	$02
	jsr	LC7CB
	.byte	$F7
	dec	$C0
	jsr	LDCFE
	jmp	LCFD9

LDCFE:	jsr	LC55B
	lda	$95
	and	#$07
	clc
	adc	#$17
	adc	$C5
	cmp	$CA
	bcc	LDD10
	lda	$CA
LDD10:	sta	$C5
	jsr	LEE28
	jsr	LC6F0
	brk
	rts

LDD1A:	cmp	#$04
	bne	LDD53
	lda	$16
	cmp	#$20
	beq	LDD2F
	jsr	LC6F0
	.byte	$02
	jsr	LC7CB
	and	$4C, x
	.byte	$D9
	.byte	$CF
LDD2F:	lda	#$01
	jsr	LE04B
	lda	#$00
	sta	$DA
	lda	#$02
	jsr	LA7A2
	lda	#$03
	jsr	LA7A2
	lda	#$00
	jsr	LA7A2
	lda	#$03
	sta	$D0
	lda	#$92
	brk
	.byte	$04
	.byte	$17
	jmp	LB30E

LDD53:	cmp	#$05
	bne	LDD6B
	jsr	LC6F0
	.byte	$02
	jsr	LC7CB
	rol	$A9, x
	.byte	$02
	jsr	LE04B
	lda	#$FE
	sta	$DB
	jmp	LCFD9

LDD6B:	cmp	#$06
	bne	LDD95
	jsr	LC6F0
	.byte	$02
	lda	$16
	cmp	#$20
	beq	LDD7F
	lda	$45
	cmp	#$06
	bne	LDD86
LDD7F:	jsr	LC7CB
	sec
	jmp	LCFD9

LDD86:	lda	#$03
	jsr	LE04B
	jsr	LC7CB
	and	$3720, y
	.byte	$DB
	jmp	LDB00

LDD95:	cmp	#$07
	bne	LDDA3
	jsr	LC6F0
	.byte	$02
	jsr	LDFB9
	jmp	LCFD9

LDDA3:	cmp	#$08
	bne	LDDC6
	jsr	LC6F0
	.byte	$02
	jsr	LC7CB
	.byte	$3C
	lda	#$12
	brk
	.byte	$04
	.byte	$17
	brk
	.byte	$03
	.byte	$17
	ldx	$45
	lda	$B1AE, x
	brk
	.byte	$04
	.byte	$17
	jsr	LC7CB
	.byte	$33
	jmp	LCFD9

LDDC6:	cmp	#$09
	bne	LDDD4
	jsr	LC6F0
	.byte	$02
	jsr	LDFD1
	jmp	LCFD9

LDDD4:	cmp	#$0A
	bne	LDDEC
	jsr	LC6F0
	.byte	$02
	lda	#$38
LDDDE:	jsr	LDBF0
	jsr	LC7CB
	rti

	jsr	LC7CB
	.byte	$33
	jmp	LCFD9

LDDEC:	cmp	#$0F
	bne	LDDF8
	jsr	LC6F0
	.byte	$02
	lda	#$3D
	bne	LDDDE
LDDF8:	cmp	#$10
	bne	LDE04
	jsr	LC6F0
	.byte	$02
	lda	#$3E
	bne	LDDDE
LDE04:	cmp	#$0D
	bne	LDE50
	jsr	LC6F0
	.byte	$02
	jsr	LC7CB
	eor	($A9, x)
	ora	($00), y
	.byte	$04
	.byte	$17
	brk
	.byte	$03
	.byte	$17
	lda	$45
	cmp	#$01
	bne	LDE41
LDE1E:	jsr	LC55B
	lda	$95
	and	#$07
	cmp	#$05
	beq	LDE1E
	cmp	#$07
	beq	LDE1E
	pha
	lda	#$02
	jsr	LA7A2
	lda	#$03
	jsr	LA7A2
	lda	#$00
	jsr	LA7A2
	pla
	jmp	LE4DF

LDE41:	ldx	$45
	lda	$B1AE, x
	brk
	.byte	$04
	.byte	$17
	jsr	LC7CB
	.byte	$33
	jmp	LCFD9

LDE50:	cmp	#$0C
	bne	LDE66
	jsr	LC6F0
	.byte	$02
	jsr	LDFE7
	ldx	$45
	lda	$B1AE, x
	brk
	.byte	$04
	.byte	$17
	jmp	LCFD9

LDE66:	cmp	#$0E
	bne	LDE7C
	jsr	LC6F0
	.byte	$02
	jsr	LE00A
	ldx	$45
	lda	$B1AE, x
	brk
	.byte	$04
	.byte	$17
	jmp	LCFD9

LDE7C:	cmp	#$11
	beq	LDE83
	jmp	LDF0D

LDE83:	jsr	LC6F0
	.byte	$02
	jsr	LC7CB
	.byte	$04
	lda	$45
	cmp	#$01
	bne	LDF08
	lda	$3A
	cmp	#$41
	bne	LDF08
	lda	$3B
	cmp	#$31
	bne	LDF08
	lda	$CF
	and	#$08
	bne	LDF08
	lda	#$02
LDEA5:	jsr	LA7A2
	lda	#$03
	jsr	LA7A2
	lda	#$00
	jsr	LA7A2
	lda	#$13
	brk
	.byte	$04
	.byte	$17
	lda	$CF
	ora	#$08
	sta	$CF
	lda	#$FE
	sta	$0F
	lda	#$00
	sta	$10
	lda	#$04
	sta	$4C
LDEC9:	lda	#$21
	sta	$08
LDECD:	jsr	LFF74
	jsr	LFF74
	jsr	LFF74
	jsr	LFF74
	jsr	LFF74
	lda	#$03
	sta	$0A
	lda	#$3F
	sta	$0B
	jsr	LC690
	inc	$08
	lda	$08
	cmp	#$12
	beq	LDEFD
	cmp	#$2D
	bne	LDECD
	dec	$4C
	bne	LDEC9
	lda	#$11
	sta	$08
	bne	LDECD
LDEFD:	brk
	.byte	$03
	.byte	$17
	lda	#$05
	brk
	.byte	$04
	.byte	$17
	jmp	LAD66

LDF08:	lda	#$05
	jmp	LD242

LDF0D:	cmp	#$0B
	bne	LDF74
	jsr	LC6F0
	.byte	$02
	lda	$C7
	cmp	#$1E
	bne	LDF22
	jsr	LC7C5
	ora	$4C
	and	#$DF
LDF22:	jsr	LF134
	jsr	LC7CB
	.byte	$DB
	lda	$45
	cmp	#$01
	bne	LDF6F
	jsr	LC7CB
	.byte	$DC
	jsr	LC7CB
	cmp	$3BA5, x
	sec
	sbc	#$2B
	bcs	LDF48
	eor	#$FF
	sta	$00
	inc	$00
	lda	#$DF
	bne	LDF4C
LDF48:	sta	$00
	lda	#$DE
LDF4C:	ldx	#$00
	stx	$01
	jsr	LC7BD
	lda	$3A
	sec
	sbc	#$2B
	bcs	LDF64
	eor	#$FF
	sta	$00
	inc	$00
	lda	#$E0
	bne	LDF68
LDF64:	sta	$00
	lda	#$E1
LDF68:	ldx	#$00
	stx	$01
	jsr	LC7BD
LDF6F:	lda	#$BD
	jmp	LD242

LDF74:	jmp	LCFD9

LDF77:	ldx	#$00
	lda	#$01
	sta	$A3, x
	inx
	lda	$C0
	beq	LDF87
	lda	#$02
	sta	$A3, x
	inx
LDF87:	lda	$BF
	beq	LDF90
	lda	#$03
	sta	$A3, x
	inx
LDF90:	ldy	#$00
LDF92:	lda	$C1, y
	and	#$0F
	beq	LDF9F
	clc
	adc	#$03
	sta	$A3, x
	inx
LDF9F:	lda	$C1, y
	and	#$F0
	.byte	$F0
LDFA5:	ora	#$4A
	lsr	a
	lsr	a
	lsr	a
	adc	#$03
	sta	$A3, x
	inx
	iny
	cpy	#$04
	bne	LDF92
	lda	#$FF
	sta	$A3, x
	rts

LDFB9:	lda	$CF
	and	#$10
	bne	LDFCC
	lda	$CF
	ora	#$10
	sta	$CF
	jsr	LC7CB
	.byte	$3A
	jmp	LF050

LDFCC:	jsr	LC7CB
	.byte	$3B
	rts

LDFD1:	lda	$CF
	and	#$20
	bne	LDFE2
	lda	$CF
	ora	#$20
	sta	$CF
	jsr	LC7CB
	.byte	$3E
	rts

LDFE2:	jsr	LC7CB
	.byte	$3F
	rts

LDFE7:	lda	#$3A
	jsr	LDBF0
	bit	$CF
	bvs	LE003
	lda	$CF
	ora	#$40
	sta	$CF
LDFF6:	jsr	LC7CB
	.byte	$42
LDFFA:	lda	#$17
	brk
	.byte	$04
	.byte	$17
LDFFF:	brk
	.byte	$03
	.byte	$17
	rts

LE003:	jsr	LC7CB
	.byte	$43
	jmp	LDFFA

LE00A:	lda	#$3C
	jsr	LDBF0
	lda	$CF
	bmi	LE003
	lda	$CF
	ora	#$80
	sta	$CF
	bne	LDFF6
LE01B:	sta	$3E
	ldx	#$00
LE01F:	lda	$C1, x
	and	#$0F
	bne	LE02E
	lda	$C1, x
	and	#$F0
	ora	$3E
	sta	$C1, x
	rts

LE02E:	lda	$C1, x
	and	#$F0
	bne	LE045
	asl	$3E
	asl	$3E
	asl	$3E
	asl	$3E
	lda	$C1, x
	and	#$0F
	ora	$3E
	sta	$C1, x
	rts

LE045:	inx
	cpx	#$04
	bne	LE01F
	rts

LE04B:	jsr	LE055
	and	$C1, y
	sta	$C1, y
	rts

LE055:	ldy	#$00
	sta	$3C
LE059:	lda	$C1, y
	and	#$0F
	cmp	$3C
	bne	LE065
	lda	#$F0
	rts

LE065:	lda	$C1, y
	lsr	a
	lsr	a
	lsr	a
	lsr	a
	cmp	$3C
	bne	LE073
	lda	#$0F
	rts

LE073:	iny
	cpy	#$04
	bne	LE059
	lda	#$FF
	rts

LE07B:	sta	$DE
	jsr	LC7CB
	cpy	LF020
	dec	$09
	beq	LE094
LE087:	lda	$DE
	clc
	adc	#$31
	jsr	LDBF0
	lda	#$CD
	jmp	LD242

LE094:	jsr	LC7CB
	dec	$7720
	.byte	$DF
	jsr	LC6F0
	.byte	$07
	cmp	#$FF
	beq	LE087
	tax
	lda	$A4, x
	ldy	#$00
LE0A8:	cmp	LE0FA, y
	bne	LE0B4
	jsr	LC7CB
	cmp	($4C), y
	sty	$E0, x
LE0B4:	iny
	cpy	#$09
	bne	LE0A8
	cmp	#$0C
	bne	LE0C8
	bit	$CF
	bvc	LE0C8
LE0C1:	jsr	LC7CB
	clc
	jmp	LE094

LE0C8:	lda	$A4, x
	cmp	#$0E
	bne	LE0D2
	lda	$CF
	bmi	LE0C1
LE0D2:	lda	$A4, x
	pha
	clc
	adc	#$2E
	jsr	LDBF0
	jsr	LC7CB
	.byte	$CF
	lda	$DE
	clc
	adc	#$31
	jsr	LDBF0
	jsr	LC7CB
	bne	LE154
	sec
	sbc	#$03
	jsr	LE04B
	lda	$DE
	jsr	LE01B
	jmp	LCFD9

LE0FA:	.byte	$02
	.byte	$03
	php
	asl	a
	.byte	$0B
	ora	$100F
	.byte	$11
LE103:	jsr	LC6F0
	.byte	$02
	jsr	LC7CB
	.byte	$D2
	lda	$45
	cmp	#$01
	bne	LE14A
	lda	$3A
	cmp	#$53
	bne	LE14A
	lda	$3B
	cmp	#$71
	bne	LE14A
	lda	#$07
LE11F:	sta	$DE
	jsr	LE055
	cmp	#$FF
	beq	LE12D
LE128:	lda	#$D3
	jmp	LD242

LE12D:	lda	$DE
	clc
	adc	#$31
	jsr	LDBF0
	jsr	LC7CB
	cmp	$A5, x
	dec	$1B20, x
	cpx	#$E0
	.byte	$04
	beq	LE145
	jmp	LCFD9

LE145:	lda	$DE
	jmp	LE07B

LE14A:	lda	$45
	cmp	#$07
	bne	LE160
	lda	$3A
	cmp	#$09
LE154:	bne	LE160
	lda	$3B
	cmp	#$06
	bne	LE160
	lda	#$05
	bne	LE11F
LE160:	lda	$45
	cmp	#$03
	bne	LE18A
	lda	$3A
	cmp	#$12
	bne	LE18A
	lda	$3B
	cmp	#$0C
	bne	LE18A
	lda	$BE
	and	#$1C
	cmp	#$1C
	beq	LE128
	lda	$BE
	ora	#$1C
	sta	$BE
	lda	#$28
	jsr	LDBF0
	lda	#$D5
	jmp	LD242

LE18A:	lda	$45
	cmp	#$02
	bne	LE1C8
	lda	$3A
	cmp	#$0A
	bne	LE1C8
	lda	$3B
	cmp	#$03
	bne	LE1A1
	lda	#$D6
	jmp	LD242

LE1A1:	cmp	#$01
	bne	LE1C8
	lda	$CF
	and	#$04
	bne	LE1C8
	lda	$CF
	ora	#$04
	sta	$CF
	lda	#$0F
	jsr	LDBF0
	jsr	LC7CB
	cmp	$A9, x
	brk
	sta	$10
	sta	$0F
	sta	$4C
	jsr	LAD66
	jmp	LCFD9

LE1C8:	lda	$3A
	sta	$3C
	lda	$3B
	sta	$3E
	jsr	LAC17
	lda	$3C
	cmp	#$0C
	bne	LE1DE
	lda	#$D4
	jmp	LD242

LE1DE:	lda	#$D3
	jmp	LD242

LE1E3:	jsr	LC6F0
	.byte	$02
	lda	$3A
	sta	$3C
	lda	$3B
	sta	$3E
	jsr	LAC17
	lda	$3C
	cmp	#$0C
	beq	LE1FD
LE1F8:	lda	#$D7
	jmp	LD242

LE1FD:	brk
	php
	.byte	$17
	ldy	#$00
LE202:	lda	$45
	cmp	$0320, y
	bne	LE217
	lda	$3A
	cmp	$0321, y
	bne	LE217
	lda	$3B
	cmp	$0322, y
	beq	LE221
LE217:	iny
	iny
	iny
	iny
	cpy	#$7C
	bne	LE202
	beq	LE1F8
LE221:	lda	$0323, y
	sta	$DE
	cmp	#$03
	bne	LE242
	lda	$BF
	cmp	#$06
	bne	LE238
LE230:	jsr	LE39A
	lda	#$DA
	jmp	LD242

LE238:	inc	$BF
LE23A:	jsr	LE39A
	lda	#$D9
	jmp	LD242

LE242:	cmp	#$02
	bne	LE250
	lda	$C0
	cmp	#$06
	beq	LE230
	inc	$C0
	bne	LE23A
LE250:	cmp	#$0E
	bne	LE297
	lda	$DF
	and	#$04
	bne	LE288
	jsr	LC55B
	lda	$95
	and	#$1F
	bne	LE288
	lda	$DF
	ora	#$04
	sta	$DF
LE269:	jsr	LE39A
	lda	$DE
	sec
	sbc	#$03
	sta	$DE
	jsr	LC7CB
	cmp	LDEA5, y
	jsr	LE01B
	cpx	#$04
	beq	LE283
	jmp	LCFD9

LE283:	lda	$DE
	jmp	LE07B

LE288:	lda	#$1F
	sta	$3E
	lda	#$64
	sta	$00
	lda	#$00
	sta	$01
	jmp	LE365

LE297:	cmp	#$11
	bne	LE2B9
	lda	$BE
	and	#$E0
	cmp	#$E0
	bne	LE2A6
LE2A3:	jmp	LE230

LE2A6:	lda	$BE
	ora	#$E0
	sta	$BE
	jsr	LE39A
	lda	#$21
	jsr	LDBF0
	lda	#$D9
	jmp	LD242

LE2B9:	cmp	#$0D
	bne	LE2DB
	lda	#$0A
	jsr	LE055
	cmp	#$FF
	bne	LE2A3
	lda	#$0D
	jsr	LE055
	cmp	#$FF
	bne	LE2A3
LE2CF:	lda	#$0E
	jsr	LE055
	cmp	#$FF
	bne	LE2A3
	jmp	LE269

LE2DB:	cmp	#$0F
	bne	LE2EA
	lda	#$0C
	jsr	LE055
	cmp	#$FF
	bne	LE2A3
	beq	LE2CF
LE2EA:	cmp	#$11
	bcs	LE2F1
	jmp	LE269

LE2F1:	cmp	#$12
	bne	LE303
	lda	#$0F
	sta	$3E
	lda	#$05
	sta	$00
	lda	#$00
	sta	$01
	beq	LE365
LE303:	cmp	#$13
	bne	LE315
	lda	#$07
	sta	$3E
	lda	#$06
	sta	$00
	lda	#$00
	sta	$01
	beq	LE365
LE315:	cmp	#$14
	bne	LE327
	lda	#$07
	sta	$3E
	lda	#$0A
	sta	$00
	lda	#$00
	sta	$01
	beq	LE365
LE327:	cmp	#$15
	bne	LE339
	lda	#$FF
	sta	$3E
	lda	#$F4
	sta	$00
	lda	#$01
	sta	$01
	bne	LE365
LE339:	cmp	#$16
	bne	LE349
	lda	#$00
	sta	$3E
	sta	$01
	lda	#$78
	sta	$00
	bne	LE365
LE349:	jsr	LE39A
	ldx	#$00
LE34E:	lda	LE363, x
	sta	$A3, x
	inx
	cpx	#$02
	bne	LE34E
	jsr	LC7CB
	cmp	$C520, y
	.byte	$C7
	.byte	$03
	jmp	LCFD9

LE363:	.byte	$19
	.byte	$FA
LE365:	jsr	LC55B
	lda	$95
	and	$3E
	clc
	adc	$00
	sta	$00
	lda	$01
	adc	#$00
	sta	$01
	jsr	LE39A
	lda	$BC
	clc
	adc	$00
	sta	$BC
	lda	$BD
	adc	$01
	sta	$BD
	bcc	LE38F
	lda	#$FF
	sta	$BC
	sta	$BD
LE38F:	jsr	LC7CB
	cld
	jsr	LC6F0
	brk
	jmp	LCFD9

LE39A:	ldx	#$00
LE39C:	lda	$601C, x
	ora	$601D, x
	beq	LE3AB
	inx
	inx
	cpx	#$10
	bne	LE39C
	rts

LE3AB:	lda	$3A
	sta	$601C, x
	lda	$3B
	sta	$601D, x
	lda	#$00
	sta	$0F
	sta	$10
	sta	$4C
	lda	#$93
	brk
	.byte	$04
	.byte	$17
	jsr	LAD66
	lda	$DE
	clc
	adc	#$2E
	jmp	LDBF0

LE3CD:	lda	$E0
	cmp	#$27
	bne	LE3D4
	rts

LE3D4:	ldx	#$00
	stx	$4D
	stx	$98
	lda	#$0A
	sta	$3C
	lda	#$05
	sta	$3D
LE3E2:	ldy	#$00
LE3E4:	lda	$45
	cmp	#$01
	beq	LE3EE
	lda	#$5F
	bne	LE3F1
LE3EE:	lda	$9C8F, x
LE3F1:	sta	($3C), y
	inx
	iny
	cpy	#$0E
	bne	LE3E4
	lda	$3C
	clc
	adc	#$20
	sta	$3C
	lda	$3D
	adc	#$00
	sta	$3D
	cpx	#$C4
	bne	LE3E2
	jsr	LFF74
	jsr	LB6DA
LE410:	ldx	$98
	lda	LF397, x
	lsr	a
	lsr	a
	lsr	a
	lsr	a
	clc
	adc	#$FA
	sta	$0F
	clc
	adc	#$10
	sta	$3C
	lda	LF397, x
	and	#$0F
	clc
	adc	#$FA
	sta	$10
	clc
	adc	#$0E
	sta	$3E
	jsr	LC59E
	lda	$0A
	sta	$99
	lda	$0B
	sta	$9A
	jsr	LFF74
	jsr	LC707
	inc	$98
	lda	$98
	cmp	#$31
	bne	LE410
	jsr	LFF74
	rts

LE44F:	lda	$E0
	cmp	#$27
	bne	LE45D
	lda	#$00
	sta	$E0
	brk
	.byte	$02
	.byte	$07
	rts

LE45D:	lda	#$00
	sta	$3E
	sta	$41
	lda	#$03
	sta	$3F
	lda	#$A0
	sta	$40
	brk
	asl	a
	.byte	$17
	ldx	#$10
	ldy	#$00
	sty	$99
	lda	#$03
	sta	$9A
LE478:	lda	($99), y
	beq	LE4C8
	sta	$0201, x
	iny
	lda	($99), y
	and	#$3F
	clc
	adc	#$44
	sta	$0200, x
	lda	($99), y
	and	#$C0
	sta	$3C
	iny
	lda	($99), y
	and	#$03
	ora	$3C
	sta	$3C
	lda	($99), y
	lsr	a
	lsr	a
	sec
	sbc	#$1C
	sta	$3D
	lda	$09
	beq	LE4B4
	lda	$3D
	eor	#$FF
	sta	$3D
	inc	$3D
	lda	$3C
	eor	#$40
	sta	$3C
LE4B4:	lda	$3C
	sta	$0202, x
	lda	$3D
	clc
	adc	#$84
	sta	$0203, x
	inx
	inx
	inx
	inx
	iny
	bne	LE478
LE4C8:	jsr	LEEFD
	jsr	LFCB8
	lda	#$00
	sta	$3D
	lda	#$30
	sta	$3C
	jsr	LC632
	jsr	LC529
	jmp	LC529

LE4DF:	sta	$E0
	sta	$40
	cmp	#$27
	bne	LE4EB
	lda	#$0F
	bne	LE4ED
LE4EB:	lda	#$0E
LE4ED:	brk
	.byte	$04
	.byte	$17
	lda	#$00
	sta	$41
	asl	$40
	rol	$41
	asl	$40
	rol	$41
	asl	$40
	rol	$41
	asl	$40
	rol	$41
	lda	$E0
	pha
	lda	#$00
	sta	$E0
	brk
	.byte	$0C
	.byte	$17
	pla
	sta	$E0
	cmp	#$25
	bne	LE521
	lda	#$46
	sta	$0110
	lda	#$FA
	sta	$0111
	bne	LE526
LE521:	lda	#$FA
	sta	$0110
LE526:	jsr	LB6DA
	lda	$DF
	and	#$0F
	sta	$DF
	lda	$E0
	pha
	lda	#$00
	sta	$E0
	jsr	LF050
	pla
	sta	$E0
	asl	a
	tay
	ldx	#$22
	brk
	.byte	$8B
	.byte	$17
	lda	#$01
	jsr	LFD1C
	clc
	adc	#$00
	sta	$3C
	php
	iny
	lda	#$01
	jsr	LFD1C
	tay
	and	#$7F
	plp
	adc	#$80
	sta	$3D
	tya
	pha
	lda	$3C
	sta	$26
	pha
	lda	$3D
	sta	$27
	pha
	jsr	LE3CD
	pla
	sta	$3D
	pla
	sta	$3C
	pla
	and	#$80
	sta	$09
	lda	$8D
	ora	#$80
	sta	$8D
	jsr	LE44F
	jsr	LC6F0
	.byte	$02
	lda	$E0
	cmp	#$27
	bne	LE592
	jsr	LC7C5
	ora	$02AD, y
	ora	($D0, x)
	plp
LE592:	jsr	LDBE4
	jsr	LC7CB
	.byte	$E2
	lda	$0102
	sta	$3E
	jsr	LC55B
	lda	$95
	sta	$3C
	lda	#$00
	sta	$3D
	sta	$3F
	jsr	LC1C9
	lda	$41
	lsr	a
	lsr	a
	sta	$40
	lda	$0102
	sec
	sbc	$40
	sta	$E2
	jsr	LEFB7
	jsr	LEEC0
	bcs	LE5CE
	jsr	LDBE4
	jsr	LC7CB
	cpx	$4C
	.byte	$1B
	.byte	$EB
LE5CE:	jsr	LC6F0
	brk
	lda	$DF
	bpl	LE5EF
	jsr	LC55B
	lda	$95
	lsr	a
	bcs	LE5E5
	jsr	LC7C5
	.byte	$07
	jmp	LEB1B

LE5E5:	lda	$DF
	and	#$7F
	sta	$DF
	jsr	LC7C5
	php
LE5EF:	jsr	LC7CB
	inx
	jsr	LC6F0
	.byte	$04
	lda	$D7
	beq	LE5FE
	jmp	LE6B6

LE5FE:	lda	#$04
	jsr	LA7A2
	lda	#$89
	brk
	.byte	$04
	.byte	$17
	jsr	LC7CB
	sbc	$A5
	cpy	$4285
	lda	$0101
	sta	$43
	lda	$E0
	cmp	#$26
	beq	LE651
	cmp	#$27
	beq	LE651
	jsr	LC55B
	lda	$95
	and	#$1F
	bne	LE651
	lda	#$88
	brk
	.byte	$04
	.byte	$17
	jsr	LDBE4
	jsr	LC7C5
	.byte	$04
	jsr	LC55B
	lda	$95
	sta	$3C
	lda	$CC
	lsr	a
	sta	$3E
	lda	#$00
	sta	$3D
	sta	$3F
	jsr	LC1C9
	lda	$CC
	sec
	sbc	$41
	jmp	LE664

LE651:	jsr	LEFE5
	lda	$3C
	bne	LE664
	lda	#$8D
	brk
	.byte	$04
	.byte	$17
	jsr	LC7CB
	.byte	$E7
	jmp	LEB1B

LE664:	sta	$00
	lda	#$00
	sta	$01
	bit	$DF
	bvs	LE69A
	jsr	LC55B
	lda	$95
	and	#$3F
	sta	$95
	lda	$0105
	and	#$0F
	beq	LE69A
	sec
	sbc	#$01
	cmp	$95
	bcc	LE69A
	jsr	LDBE4
	lda	#$8D
	brk
	.byte	$04
	.byte	$17
	jsr	LC7C5
	asl	a
	jmp	LEB1B

LE694:	sta	$00
	lda	#$00
	sta	$01
LE69A:	lda	#$87
	brk
	.byte	$04
	.byte	$17
	lda	$9A22
	sta	$42
	lda	$9A23
	sta	$43
	jsr	LEF38
	jsr	LDBE4
	jsr	LC7CB
	inc	$4C
	.byte	$5D
	.byte	$E9
LE6B6:	cmp	#$02
	beq	LE6BD
	jmp	LE7A2

LE6BD:	lda	$CE
	sta	$3E
	lda	$CF
	and	#$03
	sta	$3F
	ora	$3E
	bne	LE6D7
	lda	#$04
	jsr	LA7A2
	jsr	LC7CB
	and	($4C), y
	.byte	$CE
	.byte	$E5
LE6D7:	jsr	LDB56
	cmp	#$FF
	bne	LE6E6
	lda	#$04
	jsr	LA7A2
	jmp	LE5CE

LE6E6:	pha
	lda	#$04
	jsr	LA7A2
	pla
	cmp	#$03
	beq	LE6FD
	cmp	#$07
	beq	LE6FD
	cmp	#$05
	beq	LE6FD
	cmp	#$06
	bne	LE704
LE6FD:	jsr	LC7CB
	sbc	#$4C
	.byte	$CE
	.byte	$E5
LE704:	jsr	LDB85
	cmp	#$32
	bne	LE711
	jsr	LC7BD
	jmp	LE5CE

LE711:	sta	$D7
	lda	$DF
	and	#$10
	beq	LE720
	jsr	LC7CB
	nop
	jmp	LEB1B

LE720:	lda	$D7
	cmp	#$00
	bne	LE72C
	jsr	LDBB8
	jmp	LEB1B

LE72C:	cmp	#$08
	bne	LE736
	jsr	LDBD7
	jmp	LEB1B

LE736:	cmp	#$01
	bne	LE751
	lda	$0105
	lsr	a
	lsr	a
	lsr	a
	lsr	a
	jsr	LE946
	jsr	LC55B
	lda	$95
	and	#$07
	clc
	adc	#$05
	jmp	LE694

LE751:	cmp	#$09
	bne	LE76C
	lda	$0105
	lsr	a
	lsr	a
	lsr	a
	lsr	a
	jsr	LE946
	jsr	LC55B
	lda	$95
	and	#$07
	clc
	adc	#$3A
	jmp	LE694

LE76C:	cmp	#$02
	bne	LE78A
	lda	$0104
	lsr	a
	lsr	a
	lsr	a
	lsr	a
	jsr	LE946
	jsr	LDBE4
	jsr	LC7CB
	cpx	LDFA5
	ora	#$40
	sta	$DF
	jmp	LEB3E

LE78A:	lda	$0104
	and	#$0F
	jsr	LE946
	jsr	LDBE4
	jsr	LC7CB
	sbc	LDFA5
	ora	#$20
	sta	$DF
	jmp	LEB1B

LE7A2:	cmp	#$03
	beq	LE7A9
	jmp	LE87F

LE7A9:	jsr	LDF77
	cpx	#$01
	bne	LE7BC
	lda	#$04
	jsr	LA7A2
	jsr	LC7CB
	and	LCE4C, x
	.byte	$E5
LE7BC:	jsr	LC6F0
	.byte	$07
	cmp	#$FF
	bne	LE7CC
	lda	#$04
	jsr	LA7A2
	jmp	LE5CE

LE7CC:	pha
	lda	#$04
	jsr	LA7A2
	pla
	tax
	lda	$A4, x
	cmp	#$02
	bne	LE7E6
	dec	$C0
	jsr	LC7CB
	.byte	$F7
	jsr	LDCFE
	jmp	LEB1B

LE7E6:	cmp	#$08
	bne	LE820
	jsr	LC7CB
	.byte	$3C
	lda	#$12
	brk
	.byte	$04
	.byte	$17
	brk
	.byte	$03
	.byte	$17
	lda	$E0
	cmp	#$27
	beq	LE801
	lda	#$18
	jmp	LE803

LE801:	lda	#$0F
LE803:	brk
	.byte	$04
	.byte	$17
	lda	$E0
	cmp	#$18
	bne	LE819
	jsr	LC7CB
	.byte	$F3
	lda	$DF
	ora	#$40
	sta	$DF
	jmp	LEB3E

LE819:	jsr	LC7CB
	.byte	$33
	jmp	LEB1B

LE820:	cmp	#$0D
	bne	LE84A
	jsr	LC7CB
	eor	($A9, x)
	ora	($00), y
	.byte	$04
	.byte	$17
	brk
	.byte	$03
	.byte	$17
	lda	$E0
	cmp	#$27
	beq	LE83B
	lda	#$18
	jmp	LE83D

LE83B:	lda	#$0F
LE83D:	brk
	.byte	$04
	.byte	$17
	jsr	LDBE4
	jsr	LC7CB
	.byte	$F4
	jmp	LEB1B

LE84A:	cmp	#$07
	bne	LE854
	jsr	LDFB9
	jmp	LEB1B

LE854:	cmp	#$09
	bne	LE85E
	jsr	LDFD1
	jmp	LEB1B

LE85E:	cmp	#$0C
	bne	LE86D
	jsr	LDFE7
	lda	#$18
	brk
	.byte	$04
	.byte	$17
	jmp	LEB1B

LE86D:	cmp	#$0E
	bne	LE87C
	jsr	LE00A
	lda	#$18
	brk
	.byte	$04
	.byte	$17
	jmp	LEB1B

LE87C:	jmp	LE6FD

LE87F:	cmp	#$01
	beq	LE886
	jmp	LE5EF

LE886:	lda	#$04
	jsr	LA7A2
	lda	#$83
	brk
	.byte	$04
	.byte	$17
	jsr	LC7CB
	sbc	$24, x
	.byte	$DF
	bvs	LE8A4
	jsr	LEE91
	bcs	LE8A4
	jsr	LC7CB
	inc	$4C, x
	.byte	$1B
	.byte	$EB
LE8A4:	ldx	$45
	lda	$B1AE, x
	brk
	.byte	$04
	.byte	$17
	lda	$E0
	cmp	#$27
	bne	LE8D4
	jsr	LFF74
	lda	$9A18
	sta	$0C
	lda	$9A19
	sta	$0D
	lda	#$00
	sta	$3C
	sta	$E0
	jsr	LC632
	jsr	LC63D
	jsr	LFF74
	jsr	LA788
	jmp	LB091

LE8D4:	lda	$45
	cmp	#$03
	bne	LE8FB
	lda	$3A
	cmp	#$12
	bne	LE8FB
	lda	$3B
	cmp	#$0C
	bne	LE8FB
	jsr	LC6BB
	dec	$3A
	dec	$8E
	lda	$90
	sec
	sbc	#$10
	sta	$90
	bcs	LE8F8
	dec	$91
LE8F8:	jmp	LB097

LE8FB:	lda	$45
	cmp	#$15
	bne	LE928
	lda	$3A
	cmp	#$04
	bne	LE928
	lda	$3B
	cmp	#$0E
	bne	LE928
	lda	$E4
	and	#$40
	bne	LE928
LE913:	jsr	LC6BB
	dec	$3B
	dec	$8F
	lda	$92
	sec
	sbc	#$10
	sta	$92
	bcs	LE925
	dec	$93
LE925:	jmp	LB097

LE928:	lda	$45
	cmp	#$01
	bne	LE940
	lda	$3A
	cmp	#$49
	bne	LE940
	lda	$3B
	cmp	#$64
	bne	LE940
	lda	$E4
	and	#$02
	beq	LE913
LE940:	jsr	LC6BB
	jmp	LEE5A

LE946:	sta	$3E
	jsr	LC55B
	lda	$95
	and	#$0F
	cmp	$3E
	bcc	LE954
	rts

LE954:	pla
	pla
	jsr	LC7CB
	.byte	$EB
	jmp	LEB1B

	lda	$E2
	sec
	sbc	$00
	sta	$E2
	bcc	LE96B
	beq	LE96B
	jmp	LEB1B

LE96B:	lda	$E0
	cmp	#$1E
	bne	LE97F
	lda	$45
	cmp	#$15
	bne	LE97F
	lda	$E4
	ora	#$40
	sta	$E4
	bne	LE98F
LE97F:	cmp	#$18
	bne	LE98F
	lda	$45
	cmp	#$01
	bne	LE98F
	lda	$E4
	ora	#$02
	sta	$E4
LE98F:	jsr	LDBE4
	jsr	LC7CB
	inc	$BB20
	dec	$A9
	ora	$0400, y
	.byte	$17
	lda	$E0
	cmp	#$26
	bne	LE9B1
	ldx	#$50
LE9A6:	jsr	LFF74
	dex
	bne	LE9A6
	lda	#$27
	jmp	LE4DF

LE9B1:	cmp	#$27
	bne	LEA0A
	sta	$600A
	lda	#$02
	sta	$602F
	lda	#$2C
	sta	$0A
	lda	#$21
	sta	$0B
	lda	#$5F
	sta	$08
	lda	#$06
	sta	$3C
LE9CD:	ldy	#$07
LE9CF:	jsr	LC690
	dey
	bne	LE9CF
	lda	$0A
	clc
	adc	#$19
	sta	$0A
	bcc	LE9E0
	inc	$0B
LE9E0:	dec	$3C
	bne	LE9CD
	jsr	LFF74
	jsr	LFF74
	lda	#$00
	sta	$E0
	jsr	LC7C5
	.byte	$1A
	lda	$E4
	ora	#$04
LE9F6:	sta	$E4
	jsr	LCFE4
	lda	$CA
	sta	$C5
	lda	$CB
	sta	$C6
	ldx	#$12
	lda	#$02
	jmp	LD9E2

LEA0A:	lda	$0106
	sta	$00
	lda	#$00
	sta	$01
	jsr	LC7CB
	.byte	$EF
	lda	$00
	clc
	adc	$BA
	sta	$BA
	bcc	LEA2A
	inc	$BB
	bne	LEA2A
	lda	#$FF
	sta	$BA
	sta	$BB
LEA2A:	lda	$0107
	sta	$3E
	jsr	LC55B
	lda	$95
	and	#$3F
	clc
	adc	#$C0
	sta	$3C
	lda	#$00
	sta	$3D
	sta	$3F
	jsr	LC1C9
	lda	$41
	sta	$00
	lda	#$00
	sta	$01
	jsr	LC7CB
	beq	LE9F6
	brk
	clc
	adc	$BC
	sta	$BC
	bcc	LEA63
	inc	$BD
	bne	LEA63
	lda	#$FF
	sta	$BC
	sta	$BD
LEA63:	jsr	LC6F0
	brk
	lda	$CB
	pha
	lda	$CA
	pha
	lda	$C9
	pha
	lda	$C8
	pha
	lda	$C7
	pha
	jsr	LF050
	pla
	cmp	$C7
	bne	LEA90
	brk
	.byte	$03
	.byte	$17
	ldx	$45
	lda	$B1AE, x
	brk
	.byte	$04
	.byte	$17
	pla
	pla
	pla
	pla
	jmp	LEE54

LEA90:	lda	#$1A
	brk
	.byte	$04
	.byte	$17
	brk
	.byte	$03
	.byte	$17
	ldx	$45
	lda	$B1AE, x
	brk
	.byte	$04
	.byte	$17
	jsr	LC7CB
	sbc	($A9), y
	brk
	sta	$01
	pla
	sta	$3C
	lda	$C8
	sec
	sbc	$3C
	beq	LEAB8
	sta	$00
	jsr	LC7C5
	.byte	$0E
LEAB8:	pla
	sta	$3C
	lda	$C9
	sec
	sbc	$3C
	beq	LEAC8
	sta	$00
	jsr	LC7C5
	.byte	$0F
LEAC8:	pla
	sta	$3C
	lda	$CA
	sec
	sbc	$3C
	sta	$00
	jsr	LC7C5
	bpl	LEB3F
	sta	$3C
	lda	$CB
	sec
	sbc	$3C
	beq	LEAE6
	sta	$00
	jsr	LC7C5
	.byte	$11
LEAE6:	lda	$C7
	cmp	#$03
	beq	LEB10
	cmp	#$04
	beq	LEB10
	cmp	#$07
	beq	LEB10
	cmp	#$09
	beq	LEB10
	cmp	#$0A
	beq	LEB10
	cmp	#$0C
	beq	LEB10
	cmp	#$0D
	beq	LEB10
	cmp	#$0F
	beq	LEB10
	cmp	#$11
	beq	LEB10
	cmp	#$13
	bne	LEB14
LEB10:	jsr	LC7CB
	.byte	$F2
LEB14:	jsr	LC6F0
	brk
	jmp	LEE54

LEB1B:	lda	$DF
	and	#$40
	beq	LEB48
LEB21:	jsr	LC55B
	lda	$95
	and	#$03
	beq	LEB21
	cmp	#$01
	bne	LEB3E
	lda	$DF
	and	#$BF
	sta	$DF
	jsr	LDBE4
	jsr	LC7CB
	brk
	jmp	LEB48

LEB3E:	.byte	$20
LEB3F:	cpx	$DB
	jsr	LC7CB
	sed
	jmp	LE5CE

LEB48:	jsr	LEFB7
	jsr	LC55B
	lda	$0103
	and	#$30
	sta	$3C
	lda	$95
	and	#$30
	cmp	$3C
	bcs	LEB94
	lda	$0103
	and	#$C0
	bne	LEB6B
	lda	$DF
	bmi	LEB94
	jmp	LEC92

LEB6B:	cmp	#$40
	bne	LEB78
	lda	$DF
	and	#$10
	bne	LEB94
	jmp	LEC69

LEB78:	cmp	#$80
	bne	LEB88
	lda	$0102
	lsr	a
	lsr	a
	cmp	$E2
	bcc	LEB94
	jmp	LECA6

LEB88:	lda	$0102
	lsr	a
	lsr	a
	cmp	$E2
	bcc	LEB94
	jmp	LECCE

LEB94:	jsr	LC55B
	lda	$0103
	and	#$03
	sta	$3C
	lda	$95
	and	#$03
	cmp	$3C
	bcs	LEBC1
	lda	$0103
	and	#$0C
	bne	LEBB0
	jmp	LEC23

LEBB0:	cmp	#$04
	bne	LEBB7
	jmp	LEC55

LEBB7:	cmp	#$08
	bne	LEBBE
	jmp	LECED

LEBBE:	jmp	LECE1

LEBC1:	lda	#$8C
	brk
	.byte	$04
	.byte	$17
	jsr	LDBE4
	jsr	LC7CB
	sbc	$AD, y
	ora	($85, x)
	.byte	$42
	lda	$CD
	sta	$43
	jsr	LEFF4
	lda	$3C
	bne	LEBE9
	lda	#$8E
	brk
	.byte	$04
	.byte	$17
	jsr	LC7CB
	.byte	$FB
	jmp	LE5CE

LEBE9:	sta	$00
	jmp	LED20

LEBEE:	jsr	LDBE4
	jsr	LC7CB
	.byte	$FC
	lda	$D7
	jsr	LDBF0
	jsr	LC7CB
	sbc	$91A9, x
	brk
	.byte	$04
	.byte	$17
	lda	$9A26
	sta	$42
	lda	$9A27
	sta	$43
	jsr	LEF38
	brk
	.byte	$03
	.byte	$17
	lda	$DF
	and	#$20
	bne	LEC1A
	rts

LEC1A:	jsr	LC7CB
	nop
	pla
	pla
	jmp	LE5CE

LEC23:	lda	#$11
	sta	$D7
	jsr	LEBEE
	jsr	LC55B
	lda	$95
	and	#$07
	clc
	adc	#$03
LEC34:	sta	$00
	lda	$BE
	and	#$1C
	cmp	#$1C
	beq	LEC42
	cmp	#$18
	bne	LEC52
LEC42:	lda	$00
	sta	$3C
	lda	#$03
	sta	$3E
	jsr	LC1F0
	lda	$3C
	asl	a
	sta	$00
LEC52:	jmp	LED20

LEC55:	lda	#$19
	sta	$D7
	jsr	LEBEE
	jsr	LC55B
	lda	$95
	and	#$0F
	clc
	adc	#$1E
	jmp	LEC34

LEC69:	lda	#$14
	sta	$D7
	jsr	LEBEE
	lda	$BE
	and	#$1C
	cmp	#$1C
	beq	LEC8E
	jsr	LC55B
	lda	$95
	lsr	a
	bcc	LEC8E
	lda	$DF
	ora	#$10
	sta	$DF
	lda	#$FE
LEC88:	jsr	LC7BD
	jmp	LE5CE

LEC8E:	lda	#$EB
	bne	LEC88
LEC92:	lda	#$12
	sta	$D7
	jsr	LEBEE
	lda	$DF
	ora	#$80
	sta	$DF
	jsr	LC7C5
	asl	$4C
	.byte	$DE
	.byte	$E5
LECA6:	lda	#$10
	sta	$D7
	jsr	LEBEE
	jsr	LC55B
	lda	$95
	and	#$07
	clc
	adc	#$14
LECB7:	clc
	adc	$E2
	cmp	$0102
	bcc	LECC2
	lda	$0102
LECC2:	sta	$E2
	jsr	LDBE4
	jsr	LC7C5
	ora	#$4C
	.byte	$CE
	.byte	$E5
LECCE:	lda	#$18
	sta	$D7
	jsr	LEBEE
	jsr	LC55B
	lda	$95
	and	#$0F
	clc
	adc	#$55
	bne	LECB7
LECE1:	jsr	LC55B
	lda	$95
	and	#$07
	clc
	adc	#$41
	bne	LECF6
LECED:	jsr	LC55B
	lda	$95
	and	#$07
	ora	#$10
LECF6:	sta	$00
	lda	#$00
	sta	$01
	lda	$BE
	and	#$1C
	cmp	#$1C
	bne	LED14
	lda	$00
	sta	$3C
	lda	#$03
	sta	$3E
	jsr	LC1F0
	lda	$3C
	asl	a
	sta	$00
LED14:	lda	#$95
	brk
	.byte	$04
	.byte	$17
	jsr	LDBE4
	jsr	LC7CB
	.byte	$FF
LED20:	lda	#$8A
	brk
	.byte	$04
	.byte	$17
	lda	#$00
	sta	$01
	lda	$C5
	sec
	sbc	$00
	bcs	LED32
	lda	#$00
LED32:	sta	$C5
	lda	#$08
	sta	$42
	lda	$05
	sta	$0F
	lda	$07
	sta	$10
LED40:	jsr	LFF74
	lda	$C5
	beq	LED4D
	jsr	LFF74
	jmp	LED56

LED4D:	lda	$E0
	cmp	#$27
	beq	LED56
	jsr	LEE14
LED56:	lda	$42
	and	#$01
	bne	LED66
	lda	$0F
	clc
	adc	#$02
	sta	$05
	jmp	LED6D

LED66:	lda	$10
	clc
	adc	#$02
	sta	$07
LED6D:	lda	$E0
	cmp	#$27
	bne	LED7B
	lda	$0F
	sta	$05
	lda	$10
	sta	$07
LED7B:	jsr	LFF74
	jsr	LEE28
	lda	$0F
	sta	$05
	lda	$10
	sta	$07
	dec	$42
	bne	LED40
	jsr	LC7CB
	.byte	$FA
	jsr	LC6F0
	brk
	lda	$C5
	beq	LED9C
	jmp	LE5CE

LED9C:	lda	#$14
	brk
	.byte	$04
	.byte	$17
	brk
	.byte	$03
	.byte	$17
	jmp	LEDB4

LEDA7:	lda	#$14
	brk
	.byte	$04
	.byte	$17
	brk
	.byte	$03
	.byte	$17
	lda	#$02
	jsr	LC703
LEDB4:	jsr	LC7CB
	ora	($A9, x)
	sei
	sta	$603A
	lda	#$00
	sta	$602F
LEDC2:	jsr	LC608
	lda	$47
	and	#$09
	beq	LEDC2
	lsr	$BD
	ror	$BC
	lda	$DF
	and	#$FE
	sta	$DF
	lda	$E0
	sta	$600A
	lda	#$00
	sta	$E0
	jsr	LCB47
	lda	$CF
	and	#$C0
	beq	LEDF2
	jsr	LC7C5
	.byte	$14
	ldx	#$0C
	lda	#$02
	jmp	LD9E2

LEDF2:	jsr	LC7C5
	ora	LC7A5
	cmp	#$1E
	beq	LEE03
	jsr	LF134
	jsr	LC7C5
	.byte	$12
LEE03:	jsr	LC7C5
	.byte	$13
	jsr	LCFE4
	lda	#$02
	jsr	LA7A2
	lda	#$00
	sta	$96
	rts

LEE14:	jsr	LFF74
	lda	$9A22
	sta	$0C
	lda	$9A23
	sta	$0D
	lda	#$00
	sta	$3C
	jmp	LC63D

LEE28:	jsr	LFF74
	lda	$E0
	cmp	#$27
	bne	LEE3E
	lda	LEF9D
	sta	$0C
	lda	LEF9E
	sta	$0D
	jmp	LEE4D

LEE3E:	lda	$9A1A
	clc
	adc	$16
	sta	$0C
	lda	$9A1B
	adc	#$00
	sta	$0D
LEE4D:	lda	#$00
	sta	$3C
	jmp	LC63D

LEE54:	jsr	LC6BB
	jsr	LCFE4
LEE5A:	jsr	LFF74
	lda	$9A24
	sta	$0C
	lda	$9A25
	sta	$0D
	lda	#$00
	sta	$3C
	jsr	LC632
	lda	$8D
	and	#$70
	beq	LEE76
	lda	#$FF
LEE76:	sta	$8D
	lda	#$02
	jsr	LA7A2
	lda	#$0B
	jsr	LA7A2
	lda	#$02
	sta	$602F
	jsr	LFF74
	jsr	LFCAD
	jsr	LB6DA
	rts

LEE91:	jsr	LC55B
	lda	$E0
	cmp	#$23
	bcc	LEE9F
	lda	$95
	jmp	LEEC7

LEE9F:	cmp	#$1E
	bcc	LEEAA
	lda	$95
	and	#$7F
	jmp	LEEC7

LEEAA:	cmp	#$14
	bcc	LEEC0
	lda	$95
	and	#$3F
	sta	$3E
	jsr	LC55B
	lda	$95
	and	#$1F
	adc	$3E
	jmp	LEEC7

LEEC0:	jsr	LC55B
	lda	$95
	and	#$3F
LEEC7:	sta	$3C
	lda	$0101
	sta	$3E
	lda	#$00
	sta	$3D
	sta	$3F
	jsr	LC1C9
	lda	$40
	sta	$42
	lda	$41
	sta	$43
	jsr	LC55B
	lda	$95
	sta	$3C
	lda	$C9
	sta	$3E
	lda	#$00
	sta	$3D
	sta	$3F
	jsr	LC1C9
	lda	$40
	sec
	sbc	$42
	lda	$41
	sbc	$43
	rts

LEEFD:	lda	$E0
	sta	$3C
	lda	#$0C
	sta	$3E
	lda	#$00
	sta	$3D
	sta	$3F
	jsr	LC1C9
	lda	$40
	clc
	adc	$9A2A
	sta	$3C
	lda	$41
	adc	$9A2B
	sta	$3D
	tya
	pha
	ldy	#$0B
LEF21:	lda	($3C), y
	sta	$03A0, y
	dey
	bpl	LEF21
	pla
	tay
	lda	#$03
	sta	$3F
	sta	$0D
	lda	#$A0
	sta	$3E
	sta	$0C
	rts

LEF38:	lda	#$05
	sta	$DE
LEF3C:	lda	$42
	sta	$0C
	lda	$43
	sta	$0D
	jsr	LFF74
	jsr	LFF74
	jsr	LFF74
	lda	#$00
	sta	$3C
	jsr	LC632
	lda	$E0
	cmp	#$27
	bne	LEF67
	lda	LEF9B
	sta	$0C
	lda	LEF9C
	sta	$0D
	jsr	LC63D
LEF67:	jsr	LFF74
	jsr	LFF74
	lda	$42
	pha
	lda	$43
	pha
	jsr	LEEFD
	pla
	sta	$43
	pla
	sta	$42
	lda	#$00
	sta	$3C
	jsr	LC632
	lda	$E0
	cmp	#$27
	bne	LEF96
	lda	LEF9D
	sta	$0C
	lda	LEF9E
	sta	$0D
	jsr	LC63D
LEF96:	dec	$DE
	bne	LEF3C
	rts

LEF9B:	.byte	$9F
LEF9C:	.byte	$EF
LEF9D:	.byte	$AB
LEF9E:	.byte	$EF
	.byte	$30
	.byte	$0E
	.byte	$30
	.byte	$16
	asl	$16, x
	asl	$16, x
	asl	$16, x
	asl	$16, x
	bmi	LEFBB
	bmi	LEFC6
	.byte	$15
	.byte	$30
	.byte	$21
	.byte	$22
	.byte	$27
	.byte	$0F
	.byte	$27
	.byte	$27
LEFB7:	lda	$C8
LEFB9:	lsr	a
	.byte	$CD
LEFBB:	brk
	ora	($90, x)
	and	$20
	.byte	$5B
	cmp	$A5
	sta	$29, x
	.byte	$03
LEFC6:	bne	LEFE4
	jsr	LC6BB
	lda	#$83
	brk
	.byte	$04
	.byte	$17
	jsr	LDBE4
	jsr	LC7CB
	.byte	$E3
	ldx	$45
	lda	$B1AE, x
	brk
	.byte	$04
	.byte	$17
	pla
	pla
	jmp	LEE54

LEFE4:	rts

LEFE5:	lsr	$43
	lda	$42
	sec
	sbc	$43
	bcc	LF026
	cmp	#$02
	bcs	LF030
	bcc	LF026
LEFF4:	lsr	$43
	lda	$42
	lsr	a
	sta	$3E
	inc	$3E
	lda	$42
	sec
	sbc	$43
	bcc	LF008
	cmp	$3E
	bcs	LF030
LF008:	jsr	LC55B
	lda	$95
	sta	$3C
	lda	#$00
	sta	$3D
	sta	$3F
	jsr	LC1C9
	lda	$41
	clc
	adc	#$02
	sta	$3C
	.byte	$A9
LF020:	.byte	$03
	sta	$3E
	jmp	LC1F0

LF026:	jsr	LC55B
	lda	$95
	and	#$01
	sta	$3C
	rts

LF030:	sta	$42
	sta	$3E
	inc	$3E
	jsr	LC55B
	lda	$95
	sta	$3C
	lda	#$00
	sta	$3D
	sta	$3F
	jsr	LC1C9
	lda	$41
	clc
	adc	$42
	ror	a
	lsr	a
	sta	$3C
	rts

LF050:	ldx	#$3A
	lda	#$1E
	sta	$C7
LF056:	lda	$BA
	sec
	sbc	LF35B, x
	lda	$BB
	sbc	LF35C, x
	bcs	LF069
	dec	$C7
	dex
	dex
	bne	LF056
LF069:	lda	$C7
	sec
	sbc	#$01
	asl	a
	sta	$3C
	asl	a
	clc
	adc	$3C
	sta	$3C
	lda	#$FF
	sta	$02
LF07B:	jsr	LFF74
	lda	$02
	bne	LF07B
	brk
	ora	$A217
	.byte	$04
	lda	#$00
LF089:	clc
	adc	a:$B4, x
	dex
	bne	LF089
	sta	$42
	and	#$03
	sta	$43
	lda	$42
	lsr	a
	lsr	a
	and	#$03
	sta	$42
	lda	$43
	lsr	a
	bcs	LF0AD
	lda	$C8
	jsr	LF10C
	sta	$C8
	jmp	LF0B6

LF0AD:	lda	$CB
	beq	LF0B6
	jsr	LF10C
	sta	$CB
LF0B6:	lda	$43
	and	#$02
	bne	LF0C6
	lda	$C9
	jsr	LF10C
	sta	$C9
	jmp	LF0CD

LF0C6:	lda	$CA
	jsr	LF10C
	sta	$CA
LF0CD:	lda	$BE
	lsr	a
	lsr	a
	lsr	a
	lsr	a
	lsr	a
	tax
	lda	$99CF, x
	clc
	adc	$C8
	sta	$CC
	lda	$C9
	lsr	a
	sta	$CD
	lda	$BE
	lsr	a
	lsr	a
	and	#$07
	tax
	lda	$99D7, x
	clc
	adc	$CD
	sta	$CD
	lda	$BE
	and	#$03
	tax
	lda	$99DF, x
	clc
	adc	$CD
	sta	$CD
	lda	$CF
	and	#$10
	beq	LF10B
	lda	$CD
	clc
	adc	#$02
	sta	$CD
LF10B:	rts

LF10C:	sta	$3C
	lda	#$09
	sta	$3E
	lda	#$00
	sta	$3D
	sta	$3F
	jsr	LC1C9
	lda	$40
	sta	$3C
	lda	$41
	sta	$3D
	lda	#$0A
	sta	$3E
	lda	#$00
	sta	$3F
	jsr	LC1F4
	lda	$3C
	clc
	adc	$42
	rts

LF134:	lda	$C7
	asl	a
	tax
	lda	LF35B, x
	sec
	sbc	$BA
	sta	$00
	lda	LF35C, x
	sbc	$BB
	sta	$01
	rts

LF148:	lda	#$01
	jsr	LC587
	jmp	LF9DF

LF150:	.byte	$55
LF151:	.byte	$F1
LF152:	.byte	$54
LF153:	sbc	($FA), y
	eor	$62, x
	.byte	$FA
	eor	($4E, x)
	rti

	.byte	$62
	.byte	$FA
	.byte	$FA
	rti

	.byte	$47
	sed
	.byte	$6B
	lsr	LFA4D
	.byte	$0C
	and	$14, x
	.byte	$1C
	sed
	.byte	$23
	.byte	$FA
	.byte	$2F
	.byte	$0C
	.byte	$13
	clc
	.byte	$FA
	asl	a
	sec
	ora	$4CFA, y
	sed
	rti

	lsr	a
	.byte	$22
	.byte	$23
	.byte	$0F
	.byte	$FA
	.byte	$1B
	ora	$1C0D, y
	ora	($33), y
	.byte	$FA
	.byte	$42
	eor	$4E42
	rti

	.byte	$FA
	and	($4D), y
	.byte	$43
	.byte	$47
	sed
	lsr	LFA43
	plp
	asl	$4F, x
	.byte	$FA
	ora	$F8, x
	and	LFA4F, y
	.byte	$1C
	sed
	asl	$0F, x
	rts

	.byte	$FA
	plp
	asl	$0F, x
	rts

	.byte	$FA
	.byte	$13
	sed
	.byte	$14
	sed
	.byte	$0B
	plp
	asl	$4F, x
	.byte	$FA
	brk
	.byte	$FA
	brk
	.byte	$FA
	bit	$FA
	ora	($FA, x)
	.byte	$02
	.byte	$FA
	.byte	$03
	.byte	$FA
	.byte	$04
	.byte	$FA
	ora	$FA
	asl	$FA
	.byte	$07
	.byte	$FA
	php
	.byte	$FA
	ora	#$FA
	asl	a
	.byte	$FA
	.byte	$44
	rol	$F8
	.byte	$43
	.byte	$FA
	ora	($FA, x)
	.byte	$02
	.byte	$FA
	.byte	$03
	.byte	$FA
	.byte	$04
	.byte	$FA
	ora	$FA
	asl	$FA
	.byte	$07
	.byte	$FA
	php
	.byte	$FA
	ora	#$FA
	asl	a
	.byte	$FA
	.byte	$0B
	.byte	$FA
	.byte	$0C
	.byte	$FA
	.byte	$0D
	.byte	$FA
LF1E8:	asl	$0FFA
	.byte	$FA
	bpl	LF1E8
	ora	($FA), y
	.byte	$3C
	.byte	$5F
	.byte	$5F
	and	$F8
	bpl	LF24F
	.byte	$FA
	.byte	$3C
	.byte	$5F
	.byte	$2F
	.byte	$34
	.byte	$0B
	cli
	.byte	$FA
	plp
	.byte	$13
	ora	$5F22, x
	.byte	$FA
	eor	($6B, x)
	.byte	$22
	.byte	$5F
	.byte	$1A
	rol	$24, x
	ora	($FA), y
	.byte	$12
	.byte	$FA
	asl	$FA, x
	.byte	$13
	.byte	$FA
	.byte	$17
	.byte	$FA
	ora	$FA, x
	.byte	$14
	.byte	$FA
	.byte	$1A
	.byte	$FA
	.byte	$22
	.byte	$FA
	.byte	$23
	.byte	$FA
	asl	$20FA, x
	.byte	$FA
	.byte	$1B
	.byte	$FA
	and	($FA, x)
	ora	$1CFA, x
	.byte	$FA
	.byte	$1F
	.byte	$FA
	ora	$0F, x
	ora	$FA, x
	ora	$F8, x
	.byte	$3A
	bit	LFA37
	asl	a
	asl	LFA19, x
	plp
	ora	LFAF8, y
	clc
	.byte	$33
	.byte	$0B
	ora	$F8, x
	.byte	$3B
	.byte	$0C
	.byte	$FA
	bit	$281C
	.byte	$17
	.byte	$37
	.byte	$0F
	sed
LF24F:	jmp	($EFA)

	.byte	$0F
	and	($0F, x)
	sed
	.byte	$5F
	ora	$2831, y
	.byte	$17
	.byte	$37
	.byte	$0F
	sed
	jmp	($1EFA)

	.byte	$1F
	.byte	$0F
	.byte	$FA
	ora	$33F8, x
	rol	$FA, x
	asl	$361F, x
	.byte	$FA
	.byte	$1C
	.byte	$1F
	.byte	$0B
	.byte	$33
	ora	$29FA, y
	.byte	$1B
	.byte	$12
	ora	$35FA, y
	ora	LFA15, y
	eor	($4E, x)
	rti

	.byte	$FA
	bit	$2B
	.byte	$FA
	.byte	$1C
	sed
	asl	$4F, x
	.byte	$FA
	asl	a
	.byte	$2B
	ora	$195F, x
	.byte	$0B
	.byte	$2F
	.byte	$0C
	.byte	$0F
	sed
	.byte	$5F
	asl	a
	and	$14, x
	.byte	$32
	.byte	$FA
	ora	$1216, y
	ora	$15F8, y
	.byte	$1C
	.byte	$FA
	.byte	$13
	sed
	.byte	$14
	sed
	.byte	$0B
	plp
	asl	$FA, x
	ora	($19), y
	sed
	.byte	$14
	.byte	$0B
	.byte	$FA
	.byte	$14
	plp
	.byte	$FA
	asl	$140C
	plp
	.byte	$FA
	.byte	$13
	.byte	$13
	.byte	$23
	.byte	$FA
	clc
	asl	LFA19, x
	asl	a
	bmi	LF2F6
	.byte	$33
	.byte	$FA
	.byte	$0F
	sed
	.byte	$2F
	.byte	$0B
	.byte	$52
	.byte	$FA
	ora	$0CF8, x
	.byte	$0F
	.byte	$FA
	rol	$150C
	and	$23FA, y
	asl	$F8, x
	.byte	$FA
	.byte	$14
	.byte	$0F
	sed
	asl	$FA, x
	asl	a
	.byte	$32
	bmi	LF2F5
	.byte	$0B
	.byte	$FA
	ora	$13F8, x
	.byte	$0F
	.byte	$1F
	.byte	$FA
	bit	$1B
	.byte	$2F
	.byte	$0C
	.byte	$FA
	asl	$0D28
	.byte	$FA
	asl	$3415
	.byte	$FA
LF2F5:	.byte	$25
LF2F6:	.byte	$32
	.byte	$0B
	.byte	$5F
	.byte	$0B
	.byte	$0B
	.byte	$1B
	ora	LFA0D, y
	plp
	.byte	$1A
	.byte	$FA
	plp
	bit	$1922
	.byte	$1A
	.byte	$FA
	asl	a
	and	($0F), y
LF30B:	sed
LF30C:	ora	LFA0C, x
	.byte	$0F
	sed
	.byte	$FA
	bpl	LF30C
	.byte	$FA
	ora	($F8), y
	.byte	$FA
	.byte	$12
	sed
	.byte	$FA
	.byte	$13
	sed
LF31D:	.byte	$FA
	.byte	$14
	sed
	.byte	$FA
	ora	$F8, x
	.byte	$FA
	asl	$F8, x
	.byte	$FA
	.byte	$17
	sed
	.byte	$FA
	clc
	sed
	.byte	$FA
	.byte	$19
	sed
LF32F:	.byte	$FA
	.byte	$1A
	sed
	.byte	$FA
	.byte	$1B
	sed
	.byte	$FA
	.byte	$1C
	sed
	.byte	$FA
	ora	LFAF8, x
	.byte	$23
	sed
	.byte	$FA
	.byte	$24
LF340:	sed
	.byte	$FA
	and	$F8
	.byte	$FA
	rol	$F8
	.byte	$FA
	.byte	$27
	sed
	.byte	$FA
	.byte	$23
	sbc	$24FA, y
	sbc	$25FA, y
LF352:	sbc	$26FA, y
	sbc	$27FA, y
	sbc	LFAFA, y
LF35B:	brk
LF35C:	brk
	.byte	$07
	brk
	.byte	$17
	brk
	.byte	$2F
	brk
LF363:	ror	LDC00
	brk
	.byte	$C2
	ora	($20, x)
	.byte	$03
	.byte	$14
	ora	$D0
	.byte	$07
	.byte	$54
	.byte	$0B
	ldy	#$0F
	.byte	$7C
	.byte	$15
LF375:	jmp	$101D

	.byte	$27
	iny
	.byte	$32
	.byte	$80
	rol	$4A38, x
	beq	LF3D6
	bcc	LF3E8
	bmi	LF3FA
	bne	LF30B
LF387:	bvs	LF31D
	bpl	LF32F
	bcs	LF340
	bvc	LF352
	beq	LF363
	bcc	LF375
	bmi	LF387
	.byte	$FF
	.byte	$FF
LF397:	ror	$68
	pha
	lsr	$44
	.byte	$64
	sty	$86
	dey
	txa
	ror	a
	lsr	a
	rol	a
	plp
	rol	$24
	.byte	$22
	.byte	$42
	.byte	$62
	.byte	$82
	ldx	#$A4
	ldx	$A8
	tax
	ldy	$6C8C
	jmp	$C2C

	asl	a
	php
	asl	$04
	.byte	$02
	brk
	jsr	$6040
	.byte	$80
	ldy	#$C0
	.byte	$C2
	cpy	$C6
	iny
	dex
	.byte	$CC
LF3C8:	.byte	$01
LF3C9:	.byte	$02
LF3CA:	.byte	$02
	ora	($51, x)
	ora	($01, x)
	pla
	asl	a
	.byte	$01
	.byte	$30
	and	#$01
	.byte	$2B
LF3D6:	.byte	$2B
	ora	($68, x)
	bit	$3001
	bmi	LF3DF
	pla
LF3DF:	and	($01), y
	ora	$0139, x
	ror	$48
	ora	($19, x)
LF3E8:	eor	$4901, y
	ror	$01
	jmp	($16D)

	.byte	$1C
	.byte	$0C
	.byte	$02
	asl	a
	ora	($02, x)
	.byte	$04
	asl	$0F02
LF3FA:	asl	$1D04
	ora	$0805, x
	php
	ora	#$13
	brk
	.byte	$0F
	.byte	$0F
	ora	($0F, x)
	ora	$0F07
	.byte	$13
	.byte	$07
	.byte	$0F
	asl	$0F09
	.byte	$02
	asl	$020F
	.byte	$04
	.byte	$0F
	php
	.byte	$13
	bpl	LF41E
	brk
	bpl	LF427
LF41E:	ora	($10, x)
	brk
	php
	bpl	LF425
	.byte	$09
LF425:	ora	($01), y
LF427:	asl	$11
	.byte	$07
	.byte	$07
	.byte	$12
	.byte	$02
	.byte	$02
	.byte	$12
	php
	ora	($13, x)
	ora	$05
	.byte	$13
	brk
	brk
	.byte	$14
	ora	#$00
	.byte	$14
	ora	#$06
	asl	$00, x
	brk
	asl	$06, x
	ora	$16
	.byte	$0C
	.byte	$0C
	clc
	ora	($12, x)
	ora	$0101, y
	ora	$010C, y
	ora	$0605, y
	ora	$0A01, y
	ora	$0A0C, y
	.byte	$1A
	ora	#$05
	.byte	$1A
	asl	a
	ora	#$1C
	ora	#$09
LF461:	.byte	$09
LF462:	brk
LF463:	asl	$040D
	ora	#$07
	.byte	$13
	.byte	$17
	php
	brk
	.byte	$0F
	.byte	$04
	.byte	$0B
	ora	a:$15, x
	brk
	.byte	$02
	asl	a
	.byte	$13
	ora	$00, x
	ora	a:$16, x
	.byte	$07
	.byte	$0B
	ora	$030E, x
	brk
	asl	a
	asl	a
	.byte	$0F
	brk
	asl	$0400
	.byte	$1C
	brk
	brk
	.byte	$0F
	ora	#$00
	.byte	$0F
	php
	ora	$110F
	.byte	$0F
	.byte	$0C
	brk
	.byte	$04
	.byte	$04
	.byte	$07
	.byte	$07
	clc
	asl	$0B
	bpl	LF4A7
	brk
	bpl	LF4A6
	.byte	$04
	bpl	LF4AE
	php
LF4A6:	.byte	$10
LF4A7:	php
	ora	#$10
	brk
	ora	($10, x)
	brk
LF4AE:	brk
	bpl	LF4B6
	brk
	ora	($07), y
	brk
	.byte	$11
LF4B6:	.byte	$02
	.byte	$02
	ora	($05), y
	.byte	$04
	ora	($00), y
	ora	#$12
	brk
	ora	#$12
	.byte	$07
	.byte	$07
	.byte	$13
	ora	#$00
	.byte	$13
	.byte	$04
	brk
	.byte	$14
	brk
	brk
	.byte	$14
	brk
	asl	$14
	brk
	brk
	asl	$0A
	ora	a:$17, x
	brk
	.byte	$17
	asl	$05
	.byte	$17
	.byte	$0C
	.byte	$0C
	ora	$020B, y
	.byte	$1A
	asl	$1A01
	.byte	$12
	ora	($1A, x)
	asl	$0B
	.byte	$1A
	.byte	$02
	ora	($1A), y
	.byte	$12
	ora	a:$1B
	.byte	$04
	.byte	$1B
	ora	$04
	ora	$908, x
LF4FA:	ora	$07
	ora	#$0B
	.byte	$0B
	asl	$1412
	.byte	$12
	clc
	asl	$1C, x
	.byte	$1C
	bit	$28
	bit	$280A
	.byte	$32
	.byte	$2F
	.byte	$34
	sec
	.byte	$3C
	.byte	$44
	sei
	bmi	LF561
	lsr	$564F
	cli
	lsr	$50, x
	lsr	$6462, x
	adc	#$78
	.byte	$5A
	.byte	$8C
LF522:	.byte	$33
	.byte	$22
	and	$45, x
	.byte	$32
	.byte	$12
	.byte	$33
	eor	$41
	brk
	.byte	$23
	eor	$51
	.byte	$1C
	ror	$66
	eor	$4C, x
	.byte	$97
	.byte	$77
	lda	#$8C
	cpy	$AA87
	ldy	$98DD, x
	.byte	$BB
	cmp	$99DC
LF542:	bpl	LF555
	ora	($11), y
	.byte	$12
	.byte	$12
	.byte	$13
	.byte	$13
	asl	$070E
	.byte	$0F
	.byte	$0F
LF54F:	brk
	ora	($00, x)
	ora	($00, x)
	.byte	$01
LF555:	brk
	ora	($02, x)
	ora	($00, x)
	.byte	$03
	.byte	$02
	.byte	$03
	ora	($01, x)
	ora	($02, x)
LF561:	.byte	$03
	.byte	$04
	.byte	$03
	.byte	$04
	ora	$05
	asl	$03
	.byte	$04
	ora	$06
	.byte	$0B
	ora	$06
	.byte	$0B
	.byte	$0C
	asl	$0C0B
	ora	$0E0E
	ora	$120F
	.byte	$12
	ora	$150F, y
	.byte	$12
	ora	$19, x
	ora	$16, x
	.byte	$17
	.byte	$1A
	.byte	$1C
	.byte	$17
	.byte	$1A
	.byte	$1B
	.byte	$1C
	bpl	LF5A6
	.byte	$1B
	.byte	$1C
	ora	$1D1F, x
	asl	$1F1F, x
	jsr	$908
	asl	a
	.byte	$0B
	.byte	$0C
	ora	($12), y
	.byte	$13
	.byte	$14
LF59E:	.byte	$17
	ora	$1F1E, x
	jsr	$2021
	.byte	$21
LF5A6:	.byte	$22
	.byte	$22
	.byte	$23
	jsr	$2423
	bit	$25
	.byte	$03
	.byte	$04
	asl	$07
	.byte	$07
	lda	$F5, x
	ldy	#$A0
	ldy	#$A0
	.byte	$02
	lda	($A1, x)
	lda	($A1, x)
	.byte	$03
	ldx	#$A3
	ldy	$A5
	.byte	$03
	.byte	$73
	.byte	$74
	adc	$76, x
	ora	($77, x)
	.byte	$77
	.byte	$77
	.byte	$77
	ora	($7B, x)
	.byte	$7C
	adc	$017E, x
	sta	($81, x)
	sta	($81, x)
	.byte	$02
	.byte	$82
	.byte	$83
	sty	$85
	brk
	stx	$87
	dey
	.byte	$89
	ora	($8A, x)
	.byte	$8B
	sty	a:$8D
	.byte	$9B
	.byte	$9C
	sta	a:$9E, x
	.byte	$7F
	.byte	$7F
	.byte	$80
	.byte	$80
	.byte	$02
	sed
	.byte	$FA
	sbc	$01FB, y
	stx	$8E8E
	stx	LF703
	.byte	$F3
	adc	$026E
LF600:	inc	$A8, x
	tay
	inc	$00, x
	.byte	$6F
	bvs	LF679
	.byte	$72
	ora	($8F, x)
	bcc	LF59E
	.byte	$92
	ora	($A6, x)
	.byte	$A7
	.byte	$F4
	sbc	$01, x
	.byte	$93
	sty	$95, x
	stx	$01, y
	.byte	$97
	tya
	sta	$019A, y
	.byte	$9F
	.byte	$9F
	.byte	$9F
	.byte	$9F
	ora	($5F, x)
	.byte	$5F
	.byte	$5F
	.byte	$5F
	ora	($A9, x)
	.byte	$AB
	tax
	ldy	$4601
	lsr	a
	tay
	inc	$00, x
	.byte	$5A
	tay
	.byte	$5B
	inc	$00, x
	lsr	$5B4A, x
	inc	$00, x
	inc	$5C, x
	tay
	eor	$4600, x
	ror	a
	tay
	eor	$5A00, x
	.byte	$5C
	.byte	$5B
	eor	$5E00, x
	ror	a
	.byte	$5B
	eor	$F600, x
	tay
	cli
	eor	$4600, y
	lsr	a
	cli
	eor	$5A00, y
	tay
	.byte	$6B
	eor	$5E00, y
	lsr	a
	.byte	$6B
	eor	$F600, y
	.byte	$5C
	cli
	jmp	($4600)

	ror	a
	cli
	jmp	($5A00)

	.byte	$5C
	.byte	$6B
	jmp	($5E00)

	ror	a
	.byte	$6B
	.byte	$6C
	brk
LF678:	.byte	$20
LF679:	.byte	$42
	sed
	ldx	$6039
	lda	$6045, x
	sta	$603A
	rts
; ------------------------------------------------------------
	.export	LF685
LF685:
	lda	$6030
	jsr	LFAC1
	jsr	LC156
	rts
; ------------------------------------------------------------
LF68F:	lda	#$00
	sta	$BA
	sta	$BB
	sta	$BC
	sta	$BD
	sta	$C1
	sta	$C2
	sta	$C3
	sta	$C4
	sta	$BF
	sta	$C0
	sta	$BE
	sta	$CF
	sta	$DF
	sta	$E4
	sta	$C5
	sta	$C6
	ldx	$6039
	lda	#$78
	sta	$6045, x
	rts

LF6BA:	lda	$6031
	jsr	LFC00
	lda	$22
	sta	$3E
	lda	$23
	sta	$3F
	lda	$6039
	jsr	LFC00
	ldy	#$00
LF6D0:	lda	($22), y
	sta	($3E), y
	dey
	bne	LF6D0
	ldx	$23
	inx
	stx	$23
	ldx	$3F
	inx
	stx	$3F
LF6E1:	lda	($22), y
	sta	($3E), y
	iny
	cpy	#$40
	bne	LF6E1
	lda	$6031
	sta	$6039
	jsr	LF7DA
	rts

LF6F4:	ldx	#$09
LF6F6:	lda	$6428, x
	cmp	LFC76, x
	bne	LF704
	dex
	bpl	LF6F6
	.byte	$4C
	.byte	$53
LF703:	.byte	$F7
LF704:	ldx	#$09
LF706:	lda	$603B, x
	cmp	LFC76, x
	bne	LF713
	dex
	bpl	LF706
	bmi	LF753
LF713:	inc	$6435
	lda	#$00
	ldx	#$00
LF71A:	sta	$6068, x
	sta	$61A8, x
	sta	$62E8, x
	dex
	bne	LF71A
	ldx	#$00
LF728:	sta	$6168, x
	sta	$62A8, x
	sta	$63E8, x
	inx
	cpx	#$40
	bcc	LF728
	lda	#$00
	sta	$6035
	sta	$6036
	sta	$6037
	sta	$6045
	sta	$6046
	sta	$6047
	sta	$6432
	sta	$6433
	sta	$6434
LF753:	ldx	#$09
LF755:	lda	LFC76, x
	sta	$6428, x
	sta	$603B, x
	dex
	bpl	LF755
	lda	#$00
	sta	$6038
	jsr	LF7B5
	lda	$6038
	sta	$6031
	lda	#$02
	sta	$6032
	lda	#$00
	sta	$6039
LF779:	lsr	$6031
	bcc	LF7AC
	lda	$6039
	jsr	LFAC1
	cmp	#$00
	beq	LF7AC
	lda	$6039
	jsr	LF80F
	jsr	LC6F0
	.byte	$02
	lda	$6039
	clc
	adc	#$01
	sta	$00
	lda	#$00
	sta	$01
	jsr	LC7C5
	and	#$A9
	asl	$7020, x
	cmp	($A9, x)
	.byte	$02
	brk
	ora	$07
LF7AC:	inc	$6039
	dec	$6032
	bpl	LF779
	rts

LF7B5:	txa
	pha
	lda	#$00
	ldx	$6035
	cpx	#$C8
	bne	LF7C2
	ora	#$01
LF7C2:	ldx	$6036
	cpx	#$C8
	bne	LF7CB
	ora	#$02
LF7CB:	ldx	$6037
	cpx	#$C8
	bne	LF7D4
	ora	#$04
LF7D4:	sta	$6038
	pla
	tax
	rts

LF7DA:	pha
	txa
	pha
	lda	$6038
	ldx	$6039
	beq	LF7FF
	cpx	#$01
	beq	LF7F5
	cpx	#$02
	ora	#$04
	ldx	#$C8
	stx	$6037
	jmp	LF806

LF7F5:	ora	#$02
	ldx	#$C8
	stx	$6036
	jmp	LF806

LF7FF:	ora	#$01
	ldx	#$C8
	stx	$6035
LF806:	and	#$07
	sta	$6038
	pla
	tax
	pla
	rts

LF80F:	pha
	txa
	pha
	lda	$6038
	ldx	$6039
	beq	LF834
	cpx	#$01
	beq	LF82A
	cpx	#$02
	and	#$03
	ldx	#$00
	stx	$6037
	jmp	LF83B

LF82A:	and	#$05
	ldx	#$00
	stx	$6036
	jmp	LF83B

LF834:	and	#$06
	ldx	#$00
	stx	$6035
LF83B:	sta	$6038
	pla
	tax
	pla
	rts

	jsr	LFC4D
	jsr	LC6BB
	jsr	LC17A
LF84B:	lda	#$18
	sta	$2001
	jsr	LFF74
	brk
	ora	($07, x)
	jsr	LF6F4
	lda	$6038
	and	#$07
	beq	LF8A4
	cmp	#$07
	bne	LF87D
LF864:	jsr	LC6F0
	.byte	$0F
	cmp	#$00
	bne	LF86F
	jmp	LF8AF

LF86F:	cmp	#$01
	bne	LF876
	jmp	LF8EC

LF876:	cmp	#$02
	bne	LF864
	jmp	LF911

LF87D:	jsr	LC6F0
	bpl	LF84B
	brk
	bne	LF888
	jmp	LF8AF

LF888:	cmp	#$01
	bne	LF88F
	jmp	LF8EC

LF88F:	cmp	#$02
	bne	LF896
	jmp	LF8C2

LF896:	cmp	#$03
	bne	LF89D
	jmp	LF93B

LF89D:	cmp	#$04
	bne	LF87D
	jmp	LF911

LF8A4:	jsr	LC6F0
	ora	($C9), y
	brk
	bne	LF8A4
	jmp	LF8C2

LF8AF:	lda	#$00
	jsr	LF99F
	cmp	#$FF
	bne	LF8BB
	jmp	LF96A

LF8BB:	sta	$6039
	jsr	LFAC1
	rts

LF8C2:	lda	#$FF
	jsr	LF983
	cmp	#$FF
	bne	LF8CE
	jmp	LF96A

LF8CE:	sta	$6039
	brk
	ora	($17), y
	lda	#$01
	sta	$E5
	jsr	LC6F0
	.byte	$0C
	cmp	#$FF
	bne	LF8E3
	jmp	LF96A

LF8E3:	sta	$E5
	jsr	LF68F
	jsr	LF9DF
	rts

LF8EC:	lda	#$00
	jsr	LF99F
	cmp	#$FF
	bne	LF8F8
	jmp	LF96A

LF8F8:	sta	$6039
	jsr	LFAC1
	jsr	LC6F0
	.byte	$0C
	cmp	#$FF
	bne	LF909
	jmp	LF96A

LF909:	sta	$E5
	jsr	LF9DF
	jmp	LF96A

LF911:	lda	#$00
	jsr	LF99F
	cmp	#$FF
	bne	LF91D
	jmp	LF96A

LF91D:	sta	$6039
	sta	$6030
	jsr	LC6F0
	jsr	LF020
	dec	$21
	cmp	#$00
	beq	LF932
	jmp	LF96A

LF932:	lda	$6039
	jsr	LF80F
	jmp	LF96A

LF93B:	lda	#$00
	jsr	LF99F
	cmp	#$FF
	bne	LF947
	jmp	LF96A

LF947:	sta	$6039
	lda	#$FF
	jsr	LF983
	cmp	#$FF
	bne	LF956
	jmp	LF96A

LF956:	sta	$6031
	jsr	LC6F0
	and	($C9, x)
	brk
	beq	LF964
	jmp	LF96A

LF964:	jsr	LF6BA
	jmp	LF96A

LF96A:	lda	#$FF
	brk
	ora	$07
	lda	$6038
	and	#$07
	beq	LF980
	cmp	#$07
	bne	LF97D
	jmp	LF864

LF97D:	jmp	LF87D

LF980:	jmp	LF8A4

LF983:	eor	$6038
	and	#$07
	beq	LF997
	sta	$6034
	clc
	adc	#$11
	brk
	bpl	LF9AA
	cmp	#$FF
	bne	LF99C
LF997:	pla
	pla
	jmp	LF96A

LF99C:	jmp	LF9BB

LF99F:	eor	$6038
	and	#$07
	beq	LF9B3
	sta	$6034
	clc
LF9AA:	adc	#$18
	brk
	bpl	LF9C6
	cmp	#$FF
	bne	LF9B8
LF9B3:	pla
	pla
	jmp	LF96A

LF9B8:	jmp	LF9BB

LF9BB:	ldx	$6034
	cpx	#$02
	bne	LF9C5
	lda	#$01
	rts

LF9C5:	.byte	$E0
LF9C6:	.byte	$04
	bne	LF9CC
	lda	#$02
	rts

LF9CC:	cpx	#$05
	bne	LF9D7
	cmp	#$01
	bne	LF9DE
	lda	#$02
	rts

LF9D7:	cpx	#$06
	bne	LF9DE
	clc
	adc	#$01
LF9DE:	rts

LF9DF:	pha
	txa
	pha
	tya
	pha
	lda	$6039
	and	#$07
	sta	$6039
	jsr	LF7DA
	lda	LFC72
	sta	$22
	lda	LFC73
	sta	$23
	jsr	LFA18
	jsr	LFBE0
	lda	LFC72
	sta	$3C
	lda	LFC73
	sta	$3D
	lda	$6039
LFA0C:	.byte	$20
LFA0D:	brk
	.byte	$FC
	jsr	LFAA3
	pla
	tay
	pla
LFA15:	tax
	pla
	rts

LFA18:	.byte	$A0
LFA19:	brk
	lda	$BA
	sta	($22), y
	iny
	lda	$BB
	sta	($22), y
	iny
	lda	$BC
	sta	($22), y
	iny
	lda	$BD
	sta	($22), y
	iny
	lda	$C1
	sta	($22), y
	iny
	lda	$C2
	sta	($22), y
LFA37:	iny
	lda	$C3
	sta	($22), y
	iny
	lda	$C4
	sta	($22), y
	iny
	.byte	$A5
LFA43:	.byte	$BF
	and	#$0F
	cmp	#$07
	bcc	LFA4C
	lda	#$06
LFA4C:	.byte	$91
LFA4D:	.byte	$22
	iny
LFA4F:	lda	$C0
	sta	($22), y
	iny
	lda	$BE
	sta	($22), y
	iny
	lda	$CF
	sta	($22), y
	iny
	lda	$DF
	sta	($22), y
	iny
	lda	$E4
	sta	($22), y
	iny
	ldx	#$03
LFA6A:	lda	$B5, x
	sta	($22), y
	iny
	dex
	bpl	LFA6A
	ldx	#$03
LFA74:	lda	$64C6, x
	sta	($22), y
	iny
	dex
	bpl	LFA74
	lda	$E5
	sta	($22), y
	iny
	lda	$C5
	sta	($22), y
	iny
	lda	$C6
	sta	($22), y
	ldx	$6039
	lda	$6045, x
	iny
	sta	($22), y
	lda	#$C8
	iny
	sta	($22), y
	iny
	sta	($22), y
	iny
	sta	($22), y
	iny
	sta	($22), y
	rts

LFAA3:	ldx	#$09
LFAA5:	jsr	LFAB7
	lda	$22
	clc
	adc	#$20
	sta	$22
	bcc	LFAB3
	inc	$23
LFAB3:	dex
	bpl	LFAA5
	rts

LFAB7:	ldy	#$1F
LFAB9:	lda	($3C), y
	sta	($22), y
	dey
	bpl	LFAB9
	rts

LFAC1:	sta	$3E
	txa
	pha
	tya
	pha
	lda	$3E
	and	#$07
	sta	$3E
	jsr	LFC00
	ldx	#$0A
LFAD2:	txa
	pha
	jsr	LFB4A
	bcs	LFAF2
	jsr	LFB6B
	lda	LFC72
	sta	$3C
	lda	LFC73
	sta	$3D
	jsr	LFB40
	jsr	LFB15
	pla
	lda	#$00
	jmp	LFB0C

LFAF2:	txa
	ldx	$6039
	.byte	$FE
	.byte	$32
LFAF8:	.byte	$64
	tax
LFAFA:	lda	$22
	clc
	adc	#$20
	sta	$22
	bcc	LFB05
	inc	$23
LFB05:	pla
	tax
	dex
	bne	LFAD2
	lda	#$FF
LFB0C:	sta	$3E
	pla
	tay
	pla
	tax
	lda	$3E
	rts

LFB15:	lda	$3E
	jsr	LFC00
	lda	$22
	sta	$3C
	lda	$23
	sta	$3D
	lda	LFC72
	sta	$22
	lda	LFC73
	sta	$23
	ldx	#$0A
LFB2E:	jsr	LFB40
	lda	$3C
	clc
	adc	#$20
	sta	$3C
	bcc	LFB3C
	inc	$3D
LFB3C:	dex
	bne	LFB2E
	rts

LFB40:	ldy	#$1F
LFB42:	lda	($22), y
	sta	($3C), y
	dey
	bpl	LFB42
	rts

LFB4A:	ldy	#$1E
	lda	($22), y
	sta	$40
	iny
	lda	($22), y
	sta	$41
	jsr	LFBE0
	ldy	#$1E
	lda	$40
	cmp	($22), y
	bne	LFB67
	iny
	lda	$41
	cmp	($22), y
	beq	LFB69
LFB67:	sec
	rts

LFB69:	clc
	rts

LFB6B:	ldy	#$00
	lda	($22), y
	sta	$BA
	iny
	lda	($22), y
	sta	$BB
	iny
	lda	($22), y
	sta	$BC
	iny
	lda	($22), y
	sta	$BD
	iny
	lda	($22), y
	sta	$C1
	iny
	lda	($22), y
	sta	$C2
	iny
	lda	($22), y
	sta	$C3
	iny
	lda	($22), y
	sta	$C4
	iny
	lda	($22), y
	sta	$BF
	iny
	lda	($22), y
	sta	$C0
	iny
	lda	($22), y
	sta	$BE
	iny
	lda	($22), y
	sta	$CF
	iny
	lda	($22), y
	sta	$DF
	iny
	lda	($22), y
	sta	$E4
	iny
	ldx	#$03
LFBB5:	lda	($22), y
	sta	$B5, x
	iny
	dex
	bpl	LFBB5
	ldx	#$03
LFBBF:	lda	($22), y
	sta	$64C6, x
	iny
	dex
	bpl	LFBBF
	lda	($22), y
	sta	$E5
	iny
	lda	($22), y
	sta	$C5
	iny
	lda	($22), y
	sta	$C6
	iny
	lda	($22), y
	ldx	$6039
	sta	$6045, x
	rts

LFBE0:	jsr	LFBEF
	ldy	#$1E
	lda	$94
	sta	($22), y
	iny
	lda	$95
	sta	($22), y
	rts

LFBEF:	ldy	#$1D
	sty	$94
	sty	$95
LFBF5:	lda	($22), y
	sta	$3C
	jsr	LFC2A
	dey
	bpl	LFBF5
	rts

LFC00:	sta	$22
	txa
	pha
	lda	$22
	ldx	LFC74
	stx	$22
	ldx	LFC75
	stx	$23
	tax
	beq	LFC19
LFC13:	jsr	LFC1C
	dex
	bne	LFC13
LFC19:	pla
	tax
	rts

LFC1C:	lda	$22
	clc
	adc	#$40
	sta	$22
	lda	$23
	adc	#$01
	sta	$23
	rts

LFC2A:	tya
	pha
	ldy	#$08
LFC2E:	lda	$95
	eor	$3C
	asl	$94
	rol	$95
	asl	$3C
	asl	a
	bcc	LFC47
	lda	$94
	eor	#$21
	sta	$94
	lda	$95
	eor	#$10
	sta	$95
LFC47:	dey
	bne	LFC2E
	pla
	tay
	rts

LFC4D:	lda	#$5F
	ldx	#$00
LFC51:	sta	$0400, x
	dex
	bne	LFC51
	ldx	#$00
LFC59:	sta	$0500, x
	dex
	bne	LFC59
	ldx	#$00
LFC61:	sta	$0600, x
	dex
	bne	LFC61
	ldx	#$00
LFC69:	sta	$0700, x
	inx
	cpx	#$C0
	bcc	LFC69
	rts

LFC72:	pha
LFC73:	rts

LFC74:	pla
LFC75:	rts

LFC76:	.byte	$4B
	eor	$4E
	jsr	$414D
	.byte	$53
	eor	$54, x
	.byte	$41
LFC80:	pha
	lda	#$03
	jsr	LFF96
	pla
	rts

LFC88:	pha
	lda	#$13
	sta	$6004
	jsr	LFF96
	lda	#$08
	sta	$2000
	pla
	rts
; ------------------------------------------------------------
	.export	LFC98, LFCA3, LFCA8, LFCAD
LFC98:
	pha
	lda	#$01
; --------------
LFC9B:
	sta	$6002
	jsr	LFFAC
	pla
	rts
; --------------
LFCA3:
	pha
	lda	#$00
	beq	LFC9B
; --------------
LFCA8:
	pha
	lda	#$00
	beq	LFCB0
; --------------
LFCAD:
	pha
	lda	#$02
; --------------
LFCB0:
	sta	$6003
	jsr	LFFC2
	pla
	rts
; --------------
LFCB8:
	pha
	lda	#$03
	bne	LFCB0
; ------------------------------------------------------------
LFCBD:
	sta	$37
	stx	$38
	lda	$6004
	pha
	php
	lda	$6004
	sta	$6006
	jsr	LFCEC
	lda	#$4C
	sta	$30
	ldx	$38
	lda	$37
	plp
	jsr	$30
	php
	sta	$37
	pla
	sta	$30
	pla
	jsr	LFF91
	lda	$30
	pha
	lda	$37
	plp
	rts

LFCEC:	lda	$30
	jsr	LFF91
	lda	$31
	asl	a
	tax
	lda	$8000, x
	sta	$31
	lda	$8001, x
	sta	$32
	rts
; ------------------------------------------------------------
	.export	LFD00
LFD00:
	sta	$37
	stx	$38
	lda	$6004
	pha
	jsr	LFCEC
	pla
	jsr	LFF91
	ldx	$38
	lda	$31
	sta	$00, x
	lda	$32
	sta	$01, x
	lda	$37
	rts
; ------------------------------------------------------------
	.export	LFD1C
LFD1C:
	sta	$37
	lda	$6004
	pha
	lda	$37
	jsr	LFF91
	lda	$00, x
	sta	$30
	lda	$01, x
	sta	$31
	lda	($30), y
	sta	$37
	pla
	jsr	LFF91
	lda	$37
	rts
; -----------------------------------------------------------
	.export	apu_irq_enter
apu_irq_enter:
	sei
	php
	bit	$4015
	sta	$37
	stx	$38
	sty	$39
	tsx
	lda	$0103, x
	sec
	sbc	#$01
	sta	$33
	lda	$0104, x
	sbc	#$00
	sta	$34
	ldy	#$01
	lda	($33), y
	pha
	and	#$08
	cmp	#$08
	pla
	ror	a
	lsr	a
	lsr	a
	lsr	a
	sta	$30
	dey
	lda	($33), y
	bmi	LFD77
	sta	$31
	ldy	$39
	ldx	$38
	plp
	pla
	lda	$37
	jmp	LFCBD

LFD77:	and	#$3F
	sta	$31
	ldy	$39
	ldx	$38
	plp
	pla
	lda	$37
	jmp	LFD00

start:	cld
	lda	#$10
	sta	$2000
LFD8C:	lda	$2002
	bmi	LFD8C
LFD91:	lda	$2002
	bpl	LFD91
LFD96:	lda	$2002
	bmi	LFD96
	lda	#$00
	sta	$2001
	ldx	#$FF
	txs
	tax
	sta	$602C
LFDA7:	sta	$00, x
	sta	$0300, x
	sta	$0400, x
	sta	$0500, x
	sta	$0600, x
	sta	$0700, x
	inx
	bne	LFDA7
	jsr	LFC80
	sta	$6004
	lda	#$1E
	sta	$6001
	lda	#$00
	sta	$6002
	sta	$6003
	jsr	LFDF4
	lda	$2002
	lda	#$10
	sta	$2006
	lda	#$00
	sta	$2006
	ldx	#$10
LFDE0:	sta	$2007
	dex
	bne	LFDE0
	lda	#$88
	sta	$2000
	jsr	LC6BB
	jsr	LFF74
	jmp	LC9B5
; ------------------------------------------------------------
LFDF4:
	inc	LFFDF
	lda	$6001
	jsr	LFE09
	lda	$6002
	jsr	LFFAC
	lda	$6003
	jmp	LFFC2
; -----------------------------
LFE09:
	sta	$6001
	sta	$9FFF
	lsr	a
	sta	$9FFF
	lsr	a
	sta	$9FFF
	lsr	a
	sta	$9FFF
	lsr	a
	sta	$9FFF
	rts
; -----------------------------
LFE20:
	ldy	#$01
	lda	$0300, x
	bpl	LFE38
	tay
	lsr	a
	lsr	a
	lsr	a
	lsr	a
	and	#$04
	ora	#$88
	sta	$2000
	tya
	inx
	ldy	$0300, x

LFE38:
	inx
	and	#$3F
	sta	$2006
	lda	$0300, x
	inx
	sta	$2006

LFE45:
	lda	$0300, x
	inx
	sta	$2007
	dey
	bne	LFE45
	dec	$03
	bne	LFE20
	beq	LFEB1
; -----------------------------
LFE55:
	jsr	LFF2D
	lda	#$02
	sta	$4014
	jmp	LFEE0
; -----------------------------
LFE60:
	lda	#$02
	sta	$4014
	bne	LFEB1
; -----------------------------
	.export	ppu_nmi_enter
ppu_nmi_enter:
	pha
	txa
	pha
	tya
	pha
	tsx
	lda	$0106, x
	cmp	#$FF
	bne	LFE55
	lda	$0105, x
	cmp	#$77
	bcc	LFE55
	cmp	#$7D
	bcs	LFE55
	lda	$2002
	inc	$4F
	lda	$03
	beq	LFE60
	cmp	#$08
	bcs	LFE91
	lda	#$02
	sta	$4014
LFE91:	ldx	#$00
	lda	$602C
	bmi	LFE20
LFE98:	lda	$0300, x
	sta	$2006
	lda	$0301, x
	sta	$2006
	lda	$0302, x
	sta	$2007
	inx
	inx
	inx
	cpx	$04
	bne	LFE98

LFEB1:
	lda	#$3F
	sta	$2006
	lda	#$00
	sta	$02
	sta	$03
	sta	$04
	sta	$602C
	sta	$2006
	lda	#$0F
	sta	$2007
	lda	$06
	bne	LFED1
	lda	#$88
	bne	LFED3
LFED1:	lda	#$89
LFED3:	sta	$2000
	lda	$05
	sta	$2005
	lda	$07
	sta	$2005
LFEE0:	jsr	LFDF4
	lda	$6005
	bne	LFEF0
	lda	#$01
	jsr	LFF96
	jsr	L8028
LFEF0:	lda	$6004
	jsr	LFF91
	tsx
	lda	$0106, x
	sta	$36
	cmp	#$FF
	bne	LFF10
	lda	$0105, x
	cmp	#$96
	bcc	LFF10
	cmp	#$D6
	bcs	LFF10
	lda	#$D6
	sta	$0105, x
LFF10:	lda	$0105, x
	sta	$35
	ldy	#$00
	lda	($35), y
	and	#$0F
	cmp	#$07
	beq	LFF25
	pla
	tay
	pla
	tax
	pla
	rti

LFF25:	pla
	tay
	pla
	tax
	pla
	jmp	apu_irq_enter

LFF2D:	lda	#$3F
	sta	$2006
	lda	#$00
	sta	$2006
	lda	#$0F
	sta	$2007
	lda	$06
	bne	LFF44
	lda	#$88
	bne	LFF46
LFF44:	lda	#$89
LFF46:	sta	$2000
	lda	$05
	sta	$2005
	lda	$07
	sta	$2005
	rts
; ------------------------------------------------------------
	.res	32, $FF
; ------------------------------------------------------------
	.export	LFF74
LFF74:
	lda	#$01
	sta	$02

LFF78:
	lda	$02
	bne	LFF78

	rts
; ------------------------------------------------------------
	.res	17, $FF
; ------------------------------------------------------------
	.export	start_banked
start_banked:
	jmp	start
; ------------------------------------------------------------
LFF91:	sta	$6004
	nop
	nop
LFF96:	sta	$FFFF
	lsr	a
	sta	$FFFF
	lsr	a
	sta	$FFFF
	lsr	a
	sta	$FFFF
	lsr	a
	sta	$FFFF
	nop
	nop
	rts

LFFAC:	sta	$BFFF
	lsr	a
	sta	$BFFF
	lsr	a
	sta	$BFFF
	lsr	a
	sta	$BFFF
	lsr	a
	sta	$BFFF
	nop
	nop
	rts

LFFC2:	sta	LDFFF
	lsr	a
	sta	LDFFF
	lsr	a
	sta	LDFFF
	lsr	a
	sta	LDFFF
	lsr	a
	sta	LDFFF
	nop
	nop
	rts
; -----------------------------------------------------------
	.export	start_dw
start_dw:
	sei
	inc	LFFDF
	jmp	start
; -----------------------------------------------------------
LFFDF:	.byte	$80
; -----------------------------------------------------------
LFFE0:	.byte	"DRAGON WARRIOR  "
; -----------------------------------------------------------
	.byte	$56
	.byte	$DE
	.byte	$30
	.byte	$70
	.byte	$01
	.byte	$04
	.byte	$01
	.byte	$0F
	.byte	$07
	.byte	$44
