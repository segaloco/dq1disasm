.include	"bssmap.i"
.include	"map.i"

.segment	"CHR3CODE"

	.export tbl_treasures_addr, tbl_treasures, tbl_treasures_end
tbl_treasures_addr:
.org	PPU_NMI_BUFFER+$20
tbl_treasures:
	.byte	maps::castle_courtyard, $01, $0D, $13
	.byte	maps::castle_courtyard, $01, $0F, $13
	.byte	maps::castle_courtyard, $02, $0E, $13
	.byte	maps::castle_courtyard, $03, $0F, $13
	.byte	maps::castle_throneroom, $04, $04, $16
	.byte	maps::castle_throneroom, $05, $04, $04
	.byte	maps::castle_throneroom, $06, $01, $03
	.byte	maps::map_0B, $18, $17, $06
	.byte	maps::map_09, $08, $05, $14
	.byte	maps::map_09, $08, $06, $02
	.byte	maps::map_09, $09, $05, $04
	.byte	maps::map_06, $0B, $0B, $02
	.byte	maps::map_06, $0B, $0C, $15
	.byte	maps::map_06, $0B, $0D, $06
	.byte	maps::map_06, $0C, $0C, $03
	.byte	maps::map_06, $0C, $0D, $0C
	.byte	maps::map_06, $0D, $0D, $02
	.byte	maps::map_0C, $04, $05, $0F
	.byte	maps::map_0D, $03, $04, $10
	.byte	maps::map_18, $0B, $00, $02
	.byte	maps::map_18, $0C, $00, $12
	.byte	maps::map_18, $0D, $00, $13
	.byte	maps::map_1A, $01, $01, $0C
	.byte	maps::map_1A, $0D, $06, $0D
	.byte	maps::map_10, $05, $05, $11
	.byte	maps::map_17, $01, $06, $0E
	.byte	maps::map_17, $03, $02, $04
	.byte	maps::map_17, $02, $02, $09
	.byte	maps::map_17, $0A, $09, $14
	.byte	maps::map_16, $0D, $05, $02
	.byte	maps::map_1D, $09, $03, $17
tbl_treasures_end:
