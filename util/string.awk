BEGIN {
	FS = ")"
}

function print_byte(c) {
	if (c == "あ")
		return "CHAR_HIRA_A"
	else if (c == "い")
		return "CHAR_HIRA_I"
	else if (c == "う")
		return "CHAR_HIRA_U"
	else if (c == "え")
		return "CHAR_HIRA_E"
	else if (c == "お")
		return "CHAR_HIRA_O"
	else if (c == "か")
		return "CHAR_HIRA_KA"
	else if (c == "が")
		return "CHAR_HIRA_GA_B"
	else if (c == "き")
		return "CHAR_HIRA_KI"
	else if (c == "ぎ")
		return "CHAR_HIRA_GI_B"
	else if (c == "く")
		return "CHAR_HIRA_KU"
	else if (c == "ぐ")
		return "CHAR_HIRA_GU_B"
	else if (c == "け")
		return "CHAR_HIRA_KE"
	else if (c == "げ")
		return "CHAR_HIRA_GE_B"
	else if (c == "こ")
		return "CHAR_HIRA_KO"
	else if (c == "ご")
		return "CHAR_HIRA_GO_B"
	else if (c == "さ")
		return "CHAR_HIRA_SA"
	else if (c == "ざ")
		return "CHAR_HIRA_ZA_B"
	else if (c == "し")
		return "CHAR_HIRA_SHI"
	else if (c == "じ")
		return "CHAR_HIRA_JI_B"
	else if (c == "す")
		return "CHAR_HIRA_SU"
	else if (c == "ず")
		return "CHAR_HIRA_ZU_B"
	else if (c == "せ")
		return "CHAR_HIRA_SE"
	else if (c == "ぜ")
		return "CHAR_HIRA_ZE_B"
	else if (c == "そ")
		return "CHAR_HIRA_SO"
	else if (c == "ぞ")
		return "CHAR_HIRA_ZO_B"
	else if (c == "た")
		return "CHAR_HIRA_TA"
	else if (c == "だ")
		return "CHAR_HIRA_DA_B"
	else if (c == "ち")
		return "CHAR_HIRA_CHI"
	else if (c == "ぢ")
		return "CHAR_HIRA_DI_B"
	else if (c == "つ")
		return "CHAR_HIRA_TSU"
	else if (c == "づ")
		return "CHAR_HIRA_DU_B"
	else if (c == "て")
		return "CHAR_HIRA_TE"
	else if (c == "で")
		return "CHAR_HIRA_DE_B"
	else if (c == "と")
		return "CHAR_HIRA_TO"
	else if (c == "ど")
		return "CHAR_HIRA_DO_B"
	else if (c == "な")
		return "CHAR_HIRA_NA"
	else if (c == "に")
		return "CHAR_HIRA_NI"
	else if (c == "ぬ")
		return "CHAR_HIRA_NU"
	else if (c == "ね")
		return "CHAR_HIRA_NE"
	else if (c == "の")
		return "CHAR_HIRA_NO"
	else if (c == "は")
		return "CHAR_HIRA_HA"
	else if (c == "ば")
		return "CHAR_HIRA_BA_B"
	else if (c == "ぱ")
		return "CHAR_HIRA_PA_B"
	else if (c == "ひ")
		return "CHAR_HIRA_HI"
	else if (c == "び")
		return "CHAR_HIRA_BI_B"
	else if (c == "ぴ")
		return "CHAR_HIRA_PI_B"
	else if (c == "ふ")
		return "CHAR_HIRA_FU"
	else if (c == "ぶ")
		return "CHAR_HIRA_BU_B"
	else if (c == "ぷ")
		return "CHAR_HIRA_PU_B"
	else if (c == "へ")
		return "CHAR_HIRA_HE"
	else if (c == "べ")
		return "CHAR_HIRA_BE_B"
	else if (c == "ぺ")
		return "CHAR_HIRA_PE_B"
	else if (c == "ほ")
		return "CHAR_HIRA_HO"
	else if (c == "ぼ")
		return "CHAR_HIRA_BO_B"
	else if (c == "ぽ")
		return "CHAR_HIRA_PO_B"
	else if (c == "ま")
		return "CHAR_HIRA_MA"
	else if (c == "み")
		return "CHAR_HIRA_MI"
	else if (c == "む")
		return "CHAR_HIRA_MU"
	else if (c == "め")
		return "CHAR_HIRA_ME"
	else if (c == "も")
		return "CHAR_HIRA_MO"
	else if (c == "や")
		return "CHAR_HIRA_YA"
	else if (c == "ゆ")
		return "CHAR_HIRA_YU"
	else if (c == "よ")
		return "CHAR_HIRA_YO"
	else if (c == "ら")
		return "CHAR_HIRA_RA"
	else if (c == "り")
		return "CHAR_HIRA_RI"
	else if (c == "る")
		return "CHAR_HIRA_RU"
	else if (c == "れ")
		return "CHAR_HIRA_RE"
	else if (c == "ろ")
		return "CHAR_HIRA_RO"
	else if (c == "わ")
		return "CHAR_HIRA_WA"
	else if (c == "を")
		return "CHAR_HIRA_WO"
	else if (c == "ん")
		return "CHAR_HIRA_N"
	else if (c == "っ")
		return "CHAR_HIRA_TSU_S"
	else if (c == "ゃ")
		return "CHAR_HIRA_YA_S"
	else if (c == "ゅ")
		return "CHAR_HIRA_YU_S"
	else if (c == "ょ")
		return "CHAR_HIRA_YO_S"
	else if (c == "イ")
		return "CHAR_KATA_I"
	else if (c == "カ")
		return "CHAR_KATA_KA"
	else if (c == "ガ")
		return "CHAR_KATA_GA"
	else if (c == "キ")
		return "CHAR_KATA_KI"
	else if (c == "ギ")
		return "CHAR_KATA_GI"
	else if (c == "コ")
		return "CHAR_KATA_KO"
	else if (c == "ゴ")
		return "CHAR_KATA_GO"
	else if (c == "シ")
		return "CHAR_KATA_SHI"
	else if (c == "ジ")
		return "CHAR_KATA_JI"
	else if (c == "ス")
		return "CHAR_KATA_SU"
	else if (c == "ズ")
		return "CHAR_KATA_ZU"
	else if (c == "タ")
		return "CHAR_KATA_TA"
	else if (c == "ダ")
		return "CHAR_KATA_DA"
	else if (c == "ト")
		return "CHAR_KATA_TO"
	else if (c == "ド")
		return "CHAR_KATA_DO"
	else if (c == "ヘ")
		return "CHAR_KATA_HE"
	else if (c == "ベ")
		return "CHAR_KATA_BE_B"
	else if (c == "ペ")
		return "CHAR_KATA_PE_B"
	else if (c == "ホ")
		return "CHAR_KATA_HO"
	else if (c == "ポ")
		return "CHAR_KATA_PO"
	else if (c == "マ")
		return "CHAR_KATA_MA"
	else if (c == "ミ")
		return "CHAR_KATA_MI"
	else if (c == "ム")
		return "CHAR_KATA_MU"
	else if (c == "メ")
		return "CHAR_KATA_ME"
	else if (c == "ラ")
		return "CHAR_KATA_RA"
	else if (c == "リ")
		return "CHAR_KATA_RI"
	else if (c == "ル")
		return "CHAR_KATA_RU"
	else if (c == "レ")
		return "CHAR_KATA_RE"
	else if (c == "ロ")
		return "CHAR_KATA_RO"
	else if (c == "ン")
		return "CHAR_KATA_N"
	else if (c == "。")
		return "CHAR_KANA_PERIOD"
	else if (c == "ー")
		return "CHAR_LONG_VOWEL"
	else if (c == "「")
		return "CHAR_QUOTE_MARK"
	else if (c == " ")
		return "\" \""
	else if (c == ":")
		return "\":\""
	else if (c == "H")
		return "\"H\""
	else if (c == "P")
		return "\"P\""
	else if (c == "G")
		return "\"G\""
	else if (c == "E")
		return "\"E\""
	else if (c == "M")
		return "\"M\""
	else if (c == "*")
		return "\"*\""
	else if (c == "?")
		return "\"?\""
	else if (c == "!")
		return "\"!\""
	else if (c == ",")
		return "\",\""
	else if (c == "0")
		return "\"0\""
	else if (c == "1")
		return "\"1\""
	else if (c == "2")
		return "\"2\""
	else if (c == "3")
		return "\"3\""
	else if (c == "4")
		return "\"4\""
	else if (c == "5")
		return "\"5\""
	else if (c == "6")
		return "\"6\""
	else if (c == "7")
		return "\"7\""
	else if (c == "8")
		return "\"8\""
	else if (c == "9")
		return "\"9\""
	else if (c == "…")
		return "CHAR_ELLIPSES"
	else if (c == "◦")
		return "CHAR_HANDAKUTEN_LO"
	else if (c == "╔")
		return "CHAR_W_BORD_NW"
	else if (c == "═")
		return "CHAR_W_BORD_NA"
	else if (c == "╤")
		return "CHAR_W_BORD_NB"
	else if (c == "╗")
		return "CHAR_W_BORD_NE"
	else if (c == "╢")
		return "CHAR_W_BORD_E"
	else if (c == "╝")
		return "CHAR_W_BORD_SE"
	else if (c == "╧")
		return "CHAR_W_BORD_S"
	else if (c == "╚")
		return "CHAR_W_BORD_SW"
	else if (c == "╟")
		return "CHAR_W_BORD_W"
	else if (c == "゛")
		return "CHAR_DAKUTEN_HI"
	else if (c == "゜")
		return "CHAR_HANDAKUTEN_HI"
}

function is_wide(c) {
	if (c == "ガ")
		return 1;
	else if (c == "ギ")
		return 1;
	else if (c == "ゴ")
		return 1;
	else if (c == "ジ")
		return 1;
	else if (c == "ズ")
		return 1;
	else if (c == "ダ")
		return 1;
	else if (c == "ポ")
		return 1;
	else
		return 0;
}

function print_line(count, c) {
	wide = is_wide(c)
	if (wide)
		type = "dbyt"
	else
		type = "byte"

	if (count > 3 && !wide) {
		printf "\t.byte\tCHAR_CTRL_RLE, %d, %s\n", count, print_byte(c)
	} else {
		for (j = 0; j < count; j++) {
			printf "\t.%s\t%s\n", type, print_byte(c)
		}
	}
}

function print_special(c) {
	if (c == "n")
		printf "\t.byte\tCHAR_CTRL_CONTENT_NEWLINE\n"
	else if (c == "r")
		printf "\t.byte\tCHAR_CTRL_WINDOW_NEWLINE\n"
	else if (c == "s")
		printf "\t.byte\tCHAR_CTRL_STREND\n"
	else if (c == "z")
		printf "\t.byte\tCHAR_CTRL_END\n"
	else if (c == "v")
		printf "\t.byte\tCHAR_CTRL_SCROLL\n"
}

/a_dq_charmap_str/ {
	in_string = 0;
	last_char = -1;
	counter = 0;
	n = 0;
	len = length($1);
	has_label = 0;
	label_done = 0;

	if (/:/) {
		has_label = 1;
	}

	for (i = 0; i < len; i++) {
		n = substr($1, i, 1)

		if (!in_string && has_label && label_done) {
			printf "%c", n
			if (n == ":") {
				label_done = 1;
				printf "\n\t"
			}
		}

		if (!in_string && n == "\"") {
			in_string = 1;
			continue;
		}

		if (!in_string)
			continue;

		if (n == "\\") {
			print_line(counter, last_char)

			n2 = substr($1, i+1, 1)
			if (n2 != "\\") {
				print_special(n2)
				last_char = -1;
				i += 2;
				continue;
			}
		} 

		if (last_char == -1) {
			last_char = n;
			counter = 1;
			continue;
		}

		if (n == last_char) {
			counter++;
			continue;
		} else {
			print_line(counter, last_char)

			last_char = n;
			counter = 1;
		}
	}

	if (i == len)
		print_line(counter, last_char)
}

!/a_dq_charmap_str/ {
	print
}