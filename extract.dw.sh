#!/bin/sh

# dd positions of banks
# iNES - skip=0     count=16
# PRG0 - skip=16    count=16384
# PRG1 - skip=16400 count=16384
# PRG2 - skip=32784 count=16384
# PRG3 - skip=49168 count=16384
# CHR0 - skip=65552 count=8192
# CHR1 - skip=73744 count=8192

INFILE=$1

if [ -z $INFILE ]
then
	INFILE=/dev/zero
fi

# Directory structure
mkdir -p data/chr00.dw data/chr01.dw

# PRG
dd if=$INFILE bs=1 skip=16 count=16384 >data/prg0.dw.bin
dd if=$INFILE bs=1 skip=16400 count=16384 >data/prg1.dw.bin
dd if=$INFILE bs=1 skip=32784 count=16384 >data/prg2.dw.bin
dd if=$INFILE bs=1 skip=49168 count=16378 >data/prg3.dw.bin

# CHR Common
TILE_SIZE=16
OBJ_BANK=256

# CHR bank 00
BANKBASE=65552
BANKLEN=8192
TILES=`expr $BANKLEN / $TILE_SIZE`

I=0
while [ $I -ne $OBJ_BANK ] && [ $I -ne $TILES ]
do
        dd if=$INFILE bs=1 count=$TILE_SIZE skip=`expr $I \* $TILE_SIZE + $BANKBASE` >data/chr00.dw/tile_bg_`printf "%.2X" $I`.bin
        I=`expr $I + 1`
done

while [ $I -ne $TILES ]
do
	J=`expr $I - $OBJ_BANK`
        dd if=$INFILE bs=1 count=$TILE_SIZE skip=`expr $I \* $TILE_SIZE + $BANKBASE` >data/chr00.dw/tile_obj_`printf "%.2X" $J`.bin
	I=`expr $I + 1`
done

# CHR bank 01
BANKBASE=73744
BANKLEN=8192
TILES=`expr $BANKLEN / $TILE_SIZE`

I=0
while [ $I -ne $OBJ_BANK ] && [ $I -ne $TILES ]
do
        dd if=$INFILE bs=1 count=$TILE_SIZE skip=`expr $I \* $TILE_SIZE + $BANKBASE` >data/chr01.dw/tile_bg_`printf "%.2X" $I`.bin
        I=`expr $I + 1`
done

while [ $I -ne $TILES ]
do
	J=`expr $I - $OBJ_BANK`
        dd if=$INFILE bs=1 count=$TILE_SIZE skip=`expr $I \* $TILE_SIZE + $BANKBASE` >data/chr01.dw/tile_obj_`printf "%.2X" $J`.bin
	I=`expr $I + 1`
done
