.include	"system/cnrom.i"
.include	"system/ppu.i"

.include	"bssmap.i"
.include	"macros.i"

.segment	"VECTORS"

; vectors - the vectors reside at 0xFFFA and are the entrypoints
; to the non-maskable interrupt, reset, and interrupt request
; vectors respectively, in this title providing the
; per-frame operations, main bootstrap, and APU interrupts
	.addr	ppu_nmi_enter
.if	.defined(DQ)
	.addr	start
.elseif	.defined(DW)
	.addr	start_dw
.endif
	.addr	apu_irq_enter
; ------------------------------------------------------------
.segment	"CODE"

.if	.defined(DQ)
; start - this is the very first code run after RESET is lowered
; the PPU is set to defaults and display blanked
; then the status register is read twice, once to clear
; the vertical retrace flag, then looping again to await the precise
; moment of the next vertical retrace as to synchronize with the PPU
; next, the stack is cleared
; finally, the non-maskable vertical interrupt is enabled and
; all sprites are pushed offscreen
; the system awaits the current frame and then begins the main title
start:
	.if	.defined(DW)
		cld
	.endif	; DW

	lda	#ppu_ctlr0::bg_seg0|ppu_ctlr0::obj_seg1|ppu_ctlr0::bg1
	sta	PPU_CTLR0

	.if	.defined(DQ)
		lda	#ppu_ctlr1::bgblk_on|ppu_ctlr1::objblk_on|ppu_ctlr1::color
		sta	PPU_CTLR1
	.endif	; DQ

	.if	.defined(DQ)
	:
		lda	PPU_SR
		bpl	:-

	:
		lda	PPU_SR
		bpl	:-
	.elseif	.defined(DW)
	:
		lda	PPU_SR
		bmi	:-

	:
		lda	PPU_SR
		bpl	:-

	:
		lda	PPU_SR
		bmi	:-
	.endif	; DQ|DW

	.if	.defined(DW)
		lda	#0
		sta	PPU_CTRL1
	.endif	; DW

	ldx	#<((STACK_END-STACK_BASE)-1)
	txs

	.if	.defined(DQ)
		lda	#0
	.endif	; DQ

	tax

	.if	.defined(DW)
		sta	bgupdate
	.endif	; DW

	: ; for (x = 0; x < 0x100; x++) {
		sta	0, x

		.if	.defined(DW)
			sta	bram, x
			sta	winram, x
			sta	winram+$100, x
			sta	winram+$200, x
			sta	winram+$300, x
		.endif	; DW

		inx
		bne	:-
	; }

	.if	.defined(DW)
		jsr	mmc1_load_prg3
		sta	mmc1_bank_cur

		lda	#$1E
		sta	mmc1_config

		lda	#0
		sta	nt_curr_0
		sta	nt_curr_1
		jsr	mmc1_exec

		lda	PPU_SR
		lda	#$10
		sta	PPU_VRAM_AR
		lda	#$00
		sta	PPU_VRAM_AR
		ldx	#$10
		: ; for (x = 0x10; x > 0; x--) {
			sta	PPU_VRAM_IO

			dex
			bne	:-
		; }
	.endif	; DW

	lda	#ppu_ctlr0::int|ppu_ctlr0::bg_seg0|ppu_ctlr0::obj_seg1|ppu_ctlr0::bg1
	sta	PPU_CTLR0
	jsr	oam_init
	jsr	ppu_nmi_wait
	; fall through, this and the subsequent code are
	; split in the US release of Dragon Warrior
	.if	.defined(DW)
		jmp	_main
	.endif	; DW
.endif	; DQ
