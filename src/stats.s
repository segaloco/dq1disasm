.include	"system/cpu.i"
.include	"system/cnrom.i"

.include	"ppu_nmi.i"
.include	"ppu_nmi_write.i"
.include	"ppu_pal.i"
.include	"general_bss.i"
.include	"game_bss.i"
.include	"joypad.i"
.include	"math.i"
.include	"map.i"
.include	"flags.i"
.include	"window.i"
.include	"dialogs.i"
.include	"tunables.i"
.include	"macros.i"

.segment	"ZEROPAGE"

	.export rpg_name, rpg_name_end
	.export rpg_started
	.export rpg_exp, rpg_gold
	.export rpg_equip, rpg_keys
	.export rpg_herbs, rpg_inv
	.export rpg_hp, rpg_mp
	.export rpg_level, rpg_str, rpg_agi
	.export rpg_maxhp, rpg_maxmp
	.export rpg_att, rpg_def
	.export rpg_flags
rpg_name:	.byte	0, 0, 0, 0
rpg_name_end:
rpg_started:	.byte	0
rpg_exp:	.word	0
rpg_gold:	.word	0
rpg_equip:	.byte	0
rpg_keys:	.byte	0
rpg_herbs:	.byte	0
rpg_inv:	.byte	0, 0, 0, 0
rpg_hp:		.byte	0
rpg_mp:		.byte	0
rpg_level:	.byte	0
rpg_str:	.byte	0
rpg_agi:	.byte	0
rpg_maxhp:	.byte	0
rpg_maxmp:	.byte	0
rpg_att:	.byte	0
rpg_def:	.byte	0
rpg_flags:	.word	0
; ------------------------------------------------------------
.segment	"CODE"

	.export stats_load
stats_load:
	ldx     #<(LEVEL_MAX-1)*WORD_SIZE
        lda     #<(LEVEL_MAX)
        sta     rpg_level
	: ; for (rung of tbl_level_exp) {
		lda	rpg_exp
		sec
		sbc     tbl_level_exp,x
		lda     rpg_exp+1
		sbc     tbl_level_exp+1,x
		bcs     :+ ; if (rpg_exp < tbl_level_exp[x]) {
			dec     rpg_level
			dex
			dex
			bne     :-
		: ; }
	; }

	lda     rpg_level
        sec
        sbc     #1
        asl     a
        sta     stats_level_tbl_idx
        asl     a
        clc
        adc     stats_level_tbl_idx
        sta     stats_level_tbl_idx
        lda     #$FF
        sta     ppu_nmi_run
        sei
		: ; while (ppu_nmi_run) {
			lda     ppu_nmi_run
			bne     :-
		; }

	rpg_stats_bankin:
		lda	#CNROM_SECURITY_BITS|1
		sta	rpg_stats_bankin+1
		lda	#>(tbl_levelstats)
		sta     PPU_VRAM_AR
		lda     stats_level_tbl_idx
		sta     PPU_VRAM_AR
		lda     PPU_VRAM_IO
		lda     PPU_VRAM_IO
		sta     rpg_str
		lda     PPU_VRAM_IO
		sta     rpg_agi
		lda     PPU_VRAM_IO
		sta     rpg_maxhp
		lda     PPU_VRAM_IO
		sta     rpg_maxmp
		lda     PPU_VRAM_IO
		ora     rpg_flags+1
		sta     rpg_flags+1
		lda     PPU_VRAM_IO
		sta     rpg_flags
		jsr     cnrom_bank_reset
        cli

        ldx     #4
        lda     #0
	: ; for (x = 4; x > 0 x--) {
		clc
		adc     dialog_buffer_end-1,x
		dex
		bne     :-
	; }
        sta     rpg_debuff_high
        and     #$03
        sta     rpg_debuff_low
        lda     rpg_debuff_high
        lsr     a
        lsr     a
        and     #$03
        sta     rpg_debuff_high
        lda     rpg_debuff_low
        lsr     a
        bcs     :+ ; if (rpg_debuff_high & 1) {
		lda     rpg_str
		jsr     rpg_debuff_stat
		sta     rpg_str
		jmp     :++
	: ; } else {
		lda     rpg_maxmp
		beq     :+ ; if (rpg_maxmp) {
			jsr     rpg_debuff_stat
			sta     rpg_maxmp
		; }
	: ; }

	lda     rpg_debuff_low
        and     #$02
        bne     :+ ; if (!(rpg_debuff_high & $2)) {
		lda     rpg_agi
		jsr     rpg_debuff_stat
		sta     rpg_agi
		jmp     :++
	: ; } else {
		lda     rpg_maxhp
		jsr     rpg_debuff_stat
		sta     rpg_maxhp
	: ; }

	lda     rpg_equip
        lsr     a
        lsr     a
        lsr     a
        lsr     a
        lsr     a
        tax
        lda     tbl_buf_atk_weapon,x
        clc
        adc     rpg_str
        sta     rpg_att
        lda     rpg_agi
        lsr     a
	sta	rpg_def
	lda	rpg_equip
        lsr     a
        lsr     a
        and     #$07
        tax
        lda     tbl_buf_def_armor,x
        clc
        adc     rpg_def
        sta     rpg_def
        lda     rpg_equip
        and     #$03
        tax
        lda     tbl_buf_def_shield,x
        clc
        adc     rpg_def
        sta     rpg_def
        lda     rpg_flags+1
        and     #state_flags::equipped_scale>>8
        beq     :+ ; if (rpg_flags & equipped_scale) {
		lda     rpg_def
		clc
		adc     #RPG_BUFF_DEF_SCALE
		sta     rpg_def
	: ; }

	rts
; -----------------------------
rpg_debuff_stat:
	sta     math_mulw_a
        lda     #RPG_DEBUFF_DIVIDEND
        sta     math_mulw_b
        lda     #0
        sta     math_mulw_a+1
        sta     math_mulw_b+1
        jsr     math_mulw
        lda     math_result
        sta     math_div_a
        lda     math_result+1
        sta     math_div_a+1
        lda     #<(RPG_DEBUFF_DIVISOR)
        sta     math_div_b
        lda     #>(RPG_DEBUFF_DIVISOR)
        sta     math_div_b+1
        jsr     math_divw
        lda     math_div_a
        clc
        adc     rpg_debuff_high
        rts
; ------------------------------------------------------------
exp_temp	= zeroword

	.export stats_calc_exp
stats_calc_exp:
	lda     rpg_level
        asl     a
        tax
        lda     tbl_level_exp,x
        sec
        sbc     rpg_exp
        sta     exp_temp
        lda     tbl_level_exp+1,x
        sbc     rpg_exp+1
        sta     exp_temp+1
        rts
