.segment    "ZEROPAGE"

	.export ppu_pal_load_cutoff
	.export general_word_0
	.export general_byte_2, window_itemname
	.export	window_dialog_str_tbl, window_str_id, battle_fight_hit
	.export dialog_sale_gold_diff
	.export dialog_string_ptr, stats_level_tbl_idx

	.export area_check_retval
	.export general_byte_3

	.export general_word_1, window_itemname_ptr, window_str_id_ptr
	.export general_byte_4, reward_add_mask

	.export	ppu_pal_fade_idx, ppu_pal_fade_has_bg, ppu_pal_fade_obj, ppu_pal_fade_bg
	.export math_div_a, math_div_b, math_mulw_a, math_mulw_b, math_result, old_rand_word
	.export general_word_2, battle_enemy_chr_index

	.export cnrom_load_src, cnrom_load_dest
	.export cnrom_load_count, cnrom_load_page_size, cnrom_load_bank
	.export map_blk_col, map_blk_row, map_blkid
	.export nt_calc_column, nt_calc_row
	.export	map_npc_xpos, map_npc_ypos
	.export window_block_x, window_block_y
	.export general_byte_0
	.export battle_palette_addr
	.export battle_fight_attack, battle_fight_diff, battle_fight_defense
	.export rpg_debuff_low, rpg_debuff_high

	.export map_eng_number

ppu_pal_load_cutoff:
nt_calc_column:
map_blk_col:
map_blkid:
old_rand_word:
cnrom_load_src:
math_mulw_a:
math_div_a:
ppu_pal_fade_idx:
window_itemname:
window_dialog_str_tbl:
window_str_id:
battle_fight_hit:
dialog_sale_gold_diff:
dialog_string_ptr:
stats_level_tbl_idx:
general_word_0:
general_byte_2:		.byte	0
ppu_pal_fade_has_bg:
area_check_retval:
general_byte_3:		.byte	0
nt_calc_row:
cnrom_load_dest:
ppu_pal_fade_obj:
math_mulw_b:
math_div_b:
window_itemname_ptr:
window_str_id_ptr:
map_blk_row:
reward_add_mask:
general_word_1:		
general_byte_4:		.word	0
cnrom_load_count:
ppu_pal_fade_bg:
math_result:
battle_enemy_chr_index:
general_word_2:		.word	0
cnrom_load_page_size:
map_npc_xpos:
window_block_x:
battle_palette_addr:
battle_fight_attack:
battle_fight_diff:
rpg_debuff_high:
general_byte_0:		.byte	0
cnrom_load_bank:
map_npc_ypos:
window_block_y:
battle_fight_defense:
rpg_debuff_low:		.byte	0
			.byte	0
map_eng_number:		.byte	0

	.end
