;************************************************************************************************
;*                                            apu.s                                             *
;* Name: apu                                                                                    *
;* Description: This is the APU engine.  The format is described in the apu_frame_play section  *
;* Entrypoints:                                                                                 *
;*  apu_irq_enter   - This is the entry from the IRQ vector and should only be used as such     *
;*  apu_wait_music  - Wait for the current track to complete                                    *
;*  apu_init        - Reset/initialize the driver                                               *
;*  apu_play        - Play a given entry                                                        *
;************************************************************************************************
    .include    "system/apu.i"

    .include    "apu_commands.i"
;************************************************************************************************
;*                                             BSS                                              *
;************************************************************************************************
    .segment    "ZEROPAGE"

apu_channel_index:
    .export     apu_pulse1_index
apu_pulse1_index:   .addr   0
apu_pulse2_index:   .addr   0
apu_triangle_index: .addr   0
apu_sfx_index:      .addr   0

apu_channel_ret:
apu_pulse1_ret:     .addr   0
apu_pulse2_ret:     .addr   0
apu_tri_ret:        .addr   0

apu_channel_len:
apu_pulse1_len:     .byte   0

apu_channel_trough:
apu_pulse1_trough:  .byte   0

apu_pulse2_len:     .byte   0
apu_pulse2_trough:  .byte   0

apu_tri_len:        .byte   0
apu_tri_trough:     .byte   0

apu_sfx_on:         .byte   0

apu_tempo:          .byte   0
apu_tempo_count:    .byte   0
apu_note_index:     .byte   0
apu_buffer:         .byte   0
apu_pulse2_cfg:     .byte   0

;************************************************************************************************
;*                                             CODE                                             *
;************************************************************************************************
    .segment    "CODE"

;************************************************************************************************
;*                                           FUNCTION                                           *
;* Name: apu_irq_enter                                                                          *
;* Description: The IRQ entry point from DMC finish or frame interrupt, here only used          *
;*              during frame interrupt to update the audio frame of each channel                *
;*                                                                                              *
;*              For this IRQ, pulse 2 is prioritized for sound effects                          *
;*                                                                                              *
;*              The sfx channel itself plays whenever it has data but the music channels        *
;*              instead play against the tempo counter                                          *
;************************************************************************************************
    .export     apu_irq_enter
; X
APU_CHANNEL_PULSE1  .set    0
APU_CHANNEL_PULSE2  .set    2
APU_CHANNEL_TRI     .set    4
APU_CHANNEL_SFX     .set    6

; Y
APU_REGFRAME_PULSE1 .set    0
APU_REGFRAME_PULSE2 .set    4
APU_REGFRAME_TRI    .set    8
APU_REGFRAME_DMC    .set    $10
apu_irq_enter:
    BIT         DMCSTATUS
    BVC         not_frame_interrupt   ;only handle DMC frame interrupt

; if (DMCSTATS & dmcstatus::frame_interrupt) {
        PHA
        TXA
        PHA
        TYA
        PHA

        LDX         #APU_CHANNEL_SFX
        LDY         #APU_REGFRAME_PULSE2

        LDA         apu_sfx_on
        BEQ         skip_sfx

    ; if (apu_sfx_on) {
            LDA         apu_note_index
            PHA

            LDA         #0
            STA         apu_note_index
            JSR         apu_frame_play

            TAX

            PLA
            STA         apu_note_index

            TXA
            BNE         skip_sfx_cleanup

        ; if (!apu_frame_play(sfx)) {
                LDA         #dmcstatus::pulse_1|dmcstatus::tri
                STA         DMCSTATUS
                LDA         #dmcstatus::pulse_1|dmcstatus::pulse_2|dmcstatus::tri|dmcstatus::noise
                STA         DMCSTATUS

                LDA         apu_pulse2_cfg
                STA         PULSE2_VOL
                LDA         #pulse_sweep::negate
                STA         PULSE2_SWEEP

                LDA         #channel_vol::counter_halt|channel_vol::constant_vol
                STA         NOISE_VOL
        ; }
    ; }

    skip_sfx:
    skip_sfx_cleanup:
        LDA         apu_tempo_count
        CLC
        ADC         apu_tempo
        STA         apu_tempo_count
        BCC         no_triggered_frame

    ; if (triggered_frame) {
            SBC         #$96
            STA         apu_tempo_count     ;proper rollover of tempo

            LDX         #APU_CHANNEL_TRI
            LDY         #APU_REGFRAME_TRI
            JSR         apu_frame_play

            LDX         #APU_CHANNEL_PULSE2
            LDY         #APU_REGFRAME_PULSE2
            LDA         apu_sfx_on
            BEQ         still_skip_sfx

        ; if (apu_sfx_on) {
                LDY         #APU_REGFRAME_DMC   ;pulse 2 using DMC frames
        ; }

        still_skip_sfx:
            JSR         apu_frame_play

            LDX         #APU_CHANNEL_PULSE1
            LDY         #APU_REGFRAME_PULSE1
            JSR         apu_frame_play
    ; }

    no_triggered_frame:
        PLA
        TAY
        PLA
        TAX
        PLA
; }

not_frame_interrupt:
    RTI

;************************************************************************************************
;*                                           FUNCTION                                           *
;* Name: apu_song_ret (0xFD)                                                                    *
;* Description: Set the channel index to its end                                                *
;************************************************************************************************
apu_song_ret:
    LDA         apu_channel_ret,X
    STA         apu_channel_index,X
    LDA         apu_channel_ret+1,X
    STA         apu_channel_index+1,X
    BNE         apu_next_byte           ; continue

;************************************************************************************************
;*                                           FUNCTION                                           *
;* Name: apu_note_load* (0x80-0xDF)                                                             *
;* Description: Load the note indexed by the accumulator onto the given channel                 *
;* Arguments:                                                                                   *
;*  A - note index                                                                              *
;* Returns: The current trough value of the channel if not sfx                                  *
;************************************************************************************************
apu_note_load:
    CLC                                                  
    ADC         apu_note_index
    ASL         A                                       
    STX         apu_buffer

    TAX                                                  
    LDA         apu_data_notes,X
    STA         CHANNEL_TIMER,Y
    LDA         apu_data_notes+1,X
    STA         CHANNEL_COUNTER,Y

    LDX         apu_buffer
    CPX         #APU_CHANNEL_SFX
    BEQ         apu_next_byte           ; continue

; if (not_sfx) {
        LDA         apu_channel_trough,X

        BEQ         apu_next_byte           ; continue
        BNE         apu_next_note           ; return
; }

;************************************************************************************************
;*                                           FUNCTION                                           *
;* Name: apu_trough (0xF7)                                                                      *
;* Description: The next byte determines the length of the next trough                          *
;************************************************************************************************
apu_trough:
    JSR         apu_get_byte
    STA         apu_channel_trough,X
    JMP         apu_next_byte           ; continue

;************************************************************************************************
;*                                           FUNCTION                                           *
;* Name: apu_trough_end (0xF6)                                                                  *
;* Description: Clear the trough length for this silence                                        *
;************************************************************************************************
apu_trough_end:
    LDA         #0
    STA         apu_channel_trough,X
    BEQ         apu_next_byte           ; continue

;************************************************************************************************
;*                                           FUNCTION                                           *
;* Name: apu_frame_play                                                                         *
;* Description: Begin processing the channel buffer indicated by X                              *
;*                                                                                              *
;*              The buffer will be processed until either it is empty, a note has been played,  *
;*              or a new string of commands has been encountered.  Some commands optionally     *
;*              take an index Y pointing to the register frame for a given channel.  Such       *
;*              commands are marked with * in commentary                                        *
;*              The following commands are recognized:                                          *
;*                  0x00-0x7F:  Indicates how long to hold the current state                    *
;*                  0x80-0xDF:  Note indices for apu_note_load*();                              *
;*                              0xC9-0xDF are undefined                                         *
;*                  0xE0-0xF5:  Noise indices for apu_noise_play();                             *
;*                              This value is of the format:                                    *
;*                                  %111xPPPP where PPPP is the noise period                    *
;*                                  Beware that if x = 1 this calls commands above period 5     *
;*                  0xF6:       apu_trough_end();                                               *
;*                  0xF7:       apu_trough();                                                   *
;*                  0xF8:       apu_noise_vol_change();                                         *
;*                  0xF9:       apu_note_get();                                                 *
;*                  0xFA:       apu_pulse_sweep_cfg*();                                         *
;*                  0xFB:       apu_wave_vol_change*();                                         *
;*                  0xFC:       apu_next_byte();                                                *
;*                  0xFD:       apu_song_ret();                                                 *
;*                  0xFE:       apu_song_jsr();                                                *
;*                  0xFF:       apu_tempo_change();                                             *
;* Arguments:                                                                                   *
;*  X - Channel index, as passed through to subsequent functions                                *
;*      0 - Pulse 1                                                                             *
;*      1 - Pulse 2                                                                             *
;*      2 - Triangle                                                                            *
;*      3 - Noise                                                                               *
;*  Y - Register frame for certain operations                                                   *
;*      0  - pulse 1                                                                            *
;*      4  - pulse 2                                                                            *
;*      8  - triangle                                                                           *
;*      10 - DMC                                                                                *
;************************************************************************************************
apu_frame_play:
    LDA         apu_channel_len,X
    BEQ         apu_frame_play_done

; if (channel_length) {
        DEC         apu_channel_len,X
        BNE         apu_frame_play_done

    ; if (!--channel_length) {

;************************************************************************************************
;*                                           FUNCTION                                           *
;* Name: apu_next_byte (0xFC)                                                                   *
;* Description: Get the next byte in the buffer                                                 *
;************************************************************************************************
apu_next_byte:
            JSR         apu_get_byte
        ; uint8_t next_byte = apu_get_byte();
            CMP         #apu_cmd_song_jsr
            BEQ         apu_song_jsr            ; if (next_byte == 0xFE)        apu_song_jsr();
            BCS         apu_tempo_change        ; else if (next_byte == 0xFF)   apu_tempo_change();
            CMP         #apu_cmd_next_byte
            BEQ         apu_next_byte           ; else if (next_byte == 0xFC)   apu_next_byte();
            BCS         apu_song_ret            ; else if (next_byte == 0xFD)   apu_song_ret();
            CMP         #apu_cmd_pulse_sweep_cfg
            BEQ         apu_pulse_sweep_cfg     ; else if (next_byte == 0xFA)   apu_pulse_sweep_cfg*();
            BCS         apu_wave_vol_change     ; else if (next_byte == 0xFB)   apu_wave_vol_change*();
            CMP         #apu_cmd_noise_vol_change
            BEQ         apu_noise_vol_change    ; else if (next_byte == 0xF8)   apu_noise_vol_change();
            BCS         apu_note_get            ; else if (next_byte == 0xF9)   apu_note_get();
            CMP         #apu_cmd_trough_end
            BEQ         apu_trough_end          ; else if (next_byte == 0xF6)   apu_trough_end();
            BCS         apu_trough              ; else if (next_byte == 0xF7)   apu_trough();
            CMP         #apu_cmd_noise_play
            BCS         apu_noise_play          ; else if (next_byte >= 0xE0)   apu_noise_play();
            CMP         #apu_cmd_note_load
            BCS         apu_note_load           ; else if (next_byte >= 0x80)   apu_note_load*();
apu_next_note:
            STA         apu_channel_len,X       ; else                          tone length
    ; }

; }
apu_frame_play_done:
    RTS

;************************************************************************************************
;*                                           FUNCTION                                           *
;* Name: apu_tempo_change (0xFF)                                                                *
;* Description: Retrieve the next byte in the stream, set the tempo                             *
;************************************************************************************************
apu_tempo_change:
    JSR         apu_get_byte
    STA         apu_tempo
    JMP         apu_next_byte   ; continue

;************************************************************************************************
;*                                           FUNCTION                                           *
;* Name: apu_song_jsr (0xFE)                                                                   *
;* Description: Start a new song by pointing the channel index to the next pointer in the       *
;*              stream                                                                          *
;************************************************************************************************
apu_song_jsr:
    JSR         apu_get_byte
    PHA

    JSR         apu_get_byte
    PHA                                                  

    LDA         apu_channel_index,X
    STA         apu_channel_ret,X
    LDA         apu_channel_index+1,X
    STA         apu_channel_ret+1,X

    PLA                                                  
    STA         apu_channel_index+1,X

    PLA                                                  
    STA         apu_channel_index,X

    JMP         apu_next_byte           ; continue

;************************************************************************************************
;*                                           FUNCTION                                           *
;* Name: apu_wave_vol_change* (0xFB)                                                            *
;* Description: Set wave channel volume to the next byte                                        *
;************************************************************************************************
apu_wave_vol_change:
    JSR         apu_get_byte
    CPX         #APU_CHANNEL_PULSE2
    BNE         not_pulse2

; if (pulse_2) {
        STA         apu_pulse2_cfg
; }

not_pulse2:
    STA         CHANNEL_VOL,Y
    JMP         apu_next_byte   ; continue

;************************************************************************************************
;*                                           FUNCTION                                           *
;* Name: apu_noise_vol_change (0xF8)                                                            *
;* Description: Set noise volume to the next byte                                               *
;************************************************************************************************
apu_noise_vol_change:
    JSR         apu_get_byte
    STA         NOISE_VOL                               
    JMP         apu_next_byte   ; continue

;************************************************************************************************
;*                                           FUNCTION                                           *
;* Name: apu_noise_play (0xE0-0xF5)                                                             *
;* Description: Play the given noise period for 254ms                                           *
;************************************************************************************************
apu_noise_play:
    AND         #NOISE_PERIOD_MASK
    STA         NOISE_PERIOD                             
    LDA         #channel_counter::length_254
    STA         NOISE_COUNTER                             
    BNE         apu_next_byte   ; continue

;************************************************************************************************
;*                                           FUNCTION                                           *
;* Name: apu_note_get (0xF9)                                                                    *
;* Description: Set note index to the next byte                                                 *
;************************************************************************************************
apu_note_get:
    JSR         apu_get_byte
    STA         apu_note_index
    JMP         apu_next_byte   ; continue

;************************************************************************************************
;*                                           FUNCTION                                           *
;* Name: apu_pulse_sweep_cfg* (0xFA)                                                            *
;* Description: Set given sweep to the next byte                                                *
;************************************************************************************************
apu_pulse_sweep_cfg:
    JSR         apu_get_byte
    STA         PULSE_SWEEP,Y
    JMP         apu_next_byte   ; continue

;************************************************************************************************
;*                                           FUNCTION                                           *
;* Name: apu_get_byte                                                                           *
;* Description: Retrieve the next byte from the channel buffer and increment                    *
;* Arguments:                                                                                   *
;*  X - Channel index                                                                           *
;* Returns:                                                                                     *
;*  A - The next byte                                                                           *
;************************************************************************************************
apu_get_byte:
    LDA         (apu_channel_index,X)

;************************************************************************************************
;*                                           FUNCTION                                           *
;* Name: apu_increment                                                                          *
;* Description: Increment the data pointer for a channel buffer                                 *
;* Arguments:                                                                                   *
;*  X - Channel index                                                                           *
;************************************************************************************************
apu_increment:
    INC         apu_channel_index,X
    BNE         no_carry
    INC         apu_channel_index+1,X
no_carry:
    RTS

;************************************************************************************************
;*                                           FUNCTION                                           *
;* Name: apu_wait_music                                                                         *
;* Description: Await completion of pulse 1, triangle, and sfx streams, unmasks any masked IRQ  *
;************************************************************************************************
    .export     apu_wait_music
apu_wait_music:
; do {
        CLI

        LDA         #apu_cmd_next_byte

        SEI

        LDX         #APU_CHANNEL_PULSE1
        CMP         (apu_channel_index,X)
        BEQ         apu_wait_music_done

        LDX         #APU_CHANNEL_SFX
        CMP         (apu_channel_index,X)
        BEQ         apu_wait_music_done

        LDX         #APU_CHANNEL_TRI
        CMP         (apu_channel_index,X)
        BNE         apu_wait_music
; } while (music_playing);

apu_wait_music_done:
    JSR         apu_increment

    CLI
    RTS

;************************************************************************************************
;*                                           FUNCTION                                           *
;* Name: apu_init                                                                               *
;* Description: Resets the APU back to initial configuration, masks IRQ to do so                *
;************************************************************************************************
    .export     apu_init
apu_init:
    SEI

    LDA         #0                                      
    STA         DMCFLAG                                 
    STA         DMCFRAMECOUNT                           
    STA         DMCSTATUS                               
    STA         apu_pulse1_len
    STA         apu_pulse2_len
    STA         apu_tri_len
    STA         apu_sfx_on

    LDA         #dmcstatus::pulse_1|dmcstatus::pulse_2|dmcstatus::tri|dmcstatus::noise
    STA         DMCSTATUS

    LDA         #$ff
    STA         apu_tempo

    LDA         #pulse_sweep::negate
    STA         PULSE1_SWEEP                            
    STA         PULSE2_SWEEP

    CLI                                                  
    RTS

;************************************************************************************************
;*                                           FUNCTION                                           *
;* Name: apu_play                                                                               *
;* Description: Trigger an APU object to start, masks IRQ to do so                              *
;* Arguments:                                                                                   *
;*  A - The asset to load                                                                       *
;************************************************************************************************
    .export     apu_play
apu_play:
    SEI

    TAX                                                  
    BMI         sfx
; if (not_sfx) {
        ASL         A
        STA         apu_buffer
        ASL         A
        ADC         apu_buffer
        ADC         #4
        TAY
        LDX         #4

    ; while (have_entries) {
        init_loop:
            LDA         apu_data_songs+1,Y
            BNE         no_song

        ; if (have_song) {
                LDA         apu_data_songs+1,X
                STA         apu_channel_index+1,X
                LDA         apu_data_songs,X
                JMP         load_triangle
        ; } else {
        no_song:
                STA         apu_channel_index+1,X
                LDA         apu_data_songs,Y
        ; }

        load_triangle:
            STA         apu_channel_index,X
            LDA         #1
            STA         apu_channel_len,X

            DEY
            DEY
            DEX
            DEX
            BPL         init_loop
    ; }

        LDA         #0
        STA         apu_note_index
        STA         apu_pulse1_trough
        STA         apu_pulse2_trough
        STA         apu_tri_trough

        CLI
        RTS
; } else {
sfx:
        ASL         A
        TAX
        LDA         #1
        STA         apu_sfx_on

        LDA         apu_data_sfx_table,X
        STA         apu_sfx_index
        LDA         apu_data_sfx_table+1,X
        STA         apu_sfx_index+1

        LDA         #pulse_sweep::negate
        STA         PULSE2_SWEEP
        LDA         #channel_vol::counter_halt|channel_vol::constant_vol
        STA         PULSE2_VOL

        STA         NOISE_VOL

        CLI
        RTS
; }
