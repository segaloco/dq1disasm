.include	"ppu_nmi_write.i"
.include	"general_bss.i"
.include	"map_calc.i"
.include	"ppu_bss.i"
.include	"game_bss.i"
.include	"enemies.i"
.include	"map.i"
.include	"rpg.i"
.include	"flags.i"
.include	"tunables.i"

.segment	"ZEROPAGE"

sprite_frame:		.byte	0

	.export sprite_npc_tbl, sprite_npc_x, sprite_npc_y, sprite_npc_z, sprite_npc_tbl_end
sprite_npc_tbl:
sprite_npc_x:		.byte 0
sprite_npc_y:		.byte 0
sprite_npc_z:		.byte 0
			.byte 0, 0, 0
			.byte 0, 0, 0
			.byte 0, 0, 0
			.byte 0, 0, 0
			.byte 0, 0, 0
			.byte 0, 0, 0
			.byte 0, 0, 0
			.byte 0, 0, 0
			.byte 0, 0, 0
			.byte 0, 0, 0
			.byte 0, 0, 0
			.byte 0, 0, 0
			.byte 0, 0, 0
			.byte 0, 0, 0
			.byte 0, 0, 0
			.byte 0, 0, 0
			.byte 0, 0, 0
			.byte 0, 0, 0

	.export sprite_npc_lora_xpos, sprite_npc_lora_ypos, sprite_npc_lora_offset
sprite_npc_lora_xpos:	.byte 0
sprite_npc_lora_ypos:	.byte 0
sprite_npc_lora_offset:	.byte 0
sprite_npc_tbl_end:

	.export sprite_check_flag
	.export sprite_char_x, sprite_char_y
	.export sprite_x, sprite_y
sprite_check_flag:	.byte 0
sprite_char_x:		.byte 0
sprite_char_y:		.byte 0
sprite_x:		.word 0
sprite_y:		.word 0

	.export math_rand_word, sprite_npc_stop
math_rand_word:		.word 0
sprite_npc_stop:	.byte 0
; ------------------------------------------------------------
.segment	"CODE"

	.export sprites_draw
sprites_draw:
	lda	battle_enemy
	cmp	#enemy::dragonlord
	bne	:+
	; ; if (battle_enemy == enemy::dragonlord) {
		rts
	: ; }

	lda	ppu_frame_count
	and	#ANI_FRAME_COUNT_MASK
	bne	:+ ; if (!(ppu_frame_count & ANI_FRAME_COUNT_MASK)) {
		lda	sprite_frame
		clc
		adc	#8
		sta	sprite_frame
	: ; }

	lda	game_player_flags
	and	#player_flags::lora_carry
	beq	:+ ; if (game_player_flags & lora_carry) {
		lda	#$C0
		sta	general_word_0
		bne	:+++
	: ; } else {
		lda	#$80
		sta	general_word_0

		lda	rpg_equip
		and	#$E0
		beq	:+ ; if (rpg_equip & $E0) {
			lda	#$90
			sta	general_word_0
		: ; }

		lda	rpg_equip
		and	#$03
		beq	:+ ; if (rpg_equip & $03) {
			lda	#$20
			ora	general_word_0
			sta	general_word_0
		; }
	: ; }

	lda	sprite_frame
	and	#$08
	ora	general_word_0
	tax
	ldy	#0
	lda	#$6F
	sta	general_word_0
	: ; for (general_word_0 = $6F; general_word_0 != $7F; general_word_0 += 8) {
		lda	#$80
		sta	general_word_0+1
		: ; for (general_word_0_1 = $80; general_word_0_1 = $90; general_word_0_1 += 8) {
			lda	game_buffer+$1D0
			cmp	#$FF
			beq	:+ ; if (game_buffer[$1D0] != $FF) {
				lda	#$F0
				bne	:++
			: ; } else {
				lda	general_word_0
			: ; }

			sta	oam_buffer,y
			iny
			lda	tbl_sprites,x
			sta	oam_buffer,y
			iny
			inx
			lda	tbl_sprites,x
			sta	oam_buffer,y
			iny
			inx
			lda	general_word_0+1
			sta	oam_buffer,y
			iny

			lda	general_word_0+1
			clc
			adc	#8
			sta	general_word_0+1
			cmp	#$90
			bne	:---
		; }

		lda	general_word_0
		clc
		adc	#8
		sta	general_word_0
		cmp	#$7F
		bne	:----
	; }

	lda	sprite_check_flag
	and	#$F0
	beq	:+ ; if (sprite_check_flag & $F0) {
		jmp	sprite_accounting
	: ; }

	lda	sprite_check_flag
	asl	a
	sta	general_word_0
	asl	a
	adc	general_word_0
	tax
	lda	#2
	sta	ppu_npc_loop_iter
	@loop: ; for (ppu_npc_loop_iter = 2; ppu_npc_loop_iter > 0; ppu_npc_loop_iter--) {
		lda	sprite_npc_x,x
		and	#PPU_POS_MASK
		bne	:+
		lda	sprite_npc_y,x
		and	#PPU_POS_MASK
		bne	:+ ; if (!(sprite_npc_x[x] & PPU_POS_MASK) && !(sprite_npc_y[x] & PPU_POS_MASK)) {
			jmp	@continue
		: ; }

		lda	ppu_frame_count
		and	#ANI_FRAME_COUNT_MASK
		cmp	#ANI_OBJ_FRAME_ID
		beq	:+ ; if ((ppu_frame_count & ANI_FRAME_COUNT_MASK) != ANI_OBJ_FRAME_ID) {
			jmp	@skip
		: ;}

		lda	sprite_npc_stop
		beq	:+ ; if (sprite_npc_stop) {
		@halt:
			asl	sprite_npc_y,x
			lsr	sprite_npc_y,x
			jmp	@continue
		: ; }

		jsr	math_rand
		lda	sprite_npc_y,x
		and	#$9F
		sta	sprite_npc_y,x
		lda	math_rand_word+1
		and	#$60
		ora	sprite_npc_y,x
		sta	sprite_npc_y,x

		jsr	sprite_pos_normalize
		jsr	sprite_win_chk

		lda	general_word_2+1
		beq	:+ ; if (!general_word_2_1) {
			lda	general_word_2
			cmp	#$FF
			bne	@halt
		: ; }

		jsr	area_check
		lda	sprite_npc_y,x
		and	#$60
		bne	:+ ; if (!(sprite_npc_y[x] & $60)) {
			dec	map_npc_ypos
			jmp	:++++
		:
		cmp	#$20
		bne	:+ ; } else if (sprite_npc_y[x] == $20) {
			inc	map_npc_xpos
			jmp	:+++
		:
		cmp	#$40
		bne	:+ ; } else if (sprite_npc_y[x] == $40) {
			inc	map_npc_ypos
			jmp	:++
		: ; } else {
			dec	map_npc_xpos
		: ; }

		lda	map_height
		cmp	map_npc_ypos
		bcs	:+ ; if (map_npc_ypos >= map_height) {
			jmp	@halt
		: ; }

		lda	map_width
		cmp	map_npc_xpos
		bcc	@halt

		jsr	sprite_win_chk
		lda	general_word_2+1
		beq	:+ ; if (general_word_2_1) {
			lda	general_word_2
			cmp	#$FF
			bne	@halt
		: ; }

		lda	map_npc_xpos
		cmp	map_player_x
		bne	:+ ; if (map_npc_xpos == map_player_x) {
			lda	map_npc_ypos
			cmp	map_player_y
			beq	@halt
		: ; }

		lda	map_npc_xpos
		cmp	sprite_char_x
		bne	:+ ; if (map_npc_xpos == sprite_char_x) {
			lda	map_npc_ypos
			cmp	sprite_char_y
			beq	@halt
		: ; }

		ldy	#0
		: ; for (index = 0; index < NPC_MAX, index++) {
			lda	sprite_npc_x,y
			and	#PPU_POS_MASK
			cmp	map_npc_xpos
			bne	:+
			lda	sprite_npc_y,y
			and	#PPU_POS_MASK
			cmp	map_npc_ypos
			bne	:+ ; if ((sprite_npc_x[y] & PPU_POS_MASK) == map_npc_xpos && (sprite_npc_y[y] & PPU_POS_MASK) == map_npc_xpos_1) {
				jmp	@halt
			: ; }

			iny
			iny
			iny
			cpy	#(sprite_npc_tbl_end-sprite_npc_tbl)
			bne	:--
		; }

		lda	general_word_0+1
		pha
		lda	map_npc_xpos
		sta	map_blk_col
		lda	map_npc_ypos
		sta	map_blk_row
		jsr	map_get_blkid
		jsr	area_data_check
		pla
		cmp	area_check_retval
		beq	:+ ; if (old_general_word_0_1 != area_check_retval) {
			jmp	@halt
		: ; }

		lda	general_word_0
		cmp	#$0D
		bcc	:+ ; if (general_word_0 < $0D) {
			jmp	@halt
		: ; }

		lda	sprite_npc_y,x
		ora	#$80
		sta	sprite_npc_y,x

	@skip:
		lda	sprite_npc_y,x
		bmi	:+ ; if (sprite_npc_y[x] > 0) {
			jmp	@continue
		: ; }

		lda	sprite_npc_stop
		beq	:+ ; if (sprite_npc_stop) {
			jmp	@continue
		: ; }

		lda	sprite_npc_y,x
		and	#$60
		bne	:+ ; if (!(sprite_npc_y[x] & $60)) {
			lda	sprite_npc_z,x
			and	#$0F
			sec
			sbc	#$01
			and	#$0F
			sta	general_word_0
			lda	sprite_npc_z,x
			and	#$F0
			ora	general_word_0
			sta	sprite_npc_z,x
			lda	general_word_0
			cmp	#$0F
			bne	@continue
			dec	sprite_npc_y,x
			jmp	@continue
		:

		cmp	#$20
		bne	:+ ; } else if (sprite_npc_y == $20) {
			lda	sprite_npc_z,x
			and	#$F0
			clc
			adc	#$10
			sta	general_word_0
			lda	sprite_npc_z,x
			and	#$0F
			ora	general_word_0
			sta	sprite_npc_z,x
			lda	general_word_0
			bne	@continue
			inc	sprite_npc_x,x
			jmp	@continue
		:

		cmp	#$40
		bne	:+ ; } else if (sprite_npc_y == $40) {
			lda	sprite_npc_z,x
			and	#$0F
			clc
			adc	#$01
			and	#$0F
			sta	general_word_0
			lda	sprite_npc_z,x
			and	#$F0
			ora	general_word_0
			sta	sprite_npc_z,x
			lda	general_word_0
			bne	@continue
			inc	sprite_npc_y,x
			jmp	@continue
		: ; } else {
			lda	sprite_npc_z,x
			and	#$F0
			sec
			sbc	#$10
			sta	general_word_0
			lda	sprite_npc_z,x
			and	#$0F
			ora	general_word_0
			sta	sprite_npc_z,x
			lda	general_word_0
			cmp	#$F0
			bne	@continue
			dec	sprite_npc_x,x
	@continue: ; }
		inx
		inx
		inx
		dec	ppu_npc_loop_iter
		beq	:+
		jmp	@loop
	: ; }

	ldx	#0
	lda	#$10
	sta	ppu_npc_loop_iter
	@loop2: ; for (index = 0; index < NPC_MAX; index) {
		lda	sprite_npc_x,x
		and	#PPU_POS_MASK
		bne	:+
		lda	sprite_npc_y,x
		and	#PPU_POS_MASK
		bne	:+
		; if (!(sprite_npc_x[x] & PPU_POS_MASK) && !(sprite_npc_y[x] & PPU_POS_MASK)) {
			jmp	@continue2
		: ; }

		jsr	sprite_calc_x

		lda	general_word_1
		clc
		adc	#7
		sta	general_word_1
		lda	general_word_1+1
		adc	#0
		beq	:++ ; if (general_word_1_1) {
			cmp	#1
			beq	:+ ; if (general_word_1_1 != 1) {
				jmp	@continue2
			: ; }

			lda	general_word_1
			cmp	#7
			bcc	:+ ; if (general_word_1 < 7) {
				jmp	@continue2
			; }
		: ; }

		jsr	sprite_calc_y

		lda	general_word_2
		clc
		adc	#$11
		sta	general_word_2
		lda	general_word_2+1
		adc	#0
		beq	:+ ; if (general_word_2_1) {
			jmp	@continue2
		: ; }

		jsr	sprite_pos_normalize
		jsr	sprite_win_chk

		lda	general_word_2+1
		beq	:+
		lda	general_word_2
		cmp	#$FF
		beq	:+ ; if (general_word_2_1 && general_word_2 != $FF) {
			jmp	@continue2
		: ; }

		lda	map_npc_xpos
		sta	general_word_0
		lda	map_npc_ypos
		sta	general_word_1
		jsr	area_check
		lda	area_check_retval
		cmp	map_cover_stat
		beq	:+ ; if (area_check_retval != map_cover_stat) {
			jmp	@continue2
		: ; }

		lda	sprite_npc_x,x
		and	#$E0
		lsr	a
		sta	general_word_0
		cmp	#$60
		bne	:+++ ; if (general_word_0 == $60) {
			lda	map_eng_number
			cmp	#maps::castle_courtyard
			bne	:+ ; if (map_eng_number == castle_courtyard) {
				lda	game_story_flags
				and	#story_flags::defeated_dragonlord
				bne	:++
			: ; } else {
				lda	map_eng_number
				cmp	#maps::castle_throneroom
				bne	@almostdone
			: ; }

			lda	#$D0
			sta	general_word_0
			bne	@almostdone
		: ; }

		lda	general_word_0
		cmp	#$50
		bne	:++ ; if (general_word_0 == $50) {
			lda	map_eng_number
			cmp	#maps::castle_courtyard
			bne	:+
			lda	game_story_flags
			and	#story_flags::defeated_dragonlord
			beq	:+ ; if (map_eng_number == castle_courtyard && !(game_story_flags & defeated_dragonlord)) {
				lda	#$F0
				sta	general_word_0

			@finally:
				lda	rpg_level
				cmp	#$FF
				bne	@done
				lda	general_word_0
				ora	#$08
				sta	general_word_0
				bne	@done
			: ; }

			lda	map_eng_number
			cmp	#maps::map_06
			bne	@almostdone
			lda	#$E0
			sta	general_word_0
			bne	@almostdone
		: ; }

		cmp	#$70
		beq	@finally

	@almostdone:
		lda	sprite_frame
		and	#$08
		ora	general_word_0
		sta	general_word_0

	@done:
		jsr	sprite_calc_x
		jsr	sprite_calc_y
		ldy	ppu_npc_loop_iter
		stx	ppu_npc_loop_iter
		ldx	general_word_0

		lda	#0
		sta	general_word_0
		: ; for (general_word_0 = 0; general_word_0 < $10; general_word_0 += 8) {
			lda	#0
			sta	general_word_0+1
			: ; for (general_word_0_1 = 0; general_word_0_1 < $10; general_word_0_1 += 8) {
				lda	general_word_1
				clc
				adc	general_word_0+1
				sta	map_npc_xpos
				lda	general_word_1+1
				adc	#0
				bne	:+ ; if (general_word_1_1) {
					lda	map_npc_xpos
					sta	oam_buffer+3,y
					lda	general_word_2
					clc
					adc	general_word_0
					sta	oam_buffer,y
					lda	tbl_sprites,x
					sta	oam_buffer+1,y
					lda	tbl_sprites+1,x
					sta	oam_buffer+2,y
					iny
					iny
					iny
					iny
				: ; }

				inx
				inx
				tya
				beq	@continue3

				lda	general_word_0+1
				clc
				adc	#8
				sta	general_word_0+1
				cmp	#$10
				bne	:--
			; }

			lda	general_word_0
			clc
			adc	#8
			sta	general_word_0
			cmp	#$10
			bne	:---
		; }

		ldx	ppu_npc_loop_iter
		sty	ppu_npc_loop_iter

	@continue2:
		inx
		inx
		inx
		cpx	#(sprite_npc_tbl_end-sprite_npc_tbl)
		beq	@continue3
		jmp	@loop2
	@continue3: ; }

	ldy	ppu_npc_loop_iter
	lda	#$F0
	: ; while (y != 0) {
		cpy	#0
		beq	:+
		sta	oam_buffer,y
		iny
		iny
		iny
		iny
		jmp	:-
	: ; }

sprite_accounting:
	lda	ppu_frame_count
	and	#ANI_FRAME_COUNT_MASK
	beq	:+ ; if (ppu_frame_count & ANI_FRAME_COUNT_MASK) {
		rts
	: ; }

	lda	sprite_check_flag
	cmp	#$FF
	beq	:+ ; if (sprite_check_flag != $FF) {
		inc	sprite_check_flag
		lda	sprite_check_flag
		cmp	#5
		bne	:+ ; if (++sprite_check_flag == 5) {
			lda	#0
			sta	sprite_check_flag
		; }
	: ; }

	rts

sprite_pos_normalize:
	lda	sprite_npc_x,x
	and	#PPU_POS_MASK
	sta	map_npc_xpos
	lda	sprite_npc_y,x
	and	#PPU_POS_MASK
	sta	map_npc_ypos
	rts

sprite_win_chk:
	lda	#0
	sta	general_word_2+1
	lda	map_npc_xpos
	sec
	sbc	map_player_x
	clc
	adc	#PPU_H_INIT
	sta	nt_calc_column
	cmp	#$10
	bcc	:+ ; if (nt_calc_column > $10) {
		rts
	: ; }

	lda	map_npc_ypos
	sec
	sbc	map_player_y
	clc
	adc	#PPU_V_INIT
	sta	nt_calc_row
	cmp	#$0F
	bcc	:+ ; if (nt_calc_row > $0F) {
		rts
	: ; }

	jsr	nt_calc_ppu_addr
	ldy	#0
	lda	(ppu_nmi_write_dest),y
	sta	general_word_2
	lda	#$FF
	sta	general_word_2+1
	rts

sprite_calc_x:
	lda	sprite_npc_x,x
	and	#PPU_POS_MASK
	sta	general_word_1+1
	lda	sprite_npc_z,x
	sta	general_word_1
	lsr	general_word_1+1
	ror	general_word_1
	lsr	general_word_1+1
	ror	general_word_1
	lsr	general_word_1+1
	ror	general_word_1
	lsr	general_word_1+1
	ror	general_word_1
	lda	general_word_1
	sec
	sbc	sprite_x
	sta	general_word_1
	lda	general_word_1+1
	sbc	sprite_x+1
	sta	general_word_1+1
	lda	general_word_1
	eor	#$80
	sta	general_word_1
	bmi	:+
		inc	general_word_1+1
	:

	rts

sprite_calc_y:
	lda	sprite_npc_y,x
	and	#PPU_POS_MASK
	sta	general_word_2+1
	lda	#0
	sta	general_word_2
	lsr	general_word_2+1
	ror	general_word_2
	lsr	general_word_2+1
	ror	general_word_2
	lsr	general_word_2+1
	ror	general_word_2
	lsr	general_word_2+1
	ror	general_word_2
	lda	sprite_npc_z,x
	and	#$0F
	ora	general_word_2
	sta	general_word_2
	sec
	sbc	sprite_y
	sta	general_word_2
	lda	general_word_2+1
	sbc	sprite_y+1
	sta	general_word_2+1
	lda	general_word_2
	clc
	adc	#$6F
	sta	general_word_2
	bcc	:+
		inc	general_word_2+1
	:

	rts
