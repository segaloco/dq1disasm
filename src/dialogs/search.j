.include	"system/cpu.i"
.include	"system/cnrom.i"

.include	"ppu_nmi.i"
.include	"apu_commands.i"
.include	"math.i"
.include	"general_bss.i"
.include	"ppu_bss.i"
.include	"game_bss.i"
.include	"map.i"
.include	"map_calc.i"
.include	"rpg.i"
.include	"window.i"
.include	"sprite.i"
.include	"dialogs.i"
.include	"flags.i"
.include	"tunables.i"
.include	"charmap.i"
.include	"macros.i"

	.export dialog_search_check
dialog_search_check:
	window_function_m	((window_dialog-tbl_windows)/FUNC_WINDOW_DEF_SIZE)
	window_low_m	((str_0D2_offset-tbl_string_offsets)/WORD_SIZE)
	lda	map_eng_number
	cmp	#maps::overworld
	bne	:++++
	lda	map_player_x
	cmp	#$53
	bne	:++++
	lda	map_player_y
	cmp	#$71
	bne	:++++ ; if (map == overworld && player_x == $53 && player_y == $71) {
		lda	#item::token_loto
	:
		sta	dialog_item_idx
		jsr	dialog_inv
		cmp	#$FF
		beq	:+ ; if (dialog_inv() != $FF) {
		dialog_search_end:
			lda	#<((str_0D3_offset-tbl_string_offsets)/WORD_SIZE)
			jmp	dialog_end
		: ; }

		lda	dialog_item_idx
		clc
		adc	#$31
		jsr	dialog_string_base
		window_low_m	((str_0D5_offset-tbl_string_offsets)/WORD_SIZE)
		lda	dialog_item_idx
		jsr	dialog_inv_add
		cpx	#4
		beq	:+ ; if (dialog_inv_add() != 4) {
			jmp	dialog_resume_game
		: ; } else {
			lda	dialog_item_idx
			jmp	dialog_inv_discard
		; }

	: lda	map_eng_number
	cmp	#maps::map_07
	bne	:+
	lda	map_player_x
	cmp	#9
	bne	:+
	lda	map_player_y
	cmp	#6
	bne	:+ ; } else if (map == map_07 && player_x == 9 && player_y == 6) {
		lda	#5
		bne	:----

	: lda	map_eng_number
	cmp	#maps::map_03
	bne	:+
	lda	map_player_x
	cmp	#$12
	bne	:+
	lda	map_player_y
	cmp	#$0C
	bne	:+
	lda	rpg_equip
	and	#$1C
	cmp	#$1C
	beq	dialog_search_end ; } else if (map == map_03 && player_x == $12 && player_y == $0C && rpg_equip != $1C) {
		lda	rpg_equip
		ora	#$1C
		sta	rpg_equip
		lda	#$28
		jsr	dialog_string_base
		lda	#<((str_0D5_offset-tbl_string_offsets)/WORD_SIZE)
		jmp	dialog_end

	: lda	map_eng_number
	cmp	#maps::map_02
	bne	:++
	lda	map_player_x
	cmp	#$0A
	bne	:++
	lda	map_player_y
	cmp	#3
	bne	:+ ; } else if (map == map_02 && player_x == $0A && player_y == 3) {
		lda	#<((str_0D6_offset-tbl_string_offsets)/WORD_SIZE)
		jmp	dialog_end

	: cmp	#$01
	bne	:+
	lda	rpg_flags+1
	and	#state_flags::dragonlord_open>>8
	bne	:+ ; } else if (map == overworld && !dragonlord_open) {
		lda	rpg_flags+1
		ora	#state_flags::dragonlord_open>>8
		sta	rpg_flags+1
		lda	#$0F
		jsr	dialog_string_base
		window_low_m	((str_0D5_offset-tbl_string_offsets)/WORD_SIZE)
		lda	#0
		sta	map_tile_ypos
		sta	map_tile_xpos
		sta	ppu_blkdel_flags
		jsr	map_block_mod
		jmp	dialog_resume_game
	: ; } else {
		lda	map_player_x
		sta	map_blk_col
		lda	map_player_y
		sta	map_blk_row
		jsr	map_get_blkid
		lda	map_blkid
		cmp	#$0C
		bne	:+ ; if (blkid == $0C) {
			lda	#<((str_0D4_offset-tbl_string_offsets)/WORD_SIZE)
			jmp	dialog_end
		: ; } else {
			lda	#<((str_0D3_offset-tbl_string_offsets)/WORD_SIZE)
			jmp	dialog_end
		; }
	; }
; ------------------------------------------------------------
reward	= zeroword

	.export	dialog_take_check
dialog_take_check:
	window_function_m	((window_dialog-tbl_windows)/FUNC_WINDOW_DEF_SIZE)
	lda	map_player_x
	sta	map_blk_col
	lda	map_player_y
	sta	map_blk_row
	jsr	map_get_blkid
	lda	map_blkid
	cmp	#$0C
	beq	:+ ; if (blkid != $0C) {
	dialog_no_treasure:
		lda	#<((str_0D7_offset-tbl_string_offsets)/WORD_SIZE)
		jmp	dialog_end
	: ; }

	movw_m	CNROM_BANK_INDEX_3_H|tbl_treasures_addr, cnrom_load_src
	movw_m	ppu_nmi_buffer+$20, cnrom_load_dest
	movw_m	tbl_treasures_end-tbl_treasures, cnrom_load_count
	jsr	cnrom_load

	ldy	#0
	: ; for (y = 0; buf[y] != { map_eng_number, map_player_x, map_player_y } && y < DIALOG_TREASURE_CNROM_SIZE; y += 4) {
		lda	map_eng_number
		cmp	tbl_treasures,y
		bne	:+
		lda	map_player_x
		cmp	tbl_treasures+1,y
		bne	:+
		lda	map_player_y
		cmp	tbl_treasures+2,y
		beq	:++
		: ; if (buffer[map] != map_eng_number) {
			iny
			iny
			iny
			iny
			cpy	#<(tbl_treasures_end-tbl_treasures)
			bne	:--
			beq	dialog_no_treasure
		: ; }
	; }

	lda	tbl_treasures+3,y
	sta	dialog_item_idx ; switch (dialog_item_idx) {
	cmp	#3
	bne	:+++ ; case 3:
		lda	rpg_keys
		cmp	#KEY_MAX
		bne	:+ ; if (rpg_keys >= KEY_MAX) {
		dialog_trytreasure:
			jsr	dialog_dotreasure
			lda	#<((str_0DA_offset-tbl_string_offsets)/WORD_SIZE)
			jmp	dialog_end
		: ; } else {
			inc	rpg_keys

			: jsr	dialog_dotreasure
			lda	#<((str_0D9_offset-tbl_string_offsets)/WORD_SIZE)
			jmp	dialog_end
		; }

	: cmp	#2
	bne	:+ ; case 2:
		lda	rpg_herbs
		cmp	#HERB_MAX
		beq	dialog_trytreasure

		inc	rpg_herbs
		bne	:--

	: cmp	#$0E
	bne	:+++ ; case $0E:
		lda	game_player_flags
		and	#player_flags::own_cursed_necklace
		bne	:++
		jsr	math_rand
		lda	math_rand_word+1
		and	#$1F
		bne	:++ ; if (!own_cursed_necklace && math_rand()) {
			lda	game_player_flags
			ora	#player_flags::own_cursed_necklace
			sta	game_player_flags

		dialog_treasure_set:
			jsr	dialog_dotreasure
			lda	dialog_item_idx
			sec
			sbc	#3
			sta	dialog_item_idx
			window_low_m	((str_0D9_offset-tbl_string_offsets)/WORD_SIZE)
			lda	dialog_item_idx
			jsr	dialog_inv_add
			cpx	#4
			beq	:+ ; if (dialog_inv_add() != 4) {
				jmp	dialog_resume_game
			: ; } else { 
				lda	dialog_item_idx
				jmp	dialog_inv_discard
			; }
		: ; }

		lda	#$1F
		sta	reward_add_mask
		movw_m	$64, reward
		jmp	dialog_treasure_calc

	: cmp	#$11
	bne	:++ ; case $11:
		lda	rpg_equip
		and	#$E0
		cmp	#$E0
		bne	:+ ; if () {
		dialog_trytreasure_2:
			jmp	dialog_trytreasure
		: ; }

		lda	rpg_equip
		ora	#$E0
		sta	rpg_equip
		jsr	dialog_dotreasure
		lda	#$21
		jsr	dialog_string_base
		lda	#<((str_0D9_offset-tbl_string_offsets)/WORD_SIZE)
		jmp	dialog_end

	: cmp	#$0D
	bne	:++ ; case $0D:
		lda	#item::harp
		jsr	dialog_inv
		cmp	#$FF
		bne	dialog_trytreasure_2
		lda	#item::staff
		jsr	dialog_inv
		cmp	#$FF
		bne	dialog_trytreasure_2

		: lda	#item::drop
		jsr	dialog_inv
		cmp	#$FF
		bne	dialog_trytreasure_2
		jmp	dialog_treasure_set

	: cmp	#$0F
	bne	:+ ; case $0F:
		lda	#item::stones
		jsr	dialog_inv
		cmp	#$FF
		bne	dialog_trytreasure_2
		beq	:--

	: cmp	#$11
	bcs	:+ ; case <= $11:
		jmp	dialog_treasure_set

	: cmp	#$12
	bne	:+ ; case $12:
		lda	#$0F
		sta	reward_add_mask
		movw_m	5, reward
		beq	dialog_treasure_calc

	: cmp	#$13
	bne	:+ ; case $13:
		lda	#7
		sta	reward_add_mask
		movw_m	6, reward
		beq	dialog_treasure_calc

	: cmp	#$14
	bne	:+ ; case $14:
		lda	#7
		sta	reward_add_mask
		movw_m	$A, reward
		beq	dialog_treasure_calc

	: cmp	#$15
	bne	:+ ; case $15:
		lda	#$FF
		sta	reward_add_mask
		movw_m	$1F4, reward
		bne	dialog_treasure_calc

	: cmp	#$16
	bne	:+ ; case $16:
		lda	#0
		sta	reward_add_mask
		sta	reward+1
		lda	#$78
		sta	reward
		bne	dialog_treasure_calc

	: ; default:
		jsr	dialog_dotreasure
		ldx	#0
		: ; for (x = 0; x < dialog_tablet_desc_len; x++) {
			lda	dialog_tablet_desc,x
			sta	dialog_buffer,x
			inx
			cpx	#dialog_tablet_desc_end-dialog_tablet_desc
			bne	:-
		; }

		window_low_m	((str_0D9_offset-tbl_string_offsets)/WORD_SIZE)
		window_high_m	((str_103_offset-tbl_string_offsets)/WORD_SIZE)
		jmp	dialog_resume_game
	; }

dialog_tablet_desc:
	a_dq_charmap_str("せきばん\s")
dialog_tablet_desc_end:

dialog_treasure_calc:
	jsr	math_rand
	lda	math_rand_word+1
	and	reward_add_mask
	clc
	adc	reward
	sta	reward
	lda	reward+1
	adc	#0
	sta	reward+1
	jsr	dialog_dotreasure
	lda	rpg_gold
	clc
	adc	reward
	sta	rpg_gold
	lda	rpg_gold+1
	adc	reward+1
	sta	rpg_gold+1
	bcc	:+ ; if (rpg_gold > $FFFF) {
		lda	#$FF
		sta	rpg_gold
		sta	rpg_gold+1
	: ; }

	window_low_m	((str_0D8_offset-tbl_string_offsets)/WORD_SIZE)
	window_function_m	((window_main_field-tbl_windows)/FUNC_WINDOW_DEF_SIZE)
	jmp	dialog_resume_game

dialog_dotreasure:
	ldx	#0
	: ; for (x = 0; x < map_chest_tbl_size; x += 2) {
		lda	map_chest_x_pos,x
		ora	map_chest_y_pos,x
		beq	:+

		inx
		inx
		cpx	#map_chest_tbl_end-map_chest_tbl
		bne	:-
	; } ; if (!found) {
		rts
	: ; } else {
		lda	map_player_x
		sta	map_chest_x_pos,x
		lda	map_player_y
		sta	map_chest_y_pos,x
		lda	#0
		sta	map_tile_xpos
		sta	map_tile_ypos
		sta	ppu_blkdel_flags
		apusfx_m	sfx_chest
		jsr	apu_play
		jsr	map_block_mod
		lda	dialog_item_idx
		clc
		adc	#$2E
		jmp	dialog_string_base
	; }
	.end
