.include	"system/cpu.i"

.include	"general_bss.i"
.include	"ppu_nmi.i"
.include	"rpg.i"
.include	"window.i"
.include	"tunables.i"

cost	= zeroword

	.export dialog_keys
dialog_keys:
	sec
	sbc	#$0C
	tax
	lda	tbl_costs_keys,x
	sta	cost
	lda	#0
	sta	cost+1
	window_low_m	((str_016_offset-tbl_string_offsets)/WORD_SIZE)
	: ; for () {
		window_function_m	((window_decision-tbl_windows)/FUNC_WINDOW_DEF_SIZE)
		beq	:+ ; if (window_decision() == no) {
		dialog_keys_end:
			window_low_m	((str_012_offset-tbl_string_offsets)/WORD_SIZE)
			jmp	dialog_resume_game
		: ; }

		lda	rpg_keys
		cmp	#KEY_MAX
		bne	:+ ; if (rpg_keys == KEY_MAX) {
			window_low_m	((str_014_offset-tbl_string_offsets)/WORD_SIZE)
			jmp	dialog_keys_end
		: ; }

		lda	rpg_gold
		sec
		sbc	cost
		sta	general_word_0
		lda	rpg_gold+1
		sbc	cost+1
		sta	general_word_0+1
		bcs	:+ ; if () {
			window_low_m	((str_013_offset-tbl_string_offsets)/WORD_SIZE)
			jmp	dialog_keys_end
		: ; }

		lda	general_word_0
		sta	rpg_gold
		lda	general_word_0+1
		sta	rpg_gold+1
		inc	rpg_keys
		window_function_m	((window_main_field-tbl_windows)/FUNC_WINDOW_DEF_SIZE)
		window_low_m	((str_015_offset-tbl_string_offsets)/WORD_SIZE)
		jmp	:----
	; }
	.end
