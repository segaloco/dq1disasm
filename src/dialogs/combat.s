.include	"system/cnrom.i"
.include	"system/ppu.i"

.include	"bssmap.i"
.include	"ppu_nmi_write.i"
.include	"ppu_pal.i"
.include	"general_bss.i"
.include	"game_bss.i"
.include	"ppu_bss.i"
.include	"window.i"
.include	"map_calc.i"
.include	"map.i"
.include	"enemies.i"
.include	"tunables.i"
.include	"macros.i"

COMBAT_BG_WIDTH		= 14
COMBAT_BG_HEIGHT	= 14
WINDOW_BUF_PTR		= $50A
SPRITE_OAM_MAX	= $A0

COMBAT_OBJ_POS_V	= 68
COMBAT_OBJ_POS_V_MASK	= $3F
COMBAT_OBJ_POS_H_OFF	= 28
COMBAT_OBJ_POS_H	= 132

	.export dialog_combat, dialog_load_enemy
dialog_combat:
	lda	battle_enemy
	cmp	#enemy::dragonlord
	bne	:+ ; if (battle_enemy == dragonlord) {
		rts
	: ; }

	movw_m	CNROM_BANK_INDEX_3_H|dialog_combat_addr, cnrom_load_src
	lda	#>cnrom_buffer
	sta	cnrom_load_dest+1
	lda	#<cnrom_buffer
	sta	cnrom_load_dest
	movw_m	dialog_combat_ani_end-dialog_combat_ani, cnrom_load_count
	jsr	cnrom_load
	ldx	#0
	stx	ppu_blk_counter
	stx	window_block_count
	movw_m	WINDOW_BUF_PTR, general_word_0
	: ; for (x = 0, general_word_0 = WINDOW_BUF_PTR; x < $C4; x += $E, general_word_0 += $20) {
		ldy	#0
		: ; for (y = 0; y < COMBAT_BG_WIDTH; y++) {
			lda	map_eng_number
			cmp	#maps::overworld
			beq	:+
			lda	#<((tile_blank_chr00-chr00_base)/PPU_TILE_SIZE)
			bne	:++
			: ; if (map == overworld) {
				lda	map_battle_field,x
			: ; }

			sta	(general_word_0),y
			inx
			iny
			cpy	#COMBAT_BG_WIDTH
			bne	:---
		; }

		lda	general_word_0
		clc
		adc	#$20
		sta	general_word_0
		lda	general_word_0+1
		adc	#0
		sta	general_word_0+1
		cpx	#<(map_battle_field_end-map_battle_field)
		bne	:----
	; }

	jsr	ppu_nmi_wait
	jsr	sprites_draw

	: ; for (window_block_count = 0; window_block_count < 0x31; window_block_count++) {
		ldx	window_block_count
		lda	dialog_combat_ani,x
		lsr	a
		lsr	a
		lsr	a
		lsr	a
		clc
		adc	#$FA
		sta	map_tile_xpos
		clc
		adc	#$10
		sta	nt_calc_column
		lda	dialog_combat_ani,x
		and	#$0F
		clc
		adc	#$FA
		sta	map_tile_ypos
		clc
		adc	#$0E
		sta	nt_calc_row
		jsr	nt_calc_ram_addr
		lda	ppu_nmi_write_dest
		sta	window_block_addr
		lda	ppu_nmi_write_dest+1
		sta	window_block_addr+1
		jsr	ppu_nmi_wait
		jsr	window_clearbuf

		inc	window_block_count
		lda	window_block_count
		cmp	#$31
		bne	:-
	; }

	rts

dialog_load_enemy:
	lda	battle_enemy
	cmp	#enemy::dragonlord
	bne	:+++ ; if (battle_enemy == dragonlord) {
		movw_m	CNROM_BANK_INDEX_3_H|dragonlord_addr, cnrom_load_src
		lda	#0
		sta	cnrom_load_dest
		sta	battle_enemy
		lda	#>game_buffer
		sta	cnrom_load_dest+1
		movw_m	dragonlord_load_end-dragonlord_load, cnrom_load_count
		jsr	cnrom_load
		jsr	dragonlord_load

		ldy	#<game_buffer
		lda	#>game_buffer
		sta	window_block_addr+1
		sty	window_block_addr
		: ; for (i = game_buffer; i < LOWMEM_END; i++) {
			lda	#GAME_BUFFER_CLEAR
			sta	(window_block_addr),y
			inc	window_block_addr
			bne	:+
				inc	window_block_addr+1
			:

			lda	window_block_addr+1
			cmp	#>(LOWMEM_END)
			bne	:--
		; }

		rts
	: ; } else {
		lda	#0
		sta	cnrom_load_dest
		sta	cnrom_load_count+1
		lda	#>cnrom_buffer
		sta	cnrom_load_dest+1
		lda	#SPRITE_OAM_MAX
		sta	cnrom_load_count
		jsr	cnrom_load

		ldx	#$10
		ldy	#0
		sty	window_block_addr
		lda	#>cnrom_buffer
		sta	window_block_addr+1
		: ; for (x = 0x10, y = 0; y < 0x100; x += 4, y += 3) {
			lda	(window_block_addr),y
			beq	:++
			sta	oam_buffer+OBJ_CHR_NO,x

			iny
			lda	(window_block_addr),y
			and	#COMBAT_OBJ_POS_V_MASK
			clc
			adc	#COMBAT_OBJ_POS_V
			sta	oam_buffer+OBJ_POS_V,x

			lda	(window_block_addr),y
			and	#obj_attribute::flip_bits
			sta	general_word_0
			iny
			lda	(window_block_addr),y
			and	#obj_attribute::color_bits
			ora	general_word_0
			sta	general_word_0
			lda	(window_block_addr),y
			lsr	a
			lsr	a
			sec
			sbc	#COMBAT_OBJ_POS_H_OFF
			sta	general_word_0+1
			lda	ppu_nmi_apply_mirror
			beq	:+ ; if (apply_mirror) {
				lda	general_word_0+1
				eor	#<~0
				sta	general_word_0+1
				inc	general_word_0+1
				lda	general_word_0
				eor	#obj_attribute::h_flip
				sta	general_word_0
			: ; }
			lda	general_word_0
			sta	oam_buffer+OBJ_ATTR,x

			lda	general_word_0+1
			clc
			adc	#COMBAT_OBJ_POS_H
			sta	oam_buffer+OBJ_POS_H,x

			inx
			inx
			inx
			inx
			iny
			bne	:--
		: ; }

		jsr	battle_palette_load_enemy
		lda	#PPU_PAL_BG_FALSE
		sta	ppu_pal_fade_has_bg
		lda	#PPU_COLOR_LEV3
		sta	ppu_pal_load_cutoff
		jsr	ppu_pal_load_obj
		jsr	ppu_pal_fadein
		jmp	ppu_pal_fadein
	; }
	.end
