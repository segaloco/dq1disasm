.include	"system/cpu.i"

.include	"ppu_nmi.i"
.include	"ppu_pal.i"
.include	"apu_commands.i"
.include	"general_bss.i"
.include	"game_bss.i"
.include	"rpg.i"
.include	"window.i"
.include	"tunables.i"
.include	"macros.i"

cost	= zeroword

	.export dialog_inn
dialog_inn:
	sec
	sbc	#NPC_INKEEP_START
	tax
	lda	tbl_costs_inns,x
	sta	cost
	lda	#0
	sta	cost+1
	window_low_m	((str_00B_offset-tbl_string_offsets)/WORD_SIZE)
	window_function_m	((window_decision-tbl_windows)/FUNC_WINDOW_DEF_SIZE)
	beq :+ ; if (window_decision() == no) {
		jsr	window_string_low_s
	dialog_inn_end:
		.byte	<((str_00A_offset-tbl_string_offsets)/WORD_SIZE)
		jmp	dialog_resume_game
	: ; }

	lda	rpg_gold
	sec
	sbc	cost
	sta	dialog_sale_gold_diff
	lda	rpg_gold+1
	sbc	cost+1
	sta	dialog_sale_gold_diff+1
	bcs	:+ ; if (rpg_gold < cost) {
		window_low_m	((str_022_offset-tbl_string_offsets)/WORD_SIZE)
		jmp	dialog_inn_end
	: ; }

	lda	dialog_sale_gold_diff
	sta	rpg_gold
	lda	dialog_sale_gold_diff+1
	sta	rpg_gold+1
	window_function_m	((window_main_field-tbl_windows)/FUNC_WINDOW_DEF_SIZE)
	window_low_m	((str_009_offset-tbl_string_offsets)/WORD_SIZE)
	jsr	dialog_inn_palfade
	jsr	ppu_pal_fadeout
	apusong_m	apu_song_inn
	jsr	apu_play
	lda	rpg_maxhp
	sta	rpg_hp
	lda	rpg_maxmp
	sta	rpg_mp
	window_function_m	((window_main_field-tbl_windows)/FUNC_WINDOW_DEF_SIZE)
	jsr	dialog_inn_palfade
	jsr	apu_wait_music
	apusong_m	apu_song_town
	jsr	apu_play

	jsr	ppu_pal_fadein
	lda	game_player_flags
	lsr	a
	bcc	:+ ; if (lora_carry) {
		window_low_m	((str_006_offset-tbl_string_offsets)/WORD_SIZE)
		jmp	:++
	: ; } else {
		window_low_m	((str_008_offset-tbl_string_offsets)/WORD_SIZE)
	: ; }

	window_low_m	((str_007_offset-tbl_string_offsets)/WORD_SIZE)
	jmp	dialog_resume_game

dialog_inn_palfade:
	movw_m	ppu_pal_obj_fade, ppu_pal_fade_obj
	movw_m	ppu_pal_bg_fade, ppu_pal_fade_bg
	lda	#PPU_PAL_BG_TRUE
	sta	ppu_pal_fade_has_bg
	rts
	.end
