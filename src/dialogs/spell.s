.include	"system/cpu.i"
.include	"system/ppu.i"

.include	"apu_commands.i"
.include	"general_bss.i"
.include	"game_bss.i"
.include	"map_calc.i"
.include	"math.i"
.include	"map.i"
.include	"window.i"
.include	"dialogs.i"
.include	"sprite.i"
.include	"rpg.i"
.include	"flags.i"
.include	"tunables.i"

	.export dialog_spell_check
	.export dialog_spell_wing
	.export dialog_spell_flash
	.export dialog_spell
	.export dialog_spell_mpcheck
	.export dialog_spell_heal, dialog_spell_midheal
dialog_spell_check:
	lda	rpg_flags
	sta	general_word_1
	lda	rpg_flags+1
	and	#(state_flags::sizzle|state_flags::midheal)>>8
	sta	general_word_1+1
	ora	general_word_1
	bne	:+ ; if (!(rpg_flags & spells)) {
		window_function_m	((window_dialog-tbl_windows)/FUNC_WINDOW_DEF_SIZE)
		window_low_m		((str_031_offset-tbl_string_offsets)/WORD_SIZE)
		jmp	dialog_resume_game
	: ; }

	jsr	dialog_spell
	cmp	#$FF
	bne	:+ ; if (dialog_spell() == $FF) {
		jmp	dialog_command_clear
	: ; }

	pha
	window_function_m	((window_dialog-tbl_windows)/FUNC_WINDOW_DEF_SIZE)
	pla
	jsr	dialog_spell_mpcheck ; switch (dialog_spell_mpcheck) {
	cmp	#<((str_032_offset-tbl_string_offsets)/WORD_SIZE)
	bne	:+ ; case ((str_032_offset-tbl_string_offsets)/WORD_SIZE):
		jsr	window_string_low_a
		jmp	dialog_resume_game

	: cmp	#<((str_000_offset-tbl_string_offsets)/WORD_SIZE)
	bne	:+ ; case ((str_000_offset-tbl_string_offsets)/WORD_SIZE):
		jsr	dialog_spell_heal
		jmp	dialog_resume_game

	: cmp	#<((str_001_offset-tbl_string_offsets)/WORD_SIZE)
	bne	:+ ; case ((str_001_offset-tbl_string_offsets)/WORD_SIZE):
		   ; case ((str_002_offset-tbl_string_offsets)/WORD_SIZE):
	dialog_spell_fizzout:
		window_low_m	((str_033_offset-tbl_string_offsets)/WORD_SIZE)
		jmp	dialog_resume_game

	: cmp	#<((str_002_offset-tbl_string_offsets)/WORD_SIZE)
	beq	dialog_spell_fizzout
	cmp	#<((str_003_offset-tbl_string_offsets)/WORD_SIZE)
	bne	:+++ ; case ((str_003_offset-tbl_string_offsets)/WORD_SIZE):
		lda	map_type
		cmp	#map_types::dungeon
		bne	dialog_spell_fizzout

		lda	#SPELL_GLOW_TIME_MAX
		sta	game_glow_timer
		window_function_clear_m	((window_dialog-tbl_windows)/FUNC_WINDOW_DEF_SIZE)
		window_function_clear_m	((window_cmd_field-tbl_windows)/FUNC_WINDOW_DEF_SIZE)
		window_function_clear_m	((window_main_field-tbl_windows)/FUNC_WINDOW_DEF_SIZE)
		: ; for (rpg_glowdia; rpg_glowdia < dia_max; rpg_glowdia += dia_incr) {
			lda	rpg_glowdia
			cmp	#SPELL_GLOW_DIA_MAX
			bne	:+
				rts
			:

			clc
			adc	#SPELL_GLOW_DIA_INCR
			sta	rpg_glowdia
			apusfx_m	sfx_glow
			jsr	apu_play
			jsr	map_moveend
			jmp	:--
		; }

	: cmp	#<((str_007_offset-tbl_string_offsets)/WORD_SIZE)
	bne	:+ ; case ((str_007_offset-tbl_string_offsets)/WORD_SIZE):
		lda	#SPELL_PROTECT_TIME_MAX
		sta	game_protection_timer
		jmp	dialog_resume_game

	: cmp	#<((str_005_offset-tbl_string_offsets)/WORD_SIZE)
	bne	:++++++ ; case ((str_005_offset-tbl_string_offsets)/WORD_SIZE):
		lda	map_eng_number
		cmp	#maps::map_1C
		bcc	:+ ; if (map >= map_1C) {
			ldx	#$27
			jmp	dialog_mapswitch_high

		: cmp	#maps::map_18
		bcc	:+ ; } else if (map >= map_18) {
			ldx	#$39
			jmp	dialog_mapswitch_high

		: cmp	#maps::map_16
		bcc	:+ ; } else if (map >= map_16) {
			ldx	#$18
			jmp	dialog_mapswitch_high

		: cmp	#maps::dungeon_greendragon
		bne	:+ ; } else if (map == dungeon_greendragon) {
			ldx	#$0F
			jmp	dialog_mapswitch_high

		: cmp	#maps::map_0F
		bcs	:+
		cmp	#maps::map_06
		beq	:+ ; } else if (map <= map_0F && map != map_06) {
			jmp	dialog_spell_fizzout

		: ; } else {
			ldx	#$12
			jmp	dialog_mapswitch_high
		; }

	: cmp	#<((str_008_offset-tbl_string_offsets)/WORD_SIZE)
	bne	:+ ; case ((str_008_offset-tbl_string_offsets)/WORD_SIZE):
		jsr	dialog_spell_midheal
		jmp	dialog_resume_game

	: cmp	#<((str_006_offset-tbl_string_offsets)/WORD_SIZE)
	bne	:++++ ; case ((str_006_offset-tbl_string_offsets)/WORD_SIZE):
		lda	map_type
		cmp	#map_types::dungeon
		beq	:+
		lda	map_eng_number
		cmp	#maps::map_06
		bne	:++
		: ; if (map_type == dungeon || map_eng_number == map_06) {
			jmp	dialog_spell_fizzout
		: ; }

	dialog_spell_wing:
		lda	#maps::overworld
		sta	map_eng_number
		lda	#$2A
		sta	map_player_x
		sta	sprite_char_x
		sta	sprite_x
		lda	#$2B
		sta	map_player_y
		sta	sprite_char_y
		sta	sprite_y
		lda	#0
		sta	sprite_x+1
		sta	sprite_y+1

		ldx	#4
		: ; for (x = 4; x > 0; x--) {
			asl	sprite_x
			rol	sprite_x+1
			asl	sprite_y
			rol	sprite_y+1
			dex
			bne	:-
		; }

		apusfx_m	sfx_wing
		jsr	apu_play
		jmp	map_transition1

	: ; default:
		jmp	dialog_spell_fizzout

	; }

dialog_spell_flash:
	ldx	#8
	: ; for (x = 8; x > 0; x--) {
		jsr	ppu_nmi_wait
		jsr	ppu_nmi_wait
		lda	#ppu_ctlr1::objblk_off|ppu_ctlr1::bgblk_off|ppu_ctlr1::monotone
		sta	PPU_CTLR1
		jsr	ppu_nmi_wait
		jsr	ppu_nmi_wait
		jsr	ppu_nmi_wait
		lda	#ppu_ctlr1::objblk_off|ppu_ctlr1::bgblk_off|ppu_ctlr1::color
		sta	PPU_CTLR1
		dex
		bne	:-
	; }

	rts

dialog_spell:
	jsr	dialog_init_buffer
	lda	#2
	sta	general_word_2
	ldx	#1
	: ; for (general_word_2 = 2; general_word_2 < $C; general_word_2++) {
		lsr	general_word_1+1
		ror	general_word_1
		bcc	:+ ; if () {
			lda	general_word_2
			sta	dialog_buffer,x
			inx
		: ; }

		inc	general_word_2
		lda	general_word_2
		cmp	#$0C
		bne	:--
	; }

	lda	#$FF
	sta	dialog_buffer,x
	window_function_m	((window_spell-tbl_windows)/FUNC_WINDOW_DEF_SIZE)
	cmp	#$FF
	beq	:+ ; if (window_draw(6) != $FF) {
		tax
		lda	dialog_buffer+1,x
		sec
		sbc	#2
	: ; }

	rts

dialog_spell_mpcheck:
	sta	window_choice
	ldx	window_choice
	lda	rpg_mp
	cmp	tbl_mpcosts,x
	bcs	:+ ; if () {
		lda	#$32
		rts
	: ; }

	sbc	tbl_mpcosts,x
	sta	rpg_mp
	lda	window_choice
	clc
	adc	#$10
	jsr	dialog_string_base
	window_low_m	((str_030_offset-tbl_string_offsets)/WORD_SIZE)
	apusfx_m	sfx_spell
	jsr	apu_play
	jsr	dialog_spell_flash
	jsr	apu_wait_music
	lda	window_choice
	pha
	window_function_m	((window_main_field-tbl_windows)/FUNC_WINDOW_DEF_SIZE)
	pla
	rts

dialog_spell_heal:
	jsr	math_rand
	lda	math_rand_word+1
	and	#SPELL_HEAL_MAXOFFSET
	clc
	adc	#SPELL_HEAL_BASE

dialog_spell_doheal:
	clc
	adc	rpg_hp
	bcs	:+
	cmp	rpg_maxhp
	bcc	:++
	: ; if (rpg_hp + > $FF || rpg_hp + > rpg_maxhp) {
		lda	rpg_maxhp
	: ; }
	sta	rpg_hp
	jsr	battle_palette_normal
	window_function_m	((window_main_field-tbl_windows)/FUNC_WINDOW_DEF_SIZE)
	rts

dialog_spell_midheal:
	jsr	math_rand
	lda	math_rand_word+1
	and	#SPELL_MIDHEAL_MAXOFFSET
	clc
	adc	#SPELL_MIDHEAL_BASE
	jmp	dialog_spell_doheal
	.end
