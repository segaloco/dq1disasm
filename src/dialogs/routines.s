.include	"system/cpu.i"
.include	"system/cnrom.i"

.include	"ppu_pal.i"
.include	"map.i"
.include	"sprite.i"
.include	"general_bss.i"
.include	"window.i"
.include	"macros.i"

DIALOG_BANK_SIZE	= $99
BUFFER_INIT_SIZE	= $B

.segment	"ZEROPAGE"

	.export dialog_buffer, dialog_buffer_end
dialog_buffer:
	.res	18, 0
dialog_buffer_end:
; ------------------------------------------------------------ 
.segment	"CODE"

	.export dialog_init_buffer
dialog_init_buffer:
	ldx	#0
	lda	#1
	: ; for (x = 0; x < $B; x++) {
		sta	dialog_buffer,x
		inx
		clc
		adc	#1
		cpx	#BUFFER_INIT_SIZE
		bne	:-
	; }
	rts

	.export dialog_mapchange_low, dialog_mapswitch_low
dialog_mapchange_low:
	jsr	dialog_bank_low
	ldx	#0
	: ; for (entry of dialog_bank_low) {
		lda	map_eng_number
		cmp	ppu_nmi_buffer,x
		bne	dialog_mapswitch_low
		lda	map_player_x
		cmp	ppu_nmi_buffer+1,x
		bne	dialog_mapswitch_low
		lda	map_player_y
		cmp	ppu_nmi_buffer+2,x
		bne	dialog_mapswitch_low ; if () {
			jsr	dialog_bank_high
			lda	ppu_nmi_buffer,x
			sta	map_eng_number
			lda	ppu_nmi_buffer+1,x
			sta	map_player_x
			sta	sprite_char_x
			sta	sprite_x
			lda	ppu_nmi_buffer+2,x
			sta	map_player_y
			sta	sprite_char_y
			sta	sprite_y

		dialog_doscenechange:
			lda	#0
			sta	sprite_x+1
			sta	sprite_y+1
			ldx	#4
			: ; for (x = 4; x > 0; x--) {
				asl	sprite_x
				rol	sprite_x+1
				asl	sprite_y
				rol	sprite_y+1
				dex
				bne	:-
			; }

			jmp	map_transition0
		; }

		.export dialog_mapswitch_low
	dialog_mapswitch_low:
		inx
		inx
		inx
		cpx	#DIALOG_BANK_SIZE
		bne	:--
	; }

	window_function_m	((window_dialog-tbl_windows)/FUNC_WINDOW_DEF_SIZE)
	window_low_m	((str_00E_offset-tbl_string_offsets)/WORD_SIZE)
	jmp	dialog_resume_game

	.export dialog_bank_low
dialog_bank_low:
	txa
	pha
	movw_m	CNROM_BANK_INDEX_3_H|map_transitions_low, cnrom_load_src
	lda	#0
	sta	cnrom_load_dest
	sta	cnrom_load_count+1
	lda	#>(ppu_nmi_buffer)
	sta	cnrom_load_dest+1
	lda	#DIALOG_BANK_SIZE
	sta	cnrom_load_count
	jsr	cnrom_load
	pla
	tax
	rts

	.export dialog_bank_high
dialog_bank_high:
	txa
	pha
	movw_m	CNROM_BANK_INDEX_3_H|dialog_chr_high_addr, cnrom_load_src
	lda	#0
	sta	cnrom_load_dest
	sta	cnrom_load_count+1
	lda	#>(ppu_nmi_buffer)
	sta	cnrom_load_dest+1
	lda	#DIALOG_BANK_SIZE
	sta	cnrom_load_count
	jsr	cnrom_load
	pla
	tax
	rts

	.export dialog_stairs_check, dialog_mapswitch_high
dialog_stairs_check:
	lda	map_player_x
	sta	map_blk_col
	lda	map_player_y
	sta	map_blk_row
	jsr	map_get_blkid
	lda	map_blkid
	cmp	#5 ; switch (blkid) {
	bne	:+ ; case 5:
		jmp	dialog_mapchange_low

	: cmp	#3
	bne	:+++ ; case 3:
		jsr	dialog_bank_high
		ldx	#0
		: ; for (entry of dialog_bank_high) {
			lda	map_eng_number
			cmp	ppu_nmi_buffer,x
			bne	:+
			lda	map_player_x
			cmp	ppu_nmi_buffer+1,x
			bne	:+
			lda	map_player_y
			cmp	ppu_nmi_buffer+2,x
			bne	:+ ; if () {
			dialog_mapswitch_high:
				jsr	dialog_bank_low
				lda	ppu_nmi_buffer,x
				sta	map_eng_number
				lda	ppu_nmi_buffer+1,x
				sta	map_player_x
				sta	sprite_char_x
				sta	sprite_x
				lda	ppu_nmi_buffer+2,x
				sta	map_player_y
				sta	sprite_char_y
				sta	sprite_y
				jmp	dialog_doscenechange
			: ; }

			inx
			inx
			inx
			cpx	#DIALOG_BANK_SIZE
			bne	:--
		; }

	: ; default:
		window_function_m	((window_dialog-tbl_windows)/FUNC_WINDOW_DEF_SIZE)
		window_low_m	((str_00D_offset-tbl_string_offsets)/WORD_SIZE)
		jmp	dialog_resume_game
	; }
