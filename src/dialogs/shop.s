.include	"system/cpu.i"

.include	"ppu_nmi.i"
.include	"general_bss.i"
.include	"game_bss.i"
.include	"ppu_bss.i"
.include	"rpg.i"
.include	"window.i"
.include	"dialogs.i"
.include	"tunables.i"

cost	= zeroword

	.export dialog_weapons
dialog_weapons:
	sta	dialog_item_idx
	window_low_m	((str_028_offset-tbl_string_offsets)/WORD_SIZE)

	dialog_weapons_loop: ; for () {
		window_function_m	((window_decision-tbl_windows)/FUNC_WINDOW_DEF_SIZE)
		beq	:+ ; if (window_decision() == no) {
			jmp	dialog_weapons_end
		: ; }
		
		window_low_m	((str_02D_offset-tbl_string_offsets)/WORD_SIZE)
		jsr	dialog_shop
		lda	window_choice
		cmp	#$FF
		bne	:+ ; if (window_choice == $FF) {
			jmp	dialog_weapons_end
		: ; }

		lda	tbl_shopitems,x
		pha
		clc
		adc	#$1B
		jsr	dialog_string_base
		window_low_m	((str_029_offset-tbl_string_offsets)/WORD_SIZE)
		lda	#0
		sta	cost
		sta	cost+1
		pla
		asl	a
		tax
		lda	rpg_gold
		sec
		sbc	tbl_costs,x
		lda	rpg_gold+1
		sbc	tbl_costs+1,x
		bcs	:+ ; if (rpg_gold < tbl_costs) {
			jmp	dialog_weapons_cantbuy
		: ; }

		txa
		pha
		cmp	#$0E
		bcs	:+ ; if (shop_item < $0E) {
			lda	rpg_equip
			lsr	a
			lsr	a
			lsr	a
			lsr	a
			lsr	a
			beq	:++++
			bne	:+++
		: cmp	#$1C
		bcs	:+ ; } else if (shop_item < $1C) {
			lda	rpg_equip
			lsr	a
			lsr	a
			and	#$07
			beq	:+++

			clc
			adc	#7
			bne	:++
		: ; } else { 
			lda	rpg_equip
			and	#$03
			beq	:++

			clc
			adc	#$E
		: ; }

		; if (equipped) {
			asl	a
			tay
			lda	tbl_costs-WORD_SIZE,y
			sta	cost
			lda	tbl_costs-WORD_SIZE+1,y
			sta	cost+1
			lsr	cost+1
			ror	cost
			tya
			lsr	a
			clc
			adc	#$1A
			jsr	dialog_string_base
			window_low_m	((str_02A_offset-tbl_string_offsets)/WORD_SIZE)
		: ; }

		window_low_m	((str_027_offset-tbl_string_offsets)/WORD_SIZE)
		window_function_m	((window_decision-tbl_windows)/FUNC_WINDOW_DEF_SIZE)
		pla
		tax
		lda	window_choice
		beq	:+ ; if (window_choice == no) {
			window_low_m	((str_026_offset-tbl_string_offsets)/WORD_SIZE)
			jmp	dialog_weapons_prob2
		: ; }

		lda	rpg_gold
		sec
		sbc	tbl_costs,x
		sta	rpg_gold
		lda	rpg_gold+1
		sbc	tbl_costs+1,x
		sta	rpg_gold+1
		lda	rpg_gold
		clc
		adc	cost
		sta	rpg_gold
		lda	rpg_gold+1
		adc	cost+1
		sta	rpg_gold+1
		bcc	:+ ; if (rpg_gold > $FFFF) {
			lda	#$FF
			sta	rpg_gold
			sta	rpg_gold+1
		: ; }

		txa
		lsr	a
		cmp	#$07
		bcs	:++ ; if (shop_item < $07) {
			clc
			adc	#1
			asl	a
			asl	a
			asl	a
			asl	a
			asl	a
			sta	general_word_0
			lda	rpg_equip
			and	#$1F
			ora	general_word_0
			sta	rpg_equip

			: window_low_m	((str_02E_offset-tbl_string_offsets)/WORD_SIZE)
			window_function_m	((window_main_field-tbl_windows)/FUNC_WINDOW_DEF_SIZE)
			jmp	dialog_weapons_prob2
		: cmp	#$0E
		bcs	:+ ; } else if (shop_item < $0E) {
			sec
			sbc	#6
			asl	a
			asl	a
			sta	general_word_0
			lda	rpg_equip
			and	#$E3
			ora	general_word_0
			sta	rpg_equip
			bne	:--
		: ; } else {
			sec
			sbc	#$0D
			sta	general_word_0
			lda	rpg_equip
			and	#$FC
			ora	general_word_0
			sta	rpg_equip
			bne	:---
		; }

	dialog_weapons_cantbuy:
		window_low_m	((str_02B_offset-tbl_string_offsets)/WORD_SIZE)

	dialog_weapons_prob2:
		window_low_m	((str_02C_offset-tbl_string_offsets)/WORD_SIZE)
		jmp	dialog_weapons_loop
	; }

dialog_weapons_end:
	window_low_m	((str_02F_offset-tbl_string_offsets)/WORD_SIZE)
	jmp	dialog_resume_game

dialog_shop:
	ldx	#0
	lda	dialog_item_idx
	sta	general_word_0
	beq	:++
	: ; for (x = 0, dialog_item_idx; tbl_shopitems[x] != SHOP_LIST_END && dialog_item_idx > 0; x++, dialog_item_idx--) {
		lda	tbl_shopitems,x
		inx
		cmp	#SHOP_LIST_END
		bne	:-
		dec	general_word_0
		bne	:-
	: ; }

	txa
	pha

	ldy	#1
	sty	dialog_buffer
	: ; for (y = 1; dialog_buffer[y] != $FF && y < $100; y++) {
		lda	tbl_shopitems,x
		clc
		adc	#2
		sta	dialog_buffer,y
		cmp	#$FF
		beq	:+
		inx
		iny
		bne	:-
	: ; }

	window_function_m	((window_shop-tbl_windows)/FUNC_WINDOW_DEF_SIZE)
	pla
	clc
	adc	window_choice
	tax
	rts

	.export dialog_tools
dialog_tools:
	sta	dialog_item_idx
	window_low_m	((str_025_offset-tbl_string_offsets)/WORD_SIZE)
	window_function_m	((window_buysell-tbl_windows)/FUNC_WINDOW_DEF_SIZE)
	beq	:++
	cmp	#1
	bne	:+ ; if (window_buysell() == sell) {
		jmp	dialog_tools_sell_loop
	: ; } else if (window_function($A) != 0) {
	dialog_tools_end:
		window_low_m	((str_01E_offset-tbl_string_offsets)/WORD_SIZE)
		jmp	dialog_resume_game
	: ; }

	dialog_tools_loop: ; for () {
		window_low_m	((str_024_offset-tbl_string_offsets)/WORD_SIZE)
		jsr	dialog_shop
		lda	window_choice
		cmp	#$FF
		beq	dialog_tools_end

		lda	tbl_shopitems,x
		pha
		clc
		adc	#$1F
		jsr	dialog_string_base
		pla
		asl	a
		tax
		lda	rpg_gold
		sec
		sbc	tbl_costs,x
		sta	dialog_sale_gold_diff
		lda	rpg_gold+1
		sbc	tbl_costs+1,x
		sta	dialog_sale_gold_diff+1
		bcs	:+ ; if (rpg_gold < tbl_costs) {
			window_low_m	((str_022_offset-tbl_string_offsets)/WORD_SIZE)
			jmp	dialog_tools_cantbuy
		: ; }

		cpx	#<(tbl_costs_tools-tbl_costs)
		bne	:+++ ; if (shop_item == herb) {
			lda	rpg_herbs
			cmp	#HERB_MAX
			bne	:+ ; if (rpg_herbs == HERB_MAX) {
				window_low_m	((str_020_offset-tbl_string_offsets)/WORD_SIZE)
				jmp	dialog_tools_cantbuy
			: ; }

			inc	rpg_herbs

		dialog_tools_buy:
			lda	dialog_sale_gold_diff
			sta	rpg_gold
			lda	dialog_sale_gold_diff+1
			sta	rpg_gold+1
			window_low_m	((str_023_offset-tbl_string_offsets)/WORD_SIZE)
			window_function_m	((window_main_field-tbl_windows)/FUNC_WINDOW_DEF_SIZE)

		dialog_tools_cantbuy:
			window_low_m	((str_01F_offset-tbl_string_offsets)/WORD_SIZE)
			window_function_m	((window_decision-tbl_windows)/FUNC_WINDOW_DEF_SIZE)
			bne	:+ ; if (window_decision() == yes) {
				jmp	dialog_tools_loop
			: ; } else { 
				jmp	dialog_tools_end
			; }
		: ; }

		txa
		lsr	a
		sec
		sbc	#$12
		jsr	dialog_inv_add
		cpx	#4
		bne	dialog_tools_buy

		window_low_m	((str_021_offset-tbl_string_offsets)/WORD_SIZE)
		jmp	dialog_tools_cantbuy
	; }

dialog_tools_sell_loop:
	jsr	dialog_inv_list
	cpx	#1
	bne	:+ ; if (dialog_inv_list() == 1) {
		window_low_m	((str_019_offset-tbl_string_offsets)/WORD_SIZE)
		jmp	dialog_tools_end
	: ; }

	window_low_m	((str_01D_offset-tbl_string_offsets)/WORD_SIZE)
	window_function_m	((window_tool-tbl_windows)/FUNC_WINDOW_DEF_SIZE)
	cmp	#$FF
	bne	:+ ; if (window_function(7) == $FF) {
		jmp	dialog_tools_end
	: ; }

	tax

	lda	dialog_buffer+1,x
	sta	dialog_item_idx
	clc
	adc	#$2E
	jsr	dialog_string_base
	lda	dialog_item_idx
	clc
	adc	#$0F
	asl	a
	tax
	lda	tbl_costs,x
	sta	cost
	lda	tbl_costs+1,x
	sta	cost+1
	ora	cost
	bne	:++ ; if (!cost) {
		window_low_m	((str_01B_offset-tbl_string_offsets)/WORD_SIZE)

	dialog_tools_sell_done:
		window_low_m	((str_01A_offset-tbl_string_offsets)/WORD_SIZE)
		window_function_m	((window_decision-tbl_windows)/FUNC_WINDOW_DEF_SIZE)
		bne	:+ ; if (window_decision() == yes) {
			jmp	dialog_tools_sell_loop
		: ; } else {
			jmp	dialog_tools_end
		; }
	: ; } else {
		lsr	cost+1
		ror	cost
		window_low_m	((str_01C_offset-tbl_string_offsets)/WORD_SIZE)
		window_function_m	((window_decision-tbl_windows)/FUNC_WINDOW_DEF_SIZE)
		bne	dialog_tools_sell_done ; if (window_decision() == yes) {
			lda	dialog_item_idx ; switch (dialog_item_idx) {
			cmp	#3
			bne	:++ ; case 3:
				dec	rpg_keys

			dialog_tools_get_paid:
				lda	rpg_gold
				clc
				adc	cost
				sta	rpg_gold
				lda	rpg_gold+1
				adc	cost+1
				sta	rpg_gold+1
				bcc	:+ ; if (rpg_gold > $FFFF) {
					lda	#$FF
					sta	rpg_gold
					sta	rpg_gold+1
				: ; }

				window_function_m	((window_main_field-tbl_windows)/FUNC_WINDOW_DEF_SIZE)
				jmp	dialog_tools_sell_done

			: cmp	#2
			bne	:+ ; case 2:
				dec	rpg_herbs
				jmp	dialog_tools_get_paid

			: cmp	#$0C
			bne	:++ ; case $C:
				pha
				bit	rpg_flags+1
				bvc	:+++

			:
				pla
				window_low_m	((str_018_offset-tbl_string_offsets)/WORD_SIZE)
				window_low_m	((str_017_offset-tbl_string_offsets)/WORD_SIZE)
				jmp	dialog_tools_sell_done

			: cmp	#$0E
			bne	:++ ; case $E:
				pha
				lda	rpg_flags+1
				bmi	:--

			:
				pla

			: ; default:
				sec
				sbc	#3
				jsr	dialog_inv_del
				jmp	dialog_tools_get_paid
			; }
		; }
	; }
	.end
