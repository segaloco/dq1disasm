.include	"system/cpu.i"

.include	"ppu_nmi.i"
.include	"general_bss.i"
.include	"rpg.i"
.include	"window.i"
.include	"tunables.i"

cost	= zeroword

	.export dialog_fairy
dialog_fairy:
	lda	#<FAIRY_COST
	sta	cost
	lda	#>FAIRY_COST
	sta	cost+1
	window_low_m	((str_011_offset-tbl_string_offsets)/WORD_SIZE)

	: ; for () {
		window_function_m	((window_decision-tbl_windows)/FUNC_WINDOW_DEF_SIZE)
		beq	:+ ; if () {
		dialog_fairy_end:
			window_low_m	((str_00C_offset-tbl_string_offsets)/WORD_SIZE)
			jmp	dialog_resume_game
		: ; }

		lda	rpg_gold
		sec
		sbc	cost
		sta	dialog_sale_gold_diff
		lda	rpg_gold+1
		sbc	cost+1
		sta	dialog_sale_gold_diff+1
		bcs	:+ ; if (rpg_gold < cost) {
			window_low_m	((str_022_offset-tbl_string_offsets)/WORD_SIZE)
			jmp	dialog_fairy_end
		: ; }

		lda	#item::fairy_water
		jsr	dialog_inv_add
		cpx	#4
		bne	:+ ; if (dialog_inv_add(fairy_water) == 4) {
			window_low_m	((str_021_offset-tbl_string_offsets)/WORD_SIZE)
			jmp	dialog_fairy_end
		: ; }

		lda	dialog_sale_gold_diff
		sta	rpg_gold
		lda	dialog_sale_gold_diff+1
		sta	rpg_gold+1
		window_function_m	((window_main_field-tbl_windows)/FUNC_WINDOW_DEF_SIZE)
		window_low_m	((str_010_offset-tbl_string_offsets)/WORD_SIZE)
		jmp	:----
	; }
	.end
