.include	"ppu_pal.i"
.include	"window.i"
.include	"general_bss.i"
.include	"game_bss.i"
.include	"ppu_bss.i"
.include	"npc.i"
.include	"rpg.i"
.include	"joypad.i"
.include	"dialogs.i"
.include	"tunables.i"

	.export	dialog_command, dialog_command_clear
dialog_command:
	: ; while ((ppu_frame_count & ANI_FRAME_COUNT_MASK) != ANI_OBJ_FRAME_ID) {
		jsr	ppu_nmi_wait
		lda	ppu_frame_count
		and	#ANI_FRAME_COUNT_MASK
		cmp	#ANI_OBJ_FRAME_ID
		beq	:+
		jsr	sprites_draw
		jmp	:-
	: ; }

	lda	#NPCS_STOP
	sta	sprite_npc_stop
	window_function_m	((window_main_field-tbl_windows)/FUNC_WINDOW_DEF_SIZE)
	window_function_m	((window_cmd_field-tbl_windows)/FUNC_WINDOW_DEF_SIZE)
	cmp	#$FF
	bne	:+ ; if (window_command() == $FF) {
	dialog_command_clear:
		window_function_clear_m ((window_cmd_field-tbl_windows)/FUNC_WINDOW_DEF_SIZE)
		lda	#NPCS_RUN
		sta	sprite_npc_stop
		rts
	: ; }

	lda	window_choice ; switch (window_choice) {
	cmp	#1
	bne	:+ ; case 1:
		jsr	stats_load
		jsr	dialog_init_buffer
		lda	rpg_equip
		lsr	a
		lsr	a
		lsr	a
		lsr	a
		lsr	a
		clc
		adc	#9
		sta	dialog_buffer+8
		lda	rpg_equip
		lsr	a
		lsr	a
		and	#$07
		clc
		adc	#$11
		sta	dialog_buffer+9
		lda	rpg_equip
		and	#$03
		clc
		adc	#$19
		sta	dialog_buffer+10
		window_function_m	((window_stats-tbl_windows)/FUNC_WINDOW_DEF_SIZE)
		jsr	dialog_wait_button
		window_function_clear_m	((window_stats-tbl_windows)/FUNC_WINDOW_DEF_SIZE)
		jmp	dialog_command_clear

	: cmp	#0 ; case 0:
		beq	dialog_cmd_end

	cmp	#2
	bne	:+ ; case 2:
		jmp	dialog_stairs_check

	: cmp	#3
	bne	:+ ; case 3:
		jmp	dialog_door_check

	: cmp	#4
	bne	:+ ; case 4:
		jmp	dialog_spell_check

	: cmp	#5
	bne	:+ ; case 5:
		jmp	dialog_inv_check

	: cmp	#6
	bne	:+ ; case 6:
		jmp	dialog_search_check

	: ; default:
		jmp	dialog_take_check
	; }

	.export dialog_resume_game
dialog_resume_game:
	jsr	dialog_wait_button
	window_function_clear_m	((window_dialog-tbl_windows)/FUNC_WINDOW_DEF_SIZE)
	jmp	dialog_command_clear

	.export dialog_wait_button
dialog_wait_button:
	: ; do {
		jsr	ppu_nmi_wait
		jsr	joypad_read
		lda	joypad_state
	bne	:- ; } while (joypad_state);

	: ; do {
		jsr	ppu_nmi_wait
		jsr	joypad_read
		lda	joypad_state
	beq	:- ; } while (!joypad_state);
	rts

dialog_cmd_end:

	.end
