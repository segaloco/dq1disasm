.include	"system/cpu.i"

.include	"ppu_nmi.i"
.include	"ppu_nmi_write.i"
.include	"apu_commands.i"
.include	"general_bss.i"
.include	"game_bss.i"
.include	"ppu_bss.i"
.include	"joypad.i"
.include	"math.i"
.include	"window.i"
.include	"map.i"
.include	"map_calc.i"
.include	"flags.i"
.include	"tunables.i"
.include	"rpg.i"
.include	"dialogs.i"
.include	"macros.i"

	.export dialog_inv_check, dialog_door_check
	.export dialog_inv_doherb
	.export dialog_inv_dragonscale
	.export dialog_inv_ring
	.export dialog_inv_cursedbelt, dialog_inv_cursednecklace
	.export dialog_inv_list
	.export dialog_inv_add
	.export dialog_inv_del
	.export dialog_inv
	.export dialog_inv_discard
dialog_inv_check:
	jsr	dialog_inv_list
	cpx	#1
	bne	:+ ; if (dialog_inv_list() == 1) {
		window_function_m	((window_dialog-tbl_windows)/FUNC_WINDOW_DEF_SIZE)
		window_low_m	((str_03D_offset-tbl_string_offsets)/WORD_SIZE)
		jmp	dialog_resume_game
	: ; }

	window_function_m	((window_tool-tbl_windows)/FUNC_WINDOW_DEF_SIZE)
	cmp	#$FF
	bne	:+ ; if (window_function(7) == $FF) {
		jmp	dialog_command_clear
	: ; }

	tax
	lda	dialog_buffer+1,x
	cmp	#3
	beq	dialog_door_check
	jmp	:++++++ ; switch((dialog_buffer+1)[x]) {
	dialog_door_check: ; case 3:
		lda	map_player_x
		sta	map_blk_col
		lda	map_player_y
		sta	map_blk_row
		dec	map_blk_row
		jsr	map_get_blkid
		lda	map_blkid
		cmp	#$11
		beq	:+
		lda	map_player_x
		sta	map_blk_col
		lda	map_player_y
		sta	map_blk_row
		inc	map_blk_row
		jsr	map_get_blkid
		lda	map_blkid
		cmp	#$11
		beq	:+
		lda	map_player_x
		sta	map_blk_col
		lda	map_player_y
		sta	map_blk_row
		dec	map_blk_col
		jsr	map_get_blkid
		lda	map_blkid
		cmp	#$11
		beq	:+
		lda	map_player_x
		sta	map_blk_col
		lda	map_player_y
		sta	map_blk_row
		inc	map_blk_col
		jsr	map_get_blkid
		lda	map_blkid
		cmp	#$11
		beq	:+ ; if (player_touching_blkid != $11) {
			window_function_m	((window_dialog-tbl_windows)/FUNC_WINDOW_DEF_SIZE)
			window_high_m	((str_10B_offset-tbl_string_offsets)/WORD_SIZE)
			jmp	dialog_resume_game
		: ; } else {
			lda	rpg_keys
			bne	:+ ; if (!rpg_keys) {
				window_function_m	((window_dialog-tbl_windows)/FUNC_WINDOW_DEF_SIZE)
				window_high_m	((str_10C_offset-tbl_string_offsets)/WORD_SIZE)
				jmp	dialog_resume_game
			: ; } else {
				dec	rpg_keys
				ldx	#0
				: ; for (x = 0; x < $10; x++) {
					lda	map_door_x_pos,x
					beq	:+

					inx
					inx
					cpx	#$10
					bne	:-
				; }

				jmp	dialog_resume_game
			; }
		: ; }

		lda	map_npc_xpos
		sta	map_door_x_pos,x
		lda	map_npc_ypos
		sta	map_door_y_pos,x
		lda	map_npc_xpos
		sec
		sbc	map_player_x
		asl	a
		sta	map_tile_xpos
		lda	map_npc_ypos
		sec
		sbc	map_player_y
		asl	a
		sta	map_tile_ypos
		lda	#0
		sta	ppu_blkdel_flags
		apusfx_m	sfx_door
		jsr	apu_play
		jsr	map_block_mod

		: ; while (joypad_read()) {
			jsr	joypad_read
			lda	joypad_state
			bne	:-
		; }

		jmp	dialog_command_clear

	: cmp	#2
	bne	:++ ; case 2:
		window_function_m	((window_dialog-tbl_windows)/FUNC_WINDOW_DEF_SIZE)
		window_low_m	((str_0F7_offset-tbl_string_offsets)/WORD_SIZE)
		dec	rpg_herbs
		jsr	dialog_inv_doherb
		jmp	dialog_resume_game

	dialog_inv_doherb:
		jsr	math_rand
		lda	math_rand_word+1
		and	#HERB_HEAL_MAXOFFSET
		clc
		adc	#HERB_HEAL_BASE
		adc	rpg_hp
		cmp	rpg_maxhp
		bcc	:+
			lda	rpg_maxhp
		:

		sta	rpg_hp
		jsr	battle_palette_normal
		window_function_m	((window_main_field-tbl_windows)/FUNC_WINDOW_DEF_SIZE)
		rts

	: cmp	#4
	bne	:++ ; case 4:
		lda	map_type
		cmp	#map_types::dungeon
		beq	:+ ; if (map_type != dungeon) {
			window_function_m	((window_dialog-tbl_windows)/FUNC_WINDOW_DEF_SIZE)
			window_low_m	((str_035_offset-tbl_string_offsets)/WORD_SIZE)
			jmp	dialog_resume_game
		: ; }

		lda	#item::torch
		jsr	dialog_inv_del
		lda	#0
		sta	game_glow_timer
		window_function_clear_m	((window_dialog-tbl_windows)/FUNC_WINDOW_DEF_SIZE)
		window_function_clear_m	((window_cmd_field-tbl_windows)/FUNC_WINDOW_DEF_SIZE)
		window_function_clear_m	((window_main_field-tbl_windows)/FUNC_WINDOW_DEF_SIZE)
		lda	#SPELL_GLOW_DIA_MIN
		sta	rpg_glowdia
		apusfx_m	sfx_glow
		jsr	apu_play
		jmp	map_moveend

	: cmp	#5
	bne	:+ ; case $5:
		window_function_m	((window_dialog-tbl_windows)/FUNC_WINDOW_DEF_SIZE)
		window_low_m	((str_036_offset-tbl_string_offsets)/WORD_SIZE)
		lda	#item::fairy_water
		jsr	dialog_inv_del
		lda	#$FE
		sta	game_protection_timer
		jmp	dialog_resume_game

	: cmp	#6
	bne	:+++ ; case 6:
		window_function_m	((window_dialog-tbl_windows)/FUNC_WINDOW_DEF_SIZE)
		lda	map_type
		cmp	#map_types::dungeon
		beq	:+
		lda	map_eng_number
		cmp	#maps::map_06
		bne	:++
		: ; if (map_type == dungeon || map_eng_number == map_06) {
			window_low_m	((str_038_offset-tbl_string_offsets)/WORD_SIZE)
			jmp	dialog_resume_game
		: ; }

		lda	#item::chimera_wing
		jsr	dialog_inv_del
		window_low_m	((str_039_offset-tbl_string_offsets)/WORD_SIZE)
		jsr	dialog_spell_flash
		jmp	dialog_spell_wing

	: cmp	#7
	bne	:+ ; case 7:
		window_function_m	((window_dialog-tbl_windows)/FUNC_WINDOW_DEF_SIZE)
		jsr	dialog_inv_dragonscale
		jmp	dialog_resume_game

	: cmp	#8
	bne	:+ ; case 8:
		window_function_m	((window_dialog-tbl_windows)/FUNC_WINDOW_DEF_SIZE)
		window_low_m	((str_03C_offset-tbl_string_offsets)/WORD_SIZE)
		apusong_m	apu_song_flute
		jsr	apu_play
		jsr	apu_wait_music
		ldx	map_eng_number
		lda	map_songs,x
		jsr	apu_play
		window_low_m	((str_033_offset-tbl_string_offsets)/WORD_SIZE)
		jmp	dialog_resume_game

	: cmp	#$09
	bne	:+ ; case 9:
		window_function_m	((window_dialog-tbl_windows)/FUNC_WINDOW_DEF_SIZE)
		jsr	dialog_inv_ring
		jmp	dialog_resume_game

	: cmp	#$0A
	bne	:++ ; case $A:
		window_function_m	((window_dialog-tbl_windows)/FUNC_WINDOW_DEF_SIZE)
		lda	#$38

	:
		jsr	dialog_string_base
		window_low_m	((str_040_offset-tbl_string_offsets)/WORD_SIZE)
		window_low_m	((str_033_offset-tbl_string_offsets)/WORD_SIZE)
		jmp	dialog_resume_game

	: cmp	#$0F
	bne	:+ ; case $F:
		window_function_m	((window_dialog-tbl_windows)/FUNC_WINDOW_DEF_SIZE)
		lda	#$3D
		bne	:--

	: cmp	#$10
	bne	:+ ; case $10:
		window_function_m	((window_dialog-tbl_windows)/FUNC_WINDOW_DEF_SIZE)
		lda	#$3E
		bne	:---

	: cmp	#$0D
	bne	:+++ ; case $0D:
		window_function_m	((window_dialog-tbl_windows)/FUNC_WINDOW_DEF_SIZE)
		window_low_m	((str_041_offset-tbl_string_offsets)/WORD_SIZE)
		apusong_m	apu_song_lyre
		jsr	apu_play
		jsr	apu_wait_music
		lda	map_eng_number
		cmp	#maps::overworld
		bne	:++ ; if (map == overworld) {
			: ; while () {
				jsr	math_rand
				lda	math_rand_word+1
				and	#$07
				cmp	#$05
				beq	:-
				cmp	#$07
				beq	:-
			; }

			pha
			window_function_clear_m	((window_dialog-tbl_windows)/FUNC_WINDOW_DEF_SIZE)
			window_function_clear_m ((window_cmd_field-tbl_windows)/FUNC_WINDOW_DEF_SIZE)
			window_function_clear_m	((window_main_field-tbl_windows)/FUNC_WINDOW_DEF_SIZE)
			pla
			jmp	battle_init
		: ; }

		ldx	map_eng_number
		lda	map_songs,x
		jsr	apu_play
		window_low_m	((str_033_offset-tbl_string_offsets)/WORD_SIZE)
		jmp	dialog_resume_game

	: cmp	#$0C
	bne	:+ ; case $0C:
		window_function_m	((window_dialog-tbl_windows)/FUNC_WINDOW_DEF_SIZE)
		jsr	dialog_inv_cursedbelt
		ldx	map_eng_number
		lda	map_songs,x
		jsr	apu_play
		jmp	dialog_resume_game

	: cmp	#$0E
	bne	:+ ; case $0E:
		window_function_m	((window_dialog-tbl_windows)/FUNC_WINDOW_DEF_SIZE)
		jsr	dialog_inv_cursednecklace
		ldx	map_eng_number
		lda	map_songs,x
		jsr	apu_play
		jmp	dialog_resume_game

	: cmp	#$11
	beq	:+
	jmp	:++++++
	: ; case $11:
		window_function_m	((window_dialog-tbl_windows)/FUNC_WINDOW_DEF_SIZE)
		window_low_m	((str_004_offset-tbl_string_offsets)/WORD_SIZE)
		lda	map_eng_number
		cmp	#maps::overworld
		bne	:++++
		lda	map_player_x
		cmp	#$41
		bne	:++++
		lda	map_player_y
		cmp	#$31
		bne	:++++
		lda	rpg_flags+1
		and	#state_flags::bridge_open>>8
		bne	:++++ ; if (map == overworld && player_x == $41 && player_y == $31 && !bridge_open) {
			window_function_clear_m	((window_dialog-tbl_windows)/FUNC_WINDOW_DEF_SIZE)
			window_function_clear_m	((window_cmd_field-tbl_windows)/FUNC_WINDOW_DEF_SIZE)
			window_function_clear_m	((window_main_field-tbl_windows)/FUNC_WINDOW_DEF_SIZE)
			apusong_m	apu_song_bridge
			jsr	apu_play
			lda	rpg_flags+1
			ora	#state_flags::bridge_open>>8
			sta	rpg_flags+1
			lda	#$FE
			sta	map_tile_xpos
			lda	#0
			sta	map_tile_ypos
			lda	#4
			sta	ppu_blkdel_flags
			: ; for (ppu_blkdel_flags = 4; ppu_blkdel_flags > 0; ppu_blkdel_flags--) {
				lda	#$21
				sta	ppu_nmi_write_data
				: ; for (ppu_nmi_write_data = $21; ppu_nmi_write_data < $2D; ppu_nmi_write_data++) {
					jsr	ppu_nmi_wait
					jsr	ppu_nmi_wait
					jsr	ppu_nmi_wait
					jsr	ppu_nmi_wait
					jsr	ppu_nmi_wait
					movw_m	PPU_COLOR_PAGE_BG+3, ppu_nmi_write_dest
					jsr	ppu_nmi_write
					inc	ppu_nmi_write_data
					lda	ppu_nmi_write_data
					cmp	#$12
					beq	:+

					cmp	#$2D
					bne	:-
					dec	ppu_blkdel_flags
					bne	:--
					lda	#$11
					sta	ppu_nmi_write_data
					bne	:-
				; }
			: ; }

			jsr	apu_wait_music
			apusong_m	apu_song_overworld
			jsr	apu_play
			jmp	map_block_mod
		: ; }

		lda	#<((str_005_offset-tbl_string_offsets)/WORD_SIZE)
		jmp	dialog_end

	: cmp	#$0B
	bne	:++++++++ ; case $B:
		window_function_m	((window_dialog-tbl_windows)/FUNC_WINDOW_DEF_SIZE)
		lda	rpg_level
		cmp	#<(LEVEL_MAX)
		bne	:+ ; if (rpg_level == LEVEL_MAX) {
			window_high_m	((str_105_offset-tbl_string_offsets)/WORD_SIZE)
			jmp	:++
		: ; } else { 
			jsr	stats_calc_exp
			window_low_m	((str_0DB_offset-tbl_string_offsets)/WORD_SIZE)
		: ; }

		lda	map_eng_number
		cmp	#maps::overworld
		bne	:+++++ ; if (map == overworld) {
			window_low_m	((str_0DC_offset-tbl_string_offsets)/WORD_SIZE)

			lda	map_player_y
			sec
			sbc	#$2B
			bcs	:+ ; if () {
				eor	#$FF
				sta	zeroword
				inc	zeroword
				lda	#<((str_0DF_offset-tbl_string_offsets)/WORD_SIZE)
				bne	:++
			: ; } else {
				sta	zeroword
				lda	#<((str_0DE_offset-tbl_string_offsets)/WORD_SIZE)
			: ; }

			ldx	#0
			stx	zeroword+1
			jsr	window_string_low_a

			lda	map_player_x
			sec
			sbc	#$2B
			bcs	:+ ; if () {
				eor	#$FF
				sta	zeroword
				inc	zeroword
				lda	#<((str_0E0_offset-tbl_string_offsets)/WORD_SIZE)
				bne	:++
			: ; } else {
				sta	zeroword
				lda	#<((str_0E1_offset-tbl_string_offsets)/WORD_SIZE)
			: ; }

			ldx	#0
			stx	zeroword+1
			jsr	window_string_low_a

			window_low_m	((str_0DD_offset-tbl_string_offsets)/WORD_SIZE)
		: ; }

		lda	#<((str_0BD_offset-tbl_string_offsets)/WORD_SIZE)
		jmp	dialog_end

	: ; default:
		jmp	dialog_resume_game
	; }

dialog_inv_list:
	ldx	#0
	lda	#1
	sta	dialog_buffer,x
	inx
	lda	rpg_herbs
	beq	:+ ; if (rpg_herbs) {
		lda	#2
		sta	dialog_buffer,x
		inx
	: ; }

	lda	rpg_keys
	beq	:+ ; if (rpg_keys) {
		lda	#3
		sta	dialog_buffer,x
		inx
	: ; }

	ldy	#0
	: ; for (y = 0; y < 4; y++) {
		lda	rpg_inv,y
		and	#$0F
		beq	:+ ; if (rpg_inv[y] & $0F) {
			clc
			adc	#3
			sta	dialog_buffer,x
			inx
		: ; }

		lda	rpg_inv,y
		and	#$F0
		beq	:+ ; if (rpg_inv[y] & $F0) {
			lsr	a
			lsr	a
			lsr	a
			lsr	a
			adc	#3
			sta	dialog_buffer,x
			inx
		: ; }

		iny
		cpy	#4
		bne	:---
	; }
	lda	#$FF
	sta	dialog_buffer,x
	rts

dialog_inv_dragonscale:
	lda	rpg_flags+1
	and	#state_flags::equipped_scale>>8
	bne	:+ ; if (!equipped_scale) {
		lda	rpg_flags+1
		ora	#state_flags::equipped_scale>>8
		sta	rpg_flags+1
		window_low_m	((str_03A_offset-tbl_string_offsets)/WORD_SIZE)
		jmp	stats_load
	: ; } else { 
		window_low_m	((str_03B_offset-tbl_string_offsets)/WORD_SIZE)
		rts
	; }

dialog_inv_ring:
	lda	rpg_flags+1
	and	#state_flags::equipped_ring>>8
	bne	:+ ; if (!equipped_ring) {
		lda	rpg_flags+1
		ora	#state_flags::equipped_ring>>8
		sta	rpg_flags+1
		window_low_m	((str_03E_offset-tbl_string_offsets)/WORD_SIZE)
		rts
	: ; } else {
		window_low_m	((str_03F_offset-tbl_string_offsets)/WORD_SIZE)
		rts
	; }

dialog_inv_cursedbelt:
	lda	#$3A
	jsr	dialog_string_base
	bit	rpg_flags+1
	bvs	dialog_inv_alreadycursed ; if (!equipped_cursed_belt) {
		lda	rpg_flags+1
		ora	#state_flags::equipped_cursed_belt>>8
		sta	rpg_flags+1

	dialog_inv_wearcurse:
		window_low_m	((str_042_offset-tbl_string_offsets)/WORD_SIZE)

	dialog_inv_startcurse:
		apusong_m	apu_song_curse
		jsr	apu_play
		jsr	apu_wait_music
		rts
	; }

dialog_inv_alreadycursed:
	window_low_m	((str_043_offset-tbl_string_offsets)/WORD_SIZE)
	jmp	dialog_inv_startcurse

dialog_inv_cursednecklace:
	lda	#$3C
	jsr	dialog_string_base
	lda	rpg_flags+1
	bmi	dialog_inv_alreadycursed
	lda	rpg_flags+1
	ora	#state_flags::equipped_cursed_necklace>>8
	sta	rpg_flags+1
	bne	dialog_inv_wearcurse

dialog_inv_add:
	sta	general_word_1
	ldx	#0
	: ; for (x = 0; x < 4; x++) {
		lda	rpg_inv,x
		and	#$0F
		bne	:+ ; if (!(rpg_inv[x] & $0F)) {
			lda	rpg_inv,x
			and	#$F0
			ora	general_word_1
			sta	rpg_inv,x
			rts
		: lda	rpg_inv,x
		and	#$F0
		bne	:+ ; } else if (!(rpg_inv[x] & $F0)) {
			asl	general_word_1
			asl	general_word_1
			asl	general_word_1
			asl	general_word_1
			lda	rpg_inv,x
			and	#$0F
			ora	general_word_1
			sta	rpg_inv,x
			rts
		: ; }

		inx
		cpx	#INV_ITEM_MAX
		bne	:---
	; }
	rts

dialog_inv_del:
	jsr	dialog_inv
	and	rpg_inv,y
	sta	rpg_inv,y
	rts

dialog_inv:
	ldy	#0
	sta	general_word_0
	: ; for () {
		lda	rpg_inv,y
		and	#$0F
		cmp	general_word_0
		bne	:+ ; if ((rpg_inv[y] & $0F)) {
			lda	#$F0
			rts
		: lda	rpg_inv,y
		lsr	a
		lsr	a
		lsr	a
		lsr	a
		cmp	general_word_0
		bne	:+ ; } else if ((rpg_inv[y] & $F0)) {
			lda	#$0F
			rts
		: ; }

		iny
		cpy	#INV_ITEM_MAX
		bne	:---
	; }

	lda	#$FF
	rts

dialog_inv_discard:
	sta	dialog_item_idx
	window_low_m	((str_0CC_offset-tbl_string_offsets)/WORD_SIZE)
	window_function_m	((window_decision-tbl_windows)/FUNC_WINDOW_DEF_SIZE)
	beq	:+ ; if (window_decision() == no) {
	dialog_inv_discard_end:
		lda	dialog_item_idx
		clc
		adc	#$31
		jsr	dialog_string_base
		lda	#<((str_0CD_offset-tbl_string_offsets)/WORD_SIZE)
		jmp	dialog_end
	: ; }

	: ; for () {
		window_low_m	((str_0CE_offset-tbl_string_offsets)/WORD_SIZE)
		jsr	dialog_inv_list
		window_function_m	((window_tool-tbl_windows)/FUNC_WINDOW_DEF_SIZE)
		cmp	#$FF
		beq	dialog_inv_discard_end

		tax
		lda	dialog_buffer+1,x
		ldy	#0
		: ; for (item of dialog_inv_tbl) {
			cmp	dialog_inv_tbl,y
			bne	:+ ; if (item == dialog_buffer[x][1]) {
				window_low_m	((str_0D1_offset-tbl_string_offsets)/WORD_SIZE)
				jmp	:--
			: ; }

			iny
			cpy	#dialog_inv_tbl_end-dialog_inv_tbl
			bne	:--
		; }

		cmp	#$c
		bne	:++
		bit	rpg_flags+1
		bvc	:++

	:
		window_low_m	((str_018_offset-tbl_string_offsets)/WORD_SIZE)
		jmp	:----
	: ; }

	lda	dialog_buffer+1,x
	cmp	#$0E
	bne	:+ ; if (dialog_buffer[x+1] == $E) {
		lda	rpg_flags+1
		bmi	:--
	: ; }

	lda	dialog_buffer+1,x
	pha
	clc
	adc	#$2E
	jsr	dialog_string_base
	window_low_m	((str_0CF_offset-tbl_string_offsets)/WORD_SIZE)
	lda	dialog_item_idx
	clc
	adc	#$31
	jsr	dialog_string_base
	window_low_m	((str_0D0_offset-tbl_string_offsets)/WORD_SIZE)
	pla
	sec
	sbc	#$3
	jsr	dialog_inv_del
	lda	dialog_item_idx
	jsr	dialog_inv_add
	jmp	dialog_resume_game

dialog_inv_tbl:
	.byte	$02
	.byte	$03
	.byte	$08
	.byte	$0A
	.byte	$0B
	.byte	$0D
	.byte	$0F
	.byte	$10
	.byte	$11
dialog_inv_tbl_end:
	.end
