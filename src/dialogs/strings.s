.include	"general_bss.i"
.include	"tunables.i"
.include	"battle.i"
.include	"dialogs.i"
.include	"charmap.i"
.include	"macros.i"

	.export dialog_string_enemy
dialog_string_enemy:
	movw_m	tbl_enemy_props+ENEMY_PTR_DESC, dialog_string_ptr
	ldy	#0
	beq	:+++

	.export dialog_string_base
dialog_string_base:
	clc
	adc	#3
	tax
	inx
	movw_m	tbl_str_common, dialog_string_ptr
	ldy	#0

	: lda	(dialog_string_ptr),y
		inc	dialog_string_ptr
		bne	:+
			inc	dialog_string_ptr+1
		:  cmp	#CHAR_CTRL_STREND
		bne	:--
		dex
		bne	:--

	:
		lda	(dialog_string_ptr),y
		sta	dialog_buffer,y
		iny
		cmp	#CHAR_CTRL_STREND
		bne	:-

	rts
	.end
