.include	"system/cpu.i"

.include	"ppu_pal.i"
.include	"general_bss.i"
.include	"game_bss.i"
.include	"ppu_bss.i"
.include	"apu_commands.i"
.include	"map.i"
.include	"map_calc.i"
.include	"window.i"
.include	"sprite.i"
.include	"math.i"
.include	"npc.i"
.include	"rpg.i"
.include	"flags.i"
.include	"enemies.i"
.include	"tunables.i"
.include	"macros.i"

	.export dialog_talk_check, dialog_end
dialog_talk_check:
	window_function_m	((window_talk_dir-tbl_windows)/FUNC_WINDOW_DEF_SIZE)
	cmp	#$FF
	bne	:+ ; if (talk_dir == cancel) {
		jmp	dialog_command_clear
	: ; }

	pha
	window_function_m	((window_dialog-tbl_windows)/FUNC_WINDOW_DEF_SIZE)
	lda	map_player_x
	sta	map_blk_col
	lda	map_player_y
	sta	map_blk_row
	pla	; switch (talk_dir) {
	bne	:+ ; case 0:
		dec	map_blk_row
		jsr	map_get_blkid
		lda	map_blkid
		cmp	#$0E
		bne	:++++ ; if (blkid == $E) {
			dec	map_npc_ypos
			jmp	:++++
		; }
	: cmp	#1
	bne	:+ ; case 1:
		inc	map_blk_col
		jsr	map_get_blkid
		lda	map_blkid
		cmp	#$0E
		bne	:+++ ; if (blkid == $E) {
			inc	map_npc_xpos
			jmp	:+++
		; }
	: cmp	#2
	bne	:+ ; case 2:
		inc	map_blk_row
		jsr	map_get_blkid
		lda	map_blkid
		cmp	#$0E
		bne	:++ ; if (blkid == $E) {
			inc	map_npc_ypos
			jmp	:++
		; }
	: ; default:
		dec	map_blk_col
		jsr	map_get_blkid
		lda	map_blkid
		cmp	#$0E
		bne	:+ ; if (blkid == $E) {
			dec	map_npc_xpos
		; }
	: ; }

	lda	general_word_0
	cmp	#$17
	bne	:+++ ; if (general_word_0 == $17) {
		lda	map_npc_xpos
		pha
		lda	map_npc_ypos
		pha
		window_low_m	((str_0B5_offset-tbl_string_offsets)/WORD_SIZE)
		: ; while (window_decision() == no) {
			window_low_m	((str_0C5_offset-tbl_string_offsets)/WORD_SIZE)
			window_function_m	((window_decision-tbl_windows)/FUNC_WINDOW_DEF_SIZE)
			beq	:+

			window_low_m	((str_0B6_offset-tbl_string_offsets)/WORD_SIZE)
			jmp	:-
		: ; }

		lda	game_player_flags
		ora	#player_flags::lora_carry
		sta	game_player_flags
		jsr	ppu_nmi_wait
		jsr	sprites_draw
		pla
		sec
		sbc	map_player_y
		asl	a
		sta	map_tile_ypos
		pla
		sec
		sbc	map_player_x
		asl	a
		sta	map_tile_xpos
		lda	#0
		sta	ppu_blkdel_flags
		jsr	map_block_mod
		window_low_m	((str_0B7_offset-tbl_string_offsets)/WORD_SIZE)
		apusong_m	apu_song_lora
		jsr	apu_play
		jsr	apu_wait_music
		apusong_m	apu_song_dungeon0
		jsr	apu_play
		lda	#<((str_0B8_offset-tbl_string_offsets)/WORD_SIZE)
		jmp	dialog_end
	: ; }

	ldy	#0
	: ; for (y = 0; y < $3C; y += 3) {
		lda	sprite_npc_x,y
		and	#$1F
		cmp	map_npc_xpos
		bne	:++
		lda	sprite_npc_y,y
		and	#$1F
		cmp	map_npc_ypos
		bne	:++
		lda	sprite_npc_x,y
		bne	:+
		lda	sprite_npc_y,y
		bne	:+
		lda	sprite_npc_z,y
		bne	:+ ; if () {
			jmp	dialog_notalk
		: ; } else if () {
			jmp	dialog_npctalk
		: ; }

		iny
		iny
		iny
		cpy	#$3C
		bne	:---
	; }

	jmp	dialog_notalk

dialog_npctalk:
	cpy	#$1E
	bcc	:++ ; if (y > $1E) {
		tya
		sec
		sbc	#$1C
		tay
		lda	map_eng_number
		sec
		sbc	#maps::castle_courtyard
		cmp	#$0B
		bcc	:+ ; if (map > castle_courtyard) {
			jmp	dialog_notalk
		: ; }

		asl	a
		tax
		lda	tbl_npcs_static,x
		sta	general_word_0
		lda	tbl_npcs_static+1,x
		sta	general_word_0+1
		jmp	:+++
	: ; } else {
		iny
		iny
		lda	map_eng_number
		sec
		sbc	#maps::castle_courtyard
		cmp	#$0B
		bcc	:+ ; if (map > castle_courtyard) {
			jmp	dialog_notalk
		: ; }

		asl	a
		tax
		lda	tbl_npcs_mobile,x
		sta	general_word_0
		lda	tbl_npcs_mobile+1,x
		sta	general_word_0+1
	: ; }

	lda	game_story_flags
	and	#story_flags::defeated_dragonlord
	beq	:++++ ; if (defeated_dragonlord) {
		lda	map_eng_number
		cmp	#maps::castle_courtyard
		bne	:+ ; if (map == castle_courtyard) {
			window_high_m	((str_121_offset-tbl_string_offsets)/WORD_SIZE)
			jmp	dialog_resume_game
		: lda	(general_word_0),y
		cmp	#$64
		bne	:+ ; } else if (general_word_0[y] == $64) {
			window_high_m	((str_115_offset-tbl_string_offsets)/WORD_SIZE)
			jmp	dialog_resume_game
		: jsr	math_rand
		lda	math_rand_word+1
		lsr	a
		bcc	:+ ; } else if (math_rand() % 1) {
			window_high_m	((str_11F_offset-tbl_string_offsets)/WORD_SIZE)
			jmp	dialog_resume_game
		: ; } else {
			window_high_m	((str_120_offset-tbl_string_offsets)/WORD_SIZE)
			jmp	dialog_resume_game
		; }
	: ; }

	lda	(general_word_0),y
	cmp	#7
	bcs	:+ ; if (general_word_0[y] <= 7) {
		jmp	dialog_weapons

	: cmp	#$0C
	bcs	:+ ; } else if (general_word_0[y] <= $C) {
		jmp	dialog_tools

	: cmp	#$0F
	bcs	:+ ; } else if (general_word_0[y] <= $F) {
		jmp	dialog_keys

	: cmp	#NPC_INKEEP_START
	bcs	:+ ; } else if (general_word_0[y] <= NPC_INKEEP_START) {
		jmp	dialog_fairy

	: cmp	#$16
	bcs	:+ ; } else if (general_word_0[y] <= $16) {
		jmp	dialog_inn

	: cmp	#$5E
	bcs	:+++++ ; } else if (general_word_0[y] <= $5E) {
		pha
		lda	game_player_flags
		and	#player_flags::game_started
		bne	:++ ; if (!game_started) {
			pla
			cmp	#$23
			bne	:+ ; if (general_word_0[y] == $23) {
				window_high_m	((str_101_offset-tbl_string_offsets)/WORD_SIZE)
				jmp	dialog_resume_game
			: cmp	#$24
			bne	:++ ; } else if (general_word_0[y] == $24) {
				window_high_m	((str_100_offset-tbl_string_offsets)/WORD_SIZE)
				jmp	dialog_resume_game
			; }
		: ; } else {
			pla
		: ; }

		pha
		clc
		adc	#<((str_02F_offset-tbl_string_offsets)/WORD_SIZE)
		jsr	window_string_low_a
		pla
		cmp	#$1A
		bne	:+ ; if () {
			window_low_m	((str_0B0_offset-tbl_string_offsets)/WORD_SIZE)
		: ; }

		jmp	dialog_resume_game
	:
	cmp	#$62
	bcs	:++ ; } else if (general_word_0[y] <= $62) {
		clc
		adc	#<((str_02F_offset-tbl_string_offsets)/WORD_SIZE)
		sta	dialog_item_idx
		jsr	window_string_low_a
		window_function_m	((window_decision-tbl_windows)/FUNC_WINDOW_DEF_SIZE)
		bne	:+ ; if (window_decision() == yes) {
			lda	dialog_item_idx
			clc
			adc	#<((str_005_offset-tbl_string_offsets)/WORD_SIZE)
			jsr	window_string_low_a
			jmp	dialog_resume_game
		: ; } else { 
			lda	dialog_item_idx
			clc
			adc	#<((str_00A_offset-tbl_string_offsets)/WORD_SIZE)
			jsr	window_string_low_a
			jmp	dialog_resume_game
		; }

	.export dialog_notalk
dialog_notalk:
	window_low_m	((str_00F_offset-tbl_string_offsets)/WORD_SIZE)
	jmp	dialog_resume_game

	: bne	:++ ; } else if (general_word_0[y] == $62) {
		lda	game_player_flags
		and	#(player_flags::lora_saved|player_flags::lora_carry)
		bne	:+ ; if (!lora_saved && !lora_carry) {
		LD007:
			lda	#<((str_09B_offset-tbl_string_offsets)/WORD_SIZE)
			bne	dialog_end
		: ; }
		lda	#<((str_09C_offset-tbl_string_offsets)/WORD_SIZE)
		bne	dialog_end

	: cmp	#$63
	bne	:+ ; } else if (general_word_0[y] == $63) {
		lda	game_player_flags
		and	#(player_flags::lora_saved|player_flags::lora_carry)
		beq	LD007

		lda	#<((str_09D_offset-tbl_string_offsets)/WORD_SIZE)
		bne	dialog_end

	: cmp	#$64
	bne	:++ ; } else if (general_word_0[y] == $64) {
		lda	game_player_flags
		and	#(player_flags::lora_saved|player_flags::lora_carry)
		bne	:+ ; if (!lora_saved && !lora_carry) {
			lda	#<((str_09E_offset-tbl_string_offsets)/WORD_SIZE)
			bne	dialog_end
		: ; } else {
			lda	#<((str_09F_offset-tbl_string_offsets)/WORD_SIZE)
			bne	dialog_end
		; }

	: cmp	#$65
	bne	:+++ ; } else if (general_word_0[y] == $65) {
		lda	game_player_flags
		and	#(player_flags::lora_saved|player_flags::lora_carry)
		bne	:++ ; if (!lora_saved && !lora_carry) {
			window_low_m	((str_0A0_offset-tbl_string_offsets)/WORD_SIZE)
			window_function_m	((window_decision-tbl_windows)/FUNC_WINDOW_DEF_SIZE)
			beq	:+ ; if (window_decision() == no) {
				window_low_m	((str_0A1_offset-tbl_string_offsets)/WORD_SIZE)
			: ; }

			lda	#<((str_0A2_offset-tbl_string_offsets)/WORD_SIZE)
			bne	dialog_end
		: ; } else {
			lda	#<((str_0A3_offset-tbl_string_offsets)/WORD_SIZE)
		dialog_end:
			jsr	window_string_low_a
			jmp	dialog_resume_game
		; }

	: cmp	#$66
	bne	:++ ; } else if (general_word_0[y] == $66) {
		lda	#item::stones
		jsr	dialog_inv
		cmp	#$FF
		bne	:+
		lda	#item::drop
		jsr	dialog_inv
		cmp	#$FF
		bne	:+ ; if () {
			window_low_m	((str_0A4_offset-tbl_string_offsets)/WORD_SIZE)
			lda	#<((str_0C6_offset-tbl_string_offsets)/WORD_SIZE)
			bne	dialog_end
		: ; } else {
			lda	#<((str_0A5_offset-tbl_string_offsets)/WORD_SIZE)
			bne	dialog_end
		; }

	: cmp	#$67
	bne	:++++ ; } else if (general_word_0[y] == $67) {
		lda	rpg_flags+1
		and	#>(state_flags::equipped_cursed_necklace|state_flags::equipped_cursed_belt)
		bne	:+ ; if (!cursed) {
			lda	#<((str_0A6_offset-tbl_string_offsets)/WORD_SIZE)
			bne	dialog_end
		: ; } else {
			window_low_m	((str_0A7_offset-tbl_string_offsets)/WORD_SIZE)
			lda	rpg_flags+1
			bpl	:+ ; if (cursed_necklace) {
				lda	#item::curse_necklace
				jsr	dialog_inv_del
			: ; }

			bit	rpg_flags+1
			bvc	:+ ; if (cursed_belt) {
				lda	#item::curse_belt
				jsr	dialog_inv_del
			: ; }

			lda	rpg_flags+1
			and	#>~(state_flags::equipped_cursed_necklace|state_flags::equipped_cursed_belt)
			sta	rpg_flags+1
			lda	#<((str_0A8_offset-tbl_string_offsets)/WORD_SIZE)
			bne	dialog_end
		; }

	: cmp	#$68
	bne	:++ ; } else if (general_word_0[y] == $68) {
		lda	rpg_equip
		and	#$E0
		cmp	#$E0
		beq	:+ ; if ((rpg_equip & $E0) != $E0) {
			lda	#<((str_0A9_offset-tbl_string_offsets)/WORD_SIZE)
			bne	dialog_end
		: ; } else { 
			lda	#<((str_0AA_offset-tbl_string_offsets)/WORD_SIZE)
			bne	dialog_end
		; }

	: cmp	#$69
	bne	:+++ ; } else if (general_word_0[y] == $69) {
		lda	#item::ring_fighter
		jsr	dialog_inv
		cmp	#$FF
		bne	:+ ; if (dialog_inv(ring_fighter) == $FF) {
			lda	rpg_flags+1
			and	#<~(state_flags::equipped_ring>>8)
			sta	rpg_flags+1
		: ; }

		lda	rpg_flags+1
		and	#state_flags::equipped_ring>>8
		bne	:+ ; if (equipped_ring) {
			lda	#<((str_0AC_offset-tbl_string_offsets)/WORD_SIZE)
			jmp	dialog_end
		: ; } else {
			lda	#<((str_0AB_offset-tbl_string_offsets)/WORD_SIZE)
			jmp	dialog_end
		; }

	: cmp	#$6A
	bne	:+ ; } else if (general_word_0[y] == $6A) {
		window_low_m	((str_0AD_offset-tbl_string_offsets)/WORD_SIZE)
		jsr	dialog_spell_flash
		lda	rpg_maxmp
		sta	rpg_mp
		window_function_m	((window_main_field-tbl_windows)/FUNC_WINDOW_DEF_SIZE)
		jmp	dialog_resume_game

	: cmp	#$6B
	bne	:+ ; } else if (general_word_0[y] == $6B) {
		window_low_m	((str_04C_offset-tbl_string_offsets)/WORD_SIZE)
		window_low_m	((str_0AE_offset-tbl_string_offsets)/WORD_SIZE)
		window_low_m	((str_0AF_offset-tbl_string_offsets)/WORD_SIZE)
		jmp	dialog_resume_game

	: cmp	#$6C
	bne	:+++ ; } else if (general_word_0[y] == $6C) {
		lda	#item::drop
		jsr	dialog_inv
		cmp	#$FF
		bne	LD109
		lda	#item::staff
		jsr	dialog_inv
		cmp	#$FF
		beq	:+
		LD109: ; if () {
			lda	#<((str_0A5_offset-tbl_string_offsets)/WORD_SIZE)
		LD11B:
			jsr	window_string_low_a
			jmp	dialog_resume_game
		: ; } else {
			lda	#item::harp
			jsr	dialog_inv
			cmp	#$FF
			beq	:+ ; if () {
				window_low_m	((str_0B2_offset-tbl_string_offsets)/WORD_SIZE)
				window_low_m	((str_0A4_offset-tbl_string_offsets)/WORD_SIZE)
				window_low_m	((str_0C6_offset-tbl_string_offsets)/WORD_SIZE)
				lda	#item::harp
				jsr	dialog_inv_del
				lda	#0
				sta	sprite_npc_x+(NPC_ENTRY_SIZE*10)
				sta	sprite_npc_y+(NPC_ENTRY_SIZE*10)
				sta	sprite_npc_z+(NPC_ENTRY_SIZE*10)
				jsr	ppu_nmi_wait
				jsr	sprites_draw
				jmp	dialog_resume_game
			: ; }

			lda	#$B1
			bne	LD11B
		; }

	: cmp	#$6D
	bne	:++++ ; } else if (general_word_0[y] == $6D) {
		lda	#item::drop
		jsr	dialog_inv
		cmp	#$FF
		bne	LD109
		lda	#item::token_loto
		jsr	dialog_inv
		cmp	#$FF
		bne	:+ ; if () {
			window_low_m	((str_0B3_offset-tbl_string_offsets)/WORD_SIZE)
			jsr	dialog_spell_flash
			jmp	map_dodoor
		: ; }

		lda	#item::stones
		jsr	dialog_inv
		cmp	#$FF
		beq	:++
		lda	#item::staff
		jsr	dialog_inv
		cmp	#$FF
		beq	:++ ; if () {
			window_low_m	((str_0B4_offset-tbl_string_offsets)/WORD_SIZE)
			lda	#item::stones
			jsr	dialog_inv_del
			lda	#item::staff
			jsr	dialog_inv_del
			lda	#item::drop
			jsr	dialog_inv_add
			jsr	ppu_nmi_wait
			lda	#ppu_ctlr1::objblk_off|ppu_ctlr1::bgblk_off|ppu_ctlr1::monotone
			sta	PPU_CTLR1
			ldx	#$1E
			: ; for (x = $1E; x > 0; x--) {
				jsr	ppu_nmi_wait
				dex
				bne	:-
			; }

			lda	#ppu_ctlr1::objblk_off|ppu_ctlr1::bgblk_off|ppu_ctlr1::color
			sta	PPU_CTLR1
			jmp	dialog_resume_game
		: ; }

		window_low_m	((str_049_offset-tbl_string_offsets)/WORD_SIZE)
		lda	#$AE
		jmp	LD11B

	: cmp	#$6E
	beq	:+
	jmp	dialog_princess ; } else if (general_word_0[y] == $6E) {
	:
		lda	game_player_flags
		and	#player_flags::lora_carry
		beq	:++++ ; if (lora_carry) {
			window_low_m	((str_0B9_offset-tbl_string_offsets)/WORD_SIZE)
			lda	#item::token_lora
			jsr	dialog_inv_add
			cpx	#4
			bne	:+++ ; if (x == 4) {
				ldx	#0
				: ; for (x = 0; x < 7; x++) {
					lda	dialog_talk_invlist,x
					jsr	dialog_inv
					cmp	#$FF
					bne	:+
					inx
					cpx	#7
					bne	:-
					beq	:++
				: ; }

				; if (x == 7) {
					lda	dialog_talk_invlist,x
					pha
					jsr	dialog_inv_del
					pla
					clc
					adc	#$31
					jsr	dialog_string_base
					lda	#item::token_lora
					jsr	dialog_inv_add
					window_low_m	((str_0BA_offset-tbl_string_offsets)/WORD_SIZE)
				; }
			: ; }

			window_low_m	((str_0BB_offset-tbl_string_offsets)/WORD_SIZE)
			window_low_m	((str_0BC_offset-tbl_string_offsets)/WORD_SIZE)
			lda	game_player_flags
			and	#<~(player_flags::lora_saved|player_flags::lora_carry)
			ora	#player_flags::lora_saved
			sta	game_player_flags
			lda	#$C6
			sta	sprite_npc_x+(NPC_ENTRY_SIZE*13)
			lda	#3
			sta	sprite_npc_y+(NPC_ENTRY_SIZE*13)
			jsr	ppu_nmi_wait
			jsr	sprites_draw
			jmp	:++++
		: ; } else {
			lda	game_player_flags
			and	#player_flags::game_started
			bne	:+ ; if (!game_started) {
				lda	#<((str_0BF_offset-tbl_string_offsets)/WORD_SIZE)
				jmp	dialog_end
			: ; }

			window_low_m	((str_0C0_offset-tbl_string_offsets)/WORD_SIZE)
			lda	rpg_level
			cmp	#<(LEVEL_MAX)
			bne	:+ ; if (rpg_level == LEVEL_MAX) {
				window_low_m	((str_002_offset-tbl_string_offsets)/WORD_SIZE)
				jmp	:++
			: ; } else { 
				jsr	stats_calc_exp
				window_low_m	((str_0C1_offset-tbl_string_offsets)/WORD_SIZE)
			; }
		: ; }

		window_low_m	((str_0C2_offset-tbl_string_offsets)/WORD_SIZE)
		jsr	password_calc
		window_low_m	((str_0C3_offset-tbl_string_offsets)/WORD_SIZE)
		lda	#<((str_0C4_offset-tbl_string_offsets)/WORD_SIZE)
		jmp	dialog_end

dialog_princess:
	cmp	#$6F
	bne	:+++++ ; } else if (general_word_0[y] == $6F) {
		jsr	math_rand
		lda	math_rand_word+1
		and	#$60
		bne	:+ ; if (!(math_rand() & $60)) {
			lda	#<((str_0BB_offset-tbl_string_offsets)/WORD_SIZE)
			jmp	dialog_end
		: ; }

		cmp	#$60
		bne	:+ ; if () {
			lda	#<((str_0BD_offset-tbl_string_offsets)/WORD_SIZE)
			jmp	dialog_end
		: ; }

		: ; while (window_decision() == no) {
			window_low_m	((str_0BE_offset-tbl_string_offsets)/WORD_SIZE)
			window_function_m	((window_decision-tbl_windows)/FUNC_WINDOW_DEF_SIZE)
			beq	:+

			window_low_m	((str_0B6_offset-tbl_string_offsets)/WORD_SIZE)
			jmp	:-
		: ; }

		window_low_m	((str_0B8_offset-tbl_string_offsets)/WORD_SIZE)
		apusong_m	apu_song_lora
		jsr	apu_play
		jsr	apu_wait_music
		apusong_m	apu_song_caste_throneroom
		jsr	apu_play
		jmp	dialog_resume_game

	: cmp	#$70
	bne	:+++++ ; } else if (general_word_0[y] == $70) {
		window_low_m	((str_0C7_offset-tbl_string_offsets)/WORD_SIZE)
		window_low_m	((str_0A4_offset-tbl_string_offsets)/WORD_SIZE)
		window_low_m	((str_0C8_offset-tbl_string_offsets)/WORD_SIZE)
		window_function_m	((window_decision-tbl_windows)/FUNC_WINDOW_DEF_SIZE)
		bne	:+
		window_high_m	((str_116_offset-tbl_string_offsets)/WORD_SIZE)
		window_function_m	((window_decision-tbl_windows)/FUNC_WINDOW_DEF_SIZE)
		beq	:+++
		: ; if (decision_a == yes || decision_b == yes) {
			window_low_m	((str_0C9_offset-tbl_string_offsets)/WORD_SIZE)
			ldx	#$28
			: ; for (x = $28; x > 0; x--) {
				jsr	ppu_nmi_wait
				dex
				bne	:-
			; }

			window_function_clear_m	((window_dialog-tbl_windows)/FUNC_WINDOW_DEF_SIZE)
			window_function_clear_m	((window_cmd_field-tbl_windows)/FUNC_WINDOW_DEF_SIZE)
			window_function_clear_m	((window_main_field-tbl_windows)/FUNC_WINDOW_DEF_SIZE)
			lda	#enemy::dragonlord_weak
			jmp	battle_init
		: ; }

		window_low_m	((str_0CA_offset-tbl_string_offsets)/WORD_SIZE)
		window_low_m	((str_0C2_offset-tbl_string_offsets)/WORD_SIZE)
		lda	#0
		sta	rpg_exp
		sta	rpg_exp+1
		sta	rpg_gold
		sta	rpg_gold+1
		sta	rpg_inv
		sta	rpg_inv+1
		sta	rpg_inv+2
		sta	rpg_inv+3
		sta	rpg_keys
		sta	rpg_herbs
		sta	rpg_equip
		sta	rpg_flags+1
		sta	game_player_flags
		sta	game_story_flags
		jsr	password_calc
		window_low_m	((str_0C3_offset-tbl_string_offsets)/WORD_SIZE)
		window_low_m	((str_0CB_offset-tbl_string_offsets)/WORD_SIZE)
		movw_m	ppu_pal_bad_ending, ppu_pal_load_src
		lda	#PPU_COLOR_LEV0
		sta	ppu_pal_load_cutoff
		jsr	ppu_pal_load_bg
		: jmp	:-

	: cmp	#$71
	beq	:+
	cmp	#$72
	beq	:++ ; } else if (general_word_0[y] >= $73) {
		jmp	dialog_notalk
	: ; } else if (general_word_0[y] == $71) {
		window_low_m	((str_003_offset-tbl_string_offsets)/WORD_SIZE)
		jmp	dialog_resume_game
	: ; } else if (general_word_0[y] == $72) {
		window_low_m	((str_091_offset-tbl_string_offsets)/WORD_SIZE)
		jmp	dialog_resume_game
	; }

dialog_talk_invlist:
	.byte	item::torch
	.byte	item::dragon_scale
	.byte	item::ring_fighter
	.byte	item::fairy_water
	.byte	item::chimera_wing
	.byte	item::curse_belt
	.byte	item::staff

	.end
