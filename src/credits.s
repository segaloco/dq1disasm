.include	"system/ppu.i"
.include	"system/cnrom.i"

.include	"ppu_nmi.i"
.include	"ppu_nmi_write.i"
.include	"ppu_pal.i"
.include	"apu_commands.i"
.include	"general_bss.i"
.include	"game_bss.i"
.include	"window.i"
.include	"enemies.i"
.include	"rpg.i"
.include	"tunables.i"
.include	"credits.i"
.include	"macros.i"

PAGE_SIZE	= $30

	.export	game_credits_load
game_credits_load:
	movw_m	CNROM_BANK_INDEX_2_H|song_credits_addr, cnrom_load_src
	jsr	game_set_subbank
	movw_m	(song_credits_end-song_credits_start), cnrom_load_count
	jsr	cnrom_load
	apusong_m	apu_song_credits
	jsr	apu_play
	jsr	apu_wait_music
	lda	#PPU_PAL_BG_TRUE
	sta	ppu_pal_fade_has_bg
	movw_m	ppu_pal_obj_fade, ppu_pal_fade_obj
	movw_m	ppu_pal_bg_fade, ppu_pal_fade_bg
	jsr	ppu_pal_fadeout
	jsr	oam_init
	lda	#0
	sta	rpg_exp
	sta	ppu_nmi_scc_h
	sta	ppu_nmi_scc_v
	sta	ppu_nmi_bg_high
	lda	#enemy::dragonlord
	sta	battle_enemy
	movw_m	CNROM_BANK_INDEX_2_H|str_credits_addr, window_block_addr

	credits_loop: ; for (;;) {
		movw_m	PPU_VRAM_BG1, ppu_nmi_write_dest
		lda	#<((tile_blank_chr00-chr00_base)/PPU_TILE_SIZE)
		sta	ppu_nmi_write_data
		: ; for (ppu_nmi_write_dest = PPU_VRAM_BG1; ppu_nmi_write_dest < PPU_VRAM_BG2; ppu_nmi_write_dest += PPU_TILE_PER_LINE) {
			jsr	ppu_nmi_wait
			ldy	#PPU_TILE_PER_LINE
			: ; for (y = PPU_TILE_PER_LINE; y > 0; y--) {
				jsr	ppu_nmi_write
				dey
				bne	:-
			; }

			lda	ppu_nmi_write_dest+1
			cmp	#>PPU_VRAM_BG2
			bne	:--
		; }

		: ; if () {
			lda	window_block_addr
			sta	cnrom_load_src
			lda	window_block_addr+1
			sta	cnrom_load_src+1
			movw_m	cnrom_buffer3, cnrom_load_dest
			movw_m	PAGE_SIZE, cnrom_load_count
			jsr	cnrom_load
			lda	str_credits
			sta	ppu_nmi_write_dest
			lda	str_credits+1
			sta	ppu_nmi_write_dest+1
			ldy	#2
			: ; for (y = 2; y < $FF; y += ?) {
				lda	str_credits,y
				sta	ppu_nmi_write_data
				cmp	#CREDITS_RLE
				bne	:++ ; if () {
					iny
					lda	str_credits,y
					sta	general_word_0
					iny
					lda	str_credits,y
					sta	ppu_nmi_write_data
					: ; for (general_word_0 = str_credits[y].count; general_word_0 > 0; general_word_0--) {
						jsr	ppu_nmi_write
						dec	general_word_0
						bne	:-
					; }

					iny
					bne	:--
				: ; }

				cmp	#CREDITS_ENDLINE
				beq	:+
				cmp	#CREDITS_ENDPAGE
				beq	:+ ; if () {
					jsr	ppu_nmi_write
					iny
					bne	:---
				; }
			: ; }

			iny
			tya
			clc
			adc	window_block_addr
			sta	window_block_addr
			bcc	:+
				inc	window_block_addr+1
			:

			lda	ppu_nmi_write_data
			cmp	#CREDITS_ENDLINE
			beq	:------
		; }

		jsr	credits_load_pal
		jsr	ppu_pal_fadein
		lda	rpg_exp
		bne	:+ ; if (!rpg_exp) {
			ldy	#8
			bne	:+++
		: cmp	#1
		bne	:+ ; } else if (rpg_exp == 1) {
			ldy	#2
			bne	:++
		: ; } else {
			ldy	#3
		: ; }
		: ; for (y = ?; y > 0; y--) {
			jsr	apu_wait_music
			dey
			bne	:-
		; }

		inc	rpg_exp
		lda	window_block_addr
		cmp	#<(CNROM_BANK_INDEX_2_H|(str_credits_addr+(str_credits_end-str_credits)))
		bne	:++
		lda	window_block_addr+1
		cmp	#>(CNROM_BANK_INDEX_2_H|(str_credits_addr+(str_credits_end-str_credits)))
		bne	:++ ; if (window_block_addr == str_credits_end) {
			: ; for (;;) {
				jmp	:-
			; }
		: ; }

		jsr	credits_load_pal
		jsr	ppu_pal_fadeout
		jmp	credits_loop
	; }

credits_load_pal:
	movw_m	ppu_pal_null, ppu_pal_fade_obj
	movw_m	credits_pal, ppu_pal_fade_bg
	lda	#PPU_PAL_BG_TRUE
	sta	ppu_pal_fade_has_bg
	rts

credits_pal:
	.byte	PPU_COLOR_NULL, PPU_COLOR_LEV3|PPU_COLOR_GREY,	  PPU_COLOR_LEV3|PPU_COLOR_GREY
	.byte	PPU_COLOR_NULL, PPU_COLOR_LEV2|PPU_COLOR_MAGENTA, PPU_COLOR_LEV2|PPU_COLOR_MAGENTA
	.byte	PPU_COLOR_NULL, PPU_COLOR_LEV2|PPU_COLOR_ORANGE,  PPU_COLOR_LEV2|PPU_COLOR_ORANGE
	.byte	PPU_COLOR_NULL, PPU_COLOR_LEV2|PPU_COLOR_GREEN,   PPU_COLOR_LEV2|PPU_COLOR_GREEN
