.include	"system/cpu.i"

.include	"ppu_nmi.i"
.include	"ppu_nmi_write.i"
.include	"ppu_pal.i"
.include	"general_bss.i"
.include	"game_bss.i"
.include	"joypad.i"
.include	"math.i"
.include	"map.i"
.include	"rpg.i"
.include	"flags.i"
.include	"window.i"
.include	"dialogs.i"
.include	"tunables.i"
.include	"charmap.i"
.include	"password.i"
.include	"macros.i"

PASSWORD_CHAR_BUF_SIZE	= 28

PASSWORD_GROUP_END_1	= 5
PASSWORD_GROUP_END_2	= 13
PASSWORD_GROUP_END_3	= 21
PASSWORD_TEXT_END	= 25

password_crc	= general_word_0
password_crc_data = general_word_1
password_tile_idx	= general_word_0
password_bit_entropy	= general_word_1

password_crc_ptr	= ppu_nmi_buffer
password_data		= password_crc_ptr+1
password_exp_0		= password_data+pw_byte::exp_0
password_name_2		= password_data+pw_byte::name_2
password_inv_1		= password_data+pw_byte::inv_1
password_gold_0		= password_data+pw_byte::gold_0
password_name_0		= password_data+pw_byte::name_0
password_inv_3		= password_data+pw_byte::inv_3
password_name_3		= password_data+pw_byte::name_3
password_equip		= password_data+pw_byte::equip
password_gold_1		= password_data+pw_byte::gold_1
password_herbs		= password_data+pw_byte::herbs
password_inv_2		= password_data+pw_byte::inv_2
password_exp_1		= password_data+pw_byte::exp_1
password_name_1		= password_data+pw_byte::name_1
password_inv_0		= password_data+pw_byte::inv_0

	.export password_calc
password_calc:
	: ; while (ppu_nmi_entry_index) {
		lda     ppu_nmi_entry_index
		bne     :-
	; }

        lda     rpg_name+2
        jsr     password_name_encode
        sta     password_name_2
        lda     rpg_name
        jsr     password_name_encode
        asl     a
        asl     a
        sta     password_name_0
        lda     rpg_name+3
        jsr     password_name_encode
        sta     password_name_3
        lda     rpg_name+1
        jsr     password_name_encode
        asl     a
        sta     password_name_1

        lda     rpg_exp+1
        sta     password_exp_1
        lda     rpg_exp
        sta     password_exp_0
        lda     rpg_gold
        sta     password_gold_0
        lda     rpg_gold+1
        sta     password_gold_1
        lda     rpg_inv+1
        sta     password_inv_1
        lda     rpg_inv+3
        sta     password_inv_3
        lda     rpg_inv+2
        sta     password_inv_2
        lda     rpg_inv
        sta     password_inv_0
        lda     rpg_equip
        sta     password_equip
        lda     rpg_keys
        asl     a
        asl     a
        asl     a
        asl     a
        ora     rpg_herbs
        sta     password_herbs

        lda     game_player_flags
        and     #player_flags::own_cursed_necklace
        beq     :+ ; if (game_player_flags & own_cursed_necklace) {
		lda     #PASSWORD_CURSED_NECKLACE_BIT
		ora     password_name_2
		sta     password_name_2
	: ; }

	lda     rpg_flags+1
        and     #(state_flags::equipped_scale)>>8
        beq     :+ ; if (equipped_scale) {
		lda     #PASSWORD_SCALE_BIT
		ora     password_name_1
		sta     password_name_1
	: ; }

	lda     rpg_flags+1
        and     #(state_flags::equipped_ring)>>8
        beq     :+ ; if (equipped_ring) {
		lda     #PASSWORD_RING_BIT
		ora     password_name_1
		sta     password_name_1
	: ; }

	lda     game_story_flags
        and     #story_flags::defeated_golem
        ora     password_name_0
        sta     password_name_0
        lda     game_story_flags
        and     #story_flags::defeated_greendragon
        ora     password_name_3
        sta     password_name_3

        jsr     math_rand
        lda     math_rand_word+1
        and     #PASSWORD_NAME_2_RAND_BIT
        ora     password_name_2
        sta     password_name_2
        lda     math_rand_word+1
        and     #PASSWORD_NAME_0_RAND_BIT
        ora     password_name_0
        sta     password_name_0
        jsr     math_rand
        lda     math_rand_word+1
        and     #PASSWORD_NAME_3_RAND_BIT
        ora     password_name_3
        sta     password_name_3

        lda     #0
        sta     password_crc
        sta     password_crc+1
        ldx     #1
	: ; for (byte of buffer) {
		ldy     #BITS_IN_BYTE
		lda     password_data-1, x
		sta     password_crc_data
		: ; for (bit of byte) {
			lda     password_crc+1
			eor     password_crc_data
			asl     password_crc
			rol     password_crc+1
			asl     password_crc_data
			asl     a
			bcc     :+ ; if (one_shifted) {
				lda     password_crc
				eor     #%00100001
				sta     password_crc
				lda     password_crc+1
				eor     #%00010000
				sta     password_crc+1
			: ; }

			dey
			bne     :--
		; }

		inx
		cpx     #PASSWORD_BUF_SIZE
		bne     :---
	; }
        lda     password_crc
        sta     password_crc_ptr

        lda     #<((tile_blank_chr00-chr00_base)/PPU_TILE_SIZE)
        ldx     #0
	: ; for (character of buffer) {
		sta     game_buffer, x
		inx
		cpx     #PASSWORD_CHAR_BUF_SIZE
		bne     :-
	; }

        lda     #CHAR_CTRL_CONTENT_NEWLINE
        sta     game_buffer+$F
        lda     #CHAR_CTRL_WINDOW_NEWLINE
        sta     game_buffer+$1B
        lda     #0
        sta     password_tile_idx
        sta     password_bit_entropy
	: ; for (tile of password_data) {
		lda     password_crc_ptr
		and     #PASSWORD_CHAR_MASK
		clc
		adc     password_bit_entropy
		clc
		adc     #PASSWORD_CHAR_INCR
		and     #PASSWORD_CHAR_MASK
		sta     password_bit_entropy
		jsr     password_byte_encode
		ldx     password_tile_idx
		sta     game_buffer+2, x

		inc     password_tile_idx
		lda     password_tile_idx
		cmp     #PASSWORD_GROUP_END_1
		beq     :+++
		cmp     #PASSWORD_GROUP_END_3
		beq     :+++
		cmp     #PASSWORD_GROUP_END_2
		beq     :++
		cmp     #PASSWORD_TEXT_END
		bne     :- ; if (tile_idx == PASSWORD_TEXT_END) {
			movw_m	game_buffer, window_block_addr
			jsr     window_show_loaded

			lda     #CHAR_CTRL_END
			ldx     #0
			: ; for (x = 0; x < $20; x++) {
				sta     game_buffer, x
				inx
				cpx     #$20
				bne     :-
			; }

			rts
		: ; } else if (PASSWORD_GROUP_END.includes(tile_idx)) { 
			; if (tile_idx == PASSWORD_GROUP_END_2) {
				inc     password_tile_idx
				inc     password_tile_idx
			: ; }

			inc     password_tile_idx
			bne     :----
		; }
	; }
; ------------------------------------------------------------
password_name_encode:
	cmp     #CHAR_DAKUTEN_HI ; switch (a) {
        bne     :+ ; case CHAR_DAKUTEN_HI:
		lda     #CHAR_W_BORD_W
		rts

	: cmp	#CHAR_HANDAKUTEN_HI
        bne     :+ ; case CHAR_HANDAKUTEN_HI:
		lda     #CHAR_W_BORD_NA
		rts

	: cmp	#CHAR_LONG_VOWEL
        bne     :+ ; case CHAR_LONG_VOWEL:
		lda     #CHAR_W_BORD_NB
		rts

	: cmp	#' '
        bne     :+ ; case ' ':
		lda     #CHAR_KATA_ME

	: ; default:
		rts
	; }
; ------------------------------------------------------------
password_byte_encode:
	ldy     #PASSWORD_ENC_COUNT
	: ; for (y = 6; y > 0; y--) {
		ldx     #PASSWORD_BUF_SIZE
		: ; for (byte of password_data_buffer) {
			ror     password_crc_ptr-1, x

			dex
			bne     :-
		; }
		dey
		bne     :--
	; }

        clc
        adc     #10
        cmp     #54
        bcc     :+ ; if ((a + 10) >= 54) {
		clc
		adc	#<-95
	: ; }

	rts

