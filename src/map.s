.include	"system/ppu.i"

.include	"apu_commands.i"
.include	"ppu_nmi.i"
.include	"ppu_nmi_write.i"
.include	"ppu_pal.i"
.include	"general_bss.i"
.include	"map_calc.i"
.include	"joypad.i"
.include	"ppu_bss.i"
.include	"npc.i"
.include	"sprite.i"
.include	"window.i"
.include	"rpg.i"
.include	"flags.i"
.include	"tunables.i"
.include	"macros.i"

.macro blkset_m		flags
	lda	#flags
	sta	ppu_blkdel_flags
	sta	rpg_ppulayout
	jsr	map_block_mod
.endmacro

.macro npccycle_m
	lda	#NPCS_STOP
	sta	sprite_npc_stop
	jsr	sprites_draw
	lda	#NPCS_RUN
	sta	sprite_npc_stop
.endmacro

.macro movepre_m
	jsr	map_modalcheck

	lda	ppu_frame_count
	and	#ANI_FRAME_COUNT_MASK
	beq	:+ ; if (ppu_frame_count & ANI_FRAME_COUNT_MASK) {
		pla
		pla
		jmp	game_frame_next
	: ; }
.endmacro

.macro bgflip_m
	lda	ppu_nmi_bg_high
	eor	#1
	sta	ppu_nmi_bg_high
.endmacro

; ------------------------------------------------------------
.segment	"ZEROPAGE"

	.export map_door_pos_buffer, map_door_pos_buffer_end
	.export map_door_x_pos, map_door_y_pos
map_door_pos_buffer:
map_door_x_pos:		.byte	0
map_door_y_pos:		.byte	0
			.byte	0
			.byte	0
			.byte	0
			.byte	0
			.byte	0
			.byte	0
			.byte	0
			.byte	0
			.byte	0
			.byte	0
			.byte	0
			.byte	0
			.byte	0
			.byte	0

	.export	map_chest_tbl, map_chest_tbl_end
	.export map_chest_x_pos, map_chest_y_pos
map_chest_tbl:
map_chest_x_pos:	.byte	0
map_chest_y_pos:	.byte	0
			.byte	0
			.byte	0
			.byte	0
			.byte	0
			.byte	0
			.byte	0
			.byte	0
			.byte	0
			.byte	0
			.byte	0
			.byte	0
			.byte	0
			.byte	0
			.byte	0
map_chest_tbl_end:
map_door_pos_buffer_end:

	.export map_player_x, map_player_y
map_player_x:		.byte	0
map_player_y:		.byte	0
; ------------------------------------------------------------
.segment	"CODE"

	.export map_transition0, map_transition1, map_transition2
map_transition0:
	apusfx_m	sfx_stairs
	jsr	apu_play

map_transition1:
	jsr	ppu_unload

map_transition2:
	jsr	oam_init
	jsr	text_initmap
	jsr	text_getblkno

	lda	#$F2
	sta	map_tile_ypos
	: ; for (map_tile_ypos = $F2; map_tile_ypos != $10; map_tile_ypos += 2) {
		jsr	ppu_nmi_wait

		lda	#$EE
		sta	map_tile_xpos
		: ; for (map_tile_xpos = $EE; map_tile_xpos != 0; map_tile_xpos += 2) {
			blkset_m	0

			inc	map_tile_xpos
			inc	map_tile_xpos
			bne	:-
		; }

		jsr	ppu_nmi_wait

		: ; for (map_tile_xpos = 0; map_tile_xpos < $12; map_tile_xpos += 2) {
			blkset_m	0

			inc	map_tile_xpos
			inc	map_tile_xpos
			lda	map_tile_xpos
			cmp	#$12
			bne	:-
		; }

		inc	map_tile_ypos
		inc	map_tile_ypos
		lda	map_tile_ypos
		cmp	#$10
		bne	:---
	; }

	jsr	ppu_nmi_wait

	npccycle_m

	jsr	ppu_nmi_wait

	ldx	map_eng_number
	lda	map_songs,x
	jsr	apu_play

	movw_m	ppu_pal_obj_fade, ppu_pal_fade_obj
	lda	#PPU_PAL_BG_TRUE
	sta	ppu_pal_fade_has_bg
	lda	#<(ppu_pal_overworld)
	clc
	adc	map_type
	sta	ppu_pal_fade_bg
	lda	#>(ppu_pal_overworld)
	adc	#0
	sta	ppu_pal_fade_bg+1
	jsr	ppu_pal_fadein

	movw_m	(1<<13)|game_buffer+2, ppu_nmi_write_dest
	lda	#<((tile_blank_chr00-chr00_base)/PPU_TILE_SIZE)
	sta	ppu_nmi_write_data

	lda	#$0F
	sta	ppu_blk_counter
	: ; for (ppu_blk_counter = $0F; ppu_blk_counter > 0; ppu_blk_counter--) {
		jsr	ppu_nmi_wait

		ldy	#$1C
		: ; for (y = $1C; y > 0; y--) {
			jsr	ppu_nmi_write

			dey
			bne	:-
		; }

		lda	ppu_nmi_write_dest
		clc
		adc	#4
		sta	ppu_nmi_write_dest
		bcc	:+
			inc	ppu_nmi_write_dest+1
		:

		ldy	#$1C
		: ; for (y = $1C; y > 0; y--) {
			jsr	ppu_nmi_write

			dey
			bne	:-
		; }

		lda	ppu_nmi_write_dest
		clc
		adc	#4
		sta	ppu_nmi_write_dest
		bcc	:+
			inc	ppu_nmi_write_dest+1
		:

		dec	ppu_blk_counter
		bne	:-----
	; }

	jsr	ppu_nmi_wait

	rts

	.export map_songs
map_songs:
	.byte	APU_SONG_NULL, APU_SONG_OVERWORLD, APU_SONG_DUNGEON0, APU_SONG_DUNGEON3
	.byte	APU_SONG_CASTLE_COURTYARD, APU_SONG_CASTLE_THRONEROOM, APU_SONG_DUNGEON7, APU_SONG_TOWN
	.byte	APU_SONG_TOWN, APU_SONG_TOWN, APU_SONG_TOWN, APU_SONG_TOWN
	.byte	APU_SONG_CASTLE_COURTYARD, APU_SONG_CASTLE_COURTYARD, APU_SONG_CASTLE_COURTYARD, APU_SONG_DUNGEON1
	.byte	APU_SONG_DUNGEON2, APU_SONG_DUNGEON3, APU_SONG_DUNGEON4, APU_SONG_DUNGEON5
	.byte	APU_SONG_DUNGEON6, APU_SONG_DUNGEON0, APU_SONG_DUNGEON0, APU_SONG_DUNGEON1
	.byte	APU_SONG_DUNGEON0, APU_SONG_DUNGEON1, APU_SONG_DUNGEON2, APU_SONG_DUNGEON3
	.byte	APU_SONG_DUNGEON0, APU_SONG_DUNGEON1

map_collision:
	lda	sprite_char_x
	sta	map_blk_col
	lda	sprite_char_y
	sta	map_blk_row
	jsr	map_get_blkid
	lda	map_blkid
	cmp	#$0E
	bcc	@checknpc

@collided:
	lda	map_player_x
	sta	sprite_char_x
	lda	map_player_y
	sta	sprite_char_y
	pla
	pla
	pla
	pla
	apusfx_m	sfx_bump
	jsr	apu_play
	lda	#0
	sta	ppu_frame_count
	jmp	game_frame_next

@checknpc:
	lda	sprite_check_flag
	cmp	#$FF
	bne	:+ ; if (sprite_check_flag == $FF) {
		rts
	: ; } else {
		ldx	#0
		: ; for (index = 0; index < NPC_MAX; index++) {
			lda	sprite_npc_x,x
			and	#PPU_POS_MASK
			cmp	sprite_char_x
			bne	:+
			lda	sprite_npc_y,x
			and	#PPU_POS_MASK
			cmp	sprite_char_y
			bne	:+ ; if (sprite_char_x == sprite_npc_x[x] && sprite_char_y == sprite_npc_y[x]) {
				jmp	@collided
			: ; }

			inx
			inx
			inx
			cpx	#(sprite_npc_tbl_end-sprite_npc_tbl)
			bne	:--
		; }
	; }

	rts

	.export map_doorcheck, map_dodoor
map_doorcheck:
	lda	map_width
	cmp	map_player_x
	bcc	:+
	lda	map_height
	cmp	map_player_y
	bcc	:+ ; if (map_player_x < map_width || map_player_y < map_height) {
		jmp	game_checkevent
	: ; } else { 
	map_dodoor:
		jsr	dialog_bank_high

		ldx	#0
		lda	map_eng_number
		: ; for (index = 0; index < MAP_MAX; index++) {
			cmp	ppu_nmi_buffer,x
			beq	@mapfound

			inx
			inx
			inx
			cpx	#MAP_MAX*3
			bne	:-
		; }

		rts

	@mapfound:
		jmp	dialog_mapswitch_high
	; }

	.export map_modalcheck
map_modalcheck:
	lda	game_buffer+$84
	cmp	#GAME_BUFFER_CLEAR
	bne	:+ ; if (game_buffer[84] == GAME_BUFFER_CLEAR) {
		rts
	: ; }

	lda	ppu_frame_count
	pha
	window_function_clear_m	((window_main_field-tbl_windows)/FUNC_WINDOW_DEF_SIZE)
	pla
	sta	ppu_frame_count
	rts

	.export map_moveright
map_moveright:
	movepre_m
	inc	sprite_char_x
	jsr	map_collision

	lda	map_type
	cmp	#map_types::dungeon
	bne	:+++ ; if (map_type == dungeon) {
		inc	map_player_x
		jsr	map_updrow
		lda	sprite_x
		clc
		adc	#MAP_MOVE_VAL
		sta	sprite_x
		bcc	:+
			inc	sprite_x+1
		:

		jsr	map_moveend
		lda	sprite_x
		clc
		adc	#MAP_MOVE_VAL
		sta	sprite_x
		bcc	:+
			inc	sprite_x+1
		:

		jmp	sprites_draw
	: ; } else {
		lda	#$12
		sta	map_tile_xpos
		lda	#$F2
		sta	map_tile_ypos
		: ; for (map_tile_ypos = $F2; map_tile_ypos != $10; map_tile_ypos += 2) {
			jsr	ppu_nmi_wait

			blkset_m	0

			inc	map_tile_ypos
			inc	map_tile_ypos

			inc	ppu_nmi_scc_h
			inc	sprite_x
			jsr	sprites_draw

			lda	map_tile_ypos
			cmp	#$10
			bne	:-
		; }

		jsr	ppu_nmi_wait

		inc	ppu_nmi_scc_h
		bne	:+ ; if (++ppu_nmi_scc_h == 0) {
			bgflip_m
		: ; }

		inc	ppu_nt_xpos
		lda	#PPU_POS_MASK
		and	ppu_nt_xpos
		sta	ppu_nt_xpos
		inc	map_player_x
		inc	sprite_x
		bne	:+
			inc	sprite_x+1
		:

		jsr	sprites_draw
		jmp	map_coverdraw
	; }

map_updrow:
	lda	ppu_nt_xpos
	eor	#$10
	and	#PPU_POS_MASK
	sta	ppu_nt_xpos

	lda	#$FA
	sta	map_tile_ypos
	: ; for (map_tile_ypos = $FA; map_tile_ypos != 8; map_tile_ypos += 2) {
		jsr	ppu_nmi_wait

		lda	#$F9
		sta	map_tile_xpos
		: ; for (map_tile_xpos = $F9; map_tile_xpos != 9; map_tile_xpos += 2) {
			blkset_m	0

			inc	map_tile_xpos
			inc	map_tile_xpos
			lda	map_tile_xpos
			cmp	#9
			bne	:-
		; }

		inc	map_tile_ypos
		inc	map_tile_ypos
		lda	map_tile_ypos
		cmp	#8
		bne	:--
	; }

	jsr	ppu_nmi_wait

	bgflip_m
	rts

	.export map_moveend
map_moveend:
	lda	ppu_nt_xpos
	clc
	adc	#$10
	and	#PPU_POS_MASK
	sta	ppu_nt_xpos

	lda	#$FA
	sta	map_tile_ypos
	: ; for (map_tile_ypos = $FA; map_tile_ypos != 8; map_tile_ypos += 2) {
		jsr	ppu_nmi_wait

		lda	#$FA
		sta	map_tile_xpos
		: ; for (map_tile_xpos = $FA; map_tile_xpos != 8; map_tile_xpos += 2) {
			blkset_m	0

			inc	map_tile_xpos
			inc	map_tile_xpos
			lda	map_tile_xpos
			cmp	#8
			bne	:-
		; }

		inc	map_tile_ypos
		inc	map_tile_ypos
		lda	map_tile_ypos
		cmp	#8
		bne	:--
	; }

	jsr	ppu_nmi_wait
	
	bgflip_m
	rts

	.export map_moveleft
map_moveleft:
	movepre_m
	dec	sprite_char_x
	jsr	map_collision

	lda	map_type
	cmp	#map_types::dungeon
	bne	:+++ ; if (map_type == dungeon) {
		jsr	map_updrow
		dec	map_player_x
		lda	sprite_x
		sec
		sbc	#MAP_MOVE_VAL
		sta	sprite_x
		bcs	:+
			dec	sprite_x+1
		:

		jsr	map_moveend
		lda	sprite_x
		sec
		sbc	#MAP_MOVE_VAL
		sta	sprite_x
		bcs	:+
			dec	sprite_x+1
		:

		jmp	sprites_draw
	: ; } else {
		lda	#$EC
		sta	map_tile_xpos
		lda	#$F2
		sta	map_tile_ypos
		: ; for (map_tile_ypos = $F2; map_tile_ypos != $10; map_tile_ypos += $2) {
			jsr	ppu_nmi_wait

			blkset_m	0

			inc	map_tile_ypos
			inc	map_tile_ypos

			lda	ppu_nmi_scc_h
			sec
			sbc	#1
			sta	ppu_nmi_scc_h
			bcs	:+ ; if (--ppu_nmi_scc_h == 0) {
				bgflip_m
			: ; }

			lda	sprite_x
			sec
			sbc	#1
			sta	sprite_x
			bcs	:+
				dec	sprite_x+1
			:

			jsr	sprites_draw

			lda	map_tile_ypos
			cmp	#$10
			bne	:---
		; }

		jsr	ppu_nmi_wait

		dec	ppu_nmi_scc_h
		dec	ppu_nt_xpos
		lda	#PPU_POS_MASK
		and	ppu_nt_xpos
		sta	ppu_nt_xpos
		dec	map_player_x
		dec	sprite_x

		jsr	sprites_draw
		jmp	map_coverdraw
	; }

	.export map_movedown
map_movedown:
	movepre_m
	inc	sprite_char_y
	jsr	map_collision

	lda	map_type
	cmp	#map_types::dungeon
	bne	:+++ ; if (map_type == dungeon) {
		inc	map_player_y
		jsr	map_updcol
		lda	sprite_y
		clc
		adc	#MAP_MOVE_VAL
		sta	sprite_y
		bcc	:+
			inc	sprite_y+1
		:

		jsr	map_moveend
		lda	sprite_y
		clc
		adc	#MAP_MOVE_VAL
		sta	sprite_y
		bcc	:+
			inc	sprite_y+1
		:

		jmp	sprites_draw
	: ; } else {
		jsr	ppu_nmi_wait

		inc	ppu_nmi_scc_v
		inc	sprite_y
		jsr	sprites_draw

		lda	#$10
		sta	map_tile_ypos
		lda	#$EE
		sta	map_tile_xpos
		: ; for (map_tile_xpos = $EE; map_tile_xpos != $12; map_tile_xpos += $6) {
			lda	#3
			sta	ppu_blk_counter
			jsr	ppu_nmi_wait
			: ; for (ppu_blk_counter = 3; ppu_blk_counter > 0; ppu_blk_counter--) {
				blkset_m	$0C

				inc	map_tile_xpos
				inc	map_tile_xpos
				dec	ppu_blk_counter
				bne	:-
			; }

			inc	ppu_nmi_scc_v
			inc	sprite_y
			jsr	sprites_draw

			lda	map_tile_xpos
			cmp	#$12
			bne	:--
		; }

		lda	#$10
		sta	map_tile_ypos
		lda	#$EC
		sta	map_tile_xpos
		: ; for (map_tile_xpos = $EC; map_tile_xpos != $14; map_tile_xpos += $14) {
			lda	#5
			sta	ppu_blk_counter
			jsr	ppu_nmi_wait
			: ; for (ppu_blk_counter = 5; ppu_blk_counter > 0; ppu_blk_counter--) {
				jsr	ppu_position_rel

				lda	map_tile_xpos
				clc
				adc	#4
				sta	map_tile_xpos
				dec	ppu_blk_counter
				bne	:-
			; }

			inc	ppu_nmi_scc_v
			inc	sprite_y
			jsr	sprites_draw

			lda	map_tile_xpos
			cmp	#$14
			bne	:--
		; }

		lda	#$10
		sta	map_tile_ypos
		lda	#$EE
		sta	map_tile_xpos
		: ; for (map_tile_xpos = $EE; map_tile_xpos != $12; map_tile_xpos += 6) {
			lda	#3
			sta	ppu_blk_counter
			jsr	ppu_nmi_wait
			: ; for (ppu_blk_counter = 3; ppu_blk_counter = 0; ppu_blk_counter--) {
				blkset_m	$03

				inc	map_tile_xpos
				inc	map_tile_xpos
				dec	ppu_blk_counter
				bne	:-
			; }

			inc	ppu_nmi_scc_v
			inc	sprite_y
			jsr	sprites_draw

			lda	map_tile_xpos
			cmp	#$12
			bne	:--
		; }

		jsr	ppu_nmi_wait

		inc	ppu_nmi_scc_v
		lda	ppu_nmi_scc_v
		cmp	#PPU_SCANLINE_240
		bne	:+ ; if (++ppu_nmi_scc_v == PPU_SCANLINE_240) {
			lda	#0
			sta	ppu_nmi_scc_v
		: ; }

		inc	ppu_nt_ypos
		lda	ppu_nt_ypos
		cmp	#$0F
		bne	:+ ; if (++ppu_nt_ypos == $0F) {
			lda	#0
			sta	ppu_nt_ypos
		: ; }

		inc	map_player_y
		inc	sprite_y
		bne	:+
			inc	sprite_y+1
		:

		jsr	sprites_draw
		jmp	map_coverdraw
	; }

map_updcol:
	lda	ppu_nt_xpos
	clc
	adc	#$10
	and	#PPU_POS_MASK
	sta	ppu_nt_xpos

	lda	#$FA
	sta	map_tile_xpos
	: ; for (map_tipe_xpos = $FA; map_tile_xpos != 8; map_tile_xpos += 2) {
		jsr	ppu_nmi_wait

		lda	#$F9
		sta	map_tile_ypos
		: ; for (map_tile_ypos = $F9; map_tile_ypos = 9; map_tile_ypos += 2) {
			blkset_m	0

			inc	map_tile_ypos
			inc	map_tile_ypos
			lda	map_tile_ypos
			cmp	#9
			bne	:-
		; }

		inc	map_tile_xpos
		inc	map_tile_xpos
		lda	map_tile_xpos
		cmp	#8
		bne	:--
	; }

	jsr	ppu_nmi_wait
	
	bgflip_m
	rts

	.export map_moveup
map_moveup:
	movepre_m
	dec	sprite_char_y
	jsr	map_collision

	lda	map_type
	cmp	#map_types::dungeon
	bne	:+++ ; if (map_type == dungeon) {
		jsr	map_updcol
		dec	map_player_y
		lda	sprite_y
		sec
		sbc	#MAP_MOVE_VAL
		sta	sprite_y
		bcs	:+
			dec	sprite_y+1
		:

		jsr	map_moveend
		lda	sprite_y
		sec
		sbc	#MAP_MOVE_VAL
		sta	sprite_y
		bcs	:+
			dec	sprite_y+1
		:

		jmp	sprites_draw
	: ; } else {
		jsr	ppu_nmi_wait

		dec	ppu_nmi_scc_v
		lda	ppu_nmi_scc_v
		cmp	#$FF
		bne	:+ ; if (--ppu_nmi_scc_v == $FF) {
			lda	#$EF
			sta	ppu_nmi_scc_v
		: ; }

		lda	sprite_y
		sec
		sbc	#1
		sta	sprite_y
		bcs	:+
			dec	sprite_y+1
		:

		jsr	sprites_draw

		lda	#MAP_BOTTOM
		sta	map_tile_ypos
		lda	#$EE
		sta	map_tile_xpos
		: ; for (map_tile_xpos = $EE; map_tile_xpos != $12; map_tile_xpos += 6) {
			lda	#3
			sta	ppu_blk_counter
			jsr	ppu_nmi_wait
			: ; for (ppu_blk_counter = 3; ppu_blk_counter > 0; ppu_blk_counter--) {
				blkset_m	$03

				inc	map_tile_xpos
				inc	map_tile_xpos
				dec	ppu_blk_counter
				bne	:-
			; }

			dec	ppu_nmi_scc_v
			dec	sprite_y
			jsr	sprites_draw

			lda	map_tile_xpos
			cmp	#$12
			bne	:--
		; }

		lda	#MAP_BOTTOM
		sta	map_tile_ypos
		lda	#$EC
		sta	map_tile_xpos
		: ; for (map_tile_xpos = $EC; map_tile_xpos != $14; map_tile_xpos += $14) {
			lda	#5
			sta	ppu_blk_counter
			jsr	ppu_nmi_wait
			: ; for (ppu_blk_counter = 5; ppu_blk_counter > 0; ppu_blk_counter--) {
				jsr	ppu_position_rel

				lda	map_tile_xpos
				clc
				adc	#4
				sta	map_tile_xpos
				dec	ppu_blk_counter
				bne	:-
			; }

			dec	ppu_nmi_scc_v
			dec	sprite_y
			jsr	sprites_draw

			lda	map_tile_xpos
			cmp	#$14
			bne	:--
		; }

		lda	#MAP_BOTTOM
		sta	map_tile_ypos
		lda	#$EE
		sta	map_tile_xpos
		: ; for (map_tile_xpos = $EE; map_tile_xpos != $12; map_tile_xpos += 6) {
			lda	#3
			sta	ppu_blk_counter
			jsr	ppu_nmi_wait
			: ; for (ppu_blk_counter = 3; ppu_blk_counter > 0; ppu_blk_counter--) {
				blkset_m	$0C

				inc	map_tile_xpos
				inc	map_tile_xpos
				dec	ppu_blk_counter
				bne	:-
			; }

			dec	ppu_nmi_scc_v
			dec	sprite_y
			jsr	sprites_draw

			lda	map_tile_xpos
			cmp	#$12
			bne	:--
		; }

		jsr	ppu_nmi_wait

		dec	ppu_nmi_scc_v
		dec	ppu_nt_ypos
		lda	ppu_nt_ypos
		cmp	#$FF
		bne	:+ ; if (--ppu_nt_ypos == $FF) {
			lda	#$0E
			sta	ppu_nt_ypos
		: ; }

		dec	map_player_y
		dec	sprite_y

		jsr	sprites_draw
		jmp	map_coverdraw
	; }

map_coverdraw:
	lda	map_player_x
	sta	map_npc_xpos
	lda	map_player_y
	sta	map_npc_ypos
	jsr	area_check

	lda	area_check_retval
	cmp	map_cover_stat
	bne	:+ ; if (area_check_retval == map_cover_stat) {
		rts
	: ; }

	sta	map_cover_stat

	lda	map_cover_stat
	beq	:+ ; if (map_cover_stat) {
		jsr	ppu_nmi_wait

		lda	#PPU_SCANLINE_240
		sta	oam_buffer
		sta	oam_buffer+4
		sta	oam_buffer+8
		sta	oam_buffer+12
	: ; }

	lda	ppu_nt_xpos
	clc
	adc	#$10
	and	#PPU_POS_MASK
	sta	ppu_nt_xpos

	lda	#$F2
	sta	map_tile_ypos
	: ; for (map_tile_ypos = $F2; map_tile_ypos != $10; map_tile_ypos += $2) {
		jsr	ppu_nmi_wait

		lda	#$F0
		sta	map_tile_xpos
		: ; for (map_tile_xpos = $F0; map_tile_xpos != 0; map_tile_xpos += $2) {
			blkset_m	0

			inc	map_tile_xpos
			inc	map_tile_xpos
			lda	map_tile_xpos
			cmp	#0
			bne	:-
		; }

		jsr	ppu_nmi_wait

		: ; for (map_tile_xpos = $00; map_tile_xpos < $10; map_tile_xpos += $2) {
			blkset_m	0

			inc	map_tile_xpos
			inc	map_tile_xpos
			lda	map_tile_xpos
			cmp	#$10
			bne	:-
		; }

		inc	map_tile_ypos
		inc	map_tile_ypos
		lda	map_tile_ypos
		cmp	#$10
		bne	:---
	; }

	jsr	ppu_nmi_wait

	lda	#1
	sta	ppu_frame_count

	npccycle_m
	bgflip_m

	lda	#$EE
	sta	map_tile_xpos
	: ; for (map_tile_xpos = $EE; map_tile_xpos != $32; map_tile_xpos += $22) {
		jsr	ppu_nmi_wait

		lda	#$F2
		sta	map_tile_ypos
		: ; for (map_tile_ypos = $F2; map_tile_ypos != 2; map_tile_ypos += $02) {
			blkset_m	0

			inc	map_tile_ypos
			inc	map_tile_ypos
			lda	map_tile_ypos
			cmp	#2
			bne	:-
		; }

		jsr	ppu_nmi_wait

		: ; for (map_tile_ypos = $00; map_tile_ypos < $10; map_tile_ypos += $02) {
			blkset_m	0

			inc	map_tile_ypos
			inc	map_tile_ypos
			lda	map_tile_ypos
			cmp	#$10
			bne	:-
		; }

		lda	map_tile_xpos
		clc
		adc	#$22
		sta	map_tile_xpos
		cmp	#$32
		bne	:---
	; }

	rts
