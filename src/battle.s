.include	"system/cpu.i"
.include	"system/cnrom.i"

.include	"joypad.i"
.include	"ppu_nmi.i"
.include	"ppu_nmi_write.i"
.include	"ppu_pal.i"
.include	"apu_commands.i"
.include	"map.i"
.include	"sprite.i"
.include	"window.i"
.include	"math.i"
.include	"npc.i"
.include	"map_calc.i"
.include	"rpg.i"
.include	"flags.i"
.include	"general_bss.i"
.include	"game_bss.i"
.include	"enemies.i"
.include	"battle.i"
.include	"dialogs.i"
.include	"tunables.i"
.include	"macros.i"

PPU_DEST_212D	= $212D
ENEMY_PAL_CNROM_SIZE	= $C

battle_dmg			= zeroword
reward				= zeroword
battle_palette_load_count	= dialog_item_idx

	.export battle_init
	.export battle_palette_load_enemy
battle_init:
	sta	battle_enemy
	sta	battle_enemy_chr_index
	cmp	#enemy::dragonlord
	bne	:+ ; if (battle_enemy_chr_index == dragonlord) {
		apusong_m	apu_song_boss
		bne	:++
	: ; } else {
		apusong_m	apu_song_battle
	: ; }
	jsr	apu_play

	lda	#0
	sta	battle_enemy_chr_index+1
	asl	battle_enemy_chr_index
	rol	battle_enemy_chr_index+1
	asl	battle_enemy_chr_index
	rol	battle_enemy_chr_index+1
	asl	battle_enemy_chr_index
	rol	battle_enemy_chr_index+1
	asl	battle_enemy_chr_index
	rol	battle_enemy_chr_index+1

	lda	battle_enemy_chr_index
	clc
	adc	#<(CNROM_BANK_INDEX_3_H|tbl_enemy_props_addr)
	sta	cnrom_load_src
	lda	battle_enemy_chr_index+1
	adc	#>(CNROM_BANK_INDEX_3_H|tbl_enemy_props_addr)
	sta	cnrom_load_src+1
	lda	#0
	sta	cnrom_load_dest
	sta	cnrom_load_count+1
	lda	#<(ptr_enemy_size)
	sta	cnrom_load_count
	lda	#>(tbl_enemy_props)
	sta	cnrom_load_dest+1

	lda	battle_enemy
	pha
		lda	#0
		sta	battle_enemy
		jsr	cnrom_load
	pla
	sta	battle_enemy

	cmp	#enemy::dragon_red
	bne	:+ ; if (battle_enemy == dragon_red) {
		lda	#$46
		sta	tbl_enemy_props+ENEMY_PTR_STACK
		lda	#$FA
		sta	tbl_enemy_props+ENEMY_PTR_STACK+1
		bne	:++
	: ; } else {
		lda	#$FA
		sta	tbl_enemy_props+ENEMY_PTR_STACK
	: ; }

	jsr	sprites_draw

	lda	game_player_flags
	and	#<~(player_flags::snooze_player|player_flags::snooze_enemy|player_flags::fizzle_enemy|player_flags::fizzle_player)
	sta	game_player_flags

	jsr	dialog_combat

	lda	battle_enemy
	pha
		lda	#0
		sta	battle_enemy
		jsr	stats_load
	pla
	sta	battle_enemy

	asl	a
	tax
	lda	tbl_enemy_spr_idx,x
	clc
	adc	#<(CNROM_BANK_INDEX_3_H|tbl_enemy_spr_addr)
	sta	cnrom_load_src
	lda	tbl_enemy_spr_idx+1,x
	and	#$7F
	adc	#>(CNROM_BANK_INDEX_3_H|tbl_enemy_spr_addr)
	sta	cnrom_load_src+1
	lda	tbl_enemy_spr_idx+1,x
	and	#$80
	sta	ppu_nmi_apply_mirror
	lda	sprite_check_flag
	ora	#$80
	sta	sprite_check_flag
	jsr	dialog_load_enemy
	window_function_m	((window_dialog-tbl_windows)/FUNC_WINDOW_DEF_SIZE)
	lda	battle_enemy
	cmp	#enemy::dragonlord
	bne	:+ ; if (battle_enemy == dragonlord) {
		window_high_m	((str_119_offset-tbl_string_offsets)/WORD_SIZE)
		lda	tbl_enemy_props+ENEMY_PTR_HP
		bne	battle_enemy_preempt
	: ; }

	jsr	dialog_string_enemy
	window_low_m	((str_0E2_offset-tbl_string_offsets)/WORD_SIZE)
	lda	tbl_enemy_props+ENEMY_PTR_HP
	sta	math_mulw_b
	jsr	math_rand
	lda	math_rand_word+1
	sta	math_mulw_a
	lda	#0
	sta	math_mulw_a+1
	sta	math_mulw_b+1
	jsr	math_mulw
	lda	math_result+1
	lsr	a
	lsr	a
	sta	math_result
	lda	tbl_enemy_props+ENEMY_PTR_HP
	sec
	sbc	math_result

	battle_enemy_preempt:

	sta	battle_enemy_hp
	jsr	battle_tryrun_enemy
	jsr	battle_next_turn
	bcs	:+ ; if (battle_next_turn() == enemy) {
		jsr	dialog_string_enemy
		window_low_m	((str_0E4_offset-tbl_string_offsets)/WORD_SIZE)
		jmp	battle_turn_enemy
	: ; }

battle_turn_player:
	window_function_m	((window_main_field-tbl_windows)/FUNC_WINDOW_DEF_SIZE)
	lda	game_player_flags
	bpl	:++ ; if (game_player_flags & snooze_player) {
		jsr	math_rand
		lda	math_rand_word+1
		lsr	a
		bcs	:+ ; if (math_rand % 1) {
		battle_turn_snoozed:
			window_high_m	((str_107_offset-tbl_string_offsets)/WORD_SIZE)
			jmp	battle_turn_enemy
		: ; }

		lda	game_player_flags
		and	#<~(player_flags::snooze_player)
		sta	game_player_flags
		window_high_m	((str_108_offset-tbl_string_offsets)/WORD_SIZE)
	: ; }

battle_playermenu:
	window_low_m	((str_0E8_offset-tbl_string_offsets)/WORD_SIZE)
	window_function_m	((window_cmd_battle-tbl_windows)/FUNC_WINDOW_DEF_SIZE)
	lda	window_choice
	beq	battle_doattack_player
	jmp	battle_dospell_player

battle_doattack_player:
	window_function_clear_m	((window_cmd_battle-tbl_windows)/FUNC_WINDOW_DEF_SIZE)
	apusfx_m	sfx_hit
	jsr	apu_play
	window_low_m	((str_0E5_offset-tbl_string_offsets)/WORD_SIZE)
	lda	rpg_att
	sta	battle_fight_attack
	lda	tbl_enemy_props+ENEMY_PTR_DEF
	sta	battle_fight_defense
	lda	battle_enemy
	cmp	#enemy::dragonlord_weak
	beq	:+
	cmp	#enemy::dragonlord
	beq	:+
	jsr	math_rand
	lda	math_rand_word+1
	and	#$1F
	bne	:+ ; if (battle_enemy.not_in(dragonlord_weak, dragonlord) && !(math_rand() & $1F)) {
		apusfx_m	sfx_power
		jsr	apu_play
		jsr	dialog_string_enemy
		window_high_m	((str_104_offset-tbl_string_offsets)/WORD_SIZE)
		jsr	math_rand
		lda	math_rand_word+1
		sta	math_mulw_a
		lda	rpg_att
		lsr	a
		sta	math_mulw_b
		lda	#0
		sta	math_mulw_a+1
		sta	math_mulw_b+1
		jsr	math_mulw
		lda	rpg_att
		sec
		sbc	math_result+1
		jmp	:++
	: ; } else {
		jsr	battle_strike_enemy
		lda	battle_fight_hit
		bne	:+ ; if (!battle_fight_hit) {
			apusfx_m	sfx_miss1
			jsr	apu_play
			window_low_m	((str_0E7_offset-tbl_string_offsets)/WORD_SIZE)
			jmp	battle_turn_enemy
		; }
	: ; }

	sta	battle_dmg
	lda	#0
	sta	battle_dmg+1
	bit	game_player_flags
	bvs	:+
	jsr	math_rand
	lda	math_rand_word+1
	and	#$3F
	sta	math_rand_word+1
	lda	tbl_enemy_props+ENEMY_PTR_MDEF
	and	#$0F
	beq	:+
	sec
	sbc	#1
	cmp	math_rand_word+1
	bcc	:+ ; if ((ENEMY_PTR_MDEF & $F) - 1 > math_rand()) {
		jsr	dialog_string_enemy
		apusfx_m	sfx_miss1
		jsr	apu_play
		window_high_m	((str_10A_offset-tbl_string_offsets)/WORD_SIZE)
		jmp	battle_turn_enemy

	battle_dodamage_magic:
		sta	battle_dmg
		lda	#0
		sta	battle_dmg+1
	: ; }

	apusfx_m	sfx_strike
	jsr	apu_play
	movw_m	ppu_pal_battle_flash, battle_palette_addr
	jsr	battle_palette_load
	jsr	dialog_string_enemy
	window_low_m	((str_0E6_offset-tbl_string_offsets)/WORD_SIZE)
	jmp	battle_postattack_player

battle_dospell_player:
	cmp	#2
	beq	:+ ; if (window_choice == 2) {
		jmp	battle_doitem_player
	: ; }

	lda	rpg_flags
	sta	general_word_1
	lda	rpg_flags+1
	and	#(state_flags::sizzle|state_flags::midheal)>>8
	sta	general_word_1+1
	ora	general_word_1
	bne	:+ ; if (general_word_1) {
		window_function_clear_m	((window_cmd_battle-tbl_windows)/FUNC_WINDOW_DEF_SIZE)
		window_low_m	((str_031_offset-tbl_string_offsets)/WORD_SIZE)
		jmp	battle_turn_player
	: ; }

	jsr	dialog_spell
	cmp	#$FF
	bne	:+ ; if (dialog_spell() == 0) {
		window_function_clear_m	((window_cmd_battle-tbl_windows)/FUNC_WINDOW_DEF_SIZE)
		jmp	battle_turn_player
	: ; }
	pha
	window_function_clear_m	((window_cmd_battle-tbl_windows)/FUNC_WINDOW_DEF_SIZE)
	pla
	cmp	#3
	beq	:+
	cmp	#7
	beq	:+
	cmp	#5
	beq	:+
	cmp	#6
	bne	:++
	: ; if (spell.in(3, 5, 6, 7)) {
	battle_spell_fail:
		window_low_m	((str_0E9_offset-tbl_string_offsets)/WORD_SIZE)
		jmp	battle_turn_player
	: ; }

	jsr	dialog_spell_mpcheck
	cmp	#<((str_032_offset-tbl_string_offsets)/WORD_SIZE)
	bne	:+ ; if (dialog_spell_mpcheck() == ((str_032_offset-tbl_string_offsets)/WORD_SIZE)) {
		jsr	window_string_low_a
		jmp	battle_turn_player
	: ; }

	sta	window_choice
	lda	game_player_flags
	and	#player_flags::fizzle_player
	beq	:+ ; if (game_player_flags & fizzle_player) {
		window_low_m	((str_0EA_offset-tbl_string_offsets)/WORD_SIZE)
		jmp	battle_turn_enemy
	: ; }

	lda	window_choice ; switch (window_choice) {
	cmp	#0
	bne	:+ ; case 0:
		jsr	dialog_spell_heal
		jmp	battle_turn_enemy

	: cmp	#8
	bne	:+ ; case 8:
		jsr	dialog_spell_midheal
		jmp	battle_turn_enemy

	: cmp	#1
	bne	:+ ; case 1:
		lda	tbl_enemy_props+ENEMY_PTR_MDEF
		lsr	a
		lsr	a
		lsr	a
		lsr	a
		jsr	battle_enemy_stat_check

		jsr	math_rand
		lda	math_rand_word+1
		and	#SPELL_SIZZ_MAXOFFSET
		clc
		adc	#SPELL_SIZZ_BASE
		jmp	battle_dodamage_magic

	: cmp	#9
	bne	:+ ; case 9:
		lda	tbl_enemy_props+ENEMY_PTR_MDEF
		lsr	a
		lsr	a
		lsr	a
		lsr	a
		jsr	battle_enemy_stat_check

		jsr	math_rand
		lda	math_rand_word+1
		and	#SPELL_SIZZLE_MAXOFFSET
		clc
		adc	#SPELL_SIZZLE_BASE
		jmp	battle_dodamage_magic

	: cmp	#2
	bne	:+ ; case 2:
		lda	tbl_enemy_props+ENEMY_PTR_AGI
		lsr	a
		lsr	a
		lsr	a
		lsr	a
		jsr	battle_enemy_stat_check
		jsr	dialog_string_enemy
		window_low_m	((str_0EC_offset-tbl_string_offsets)/WORD_SIZE)
		lda	game_player_flags
		ora	#player_flags::snooze_enemy
		sta	game_player_flags
		jmp	battle_asleep_enemy

	: ; default:
		lda	tbl_enemy_props+ENEMY_PTR_AGI
		and	#$0F
		jsr	battle_enemy_stat_check
		jsr	dialog_string_enemy
		window_low_m	((str_0ED_offset-tbl_string_offsets)/WORD_SIZE)
		lda	game_player_flags
		ora	#player_flags::fizzle_enemy
		sta	game_player_flags
		jmp	battle_turn_enemy
	; }

battle_doitem_player:
	cmp	#3
	beq	:+ ; if (window_choice == 3) {
		jmp	battle_dorun_player
	: ; }

	jsr	dialog_inv_list
	cpx	#1
	bne	:+ ; if (x == 1) {
		window_function_clear_m	((window_cmd_battle-tbl_windows)/FUNC_WINDOW_DEF_SIZE)
		window_low_m	((str_03D_offset-tbl_string_offsets)/WORD_SIZE)
		jmp	battle_turn_player
	: ; }

	window_function_m	((window_tool-tbl_windows)/FUNC_WINDOW_DEF_SIZE)
	cmp	#$FF
	bne	:+ ; if (window_res == $FF) {
		window_function_clear_m	((window_cmd_battle-tbl_windows)/FUNC_WINDOW_DEF_SIZE)
		jmp	battle_turn_player
	: ; }
	pha
	window_function_clear_m	((window_cmd_battle-tbl_windows)/FUNC_WINDOW_DEF_SIZE)
	pla
	tax
	lda	dialog_buffer+1,x ; switch (game_win_ppuaddr[window_res]) {
	cmp	#2
	bne	:+ ; case 2:
		dec	rpg_herbs
		window_low_m	((str_0F7_offset-tbl_string_offsets)/WORD_SIZE)
		jsr	dialog_inv_doherb
		jmp	battle_turn_enemy

	: cmp	#8
	bne	:++ ; case 8:
		window_low_m	((str_03C_offset-tbl_string_offsets)/WORD_SIZE)
		apusong_m	apu_song_flute
		jsr	apu_play
		jsr	apu_wait_music
		apusong_m	apu_song_battle_cont
		jsr	apu_play
		lda	battle_enemy
		cmp	#enemy::golem
		bne	:+ ; if (battle_enemy == golem) {
			window_low_m	((str_0F3_offset-tbl_string_offsets)/WORD_SIZE)
			lda	game_player_flags
			ora	#player_flags::snooze_enemy
			sta	game_player_flags
			jmp	battle_asleep_enemy
		: ; }

		window_low_m	((str_033_offset-tbl_string_offsets)/WORD_SIZE)
		jmp	battle_turn_enemy

	: cmp	#$0D
	bne	:+ ; case $0D:
		window_low_m	((str_041_offset-tbl_string_offsets)/WORD_SIZE)
		apusong_m	apu_song_lyre
		jsr	apu_play
		jsr	apu_wait_music
		apusong_m	apu_song_battle_cont
		jsr	apu_play
		jsr	dialog_string_enemy
		window_low_m	((str_0F4_offset-tbl_string_offsets)/WORD_SIZE)
		jmp	battle_turn_enemy

	: cmp	#$07
	bne	:+ ; case 7:
		jsr	dialog_inv_dragonscale
		jmp	battle_turn_enemy

	: cmp	#$09
	bne	:+ ; case 9:
		jsr	dialog_inv_ring
		jmp	battle_turn_enemy

	: cmp	#$0C
	bne	:+ ; case $0C:
		jsr	dialog_inv_cursedbelt
		apusong_m	apu_song_battle_cont
		jsr	apu_play
		jmp	battle_turn_enemy

	: cmp	#$0E
	bne	:+ ; case $0E:
		jsr	dialog_inv_cursednecklace
		apusong_m	apu_song_battle_cont
		jsr	apu_play
		jmp	battle_turn_enemy

	: ; default:
		jmp	battle_spell_fail
	; }

battle_dorun_player:
	cmp	#1
	beq	:+ ; if (window_choice == 1) {
		jmp	battle_playermenu
	: ; }

	window_function_clear_m	((window_cmd_battle-tbl_windows)/FUNC_WINDOW_DEF_SIZE)
	apusfx_m	sfx_run
	jsr	apu_play
	window_low_m	((str_0F5_offset-tbl_string_offsets)/WORD_SIZE)
	bit	game_player_flags
	bvs	:+
	jsr	battle_run_roll
	bcs	:+ ; if () {
		window_low_m	((str_0F6_offset-tbl_string_offsets)/WORD_SIZE)
		jmp	battle_turn_enemy
	: ; }

	ldx	map_eng_number
	lda	map_songs,x
	jsr	apu_play
	lda	battle_enemy
	cmp	#enemy::dragonlord
	bne	:+ ; if (battle_enemy == dragonlord) {
		jsr	ppu_nmi_wait
		movw_m	ppu_pal_null, ppu_pal_load_src
		lda	#PPU_COLOR_LEV0
		sta	ppu_pal_load_cutoff
		sta	battle_enemy
		jsr	ppu_pal_load_obj
		jsr	ppu_pal_load_bg
		jsr	ppu_nmi_wait
		jsr	cnrom_bank_sec_00
		jsr	ppu_clearram
		jmp	map_transition2
	: ; }

	lda	map_eng_number
	cmp	#maps::map_03
	bne	:++
	lda	map_player_x
	cmp	#$12
	bne	:++
	lda	map_player_y
	cmp	#$0C
	bne	:++ ; if (map_eng_number == map_03 && map_player_x == $12 && map_player_y == $0C) {
		jsr	oam_init
		dec	map_player_x
		dec	sprite_char_x
		lda	sprite_x
		sec
		sbc	#$10
		sta	sprite_x
		bcs	:+
			dec	sprite_x+1
		:
		
		jmp	map_transition0
	: ; }

	lda	map_eng_number
	cmp	#maps::dungeon_greendragon
	bne	:++
	lda	map_player_x
	cmp	#4
	bne	:++
	lda	map_player_y
	cmp	#$0E
	bne	:++
	lda	game_story_flags
	and	#story_flags::defeated_greendragon
	bne	:++ ; if (map_eng_number == dungeon_greendragon && map_player_x == 4 && map_player_y == $0E && !(game_story_flags & defeated_greendragon)) {
	battle_run_questenemy:
		jsr	oam_init
		dec	map_player_y
		dec	sprite_char_y
		lda	sprite_y
		sec
		sbc	#$10
		sta	sprite_y
		bcs	:+
			dec	sprite_y+1
		:

		jmp	map_transition0
	: ; }

	lda	map_eng_number
	cmp	#maps::overworld
	bne	:+
	lda	map_player_x
	cmp	#GOLEM_X
	bne	:+
	lda	map_player_y
	cmp	#GOLEM_Y
	bne	:+ ; if (map_eng_number == overworld && map_player_x == GOLEM_X && map_player_y == GOLEM_Y) {
		lda	game_story_flags
		and	#story_flags::defeated_golem
		beq	battle_run_questenemy
	: ; }

	jsr	oam_init
	jmp	battle_exit_quick

battle_enemy_stat_check:
	sta	general_word_1
	jsr	math_rand
	lda	math_rand_word+1
	and	#$0F
	cmp	general_word_1
	bcc	:+ ; if ((math_rand() & 0xF) == general_word_1) {
		rts
	: ; }

	pla
	pla
	window_low_m	((str_0EB_offset-tbl_string_offsets)/WORD_SIZE)
	jmp	battle_turn_enemy

battle_postattack_player:
	lda	battle_enemy_hp
	sec
	sbc	battle_dmg
	sta	battle_enemy_hp
	bcc	:+
	beq	:+ ; if ((battle_enemy_hp -= battle_dmg) != 0) {
		jmp	battle_turn_enemy
	: ; }

	lda	battle_enemy
	cmp	#enemy::dragon_green
	bne	:+
	lda	map_eng_number
	cmp	#maps::dungeon_greendragon
	bne	:+ ; if (battle_enemy == dragon_green && map_eng_number == dungeon_greendragon) {
		lda	game_story_flags
		ora	#story_flags::defeated_greendragon
		sta	game_story_flags
		bne	:++
	: cmp	#enemy::golem
	bne	:+
	lda	map_eng_number
	cmp	#maps::overworld
	bne	:+ ; } else if (battle_enemy == golem && map_eng_number == overworld) {
		lda	game_story_flags
		ora	#story_flags::defeated_golem
		sta	game_story_flags
	: ; }

	jsr	dialog_string_enemy
	window_low_m	((str_0EE_offset-tbl_string_offsets)/WORD_SIZE)
	jsr	oam_init
	apusong_m	apu_song_win
	jsr	apu_play
	lda	battle_enemy
	cmp	#enemy::dragonlord_weak
	bne	:++ ; if (battle_enemy == dragonlord_weak) {
		ldx	#$50
		: ; for (x = $50; x > 0; x--) {
			jsr	ppu_nmi_wait
			dex
			bne	:-
		; }

		lda	#enemy::dragonlord
		jmp	battle_init
	: ; }

	cmp	#enemy::dragonlord
	bne	:++++ ; if (battle_enemy == dragonlord) {
		movw_m	PPU_DEST_212D, ppu_nmi_write_dest
		lda	#<((tile_blank_chr00-chr00_base)/PPU_TILE_SIZE)
		sta	ppu_nmi_write_data

		lda	#6
		sta	general_word_0
		: ; for (general_word = 6; general_word > 0; general_word--) {
			ldy	#7
			: ; for (y = 7; y > 0; y--) {
				jsr	ppu_nmi_write
				dey
				bne	:-
			; }
			lda	ppu_nmi_write_dest
			clc
			adc	#$19
			sta	ppu_nmi_write_dest
			bcc	:+
				inc	ppu_nmi_write_dest+1
			:

			dec	general_word_0
			bne	:---
		; }

		jsr	ppu_nmi_wait
		jsr	ppu_nmi_wait
		jsr	cnrom_bank_sec_00
		lda	#0
		sta	battle_enemy
		window_high_m	((str_11A_offset-tbl_string_offsets)/WORD_SIZE)
		lda	game_story_flags
		ora	#story_flags::defeated_dragonlord
		sta	game_story_flags
		jsr	dialog_wait_button
		lda	rpg_maxhp
		sta	rpg_hp
		lda	rpg_maxmp
		sta	rpg_mp
		ldx	#maps::map_12
		jmp	dialog_mapswitch_high
	: ; }

	lda	tbl_enemy_props+ENEMY_PTR_EXP
	sta	reward
	lda	#0
	sta	reward+1
	window_low_m	((str_0EF_offset-tbl_string_offsets)/WORD_SIZE)
	lda	reward
	clc
	adc	rpg_exp
	sta	rpg_exp
	bcc	:+
	inc	rpg_exp+1
	bne	:+ ; if (rpg_exp > $FF) {
		lda	#$FF
		sta	rpg_exp
		sta	rpg_exp+1
	: ; }

	lda	tbl_enemy_props+ENEMY_PTR_GOLD
	sta	math_mulw_b
	jsr	math_rand
	lda	math_rand_word+1
	and	#$3F
	clc
	adc	#$C0
	sta	math_mulw_a
	lda	#0
	sta	math_mulw_a+1
	sta	math_mulw_b+1
	jsr	math_mulw
	lda	math_result+1
	sta	reward
	lda	#0
	sta	reward+1
	window_low_m	((str_0F0_offset-tbl_string_offsets)/WORD_SIZE)
	lda	reward
	clc
	adc	rpg_gold
	sta	rpg_gold
	bcc	:+
	inc	rpg_gold+1
	bne	:+ ; if (rpg_gold > $FF) {
		lda	#$FF
		sta	rpg_gold
		sta	rpg_gold+1
	: ; }

	window_function_m	((window_main_field-tbl_windows)/FUNC_WINDOW_DEF_SIZE)
	lda	rpg_maxmp
	pha
	lda	rpg_maxhp
	pha
	lda	rpg_agi
	pha
	lda	rpg_str
	pha
	lda	rpg_level
	pha
	jsr	stats_load
	pla
	cmp	rpg_level
	bne	:+ ; if (rpg_level == last_level) {
		jsr	apu_wait_music
		ldx	map_eng_number
		lda	map_songs,x
		jsr	apu_play
		pla
		pla
		pla
		pla
		jmp	battle_exit
	: ; } else {
		apusong_m	apu_song_levelup
		jsr	apu_play
		jsr	apu_wait_music
		ldx	map_eng_number
		lda	map_songs,x
		jsr	apu_play
		window_low_m	((str_0F1_offset-tbl_string_offsets)/WORD_SIZE)
		lda	#0
		sta	reward+1
		pla
		sta	general_word_0
		lda	rpg_str
		sec
		sbc	general_word_0
		beq	:+ ; if (rpg_str > general_word_0) {
			sta	reward
			window_high_m	((str_10E_offset-tbl_string_offsets)/WORD_SIZE)
		: ; }

		pla
		sta	general_word_0
		lda	rpg_agi
		sec
		sbc	general_word_0
		beq	:+ ; if (rpg_agi > general_word_0) {
			sta	reward
			window_high_m	((str_10F_offset-tbl_string_offsets)/WORD_SIZE)
		: ; }

		pla
		sta	general_word_0
		lda	rpg_maxhp
		sec
		sbc	general_word_0
		sta	reward
		window_high_m	((str_110_offset-tbl_string_offsets)/WORD_SIZE)
		pla
		sta	general_word_0
		lda	rpg_maxmp
		sec
		sbc	general_word_0
		beq	:+ ; if (rpg_maxmp > general_word_0) {
			sta	reward
			window_high_m	((str_111_offset-tbl_string_offsets)/WORD_SIZE)
		: ; }

		lda	rpg_level
		cmp	#3
		beq	:+
		cmp	#4
		beq	:+
		cmp	#7
		beq	:+
		cmp	#9
		beq	:+
		cmp	#10
		beq	:+
		cmp	#12
		beq	:+
		cmp	#13
		beq	:+
		cmp	#15
		beq	:+
		cmp	#17
		beq	:+
		cmp	#19
		bne	:++
		: ; if (rpg_level.in(3, 4, 7, 9, 10, 12, 13, 15, 17, 19)) {
			window_low_m	((str_0F2_offset-tbl_string_offsets)/WORD_SIZE)
		: ; }

		window_function_m	((window_main_field-tbl_windows)/FUNC_WINDOW_DEF_SIZE)
		jmp	battle_exit
	; }

battle_turn_enemy:
	lda	game_player_flags
	and	#player_flags::snooze_enemy
	beq	:+++ ; if (game_player_flags & snooze_enemy) {
		: ; while (!(math_rand() & MATH_CHANCE_EQ_1_4)) {
			jsr	math_rand
			lda	math_rand_word+1
			and	#MATH_CHANCE_EQ_1_4
			beq	:-
		; }

		cmp	#1
		bne	:+ ; if (math_rand() == 1) {
			lda	game_player_flags
			and	#<~(player_flags::snooze_enemy)
			sta	game_player_flags

			jsr	dialog_string_enemy
			window_low_m	0
			jmp	:++
		: ; } else {
		battle_asleep_enemy:
			jsr	dialog_string_enemy
			window_low_m	$F8
			jmp	battle_turn_player
		; }
	: ; }

	jsr	battle_tryrun_enemy
	jsr	math_rand
	lda	tbl_enemy_props+ENEMY_PTR_SPELL
	and	#$30
	sta	general_word_0
	lda	math_rand_word+1
	and	#$30
	cmp	general_word_0
	bcs	:++++ ; if ((math_rand() & $30) < general_word_0) {
		lda	tbl_enemy_props+ENEMY_PTR_SPELL ; switch (ENEMY_PTR_SPELL & $C0) {
		and	#$C0
		bne	:+ ; case $00:
			lda	game_player_flags
			bmi	:++++ ; if (!(game_player_flags & snooze_player)) {
				jmp	battle_dosnooze_enemy
			; }

		: cmp	#$40
		bne	:+ ; case $40:
			lda	game_player_flags
			and	#player_flags::fizzle_player
			bne	:+++ ; if (!(game_player_flags & fizzle_player)) {
				jmp	battle_dofizzle_enemy
			; }

		: cmp	#$80
		bne	:+ ; case $80
			lda	tbl_enemy_props+ENEMY_PTR_HP
			lsr	a
			lsr	a
			cmp	battle_enemy_hp
			bcc	:++ ; if (battle_enemy_hp < (ENEMY_PTR_HP / 4)) {
				jmp	battle_doheal_enemy
			; }

		: ; default :
			lda	tbl_enemy_props+ENEMY_PTR_HP
			lsr	a
			lsr	a
			cmp	battle_enemy_hp
			bcc	:+ ; if (battle_enemy_hp < (ENEMY_PTR_HP / 4)) {
				jmp	battle_domidheal_enemy
			; }
		; }
	: ; }

	jsr	math_rand
	lda	tbl_enemy_props+ENEMY_PTR_SPELL
	and	#$03
	sta	general_word_0
	lda	math_rand_word+1
	and	#$03
	cmp	general_word_0
	bcs	:++++ ; if ((math_rand() & $03) < general_word_0) {
		lda	tbl_enemy_props+ENEMY_PTR_SPELL ; switch(ENEMY_PTR_SPELL & $C) {
		and	#$0C
		bne	:+ ; case 0:
			jmp	battle_dosizz_enemy

		: cmp	#4
		bne	:+ ; case 4:
			jmp	battle_dosizzcrit_enemy

		: cmp	#8
		bne	:+ ; case 8:
			jmp	battle_dosizzlecrit_enemy

		: ; default:
			jmp	battle_dosizzle_enemy
		; }
	: ; }

	apusfx_m	sfx_windup
	jsr	apu_play
	jsr	dialog_string_enemy
	window_low_m	((str_0F9_offset-tbl_string_offsets)/WORD_SIZE)
	lda	tbl_enemy_props+ENEMY_PTR_ATT
	sta	battle_fight_attack
	lda	rpg_def
	sta	battle_fight_defense
	jsr	battle_strike_player
	lda	battle_fight_hit
	bne	:+ ; if (!battle_fight_hit) {
		apusfx_m	sfx_miss2
		jsr	apu_play
		window_low_m	((str_0FB_offset-tbl_string_offsets)/WORD_SIZE)
		jmp	battle_turn_player
	: ; }

	sta	battle_dmg
	jmp	battle_postattack_enemy

battle_dospell_enemy:
	jsr	dialog_string_enemy
	window_low_m	((str_0FC_offset-tbl_string_offsets)/WORD_SIZE)
	lda	window_choice
	jsr	dialog_string_base
	window_low_m	((str_0FD_offset-tbl_string_offsets)/WORD_SIZE)
	apusfx_m	sfx_spell
	jsr	apu_play
	movw_m	ppu_pal_spell_fx, battle_palette_addr
	jsr	battle_palette_load
	jsr	apu_wait_music
	lda	game_player_flags
	and	#player_flags::fizzle_enemy
	bne	:+ ; if (!(game_player_flags & fizzle_enemy)) {
		rts
	: ; }

	window_low_m	((str_0EA_offset-tbl_string_offsets)/WORD_SIZE)
	pla
	pla
	jmp	battle_turn_player

battle_dosizz_enemy:
	lda	#$11
	sta	window_choice
	jsr	battle_dospell_enemy
	jsr	math_rand
	lda	math_rand_word+1
	and	#SPELL_SIZZ_MAXOFFSET
	clc
	adc	#SPELL_SIZZ_ENEMYBASE

battle_dosizzbase_enemy:
	sta	battle_dmg
	lda	rpg_equip
	and	#$1C
	cmp	#$1C
	beq	:+
	cmp	#$18
	bne	:++
	: ; if ((rpg_equip & $1C).in($18, $1C)) {
		lda	battle_dmg
		sta	math_div_a
		lda	#3
		sta	math_div_b
		jsr	math_divb
		lda	math_div_a
		asl	a
		sta	battle_dmg
	: ; }

	jmp	battle_postattack_enemy

battle_dosizzcrit_enemy:
	lda	#$19
	sta	window_choice
	jsr	battle_dospell_enemy
	jsr	math_rand
	lda	math_rand_word+1
	and	#SPELL_SIZZ_CRITOFFSET
	clc
	adc	#SPELL_SIZZ_ENEMYCRIT
	jmp	battle_dosizzbase_enemy

battle_dofizzle_enemy:
	lda	#$14
	sta	window_choice
	jsr	battle_dospell_enemy
	lda	rpg_equip
	and	#$1C
	cmp	#$1C
	beq	:++
	jsr	math_rand
	lda	math_rand_word+1
	lsr	a
	bcc	:++ ; if ((rpg_equip & $1C) != $1C || math_rand() % 1) {
		lda	game_player_flags
		ora	#player_flags::fizzle_player
		sta	game_player_flags
		lda	#<((str_0FE_offset-tbl_string_offsets)/WORD_SIZE)

		: jsr	window_string_low_a
		jmp	battle_turn_player
	: ; } else {
		lda	#<((str_0EB_offset-tbl_string_offsets)/WORD_SIZE)
		bne	:--
	; }

battle_dosnooze_enemy:
	lda	#$12
	sta	window_choice
	jsr	battle_dospell_enemy
	lda	game_player_flags
	ora	#player_flags::snooze_player
	sta	game_player_flags
	window_high_m	((str_106_offset-tbl_string_offsets)/WORD_SIZE)
	jmp	battle_turn_snoozed

battle_doheal_enemy:
	lda	#$10
	sta	window_choice
	jsr	battle_dospell_enemy
	jsr	math_rand
	lda	math_rand_word+1
	and	#SPELL_HEAL_MAXOFFSET
	clc
	adc	#SPELL_HEAL_ENEMYBASE

battle_dohealbase_enemy:
	clc
	adc	battle_enemy_hp
	cmp	tbl_enemy_props+ENEMY_PTR_HP
	bcc	:+ ; if ((math_rand() + spell_base + battle_enemy_hp) > ENEMY_PTR_HP) {
		lda	tbl_enemy_props+ENEMY_PTR_HP
	: ; }
	sta	battle_enemy_hp

	jsr	dialog_string_enemy
	window_high_m	((str_109_offset-tbl_string_offsets)/WORD_SIZE)
	jmp	battle_turn_player

battle_domidheal_enemy:
	lda	#$18
	sta	window_choice
	jsr	battle_dospell_enemy
	jsr	math_rand
	lda	math_rand_word+1
	and	#SPELL_MIDHEAL_MAXOFFSET
	clc
	adc	#SPELL_MIDHEAL_BASE
	bne	battle_dohealbase_enemy

battle_dosizzle_enemy:
	jsr	math_rand
	lda	math_rand_word+1
	and	#SPELL_SIZZLE_MAXOFFSET
	clc
	adc	#SPELL_SIZZLE_ENEMYBASE
	bne	:+ ; if ((math_rand() + SPELL_SIZZLE_ENEMYBASE) == 0) {
	battle_dosizzlecrit_enemy:
		jsr	math_rand
		lda	math_rand_word+1
		and	#SPELL_SIZZLE_MAXOFFSET
		ora	#SPELL_SIZZLE_ENEMYMASK
	: ; }

	sta	battle_dmg
	lda	#0
	sta	battle_dmg+1
	lda	rpg_equip
	and	#$1C
	cmp	#$1C
	bne	:+ ; if ((rpg_equip & $1C) == $1C) {
		lda	battle_dmg
		sta	math_div_a
		lda	#3
		sta	math_div_b
		jsr	math_divb
		lda	math_div_a
		asl	a
		sta	battle_dmg
	: ; }

	apusfx_m	sfx_fire
	jsr	apu_play
	jsr	dialog_string_enemy
	window_low_m	((str_0FF_offset-tbl_string_offsets)/WORD_SIZE)

battle_postattack_enemy:
	apusfx_m	sfx_damage
	jsr	apu_play
	lda	#0
	sta	battle_dmg+1
	lda	rpg_hp
	sec
	sbc	battle_dmg
	bcs	:+ ; if (battle_dmg > rpg_hp) {
		lda	#0
	: ; }
	sta	rpg_hp

	lda	#8
	sta	general_byte_0
	lda	ppu_nmi_scc_h
	sta	map_tile_xpos
	lda	ppu_nmi_scc_v
	sta	map_tile_ypos
	: ; for (general_byte_0 = 8; general_byte_0 > 0; general_byte_0--) {
		jsr	ppu_nmi_wait
		lda	rpg_hp
		beq	:+ ; if (rpg_hp) {
			jsr	ppu_nmi_wait
			jmp	:++
		: ; } else {
			lda	battle_enemy
			cmp	#enemy::dragonlord
			beq	:+ ; if (battle_enemy != dragonlord) {
				jsr	battle_screenflash
			; }
		: ; }

		lda	general_byte_0
		and	#1
		bne	:+ ; if (general_byte_0 % 1) {
			lda	map_tile_xpos
			clc
			adc	#2
			sta	ppu_nmi_scc_h
			jmp	:++
		: ; } else {
			lda	map_tile_ypos
			clc
			adc	#2
			sta	ppu_nmi_scc_v
		: ; }

		lda	battle_enemy
		cmp	#enemy::dragonlord
		bne	:+ ; if (battle_enemy == dragonlord) {
			lda	map_tile_xpos
			sta	ppu_nmi_scc_h
			lda	map_tile_ypos
			sta	ppu_nmi_scc_v
		: ; }

		jsr	ppu_nmi_wait
		jsr	battle_palette_normal
		lda	map_tile_xpos
		sta	ppu_nmi_scc_h
		lda	map_tile_ypos
		sta	ppu_nmi_scc_v

		dec	general_byte_0
		bne	:------
	; }

	window_low_m	((str_0FA_offset-tbl_string_offsets)/WORD_SIZE)
	window_function_m	((window_main_field-tbl_windows)/FUNC_WINDOW_DEF_SIZE)
	lda	rpg_hp
	beq	:+ ; if (rpg_hp) {
		jmp	battle_turn_player
	: ; }

	apusong_m	apu_song_lose
	jsr	apu_play
	jsr	apu_wait_music

	.export battle_dokill_player
battle_dokill_player:
	window_low_m	((str_001_offset-tbl_string_offsets)/WORD_SIZE)
	: ; while (!(joypad_read() & (start|face_a))) {
		jsr	joypad_read
		lda	joypad_state
		and	#joypad_button::start|joypad_button::face_a
		beq	:-
	;}

	lsr	rpg_gold+1
	ror	rpg_gold
	lda	game_player_flags
	and	#<~(player_flags::lora_carry)
	sta	game_player_flags
	lda	#0
	sta	battle_enemy
	jsr	game_set_start_room

	lda	rpg_flags+1
	and	#(state_flags::equipped_cursed_belt|state_flags::equipped_cursed_necklace)>>8
	beq	:+ ; if (rpg_flags & (equipped_cursed_belt|equipped_cursed_necklace)) {
		window_high_m	((str_114_offset-tbl_string_offsets)/WORD_SIZE)
		ldx	#maps::map_0C
		jmp	dialog_mapswitch_high
	: ;}

	window_high_m	((str_10D_offset-tbl_string_offsets)/WORD_SIZE)
	lda	rpg_level
	cmp	#<(LEVEL_MAX)
	beq	:+ ; if (rpg_level == LEVEL_MAX) {
		jsr	stats_calc_exp
		window_high_m	((str_112_offset-tbl_string_offsets)/WORD_SIZE)
	: ; }

	window_high_m	((str_113_offset-tbl_string_offsets)/WORD_SIZE)
	jsr	dialog_wait_button
	window_function_clear_m	((window_dialog-tbl_windows)/FUNC_WINDOW_DEF_SIZE)
	lda	#NPCS_RUN
	sta	sprite_npc_stop
	rts

	.export battle_screenflash
battle_screenflash:
	jsr	ppu_nmi_wait
	movw_m	ppu_pal_battle_flash, ppu_pal_load_src
	lda	#PPU_COLOR_LEV0
	sta	ppu_pal_load_cutoff
	jmp	ppu_pal_load_bg

	.export battle_palette_normal
battle_palette_normal:
	jsr	ppu_nmi_wait
	lda	battle_enemy
	cmp	#enemy::dragonlord
	bne	:+ ; if (battle_enemy == dragonlord) {
		movw_m  pal_dragonlord_01, ppu_pal_load_src
		jmp	:++
	: ; } else {
		lda	#<ppu_pal_overworld
		clc
		adc	map_type
		sta	ppu_pal_load_src
		lda	#>ppu_pal_overworld
		adc	#0
		sta	ppu_pal_load_src+1
	: ; }

	lda	#PPU_COLOR_LEV0
	sta	ppu_pal_load_cutoff
	jmp	ppu_pal_load_bg

battle_exit:
	jsr	oam_init
	jsr	dialog_wait_button

battle_exit_quick:
	jsr	ppu_nmi_wait
	movw_m	ppu_pal_obj_fade, ppu_pal_load_src
	lda	#PPU_COLOR_LEV0
	sta	ppu_pal_load_cutoff
	jsr	ppu_pal_load_obj

	lda	sprite_check_flag
	and	#$70
	beq	:+ ; if (sprite_check_flag & $70) {
		lda	#$FF
	: ; }
	sta	sprite_check_flag
	window_function_clear_m ((window_dialog-tbl_windows)/FUNC_WINDOW_DEF_SIZE)
	window_function_clear_m ((window_main_battle-tbl_windows)/FUNC_WINDOW_DEF_SIZE)
	jsr	ppu_nmi_wait
	jsr	sprites_draw
	rts

battle_run_roll:
	jsr	math_rand
	lda	battle_enemy
	cmp	#enemy::stoneman
	bcc	:+ ; if (battle_enemy >= stoneman) {
		lda	math_rand_word+1
		jmp	:++++
	: cmp	#enemy::dragon_green
	bcc	:+ ; } else if (battle_enemy >= dragon_green) {
		lda	math_rand_word+1
		and	#$7F
		jmp	:+++
	: cmp	#enemy::drollmagi
	bcc	:+ ; } else if (battle_enemy >= drollmagi) {
		lda	math_rand_word+1
		and	#$3F
		sta	general_byte_4
		jsr	math_rand
		lda	math_rand_word+1
		and	#$1F
		adc	general_byte_4
		jmp	:++
	: ; } else { 
	battle_next_turn:
		jsr	math_rand
		lda	math_rand_word+1
		and	#$3F
	: ; }

	sta	math_mulw_a
	lda	tbl_enemy_props+ENEMY_PTR_DEF
	sta	math_mulw_b
	lda	#0
	sta	math_mulw_a+1
	sta	math_mulw_b+1
	jsr	math_mulw
	lda	math_result
	sta	battle_fight_diff
	lda	math_result+1
	sta	battle_fight_diff+1
	jsr	math_rand
	lda	math_rand_word+1
	sta	math_mulw_a
	lda	rpg_agi
	sta	math_mulw_b
	lda	#0
	sta	math_mulw_a+1
	sta	math_mulw_b+1
	jsr	math_mulw
	lda	math_result
	sec
	sbc	battle_fight_diff
	lda	math_result+1
	sbc	battle_fight_diff+1
	rts

battle_palette_load_enemy:
	lda	battle_enemy
	sta	math_mulw_a
	lda	#$0C
	sta	math_mulw_b
	lda	#0
	sta	math_mulw_a+1
	sta	math_mulw_b+1
	jsr	math_mulw
	lda	math_result
	clc
	adc	#<(CNROM_BANK_INDEX_3_H|tbl_enemy_pal_addr)
	sta	cnrom_load_src
	lda	math_result+1
	adc	#>(CNROM_BANK_INDEX_3_H|tbl_enemy_pal_addr)
	sta	cnrom_load_src+1
	movw_m	cnrom_buffer2_5, cnrom_load_dest
	movw_m	ENEMY_PAL_CNROM_SIZE, cnrom_load_count
	jsr	cnrom_load
	lda	#>tbl_enemy_pal
	sta	general_word_1+1
	sta	ppu_pal_load_src+1
	lda	#<tbl_enemy_pal
	sta	general_word_1
	sta	ppu_pal_load_src
	rts

battle_palette_load:
	lda	#5
	sta	battle_palette_load_count
	: ; for (battle_palette_load_count = 5; battle_palette_load_count > 0; battle_palette_load_count--) {
		lda	battle_palette_addr
		sta	ppu_pal_load_src
		lda	battle_palette_addr+1
		sta	ppu_pal_load_src+1
		jsr	ppu_nmi_wait
		jsr	ppu_nmi_wait
		jsr	ppu_nmi_wait
		lda	#PPU_COLOR_LEV0
		sta	ppu_pal_load_cutoff
		jsr	ppu_pal_load_obj
		lda	battle_enemy
		cmp	#enemy::dragonlord
		bne	:+ ; if (battle_enemy == dragonlord) {
			movw_m	pal_dragonlord_00, ppu_pal_load_src
			jsr	ppu_pal_load_bg
		: ; }

		jsr	ppu_nmi_wait
		jsr	ppu_nmi_wait
		lda	battle_palette_addr
		pha
		lda	battle_palette_addr+1
		pha
		jsr	battle_palette_load_enemy
		pla
		sta	battle_palette_addr+1
		pla
		sta	battle_palette_addr
		lda	#PPU_COLOR_LEV0
		sta	ppu_pal_load_cutoff
		jsr	ppu_pal_load_obj
		lda	battle_enemy
		cmp	#enemy::dragonlord
		bne	:+ ; if (battle_enemy == dragonlord) {
			movw_m	pal_dragonlord_01, ppu_pal_load_src
			jsr	ppu_pal_load_bg
		: ; }

		dec	battle_palette_load_count
		bne	:---
	; }

	rts

pal_dragonlord_00:
	.byte	PPU_COLOR_LEV3|PPU_COLOR_GREY, PPU_COLOR_LEV0|PPU_COLOR_BLACK, PPU_COLOR_LEV3|PPU_COLOR_GREY, PPU_COLOR_LEV1|PPU_COLOR_RED
	.byte	PPU_COLOR_LEV1|PPU_COLOR_RED, PPU_COLOR_LEV1|PPU_COLOR_RED, PPU_COLOR_LEV1|PPU_COLOR_RED, PPU_COLOR_LEV1|PPU_COLOR_RED
	.byte	PPU_COLOR_LEV1|PPU_COLOR_RED, PPU_COLOR_LEV1|PPU_COLOR_RED, PPU_COLOR_LEV1|PPU_COLOR_RED, PPU_COLOR_LEV1|PPU_COLOR_RED

pal_dragonlord_01:
	.byte	PPU_COLOR_LEV3|PPU_COLOR_GREY, PPU_COLOR_LEV0|PPU_COLOR_BLACK, PPU_COLOR_LEV3|PPU_COLOR_GREY, PPU_COLOR_LEV1|PPU_COLOR_ORANGE
	.byte	PPU_COLOR_LEV1|PPU_COLOR_ROSE, PPU_COLOR_LEV3|PPU_COLOR_GREY, PPU_COLOR_LEV2|PPU_COLOR_AZURE, PPU_COLOR_LEV2|PPU_COLOR_BLUE
	.byte	PPU_COLOR_LEV2|PPU_COLOR_ORANGE, PPU_COLOR_LEV0|PPU_COLOR_PITCH, PPU_COLOR_LEV2|PPU_COLOR_ORANGE, PPU_COLOR_LEV2|PPU_COLOR_ORANGE

battle_tryrun_enemy:
	lda	rpg_str
	lsr	a
	cmp	tbl_enemy_props+ENEMY_PTR_ATT
	bcc	:+
	jsr	math_rand
	lda	math_rand_word+1
	and	#$03
	bne	:+ ; if (rpg_str < ENEMY_PTR_ATT || math_rand() & $03) {
		jsr	oam_init
		apusfx_m	sfx_run
		jsr	apu_play
		jsr	dialog_string_enemy
		window_low_m	((str_0E3_offset-tbl_string_offsets)/WORD_SIZE)
		ldx	map_eng_number
		lda	map_songs,x
		jsr	apu_play
		pla
		pla
		jmp	battle_exit
	: ; }

	rts

battle_strike_enemy:
	lsr	battle_fight_defense
	lda	battle_fight_attack
	sec
	sbc	battle_fight_defense
	bcc	:++
	cmp	#2
	bcs	:+++
	bcc	:++ ; if () {
	battle_strike_player:
		lsr	battle_fight_defense
		lda	battle_fight_attack
		lsr	a
		sta	math_mulw_b
		inc	math_mulw_b
		lda	battle_fight_attack
		sec
		sbc	battle_fight_defense
		bcc	:+
		cmp	math_mulw_b
		bcs	:+++
		: ; if () {
			jsr	math_rand
			lda	math_rand_word+1
			sta	math_mulw_a
			lda	#0
			sta	math_mulw_a+1
			sta	math_mulw_b+1
			jsr	math_mulw
			lda	math_result+1
			clc
			adc	#2
			sta	math_div_a
			lda	#3
			sta	math_div_b
			jmp	math_divb
		; }
	: ; } else if () {
		jsr	math_rand
		lda	math_rand_word+1
		and	#$01
		sta	battle_fight_hit
		rts
	: ; }

	sta	general_byte_0
	sta	math_mulw_b
	inc	math_mulw_b
	jsr	math_rand
	lda	math_rand_word+1
	sta	math_mulw_a
	lda	#0
	sta	math_mulw_a+1
	sta	math_mulw_b+1
	jsr	math_mulw
	lda	math_result+1
	clc
	adc	general_byte_0
	ror	a
	lsr	a
	sta	battle_fight_hit
	rts
	.end
