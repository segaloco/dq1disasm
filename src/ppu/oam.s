.include	"bssmap.i"
.include	"ppu_commands.i"
.include	"tunables.i"

.segment	"BSS"
.org		OAM_BUFFER

	.export oam_buffer
oam_buffer:
; ------------------------------------------------------------
.segment	"CODE"
.reloc

	.export oam_init
; oam_init - move all sprites offscreen
oam_init:
	jsr	ppu_nmi_wait

	ldx	#0
	lda	#MAP_BOTTOM
	: ; for (index = 0; index < MAP_BOTTOM; index++) {
		sta	oam_buffer,x
		inx
		bne	:-
	; }
	rts
