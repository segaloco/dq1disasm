; ppu_nmi - subroutines pertinent to the vertical interupt/per-frame operations

.include	"system/ppu.i"

.include	"bssmap.i"
.include	"ppu_commands.i"
.include	"ppu_bss.i"
.include	"rpg.i"
.include	"tunables.i"
.include	"macros.i"

.segment	"ZEROPAGE"

	.export zeroword
zeroword:		.word 0

	.export ppu_nmi_run, ppu_nmi_entry_count, ppu_nmi_entry_index
	.export ppu_nmi_scc_h, ppu_nmi_bg_high, ppu_nmi_scc_v
ppu_nmi_run:		.byte 0
ppu_nmi_entry_count:	.byte 0
ppu_nmi_entry_index:	.byte 0
ppu_nmi_scc_h:		.byte 0
ppu_nmi_bg_high:	.byte 0
ppu_nmi_scc_v:		.byte 0
; ------------------------------------------------------------
.segment	"BSS"
.org		PPU_NMI_BUFFER

	.export ppu_nmi_buffer
ppu_nmi_buffer:
; ------------------------------------------------------------
.segment	"CODE"
.reloc

	.export ppu_nmi_enter
; ppu_nmi_enter - non-maskable entrypoint for the vertical interrupt
; the stack frame is pushed and a flag checked regarding
; whether to run the per-frame operations
; if so, first, any awaiting display entries are moved to the PPU
; then additional per-frame operations are performed
; finally, whether run or not, the per-frame operations flag is unset,
; frame count initialized, and stack frame and control
; returned to main execution
ppu_nmi_enter:
	pha
	tya
	pha
	txa
	pha

	lda	ppu_nmi_run
	beq	:+
	cmp	#PPU_NMI_SKIP
	beq	@skip
	: ; if (ppu_nmi_run && ppu_nmi_run != PPU_NMI_SKIP) {
		lda	ppu_nmi_entry_count
		cmp	#PPU_NMI_HIGH_COUNT
		bcs	:+
		; if (ppu_nmi_entry_count <= PPU_NMI_HIGH_COUNT) {
			lda	#.hibyte(oam_buffer)
			sta	OBJ_DMA
		: ; }

		ldx	#0
		: ; for (index = 0; index < ppu_nmi_entry_index; index++) {
			cpx	ppu_nmi_entry_index
			beq	:+
			lda	ppu_nmi_buffer,x
			sta	PPU_VRAM_AR
			inx
			lda	ppu_nmi_buffer,x
			sta	PPU_VRAM_AR
			inx
			lda	ppu_nmi_buffer,x
			sta	PPU_VRAM_IO
			inx
			jmp	:-
		: ; }

		lda	#0
		sta	ppu_nmi_entry_count
		sta	ppu_nmi_entry_index
		jsr	ppu_nmi_refresh
@skip: ; }

	lda	#0
	sta	ppu_nmi_run
	inc	ppu_frame_count

	pla
	tax
	pla
	tay
	pla
	rti
; ------------------------------------------------------------
	.export ppu_nmi_refresh
; ppu_nmi_refresh - performs a per-frame data refresh
; after copying of any requested data
; first, the background color is initialized to black
; if the player has selected their name
; (i.e. the game is past the initial screens)
; if requested, the scroll plane ID and coordinates
; are updateds
ppu_nmi_refresh:
	lda	rpg_name
	beq	:+
	; if (rpg_name) {
		jsr	ppu_nmi_bgcol
		lda	#PPU_COLOR_LEV0|PPU_COLOR_PITCH
		sta	PPU_VRAM_IO
	: ; }

	jsr	ppu_nmi_bgcol
	sta	PPU_VRAM_AR
	sta	PPU_VRAM_AR

	lda	ppu_nmi_bg_high
	bne	:+
	; if (!ppu_nmi_bg_high) {
		lda	#ppu_ctlr0::int|ppu_ctlr0::bg_seg0|ppu_ctlr0::obj_seg1|ppu_ctlr0::bg1
		bne	:++
	: ; } else {
		lda	#ppu_ctlr0::int|ppu_ctlr0::bg_seg0|ppu_ctlr0::obj_seg1|ppu_ctlr0::bg2
	: ; }
	sta	PPU_CTLR0

	lda	ppu_nmi_scc_h
	sta	PPU_SCC_H_V
	lda	ppu_nmi_scc_v
	sta	PPU_SCC_H_V
	rts
; -----------------------------
ppu_nmi_bgcol:
	ppuaddr_m	PPU_COLOR_PAGE_BG
	rts
; ------------------------------------------------------------
	.export ppu_nmi_wait
; ppu_nmi_wait - this subroutine synchronizes
; main operation with the vertical interrupt, awaiting
; the next NMI operation
ppu_nmi_wait:
	lda	#PPU_NMI_RUN
	sta	ppu_nmi_run
	:
		lda	ppu_nmi_run
		bne	:-
	rts
