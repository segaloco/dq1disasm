.segment	"ZEROPAGE"

	.export scroll_pos_x, scroll_pos_y
scroll_pos_x:		.byte	0
scroll_pos_y:		.byte	0
	.export ppu_nt_xpos, ppu_nt_ypos
ppu_nt_xpos:		.byte	0
ppu_nt_ypos:		.byte	0
	.export ppu_blkdel_flags
ppu_blkdel_flags:	.byte	0
	.export ppu_blk_counter, ppu_npc_loop_iter, ppu_frame_count
ppu_blk_counter:	.byte	0
ppu_npc_loop_iter:	.byte	0
ppu_frame_count:	.byte	0
