.include	"ppu_nmi.i"
.include	"tunables.i"

.segment	"ZEROPAGE"

	.export ppu_nmi_write_data, ppu_nmi_apply_mirror, ppu_nmi_write_dest
ppu_nmi_write_data:	.byte 0
ppu_nmi_apply_mirror:	.byte 0
ppu_nmi_write_dest:	.addr 0
; ------------------------------------------------------------
.segment	"CODE"

	.export ppu_nmi_write
; ppu_nmi_write - add an entry for copy during vblank
; this routine simply loads up a byte on the vertical
; interrupt VRAM load buffer, waiting for it to drain
; a bit if full
ppu_nmi_write:
	: ; while (ppu_nmi_entry_index >= PPU_NMI_WRITE_MAX) {
		ldx	ppu_nmi_entry_index
		cpx	#PPU_NMI_WRITE_MAX
		beq	:-
	; }

	lda	ppu_nmi_write_dest+1
	sta	ppu_nmi_buffer,x
	inx
	lda	ppu_nmi_write_dest
	sta	ppu_nmi_buffer,x
	inx
	lda	ppu_nmi_write_data
	sta	ppu_nmi_buffer,x
	inx
	inc	ppu_nmi_entry_count
	stx	ppu_nmi_entry_index

	inc	ppu_nmi_write_dest
	bne	:+
	inc	ppu_nmi_write_dest+1
	:

	rts
