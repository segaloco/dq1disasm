.include	"math.i"
.include	"ppu_nmi_write.i"
.include	"general_bss.i"
.include	"ppu_bss.i"
.include	"map_calc.i"

	.export ppu_position_rel
ppu_position_rel:
	lda	ppu_nt_xpos
	asl	a
	clc
	adc	map_tile_xpos
	and	#$3F
	pha
	lda	ppu_nt_ypos
	asl	a
	clc
	adc	map_tile_ypos
	clc
	adc	#$1E
	sta	math_div_a
	lda	#$1E
	sta	math_div_b
	jsr	math_divb
	lda	math_result
	sta	nt_calc_row
	pla
	sta	nt_calc_column
	jsr	ppu_position_addr
	rts

	.export ppu_position
ppu_position_from_addr:
	jsr	nt_calc_attr_from_addr
	jmp	ppu_position
ppu_position_addr:
	jsr	nt_calc_attr_addr
ppu_position:
	tya
	pha
	ldy	#0
	lda	(ppu_nmi_write_dest),y
	sta	ppu_nmi_write_data
	pla
	tay
	lda	ppu_nmi_write_dest+1
	clc
	adc	#$20
	sta	ppu_nmi_write_dest+1
	jsr	ppu_nmi_write
	rts
