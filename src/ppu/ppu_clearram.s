.include	"bssmap.i"
.include	"general_bss.i"
.include	"map_calc.i"
.include	"ppu_pal.i"
.include	"tunables.i"
.include	"macros.i"

	.export ppu_unload, ppu_clearram
; ppu_unload - performs a standard palette fadeout
; then falls through to clearing game_buffer
ppu_unload:
	lda	#PPU_PAL_BG_TRUE
	sta	ppu_pal_fade_has_bg
	movw_m	ppu_pal_obj_fade, ppu_pal_fade_obj
	lda	#<(ppu_pal_overworld)
	clc
	adc	map_type
	sta	ppu_pal_fade_bg
	lda	#>(ppu_pal_overworld)
	adc	#0
	sta	ppu_pal_fade_bg+1
	jsr	ppu_pal_fadeout

; ppu_clearram - clears out game_buffer to the end
ppu_clearram:
	movw_m	game_buffer, general_word_0
	: ; for (general_word_0 = game_buffer; general_word_0 < LOWMEM_END; general_word_0 += $100) { */
		ldy	#0
		lda	#GAME_BUFFER_CLEAR
		: ; for (y = 0; y != 0; y++) {
			sta	(general_word_0),y
			iny
			bne	:-
		; }

		inc	general_word_0+1
		lda	general_word_0+1
		cmp	#>(LOWMEM_END)
		bne	:--
	; }
	rts
