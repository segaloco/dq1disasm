.include	"general_bss.i"
.include	"ppu_pal.i"
.include	"tunables.i"
.include	"macros.i"

	.export ppu_pal_fadeout
; ppu_pal_fadeout - fade out to the supplied
; OBJ palette, and BG palette if requested
ppu_pal_fadeout:
	lda	#PPU_PAL_FADEOUT_START
	sta	ppu_pal_fade_idx
	: ; for (i = PPU_PAL_FADEOUT_START; i < PPU_PAL_FADEOUT_END; i += PPU_PAL_FADE_INCR) {
		ldx	#PPU_PAL_FADEFRAMES
		: ; for (x = PPU_PAL_FADEFRAMES; x > 0; x--) {
			jsr	ppu_nmi_wait
			dex
			bne	:-
		; }

		movwz_m	ppu_pal_fade_obj, ppu_pal_load_src
		jsr	ppu_pal_load_obj

		lda	ppu_pal_fade_has_bg
		beq	:+
		; if (ppu_pal_fade_has_bg) {
			movwz_m	ppu_pal_fade_bg, ppu_pal_load_src
			jsr	ppu_pal_load_bg
		: ; }

		lda	ppu_pal_fade_idx
		clc
		adc	#PPU_PAL_FADE_INCR
		sta	ppu_pal_fade_idx
		cmp	#PPU_PAL_FADEOUT_END
		bne	:---
	; }
	rts
