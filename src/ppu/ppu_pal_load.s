.include	"system/ppu.i"

.include	"ppu_nmi.i"
.include	"ppu_nmi_write.i"
.include	"enemies.i"
.include	"rpg.i"
.include	"game_bss.i"
.include	"general_bss.i"
.include	"tunables.i"

.segment	"ZEROPAGE"

	.export ppu_pal_load_src
ppu_pal_load_src:	.addr	0
			.byte	0
; ------------------------------------------------------------
.segment	"CODE"

	.export ppu_pal_load_obj, ppu_pal_load_bg
; ppu_pal_load_obj - load a palette into the OBJ page,
; placing PPU_COLOR_NULL in the background color
ppu_pal_load_obj:
	: ; while (ppu_nmi_entry_index > PPU_PAL_OBJ_MAX) {
		lda	ppu_nmi_entry_index
		cmp	#PPU_PAL_OBJ_MAX
		bcs	:-
	; }

	lda	#<(PPU_COLOR_PAGE_OBJ)
	sta	ppu_nmi_write_dest
	bne	:++
; --------------
; ppu_pal_load_bg - load a palette into the BG page,
; placing PPU_COLOR_NULL in the background color
ppu_pal_load_bg:
	: ; while (ppu_nmi_entry_index > PPU_PAL_BG_MAX) {
		lda	ppu_nmi_entry_index
		cmp	#PPU_PAL_BG_MAX
		bcs	:-
	; }

	lda	#<(PPU_COLOR_PAGE_BG)
	sta	ppu_nmi_write_dest
; --------------
; load in each palette, placing COLOR_NULL
; in the background color slot
:
	lda	#>(PPU_VRAM_COLOR)
	sta	ppu_nmi_write_dest+1

	ldy	#0
	: ; for (y = 0; y < 12; y += 3) {
		lda	#PPU_COLOR_NULL
		sta	ppu_nmi_write_data
		jsr	ppu_nmi_write
		jsr	ppu_pal_load_byte
		jsr	ppu_pal_load_byte
		jsr	ppu_pal_load_byte

		cpy	#12
		bne	:-
	; }
    	rts
; -----------------------------
; load in a color generator entry, replacing it with
; certain values with red if player HP is below 25%
; a subtraction factor is applied, replacing any
; resulting negative value with the background color
ppu_pal_load_byte:
	lda	ppu_nmi_write_dest
	cmp	#1
	beq	:+

	cmp	#3
	bne	:++

	lda	battle_enemy
	cmp	#enemy::dragonlord
	bne	:++

	: ; if (ppu_nmi_write_dest == PPU_PALID_WBORDER || (ppu_nmi_write_dest == 0x3 && battle_enemy == dragonlord)) {
		lda	rpg_maxhp
		lsr	a
		lsr	a
		clc
		adc	#1
		cmp	rpg_hp
		bcc	:+

		lda	#PPU_COLOR_LEV2|PPU_COLOR_RED
		bne	:++
	: ; } else {
		lda	(ppu_pal_load_src),y
	: ; }

	sec
	sbc	ppu_pal_load_cutoff
	bcs	:+ ; if (color < ppu_pal_load_cutoff) {
		lda	#PPU_COLOR_NULL
	: ; }

	sta	ppu_nmi_write_data
	jsr	ppu_nmi_write
	iny
	rts
