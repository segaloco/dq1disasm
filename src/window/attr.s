.include	"ppu_nmi.i"
.include	"ppu_nmi_write.i"
.include	"ppu_pal.i"
.include	"general_bss.i"
.include	"ppu_bss.i"
.include	"game_bss.i"
.include	"math.i"
.include	"joypad.i"
.include	"window.i"
.include	"apu_commands.i"
.include	"macros.i"

	.export window_ppu_write_attra
window_ppu_write_attra:
	jsr	window_ppu_write_attr

window_ppu_write_attr:
	lda	ppu_nt_xpos
	asl	a
	sec
	sbc	#$0A
	and	#$3F
	sta	window_block_col
	sta	window_start_pos
	lda	ppu_nt_ypos
	asl	a
	clc
	adc	#$23
	sta	math_div_a
	lda	#$1E
	sta	math_div_b
	jsr	math_divb
	lda	math_result
	sta	window_block_row
	lda	#$86
	sta	window_block_addr
	lda	#$66
	sta	ppu_pal_load_src
	sta	map_npc_xpos
	lda	#6
	sta	window_block_addr+1
	sta	ppu_pal_load_src+1
	sta	map_npc_ypos
	lda	#$FF
	sta	ppu_nmi_apply_mirror

	lda	#4
	sta	ppu_blk_counter
	: ; for (ppu_blk_counter = 4; ppu_blk_counter > 0; ppu_blk_counter--) {
		jsr	ppu_nmi_wait

		lda	#2
		sta	ppu_npc_loop_iter
		: ; for (ppu_npc_loop_iter = 2; ppu_npc_loop_iter > 0; ppu_npc_loop_iter--) {
			jsr	window_puts
			lda	window_start_pos
			sta	window_block_col
			inc	window_block_row
			lda	window_block_row
			cmp	#$1E
			bne	:+ ; if (window_block_row == $1E) {
				lda	#0
				sta	window_block_row
			: ; }

			lda	ppu_blk_counter
			cmp	#1
			bne	:+
			lda	ppu_npc_loop_iter
			cmp	#2
			bne	:++ ; if (ppu_blk_counter != 1 && ppu_npc_loop_iter == 2) {
				movw_m	func_window_dialog, window_block_addr
				jmp	:++
			: ; } else if (ppu_blk_counter == 1 || ppu_npc_loop_iter == 2) {
				lda	ppu_pal_load_src
				clc
				adc	#$40
				sta	window_block_addr
				lda	ppu_pal_load_src+1
				adc	#0
				sta	window_block_addr+1
			: ; }

			lda	ppu_pal_load_src
			clc
			adc	#$20
			sta	ppu_pal_load_src
			sta	map_npc_xpos
			lda	ppu_pal_load_src+1
			adc	#0
			sta	ppu_pal_load_src+1
			sta	map_npc_ypos
			dec	ppu_npc_loop_iter
			bne	:----
		; }
		dec	ppu_blk_counter
		bne	:-----
	; }

	rts
