.include	"general_bss.i"
.include	"ppu_nmi.i"
.include	"ppu_nmi_write.i"
.include	"game_bss.i"
.include	"ppu_bss.i"
.include	"sprite.i"
.include	"npc.i"
.include	"math.i"
.include	"joypad.i"
.include	"rpg.i"
.include	"map_calc.i"
.include	"window.i"
.include	"apu_commands.i"
.include	"tunables.i"

window_chooser_blank:
	lda     #<((tile_blank_chr00-chr00_base)/PPU_TILE_SIZE)
	sta     ppu_nmi_write_data
	jsr     window_col_pos
	jmp     ppu_nmi_write

window_chooser_show:
	lda     #<((tile_rightarrow_chr00-chr00_base)/PPU_TILE_SIZE)
	sta     ppu_nmi_write_data
	jsr     window_col_pos
	jmp     ppu_nmi_write

	.export window_chooser
window_chooser:
	lda     #$FF
	sta     ppu_frame_count

	@loop: ; for (;;) {
		jsr     ppu_nmi_wait
		jsr     window_chooser_blank
		lda     joypad_state
		pha
		jsr     joypad_read
		pla
		beq     :+
		lda     ppu_frame_count
		and     #ANI_FRAME_COUNT_MASK
		cmp     #ANI_WIN_FRAME_ID
		beq     :+ ; if ((ppu_frame_count & ANI_FRAME_COUNT_MASK) == ANI_WIN_FRAME_ID) {
			jmp     @cont
		: ; }

		lda     joypad_state
		and     #joypad_button::face_a
		beq     :++ ; if (joypad_state & face_a) {
			jsr     window_chooser_show
			lda     window_column
			cmp     #1
			beq     :+ ; if (window_column != 1) {
				lda     #0
				sta     window_choice
			: ; }

			lda     window_row
			clc
			adc     window_choice
			sta     window_choice
			apusfx_m	sfx_menu
			jsr     apu_play
			lda     window_choice
			rts
		: ; }

		lda     joypad_state
		and     #joypad_button::face_b
		beq     :+ ; if (joypad_state & face_b) {
			jsr     window_chooser_show
			apusfx_m	sfx_menu
			jsr     apu_play
			lda     #$FF
			sta     window_choice
			rts
		: ; }

		lda     joypad_state
		and     #joypad_button::up
		beq     :++++++ ; if (joypad_state & up) {
			lda     window_column
			cmp     #5
			beq     :+++ ; if (window_column != 5) {
				lda     window_row
				bne     :+ ; if (!window_row) {
					jmp     @cont
				: ; }

				dec     window_row
				dec     window_block_count
				dec     window_block_count
				lda     window_block_count
				cmp     #$FE
				beq     :+ ; if (window_block_count == $FE) {
					jmp     @start
				: ; }

				lda     #$1C
				sta     window_block_count
				jmp     @start
			: ; }

			lda     window_row
			bne     :+ ; if (!window_row) {
				jmp     @cont
			: ; }

			lda     #0
			sta     window_row
			lda     window_start_pos
			sta     window_block_col
			lda     window_block_width
			sec
			sbc     #2
			cmp     #$FE
			bne     :+ ; if (window_block_width - 2 < 0) {
				lda     #$1C
			: ; }
			sta     window_block_count
			jmp     @start
		: ; }

		lda     joypad_state
		and     #joypad_button::down
		beq     :++++++ ; if (joypad_state & down) {
			lda     window_column
			cmp     #5
			beq     :+++ ; if (window_column != 5) {
				inc     window_row
				lda	window_row
				cmp	window_choice
				bne     :+ ; if (window_row == window_choice) {
					dec     window_row
					jmp     @cont
				: ; }

				inc     window_block_count
				inc     window_block_count
				lda     window_block_count
				cmp     #$1E
				beq     :+ ; if (window_block_count != $1E) {
					jmp     @start
				: ; }

				lda     #0
				sta     window_block_count
				jmp     @start
			: ; }

			lda     #2
			cmp     window_row
			bne     :+ ; if (window_row != 2) {
				jmp     @cont
			: ; }

			sta     window_row
			lda     window_start_pos
			sta     window_block_col
			lda     window_block_width
			clc
			adc     #2
			cmp     #$1E
			bne     :+ ; if (window_block_width + 2 == $1E) {
				lda     #0
			: ; }

			sta     window_block_count
			jmp     @start

		: ; }

		lda     joypad_state
		and     #joypad_button::left
		beq     :++ ; if (joypad_state & left) {
			lda     window_column
			cmp     #5
			beq     :+ ; if (window_column != 5) {
				lda     window_column
				cmp     #1
				bne     @cont
				dec     window_column
				lda     window_block_col
				sec
				sbc     #6
				and     #$3F
				sta     window_block_col
				jmp     @start
			: ; }

			lda     #3
			cmp     window_row
			beq     @cont
			sta     window_row
			lda     window_block_width
			sta     window_block_count
			lda     window_start_pos
			sec
			sbc     #2
			and     #$3F
			sta     window_block_col
			jmp     @start
		: ; }

		lda     joypad_state
		and     #joypad_button::right
		beq     @cont ; if (joypad_state & right) {
			lda     window_column
			cmp     #5
			beq     :+ ; if (window_column != 5) {
				lda     window_column
				bne     @cont
				inc     window_column
				lda     window_block_col
				clc
				adc     #6
				and     #$3F
				sta     window_block_col
				jmp     @start
			: ; }

			lda     #1
			cmp     window_row
			beq     @cont
			sta     window_row
			lda     window_block_width
			sta     window_block_count
			lda     window_start_pos
			clc
			adc     #2
			and     #$3F
			sta     window_block_col
		; }

	@start:
		lda     #0
		sta     ppu_frame_count

	@cont:
		lda     ppu_frame_count
		and     #$10
		bne     :+ ; if ((ppu_frame_count & $10)) {
			jsr     window_chooser_show
		: ; }

		jmp     @loop
	; }
