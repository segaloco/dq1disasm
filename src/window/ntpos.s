.include	"ppu_nmi.i"
.include	"ppu_nmi_write.i"
.include	"ppu_pal.i"
.include	"general_bss.i"
.include	"ppu_bss.i"
.include	"game_bss.i"
.include	"math.i"
.include	"joypad.i"
.include	"window.i"
.include	"apu_commands.i"

	.export	window_ntpos
window_ntpos:
	lda	ppu_nt_ypos
	asl	a
	sta	math_div_a
	lda	window_block_count
	clc
	adc	#$2C
	sec
	sbc	math_div_a
	sta	math_div_a
	lda	#$1E
	sta	math_div_b
	jsr	math_divb
	lda	math_result
	sta	nt_calc_row
	pha
	lda	ppu_nt_xpos
	asl	a
	sta	nt_calc_column
	lda	window_char_x
	clc
	adc	#$10
	sec
	sbc	nt_calc_column
	sta	nt_calc_column
	jsr	nt_calc_ram_addr
	lda	ppu_nmi_write_dest
	sta	ppu_pal_load_src
	lda	ppu_nmi_write_dest+1
	sta	ppu_pal_load_src+1
	pla
	sta	nt_calc_row
	lda	ppu_nt_xpos
	asl	a
	sta	nt_calc_column
	lda	window_text_x
	clc
	adc	#$10
	sec
	sbc	nt_calc_column
	sta	nt_calc_column
	jsr	nt_calc_ram_addr
	lda	ppu_nmi_write_dest
	sta	map_npc_xpos
	lda	ppu_nmi_write_dest+1
	sta	map_npc_ypos
	rts
