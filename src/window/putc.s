.include	"ppu_nmi.i"
.include	"ppu_nmi_write.i"
.include	"ppu_pal.i"
.include	"general_bss.i"
.include	"ppu_bss.i"
.include	"game_bss.i"
.include	"joypad.i"
.include	"window.i"
.include	"apu_commands.i"
.include	"tunables.i"
.include	"charmap.i"
.include	"rpg.i"
.include	"dialogs.i"
.include	"macros.i"

	.export window_puts_wait, window_puts, window_putc
window_puts_wait:
	jsr	ppu_nmi_wait

window_puts:
	jsr	window_putc
	inc	window_block_addr
	bne	window_puts
	inc	window_block_addr+1
	jmp	window_puts

window_putc:
	lda	window_string_len
	beq	:+ ; if (window_string_len != 0) {
		dec	window_string_len
		lda	window_block_addr
		dec	window_block_addr
		tay
		bne	:+
		dec	window_block_addr+1
	: ; }

	ldy	#0
	lda	(window_block_addr),y
	cmp	#CHAR_CTRL_RLE
	bne	:++ ; if (rle) {
		iny
		lda	(window_block_addr),y
		sta	window_string_len
		lda	window_block_addr
		clc
		adc	#3
		sta	window_block_addr
		bcc	:+
			inc	window_block_addr+1
		:

		jmp	window_putc
	: ; }
	cmp	#CHAR_CTRL_END
	beq	:++
	cmp	#CHAR_CTRL_WINDOW_NEWLINE
	bne	:+++ ; else if (window_newline || end) {
	@window_putc_recur:
		; if (newline) {
			lda	ppu_pal_load_src
			clc
			adc	#$40
			sta	ppu_pal_load_src
			sta	map_npc_xpos
			lda	ppu_pal_load_src+1
			adc	#0
			sta	ppu_pal_load_src+1
			sta	map_npc_ypos
			lda	window_start_pos
			sta	window_block_col
			inc	window_block_row
			inc	window_block_row
			lda	window_block_row
			cmp	#$1E
			bne	:+ ; if (window_block_row == 0x1E) {
				lda	#0
				sta	window_block_row
				beq	:++
			: ; }
			cmp	#$1F
			bne	:+ ; else if (window_block_row == 0x1F) {
				lda	#1
				sta	window_block_row
			: ; }
		; }
		pla
		pla
		rts
	: ; }
	cmp	#CHAR_CTRL_CONTENT_NEWLINE
	bne	:+ ; else if (content_newline) {
		jsr	@window_putc_recur
	: ; }
	cmp	#CHAR_CTRL_SCROLL
	bne	:+++ ; else if (scroll) {
		lda	msg_speed
		asl	a
		asl	a
		asl	a
		sta	general_byte_2
		asl	a
		adc	general_byte_2
		adc	#3
		tax
		: ; for (x = (24*msg_speed)+3; x > 0; x--) {
			; while (joypad_start) {
				lda	#1
				sta	ppu_nmi_run
				: ; for (ppu_nmi_run = 1; ppu_nmi_run != 0;) {
					lda	ppu_nmi_run
					bne	:-
				; }

				jsr	joypad_read
				lda	joypad_state
				and	#joypad_button::start
				bne	:--
			; }

			dex
			bne	:--
		; }

		rts
	: ; }
	cmp	#CHAR_CTRL_PRINTBUF
	bne	:+ ; else if (printbuf) {
		movwz_m	window_block_addr, window_map_buffer
		movw_m	dialog_buffer, window_block_addr
		jmp	window_putc_cont
	: ; }
	cmp	#CHAR_CTRL_STREND
	bne	:+ ; else if (strend) {
		movwz_m	window_map_buffer, window_block_addr
		rts
	: ; }
	cmp	#CHAR_CTRL_PRINT3
	bne	:+ ; else if (print3) {
		iny
		lda	(window_block_addr),y
		sta	window_itemname_ptr
		iny
		lda	(window_block_addr),y
		sta	window_itemname_ptr+1
		tya
		pha
		ldy	#0
		sty	window_itemname+1
		lda	(window_itemname_ptr),y
		sta	window_itemname
		pla
		tay
		jsr	window_print_itemname
		lda	window_block_addr
		clc
		adc	#2
		sta	window_map_buffer
		lda	window_block_addr+1
		adc	#0
		sta	window_map_buffer+1
		lda	#>(dialog_buffer+$C+2)
		sta	window_block_addr+1
		lda	#<(dialog_buffer+$C+2)
		sta	window_block_addr
		jmp	window_putc_cont
	: ; }
	cmp	#CHAR_CTRL_PRINT5
	bne	:+ ; else if (print5) {
		jsr	@window_putc_recur2
		jmp	window_putc_cont
	@window_putc_recur2:
		iny
		lda	(window_block_addr),y
		sta	window_itemname_ptr
		iny
		lda	(window_block_addr),y
		sta	window_itemname_ptr+1
		tya
		pha
		ldy	#0
		lda	(window_itemname_ptr),y
		sta	window_itemname
		iny
		lda	(window_itemname_ptr),y
		sta	window_itemname+1
		pla
		tay
		jsr	window_print_itemname
		lda	window_block_addr
		clc
		adc	#2
		sta	window_map_buffer
		lda	window_block_addr+1
		adc	#0
		sta	window_map_buffer+1
		lda	#>(dialog_buffer+$C)
		sta	window_block_addr+1
		lda	#<(dialog_buffer+$C)
		sta	window_block_addr
		rts
	: ; }
	cmp	#CHAR_CTRL_SKIPFIELD
	bne	:+++ ; else if (skipfield) {
		jsr	@window_putc_recur2

		ldy	#0
		: ; for (y = 0; window_block_addr[y] == <((tile_blank_chr00-chr00_base)/PPU_TILE_SIZE); y++) {
			lda	(window_block_addr),y
			cmp	#<((tile_blank_chr00-chr00_base)/PPU_TILE_SIZE)
			bne	:+
			inc	window_block_addr
			jmp	:-
		: ; }

		jmp	window_putc_cont
	: ; }
	cmp	#CHAR_CTRL_PRINTNAME
	bne	:+ ; else if (printname) {
		movwz_m	window_block_addr, window_map_buffer
		lda	#>rpg_name
		sta	window_block_addr+1
		lda	#<rpg_name
		sta	window_block_addr
		jmp	window_putc_cont
	: ; }
	cmp	#CHAR_STR_BASE
	bcc	:++++++ ; else if (window_block_char >= CHAR_STR_BASE) {
		sbc	#CHAR_STR_BASE
		tax
		inx
		movw_m	tbl_str_common_start, window_dialog_str_tbl
		: ; for (x = index; x > 0; x--) {
			ldy	#0
			: ; for (y = 0; window_dialog_str_tbl[y] != CHAR_CTRL_STREND; y++) {
				lda	(window_dialog_str_tbl),y
				cmp	#CHAR_CTRL_STREND
				beq	:+
				iny
				jmp	:-
			: ; }

			dex
			beq	:++
			; if (--x == 0) break;

			tya
			sec
			adc	window_dialog_str_tbl
			sta	window_dialog_str_tbl
			bcc	:+
				inc	window_dialog_str_tbl+1
			:

			jmp	:----
		: ; }

		movwz_m	window_block_addr, window_map_buffer
		movwz_m	window_dialog_str_tbl, window_block_addr
		jmp	window_putc_cont
	: ; }
	cmp	#CHAR_ARROW_DOWN
	beq	:+ ; else if (window_block_char != CHAR_ARROW_DOWN) {
		jmp	window_putc_cont
	: ; } else {
		lda	window_char_x
		clc
		adc	#9
		and	#$3F
		sta	window_block_col
		lda	#0
		sta	ppu_frame_count

		: ; do {
			jsr	joypad_read
			lda	joypad_state
			and	#(joypad_button::face_a|joypad_button::face_b)
			beq	:++
			: ; if (joypad_state & (face_a|face_b) || ppu_frame_count & 0x10) {
				lda	#<((tile_blank_chr00-chr00_base)/PPU_TILE_SIZE)
				bne	:++
			: ; }
			lda	ppu_frame_count
			and	#$10
			bne	:-- ; else {
				lda	#CHAR_ARROW_DOWN
			: ; }
			sta	ppu_nmi_write_data

			jsr	ppu_nmi_wait
			jsr	window_col_pos
			jsr	ppu_nmi_write

			lda	joypad_state
			and	#(joypad_button::face_a|joypad_button::face_b)
			beq	:----
		; } while(!(joypad_state & (joypad_button::face_a|joypad_button::face_b)));

		apusfx_m	sfx_menu
		jsr	apu_play
		lda	window_char_x
		sta	window_block_col
		jsr	window_ntpos
		rts
	; }

window_putc_cont:
	ldy	#0
	lda	(window_block_addr),y
	sta	ppu_nmi_write_data
	lda	ppu_nmi_apply_mirror
	beq	:+
	cmp	#1
	beq	:+ ; if (ppu_nmi_apply_mirror > 1) {
		lda	ppu_nmi_write_data
		sta	(map_npc_xpos),y
	: ; }

	jsr	window_col_pos
	jsr	ppu_nmi_write
	ldy	#1
	lda	(window_block_addr),y

	cmp	#$F8
	beq	:+
	cmp	#$F9
	bne	window_putc_done ; if (window_block_addr == $F9) {
		lda	#<((tile_handakuten_lo_chr00-chr00_base)/PPU_TILE_SIZE)
		sta	ppu_nmi_write_data
	bne	:++ ; } else if (window_block_addr == $F8) {
	:
		lda	#<((tile_dakuten_lo_chr00-chr00_base)/PPU_TILE_SIZE)
		sta	ppu_nmi_write_data
	: ; } if (window_block_addr == $F8 || window_block_addr == $F9) {
		inc	window_block_addr
		bne	:+
			inc	window_block_addr+1
		:

		lda	map_npc_xpos
		clc
		adc	#$E0
		sta	map_npc_xpos
		bcs	:+
			dec	map_npc_ypos
		:

		lda	ppu_nmi_apply_mirror
		beq	:+
		cmp	#1
		beq	:+ ; if (ppu_nmi_apply_mirror > 1) {
			lda	ppu_nmi_write_data
			ldy	#0
			sta	(map_npc_xpos),y
		: ; }

		lda	map_npc_xpos
		clc
		adc	#$20
		sta	map_npc_xpos
		bcc	:+
			inc	map_npc_ypos
		:

		dec	window_block_row
		lda	window_block_row
		cmp	#$FF
		bne	:+ ; if (--window_block_row == 0xFF) {
			lda	#$1E-1
			sta	window_block_row
		: ; }

		jsr	window_col_pos
		jsr	ppu_nmi_write

		inc	window_block_row
		lda	window_block_row
		cmp	#$1E
		bne	:+ ; if (++window_block_row == $1E) {
			lda	#0
			sta	window_block_row
		: ; }
window_putc_done: ; }

	inc	map_npc_xpos
	inc	window_block_col
	lda	window_block_col
	and	#$3F
	sta	window_block_col
	rts

	.export window_col_pos
window_col_pos:
	lda	ppu_nmi_write_data
	pha

	lda	ppu_nmi_apply_mirror
	cmp	#1
	beq	:+
	lda	window_block_row
	lsr	a
	bcs	:+
	lda	window_block_col
	lsr	a
	bcs	:+ ; if (ppu_nmi_apply_mirror != 1 && !(window_block_row % 2) && !(window_block_col % 2)) {
		lda	window_block_col
		sta	nt_calc_column
		lda	window_block_row
		sta	nt_calc_row
		lda	#0
		sta	ppu_nmi_write_data
		jsr	ppu_write_attr_addr
		jsr	ppu_position
	: ; }

	pla
	sta	ppu_nmi_write_data
	lda	window_block_col
	sta	nt_calc_column
	lda	window_block_row
	sta	nt_calc_row
	jsr	nt_calc_dest
	rts

window_print_itemname:
	ldx	#0
	lda	#<((tile_blank_chr00-chr00_base)/PPU_TILE_SIZE)
	: ; for (x = 0; x < 5; x++) {
		sta	dialog_buffer+$C,x
		inx
		cpx	#5
		bne	:-
	; }

	lda	#CHAR_CTRL_STREND
	sta	dialog_buffer+$C,x
	dex
	: ; for (dialog_buffer+$C[x] = 0xFA; window_itemname != 0; --x) {
		movw_m	$A, math_div_b
		jsr	math_divw
		lda	math_result
		sta	dialog_buffer+$C,x
		dex
		lda	window_itemname
		ora	window_itemname+1
		bne	:-
	; }

	rts
