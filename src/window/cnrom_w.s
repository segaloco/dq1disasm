.include	"system/cnrom.i"

.include	"bssmap.i"
.include	"enemies.i"
.include	"game_bss.i"

.segment	"BSS"
.org		CNROM_BUFFER

	.export cnrom_buffer, cnrom_buffer2, cnrom_buffer2_5, cnrom_buffer3
cnrom_buffer:

.org		CNROM_BUFFER2
cnrom_buffer2:

.org		CNROM_BUFFER2_5
cnrom_buffer2_5:

.org		CNROM_BUFFER3
cnrom_buffer3:
; ------------------------------------------------------------
.segment	"CODE"
.reloc

	.export cnrom_bank_reset
cnrom_bank_reset:
	lda	battle_enemy
	cmp	#enemy::dragonlord
	bne	:+ ; if (battle_enemy == dragonlord) {
		jsr	cnrom_bank_sec_02
		bne	:++
	: ; } else {
		jsr	cnrom_bank_sec_00
	: ; }

	jmp         ppu_nmi_refresh
; ------------------------------------------------------------
	.export cnrom_bank_sec_00, cnrom_bank_sec_01
	.export cnrom_bank_sec_02, cnrom_bank_sec_03
cnrom_bank_sec_00:
	lda	#CNROM_SECURITY_BITS|0
	sta	cnrom_bank_sec_00+1
	rts
cnrom_bank_sec_01:
	lda	#CNROM_SECURITY_BITS|1
	sta	cnrom_bank_sec_01+1
	rts
cnrom_bank_sec_02:
	lda	#CNROM_SECURITY_BITS|2
	sta	cnrom_bank_sec_02+1
	rts
cnrom_bank_sec_03:
	lda	#CNROM_SECURITY_BITS|3
	sta	cnrom_bank_sec_03+1
	rts
