.include	"system/cpu.i"
.include	"system/ppu.i"
.include	"system/cnrom.i"

.include	"bssmap.i"
.include	"ppu_nmi.i"
.include	"ppu_nmi_write.i"
.include	"ppu_pal.i"
.include	"general_bss.i"
.include	"game_bss.i"
.include	"sprite.i"
.include	"rpg.i"
.include	"window.i"
.include	"apu_commands.i"
.include	"charmap.i"
.include	"macros.i"

WINDOW_CNROM_DATA_OFFSET	= $50

	.export window_string_low_a, window_string_high_s, window_string_low_s
	.export window_show_common, window_show_edge, window_show_loaded
window_string_low_a:
	sta	window_str_id
	lda	#0
	sta	window_str_id+1
	beq	window_show_common

window_string_high_s:
	lda	#>WINDOW_H_OFFSET
	sta	window_str_id+1
	bne	window_show_edge

window_string_low_s:
	lda	#0
	sta	window_str_id+1

window_show_edge:
	pla
	clc
	adc	#1
	sta	window_str_id_ptr
	pla
	adc	#0
	sta	window_str_id_ptr+1
	pha
	lda	window_str_id_ptr
	pha
	ldy	#0
	lda	(window_str_id_ptr),y
	sta	window_str_id

window_show_common:
	lda	#0
	sta	tile_current
	asl	window_str_id
	rol	window_str_id+1
	lda	#<(CNROM_BANK_INDEX_3_H|tbl_string_offsets)
	clc
	adc	window_str_id
	sta	cnrom_load_src
	lda	#>(CNROM_BANK_INDEX_3_H|tbl_string_offsets)
	adc	window_str_id+1
	sta	cnrom_load_src+1
	movw_m	game_win_ppuaddr, cnrom_load_dest
	movw_m	WORD_SIZE, cnrom_load_count
	jsr	cnrom_load
	movw_m	CNROM_BUFFER2+WINDOW_CNROM_DATA_OFFSET, window_block_addr

window_show_loaded:
	lda	window_text_x
	sta	window_block_col
	lda	window_text_y
	sta	window_block_row
	lda	window_char_x
	sta	window_start_pos
	lda	#FUNC_WINDOW_MIRROR
	sta	ppu_nmi_apply_mirror
	jsr	window_ntpos
	jsr	window_show_loop
	jsr	window_show_next
	lda	window_block_col
	sta	window_text_x
	lda	window_block_row
	sta	window_text_y
	rts

	window_show_loop: ; for (;;) {
		jsr	ppu_nmi_wait
		lda	#PPU_NMI_SKIP
		sta	ppu_nmi_run
		sei

		: ; while (ppu_nmi_run) {
			lda	ppu_nmi_run
			bne	:-
		; }

		lda	window_block_addr+1
		cmp	#3
		bne	:++ ; if (window_block_addr+1 == 3) {
		@here:	lda	#CNROM_SECURITY_BITS|1
			sta	@here+1
			lda	window_block_addr
			sec
			sbc	#$B0
			clc
			adc	game_win_ppuaddr
			sta	game_win_ppuaddr
			lda	game_win_ppuaddr+1
			adc	#0
			sta	game_win_ppuaddr+1
			sta	PPU_VRAM_AR
			lda	game_win_ppuaddr
			sta	PPU_VRAM_AR
			lda	PPU_VRAM_IO
			ldx	#0

			: ; for () {
				lda	PPU_VRAM_IO
				sta	CNROM_BUFFER2+WINDOW_CNROM_DATA_OFFSET,x
				inx
				cpx	#8
				bne	:-
			; }
			jsr	cnrom_bank_reset
			lda	#$B0
			sta	window_block_addr
		: ; }

		cli
		ldx	msg_speed
		beq	:++ ; if (msg_speed) {
			: ; for (x = msg_speed; x > 0; x--) {
				jsr	ppu_nmi_wait
				dex
				bne	:-
			; }
		: ; }

		ldy	#0
		lda	(window_block_addr),y
		cmp	#CHAR_STR_BASE
		beq	:+
		cmp	#CHAR_STR_QUOTE_LORA
		bne	:++
		: ; if (window_block_char.in(CHAR_STR_BASE, CHAR_STR_QUOTE_LORA)) {
			lda	#CHAR_CTRL_END
			sta	tile_current
		: ; }

		jsr	window_putc
		lda	tile_current
		beq	:++
		ldy	#0
		lda	(window_block_addr),y
		cmp	#CHAR_FONT_STD_END
		bcc	:+
		cmp	#CHAR_FONT_SUP_START
		bcc	:++
		cmp	#CHAR_FONT_SUP_END
		bcs	:++ 
		: ; if (window_block_char < CHAR_FONT_STD_END || window_block_char.between(CHAR_FONT_SUP_START, CHAR_FONT_SUP_END)) {
			apusfx_m	sfx_text
			jsr	apu_play
		: ; }

		ldy	#0
		lda	(window_block_addr),y
		cmp	#CHAR_STR_BASE
		bcc	:+
		cmp	#CHAR_STR_TOWN_01
		bcs	:+ ; if (window_block_char > CHAR_STR_BASE && window_block_char < CHAR_STR_TOWN01) {
			lda	window_block_col
			sta	window_start_pos
			lda	map_npc_xpos
			sta	ppu_pal_load_src
			lda	map_npc_ypos
			sta	ppu_pal_load_src+1
		: ; }

		jsr	window_show_next
		inc	window_block_addr
		bne	:+
			inc	window_block_addr+1
		:

		ldy	#0
		lda	(window_block_addr),y
		cmp	#CHAR_CTRL_WINDOW_NEWLINE
		beq	:+ ; if (window_block_char != CHAR_CTRL_WINDOW_NEWLINE) {
			jmp	window_show_loop
		: ; } else {
			lda	window_char_x
			sta	window_start_pos
			jmp	window_show_loop
		;}
	; }

	.export		window_show_next
window_show_next:
	lda	window_block_row
	cmp	window_char_y
	bne	:++++ ; if (window_block_row == window_char_y) {
		lda	window_block_col
		pha
		lda	window_block_row
		pha
		lda	window_block_addr
		pha
		lda	window_block_addr+1
		pha
		lda	map_npc_xpos
		pha
		lda	map_npc_ypos
		pha
		lda	ppu_pal_load_src
		pha
		lda	ppu_pal_load_src+1
		pha
		lda	window_start_pos
		pha
		jsr	window_ppu_write_attra
		pla
		sta	window_start_pos
		pla
		sta	ppu_pal_load_src+1
		pla
		clc
		adc	#$C0
		sta	ppu_pal_load_src
		bcs	:+
			dec	ppu_pal_load_src+1
		:

		pla
		sta	map_npc_ypos
		pla
		clc
		adc	#$C0
		sta	map_npc_xpos
		bcs	:+
			dec	map_npc_ypos
		:

		pla
		sta	window_block_addr+1
		pla
		sta	window_block_addr
		pla
		sta	window_block_row
		dec	window_block_row
		dec	window_block_row
		lda	window_block_row
		cmp	#$FE
		bne	:+ ; if (window_block_row == $FE) {
			lda	#$1C
			sta	window_block_row
		: ; }

		pla
		sta	window_block_col
	: ; }

	rts
	.end
