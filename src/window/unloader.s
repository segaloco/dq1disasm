.include	"general_bss.i"
.include	"ppu_nmi.i"
.include	"ppu_nmi_write.i"
.include	"ppu_bss.i"
.include	"sprite.i"
.include	"npc.i"
.include	"math.i"
.include	"rpg.i"
.include	"map_calc.i"
.include	"window.i"
.include	"apu_commands.i"
.include	"charmap.i"

	.export window_remove
window_remove:
	sta	math_mulw_a
	lda	#0
	sta	math_mulw_a+1
	sta	math_mulw_b+1
	lda	#7
	sta	math_mulw_b
	jsr	math_mulw
	ldy	math_result
	lda	tbl_windows+tbl_windows_XPOS,y
	sta	nt_calc_column
	sec
	sbc	#8
	asl	a
	sta	window_start_pos
	lda	tbl_windows+tbl_windows_HEIGHT,y
	lsr	a
	sta	window_block_row
	sec
	sbc	#1
	clc
	adc	tbl_windows+tbl_windows_YPOS,y
	sta	nt_calc_row
	sec
	sbc	#7
	asl	a
	sta	map_tile_ypos
	lda	tbl_windows+tbl_windows_WIDTH,y
	lsr	a
	sta	window_block_width
	jsr	nt_calc_ppu_addr
	lda	ppu_nmi_write_dest
	sta	window_block_addr
	lda	ppu_nmi_write_dest+1
	sta	window_block_addr+1
	lda	tbl_windows+tbl_windows_MIRROR,y
	sta	ppu_blk_counter

	: ; for (window_block_row = tbl_windows[y].item; window_block_row > 0; window_block_row--) {
		lda	window_start_pos
		sta	map_tile_xpos
		lda	window_block_width
		sta	window_block_col
		jsr	ppu_nmi_wait
		: ; for (window_block_col = byte_9E; window_block_col > 0; window_block_col--) {
			jsr	window_clearbuf
			lda	window_block_addr
			clc
			adc	#$02
			sta	window_block_addr
			bcc	:+
				inc	window_block_addr+1
			:

			inc	map_tile_xpos
			inc	map_tile_xpos
			dec	window_block_col
			bne	:--
		; }

		lda	window_block_addr
		clc
		adc	#$C0
		sta	window_block_addr
		bcs	:+
			dec	window_block_addr+1
		:

		lda	window_block_width
		asl	a
		sta	nt_calc_column
		lda	window_block_addr
		sec
		sbc	nt_calc_column
		sta	window_block_addr
		bcs	:+
			dec	window_block_addr+1
		:

		dec	map_tile_ypos
		dec	map_tile_ypos
		dec	window_block_row
		bne	:-----
	; }

	lda	sprite_npc_stop
	beq	:+ ; if (sprite_npc_stop) {
		jsr	ppu_nmi_wait
		jsr	sprites_draw
	: ; }

	rts

	.export	window_clearbuf
window_clearbuf:
	lda	ppu_blk_counter
	bne	:+
	ldy	#0
	lda	(window_block_addr),y
	cmp	#CHAR_CTRL_END
	beq	:+
	cmp	#CHAR_CTRL_CONTENT_NEWLINE
	beq	:+
	jmp	:++
	: ; if (ppu_blk_counter || end || newline) {
		lda	#0
		sta	ppu_blkdel_flags
		sta	rpg_ppulayout
		jsr	map_block_mod
		ldy	#0
		lda	#CHAR_CTRL_END
		sta	(window_block_addr),y
		iny
		sta	(window_block_addr),y
		ldy	#$20
		sta	(window_block_addr),y
		iny
		sta	(window_block_addr),y
		rts
	: ; }

	lda	ppu_nt_ypos
	asl	a
	adc	map_tile_ypos
	clc
	adc	#$1E
	sta	math_div_a
	lda	#$1E
	sta	math_div_b
	jsr	math_divb
	lda	math_result
	sta	nt_calc_row
	sta	scroll_pos_y
	lda	ppu_nt_xpos
	asl	a
	clc
	adc	map_tile_xpos
	and	#$3F
	sta	nt_calc_column
	sta	scroll_pos_x
	jsr	nt_calc_dest
	ldy	#0
	lda	(window_block_addr),y
	sta	ppu_nmi_write_data
	jsr	ppu_nmi_write
	iny
	lda	(window_block_addr),y
	sta	ppu_nmi_write_data
	jsr	ppu_nmi_write
	lda	ppu_nmi_write_dest
	clc
	adc	#$1E
	sta	ppu_nmi_write_dest
	bcc	:+
		inc	ppu_nmi_write_dest+1
	:

	ldy	#$20
	lda	(window_block_addr),y
	sta	ppu_nmi_write_data
	jsr	ppu_nmi_write
	iny
	lda	(window_block_addr),y
	sta	ppu_nmi_write_data
	jsr	ppu_nmi_write
	lda	scroll_pos_x
	sta	nt_calc_column
	lda	scroll_pos_y
	sta	nt_calc_row

	ldy	#0
	lda	(window_block_addr),y
	cmp	#$C1
	bcs	:+
	lda	#0
	beq	:++++
	: ; if (window_block_addr > $C1) {
		cmp	#$CA
		bcs	:+ ; if (window_block_addr < $CA) {
			lda	#1
			bne	:+++
		: ; } else if (window_block_addr < $DE) {
		cmp	#$DE
		bcs	:+ 
			lda	#2
			bne	:++
		: ; } else {
			lda	#3
		; }
	: ; }
	sta	ppu_nmi_write_data
	jsr	ppu_write_attr_addr
	lda	ppu_nmi_write_dest+1
	clc
	adc	#$20
	sta	ppu_nmi_write_dest+1
	jsr	ppu_nmi_write
	rts
