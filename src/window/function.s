.include	"general_bss.i"
.include	"ppu_nmi_write.i"
.include	"ppu_pal.i"
.include	"ppu_bss.i"
.include	"math.i"
.include	"map_calc.i"
.include	"window.i"
.include	"game_bss.i"
.include	"apu_commands.i"
.include	"charmap.i"
.include	"dialogs.i"

	.export window_function
window_function:
	pla
	clc
	adc	#1
	sta	window_str_id_ptr
	pla
	adc	#0
	sta	window_str_id_ptr+1
	pha
	lda	window_str_id_ptr
	pha

	ldy	#0
	lda	(window_str_id_ptr),y
	sta	window_str_id
	cmp	#3
	beq	:+
	cmp	#4
	bne	:++
	: ; if (window_str_id == 3 || window_str_id == 4) {
		apusfx_m	sfx_menu
		bne	:++
	: ; } else {
		cmp	#9
		bne	@noapu_play
		apusfx_m	sfx_confirm
	: ; }
	jsr	apu_play

@noapu_play:
	lda	window_str_id
	cmp	#2
	bcc	window_contents
	bne	:++ ; if (window_str_id == 2) {
		lda	#$FF
		sta	window_text_x
		sta	window_text_y
		jsr	window_contents
		inc	window_block_row
		lda	window_block_row
		cmp	#$1E
		bne	:+ ; if (window_block_row == $1E) {
			lda	#0
		: ; }

		sta	window_char_y
		rts
	: ; } else if (general_word > 2) {
		pha
		pha
		lda	window_text_y
		pha
		lda	#$FF
		sta	window_text_y
		lda	#$FE
		sta	window_choice
		jsr	window_contents
		lda	window_text_y
		sta	window_block_row
		pla
		sta	window_text_y
		inc	window_start_pos
		lda	window_start_pos
		sta	window_block_col
		pla
		cmp	#6
		bcs	:+++
		cmp	#5
		bne	:++ ; if (req == 5) {
			lda	window_block_col
			clc
			adc	#2
			and	#$3F
			sta	window_block_col
			sta	window_start_pos
			lda	window_block_row
			clc
			adc	#2
			cmp	#$1E
			bne	:+ ; if (window_block_row + 2 == $1E) {
				lda	#0
			: ; }

			sta	window_block_width
			lda	#5
			bne	:+++
		: ; } else if (req == 6) {
			lda	#0
			beq	:++
		: ; } else {
			lda	#$FF
		: ; }
		sta	window_column
		lda	#0
		sta	window_row
		jsr	window_chooser
		pla
		cmp	#<((window_talk_dir-tbl_windows)/FUNC_WINDOW_DEF_SIZE)
		bcc	:+ ; if (window >= talk_dir) {
			jsr	window_remove
		: ; }

		lda	window_choice
		rts
	; }

window_contents:
	lda	window_str_id
	cmp	#1
	beq	:+
	cmp	#6
	beq	:+
	cmp	#7
	beq	:+
	cmp	#8
	beq	:+ ; if (window_str_id != 1 && window_str_id != 6
			; && window_str_id != 7 && window_str_id != 8) {
		jsr	dialog_init_buffer
	: ; }

	lda	#0
	sta	window_str_id+1
	sta	math_mulw_b+1
	lda	#7
	sta	math_mulw_b
	jsr	math_mulw
	ldy	math_result
	lda	tbl_windows+tbl_windows_MIRROR,y
	sta	ppu_nmi_apply_mirror
	lda	tbl_windows+tbl_windows_TEXT,y
	sta	window_text
	lda	tbl_windows+tbl_windows_TEXT+1,y
	sta	window_text+1
	lda	tbl_windows+tbl_windows_HEIGHT,y
	lsr	a
	tax
	lda	#$FF
	sta	dialog_buffer,x
	lda	tbl_windows+tbl_windows_XPOS,y
	clc
	adc	ppu_nt_xpos
	sec
	sbc	#8
	and	#PPU_POS_MASK
	asl	a
	sta	window_block_col
	lda	window_text_x
	cmp	#$FF
	bne	:+ ; if (window_text_x == $FF) {
		lda	window_block_col
		clc
		adc	#1
		sta	window_text_x
		sta	window_char_x
	: ; }

	lda	tbl_windows+tbl_windows_YPOS,y
	clc
	adc	ppu_nt_ypos
	clc
	adc	#8
	sta	math_div_a
	lda	#$0F
	sta	math_div_b
	tya
	pha
	jsr	math_divb
	pla
	tay
	lda	math_result
	asl	a
	sta	window_block_row

	lda	window_text_y
	cmp	#$FF
	bne	:++ ; if (window_text_y == $FF) {
		lda	window_block_row
		clc
		adc	#2
		cmp	#$1E
		bne	:+ ; if (window_block_row + 2 == $1E) {
			lda	#0
		: ; }
		sta	window_text_y
	: ; }

	lda	tbl_windows+tbl_windows_XPOS,y
	sta	nt_calc_column
	lda	tbl_windows+tbl_windows_YPOS,y
	sta	nt_calc_row
	jsr	nt_calc_ppu_addr
	lda	ppu_nmi_write_dest
	sta	window_block_x
	sta	ppu_pal_load_src
	lda	ppu_nmi_write_dest+1
	sta	window_block_y
	sta	ppu_pal_load_src+1
	lda	tbl_windows+tbl_windows_WIDTH,y
	sta	window_start_pos

	ldy	#0
	: ; for (y = 0; window_text[y] != CHAR_CTRL_END; y++) {
		lda	(window_text),y
		cmp	#CHAR_CTRL_END
		beq	:+
		iny
		jmp	:-
	: ; }
	tya
	sec
	adc	window_text
	sta	window_desc
	lda	window_text+1
	adc	#0
	sta	window_desc+1

	ldy	#0
	: ; for (y = 0; window_desc[y] != CHAR_CTRL_END; y++) {
		lda	(window_desc),y
		cmp	#CHAR_CTRL_END
		beq	:+
		iny
		jmp	:-
	: ; }
	tya
	sec
	adc	window_desc
	sta	window_block_addr
	sta	window_map_buffer
	lda	window_desc+1
	adc	#0
	sta	window_block_addr+1
	sta	window_map_buffer+1
	lda	#0
	sta	window_block_width
	ldy	#0
	: ; for (y = 0; dialog_buffer[y] != CHAR_CTRL_END; y++) {
		lda	dialog_buffer,y
		cmp	#CHAR_CTRL_END
		beq	:+
		inc	window_block_width
		inc	window_block_width
		iny
		bne	:-
	: ; }

	lda	window_choice
	cmp	#$FE
	bne	:+ ; if (window_choice == $FE) {
		lda	window_block_width
		lsr	a
		sec
		sbc	#1
		sta	window_choice
	: ; }

	ldx	window_block_width
	: ; for (x = $9E; x > 0; x--) {
		ldy	#0
		: ; for (y = 0; !(y < 0); y++) {
			lda	ppu_nmi_apply_mirror
			bne	:+
			lda	(window_block_x),y
			cmp	#$FF
			bne	:++
			: ; if () {
				lda	#$FE
				sta	(window_block_x),y

			: ; }

			iny
			cpy	window_start_pos
			bne	:---
		; }

		lda	window_block_x
		clc
		adc	#$20
		sta	window_block_x
		bcc	:+
			inc	window_block_y
		:

		dex
		bne	:-----
	; }

	jsr	sprites_draw
	lda	ppu_pal_load_src
	sta	window_block_x
	lda	ppu_pal_load_src+1
	sta	window_block_y
	lda	window_block_col
	sta	window_start_pos
	lda	#0
	sta	window_column
	: ; for (;;) {
		lda	window_desc
		sta	window_block_addr
		lda	window_desc+1
		sta	window_block_addr+1
		ldx	window_column
		lda	dialog_buffer,x
		sta	window_row
		ldy	#0
		: ; for (y = 0; window_block_addr[y] != $FF; window_row--) {
			lda	(window_block_addr),y
			inc	window_block_addr
			bne	:+
				inc	window_block_addr+1
			:

			cmp	#$FF
			bne	:--
			dec	window_row
			bne	:--
		; }

		jsr	ppu_nmi_wait
		jsr	window_puts
		lda	ppu_pal_load_src
		clc
		adc	#$20
		sta	ppu_pal_load_src
		sta	window_block_x
		lda	ppu_pal_load_src+1
		adc	#0
		sta	ppu_pal_load_src+1
		sta	window_block_y
		lda	window_start_pos
		sta	window_block_col
		inc	window_block_row
		inc	window_column
		ldx	window_column
		lda	dialog_buffer,x
		cmp	#$FF
		bne	:+ ; if (dialog_buffer[x] == $FF) {
			lda	window_desc
			sta	window_block_addr
			lda	window_desc+1
			sta	window_block_addr+1
			jsr	window_puts
			jsr	ppu_nmi_wait
			rts
		: ; }

		lda	window_text
		sta	window_block_addr
		lda	window_text+1
		sta	window_block_addr+1
		jsr	window_puts
		lda	window_start_pos
		sta	window_block_col
		inc	window_block_row
		lda	window_block_row
		cmp	#$1E
		bne	:+ ; if (window_block_row == $1E) {
			lda	#0
			sta	window_block_row
		: ; }

		lda	ppu_pal_load_src
		clc
		adc	#$20
		sta	ppu_pal_load_src
		sta	window_block_x
		lda	ppu_pal_load_src+1
		adc	#0
		sta	ppu_pal_load_src+1
		sta	window_block_y
		jmp	:-----
	; }
