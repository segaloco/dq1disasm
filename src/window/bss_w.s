.segment	"ZEROPAGE"

	.export window_block_col, window_block_row
	.export window_block_count, window_block_addr
	.export window_map_buffer, window_start_pos
	.export window_block_width
	.export window_text, window_desc
window_block_col:	.byte	0
window_block_row:
window_block_count:	.byte	0
window_block_addr:	.addr	0
window_map_buffer:	.addr	0
window_start_pos:	.byte	0
window_block_width:	.byte	0
window_text:		.word	0
window_desc:		.word	0
