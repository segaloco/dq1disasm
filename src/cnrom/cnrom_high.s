.include	"system/cnrom.i"
.include	"system/ppu.i"

.include	"ppu_commands.i"
.include	"ppu_nmi.i"
.include	"general_bss.i"

CNROM_LOAD_PAGE_SIZE	=	$70
CNROM_LOAD_BANK_BITS	=	%01100000
CNROM_LOAD_SRC_HIGH	=	%00011111

	.export cnrom_load
cnrom_load:
	: ; while (ppu_nmi_entry_index) {
		lda	ppu_nmi_entry_index
		bne	:-
	; }

	lda	cnrom_load_src+1
	and	#CNROM_LOAD_BANK_BITS
	sta	cnrom_load_bank
	lda	cnrom_load_src+1
	and	#CNROM_LOAD_SRC_HIGH
	sta	cnrom_load_src+1

	loop: ; for (;;) {
		lda	#CNROM_LOAD_PAGE_SIZE
		sta	cnrom_load_page_size

		lda	cnrom_load_count
		sec
		sbc	#CNROM_LOAD_PAGE_SIZE
		sta	cnrom_load_count
		lda	cnrom_load_count+1
		sbc	#0
		sta	cnrom_load_count+1
		bcs	:+
		; if (cnrom_load_count > CNROM_LOAD_PAGE_SIZE) {
			lda	cnrom_load_count
			clc
			adc	#CNROM_LOAD_PAGE_SIZE
			sta	cnrom_load_page_size
			lda	#0
			sta	cnrom_load_count
			sta	cnrom_load_count+1
		: ; }

		lda	#PPU_NMI_SKIP
		sta	ppu_nmi_run
		sei

		ldy	#0

		: ; while (ppu_nmi_run) {
			lda	ppu_nmi_run
			bne	:-
		; }

		lda         cnrom_load_bank
		bne         :+
		; switch (cnrom_load_bank) {
		; case CNROM_BANK_INDEX_0:
			jsr	cnrom_bank_sec_00
			bne	:++++
		:
		cmp	#CNROM_BANK_INDEX_1
		bne	:+
		; case CNROM_BANK_INDEX_1:
			jsr	cnrom_bank_sec_01
			bne	:+++
		:
		cmp	#CNROM_BANK_INDEX_2
		bne	:+
		; case CNROM_BANK_INDEX_2:
			jsr	cnrom_bank_sec_02
			bne	:++
		: ; default:
			jsr	cnrom_bank_sec_03
		: ; }

		lda	cnrom_load_src+1
		sta	PPU_VRAM_AR
		lda	cnrom_load_src
		sta	PPU_VRAM_AR
		lda	PPU_VRAM_IO

		: ; for (y = 0; y < cnrom_load_page_size; y++) {
			lda	PPU_VRAM_IO
			sta	(cnrom_load_dest),y
			iny
			cpy	cnrom_load_page_size
			bne	:-
		; }

		jsr	cnrom_bank_reset

		cli

		lda	cnrom_load_count
		ora	cnrom_load_count+1
		bne	:+
		; if (cnrom_load_count == 0) {
			rts
		: ; }

		lda	cnrom_load_src
		clc
		adc	cnrom_load_page_size
		sta	cnrom_load_src
		lda	cnrom_load_src+1
		adc	#0
		sta	cnrom_load_src+1

		lda	cnrom_load_dest
		clc
		adc	cnrom_load_page_size
		sta	cnrom_load_dest
		lda	cnrom_load_dest+1
		adc	#0
		sta	cnrom_load_dest+1

		jmp	loop
	; }
	.end
