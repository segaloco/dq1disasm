.include	"system/ppu.i"

.include	"bssmap.i"
.include	"ppu_nmi.i"
.include	"ppu_nmi_write.i"
.include	"ppu_pal.i"
.include	"apu_commands.i"
.include	"general_bss.i"
.include	"game_bss.i"
.include	"ppu_bss.i"
.include	"map_calc.i"
.include	"enemies.i"
.include	"tunables.i"
.include	"macros.i"

DRAGONLORD_OAM_END	= $FF

.segment	"CHR3CODE"

	.export dragonlord_addr, dragonlord_load, dragonlord_load_end
dragonlord_addr:
.org	GAME_BUFFER
dragonlord_load:
	jsr	ppu_nmi_wait
	movw_m	ppu_pal_bg_fade, ppu_pal_fade_bg
	movw_m	ppu_pal_null, ppu_pal_fade_obj
	jsr	ppu_pal_fadeout
	movw_m	PPU_VRAM_BG1, ppu_nmi_write_dest
	lda	#$1e
	sta	general_word_0
	lda	#<((tile_blank_chr00-chr00_base)/PPU_TILE_SIZE)
	sta	ppu_nmi_write_data
	jsr	dragonlord_tile_line
	lda	#0
	sta	ppu_nmi_write_data
	lda	#2
	sta	general_word_0
	jsr	dragonlord_tile_line
	jsr	ppu_nmi_wait
	ldy	#0
	: ; for (entry of dragonlord_bg_tiles) {
		lda	dragonlord_bg_tiles, y
		sta	ppu_nmi_write_data
		iny
		lda	dragonlord_bg_tiles, y
		sta	ppu_nmi_write_dest
		iny
		lda	dragonlord_bg_tiles, y
		sta	ppu_nmi_write_dest+1
		iny
		jsr	ppu_nmi_write
		cpy	#(dragonlord_bg_tiles_end-dragonlord_bg_tiles)
		bne	:-
	; }

	ldx	#0
	: ; for (x = 0; x < $FF && dragonlord_oam[x] != DRAGONLORD_OAM_END; x++) {
		lda	dragonlord_oam, x
		cmp	#DRAGONLORD_OAM_END
		beq	:+
		sta	oam_buffer, x
		inx
		bne	:-
	: ; }

	jsr	ppu_nmi_wait
	jsr	cnrom_bank_sec_02
	movw_m	dragonlord_pal_1, ppu_pal_fade_obj
	movw_m	dragonlord_pal_0, ppu_pal_fade_bg
	lda	#0
	sta	ppu_nmi_scc_h
	sta	ppu_nmi_scc_v
	sta	ppu_nmi_bg_high
	lda	#PPU_H_INIT
	sta	ppu_nt_xpos
	lda	#PPU_V_INIT
	sta	ppu_nt_ypos
	apusfx_m	sfx_fire
	jsr	apu_play
	lda	#enemy::dragonlord
	sta	battle_enemy
	jsr	ppu_pal_fadein
	jsr	ppu_pal_fadein
	jsr	ppu_pal_fadein
	jsr	ppu_pal_fadein
	jsr	ppu_nmi_wait
	ldx	#$28
	: ; for (x = 0x28; x > 0; x--) {
		jsr	ppu_nmi_wait
		dex
		bne	:-
	; }

	rts

dragonlord_tile_line:
	: ; for (general_word_0; general_word_0 > 0; general_word_0--) {
		jsr	ppu_nmi_wait
		ldy	#$20
		: ; for (y = 0x20; y > 0; y--) {
			jsr	ppu_nmi_write
			dey
			bne	:-
		; }

		dec	general_word_0
		bne	:--
	; }

	rts


	lda	ppu_nt_ypos
	asl	a
	clc
	adc	map_tile_ypos
	clc
	adc	#$1e
	sta	math_div_a
	lda	#$1e
	sta	math_div_b
	jsr	math_divb
	lda	math_result
	sta	general_word_1
	lda	ppu_nt_xpos
	asl	a
	clc
	adc	map_tile_xpos
	and	#$3f
	sta	general_word_0
	rts

dragonlord_bg_tiles:
	.byte   $6D
        .addr   PPU_VRAM_BG1+$12F

        .byte   $6E
        .addr   PPU_VRAM_BG1+$14F

        .byte   $A4
        .addr   PPU_VRAM_BG1+$170

        .byte   $A7
        .addr   PPU_VRAM_BG1+$171

        .byte   $6F
        .addr   PPU_VRAM_BG1+$18E

        .byte   $71
        .addr   PPU_VRAM_BG1+$18F

        .byte   $73
        .addr   PPU_VRAM_BG1+$190

        .byte   $75
        .addr   PPU_VRAM_BG1+$191

        .byte   $E7
        .addr   PPU_VRAM_BG1+$192

        .byte   $70
        .addr   PPU_VRAM_BG1+$1AE

        .byte   $72
        .addr   PPU_VRAM_BG1+$1AF

        .byte   $74
        .addr   PPU_VRAM_BG1+$1B0

        .byte   $76
        .addr   PPU_VRAM_BG1+$1B1

        .byte   $CB
        .addr   PPU_VRAM_BG1+$1B2

        .byte   $BB
        .addr   PPU_VRAM_BG1+$1B3

        .byte   $AD
        .addr   PPU_VRAM_BG1+$1CE

        .byte   $AA
        .addr   PPU_VRAM_BG1+$1CF

        .byte   $01
        .addr   PPU_VRAM_BG1+$1D0

        .byte   $02
        .addr   PPU_VRAM_BG1+$1D1

        .byte   $44
        .addr   PPU_VRAM_BG1+PPU_VRAM_BG_VIS+$13

        .byte   $20
        .addr   PPU_VRAM_BG1+PPU_VRAM_BG_VIS+$14

        .byte   $B8
        .addr   PPU_VRAM_BG1+PPU_VRAM_BG_VIS+$1B

        .byte   $3A
        .addr   PPU_VRAM_BG1+PPU_VRAM_BG_VIS+$1C
dragonlord_bg_tiles_end:

dragonlord_oam:
	.byte   $6F, $0E, obj_attribute::color0, $90
        .byte   $77, $13, obj_attribute::color0, $70
        .byte   $77, $14, obj_attribute::color0, $78
        .byte   $77, $1D, obj_attribute::color0, $80
        .byte   $77, $1E, obj_attribute::color0, $88
        .byte   $77, $1F, obj_attribute::color0, $90
        .byte   $7F, $20, obj_attribute::color0, $70
        .byte   $7F, $21, obj_attribute::color0, $78
        .byte   $7F, $22, obj_attribute::color0, $80
        .byte   $7F, $23, obj_attribute::color0, $88
        .byte   $7F, $2C, obj_attribute::color0, $90
        .byte   $87, $2D, obj_attribute::color0, $75
        .byte   $87, $2E, obj_attribute::color0, $7D
        .byte   $87, $33, obj_attribute::color0, $88
        .byte   $87, $34, obj_attribute::color0, $90
        .byte   $8A, $31, obj_attribute::color1, $71
        .byte   $8E, $32, obj_attribute::color1, $84
        .byte   $8F, $2F, obj_attribute::color0, $74
        .byte   $8F, $30, obj_attribute::color0, $7D
        .byte   $8F, $35, obj_attribute::color0, $8B
        .byte   $8F, $36, obj_attribute::color0, $93
        .byte   $92, $38, obj_attribute::color0, $9B
        .byte   $94, $39, obj_attribute::color1, $9A
        .byte   $97, $37, obj_attribute::color0, $93
        .byte   $64, $49, obj_attribute::color1, $5E
        .byte   $60, $4A, obj_attribute::color1, $66
        .byte   $68, $4B, obj_attribute::color1, $66
        .byte   $5B, $4C, obj_attribute::color1, $6E
        .byte   $63, $4D, obj_attribute::color1, $6E
        .byte   $6B, $4E, obj_attribute::color1, $6E
        .byte   $5B, $4F, obj_attribute::color1, $76
        .byte   $63, $50, obj_attribute::color1, $76
        .byte   $6B, $51, obj_attribute::color1, $76
        .byte   $6F, $62, obj_attribute::color0, $68
        .byte   $59, $52, obj_attribute::color1, $90
        .byte   $61, $53, obj_attribute::color1, $90
        .byte   $6B, $54, obj_attribute::color1, $90
        .byte   $59, $56, obj_attribute::color1, $98
        .byte   $61, $57, obj_attribute::color1, $98
        .byte   $69, $58, obj_attribute::color1, $98
        .byte   $5F, $59, obj_attribute::color1, $A0
        .byte   $67, $5A, obj_attribute::color1, $A0
        .byte   $67, $5B, obj_attribute::color1, $A8
        .byte   $6F, $5C, obj_attribute::color1, $9F
        .byte   $3E, $5D, obj_attribute::color2, $61
        .byte   $3E, $5E, obj_attribute::color2, $69
        .byte   $46, $5F, obj_attribute::color2, $5E
        .byte   $46, $60, obj_attribute::color2, $66
        .byte   $46, $61, obj_attribute::color2, $6E
        .byte   $40, $3D, obj_attribute::color0, $78
        .byte   $48, $3E, obj_attribute::color0, $78
        .byte   $50, $3F, obj_attribute::color0, $78
        .byte   $47, $40, obj_attribute::color0, $80
        .byte   $4E, $42, obj_attribute::color2, $80
        .byte   $4F, $41, obj_attribute::color0, $80
        .byte   $47, $43, obj_attribute::color0, $88
        .byte   $4F, $44, obj_attribute::color0, $88
        .byte   $4A, $45, obj_attribute::color2, $8B
        .byte   $52, $46, obj_attribute::color2, $8B
        .byte   $4C, $47, obj_attribute::color2, $93
        .byte   $4F, $48, obj_attribute::color2, $9B
        .byte   $56, $3A, obj_attribute::color3, $64
        .byte   $56, $3B, obj_attribute::color3, $6C
        .byte   $5E, $3C, obj_attribute::color3, $6C
	.byte	DRAGONLORD_OAM_END

dragonlord_pal_0:
	.byte	PPU_COLOR_LEV3|PPU_COLOR_GREY, PPU_COLOR_NULL, PPU_COLOR_LEV3|PPU_COLOR_GREY
	.byte	PPU_COLOR_LEV1|PPU_COLOR_ORANGE, PPU_COLOR_LEV1|PPU_COLOR_ROSE, PPU_COLOR_LEV3|PPU_COLOR_GREY
	.byte	PPU_COLOR_LEV2|PPU_COLOR_AZURE, PPU_COLOR_LEV2|PPU_COLOR_BLUE, PPU_COLOR_LEV2|PPU_COLOR_ORANGE
	.byte	PPU_COLOR_LEV0|PPU_COLOR_PITCH, PPU_COLOR_LEV2|PPU_COLOR_ORANGE, PPU_COLOR_LEV2|PPU_COLOR_ORANGE

dragonlord_pal_1:
	.byte 	PPU_COLOR_LEV2|PPU_COLOR_AZURE, PPU_COLOR_LEV2|PPU_COLOR_BLUE, PPU_COLOR_LEV2|PPU_COLOR_ORANGE
	.byte	PPU_COLOR_LEV1|PPU_COLOR_ORANGE, PPU_COLOR_LEV0|PPU_COLOR_CYAN, PPU_COLOR_LEV3|PPU_COLOR_GREY
	.byte	PPU_COLOR_LEV0|PPU_COLOR_ORANGE, PPU_COLOR_LEV1|PPU_COLOR_ROSE, PPU_COLOR_LEV3|PPU_COLOR_GREY
	.byte	PPU_COLOR_LEV2|PPU_COLOR_AZURE, PPU_COLOR_LEV2|PPU_COLOR_ORANGE, PPU_COLOR_LEV1|PPU_COLOR_ROSE
dragonlord_load_end:
