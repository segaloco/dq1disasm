.include	"system/ppu.i"

.include	"bssmap.i"
.include	"ppu_nmi_write.i"
.include	"ppu_pal.i"
.include	"game_bss.i"
.include	"window.i"
.include	"charmap_2.i"
.include	"macros.i"

.segment	"CHR3CODE"
	.export title_load_map_addr, title_load_map_start, title_load_map_end
title_load_map_addr:
.org		GAME_BUFFER
title_load_map_start:

	.export title_load_map
MAPCOMP_STRING_DEF	=	$F7
MAPCOMP_END_LINE	=	$FC
; title_load_map - PPU initialization for the titlescreen
; first palettes are blanked and sprites moved offscreen
; next, the background mapping is decompressed into BG1
;
; the compression format consists of individual tile IDs
; interspersed with delimited strings of repeated tiles
; each such string is opened with byte MAPCOMP_STRING_DEF followed by
; a count and then the tile ID to emit that number of times
; additionally, MAPCOMP_END_LINE is used to await
; the next frame before proceeding,
; checking for overflow in the process
title_load_map:
	jsr	ppu_nmi_wait

	movw_m	ppu_pal_null, ppu_pal_load_src
	lda	#PPU_COLOR_LEV0
	sta	ppu_pal_load_cutoff
	jsr	ppu_pal_load_bg

	movw_m	ppu_pal_null, ppu_pal_load_src
	lda	#PPU_COLOR_LEV0
	sta	ppu_pal_load_cutoff
	jsr	ppu_pal_load_obj

	jsr	oam_init

	movw_m	mappings, window_block_addr
	movw_m	PPU_VRAM_BG1, ppu_nmi_write_dest
	: ; for (ppu_nmi_write_dest = PPU_VRAM_BG1; ppu_nmi_write_dest < PPU_VRAM_BG2; ppu_nmi_write_dest++) {
		jsr	ppu_nmi_wait

		: ; for (next_byte = 0; next_byte != MAPCOMP_END_LINE; next_byte = fetch_next_byte()) {
			jsr	fetch_next_byte
			cmp	#MAPCOMP_END_LINE
			beq	:+++
			
			cmp	#MAPCOMP_STRING_DEF
			bne	:++ ; if (next_byte == MAPCOMP_STRING_DEF) {
				jsr	fetch_next_byte
				sta	window_string_len
				jsr	fetch_next_byte
				sta	ppu_nmi_write_data
				: ; for (window_string_len = fetch_next_byte(); window_string_len > 0; window_string_len--) {
					jsr	ppu_nmi_write
					dec	window_string_len
					bne	:-
				; }
				beq	:--
			: ; } else {
				sta	ppu_nmi_write_data	 
				jsr	ppu_nmi_write
				jmp	:---
			; }
		: ; }

		lda	ppu_nmi_write_dest+1
		cmp	#.hibyte(PPU_VRAM_BG2)
		bne	:-----
	; }

	rts

fetch_next_byte:
	ldy	#0
	lda	(window_block_addr),y
	inc	window_block_addr
	bne	:+
		inc	window_block_addr+1
	:
	rts

mappings:
	.byte	MAPCOMP_STRING_DEF, 32, " ", MAPCOMP_END_LINE
	.byte	MAPCOMP_STRING_DEF, 32, " ", MAPCOMP_END_LINE
	.byte	MAPCOMP_STRING_DEF, 32, " ", MAPCOMP_END_LINE
	.byte	MAPCOMP_STRING_DEF, 32, " ", MAPCOMP_END_LINE
	.byte	MAPCOMP_STRING_DEF, 32, " ", MAPCOMP_END_LINE
	.byte	MAPCOMP_STRING_DEF, 32, " ", MAPCOMP_END_LINE

	.byte	"   "
	.byte	$77, $7A
	.byte	MAPCOMP_STRING_DEF, 20, " "
	.byte	$EB, $EF, $E4
	.byte	MAPCOMP_STRING_DEF, 4, " "
	.byte	MAPCOMP_END_LINE

	.byte	"   "
	.byte	$78, $7B, $7E, $81
	.byte	MAPCOMP_STRING_DEF, 17, " "
	.byte	$E8, $EC, $F0, $FD
	.byte	MAPCOMP_STRING_DEF, 4, " "
	.byte	MAPCOMP_END_LINE

	.byte	"   "
	.byte	$79, $7C, $7F, $82, $8C, $8E, $90, $93
	.byte	MAPCOMP_STRING_DEF, 9, " "
	.byte	$CD, $D0, $D4, $E5, $E9, $ED, $F1, $FE
	.byte	MAPCOMP_STRING_DEF, 4, " "
	.byte	MAPCOMP_END_LINE

	.byte	MAPCOMP_STRING_DEF, 4, " "
	.byte	$7D, $80, $83, $8D, $8F, $91, $94, $A5
	.byte	$A8, $AB, $AE, $BC, $BD, $BE, $C0, $CC
	.byte	$CE, $D1, $D5, $E6, $EA, $EE, $F2, $FF
	.byte	MAPCOMP_STRING_DEF, 4, " "
	.byte	MAPCOMP_END_LINE

	.byte	MAPCOMP_STRING_DEF, 5, " "
	.byte	$84, $88, $95, $99, $9C, $9F, $AF, $B2
	.byte	$B5, $B8, $C1, $C3, $C5, $C8, $D6, $D9
	.byte	$DC, $DF, $F3, $87, $FA
	.byte	MAPCOMP_STRING_DEF, 6, " "
	.byte	MAPCOMP_END_LINE

	.byte	MAPCOMP_STRING_DEF, 5, " "
	.byte	$85, $89, $96, $9A, $9D, $A0, $B0, $B3
	.byte	$B6, $B9, $C2, $C4, $C6, $C9, $D7, $DA
	.byte	$DD, $E0, $F4, $F8, $FB
	.byte	MAPCOMP_STRING_DEF, 6, " "
	.byte	MAPCOMP_END_LINE

	.byte	MAPCOMP_STRING_DEF, 5, " "
	.byte	$86, $8A, $97, $9B, $9E, $A1, $B1, $B4
	.byte	$B7
	.byte	"   "
	.byte	$C7, $CA, $D8, $DB, $DE, $E1, $F5, $F9
	.byte	MAPCOMP_STRING_DEF, 7, " "
	.byte	MAPCOMP_END_LINE

	.byte	MAPCOMP_STRING_DEF, 6, " "
	.byte	$8B, $98
	.byte	MAPCOMP_STRING_DEF, 14, " "
	.byte	$E2, $F6
	.byte	MAPCOMP_STRING_DEF, 8, " "
	.byte	MAPCOMP_END_LINE

	.byte	MAPCOMP_STRING_DEF, 32, " ", MAPCOMP_END_LINE
	.byte	MAPCOMP_STRING_DEF, 32, " ", MAPCOMP_END_LINE

	.byte	MAPCOMP_STRING_DEF, 11, " "
	.byte	">START"
	.byte	MAPCOMP_STRING_DEF, 15, " "
	.byte	MAPCOMP_END_LINE

	.byte	MAPCOMP_STRING_DEF, 32, " ", MAPCOMP_END_LINE

	.byte	MAPCOMP_STRING_DEF, 12, " "
	.byte	"CONTINUE"
	.byte	MAPCOMP_STRING_DEF, 12, " "
	.byte	MAPCOMP_END_LINE

	.byte	MAPCOMP_STRING_DEF, 32, " ", MAPCOMP_END_LINE
	.byte	MAPCOMP_STRING_DEF, 32, " ", MAPCOMP_END_LINE

	.byte	MAPCOMP_STRING_DEF, 8, " "
	.byte	"-MESSAGE SPEED-"
	.byte	MAPCOMP_STRING_DEF, 9, " "
	.byte	MAPCOMP_END_LINE

	.byte	MAPCOMP_STRING_DEF, 32, " ", MAPCOMP_END_LINE

	.byte	MAPCOMP_STRING_DEF, 6, " "
	.byte	"SLOW  >NORMAL   FAST"
	.byte	MAPCOMP_STRING_DEF, 6, " "
	.byte	MAPCOMP_END_LINE

	.byte	MAPCOMP_STRING_DEF, 32, " ", MAPCOMP_END_LINE
	.byte	MAPCOMP_STRING_DEF, 32, " ", MAPCOMP_END_LINE

	.byte	MAPCOMP_STRING_DEF, 11, " "
	.byte	"c1986 ENIX"
	.byte	MAPCOMP_STRING_DEF, 11, " "
	.byte	MAPCOMP_END_LINE

	.byte	MAPCOMP_STRING_DEF, 32, " ", MAPCOMP_END_LINE
	.byte	MAPCOMP_STRING_DEF, 32, " ", MAPCOMP_END_LINE
	.byte	MAPCOMP_STRING_DEF, 32, " ", MAPCOMP_END_LINE

title_load_map_bgpal:
	.byte	MAPCOMP_STRING_DEF, 8, $FF
	.byte	$7F, $5F, $9F, $AF, $AF, $AF, $5F, $FF, $77
	.byte	MAPCOMP_STRING_DEF, 4, $55
	.byte	$11, $44, $FF, $FF
	.byte	MAPCOMP_STRING_DEF, 4, $F5
	.byte	$F1, $F4, $FF
	.byte	MAPCOMP_END_LINE

	.byte	MAPCOMP_STRING_DEF, 16, $AA
	.byte	MAPCOMP_STRING_DEF, 16, $FF
	.byte	MAPCOMP_END_LINE

title_load_map_end:
	.end
