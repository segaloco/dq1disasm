; Titlescreen constants
.ifndef	TITLE_I
TITLE_I = 1

.include	"sprite.i"

TITLE_START_COLUMN      =   $0B
TITLE_START_ROW         =   $10
TITLE_CONTINUE_ROW      =   $12

TITLE_MSG_SLOW_COLUMN   =   $05
TITLE_MSG_MID_COLUMN    =   $0C
TITLE_MSG_FAST_COLUMN   =   $15
TITLE_MSG_ROW           =   $17

	title_start_cursor_pos_x	= sprite_char_x
	title_start_cursor_pos_y	= sprite_char_y
	title_speed_cursor_pos_x	= sprite_x
	title_ani_counter		= sprite_x+1
	title_speed_cursor_pos_y	= sprite_y
	title_ani_index			= sprite_y+1

.endif	; TITLE_I
