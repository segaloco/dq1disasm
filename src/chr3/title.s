.include	"system/ppu.i"
.include	"system/apu.i"

.include	"ppu_nmi_write.i"
.include	"ppu_pal.i"
.include	"ppu_commands.i"
.include	"apu_commands.i"
.include	"bssmap.i"
.include	"joypad.i"
.include	"general_bss.i"
.include	"window.i"
.include	"sprite.i"
.include	"tunables.i"
.include	"game_bss.i"
.include	"apu_bss.i"
.include	"macros.i"

.include	"./title.i"

TITLE_APU_CMD_CUE	=	apu_cmd_next_byte
SPARKLE_PALETTE_INDEX	=	$7

TITLE_ANI_END		= $FF

.segment	"CHR3CODE"
	.export title_addr
	.export title_start, title_end
title_addr:
.org	CNROM_BUFFER
title_start:

	.export title
; title - the titlescreen
; a blank palette is loaded and the music started
; after either player input of start or select or a
; special APU cue:
;	load the real palette
;	load OBJ attributes
;	initialize animation counters, marker positions, selections
; finally, the main titlescreen loop is entered
title:
	jsr	ppu_nmi_wait

	movw_m	palette_title, ppu_pal_load_src
	lda	#PPU_COLOR_LEV0
	sta	ppu_pal_load_cutoff
	jsr	ppu_pal_load_bg
	jsr	palette_bg

	apusong_m	apu_song_title
	jsr	apu_play

	: ; while (!(joypad_read() & (select|start)) && *apu_pulse1_index != TITLE_APU_CMD_CUE) {
		sei

		jsr	joypad_read
		lda	joypad_state
		and	#joypad_button::select|joypad_button::start
		bne	:+
		ldy	#0
		lda	(apu_pulse1_index),y
		cmp	#TITLE_APU_CMD_CUE
		beq	:+

		cli
		jmp	:-
	: ; }

	cli

	jsr	ppu_nmi_wait

	movw_m	palette_title_menu, ppu_pal_load_src
	lda	#PPU_COLOR_LEV0
	sta	ppu_pal_load_cutoff
	jsr	ppu_pal_load_bg

	lda	#PPU_COLOR_LEV3|PPU_COLOR_ORANGE
	sta	ppu_nmi_write_data
	movw_m	PPU_COLOR_PAGE_OBJ+SPARKLE_PALETTE_INDEX, ppu_nmi_write_dest
	jsr	ppu_nmi_write

	jsr	palette_bg

	jsr	ppu_nmi_wait

	ldx	#0
	: ; for (entry of title_oam) {
		lda	title_oam,x
		sta	oam_buffer,x
		inx
		cpx	#(title_oam_end-title_oam)
		bne	:-
	; }

	lda	#0
	sta	title_ani_counter
	lda	#$FF
	sta	title_ani_index

	lda	#TITLE_START_COLUMN
	sta	title_start_cursor_pos_x
	lda	#TITLE_MSG_MID_COLUMN
	sta	title_speed_cursor_pos_x
	lda	#TITLE_START_ROW
	sta	title_start_cursor_pos_y
	lda	#TITLE_MSG_ROW
	sta	title_speed_cursor_pos_y

	lda	#MSG_SPEED_MID
	sta	msg_speed

	; the main title screen loop
	; animations are applied to any applicable OBJ
	; unused OBJ entries are removed
	; animation counters are incremented
	; if a button is not held-over from the last
	; frame, handle it:
	;	start - blank the screen and start the town music (prepare for new/password screens, falls through apu_play)
	;	select - toggle between new and continue options
	;	up - move from continue to new
	;	down - move from new to continue
	;	left - lower message speed
	;	right - raise message speed
	loop: ; for (;;) {
		jsr	ppu_nmi_wait

		ldx	#0

		lda	title_ani_index
		cmp	#$FF
		beq	:++++
		; if (title_ani_index != 0xFF) {
			ldy	title_ani_index
			lda	title_ani,y
			sta	window_block_addr
			lda	title_ani+1,y
			sta	window_block_addr+1

			ldy	#0
			: ; for (line of animation || overflow) {
				lda	(window_block_addr),y
				cmp	#TITLE_ANI_END
				beq	:+

				sta	OAM_BUFFER_2,x
				iny
				inx
				lda	(window_block_addr),y
				sta	OAM_BUFFER_2,x
				iny
				inx
				lda	(window_block_addr),y
				and	#$C0
				ora	#1
				sta	OAM_BUFFER_2,x
				inx
				lda	(window_block_addr),y
				and	#$3F
				clc
				adc	#$B4
				sta	OAM_BUFFER_2,x
				iny
				inx
				bne	:-
			: ; }

			lda	title_ani_counter
			cmp	#$80
			bcs	:+
			lsr	a
			bcc	:++

			: ; if (title_ani_counter < $80 || title_ani_counter % 2 == 0) {
				inc	title_ani_index
				inc	title_ani_index

				lda	title_ani_index
				cmp	#title_ani_end-title_ani
				bne	:+
				; if ((title_ani_index += 2) == (title_ani_end-title_ani)) {
					lda	#$FF
					sta	title_ani_index
				; }
			; }
		: ; }

		lda	#PPU_SCANLINE_240
		: ; for (x = 0; x < 0xC0; x++) {
			sta	OAM_BUFFER_2,x
			inx
			cpx	#$C0
			bne	:-
		; }

		inc	title_ani_counter

		lda	title_ani_counter
		cmp	#$20
		beq	:+
		cmp	#$A0
		beq	:+
		cmp	#$C0
		bne	:++
		: ; if (++*title_ani_counter.in($20, $A0, $C0)) {
			lda	#0
			sta	title_ani_index
		: ; }

		lda	joypad_state
		pha
		jsr	joypad_read
		pla
		bne	continue1

		lda	joypad_state
		and	#joypad_button::start
		beq	:+
		; if (joypad_state & start) {
			movw_m	ppu_pal_null, ppu_pal_load_src
			lda	#PPU_COLOR_LEV0
			sta	ppu_pal_load_cutoff
			jsr	ppu_pal_load_bg

			movw_m	ppu_pal_null, ppu_pal_load_src
			lda	#PPU_COLOR_LEV0
			sta	ppu_pal_load_cutoff
			jsr	ppu_pal_load_obj

			apusong_m	apu_song_town
			jmp	apu_play
		: ; }

		lda	joypad_state
		and	#joypad_button::select
		beq	:+
		; if (joypad_state & select) {
			lda	title_start_cursor_pos_y
			cmp	#TITLE_START_ROW
			beq	move_arrow_down
			bne	move_arrow_up
		: ; }

		lda	joypad_state
		and	#joypad_button::up
		beq	:+
		; if (joypad_state & up) {
			lda	title_start_cursor_pos_y
			cmp	#TITLE_CONTINUE_ROW
			bne	continue1

		move_arrow_up:
			jsr	arrow_hide_start_type
			lda	#TITLE_START_ROW
			sta	title_start_cursor_pos_y

		reposition_start_arrow:
			jsr	arrow_show_start_type

		continue1:
			jmp	loop
		: ; }

		lda	joypad_state
		and	#joypad_button::down
		beq	:+
		; if (joypad_state & down) {
			lda	title_start_cursor_pos_y
			cmp	#TITLE_START_ROW
			bne	continue1

		move_arrow_down:
			jsr	arrow_hide_start_type
			lda	#TITLE_CONTINUE_ROW
			sta	title_start_cursor_pos_y
			bne	reposition_start_arrow
		: ; }

		lda	joypad_state
		and	#joypad_button::left
		beq	:+
		; if (joypad_state & left) {
			lda	title_speed_cursor_pos_x
			cmp	#TITLE_MSG_SLOW_COLUMN
			beq	continue2

			jsr	arrow_hide_msg_speed
			inc	msg_speed
			lda	title_speed_cursor_pos_x
			cmp	#TITLE_MSG_FAST_COLUMN
			beq	move_to_middle

			; if (title_speed_cursor_pos_x != fast_column) {
				lda	#TITLE_MSG_SLOW_COLUMN
				bne	reposition_speed_arrow
			; } else {
			move_to_middle:
				lda	#TITLE_MSG_MID_COLUMN
			; }

		reposition_speed_arrow:

			sta	title_speed_cursor_pos_x
			jsr	arrow_show_msg_speed

		continue2:

			jmp	loop
		: ; }

		lda	joypad_state
		bpl	continue2
		; if (joypad_state & right) {
			lda	title_speed_cursor_pos_x
			cmp	#TITLE_MSG_FAST_COLUMN
			beq	continue2
			
			jsr	arrow_hide_msg_speed
			dec	msg_speed

			lda	title_speed_cursor_pos_x
			cmp	#TITLE_MSG_SLOW_COLUMN
			beq	move_to_middle
			; if (title_speed_cursor_pos_x != slow_column) {
				lda	#TITLE_MSG_FAST_COLUMN
				bne	reposition_speed_arrow
			; }
		; }
	; }

arrow_hide_start_type:
	lda	#<((tile_blank_chr02-chr02_base)/PPU_TILE_SIZE)
	sta	ppu_nmi_write_data
	bne	arrow_update_start_type

arrow_show_start_type:
	lda	#<((tile_rightarrow_chr02-chr02_base)/PPU_TILE_SIZE)
	sta	ppu_nmi_write_data

arrow_update_start_type:
	lda	title_start_cursor_pos_x
	sta	nt_calc_column
	lda	title_start_cursor_pos_y
	sta	nt_calc_row
	jsr	nt_calc_dest
	jmp	ppu_nmi_write

arrow_hide_msg_speed:
	lda	#<((tile_blank_chr02-chr02_base)/PPU_TILE_SIZE)
	bne	arrow_update_msg_speed

arrow_show_msg_speed:
	lda	#<((tile_rightarrow_chr02-chr02_base)/PPU_TILE_SIZE)

arrow_update_msg_speed:
	sta	ppu_nmi_write_data
	lda	title_speed_cursor_pos_x
	sta	nt_calc_column
	lda	title_speed_cursor_pos_y
	sta	nt_calc_row
	jsr	nt_calc_dest
	jmp	ppu_nmi_write

palette_bg:
	lda	#PPU_COLOR_LEV0|PPU_COLOR_CYAN
	sta	ppu_nmi_write_data
	movw_m	PPU_COLOR_PAGE_BG, ppu_nmi_write_dest
	jmp	ppu_nmi_write

palette_title:
	.byte	PPU_COLOR_LEV0|PPU_COLOR_GREY, PPU_COLOR_LEV1|PPU_COLOR_GREY, PPU_COLOR_LEV2|PPU_COLOR_GREY
	.byte	PPU_COLOR_LEV0|PPU_COLOR_GREY, PPU_COLOR_LEV1|PPU_COLOR_GREY, PPU_COLOR_LEV0|PPU_COLOR_CYAN
	.byte	PPU_COLOR_LEV0|PPU_COLOR_CYAN, PPU_COLOR_LEV0|PPU_COLOR_CYAN, PPU_COLOR_LEV0|PPU_COLOR_CYAN
	.byte	PPU_COLOR_LEV0|PPU_COLOR_CYAN, PPU_COLOR_LEV0|PPU_COLOR_CYAN, PPU_COLOR_LEV0|PPU_COLOR_CYAN
palette_title_menu:
	.byte	PPU_COLOR_LEV0|PPU_COLOR_GREY, PPU_COLOR_LEV1|PPU_COLOR_GREY,    PPU_COLOR_LEV2|PPU_COLOR_GREY
	.byte	PPU_COLOR_LEV0|PPU_COLOR_GREY, PPU_COLOR_LEV1|PPU_COLOR_GREY,    PPU_COLOR_LEV0|PPU_COLOR_CYAN
	.byte	PPU_COLOR_LEV0|PPU_COLOR_CYAN, PPU_COLOR_LEV2|PPU_COLOR_ORANGE,  PPU_COLOR_LEV2|PPU_COLOR_ORANGE
	.byte	PPU_COLOR_LEV0|PPU_COLOR_CYAN, PPU_COLOR_LEV2|PPU_COLOR_MAGENTA, PPU_COLOR_LEV2|PPU_COLOR_MAGENTA

title_oam:
	.byte	$32, $00, obj_attribute::priority_high|obj_attribute::color0, $30
	.byte	$32, $01, obj_attribute::priority_high|obj_attribute::color0, $38
	.byte	$32, $02, obj_attribute::priority_high|obj_attribute::color0, $40
	.byte	$2F, $03, obj_attribute::priority_high|obj_attribute::color0, $48
	.byte	$3A, $04, obj_attribute::priority_high|obj_attribute::color0, $38
	.byte	$3A, $05, obj_attribute::priority_high|obj_attribute::color0, $40
	.byte	$37, $06, obj_attribute::priority_high|obj_attribute::color0, $48
	.byte	$42, $07, obj_attribute::priority_high|obj_attribute::color0, $39
	.byte	$42, $08, obj_attribute::priority_high|obj_attribute::color0, $41
	.byte	$4A, $09, obj_attribute::priority_high|obj_attribute::color0, $36
	.byte	$4A, $0A, obj_attribute::priority_high|obj_attribute::color0, $3E
	.byte	$49, $0B, obj_attribute::priority_high|obj_attribute::color0, $46
	.byte	$52, $0C, obj_attribute::priority_high|obj_attribute::color0, $38
title_oam_end:

title_ani:
	.addr	title_ani_frame0
	.addr	title_ani_frame1
	.addr	title_ani_frame1
	.addr	title_ani_frame2
	.addr	title_ani_frame2
	.addr	title_ani_frame3
	.addr	title_ani_frame3
	.addr	title_ani_frame3
	.addr	title_ani_frame2
	.addr	title_ani_frame2
	.addr	title_ani_frame1
	.addr	title_ani_frame0
title_ani_end:

title_ani_frame0:
	.byte   $4C, $0D, $0D
	.byte   $54, $0D, obj_attribute::v_flip|obj_attribute::h_flip|$0D
	.byte	TITLE_ANI_END

title_ani_frame1:
	.byte   $44, $0F, $12
        .byte   $4C, $10, $09
        .byte   $4C, $11, $11
        .byte   $54, $11, obj_attribute::v_flip|obj_attribute::h_flip|$09
        .byte   $54, $10, obj_attribute::v_flip|obj_attribute::h_flip|$11
        .byte   $5C, $0F, obj_attribute::v_flip|obj_attribute::h_flip|$08
        .byte   $58, $0F, obj_attribute::v_flip|$0F
        .byte   TITLE_ANI_END

title_ani_frame2:
	.byte   $3C, $15, $15
        .byte   $44, $17, $0B
        .byte   $44, $16, $15
        .byte   $4C, $1A, $03
        .byte   $4C, $1B, $0B
        .byte   $4C, $1C, $13
        .byte   $4C, $19, $1A
        .byte   $54, $1C, obj_attribute::v_flip|obj_attribute::h_flip|$07
        .byte   $54, $1B, obj_attribute::v_flip|obj_attribute::h_flip|$0F
        .byte   $54, $1A, obj_attribute::v_flip|obj_attribute::h_flip|$17
        .byte   $5C, $16, obj_attribute::v_flip|obj_attribute::h_flip|$05
        .byte   $5C, $17, obj_attribute::v_flip|obj_attribute::h_flip|$0F
        .byte   $5E, $18, $0F
        .byte   $64, $15, obj_attribute::v_flip|obj_attribute::h_flip|$05
        .byte   TITLE_ANI_END

title_ani_frame3:
	.byte   $34, $0F, $1A
        .byte   $3C, $24, obj_attribute::v_flip|obj_attribute::h_flip|$19
        .byte   $44, $2A, obj_attribute::v_flip|obj_attribute::h_flip|$08
        .byte   $44, $25, obj_attribute::v_flip|obj_attribute::h_flip|$14
        .byte   $48, $2B, $00
        .byte   $4C, $28, obj_attribute::v_flip|obj_attribute::h_flip|$08
        .byte   $4C, $27, obj_attribute::v_flip|obj_attribute::h_flip|$10
        .byte   $4C, $26, obj_attribute::v_flip|obj_attribute::h_flip|$18
        .byte   $54, $26, $02
        .byte   $54, $27, $0A
        .byte   $54, $28, $12
        .byte   $58, $29, $1A
        .byte   $59, $12, obj_attribute::v_flip|obj_attribute::h_flip|$21
        .byte   $5C, $25, $06
        .byte   $5C, $2A, $12
        .byte   $64, $24, $01
        .byte   $6C, $16, $01
        .byte   TITLE_ANI_END

	.export title_pulse1, title_pulse2, title_triangle
title_pulse1:
	.byte	apu_cmd_tempo_change, $7D
	.byte	apu_cmd_wave_vol_change, $6
	.byte	apu_cmd_note_load|apu_notes::A4, $13
	.byte	apu_cmd_note_load|apu_notes::A4, $5

	.byte	apu_cmd_trough, $C
	.byte	apu_cmd_note_load|apu_notes::A4
	.byte	apu_cmd_note_load|apu_notes::G4
	.byte	apu_cmd_note_load|apu_notes::G4
	.byte	apu_cmd_note_load|apu_notes::G4
	.byte	apu_cmd_note_load|apu_notes::F4
	.byte	apu_cmd_note_load|apu_notes::G4
	.byte	apu_cmd_note_load|apu_notes::A4
	.byte	apu_cmd_note_load|apu_notes::Ax4
	.byte	apu_cmd_note_load|apu_notes::A4
	.byte	apu_cmd_note_load|apu_notes::G4
	.byte	apu_cmd_note_load|apu_notes::A4
	.byte	apu_cmd_note_load|apu_notes::Ax4
	.byte	apu_cmd_note_load|apu_notes::C5
	.byte	apu_cmd_note_load|apu_notes::D5
	.byte	apu_cmd_note_load|apu_notes::F5
	.byte	apu_cmd_note_load|apu_notes::D5
	.byte	apu_cmd_note_load|apu_notes::C5
	.byte	apu_cmd_note_load|apu_notes::Ax4
	.byte	apu_cmd_note_load|apu_notes::A4
	.byte	apu_cmd_trough_end

	.byte	apu_cmd_note_load|apu_notes::G4, $13
	.byte	apu_cmd_note_load|apu_notes::G4, $5
	.byte	apu_cmd_note_load|apu_notes::G4, $C
	.byte	apu_cmd_note_load|apu_notes::A4, $13
	.byte	apu_cmd_note_load|apu_notes::A4, $5
	.byte	apu_cmd_note_load|apu_notes::A4, $C
	.byte	apu_cmd_note_load|apu_notes::A4, $C
	.byte	apu_cmd_note_load|apu_notes::F4, $C
	.byte	apu_cmd_note_load|apu_notes::A4, $C

	.byte	apu_cmd_wave_vol_change, $3F
	.byte	apu_cmd_note_load|apu_notes::G4, $60

	.byte	apu_cmd_wave_vol_change, $30
	.byte	$18

	.byte	apu_cmd_tempo_change, $78
		: .byte	apu_cmd_wave_vol_change, $4F
		.byte	apu_cmd_note_load|apu_notes::C5, $13
		.byte	apu_cmd_note_load|apu_notes::C5, $5
		.byte	apu_cmd_note_load|apu_notes::F5, $18

		.byte	TITLE_APU_CMD_CUE

		.byte	apu_cmd_note_load|apu_notes::G5, $18
		.byte	apu_cmd_note_load|apu_notes::A5, $18
		.byte	apu_cmd_note_load|apu_notes::Ax5, $18

		.byte	apu_cmd_wave_vol_change, $7F
		.byte	apu_cmd_note_load|apu_notes::C6, $18
		.byte	apu_cmd_note_load|apu_notes::F6, $30

		.byte	apu_cmd_wave_vol_change, $4F
		.byte	apu_cmd_note_load|apu_notes::E6, $13
		.byte	apu_cmd_note_load|apu_notes::D6, $5
		.byte	apu_cmd_note_load|apu_notes::D6, $24
		.byte	apu_cmd_note_load|apu_notes::C6, $C

		.byte	apu_cmd_wave_vol_change, $40
		.byte	$c

		.byte	apu_cmd_wave_vol_change, $4F
		.byte	apu_cmd_note_load|apu_notes::B5, $C
		.byte	apu_cmd_note_load|apu_notes::B5, $C
		.byte	apu_cmd_note_load|apu_notes::D6, $C
		.byte	apu_cmd_note_load|apu_notes::C6, $18
		.byte	apu_cmd_note_load|apu_notes::A5, $30
		.byte	apu_cmd_note_load|apu_notes::A4, $13
		.byte	apu_cmd_note_load|apu_notes::A4, $5
		.byte	apu_cmd_note_load|apu_notes::A4, $18
		.byte	apu_cmd_note_load|apu_notes::A4, $18
		.byte	apu_cmd_note_load|apu_notes::B4, $18
		.byte	apu_cmd_note_load|apu_notes::Cx5, $18

		.byte	apu_cmd_wave_vol_change, $7F
		.byte	apu_cmd_note_load|apu_notes::D5, $30

		.byte	apu_cmd_wave_vol_change, $4F
		.byte	$C

		.byte	apu_cmd_trough, $C
		.byte	apu_cmd_note_load|apu_notes::D5
		.byte	apu_cmd_note_load|apu_notes::E5
		.byte	apu_cmd_note_load|apu_notes::F5

		.byte	apu_cmd_wave_vol_change, $7F
		.byte	apu_cmd_note_load|apu_notes::G5, $24

		.byte	apu_cmd_wave_vol_change, $4F
		.byte	$C

		.byte	apu_cmd_note_load|apu_notes::D5
		.byte	apu_cmd_note_load|apu_notes::D5
		.byte	apu_cmd_note_load|apu_notes::F5
		.byte	apu_cmd_note_load|apu_notes::F5, $C
		.byte	apu_cmd_note_load|apu_notes::E5, $C
		.byte	apu_cmd_note_load|apu_notes::D5, $C
		.byte	apu_cmd_note_load|apu_notes::C5, $C

		.byte	apu_cmd_wave_vol_change, $7F
		.byte	apu_cmd_note_load|apu_notes::A5, $30

		.byte	apu_cmd_wave_vol_change, $4F
		.byte	apu_cmd_note_load|apu_notes::Ax5
		.byte	apu_cmd_note_load|apu_notes::A5
		.byte	apu_cmd_note_load|apu_notes::G5

		.byte	apu_cmd_wave_vol_change, $7F
		.byte	apu_cmd_note_load|apu_notes::F5, $24
		.byte	apu_cmd_note_load|apu_notes::D5, $C
		.byte	apu_cmd_note_load|apu_notes::F5, $C
		.byte	apu_cmd_note_load|apu_notes::G5, $30

		.byte	apu_cmd_wave_vol_change, $4F
		.byte	apu_cmd_note_load|apu_notes::A5
		.byte	apu_cmd_note_load|apu_notes::G5
		.byte	apu_cmd_note_load|apu_notes::F5

		.byte	apu_cmd_wave_vol_change, $7F
		.byte	apu_cmd_note_load|apu_notes::F5, $24
		.byte	apu_cmd_note_load|apu_notes::E5, $C
		.byte	apu_cmd_note_load|apu_notes::C5, $C
		.byte	apu_cmd_note_load|apu_notes::C6, $30

		.byte	apu_cmd_wave_vol_change, $4F
		.byte	apu_cmd_note_load|apu_notes::A5
		.byte	apu_cmd_note_load|apu_notes::Ax5
		.byte	apu_cmd_note_load|apu_notes::C6

		.byte	apu_cmd_wave_vol_change, $7F
		.byte	apu_cmd_note_load|apu_notes::D6, $30

		.byte	apu_cmd_wave_vol_change, $4F
		.byte	apu_cmd_note_load|apu_notes::D5
		.byte	apu_cmd_note_load|apu_notes::E5
		.byte	apu_cmd_note_load|apu_notes::F5
		.byte	apu_cmd_trough_end

		.byte	apu_cmd_wave_vol_change, $7F
		.byte	apu_cmd_note_load|apu_notes::Ax5, $30
		.byte	apu_cmd_note_load|apu_notes::A5, $30
		.byte	apu_cmd_note_load|apu_notes::F5, $3C

		.byte	apu_cmd_wave_vol_change, $45
		.byte	$C

		.byte	apu_cmd_song_jsr
		.addr	:-

title_pulse2:
	.byte	apu_cmd_wave_vol_change, $6
	.byte	apu_cmd_note_load|apu_notes::C4, $13
	.byte	apu_cmd_note_load|apu_notes::F4, $5

	.byte	apu_cmd_trough, $C
	.byte	apu_cmd_note_load|apu_notes::F4
	.byte	apu_cmd_note_load|apu_notes::C4
	.byte	apu_cmd_note_load|apu_notes::C4
	.byte	apu_cmd_note_load|apu_notes::C4
	.byte	apu_cmd_note_load|apu_notes::A3
	.byte	apu_cmd_note_load|apu_notes::C4
	.byte	apu_cmd_note_load|apu_notes::F4
	.byte	apu_cmd_note_load|apu_notes::G4
	.byte	apu_cmd_note_load|apu_notes::F4
	.byte	apu_cmd_note_load|apu_notes::C4
	.byte	apu_cmd_note_load|apu_notes::F4
	.byte	apu_cmd_note_load|apu_notes::G4
	.byte	apu_cmd_note_load|apu_notes::A4
	.byte	apu_cmd_note_load|apu_notes::Ax4
	.byte	apu_cmd_note_load|apu_notes::D5
	.byte	apu_cmd_note_load|apu_notes::Ax4
	.byte	apu_cmd_note_load|apu_notes::A4
	.byte	apu_cmd_note_load|apu_notes::G4
	.byte	apu_cmd_note_load|apu_notes::F4
	.byte	apu_cmd_trough_end

	.byte	apu_cmd_note_load|apu_notes::C4, $13
	.byte	apu_cmd_note_load|apu_notes::C4, $5
	.byte	apu_cmd_note_load|apu_notes::C4, $C
	.byte	apu_cmd_note_load|apu_notes::F4, $13
	.byte	apu_cmd_note_load|apu_notes::F4, $5
	.byte	apu_cmd_note_load|apu_notes::F4, $C
	.byte	apu_cmd_note_load|apu_notes::F4, $C
	.byte	apu_cmd_note_load|apu_notes::A3, $C
	.byte	apu_cmd_note_load|apu_notes::F4, $C

	.byte	apu_cmd_wave_vol_change, $3F
	.byte	apu_cmd_note_load|apu_notes::C4, $60

	.byte	apu_cmd_wave_vol_change, $30
	.byte	$18

		: .byte	apu_cmd_wave_vol_change, $4F
		.byte	apu_cmd_note_load|apu_notes::Ax4, $13
		.byte	apu_cmd_note_load|apu_notes::Ax4, $5
		.byte	apu_cmd_note_load|apu_notes::A4, $18
		.byte	apu_cmd_note_load|apu_notes::C5, $18
		.byte	apu_cmd_note_load|apu_notes::F5, $18
		.byte	apu_cmd_note_load|apu_notes::F5, $18

		.byte	apu_cmd_wave_vol_change, $7F
		.byte	apu_cmd_note_load|apu_notes::F5, $30
		.byte	apu_cmd_note_load|apu_notes::F5, $30
		.byte	apu_cmd_note_load|apu_notes::Ax5, $24
		.byte	apu_cmd_note_load|apu_notes::A5, $C

		.byte	apu_cmd_wave_vol_change, $40
		.byte	$C

		.byte	apu_cmd_wave_vol_change, $4F
		.byte	apu_cmd_note_load|apu_notes::Ab5, $C
		.byte	apu_cmd_note_load|apu_notes::Ab5, $C
		.byte	apu_cmd_note_load|apu_notes::B5, $C

		.byte	apu_cmd_wave_vol_change, $7F
		.byte	apu_cmd_note_load|apu_notes::A5, $18
		.byte	apu_cmd_note_load|apu_notes::F5, $30

		.byte	apu_cmd_wave_vol_change, $4F
		.byte	apu_cmd_note_load|apu_notes::A3, $13
		.byte	apu_cmd_note_load|apu_notes::A3, $5
		.byte	apu_cmd_note_load|apu_notes::Cx4, $18
		.byte	apu_cmd_note_load|apu_notes::Cx4, $18
		.byte	apu_cmd_note_load|apu_notes::D4, $18
		.byte	apu_cmd_note_load|apu_notes::E4, $18

		.byte	apu_cmd_wave_vol_change, $7F
		.byte	apu_cmd_note_load|apu_notes::F4, $30

		.byte	apu_cmd_wave_vol_change, $4F
		.byte	$C

		.byte	apu_cmd_trough, $C
		.byte	apu_cmd_note_load|apu_notes::F4
		.byte	apu_cmd_note_load|apu_notes::G4
		.byte	apu_cmd_note_load|apu_notes::A4

		.byte	apu_cmd_wave_vol_change, $7F
		.byte	apu_cmd_note_load|apu_notes::D5, $24

		.byte	apu_cmd_wave_vol_change, $4F
		.byte	$C

		.byte	apu_cmd_note_load|apu_notes::B4
		.byte	apu_cmd_note_load|apu_notes::B4
		.byte	apu_cmd_note_load|apu_notes::D5
		.byte	apu_cmd_note_load|apu_notes::D5, $C
		.byte	apu_cmd_note_load|apu_notes::C5, $C
		.byte	apu_cmd_note_load|apu_notes::Ax4, $C
		.byte	apu_cmd_note_load|apu_notes::Ax4, $C

		.byte	apu_cmd_wave_vol_change, $7F
		.byte	apu_cmd_note_load|apu_notes::Cx5, $30

		.byte	apu_cmd_wave_vol_change, $4F
		.byte	apu_cmd_note_load|apu_notes::Cx5
		.byte	apu_cmd_note_load|apu_notes::Cx5
		.byte	apu_cmd_note_load|apu_notes::Cx5

		.byte	apu_cmd_wave_vol_change, $7F
		.byte	apu_cmd_note_load|apu_notes::D5, $24
		.byte	apu_cmd_note_load|apu_notes::A4, $C
		.byte	apu_cmd_note_load|apu_notes::A4, $C
		.byte	apu_cmd_note_load|apu_notes::D5, $30

		.byte	apu_cmd_wave_vol_change, $4F
		.byte	apu_cmd_note_load|apu_notes::B4
		.byte	apu_cmd_note_load|apu_notes::B4
		.byte	apu_cmd_note_load|apu_notes::B4

		.byte	apu_cmd_wave_vol_change, $7F
		.byte	apu_cmd_note_load|apu_notes::Ax4, $24
		.byte	apu_cmd_note_load|apu_notes::Ax4, $C
		.byte	apu_cmd_note_load|apu_notes::Ax4, $C
		.byte	apu_cmd_note_load|apu_notes::Fx5, $30

		.byte	apu_cmd_wave_vol_change, $4F
		.byte	apu_cmd_note_load|apu_notes::Fx5
		.byte	apu_cmd_note_load|apu_notes::G5
		.byte	apu_cmd_note_load|apu_notes::A5

		.byte	apu_cmd_wave_vol_change, $7F
		.byte	apu_cmd_note_load|apu_notes::Ax5, $30

		.byte	apu_cmd_wave_vol_change, $4F
		.byte	apu_cmd_note_load|apu_notes::Ax4
		.byte	apu_cmd_note_load|apu_notes::C5
		.byte	apu_cmd_note_load|apu_notes::D5
		.byte	apu_cmd_trough_end

		.byte	apu_cmd_wave_vol_change, $7F
		.byte	apu_cmd_note_load|apu_notes::D5, $30
		.byte	apu_cmd_note_load|apu_notes::E5, $30
		.byte	apu_cmd_note_load|apu_notes::A4, $3C

		.byte	apu_cmd_wave_vol_change, $45
		.byte	$C

		.byte	apu_cmd_song_jsr
		.addr	:-

title_triangle:
	.byte	apu_cmd_wave_vol_change, $60
	.byte	apu_cmd_note_load|apu_notes::F4
	.byte	$60
	.byte	$60
	.byte	$60
	.byte	$60
	.byte	$60
	.byte	$18

	.byte	apu_cmd_wave_vol_change, $60
	.byte	apu_cmd_trough, $18
		: .byte	apu_cmd_note_load|apu_notes::F4
		.byte	apu_cmd_note_load|apu_notes::E4
		.byte	apu_cmd_note_load|apu_notes::Dx4
		.byte	apu_cmd_note_load|apu_notes::D4

		.byte	apu_cmd_wave_vol_change, $7F
		.byte	apu_cmd_note_load|apu_notes::A3, $18
		.byte	apu_cmd_note_load|apu_notes::Ax3, $18

		.byte	apu_cmd_wave_vol_change, $60
		.byte	apu_cmd_note_load|apu_notes::F4
		.byte	apu_cmd_note_load|apu_notes::C4
		.byte	apu_cmd_note_load|apu_notes::F3
		.byte	apu_cmd_note_load|apu_notes::F4
		.byte	apu_cmd_note_load|apu_notes::F4
		.byte	apu_cmd_note_load|apu_notes::A3
		.byte	apu_cmd_note_load|apu_notes::C4
		.byte	apu_cmd_note_load|apu_notes::F4

		.byte	apu_cmd_wave_vol_change, $7F
		.byte	apu_cmd_note_load|apu_notes::A3, $18
		.byte	apu_cmd_note_load|apu_notes::A3, $18

		.byte	apu_cmd_wave_vol_change, $60
		.byte	apu_cmd_note_load|apu_notes::D4
		.byte	apu_cmd_note_load|apu_notes::A3
		.byte	apu_cmd_note_load|apu_notes::F4
		.byte	apu_cmd_note_load|apu_notes::D4
		.byte	apu_cmd_note_load|apu_notes::B3
		.byte	apu_cmd_note_load|apu_notes::D4
		.byte	apu_cmd_note_load|apu_notes::G4
		.byte	apu_cmd_note_load|apu_notes::G3

		.byte	apu_cmd_wave_vol_change, $7F
		.byte	apu_cmd_note_load|apu_notes::C4, $18

		.byte	apu_cmd_wave_vol_change, $60
		.byte	apu_cmd_note_load|apu_notes::C4
		.byte	apu_cmd_note_load|apu_notes::E4
		.byte	apu_cmd_note_load|apu_notes::A3
		.byte	apu_cmd_note_load|apu_notes::Cx4
		.byte	apu_cmd_note_load|apu_notes::E4
		.byte	apu_cmd_note_load|apu_notes::A3
		.byte	apu_cmd_note_load|apu_notes::D4
		.byte	apu_cmd_note_load|apu_notes::E4
		.byte	apu_cmd_note_load|apu_notes::F4
		.byte	apu_cmd_note_load|apu_notes::D4
		.byte	apu_cmd_note_load|apu_notes::B3
		.byte	apu_cmd_note_load|apu_notes::D4
		.byte	apu_cmd_note_load|apu_notes::G4
		.byte	apu_cmd_note_load|apu_notes::G3
		.byte	apu_cmd_note_load|apu_notes::C4
		.byte	apu_cmd_note_load|apu_notes::C4
		.byte	apu_cmd_note_load|apu_notes::G4
		.byte	apu_cmd_note_load|apu_notes::Ax4
		.byte	apu_cmd_note_load|apu_notes::A4
		.byte	apu_cmd_note_load|apu_notes::Fx4
		.byte	apu_cmd_note_load|apu_notes::D4
		.byte	apu_cmd_note_load|apu_notes::A3
		.byte	apu_cmd_note_load|apu_notes::G3
		.byte	apu_cmd_note_load|apu_notes::A3
		.byte	apu_cmd_note_load|apu_notes::Ax3
		.byte	apu_cmd_note_load|apu_notes::G3
		.byte	apu_cmd_note_load|apu_notes::G4
		.byte	apu_cmd_note_load|apu_notes::C4
		.byte	apu_cmd_note_load|apu_notes::Ax4
		.byte	apu_cmd_note_load|apu_notes::C4
		.byte	apu_cmd_note_load|apu_notes::F4
		.byte	apu_cmd_note_load|apu_notes::C4
		.byte	apu_cmd_note_load|apu_notes::F4, $18

		.byte	apu_cmd_song_jsr
		.addr	:-

title_end:
	.end
