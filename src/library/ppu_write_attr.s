.include	"ppu_nmi_write.i"
.include	"general_bss.i"

	.export ppu_write_attr_addr, ppu_write_attr
ppu_write_attr_from_addr:
	jsr	nt_calc_attr_from_addr
	jmp	ppu_write_attr
ppu_write_attr_addr:
	jsr	nt_calc_attr_addr

ppu_write_attr:
	tya
	pha

	lda	nt_calc_row
	and	#2
	asl	a
	sta	nt_calc_column+1
	lda	nt_calc_column
	and	#2
	clc
	adc	nt_calc_column+1
	tay
	lda	#$FC
	cpy	#0
	beq	:++

	: ; while (y-- > 0) {
		sec
		rol	a
		asl	ppu_nmi_write_data
		dey
		bne	:-
	: ; }

	and	(ppu_nmi_write_dest),y
	ora	ppu_nmi_write_data
	sta	(ppu_nmi_write_dest),y
	sta	ppu_nmi_write_data

	pla
	tay
	rts
