.include	"system/ppu.i"

.include	"ppu_nmi_write.i"
.include	"map_calc.i"
.include	"general_bss.i"

	.export	nt_calc_ppu_addr, nt_calc_ram_addr
nt_calc_ppu_addr:
	lda	#PPU_POS_BIT6
	ora	nt_calc_column
	sta	nt_calc_column
	bne	block_to_tile

nt_calc_ram_addr:
	lda	#PPU_POS_BIT7
	ora	nt_calc_column
	sta	nt_calc_column
	bne	nt_calc_dest

block_to_tile:
	asl	nt_calc_column
	asl	nt_calc_row

	.export nt_calc_dest
nt_calc_dest:
	lda	nt_calc_row
	sta	ppu_nmi_write_dest+1
	lda	#0
	sta	ppu_nmi_write_dest
	lsr	ppu_nmi_write_dest+1
	ror	ppu_nmi_write_dest
	lsr	ppu_nmi_write_dest+1
	ror	ppu_nmi_write_dest
	lsr	ppu_nmi_write_dest+1
	ror	ppu_nmi_write_dest
	lda	nt_calc_column
	and	#PPU_POS_MASK
	clc
	adc	ppu_nmi_write_dest
	sta	ppu_nmi_write_dest
	php

	lda	nt_calc_column
	bpl	:+
	; if (nt_calc_column & PPU_POS_BIT7) {
		lda	#4
		bne	:+++
	: ; } else {
		and	#.hibyte(PPU_VRAM_BG1)
		bne	:+
		; if (PPU_VRAM_BG1 & nt_calc_column) {
			lda	#.hibyte(PPU_VRAM_BG1)
			bne	:++
		: ; } else {
			lda	#.hibyte(PPU_VRAM_BG2)
		; }
	: ; }

	plp
	adc	ppu_nmi_write_dest+1
	sta	ppu_nmi_write_dest+1
	rts

	.export nt_calc_attr_from_addr, nt_calc_attr_addr
nt_calc_attr_from_addr:
	asl	nt_calc_column
	asl	nt_calc_row

nt_calc_attr_addr:
	lda	nt_calc_row
	and	#$FC
	asl	a
	sta	ppu_nmi_write_dest
	lda	nt_calc_column
	and	#PPU_POS_MASK
	lsr	a
	lsr	a
	clc
	adc	ppu_nmi_write_dest
	clc
	adc	#$C0
	sta	ppu_nmi_write_dest

	lda	nt_calc_column
	and	#PPU_POS_BIT5
	bne	:+
	; if (!(nt_calc_column & PPU_POS_BIT5)) { 
		lda	#3
		bne	:++
	: ; } else {
		lda	#7
	: ; }

	sta	ppu_nmi_write_dest+1        
	rts
