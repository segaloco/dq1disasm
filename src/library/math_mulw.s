.include	"general_bss.i"

	.export	math_mulw
math_mulw:
	lda	#0
	sta	math_result
	sta	math_result+1

@loop:
	lda	math_mulw_a
	ora	math_mulw_a+1
	beq	@done

	lsr	math_mulw_a+1
	ror	math_mulw_a
	bcc	@cont

	lda	math_mulw_b
	clc
	adc	math_result
	sta	math_result
	lda	math_mulw_b+1
	adc	math_result+1
	sta	math_result+1
	
@cont:
	asl	math_mulw_b
	rol	math_mulw_b+1
	jmp	@loop

@done:
	rts
