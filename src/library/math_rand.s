.include	"general_bss.i"
.include	"math.i"

	.export math_rand
math_rand:
	lda	math_rand_word+1
	sta	old_rand_word+1
	lda	math_rand_word
	sta	old_rand_word
	asl	math_rand_word
	rol	math_rand_word+1
	clc
	adc	math_rand_word
	sta	math_rand_word
	lda	math_rand_word+1
	adc	old_rand_word+1
	sta	math_rand_word+1
	lda	math_rand_word
	clc
	adc	math_rand_word+1
	sta	math_rand_word+1
	lda	math_rand_word
	clc
	adc	#$81
	sta	math_rand_word
	lda	math_rand_word+1
	adc	#0
	sta	math_rand_word+1
	rts
