.include	"ppu_nmi.i"
.include	"ppu_bss.i"
.include	"general_bss.i"
.include	"map.i"
.include	"sprite.i"
.include	"map_calc.i"
.include	"rpg.i"
.include	"flags.i"
.include	"game_bss.i"
.include	"window.i"
.include	"tunables.i"
.include	"macros.i"

	.export text_getblkno
text_getblkno:
	lda	map_player_x
	sta	map_blk_col
	lda	map_player_y
	sta	map_blk_row
	jsr	map_get_blkid
	jsr	area_data_check
	lda	area_check_retval
	sta	map_cover_stat
	rts

	.export	text_initmap
text_initmap:
	lda	map_eng_number
	cmp	#maps::map_0F
	bcs	:+
	; if (map_eng_number <= map_0F) {
		lda	#1
		sta	rpg_glowdia
		lsr	a
		sta	game_glow_timer
	: ; }

	lda	map_eng_number
	cmp	#maps::castle_throneroom
	beq	:+
	; if (map_eng_number != 5) {
		lda	game_player_flags
		ora	#player_flags::game_started
		sta	game_player_flags
	: ; }

	lda	map_eng_number
	cmp	#maps::overworld
	bne	:++
	; if (map_eng_number == 1) {
		ldx	#0
		txa
		: ; for (x = 0; x < map_door_pos_buffer_end-map_door_pos_buffer; x++) {
			sta	map_door_x_pos,x
			inx
			cpx	#map_door_pos_buffer_end-map_door_pos_buffer
			bne	:-
		; }
	: ; }

	lda	game_player_flags
	and	#player_flags::game_started
	beq	:+
	lda	map_eng_number
	cmp	#maps::castle_throneroom
	bne	:+
	; if (game_player_flags & game_started || map_eng_number == castle_throneroom) {
		lda	#4
		sta	map_chest_x_pos+(7*2)
		sta	map_chest_y_pos+(7*2)
		sta	map_chest_y_pos+(6*2)
		sta	map_door_x_pos+(7*2)
		lda	#5
		sta	map_chest_x_pos+(6*2)
		lda	#6
		sta	map_chest_x_pos+(5*2)
		lda	#1
		sta	map_chest_y_pos+(5*2)
		lda	#7
		sta	map_door_y_pos+(7*2)
	: ; }

	lda	#PPU_H_INIT
	sta	ppu_nt_xpos
	lda	#PPU_V_INIT
	sta	ppu_nt_ypos
	lda	#0
	sta	ppu_nmi_scc_h
	sta	ppu_nmi_scc_v
	sta	ppu_nmi_bg_high
	lda	map_eng_number
	asl	a
	asl	a
	adc	map_eng_number
	tay
	lda	tbl_maps,y
	sta	map_data
	iny
	lda	tbl_maps,y
	sta	map_data+1
	iny
	lda	tbl_maps,y
	sta	map_width
	iny
	lda	tbl_maps,y
	sta	map_height
	iny
	lda	tbl_maps,y
	sta	map_bound_id
	lda	#$FF
	sta	sprite_check_flag

	lda	game_story_flags
	and	#story_flags::defeated_dragonlord
	beq	:+
	lda	map_eng_number
	cmp	#maps::castle_courtyard
	bne	:+
	lda	#$0B
	bne	:++
	: ; if (!defeated_dragonlord || map_eng_number != castle_courtyard) {
		lda	map_eng_number
		sec
		sbc	#4
		cmp	#maps::map_0B
		bcs	initmap_nospec
	: ; } else { a = $0B; }

	asl	a
	tay
	lda	#0
	sta	sprite_check_flag
	lda	tbl_npcs_mobile,y
	sta	general_word_0
	lda	tbl_npcs_mobile+1,y
	sta	general_word_0+1

	lda	#0
	tax
	: ; for (x = 0; x != NPC_MAX; x++) {
		sta	sprite_npc_tbl,x
		inx
		cpx	#(sprite_npc_tbl_end-sprite_npc_tbl)
		bne	:-
	; }

	lda	map_eng_number
	cmp	#maps::map_06
	bne	:+
	lda	game_story_flags
	and	#story_flags::defeated_dragonlord
	bne	:+++++
	: ; if (map_eng_number != map_06 || !(story_flags & defeated_dragonlord)) {
		ldy	#0
		ldx	#0
		: ; for (x = 0, y = 0; general_word_0[y] != NPC_TABLE_END; x += 3, y += 3) {
			lda	(general_word_0),y
			cmp	#NPC_TABLE_END
			beq	:+

			sta	sprite_npc_tbl,x
			inx
			iny
			lda	(general_word_0),y
			sta	sprite_npc_tbl,x
			inx
			iny
			lda	#0
			sta	sprite_npc_tbl,x
			inx
			iny
			jmp	:-
		: ; }

		iny
		ldx	#$1E
		: ; for (x = $1E, y += 1; general_word_0[y] != $FF; x += 3, y += 3) {
			lda	(general_word_0),y
			cmp	#$FF
			beq	:+

			sta	sprite_npc_tbl,x
			inx
			iny
			lda	(general_word_0),y
			sta	sprite_npc_tbl,x
			inx
			iny
			lda	#0
			sta	sprite_npc_tbl,x
			inx
			iny
			jmp	:-
		: ; }
	initmap_nospec: ; }

	lda     map_eng_number
	cmp	#maps::castle_throneroom
	bne	:+
	lda	game_player_flags
	and	#player_flags::lora_saved
	bne	:+
	; if (map_eng_number == castle_throneroom && !(game_player_flags & lora_saved)) {
		lda	#0
		sta	SPRITE_0D_X
		sta	SPRITE_0D_Y
		sta	SPRITE_0D_Z
	: ;}

	; set map type and load overlay if needed
	lda	map_eng_number
	cmp	#maps::map_0F
	bcc	:+
	; if (map_eng_number > $F) {
		lda	#map_types::dungeon
		sta	map_type

	@nocover:
		lda	#0
		sta	map_cover_data
		sta	map_cover_data+1
		rts
	: ; }
	lda	map_eng_number
	cmp	#maps::overworld
	bne	:+
	; else if (map_eng_number == 1) {
		lda	#map_types::overworld
		sta	map_type
		beq	@nocover
	: ; } else {
		lda	#map_types::town
		sta	map_type

		lda	map_eng_number
		cmp	#maps::map_08
		bne	:+
		; if (map_eng_number == map_08) {
			movw_m	map_0C, map_cover_data
			rts
		: ; }
		cmp	#maps::map_09
		bne	:+
		; else if (map_eng_number == map_09) {
			movw_m	map_12, map_cover_data
			rts
		: ; }
		cmp	#maps::map_0A
		bne	:+
		; else if (map_eng_number == map_0A) {
			movw_m	map_cover_0A, map_cover_data
			rts
		: ; }
		cmp	#maps::map_0B
		bne	@nocover
		; else if (map_eng_number == map_0B) {
			movw_m	map_cover_0B, map_cover_data
			rts
		; }
	; }
