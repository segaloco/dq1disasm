.include	"math.i"
.include	"map.i"
.include	"map_calc.i"
.include	"npc.i"
.include	"general_bss.i"

	.export area_check, area_data_check
area_check:
	lda	map_width
	clc
	adc	#1
	lsr	a
	sta	math_mulw_a
	lda	#0
	sta	math_mulw_a+1
	sta	math_mulw_b+1
	lda	map_npc_ypos
	sta	math_mulw_b
	jsr	math_mulw
	lda	map_npc_xpos
	lsr	a
	clc
	adc	math_result
	sta	general_word_1
	lda	math_result+1
	adc	#0
	sta	general_word_1+1

area_data_check:
	lda	map_cover_data
	ora	map_cover_data+1
	bne	:++
	: ; if (*map_cover_data || map_npc_xpos > map_width || map_npc_ypos > map_height) {
		lda	#0
		sta	area_check_retval
		rts

	:
		lda	map_width
		cmp	map_npc_xpos
		bcc	:--
		lda	map_height
		cmp	map_npc_ypos
		bcc	:--
	; }
	
	lda	general_word_1
	clc
	adc	map_cover_data
	sta	general_word_1
	lda	general_word_1+1
	adc	map_cover_data+1
	sta	general_word_1+1
	tya
	pha
	ldy	#0
	lda	(general_word_1),y
	sta	area_check_retval
	pla
	tay

	lda	map_npc_xpos
	and	#1
	bne	:+
	; if (!(map_npc_xpos & 0x1)) {
		lsr	area_check_retval
		lsr	area_check_retval
		lsr	area_check_retval
		lsr	area_check_retval
	: ; }

	lda	area_check_retval
	and	#blkprop::covered
	sta	area_check_retval
	rts
