.include	"math.i"
.include	"ppu_nmi.i"
.include	"ppu_nmi_write.i"
.include	"ppu_bss.i"
.include	"general_bss.i"
.include	"game_bss.i"
.include	"npc.i"
.include	"enemies.i"
.include	"rpg.i"
.include	"flags.i"
.include	"map.i"
.include	"tunables.i"

.segment	"ZEROPAGE"

	.export map_data
	.export map_tile_xpos, map_tile_ypos
	.export map_width, map_height, map_bound_id, map_type
	.export map_cover_data, map_cover_stat
map_tile_xpos:		.byte 0
map_tile_ypos:		.byte 0
map_data:		.addr 0
map_width:		.byte 0
map_height:		.byte 0
map_bound_id:		.byte 0
map_type:		.byte 0
map_cover_data:		.word 0
map_cover_stat:		.byte 0
; ------------------------------------------------------------
.segment	"CODE"

	.export	map_get_blkid
map_get_blkid:
	lda	map_blk_col
	sta	map_npc_xpos
	lda	map_blk_row
	sta	map_npc_ypos
	lda	map_width
	cmp	map_blk_col
	bcs	:++

:
	lda	map_bound_id
	sta	map_blk_col
	rts

:
	lda	battle_enemy
	cmp	#enemy::dragonlord
	bne	:+ ; if (battle_enemy == dragonlord) {
		lda	#$16
		sta	map_blk_col
		rts
	: ; }

	lda	map_height
	cmp	map_blk_row
	bcc	:---

	tya
	pha
	lda	map_eng_number
	cmp	#maps::overworld
	bne	map_check_other

	lda	map_blk_col
	cmp	#$40
	bne	:+
	lda	map_blk_row
	cmp	#$31
	bne	:+
	lda	rpg_flags+1
	and	#>state_flags::bridge_open
	beq	:+ ; if (map_blk_col == $40 && map_blk_row == $31 && bridge_open) {
		lda	#$0A
		sta	map_blk_col
		pla
		tay
		rts
	: ; }

	lda	map_blk_row
	asl	a
	tay
	lda	map_overworld,y
	sta	general_word_2
	lda	map_overworld+1,y
	sta	general_word_2+1
	ldy	#0
	sty	map_blk_col+1
	: ; for (map_blk_col[1] = y = 0; map_blk_col[0] < map_blk_col[1]; y++) {
		lda	(general_word_2),y
		and	#$0F
		sec
		adc	map_blk_col+1
		sta	map_blk_col+1

		lda	map_blk_col
		cmp	map_blk_col+1
		bcc	:+
		iny
		jmp	:-
	: ; }

	lda	(general_word_2),y
	lsr	a
	lsr	a
	lsr	a
	lsr	a
	clc
	adc	map_type
	tay
	lda	tbl_blkids,y
	sta	map_blkid
	pla
	tay
	rts

	.export	map_check_other
map_check_other:
	lda	#0
	sta	math_mulw_a+1
	sta	math_mulw_b+1
	lda	map_width
	lsr	a
	adc	#0
	sta	math_mulw_a
	jsr	math_mulw
	lda	map_npc_xpos
	lsr	a
	clc
	adc	math_result
	sta	general_word_2
	sta	general_word_1
	lda	general_word_2+1
	adc	#0
	sta	general_word_2+1
	sta	general_word_1+1
	lda	general_word_2
	clc
	adc	map_data
	sta	general_word_2
	lda	general_word_2+1
	adc	map_data+1
	sta	general_word_2+1
	ldy	#0
	lda	(general_word_2),y
	sta	general_word_0

	lda	map_npc_xpos
	lsr	a
	bcs	:+ ; if (map_npc_xpos % 2) {
		lsr	general_word_0
		lsr	general_word_0
		lsr	general_word_0
		lsr	general_word_0
	: ; }

	lda	map_eng_number
	cmp	#maps::map_0C
	bcc	:+ ; if (map_eng_number >= map_0C) {
		lda	#7
		bne	:++
	: ; } else {
		lda	#$0F
	: ; }

	and	general_word_0
	clc
	adc	map_type
	tay
	lda	tbl_blkids,y
	sta	general_word_0
	cmp	#$17
	bne	:+ ; if (tbl_blkids[y] == $17) {
		lda	game_player_flags
		and	#(player_flags::lora_saved|player_flags::lora_carry)
		beq	map_check_done

		lda	#4
		sta	general_word_0
		bne	map_check_done
	: ; }
	cmp	#5
	bne	:+ ; else if (tbl_blkids[y] == 5) {
		lda	map_eng_number
		cmp	#maps::map_02
		bne	map_check_done

		lda	map_npc_xpos
		cmp	#$0A
		bne	map_check_done

		lda	map_npc_ypos
		cmp	#1
		bne	map_check_done

		lda	rpg_flags+1
		and	#>state_flags::dragonlord_open
		bne	map_check_done ; if (map == map_02
					; && npc_xpos == $A
					; && npc_ypos == 1
					; && !dragonlord_open) {
			lda	#$0D
			sta	general_word_0
			bne	map_check_done
		; }
	: ; }
	cmp	#$0C
	bne	map_check_adtl

	ldy	#0

:
	lda	map_npc_xpos
	cmp	map_chest_x_pos,y
	bne	:+

	iny
	lda	map_npc_ypos
	cmp	map_chest_x_pos,y
	bne	:++

map_check_resetdone:
	lda	#4
	sta	general_word_0

map_check_done:
	pla
	tay
	rts

:
	iny

:
	iny
	cpy	#$10
	bne	:---

map_check_adtl:
	lda	general_word_0
	cmp	#$11
	bne	map_check_done

	ldy	#0

:
	lda	map_npc_xpos
	cmp	map_door_x_pos,y
	bne	:+

	iny
	lda	map_npc_ypos
	cmp	map_door_x_pos,y
	bne	:++

	beq	map_check_resetdone

:
	iny

:
	iny
	cpy	#$10
	bne	:---

	beq	map_check_done

	.export	map_block_mod
map_block_mod:
	lda	ppu_nt_ypos
	asl	a
	clc
	adc	map_tile_ypos
	clc
	adc	#$1E
	sta	math_div_a
	lda	#$1E
	sta	math_div_b
	jsr	math_divb
	lda	math_result
	sta	nt_calc_row
	sta	scroll_pos_y
	lda	ppu_nt_xpos
	asl	a
	clc
	adc	map_tile_xpos
	and	#$3F
	sta	nt_calc_column
	sta	scroll_pos_x
	jsr	nt_calc_dest
	lda	map_tile_xpos
	asl	a
	lda	map_tile_xpos
	ror	a
	clc
	adc	map_player_x
	sta	map_blk_col
	lda	map_tile_ypos
	asl	a
	lda	map_tile_ypos
	ror	a
	clc
	adc	map_player_y
	sta	map_blk_row
	jsr	map_get_blkid
	lda	map_type
	cmp	#map_types::dungeon
	bne	map_block_mod_skip

	lda	map_tile_xpos
	bpl	:+

	eor	#$FF
	clc
	adc	#1
	sta	general_word_1
	jmp	:++

:
	lda	map_tile_xpos
	sta	general_word_1

:
	lda	rpg_glowdia
	cmp	general_word_1
	bcs	:+

	lda	#$16
	sta	general_word_0
	lda	#0
	sta	ppu_blkdel_flags
	beq	map_block_mod_set

:
	bne	:++

	lda	map_tile_xpos
	bpl	:+

	lda	#5
	sta	ppu_blkdel_flags
	bne	:++

:
	lda	#$0A
	sta	ppu_blkdel_flags

:
	lda	map_tile_ypos
	bpl	:+

	eor	#$FF
	clc
	adc	#1
	sta	general_word_1
	jmp	:++

:
	lda	map_tile_ypos
	sta	general_word_1

:
	lda	rpg_glowdia
	cmp	general_word_1
	bcs	:+

	lda	#$16
	sta	general_word_0
	lda	#0
	sta	ppu_blkdel_flags
	beq	map_block_mod_set

:
	bne	map_block_mod_set

	lda	map_tile_ypos
	bpl	:+

	lda	#3
	sta	ppu_blkdel_flags
	bne	map_block_mod_set

:
	lda	#$0C
	sta	ppu_blkdel_flags
	bne	map_block_mod_set

map_block_mod_skip:
	jsr	area_data_check
	lda	map_cover_stat
	eor	area_check_retval
	and	#8
	beq	map_block_mod_set

	lda	map_cover_stat
	bne	:+

	lda	#$15
	sta	general_word_0
	bne	map_block_mod_set

:
	lda	#$16
	sta	general_word_0

map_block_mod_set:
	lda	general_word_0
	asl	a
	asl	a
	adc	general_word_0
	adc	#$DF
	sta	general_word_2
	lda	#$81
	adc	#0
	sta	general_word_2+1

	ldy	#0
	lda	(general_word_2),y
	sta	ppu_nmi_write_data
	jsr	ppu_nmi_write
	lda	ppu_blkdel_flags
	lsr	a
	bcc	:++

	lda	map_type
	cmp	#map_types::dungeon
	bne	:+

	ldx	ppu_nmi_entry_index
	dex
	lda	#<((tile_blank_chr00-chr00_base)/PPU_TILE_SIZE)
	sta	ppu_nmi_buffer,x
	bne	:++

:
	dec	ppu_nmi_entry_index
	dec	ppu_nmi_entry_index
	dec	ppu_nmi_entry_index
	dec	ppu_nmi_entry_count

:
	iny
	lda	(general_word_2),y
	sta	ppu_nmi_write_data
	jsr	ppu_nmi_write
	lda	ppu_blkdel_flags
	and	#2
	beq	:++

	lda	map_type
	cmp	#map_types::dungeon
	bne	:+

	ldx	ppu_nmi_entry_index
	dex
	lda	#<((tile_blank_chr00-chr00_base)/PPU_TILE_SIZE)
	sta	ppu_nmi_buffer,x
	bne	:++

:
	dec	ppu_nmi_entry_index
	dec	ppu_nmi_entry_index
	dec	ppu_nmi_entry_index
	dec	ppu_nmi_entry_count

:
	iny
	lda	ppu_nmi_write_dest
	clc
	adc	#$1E
	sta	ppu_nmi_write_dest
	bcc	:+

	inc	ppu_nmi_write_dest+1

:
	lda	(general_word_2),y
	sta	ppu_nmi_write_data
	jsr	ppu_nmi_write
	lda	ppu_blkdel_flags
	and	#4
	beq	:++

	lda	map_type
	cmp	#map_types::dungeon
	bne	:+

	ldx	ppu_nmi_entry_index
	dex
	lda	#<((tile_blank_chr00-chr00_base)/PPU_TILE_SIZE)
	sta	ppu_nmi_buffer,x
	bne	:++

:
	dec	ppu_nmi_entry_index
	dec	ppu_nmi_entry_index
	dec	ppu_nmi_entry_index
	dec	ppu_nmi_entry_count

:
	iny
	lda	(general_word_2),y
	sta	ppu_nmi_write_data
	jsr	ppu_nmi_write
	lda	ppu_blkdel_flags
	and	#8
	beq	:++

	lda	map_type
	cmp	#map_types::dungeon
	bne	:+

	ldx	ppu_nmi_entry_index
	dex
	lda	#<((tile_blank_chr00-chr00_base)/PPU_TILE_SIZE)
	sta	ppu_nmi_buffer,x
	bne	:++

:
	dec	ppu_nmi_entry_index
	dec	ppu_nmi_entry_index
	dec	ppu_nmi_entry_index
	dec	ppu_nmi_entry_count

:
	iny
	lda	scroll_pos_x
	sta	nt_calc_column
	lda	scroll_pos_y
	sta	nt_calc_row
	lda	(general_word_2),y
	sta	ppu_nmi_write_data
	jsr	ppu_write_attr_addr
	lda	rpg_ppulayout
	bne	:+

	lda	ppu_nmi_write_dest+1
	clc
	adc	#$20
	sta	ppu_nmi_write_dest+1
	jsr	ppu_nmi_write

:
	rts
