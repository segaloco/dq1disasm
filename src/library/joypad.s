.include	"system/ctrl.i"

.include	"math.i"

.segment	"ZEROPAGE"

joypad_bit:	.byte	0

	.export joypad_state
joypad_state:	.byte	0
; ------------------------------------------------------------
.segment	"CODE"

	.export joypad_read
joypad_read:
	lda	old_rand_word
	pha
	lda	old_rand_word+1
	pha
	jsr	math_rand
	pla
	sta	old_rand_word+1
	pla
	sta	old_rand_word

	lda	#1
	sta	CTRL0
	lda	#0
	sta	CTRL0

	ldy	#CTRL_BIT_COUNT
	: ; for (bit of controller) {
		lda	CTRL0
		sta	joypad_bit
		lsr	a
		ora	joypad_bit
		lsr	a
		ror	joypad_state

		dey
		bne	:-
	; }

	rts
