.include	"general_bss.i"

	.export	math_divb, math_divw
math_divb:
	lda	#0
	sta	math_div_a+1

math_divw:
	ldy	#$10
	lda	#0
	: ; for (y = 0x10, y < 0; y--) {
		asl	math_div_a
		rol	math_div_a+1
		sta	math_result
		adc	math_result
		inc	math_div_a
		sec
		sbc	math_div_b
		bcs	:+
		; if (math_div_b < 0) {
			clc
			adc	math_div_b
			dec	math_div_a
		: ; }

		dey
		bne	:--
	; }

	sta	math_result
	rts
