.segment	"ZEROPAGE"

	.export rpg_glowdia, rpg_ppulayout
	.export window_text_x, window_text_y
	.export window_char_x
	.export window_char_y
	.export window_string_len
	.export window_choice, window_column, window_row
	.export game_glow_timer, game_protection_timer
	.export game_win_ppuaddr
	.export dialog_item_idx
	.export game_player_flags
	.export battle_enemy
	.export tile_current
	.export battle_enemy_hp
	.export magic_armor_buff
	.export game_story_flags
	.export msg_speed
rpg_glowdia:		.byte	0
rpg_ppulayout:		.byte	0
window_text_x:		.byte	0
window_text_y:		.byte	0
window_char_x:		.byte	0
window_char_y:		.byte	0
window_string_len:	.byte	0
window_choice:		.byte	0
window_column:		.byte	0
window_row:		.byte	0
game_glow_timer:	.byte	0
game_protection_timer:	.byte	0
game_win_ppuaddr:	.word	0
dialog_item_idx:	.byte	0
game_player_flags:	.byte	0
battle_enemy:		.byte	0
tile_current:		.byte	0
battle_enemy_hp:	.byte	0
magic_armor_buff:	.byte	0
game_story_flags:	.byte	0
msg_speed:		.byte	0
