.include	"system/cpu.i"
.include	"system/cnrom.i"
.include	"system/ppu.i"

.include	"ppu_nmi_write.i"
.include	"ppu_pal.i"
.include	"apu_commands.i"
.include	"bssmap.i"
.include	"joypad.i"
.include	"general_bss.i"
.include	"game_bss.i"
.include	"ppu_bss.i"
.include	"math.i"
.include	"map.i"
.include	"map_calc.i"
.include	"window.i"
.include	"npc.i"
.include	"sprite.i"
.include	"rpg.i"
.include	"flags.i"
.include	"tunables.i"
.include	"enemies.i"
.include	"macros.i"

.include	"./chr3/title.i"

.segment	"BSS"
.org		GAME_BUFFER
	.export game_buffer
game_buffer:

.segment	"CODE"
.reloc

; _main - this is the kickoff of the game engine proper
; the previous section is separated from this in the
; US release of Dragon Warrior, but falls through nonetheless
; first the APU engine is initialized, followed by reset of hitpoints
; the PPU nametable position is initialized and the titlescreen is loaded
; the results of the titlescreen fork into the new player and password screens
; either screen results in an initialized character and game state
; the player is placed in the king's chambers
; and the dialog is influence by whether one is playing a new game or
; their level upon continuing
; after clearing the dialog, the game enters its main loop
	.export	game_frame_next
_main:
	.if	.defined(DQ)
		jsr	apu_init
	.elseif	.defined(DW)
		lda	#0
		jsr	mmc1_prg_load

		lda	#0
		tax
		sta	pal_dragonlord
		sta	char_dir

		: ; for (x = 0; x < 0x10; x++) {
			sta	trsr_x_pos, x

			inx
			cpx	#$10
			bcc	:-
		; }

		brk
		.byte	$02, $17

		jsr	mmc1_nt0_bank0
		jsr	mmc1_nt1_bank3
	.endif	; DQ|DW

	lda	#<~0
	sta	rpg_hp

	lda	#PPU_H_INIT
	sta	ppu_nt_xpos
	lda	#PPU_V_INIT
	sta	ppu_nt_ypos

	.if	.defined(DQ)
		movw_m	CNROM_BANK_INDEX_3_H|title_load_map_addr, cnrom_load_src
		jsr	game_set_subbank
		movw_m	title_load_map_end-title_load_map_start, cnrom_load_count
		jsr	cnrom_load
		jsr	title_load_map
	
		movw_m	CNROM_BANK_INDEX_3_H|title_addr, cnrom_load_src
		movw_m	cnrom_buffer, cnrom_load_dest
		movw_m	title_end-title_start, cnrom_load_count
		jsr	cnrom_load
		jsr	cnrom_bank_sec_02
		lda	#ppu_ctlr1::objblk_off|ppu_ctlr1::bgblk_off|ppu_ctlr1::color
		sta	PPU_CTLR1
		jsr	title
	
		jsr	ppu_nmi_wait
		jsr	cnrom_bank_sec_00
	
		lda	title_start_cursor_pos_y
		cmp	#TITLE_START_ROW
		beq	:+
		jmp	:++
		: ; if (title_start_cursor_pos_y == TITLE_START_ROW) {
			jsr	game_set_subbank
			movw_m  CNROM_BANK_INDEX_3_H|start_new_load_map_addr, cnrom_load_src
			movw_m  start_new_load_map_end-start_new_load_map_start, cnrom_load_count
			jsr	cnrom_load
			jsr	start_new_load_map
	
			jsr	game_set_subbank
			movw_m  CNROM_BANK_INDEX_3_H|start_new_addr, cnrom_load_src
			movw_m  start_new_end-start_new_start, cnrom_load_count
			jsr	cnrom_load
			jsr	start_new
	
			jmp	:++
		: ; } else {
			jsr	game_set_subbank
			movw_m  CNROM_BANK_INDEX_3_H|start_pw_load_map_addr, cnrom_load_src
			movw_m  start_pw_load_map_end-start_pw_load_map_start, cnrom_load_count
			jsr	cnrom_load
			jsr	start_pw_load_map
	
			movw_m  cnrom_buffer2, cnrom_load_dest
			movw_m  CNROM_BANK_INDEX_3_H|start_pw_addr, cnrom_load_src
			movw_m  start_pw_end-start_pw_start, cnrom_load_count
			jsr	cnrom_load
			jsr	start_pw
		: ; }
	.elseif	.defined(DW)
		jsr	title_dw

		lda	#1
		jsr	ppu_wait_buf

		lda	#$18
		sta	PPU_CTLR1

		lda	#0
		sta	apu_stat

		brk
		.byte	$00, $27

		lda	#0
		sta	PPU_CTLR1

		jsr	ppu_wait_nmi
		jsr	mmc1_nt0_bank1
		jsr	mmc1_nt1_bank2

		lda	#VILLAGE
		brk
		.byte	$04, $17

		jsr	start_save
	.endif	; DQ|DW

	lda	#$FA
	sta	rpg_started

	lda	#0
	tax
	: ; foreach (char entry in map_door_pos) {
		sta	map_door_pos_buffer, X
		inx
		cpx	#map_door_pos_buffer_end-map_door_pos_buffer
		bne	:-
	; }

	jsr	game_set_start_room
	lda	game_player_flags
	and	#player_flags::game_started
	beq	:+++ ; if (game_player_flags & game_started) {
		window_high_m	((str_117_offset-tbl_string_offsets)/WORD_SIZE)
		lda	rpg_level
		cmp	#<(LEVEL_MAX)
		bne	:+ ; if (rpg_level == LEVEL_MAX) {
			window_low_m	((str_002_offset-tbl_string_offsets)/WORD_SIZE)
			jmp	:++
		: ; } else {
			jsr	stats_calc_exp
			window_low_m	((str_0C1_offset-tbl_string_offsets)/WORD_SIZE)
			window_high_m	((str_118_offset-tbl_string_offsets)/WORD_SIZE)
		: ; }

		window_low_m	((str_0C4_offset-tbl_string_offsets)/WORD_SIZE)
		jmp	:++
	: ; } else {
		window_high_m	((str_102_offset-tbl_string_offsets)/WORD_SIZE)
	: ; }

	jsr	dialog_wait_button
	window_function_clear_m	((window_dialog-tbl_windows)/FUNC_WINDOW_DEF_SIZE)
	lda	#NPCS_RUN
	sta	sprite_npc_stop

	; game_main_loop - this is the outer loop of the game
	; all execution until the game's completion ultimately returns here
	; the glow and protection countdowns are first ticked
	; next, the game awaits input
	; start will pause, a will trigger the map dialog system
	; and directions will trigger a map move in that direction
	; swinging back around to tick the glow and protection timers
	; the user stats are displayed after some inactivity
	; only movements trigger the next glow and protection ticks
	game_main_loop: ; for (;;) {
	lda	game_glow_timer
	beq	:+
	dec	game_glow_timer
	bne	:+
	lda	rpg_glowdia
	cmp	#SPELL_GLOW_DIA_MIN-SPELL_GLOW_DIA_INCR
	beq	:+ ; if (--game_glow_timer == 0 && rpg_glowdia > 1) {
		lda	#SPELL_GLOW_SHRINK_TIME
		sta	game_glow_timer
		dec	rpg_glowdia
		dec	rpg_glowdia
	: ; }

	lda	game_protection_timer
	beq	:++++ ; if (game_protection_timer != 0) {
		dec	game_protection_timer
		dec	game_protection_timer
		beq	:+
		lda	game_protection_timer
		cmp	#1
		bne	:++++
		: ; if ((game_protection_timer -= 2) == 0 || game_protection_timer == 1) {
			jsr	ppu_nmi_wait
			lda	#NPCS_STOP
			sta	sprite_npc_stop
			window_function_m	((window_dialog-tbl_windows)/FUNC_WINDOW_DEF_SIZE)
			lda	game_protection_timer
			bne	:+ ; if () {
				lda	#<((str_037_offset-tbl_string_offsets)/WORD_SIZE)
				bne	:++
			: ; } else {
				lda	#<((str_034_offset-tbl_string_offsets)/WORD_SIZE)
			: ; }

			jsr	window_string_low_a

			jsr	dialog_resume_game
			lda	#0
			sta	game_protection_timer
		; }
	: ; }

	game_ctrl_cycle: ; for (;;) {
	lda	#0
	sta	ppu_frame_count
			game_frame_cycle: ; for (;;) {
				jsr	joypad_read
				lda	joypad_state
				and	#joypad_button::start
				beq	:++++++ ; if (joypad_read() == start) {
					: ; while ((ppu_frame_count & ANI_FRAME_COUNT_MASK) != ANI_OBJ_FRAME_ID) {
						jsr	ppu_nmi_wait
						lda	ppu_frame_count
						and	#ANI_FRAME_COUNT_MASK
						cmp	#ANI_OBJ_FRAME_ID
						beq	:+

						jsr	sprites_draw
						jmp	:-
					: ; }

					: ; while (joypad_read() == start);
						jsr	joypad_read
						lda	joypad_state
						and	#joypad_button::start
						bne	:-

					: ; while (joypad_read() != start);
						jsr	joypad_read
						lda	joypad_state
						and	#joypad_button::start
						beq	:-

					: ; while (joypad_read() == start);
						jsr	joypad_read
						lda	joypad_state
						and	#joypad_button::start
						bne	:-

					jmp	game_ctrl_cycle
				:
				lda	joypad_state
				lsr	a
				bcc	:+ ; } else if (joypad_state == face_a) {
					jsr	dialog_command
					jmp	game_ctrl_cycle
				:
				lda	joypad_state
				and	#joypad_button::up
				beq	:+ ; } else if (joypad_state == up) {
					jsr	map_moveup
					jsr	map_doorcheck
					jmp	game_main_loop
				:
				lda	joypad_state
				and	#joypad_button::down
				beq	:+ ; } else if (joypad_state == down) {
					jsr	map_movedown
					jsr	map_doorcheck
					jmp	game_main_loop
				: 
				lda	joypad_state
				and	#joypad_button::left
				beq	:+ ; } else if (joypad_state == left) {
					jsr	map_moveleft
					jsr	map_doorcheck
					jmp	game_main_loop
				:
				lda	joypad_state
				bpl	game_frame_next ; } else if (joypad_state == right) {
					jsr	map_moveright
					jsr	map_doorcheck
					jmp	game_main_loop
				game_frame_next: ; } else {
					jsr	ppu_nmi_wait

					lda	ppu_frame_count
					cmp	#GAME_MENU_POPFRAME-1
					bne	:+ ; if (ppu_frame_count == (GAME_MENU_POPFRAME-1)) {
						window_function_m	((window_main_field-tbl_windows)/FUNC_WINDOW_DEF_SIZE)
						lda	#GAME_MENU_POPFRAME
						sta	ppu_frame_count
					: ; }

					jsr	sprites_draw
					jmp	game_frame_cycle
				; }
			; }
		; }
	; }
; ------------------------------------------------------------
	.export game_set_start_room
; game_set_start_room - this subroutine initializes the game map state
; to the player's last conversation with the king in his chambers, either
; in the event of a new game or getting a game over
; stats are refreshed and curses applied
; the map engine transitions to the player in the king's chambers
; and awaits any input
game_set_start_room:
	jsr	ppu_nmi_wait

	movw_m	ppu_pal_null, ppu_pal_load_src
	lda	#0
	sta	ppu_pal_fade_idx
	jsr	ppu_pal_load_obj
	lda	#$30
	sta	ppu_pal_fade_idx
	jsr	ppu_pal_load_bg
	jsr	ppu_nmi_wait

	jsr	stats_load

	lda	rpg_flags+1
	and	#>(state_flags::equipped_cursed_necklace|state_flags::equipped_cursed_belt)
	beq	:+ ; if (rpg_flags & (equipped_cursed_necklace|equipped_cursed_belt)) {
		lda	#1
		sta	rpg_hp
		jmp	:++
	: ; } else {
		lda	rpg_maxhp
		sta	rpg_hp
		lda	rpg_maxmp
		sta	rpg_mp
	: ; }

	lda	#3
	sta	map_player_x
	sta	sprite_char_x
	lda	#4
	sta	map_player_y
	sta	sprite_char_y
	lda	#$30
	sta	sprite_x
	lda	#$40
	sta	sprite_y
	lda	#0
	sta	window_string_len
	sta	game_protection_timer
	sta	sprite_x+1
	sta	sprite_y+1
	lda	#PPU_H_INIT
	sta	ppu_nt_xpos
	lda	#PPU_V_INIT
	sta	ppu_nt_ypos
	lda	#maps::castle_throneroom
	sta	map_eng_number
	jsr	ppu_clearram
	jsr	map_transition2
	jsr	ppu_nmi_wait

	lda	#0
	sta	ppu_frame_count

	: ; while (!joypad_read()) {
		jsr	joypad_read
		lda	joypad_state
		bne	:+

		jsr	ppu_nmi_wait

		jsr	sprites_draw
		jmp	:-
	; }

	: ; while ((ppu_frame_count & ANI_FRAME_COUNT_MASK) != ANI_OBJ_FRAME_ID) {
		jsr	ppu_nmi_wait

		lda	ppu_frame_count
		and	#ANI_FRAME_COUNT_MASK
		cmp	#ANI_OBJ_FRAME_ID
		beq	:+

		jsr	sprites_draw
		jmp	:-
	: ; }

	lda	#NPCS_STOP
	sta	sprite_npc_stop
	window_function_m	((window_dialog-tbl_windows)/FUNC_WINDOW_DEF_SIZE)
	rts
; ------------------------------------------------------------
	.export game_set_subbank
game_set_subbank:
	movw_m	game_buffer, cnrom_load_dest
	rts
; ------------------------------------------------------------
game_map_block	= battle_enemy

	.export game_checkevent
game_checkevent:
	lda	game_story_flags
	and	#story_flags::defeated_dragonlord
	bne	:+
	jmp	game_domovement
	: ; if (defeated_dragonlord) {
		lda	map_eng_number
		cmp	#maps::castle_courtyard
		bne	:+
		lda	map_player_y
		cmp	#8
		bne	:+
		lda	map_player_x
		cmp	#$0A
		beq	:++
		cmp	#$0B
		beq	:++
		: jmp	game_stepblock
		: ; if (map == castle_courtyard && map_player_y == 8 && map_player_x.in($A, $B)) {
			lda	#NPCS_STOP
			sta	sprite_npc_stop
			window_function_m	((window_dialog-tbl_windows)/FUNC_WINDOW_DEF_SIZE)
			window_high_m	((str_11B_offset-tbl_string_offsets)/WORD_SIZE)
			lda	game_player_flags
			and	#player_flags::lora_saved
			beq	:+++ ; if (lora_saved) {
				lda	#LORA_XPOS_MIN
				sta	sprite_npc_lora_xpos
				lda	#LORA_YPOS
				sta	sprite_npc_lora_ypos
				lda	#0
				sta	sprite_npc_lora_offset
				jsr	ppu_nmi_wait
				jsr	sprites_draw
				window_high_m	((str_11C_offset-tbl_string_offsets)/WORD_SIZE)
				: ; while (sprite_npc_lora_xpos != LORA_XPOS_MAX) {
					jsr	ppu_nmi_wait
					jsr	ppu_nmi_wait
					lda	sprite_npc_lora_offset
					clc
					adc	#LORA_OFFSET
					sta	sprite_npc_lora_offset
					bcc	:+
						inc	sprite_npc_lora_xpos
					:

					jsr	sprites_draw
					lda	sprite_npc_lora_xpos
					cmp	#LORA_XPOS_MAX
					bne	:--
				; }

				beq	:++
			: ; } else {
				lda	game_player_flags
				lsr	a
				bcc	:++++ ; if (lora_carry) {
					lda	game_player_flags
					and	#<~(player_flags::lora_carry)
					sta	game_player_flags
					lda	#LORA_XPOS_MAX
					sta	sprite_npc_lora_xpos
					lda	#LORA_YPOS
					sta	sprite_npc_lora_ypos
					lda	#0
					sta	sprite_npc_lora_offset
					jsr	ppu_nmi_wait
					jsr	sprites_draw
					window_high_m	((str_11C_offset-tbl_string_offsets)/WORD_SIZE)
				; }
			: ; } if (lora_saved || !lora_carry) {
				window_high_m	((str_11D_offset-tbl_string_offsets)/WORD_SIZE)
				: ; while (window_decision() == no) {
					window_high_m	((str_11E_offset-tbl_string_offsets)/WORD_SIZE)
					window_function_m	((window_decision-tbl_windows)/FUNC_WINDOW_DEF_SIZE)
					beq :+

					window_low_m	((str_0B6_offset-tbl_string_offsets)/WORD_SIZE)
					jmp	:-
				: ; }

				window_low_m	((str_0B8_offset-tbl_string_offsets)/WORD_SIZE)
				lda	#0
				sta	sprite_npc_lora_xpos
				sta	sprite_npc_lora_ypos
				sta	sprite_npc_lora_offset
				lda	game_player_flags
				ora	#player_flags::lora_carry
				sta	game_player_flags
				jsr	ppu_nmi_wait
				jsr	sprites_draw
			: ; }

			window_high_m	((str_122_offset-tbl_string_offsets)/WORD_SIZE)
			ldx	#$78
			: ; for (x = $78; x > 0; x--) {
				jsr	ppu_nmi_wait
				dex
				bne	:-
			; }

			window_function_clear_m	((window_dialog-tbl_windows)/FUNC_WINDOW_DEF_SIZE)
			ldx	#$1E
			: ; for (x = $1E; x > 0; x--) {
				jsr	ppu_nmi_wait
				dex
				bne	:-
			; }

			lda	#$FF
			sta	rpg_level
			jsr	sprites_draw
			jmp	game_credits_load
		; }
	game_domovement: ; } else {
		lda	rpg_equip
		and	#$1C
		cmp	#$1C
		beq	:+
		cmp	#$18
		bne	:+++
		inc	magic_armor_buff
		lda	magic_armor_buff
		and	#$03
		bne	:+++
		: ; if ((rpg_equip & $1C) == $1C || (rpg_equip == $18 && !(++magic_armor_buff & $3))) {
			inc	rpg_hp
			lda	rpg_hp
			cmp	rpg_maxhp
			bcc	:+ ; if (++rpg_hp > rpg_maxhp) {
				lda	rpg_maxhp
				sta	rpg_hp
			: ; }

			lda	rpg_maxhp
			lsr	a
			lsr	a
			clc
			adc	#1
			cmp	rpg_hp
			bcs	:+ ; if (rpg_hp < ((rpg_maxhp / 4) + 1)) {
				movw_m	PPU_COLOR_PAGE_BG+1, ppu_nmi_write_dest
				lda	#PPU_COLOR_LEV3|PPU_COLOR_GREY
				sta	ppu_nmi_write_data
				jsr	ppu_nmi_write
			; }
		: ; }

		lda	map_eng_number
		cmp	#maps::castle_courtyard
		bne	:+
		lda	rpg_flags+1
		and	#>(state_flags::equipped_cursed_necklace|state_flags::equipped_cursed_belt)
		beq	:+
		lda	map_player_y
		cmp	#$1B
		bne	:+ ; if (map == castle_courtyard && (equipped_cursed_necklace || equipped_cursed_belt) && map_player_y == $1B) {
			lda	#NPCS_STOP
			sta	sprite_npc_stop
			window_function_m	((window_dialog-tbl_windows)/FUNC_WINDOW_DEF_SIZE)
			window_low_m	((str_044_offset-tbl_string_offsets)/WORD_SIZE)
			jmp	map_dodoor
		: ; }

		lda	map_eng_number
		cmp	#maps::map_03
		bne	:+
		lda	map_player_x
		cmp	#AXE_KNIGHT_X
		bne	:+
		lda	map_player_y
		cmp	#AXE_KNIGHT_Y
		bne	:+ ; if (map == map_03 && map_player_x == AXE_KNIGHT_X && map_player_y == AXE_KNIGHT_Y) {
			lda	#enemy::knight_axe
			jmp	battle_init
		: ; }

		lda	map_eng_number
		cmp	#maps::dungeon_greendragon
		bne	:+
		lda	map_player_x
		cmp	#GREENDRAGON_X
		bne	:+
		lda	map_player_y
		cmp	#GREENDRAGON_Y
		bne	:+
		lda	game_story_flags
		and	#story_flags::defeated_greendragon
		bne	:+ ; if (map == dungeon_greendragon && map_player_x == GREENDRAGON_X && map_player_y == GREENDRAGON_Y && !defeated_greendragon) {
			lda	#enemy::dragon_green
			jmp	battle_init
		: ; }

		lda	map_eng_number
		cmp	#maps::overworld
		bne	:+
		lda	map_player_x
		cmp	#GOLEM_X
		bne	:+
		lda	map_player_y
		cmp	#GOLEM_Y
		bne	:+
		lda	game_story_flags
		and	#story_flags::defeated_golem
		bne	:+ ; if (map == overworld && map_player_x == GOLEM_X && map_player_y = GOLEM_Y && !defeated_golem) {
			lda	#enemy::golem
			jmp	battle_init
		: ; }
	; }

game_stepblock:
	jsr	math_rand
	lda	map_player_x
	sta	map_blk_col
	lda	map_player_y
	sta	map_blk_row
	jsr	map_get_blkid
	lda	map_blkid
	sta	game_map_block
	cmp	#7
	bcc	:+
	cmp	#$0A
	bcs	:+ ; if (player_blkid > 7 && player_blkid < 10) {
		jmp	dialog_mapchange_low
	: ; }

	lda	game_story_flags
	and	#story_flags::defeated_dragonlord
	beq	:+ ; if (defeated_dragonlord) {
		rts
	: ; }

	lda	game_map_block
	cmp	#6
	bne	:+++ ; if (player_blkid == 6) {
		lda	rpg_equip
		and	#$1C
		cmp	#$1C
		beq	:++ ; if (player_blkid == 6 && (rpg_equip & $1C) != $1C) {
			apusfx_m	sfx_swamp
			jsr	apu_play
			jsr	battle_screenflash
			jsr	ppu_nmi_wait
			lda	rpg_hp
			sec
			sbc	#2
			bcs	:+
				lda	#0
			:

			sta	rpg_hp
			jsr	ppu_nmi_wait
			jsr	battle_palette_normal
			lda	rpg_hp
			bne	:+ ; if (!rpg_hp) {
				window_function_m	((window_main_field-tbl_windows)/FUNC_WINDOW_DEF_SIZE)
				window_function_m	((window_dialog-tbl_windows)/FUNC_WINDOW_DEF_SIZE)
				jmp	battle_dokill_player
			; }
		: ; }

		lda	#MATH_CHANCE_EQ_1_16
	game_stepdone:
		and	math_rand_word+1
		beq	game_fight
		rts
	: ; }

	cmp	#1
	beq	:+
	cmp	#2
	bne	:++ ; if (player_blkid == 2) {
		jsr	ppu_nmi_wait
		jsr	ppu_nmi_wait
		jsr	ppu_nmi_wait
	: ; } if (player_blkid.in(1, 2)) {
		lda	#MATH_CHANCE_EQ_1_8
		bne	game_stepdone
	: ; }

	cmp	#$0B
	beq	game_fight_check
	cmp	#4
	beq	game_fight_check
	cmp	#$0D
	bne	:++++
	lda	rpg_equip
	and	#$1C
	cmp	#$1C
	beq	game_fight_check ; if (player_blkid == $D && (rpg_equip & $1C) != $1C) {
		apusfx_m	sfx_ffdamage
		jsr	apu_play
		lda	#3
		sta	general_byte_0
		: ; for (general_byte_0 = 3; general_byte_0 > 0; general_byte_0--) {
			jsr	ppu_nmi_wait
			jsr	battle_screenflash
			jsr	ppu_nmi_wait
			jsr	battle_palette_normal
			dec	general_byte_0
			bne	:-
		; }

		lda	rpg_hp
		sec
		sbc	#$0F
		bcs	:+ ; if (rpg_hp < $F) {
			lda	#0
			beq	:+
		: ; }

		sta	rpg_hp
		cmp	#0
		bne	:+ ; if (rpg_hp == 0) {
			window_function_m	((window_main_field-tbl_windows)/FUNC_WINDOW_DEF_SIZE)
			window_function_m	((window_dialog-tbl_windows)/FUNC_WINDOW_DEF_SIZE)
			jmp	battle_dokill_player
		: ; }
	; } else if (player_blkid.in(4, $B)) {
	game_fight_check:
		lda	#MATH_CHANCE_EQ_1_16
		bne	game_stepdone
	: ; } else {
		lda	map_player_x
		lsr	a
		bcs	:+
		lda	map_player_y
		lsr	a
		bcc	:++
		bcs	:++++
		: lda	map_player_y
		lsr	a
		bcc	:+++
		; if (((player_x % 1) && (player_y % 1)) || (!(player_x % 1) && !(player_y % 1))) {
		:	lda	#MATH_CHANCE_EQ_1_32
		:	bne	game_stepdone
		; } else {
		:	lda	#MATH_CHANCE_EQ_1_16
			bne	:--
		; }
	; }

game_fight:
	lda	map_eng_number
	; switch (map_eng_number) {
	cmp	#maps::overworld
	bne	:+++ ; case overworld:
		lda	map_player_y
		sta	math_div_a
		lda	#$0F
		sta	math_div_b
		jsr	math_divb
		lda	math_div_a
		sta	general_byte_0
		lda	map_player_x
		sta	math_div_a
		lda	#$0F
		sta	math_div_b
		jsr	math_divb
		lda	general_byte_0
		asl	a
		asl	a
		sta	general_word_1
		lda	general_word_0
		lsr	a
		clc
		adc	general_word_1
		tax
		lda	tbl_encounters,x
		sta	general_word_1
		lda	general_word_0
		lsr	a
		bcs	:+ ; if (general_word_0 & $01) {
			lsr	general_word_1
			lsr	general_word_1
			lsr	general_word_1
			lsr	general_word_1
		: ; }

		lda	general_word_1
		and	#$0F
		bne	game_fight_break ; if (general_word_1 & $0F) {
			jsr	math_rand
			lda	battle_enemy
			cmp	#2
			bne	:+ ; if (battle_enemy == 2) {
				lda	math_rand_word+1
				and	#MATH_CHANCE_EQ_1_4
				beq	game_fight_break
				rts
			: ; } else {
				lda	math_rand_word+1
				and	#MATH_CHANCE_EQ_1_2
				beq	game_fight_break
				rts
			; }
		; }

	:
	cmp	#maps::map_02
	bne	:+ ; case map_02:
		lda	#$10
		bne	game_fight_break

	:
	cmp	#maps::map_03
	bne	:+ ; case map_03:
		lda	#$0D
		bne	game_fight_break

	:
	cmp	#maps::map_06
	bne	:+ ; case map_06:
		lda	#$12
		bne	game_fight_break

	: ; default:
		cmp	#maps::map_1C
		bcs	:+
		lda	map_type
		cmp	#map_types::dungeon
		beq	:++
		: ; if (map < map_1C && map_type != dungeon) {
			rts
		: ; }

		lda	map_eng_number
		sec
		sbc	#$0F
		tax
		lda	tbl_caves,x
	; }

game_fight_break:
	sta	general_word_1
	asl	a
	asl	a
	clc
	adc	general_word_1
	sta	general_word_1
	: ; while ((math_rand() & $07) > 5) {
		jsr	math_rand
		lda	math_rand_word+1
		and	#$07
		cmp	#5
		bcs	:-
	; }

	adc	general_word_1
	tax
	lda	tbl_enemygroups,x
	sta	general_word_0
	lda	map_eng_number
	cmp	#maps::overworld
	bne	:++
	lda	game_protection_timer
	beq	:++
	lda	rpg_def
	lsr	a
	sta	general_word_1
	ldx	general_word_0
	lda	tbl_protection,x
	sec
	sbc	general_word_1
	bcc	:+
	sta	general_word_1
	lda	tbl_protection,x
	lsr	a
	cmp	general_word_1
	bcc	:++
	: ; if ((map_eng_number == overworld && game_protection_timer) || --tbl_protection[x] > 0) {
		rts
	: ; } else {
		lda	general_word_0
		jmp	battle_init
	; }
