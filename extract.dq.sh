#!/bin/sh

# dd positions of banks
# iNES - skip=0     count=16
# PRG0 - skip=16    count=16384
# PRG1 - skip=16400 count=16384
# CHR0 - skip=32784 count=8192
# CHR1 - skip=40976 count=8192
# CHR2 - skip=49168 count=8192
# CHR3 - skip=57360 count=8192

INFILE=$1

if [ -z $INFILE ]
then
	INFILE=/dev/zero
fi

# Directory structure
mkdir -p data/chr00 data/chr02

# CHR Common
TILE_SIZE=16
OBJ_BANK=256

# CHR bank 00
BANKBASE=32784
BANKLEN=8192
TILES=`expr $BANKLEN / $TILE_SIZE`

I=0
while [ $I -ne $OBJ_BANK ] && [ $I -ne $TILES ]
do
        dd if=$INFILE of=data/chr00/tile_bg_`printf "%.2X" $I`.bin bs=1 count=$TILE_SIZE skip=`expr $I \* $TILE_SIZE + $BANKBASE`
        I=`expr $I + 1`
done

while [ $I -ne $TILES ]
do
	J=`expr $I - $OBJ_BANK`
        dd if=$INFILE of=data/chr00/tile_obj_`printf "%.2X" $J`.bin bs=1 count=$TILE_SIZE skip=`expr $I \* $TILE_SIZE + $BANKBASE`
	I=`expr $I + 1`
done

# CHR bank 02
BANKBASE=49168
BANKLEN=5888
TILES=`expr $BANKLEN / $TILE_SIZE`

I=0
while [ $I -ne $OBJ_BANK ] && [ $I -ne $TILES ]
do
        dd if=$INFILE of=data/chr02/tile_bg_`printf "%.2X" $I`.bin bs=1 count=$TILE_SIZE skip=`expr $I \* $TILE_SIZE + $BANKBASE`
        I=`expr $I + 1`
done

while [ $I -ne $TILES ]
do
	J=`expr $I - $OBJ_BANK`
        dd if=$INFILE of=data/chr02/tile_obj_`printf "%.2X" $J`.bin bs=1 count=$TILE_SIZE skip=`expr $I \* $TILE_SIZE + $BANKBASE`
	I=`expr $I + 1`
done
